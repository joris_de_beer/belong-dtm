/*
Name: EBR_trackStep.js
Description: Triggered when a user moves to the next step of a multistep single page form
Extension: Core
Type: Custom Code
*/
digitalData = window.digitalData || _satellite.getVar("data_layer", { caller: "EBR:trackStep" });

var returnVal = true,
	eventName = 'trackStep',
	action = (event.detail||{}).action;

digitalData.track(eventName,event);
_satellite.logger.info('[EBR:'+eventName+']', {action:action, returnVal:returnVal, detail:event.detail});
return returnVal;
