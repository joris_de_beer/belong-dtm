// Initialise the data layer if not already available
window.digitalData = window.digitalData || _satellite.getVar('data_layer')

var returnVal = false,
	eventName = 'trackRouter',
	action = (event.detail||{}).action;

switch (action) {
	case 'onRouteChangeStart':
	case 'onRouteChangeError':
		returnVal = true;
        digitalData.track(eventName,event);
		break;
	default:
		returnVal = false;
		break;
}
_satellite.logger.info('[EBR:'+eventName+']',{action:action, returnVal:returnVal, detail:event.detail});
return returnVal;
