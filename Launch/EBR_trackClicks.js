/*
Name: EBR_track_clicks.js
Description: Format the event detail object for tracked click events
Extension: Core
Type: Custom Code
*/
digitalData = window.digitalData || _satellite.getVar('data_layer', {caller:'EBR:trackClicks'})
var _get = _satellite.getVar('get');
event._this = this
event.action = _get('nativeEvent.type', event)

event.detail = {
	el: event.element,
	action: _get('nativeEvent.type', event),
	text: _get('element.text', event) || _get('element.innerText', event),
	classList: _get('element.classList', event),
	elementPath: _get('nativeEvent.path', event),
	containers: {
		// Orders from tightest to widest
		productCTA: event.element.closest('[data-product-code]'),
		tile: event.element.closest('div[class^="Tilestyles__TileWrapper"]'),
		card: event.element.closest('div[class^="CardV2__CardBase"]'),
		grid: event.element.closest('div[class^="Grid-"]'),
		planSelector: event.element.closest('form[data-testid="choose-plan-form"] div[class^="GridItem-"]'),
		container: event.element.closest('div[class^="Container"]'),
		article: event.element.closest('article'),
		section: event.element.closest('section[class^="Sectionstyles"]'),
		pageTitle: event.element.closest('div[class^="PageTitlestyles"]'),
		header: event.element.closest('header'),
		nav: event.element.closest('nav'),
		footer: event.element.closest('div[class^="GlobalFooterstyles"]')
	}
}
digitalData.updateDataLayer('trackClick', event)
return true;
