// Initialise the data layer if not already available
window.digitalData = window.digitalData || _satellite.getVar('data_layer')

var returnVal = false,
	eventName = 'trackNavigation',
	action = (event.detail||{}).action;

switch (action) {
	case 'onCreate':
	case 'replace currentPage':
		returnVal = true;
        digitalData.track(eventName,event);
		break;
	default:
		returnVal = false;
		break;
}
_satellite.logger.info('[EBR:'+eventName+']',{action:action, returnVal:returnVal, detail:event.detail, event:event});
return returnVal;
