// Initialise the data layer if not already available
window.digitalData = window.digitalData || _satellite.getVar('data_layer')

var returnVal = false,
    eventName = 'trackStore',
	action = (event.detail||{}).action;

switch (action) {
	case 'onCreate':
	case 'replace currentPage':
		returnVal = true;
        digitalData.track(eventName,event);
		break;
	case 'other':
	case 'onPatch':
		returnVal = false;
		break;
	default:
		returnVal = false;
		break;
}
_satellite.logger.info('[EBR:'+eventName+']', {action:action, returnVal:returnVal, detail:event.detail});
return returnVal;
