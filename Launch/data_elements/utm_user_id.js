/*
Name: utm_user_id
Description: Returns the value of the utm_userid query param
Extension: Core
Type: Query param
*/