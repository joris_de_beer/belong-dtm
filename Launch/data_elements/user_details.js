/*
Name: user_details
Description: The decoded contents of the userDetails cookie set by the classic web app. Returns the following object:
	{
		broadbandId: int,
		contactRole: str, contact_role: str,
		email: email,
		expiry: ts,
		firstName: str,
		group: ??,
		lastName: str, lastname: str,
		mobileId int, mobile_id: int,
		userId: str,
		username: str,
		utilitybillId: int,
		isClassicCustomer: bool
	}
Extension: Core
Type: Custom Code
*/
var returnVal,
	userDetailsCookie = _satellite.readCookie ? _satellite.readCookie('belong') : _satellite.cookie.get('userDetails');
var userDetailsDecoded = userDetailsCookie ? decodeURIComponent(userDetailsCookie): null;
try {
	returnVal = JSON.parse(userDetailsDecoded)
}catch(err){
	_satellite.logger.warn('[DE_user_details] unable to read or parse the userDetails cookie', err)
}
return returnVal;
