/*
Name: next_data
Description: Returns the __NEXT_DATA__ object created by the React Application on first page load.
Extension: Core
Type: Custom Code
*/
var nextDataEl;
if(window.__NEXT_DATA__){
	return window.__NEXT_DATA__;
}else{
	_satellite.logger.warn('[DE_next_data] [SLOW] __NEXT_DATA__ returned by parsing DOM')
	nextDataEl = document.getElementById('__NEXT_DATA__');
	return (nextDataEl !== null) ? JSON.parse(nextDataEl.innerText) : null;
}
