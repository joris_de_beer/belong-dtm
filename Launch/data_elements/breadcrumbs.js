/* 
Name: breadcrumbs
Description: Returns an array of folders from the pathname.
Extension: Core
Type: Custom Code
*/
var crumbArray = location.pathname.split('/').slice(1, 100) || [];
if(crumbArray[0] === ""){
	crumbArray[0] = "home";
}
return crumbArray;
