/*
Name: data_layer
Description: returns a complete data layer object.
Extension: Core
Type: Custom Code
*/
// Only run if dataLayer doesn't exist yet.
if (typeof window.digitalData === "object") {
	return window.digitalData;
} else {
_satellite.logger.log('[DE:data_layer] >',(event||{}).caller, ': initialise datalayer');
var initDataLayer = (function () {
	var nextData = _satellite.getVar('next_data'),
		_get = _satellite.getVar('get');
	var ts = Date.now(),
		pageInstanceID = function () { return page.pageInfo.pageName + this.app.env },
		pageLoadTracked = false,
		track = function (eventName, event) { this.updateDataLayer(eventName, event) },
		trackRouter = function (event) { _satellite.logger.warn('[DEPRECATED] trackRoute(event). use track(eventName, {event}) instead.'); this.updateDataLayer('trackRouter', event) },
		/* The following have been consolidated into track(eventName, event)
		trackStore = function (event) { this.updateDataLayer('trackStore', event) },
		trackNavigation = function (event) { this.updateDataLayer('trackNavigation', event) },
		trackAPI = function (event) { this.updateDataLayer('trackAPI', event) },
		*/
		trackState = function (event) {
			_satellite.logger.warn('[DEPRECATED] trackState(event). use track(eventName, {event}) instead.');
			this.updateDataLayer('trackState', event)
		},
		event = [], //An array of events recorded since the last hard page refresh
		eventSeq = {}, // A list of events recorded since the last hard page refresh.
		version = "1.0",
		app = {
			env: _get('runtimeConfig.env.isProduction', nextData ? "prod" : "dev"),
			buildId: _get('buildId', nextData),
			version: _get('runtimeConfig.version', nextData),
			cfEnv: _get('runtimeConfig.contentful.environment', nextData),
			cfSpace: _get('runtimeConfig.contentful.space', nextData),
			workspace: _get('runtimeConfig.workspace', nextData)
		},
		error = [{
			code: undefined,
			description: undefined,
			sequence: undefined
		}],
		page = {
			pageInstanceID: app.env + "_" + app.cfEnv,
			pageInfo: {
				pageId: undefined,
				pageName: undefined,
				pageTitle: document ? document.title : undefined,
				destinationURL: location.href,
				referringURL: document ? document.referrer : undefined,
				host: location.hostname,
				pathname: location.pathname,
				search: location.search,
				hash: location.hash,
				sysEnv: _satellite.getVar('device_type'),
				breadCrumbs: _satellite.getVar('breadcrumbs'),
				publishDate: _satellite.getVar('build_date'),
				publisher: _satellite.getVar('site_name'),

			},
			category: {
				primaryCategory: _satellite.getVar('primary_category'),
				subCategory: _satellite.getVar('secondary_category'),
				pageType: '',
				section: location.pathname.split('/').slice(1, 3).join(':')
			},
			metadata: {
				pageTitle: undefined,
				description: undefined,
				keywords: undefined,
				sections: undefined
			},
			header: undefined,
			footer: undefined
		},
		component = [],
		products = [],
		session = {
			landingPage: _satellite.getVar('landing_page'),
			get minutesOnSite(){return _satellite.getVar('minutes_on_site')},
			sessionCount: _satellite.getVar('session_count'),
			get sessionPageViews(){return _satellite.getVar('session_page_count')},
			trafficSource: _satellite.getVar('traffic_source'),
		},
		transaction = {},
		user = {
			initialised: undefined, // boolean. user authenticated or unauthenticated
			isLoggedIn: undefined, // boolean. login status of user
			returningStatus: _satellite.getVar('new_visitor')===true ? false : true,
			sessionCount: _satellite.getVar('session_count'),
			get lifetimePageViews(){return _satellite.getVar('lifetime_page_views')},
			id: undefined, // example: "00E28000001G5Tg",
			// state": "DONE", // ignore
			firstName: undefined, // example: "John"
			// lastName: "Smith", // ignore. risky for pii capture.
			// email: undefined, // ignore. risky for pii capture.
			// dateOfBirth: "", // ignore. risky for pii capture.
			// password": "", // IGNORE!
			// addresses: {}, // ignore. risky for pii capture.
			identifiers: {
				auth0: undefined,
				octane: undefined,
				broadbandId:undefined,
				mobileId:undefined,
				utilibillId:undefined
			},
			states: {
				emailVerified: undefined,
				migrated: undefined,
				isClassicCustomer: undefined,
				hasClassicBroadband:undefined,
				hasClassicMobile:undefined,
				isMigratedToDigitalStack:undefined,
				isNewStackCustomer:undefined
			},
			services:{},
			_raw: {notice:'_raw objects are EOL',}
		},
		device = {
			get browser() { return _satellite.getVar('browser_info').browser },
			get browserVersion() { return _satellite.getVar('browser_info').version },
			get os() { return _satellite.getVar('browser_info').os },
			get type() { return _satellite.getVar('browser_info').deviceType },
			get orientation() { return window.matchMedia("(orientation: portrait)").matches ? "portrait" : "landscape" },
			cookiesEnabled: window.navigator.cookieEnabled,
			screen: window.screen,
			navigator: window.navigator
		},
		util = {
			_ : _satellite.getVar('lodash'),
			decodeJwt: function(str) {
				var b64DecodeUnicode = function(str) {
					return decodeURIComponent(atob(str).replace(/(.)/g, function(m, p) {
						var code = p.charCodeAt(0).toString(16).toUpperCase();
						if(code.length<2){code='0'+code;}
						return '%' + code;
					}));
				}
				var output = str.replace(/-/g, "+").replace(/_/g, "/");
				switch (output.length % 4) {
					case 0:break;
					case 2:output += "==";break;
					case 3:output += "=";break;
					default:throw "Illegal base64url string!";
				}
				try{return b64DecodeUnicode(output);}
				catch (err) {return atob(output);}
			},
			deleteKeys: function (obj, arrayOfKeys) {
				if (typeof obj !== "object") { return obj }
				arrayOfKeys.forEach(function (key) {
					delete obj[key];
				});
				return obj;
			},
			get: _satellite.getVar('get'),
			merge: function (a, b) {
				for (var key in b) {
					if (b.hasOwnProperty(key)) a[key] = b[key];
				}
				return a;
			},
			isJsonStructure: function (str) {
				if (typeof str !== 'string') return false;
				try {
					var result = JSON.parse(str);
					var type = Object.prototype.toString.call(result);
					return type === '[object Object]' || type === '[object Array]';
				} catch (err) { return false; }
			},
			/**
			 * searches deep into an object recursively...
			 * @param {Object} obj object to be searched
			 * @param {any} searchValue the value/key to search for
			 * @param {Object} [options]
			 * @param {boolean} options.[searchKeys] whether to search object keys as well as values. Defaults to `true` if `serchValue` is a string, `false` otherwise.
			 * @param {number} options.[maxDepth=7] maximum recursion depth (to avoid "Maximum call stack size exceeded")
			 * @returns {string[]} Paths on the object to the matching results
			 */
			findPaths: function (obj, searchValue, _debug) {
				var debug = _debug || false;
				var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
				var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };
				var _ref = arguments.length <= 2 || arguments[2] === undefined ? {} : arguments[2];

				var _ref$searchKeys = _ref.searchKeys;
				var searchKeys = _ref$searchKeys === undefined ? typeof searchValue === "string" : _ref$searchKeys;
				var _ref$maxDepth = _ref.maxDepth;
				var maxDepth = _ref$maxDepth === undefined ? 7 : _ref$maxDepth;

				var paths = [];
				var notObject = (typeof searchValue === "undefined" ? "undefined" : _typeof(searchValue)) !== "object";
				var gvpio = function gvpio(obj, maxDepth, prefix) {
					if (!maxDepth) return;
					if (debug) { console.log('gvpio(', { obj: obj, maxDepth: maxDepth, prefix: prefix }, ')') }
					if (!obj) obj = {}; // cast to object
					var _iteratorNormalCompletion = true, _didIteratorError = false, _iteratorError = undefined;
					try { for (var _step, _iterator = Object.entries(obj)[Symbol.iterator](); !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = !0) { var _step$value = _slicedToArray(_step.value, 2), curr = _step$value[0], currElem = _step$value[1]; searchKeys && curr === searchValue && paths.push(prefix + curr), "object" === (void 0 === currElem ? "undefined" : _typeof(currElem)) && (gvpio(currElem, maxDepth - 1, prefix + curr + "."), notObject) || currElem === searchValue && paths.push(prefix + curr) } } catch (r) { _didIteratorError = !0, _iteratorError = r } finally { try { !_iteratorNormalCompletion && _iterator.return && _iterator.return() } finally { _didIteratorError } }
				};
				gvpio(obj, maxDepth, "");
				return paths;
			},
			flatten: (function (isArray, wrapped) {
				return function (table) { return reduce("", {}, table); }
				function reduce(path, accumulator, table) {
					if (isArray(table)) {
						var length = table.length;

						if (length) {
							var index = 0;

							while (index < length) {
								var property = path + "[" + index + "]", item = table[index++];
								if (wrapped(item) !== item) accumulator[property] = item;
								else reduce(property, accumulator, item);
							}
						} else accumulator[path] = table;
					} else {
						var empty = true;

						if (path) {
							for (var property in table) {
								var item = table[property], property = path + "." + property, empty = false;
								if (wrapped(item) !== item) accumulator[property] = item;
								else reduce(property, accumulator, item);
							}
						} else {
							for (var property in table) {
								var item = table[property], empty = false;
								if (wrapped(item) !== item) accumulator[property] = item;
								else reduce(property, accumulator, item);
							}
						}

						if (empty) accumulator[path] = table;
					}

					return accumulator;
				}
			}(Array.isArray, Object)),
			user: {data: function (_cookieName, _tokenName) {
					var returnVar, cookieJSON, decodedToken,
						cookieName = _cookieName || "belong",
						tokenName = _tokenName || "idToken";
					var cookieData = decodeURIComponent(_satellite.cookie.get(cookieName));
					try{
						if(digitalData.util.isJsonStructure(cookieData)){
							cookieJSON = JSON.parse(decodeURIComponent(_satellite.cookie.get(cookieName)));
							decodedToken = JSON.parse(digitalData.util.decodeJwt(cookieJSON[tokenName].split('.')[1]))
							returnVar = decodedToken;
						}
					}catch(error){
						_satellite.logger.warn(
							'[DE_data_layer] digitalData.util.user.data('+cookieName+','+tokenName+')',
							{error:error,cookieData:cookieData,decodedToken:decodedToken})
					}
					return returnVar;
				},
				details: function (cookieName) {
					var cookieData = decodeURIComponent(_satellite.cookie.get(((typeof cookieName !== 'undefined')?cookieName:"userDetails")))
					if(digitalData.util.isJsonStructure(cookieData)){
						return JSON.parse(cookieData)
					}
				},
				getAuthState: function(){
					var userData = digitalData.util.user.data(),
						userDetails = digitalData.util.user.details();
					if(typeof (digitalData.util._||{}).isEmpty === 'undefined'){return undefined}
					if(digitalData.util._.isEmpty(userData) && digitalData.util._.isEmpty(userDetails)){return undefined} // unknown
					else if(!userData && !userDetails) { return false } // logged out
					else if(userData || userDetails) { return true } // logged in
					else{ return undefined} // unknown
				},
				hashEmail: function(email, authState){
					/*
					Description: Return a sha256 hash of a given email.
								By using the builtin SHA-256 hash algorithm provided by the Experience Cloud ID extension,
								we avoid having to include and manage our own crypto library.
					Docs: https://docs.adobe.com/content/help/en/id-service/using/reference/hashing-support.html
					Param: email:string, authState: boolean. Undefined for unknown/never. casts to 0, true logged in. casts to 1, false for logged out. casts to 2,
					*/
					if(!email || !authState){return undefined}
					var hashType = "SHA-256",
						authDigit = (authState === 'undefined') ? 0 : ((authState) ? 1 : 2),
						visitorIdInstance =_satellite.getVisitorId();
					visitorIdInstance.setCustomerIDs({email: {id: email, authState: authDigit}}, hashType);
					var customerId = visitorIdInstance.getCustomerIDs();
					var hashedEmail = (customerId.email||{}).id
					_satellite.logger.log('[DE_data_layer] hashEmail()',{
						email:email, hashedEmail:hashedEmail, authDigit:authDigit, authState:authState, customerId:customerId});
					return hashedEmail;
				}
			}
		},
		debug = {
			nextData: nextData,
			destinationURL: _satellite.getVar('url'),
			stores: {},
		},
		stores = {},
		updateDataLayer = function (_eventName, event) {
			/* In update data layer we massage the values from the event{}/event.detail{} and map to the datalayer.	*/

			/* Uncomment the following line if you are debugging issues with the app sending events
			   before Adobe Launch has finished loading. Early events are sent to the queue.
			*/
			// _satellite.logger.log('[updateDataLayer]', _satellite.queue);
			if (typeof _eventName !== "string") {
				_satellite.logger.error('[DE_data_layer] > updateDataLayer() _eventName parameter expects a string', { _eventName: _eventName, })
				return;
			}

			var eventName = _eventName,
				stores = _get('detail.store', event) || this.stores || {},
				stepDetail,
				apiDetail,
				omitKeys = ['addresses', 'dateOfBirth', 'lastName', 'password'],
				evt = {
					_event: event,
					sequence: Object.keys(digitalData.eventSeq).length + 1,
					element: event.element || undefined,
					detail: event.detail,
					currentPage: event.detail.currentPage || (event.detail.pageMeta || {}).pageUrl,
					action: event.action || (event.detail || {}).action,
					moment: event.moment || (event.detail || {}).moment,
					services: (((event.detail || {}).value||{}).services||[]).reverse(), // Service listing from API arrives in backwards order.
					err: event.err || (event.detail || {}).err,
					url: event.url || (event.detail || {}).url,
					operation: event.operation,
					endpoint: event.endpoint || (event.detail || {}).endpoint
				};
			// Clean stuff we don't want in the datalayer
			if (Object.keys(stores).length > 0) {
				stores.user = digitalData.util.deleteKeys(stores.user, omitKeys)
				this.stores = stores;
			}
			switch (eventName) {
				case "trackAPI":
					//_satellite.logger.log('[DE_data_layer] updateDataLayer('+eventName+')',{stores:stores,evt:evt});
					/* There isn't any easy way to identify if an error event contains an error.
					   So we run this every time, and all the logic to find errors are in the fuction.
					*/
					// When an AFTER API is triggered
					if (evt.moment === "AFTER") {
						this.updateErrorInfo(evt)
					}
					// When user logs in.
					if (evt.endpoint === "/auth/v1/login" && evt.moment === "AFTER") {
						apiDetail = { login: true, action: "login", endpoint: evt.endpoint, moment: evt.moment }
					}
					// When a PRODUCT CATALOGUE RESPONSE is received -- LIKELY DEPRECATED MARCH 2021
					else if (evt.endpoint === "/product/v1/products/mobile" && evt.moment === "AFTER") {
						digitalData.products = evt.detail.value
					}
					// When a ORDER ACTIVATION is received
					else if (evt.endpoint === "/orchestrator/v1/orders/activate" && evt.moment === "AFTER") {
						this.updateTransaction(stores.orders)
					}
					// When a SERVICE TILE IS LOADED is received
					else if (evt.endpoint === "/orchestrator/v1/services" && evt.moment === "AFTER") {
						apiDetail = {
							eventName: 'contentDisplayed',
							contentType: 'my services | main tile',
							content: ('service '+_get('0.status', evt.services)).toLowerCase(),
							activeServices: digitalData.util._.filter(evt.services, { 'status': 'ACTIVE' }).length,
							totalServices: evt.services.length,
							type: _get('0.type', evt.services),
							serviceId: _get('0.id', evt.services),
							services: evt.services
						}
						_satellite.logger.info('[DE:data_layer] updateDataLayer(' + eventName + ')', { services: evt.services });
						this.updateUserInfo({services: evt.services})
					}
					apiDetail && this.updateEventInfo(eventName, apiDetail)
					break;
				case "trackClick":
				case "trackFormInput":
					_satellite.logger.info('[DE:data_layer] updateDataLayer(' + eventName + ')', { evt: evt, stores: stores });
					if (evt.action === "click" || evt.action === "focus") {
						this.updateIXComponent(event);
					}
					break;
				case "trackNavigation":
					// When the user navigates - limited to PORTAL/Entice pages.
					this.stores = this.stores || {}
					this.stores.contentful = evt.detail.cache || evt.detail.contenful || evt.detail.pageMeta;

					if (this.stores.contentful) {
						this.updateUserInfo({})
						this.updatePageInfo(evt.detail)
						this.updateMetadata(evt.detail)
						this.updateCategory(evt.detail)
						this.updateComponents(evt.detail)
						this.updateProductInfo(evt.detail)
						this.updateEventInfo(eventName, evt.detail)
					} else {
						_satellite.logger.warn('[DE:data_layer] > trackNavigation', 'CACHE MISS', evt)
					}
					break;
				case "trackState":
					// When a page first loads, and Launch ran first
					if (evt.action == "onCreate") {
						cacheObj = _get('detail.store.contentful.cache', evt)
						stores.user && this.updateUserInfo(stores.user);
						if (stores.contentful) {
							this.updatePageInfo(stores.contentful)
							this.updateMetadata(stores.contentful)
							this.updateCategory(stores.contentful)
							this.updateComponents(stores.contentful)
						}
					}
					// When a page first loads, and Launch ran late
					else if (evt.action == "pageLoad") {
						stores.user && this.updateUserInfo(stores.user);
						if (stores.contentful) {
							this.updatePageInfo(stores.contentful)
							this.updateMetadata(stores.contentful)
							this.updateCategory(stores.contentful)
							this.updateComponents(stores.contentful)
						}
					}
					this.updateEventInfo(eventName)
					break;
				case "trackStep":
					// When the user moves to the next part of a multistep form.
					var orderId = digitalData.util.get('detail.state.shared.orderId', evt);
					var stepDetail = {
						currentStep: (evt.detail.state || {}).currentStep,
						isFirstStep: (evt.detail.state || {}).isFirstStep,
						isLastStep: (evt.detail.state || {}).isLastStep,
						contentType: (evt.detail.content || {}).contentType,
						shared: (evt.detail.state || {}).shared,
						pageName: _get('page.pageInfo.pageName', digitalData)
					},
						craftedOrderStore = {
							currentOrder: orderId,
							cache: { orderId: { orderId: orderId, shared: (evt.detail.state || {}).shared } }
						};
					stepDetail.pageName += stepDetail.isFirstStep ? " Start" : (stepDetail.isLastStep ? " Complete" : "");
					digitalData.util.merge(digitalData.page.pageInfo, stepDetail)
					orderId && this.updateTransaction(craftedOrderStore) // If there is a currentOrder, update transaction{}
					this.updateEventInfo(eventName, stepDetail)
					break;
				case "trackStore":
					_satellite.logger.warn('[DE:data_layer] updateDataLayer > ',eventName, {stores:stores, evt:evt} )
					stores.user && this.updateUserInfo(stores.user);
					if (stores.contentful) {
						this.updatePageInfo(stores.contentful)
						this.updateMetadata(stores.contentful)
						this.updateCategory(stores.contentful)
						this.updateComponents(stores.contentful)
						this.updateEventInfo(eventName, evt.detail)
					}
					break;
				default:
					_satellite.logger.warn('[DE:data_layer] updateDataLayer(' + eventName + ') unrecognised tracking event\n', event);
			}
			this.updateAppInfo();

			// Write into the event sequence array to aid development/debugging..
			digitalData.eventSeq[evt.sequence + ': ' + eventName] = evt; // This is easier to read/debug
			digitalData.pageLoadTracked = (typeof window.s) ? true : false; // Signal that we've tracked the page, so auto PLR doesn't
		},
		/* === digitalData.app{} === */
		updateAppInfo = function () {
			digitalData.util.merge(digitalData.app, {
				env: _get('runtimeConfig.env.isProduction', nextData) ? "prod" : "dev",
				buildId: _get('buildId', nextData),
				version: _get('runtimeConfig.version', nextData),
				cfEnv: _get('runtimeConfig.contentful.environment', nextData),
				cfSpace: _get('runtimeConfig.contentful.space', nextData),
				workspace: _get('runtimeConfig.workspace', nextData)
			})
		},
		/* === digitalData.page.category{} === */
		updateCategory = function (contentfulStore) {
			var currentPage = contentfulStore.currentPage;
			var cacheCurrentPage = contentfulStore.pageMeta || (contentfulStore || {}).cache[currentPage] || contentfulStore.cache || {};
			//_satellite.logger.log('[DE:data_layer] > updateCategory()', {contentfulStore:contentfulStore, currentPage:currentPage, cacheCurrentPage:cacheCurrentPage});
			digitalData.util.merge(digitalData.page.category, {
				pageType: _get('page.contentType', cacheCurrentPage),
				section: ((currentPage + "".charAt(0) == "/") ? currentPage : location.pathname).split('/').slice(1, 3).join(':')
			})
		},
		/* === digitalData.component[] === */
		updateComponents = function (contentfulStore) {
			var component = [],
				cacheCurrentPage = contentfulStore.pageMeta || contentfulStore.cache[contentfulStore.currentPage] || contentfulStore.cache || {};

			if (!cacheCurrentPage.page) {
				_satellite.logger.log('[DE:data_layer] updateComponents() contentfulStore missing page.sections{}', { contentfulStore: contentfulStore, cacheCurrentPage: cacheCurrentPage });
				return;
			}
			var sections = _get('page.sections', cacheCurrentPage) || _get('page.fields.sections', cacheCurrentPage)
			if (Array.isArray(sections)) {
				sections.forEach(function (itm) {
					var obj = {
						componentInfo: {
							componentId: itm.id || _get('sys.id', itm),
							componentName: undefined,
							description: _get('fields.identifier', itm)
						},
						category: {
							primaryCategory: undefined,
							subCategory: undefined,
							componentType: itm.contentType || _get('sys.contentType.sys.id', itm)
						},
						raw: itm
					}
					component.push(obj);
				});

				Array.prototype.push.apply(digitalData.component, component);
			}
		},
		/* === digitalData.errorInfo{} === */
		updateErrorInfo = function (evt) {
			var error = {
				code: undefined,
				description: evt.endpoint,
				sequence: evt.sequence,
				value: digitalData.util.flatten(evt.detail.value),
				url: _satellite.getVar('url')
			}
			error.detail = JSON.stringify(error.value)
			for (var id in error.value) {
				if (error.code) { break; }
				error.code = /error|failure|failed/i.test(error.value[id]) ? error.value[id] : undefined;
				digitalData.error.push(error)
			}
		},
		/* === digitalData.event[] === */
		updateEventInfo = function (eventName, evtDetail) {
			var eventInfo = {
				event: "analytics-event",
				eventName: eventName,
				pageInfo: this.page.pageInfo,
			};
			// Push our massaged event into the event array
			switch (eventName) {
				case "trackNavigation":
					digitalData.util.merge(eventInfo, {
						event: eventName,
						metaData: this.page.metadata,
						storeComponent: this.component
					})
					break;
				case "trackStep":
					digitalData.util.merge(eventInfo, {
						event: eventName,
						metaData: this.page.metadata,
						storeComponent: this.component,
						transaction: this.transaction
					})
					break;
				case "trackStore":
					digitalData.util.merge(eventInfo, {
						event: eventName,
						metaData: this.page.metadata,
						storeComponent: this.component
					})
					break;
				case "trackAPI":
					digitalData.util.merge(eventInfo, evtDetail)
					eventInfo.event = eventName;
					break;
				default:
					digitalData.util.merge(eventInfo, evtDetail)
					eventInfo.event = eventName;
					break;
			}
			this.event.push(eventInfo); // This mirrors the CEDDL format.
			(typeof window.dataLayer.push === "function" && window.dataLayer.push({'event':eventName}))
			_satellite.logger.log('[DE_data_layer] > ≈('+eventName+')',{args:{eventName:eventName, evtDetail:evtDetail},eventInfo:eventInfo});
		},
		/* === digitalData.page.metadata{}} === */
		updateMetadata = function (contentfulStore) {
			var cacheCurrentPage = contentfulStore.pageMeta || (contentfulStore || {}).cache[contentfulStore.currentPage] || contentfulStore.cache || {},
				getMetaDescription = function (descr) {
					var el, returnVal;
					if (typeof descr === 'string') {
						returnVal = descr;
					} else {
						el = document.querySelector('meta[name=description]');
						if (el !== null) {
							returnVal = el.getAttribute('content');
						}
					}
					return returnVal;
				};
			//_satellite.logger.log('[DE:data_layer] > updateMetadata()', {contentfulStore:contentfulStore, currentPage:contentfulStore.currentPage, cacheCurrentPage:cacheCurrentPage});
			digitalData.util.merge(digitalData.page.metadata, {
				description: getMetaDescription(cacheCurrentPage.description),
				pageTitle: cacheCurrentPage.pageTitle,
				keywords: cacheCurrentPage.keywords,
				sections: _get('page.sections', contentfulStore) || _get('sections', contentfulStore) || _get('cache.page.fields.sections', contentfulStore)
			})
		},
		/* === digitalData.page.pageInfo{} === */
		//#region [ green ]
		updatePageInfo = function (contentfulStore, _currentPage) {
			// Update Page Details in digitalData.page.pageInfo{}
			var currentPage = _currentPage || contentfulStore.currentPage || (contentfulStore.pageMeta || {}).pageUrl;
			var cacheCurrentPage = contentfulStore.pageMeta || (contentfulStore || {}).cache[currentPage] || contentfulStore.cache || {};

			var previousURL = function(){
				/* React doesn't update the document.referrer,
				so track it manually from cookies and/or the contentful cache. */
				var currentPageURL = location.protocol+"//"+location.hostname + currentPage,
					cookieVal = atob((_satellite.cookie.get('s_pu')||"")),
					returnVal;

				if(cookieVal.length > 20 && cookieVal !== currentPageURL){
					returnVal = cookieVal
				}else{
					returnVal = document.referrer
				}
				_satellite.cookie.set('s_pu',btoa(currentPageURL))
				return returnVal;
			};

			_satellite.logger.log('[DE:data_layer] > updatePageInfo()', { contentfulStore: contentfulStore, currentPage: currentPage, cacheCurrentPage: cacheCurrentPage, /*previousURL:previousURL()*/ });

			var pageInfo = {
				pageId: _get('page.identifier', cacheCurrentPage) || _get('sys.id', cacheCurrentPage) || _get('pageUrl.sys.id', cacheCurrentPage),
				pageName: cacheCurrentPage.pageName,
				pageTitle: cacheCurrentPage.pageTitle,
				pathname: currentPage || _satellite.getVar('pathname'),
				destinationURL: location.protocol+"//"+location.hostname + currentPage,
				referringURL: previousURL(),
				// Reset step data:
				currentStep: undefined, isFirstStep: undefined, isLastStep: undefined, contentType: undefined, shared: undefined
			}

			digitalData.util.merge(digitalData.page.pageInfo, pageInfo)
			digitalData.util.merge(digitalData.page.category, {
				journeyName: cacheCurrentPage.journeyName,
				flowName: cacheCurrentPage.flowName
			})
			digitalData.page.header = contentfulStore.globalHeader;
			digitalData.page.footer = contentfulStore.globalFooter;

			_satellite.logger.log('[DE:data_layer] > merge pageInfo', { 'pageInfo': pageInfo, 'dlPageInfo': digitalData.page.pageInfo });
		},
		//#endregion
		/* === digitalData.updateProductInfo{} === */
		updateProductInfo = function (evt) {
			var currentPage = evt.currentPage || (evt.pageMeta || {}).pageUrl,
				cacheCurrentPage,
				pathToPlanSummary,
				plansArr,
				product = [],
				_get = digitalData.util.get,
				_findPaths = digitalData.util.findPaths;

			// Navigation Events
			if (evt.action == "pageLoad" || evt.action == "replace currentPage") {
				// Cache data may appear in the following locations :(
				cacheCurrentPage = evt.pageMeta || evt.cache[currentPage] || evt.cache ||  {}
				pathToPlanSummary = _findPaths(cacheCurrentPage, 'sectionPlanSummary')[0]

				if (evt.products) {
					plansArr = evt.products;
				} else {
					if (pathToPlanSummary == false || pathToPlanSummary == null) { return } /* exit early if plans component not present */
					plansArr = _get(pathToPlanSummary.replace('contentType', 'plans'), cacheCurrentPage)
				}

				if (Array.isArray(plansArr)) {
					plansArr.forEach(function (itm) {
						itm.notice = '_raw objects are EOL';
						var obj = {
							productCode: itm.productCode || _get('product.productCode', itm),
							productName: itm.productName || _get('type', itm),
							productDescription: itm.productDescription || _get('featuresLabel', itm),
							dataValue: _get('data.value', itm) || _get('product.size.dataValue', itm),
							type: itm.productType || itm.type,
							priceValue: _get('price.value', itm) || _get('product.price.value', itm),
							price: {
								frequency: _get('price.recurringFrequency', itm) || _get('product.price.recurringFrequency', itm),
								chargeType: _get('price.chargeType', itm) || _get('product.price.chargeType', itm),
							},
							addons: { // Addons data not available.
								productName: _get('addons.0.productName', itm),
								productCode: _get('addons.0.productCode', itm),
								productDescription: _get('addons.0.productDescription', itm),
								price: {
									frequency: _get('addons.0.price.recurringFrequency', itm),
									chargeType: _get('addons.0.price.chargeType', itm)
								}
							},
							_raw: itm
						};
						product.push(obj);
					});
					// Use lodash unionBy function to prevent duplicate products going into the datalayer
					digitalData.products = digitalData.util._.unionBy(product, digitalData.products, "productCode");
				}
				_satellite.logger.log('[DE:data_layer] updateProductInfo()', {
					currentPage: digitalData.debug.cacheCurrentPage = currentPage, pathToPlanSummary: pathToPlanSummary, plansArr: plansArr, evt: evt
				});
			}
		},
		updateTransaction = function (orderStore) {
			var orderId = orderStore.currentOrder || orderStore.orderId;
			var currentOrder = orderStore.cache[orderId] || {}; // cleanup
			var payableAmount = currentOrder.payableAmount || {};
			var transactionObj = {
				orderId: orderId,
				total: {
					basePrice: (currentOrder.totalPrice || {}).value,
					voucherCode: "unavailable in api v1",
					voucherDiscount: (currentOrder.creditAppliedAmout || {}).value,
					currency: "AUD",
					taxRate: 0.1,
					priceWithTax: payableAmount.value,
					transactionTotal: payableAmount.value,
					chargeType: payableAmount.chargeType
				},
				products: [{ //Object array of products included in the transaction
					product: {
						addons: currentOrder.addons,
						dataValue: 'unavailable in api v1',
						price: currentOrder.price,
						priceValue: 10,
						productCode: currentOrder.productCode,
						productDescription: currentOrder.productDescription,
						productName: currentOrder.productName,
						serviceType: currentOrder.serviceType,
					}
				}],
				_raw: {notice:'_raw objects are EOL',currentOrder:currentOrder}
			};
			_satellite.logger.log('[DE_data_layer] updateTransaction()', { orderStore: orderStore, transaction: transactionObj });
			digitalData.transaction = transactionObj;
		},
		updateIXComponent = function (event) {
			var eventInfo = {
				event: "analytics-event",
				eventName: event.action,
				userInteraction: event.action,
				linkTarget: event.element.href,
				textContent: event.element.innerText,
				radioValue: event.element.type === 'radio' ? event.element.value : undefined,
				checkboxValue: event.element.type === 'checkbox' ? event.element.value : undefined,
			},
				containers = event.detail.containers || {},
				text = event.detail.text,
				getStoreComponent = function (contentType) {
					var _component = digitalData.component
					var path = digitalData.util.findPaths(_component, contentType)[0];
					//_satellite.logger.log('[getComponent('+contentType+')] path =',path);
					var top = Array.isArray(path) ? path.split('.').shift() : "0";
					//_satellite.logger.log('[getComponent('+contentType+')] top =',top);
					var obj = _get(top, _component);
					//_satellite.logger.log('[getComponent('+contentType+')] obj =',obj);
					return obj;
				},
				getProductData = function (productCode) {
					var _products = digitalData.products.mobilePlans || digitalData.products
					var path = String(digitalData.util.findPaths(_products, productCode)[0]);
					var productObj = digitalData.util.get(path.split('.')[0], _products)
					_satellite.logger.log('[DE_data_layer] updateIXComponent() > getProductData()', { productCode: productCode, _products: _products, path: path, productObj: productObj });
					return productObj;
				};

			// Things we do that are common to all clicks
			// "{{ /mobile/plans }}",
			eventInfo.section = undefined // "{{ hero-banner-refresh }}",
			eventInfo.sectionTitle = undefined // "{{ Hero Banner }}",
			eventInfo.imageSrc = undefined // "{{ /static/4c86567ed7c76267ea3571771af46091.png }}",
			eventInfo.altText = undefined // "{{ 10GB of mobile data for $25/mth }}",

			if (containers.article !== null) {
				// click was inside a article component (as seen in the bigboi).
				eventInfo.componentRole = "accountSetup"
				eventInfo.componentName = containers.article.querySelector('h2, h3, a').innerText
				eventInfo.componentId = containers.article.id || containers.article.className
				eventInfo.description = containers.article.querySelector('p').innerText
				eventInfo.storeComponent = getStoreComponent('sectionAccountSetup')
			}
			if (containers.grid !== null) {
				// click was inside a grid component.
				eventInfo.componentRole = "grid"
				eventInfo.componentName = containers.grid.querySelector('h1, h2, h3, h4, strong, a, button').innerText
				eventInfo.componentId = containers.grid.id || containers.grid.className
				eventInfo.description = containers.grid.innertText
				eventInfo.storeComponent = getStoreComponent('sectionFeatureTiles')
			}
			if (containers.header !== null) {
				// click was inside a banner/header component.
				eventInfo.componentType = "header"
				eventInfo.storeComponent = getStoreComponent('sectionPrimaryBanner')
				eventInfo.componentName = ((eventInfo.storeComponent || {}).category || {}).componentType
				eventInfo.componentId = ((eventInfo.storeComponent || {}).componentInfo || {}).componentId
				eventInfo.description = text
			}
			if (containers.nav !== null) {
				// click was in the global navigation.
				eventInfo.componentName = "Global Nav"
				eventInfo.componentId = containers.nav.id || containers.nav.className
				eventInfo.description = /Desktop/i.test(containers.nav.className) ? "Desktop Nav" : "Mobile Nav"
				eventInfo.componentRole = "nav"
				eventInfo.storeComponent = (digitalData.page || {}).header;
			}
			if (containers.planSelector !== null) {
				// click was on a plan selector button in account setup.
				eventInfo.productCode = event.element.getAttribute('value')
				eventInfo.componentType = "product"
				eventInfo.storeComponent = getStoreComponent('sectionPlanSelector')
				eventInfo.componentName = ((eventInfo.storeComponent || {}).category || {}).componentType
				eventInfo.componentId = ((eventInfo.storeComponent || {}).componentInfo || {}).componentId
				eventInfo.description = containers.planSelector.querySelector('h3,h4,div,span').innerText
				eventInfo.componentRole = "radio"
				eventInfo.sectionTitle = containers.section.querySelector('h1').innerText
				eventInfo.product = getProductData(eventInfo.productCode)
				_satellite.logger.log('[DE_data_layer] updateIXComponent() planSelector', { event: event, eventInfo: eventInfo })
			}
			if (containers.productCTA !== null) {
				// click was on a plan selector button.
				eventInfo.productCode = containers.productCTA.getAttribute('data-product-code')
				eventInfo.componentType = "product"
				eventInfo.storeComponent = getStoreComponent('sectionPlanSummary')
				eventInfo.componentName = ((eventInfo.storeComponent || {}).category || {}).componentType
				eventInfo.componentId = ((eventInfo.storeComponent || {}).componentInfo || {}).componentId
				eventInfo.description = text
				eventInfo.componentRole = "button"
				eventInfo.product = getProductData(eventInfo.productCode)
			}

			componentObj = { componentInfo: ((eventInfo.storeComponent || {}).componentInfo || {}), category: ((eventInfo.storeComponent || {}).category || {}), raw: event, containers: containers }
			this.component.push(componentObj);
			// Update the event array
			// TODO: Refactor so this uses this.updateEventInfo(evt.detail)
			this.event.push(eventInfo);
			(typeof window.dataLayer.push === "function" && window.dataLayer.push({'event':eventInfo.eventName}))
		},
		/* === digitalData.userInfo{} === */
		updateUserInfo = function (_userStore) {
			var userStore = _userStore || {},
				userDetails= digitalData.util.user.details('userDetails')||{}, // Read the userDetails cookie if available.
				userData = digitalData.util.user.data('belong','idToken')||{}, // belong.idToken cookie data.
				authState = digitalData.util.user.getAuthState(); // true = logged in, false = logged out, undefined = unknown
			var email = _get('email', userStore) || _get('email', userData) || _get('email', userDetails);

			digitalData.util.merge(digitalData.user, {
				initialised: authState,
				isLoggedIn: authState,
				hashedEmail: digitalData.util.user.hashEmail(email, authState),
				id: userStore.accountId || _get('stores.services.currentService',digitalData) || userDetails.userId,
				firstName: userStore.firstName || userData.given_name || userDetails.firstName
			});
			digitalData.util.merge(digitalData.user.identifiers, {
				auth0: userData.sub,
				broadbandId: _get('https://user_metadata.fixedOctaneId',userData),
				mobileId: userDetails.mobileOctaneId,
				octane: _get('https://user_metadata.fixedOctaneId',userData),
				utilibillId: _get('https://user_metadata.fixedOctaneId',userData) || userDetails.utilibillId
			})
			digitalData.util.merge(digitalData.user.states, {
				emailVerified: userData.email_verified,
				hasClassicBroadband: userDetails.hasClassicBroadband,
				hasClassicMobile: userDetails.hasClassicMobile,
				isClassicCustomer:  _get('https://user_metadata.isClassicCustomer',userData) || userDetails.isClassicCustomer,
				isMigratedToDigitalStack: userDetails.isMigratedToDigitalStack,
				isNewStackCustomer: userDetails.isNewStackCustomer,
				migrated:  _get('https://user_metadata.migrated',userData)
			})
			_satellite.logger.log('[DE_data_layer] updateUserInfo()', { userStore: userStore, _userStore:_userStore})
			if(userStore.services){
				try{
					for (var idx = 0; idx < userStore.services.length; idx++) {
						digitalData.user.services[userStore.services[idx].id] = userStore.services[idx];
					}
				}catch(e){/* eh */}
			}else if((digitalData.stores||{}).services){
				digitalData.user.services =  _get('stores.services.cache',digitalData);
				digitalData.user.services.currentService = _get('stores.services.currentService',digitalData)
			}
			digitalData.util.merge(digitalData.user._raw,{authState:authState, userStore:userStore, userDetails:userDetails, userData:userData});
		};
	return {
		// event objects
		ts: ts, event: event, eventSeq: eventSeq, pageInstanceID: pageInstanceID, pageLoadTracked: pageLoadTracked,
		// data layer objects
		app: app, error: error, page: page, component: component, products: products, session: session, transaction: transaction, user: user, device: device,
		// tracking functions
		track: track, trackRouter: trackRouter, trackState: trackState,
		/*trackStore: trackStore, trackNavigation:trackNavigation, trackAPI: trackAPI, */
		updateDataLayer: updateDataLayer,
		updateAppInfo: updateAppInfo, updateCategory: updateCategory, updateComponents: updateComponents, updateErrorInfo: updateErrorInfo,
		updateEventInfo: updateEventInfo, updateIXComponent: updateIXComponent, updateMetadata: updateMetadata, updatePageInfo: updatePageInfo,
		updateProductInfo: updateProductInfo, updateTransaction: updateTransaction, updateUserInfo: updateUserInfo,
		// utilities
		util: util, debug: debug, version: version
	};
});
return initDataLayer();
}
