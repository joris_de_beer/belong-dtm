/*
Name: url
Description: Return the url available in the data layer, falling back to the window.location.href.
             Data layer is preferered because some events will trigger momentarily before the browser is updated.
Extension: Core
Type: Custom Code
*/

var dataLayerUrl = (((window.digitalData||{}).page||{}).pageInfo||{}).destinationURL,
	browserUrl = window.location.href;

// URLs provided by contentful stores may be missing protocol. Add if needed.
dataLayerUrl = /^(file|http)/.test(dataLayerUrl) ? dataLayerUrl : dataLayerUrl && window.location.protocol+"//"+dataLayerUrl

return  (dataLayerUrl || browserUrl)