/*
Name: error_description
Description: The most recent error description in the digitalData.error[] array.
Extension: Core
Type: Javascript Variable
*/
var lastError = digitalData.error[digitalData.error.length-1];
return lastError.description;