/*
Name: salesforce_id
Description: Returns the SFID from the userDatails cookie and return it
Extension: Core
Type: Custom Code
*/
var userDatailsCookie = _satellite.cookie.get('userDetails')||"{}";
var userDetailsObj = JSON.parse(decodeURIComponent(userDatailsCookie));
return (typeof userDetailsObj.userId === "string") ? userDetailsObj.userId : undefined;
