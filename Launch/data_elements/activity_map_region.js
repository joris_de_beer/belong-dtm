/*
Name: activity_map_region
Description: Returns a user-friendly text label for the region surrounding the link or button the user clicked.
Extension: Core
Type: Custom Code
*/
_satellite.logger.log('[DE_activity_map_region]\nevent: ',window.evt_=event, '\nthis: ', window._this=this);

var _get = digitalData.util.get
event._this = this
event.action = _get('nativeEvent.type', event)

event.detail = {
	el: event.element,
	action: _get('nativeEvent.type', event),
	text: _get('element.text', event) || _get('element.innerText', event),
	classList: _get('element.classList', event),
	elementPath: _get('nativeEvent.path', event),
	containers: {
		// Orders from tightest to widest
		tile: event.element.closest('div[class^="Tilestyles__TileWrapper"]'),
		grid: event.element.closest('div[class^="Grid-"]'),
		container: event.element.closest('div[class^="Container"]'),
		section: event.element.closest('section[class^="Sectionstyles"]'),
		pageTitle: event.element.closest('div[class^="PageTitlestyles"]'),
		header: event.element.closest('header'),
		nav: event.element.closest('nav'),
		footer: event.element.closest('div[class^="GlobalFooterstyles"]')
	}
}
digitalData.updateDataLayer('trackClick', event)
return true;

return;