/*
Name: page_name
Description: The name of the page in the Adobe Analytics Style.
Extension: Core
Type: Custom Code
*/
var division = _satellite.getVar('division'),
	dataLayer = window.digitalData || _satellite.getVar('data_layer');
	referringUrl = dataLayer.page.pageInfo.referringURL,
	dlPathName = (((window.digitalData||{}).page||{}).pageInfo||{}).pathname || location.pathname;

	// Fallback to a tidy pathname if page name is missing
if(!dlPageName){
	dlPageName = dlPathName.split('/').filter(Boolean).join(':')
}

return division + ":" + dlPageName;

/*
Name: referrer
Description: Return the url of the previously loaded page. Will attempt to read it from datalayer first.
  Then check document.referrer
Extension: Core
Type: Custom Code
*/
var dataLayer = window.digitalData || _satellite.getVar('data_layer');
var referringURL = dataLayer.page.pageInfo.referringURL || document.referrer;

// Clean the URL of PII data
if(/email|username|password|/i.test(referringURL)){
	return referringURL.replace(/(email|username|password)=[^&]*(&?)/g,"$1=*******$2");
}else{
	return referringUrl;
}
