/*
Name: referrer
Description: Return the url of the previously loaded page. Will attempt to read it from datalayer first.
  Then check document.referrer
Extension: Core
Type: Custom Code
*/
var dataLayer = window.digitalData || _satellite.getVar('data_layer');
var referringURL = dataLayer.page.pageInfo.referringURL || document.referrer;

// Clean the URL of PII data
if(/email|username|password|/i.test(referringURL)){
	return referringURL.replace(/(email|username|password)=[^&]*(&?)/g,"$1=*******$2");
}else{
	return referringUrl;
}
