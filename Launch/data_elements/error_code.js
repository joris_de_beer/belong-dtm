/*
Name: error_code
Description: The most recent error code in the digitalData.error[] array.
Extension: Core
Type: Javascript Variable
*/
var lastError = digitalData.error[digitalData.error.length-1];
return lastError.code;