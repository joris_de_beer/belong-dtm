/*
Name: workspace
Description: Return the Workspace label of the React Application hosting the page.
  First check the data layer
  Then check the NextData data element
Extension: Core
Type: Custom Code
*/
var nextData = _satellite.getVar('next_data'),
    dataLayer = _satellite.getVar('data_layer');
var nextDataWorkspace = (nextData.runtimeConfig||{}).workspace,
    dlWorkspace = (dataLayer.app||{}).workspace;

return dlWorkspace ? dlWorkspace : nextDataWorkspace;
