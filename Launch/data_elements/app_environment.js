/* 
Name: app_environment
Description: Information about the environment of the React Application hosting the page.
Extension: Core
Type: Javascript Variable
var: digitalData.appInfo.env
*/