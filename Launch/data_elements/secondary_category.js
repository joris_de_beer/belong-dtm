/*
Name: secondary_category
Description: Returns the secondary category of the current pathname, per BT-20844
Extension: Mapping Table
Type: Mapping Table
*/