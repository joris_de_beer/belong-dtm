/*
Name: auth0_id
Description: Returns the AUTH0 ID from the userDetails cookie (if user is logged into Classic), or from belong cookie if logged into New Stack and return it
Extension: Core
Type: Custom Code
*/
var returnVal,
	userData, // Fallback for when classic userDetails cookie is unavailable.
	userDetailsCookie = _satellite.cookie.get('userDetails')||"{}";
var userDetailsObj = JSON.parse(decodeURIComponent(userDetailsCookie));
if(typeof userDetailsObj.userId === "string" && userDetailsObj.userId.length === 24){
	returnVal = userDetailsObj.userId;
}else if(typeof (((window.digitalData||{}).util||{}).user||{}).data ==='function'){
	userData = digitalData.util.user.data('belong','idToken')||{}
	returnVal = (typeof userData.sub  === "string") ? userData.sub.replace('auth0|','') : undefined
}
_satellite.logger.log('[DE_auth0_id]',{'userData':userData,'userDetailsObj':userDetailsObj,'returnVal':returnVal});
return returnVal
