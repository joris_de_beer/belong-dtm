/* 
Name: launch_build_date
Description: Timestamp of when Adobe Launch was published
Extension: Core
Type: Javascript Variable
var: _satellite.buildInfo.turbineBuildDate
*/