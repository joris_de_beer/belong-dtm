/*
Name: hashed_email
Description: Return a sha256 hash of a given email.
			 By using the builtin SHA-256 hash algorithm provided by the Experience Cloud ID extension,
			 we avoid having to include and manage our own crypto library.
Docs: https://docs.adobe.com/content/help/en/id-service/using/reference/hashing-support.html
Extension: Core
Type: Custom Code
*/
var email = ((event||{}).detail||{}).email,
	hashType = "SHA-256",
	authState = ((digitalData||{}).user||{}).isLoggedIn ? 1 : 2,
	visitorIdInstance =_satellite.getVisitorId();
	visitorIdInstance.setCustomerIDs({email: {id: email, authState: 1}}, hashType);
return visitorIdInstance.getCustomerIDs().email||{}.id;