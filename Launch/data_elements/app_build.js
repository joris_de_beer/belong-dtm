/* 
Name: app_build
Description: Build ID of the React Application hosting the page.
Extension: Core
Type: Javascript Variable
var: digitalData.appInfo.buildId
*/