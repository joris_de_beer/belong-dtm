/*
Name: page_name
Description: The name of the page in the Adobe Analytics Style.
Extension: Core
Type: Custom Code
*/

var _get = ((window.digitalData||{}).util||{}).get || _satellite.getVar('data_layer').util.get,
	nextData = _satellite.getVar('next_data');

var division = _satellite.getVar('division'),
	dlPageName = _get('page.pageInfo.pageName',window.digitalData) || _get('props.pageProps.pageData.pageName',nextData),
	dlPageTitle= _get('page.pageInfo.pageTitle',window.digitalData) || _get('props.pageProps.pageData.pageTitle',nextData),
	dlPathName = _get('page.pageInfo.pathname',window.digitalData) || location.pathname;
/*
_satellite.logger.log('[DE_page_name]',
	JSON.stringify((((window.digitalData||{}).page||{}).pageInfo||{}), null, "\t"),{
		'dlPageName':dlPageName,
		'nextData':JSON.parse(JSON.stringify(nextData))
	}
);
*/
// Fallback to a tidy pathname if page name is missing
if(!dlPageName){
	dlPageName = dlPathName.split('/').filter(Boolean).join(':')
}

if(dlPageTitle.indexOf('404 Error') > -1){
	dlPageName = "404"
}

// Clean up. Put any logic for cleaning up page names here:
// 1. Remove " - Public" from end of pagenames.
dlPageName = dlPageName.replace(' - Public','')

return division + ":" + dlPageName;

setTimeout(() => {

}, timeout);
