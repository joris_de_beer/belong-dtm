/*
Name: primary_category
Description: Returns the primary category of the current pathname, per BT-20844
Extension: Mapping Table
Type: Mapping Table
*/