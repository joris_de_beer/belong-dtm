/*
Name: current_date
Description: Returns the date in yyyymmdd format.
Extension: Core
Type: Custom Code
*/
var d = new Date();
var yyyy = d.getFullYear().toString(),
	mnth = (d.getMonth() + 1).toString(),
	day = d.getDate().toString();
return (yyyy + (mnth[1] ? mnth : "0" + mnth[0]) + (day[1] ? day : "0" + day[0]));
