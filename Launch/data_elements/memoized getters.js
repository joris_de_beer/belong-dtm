app1 = { // Memoized Getters
	get env(){
		var ready = _get('runtimeConfig.env', nextData) ? true : false,
			env = _get('runtimeConfig.env.isProduction', nextData) ? "prod" : "dev";
		_satellite.logger.log('[DE_data_layer > env]', ready, env, nextData);
		if(ready){delete this.env; return this.env = env;}
	},
	get buildId(){
		var bld = _get('buildId', nextData);
		_satellite.logger.log('[DE_data_layer > buildId]', {bld:bld, nextData:nextData});
		if(bld){delete this.buildId;return this.buildId = bld;}
	},
	get version(){
		var v = _get('runtimeConfig.version', nextData);
		_satellite.logger.log('[DE_data_layer > version]', {v:v, nextData:nextData});
		if(v){delete this.version;return this.version = v;}
	},
	get cfEnv(){
		var cfE = _get('runtimeConfig.contentful.environment', nextData);
		_satellite.logger.log('[DE_data_layer > buildId]', {cfE:cfE, nextData:nextData});
		if(cfE){delete this.cfEnv;return this.cfEnv = cfE;}
	},
	get cfSpace(){
		var cfS = _get('runtimeConfig.contentful.space', nextData);
		_satellite.logger.log('[DE_data_layer > cfSpace]', {cfS:cfS, nextData:nextData});
		if(cfS){delete this.cfSpace;return this.cfSpace = cfS;}
	},
	get workspace(){
		var ws = _get('runtimeConfig.workspace', nextData);
		_satellite.logger.log('[DE_data_layer > workspace]', {ws:ws, nextData:nextData});
		if(ws){delete this.workspace;return this.workspace = ws;}
	}
},