/*
Name: build_date
Description: Date and time when Adobe Launch library was last published.
Extension: Core
Type: Javascript Variable
*/
return new Date(((window._satellite||{}).buildInfo||{}).buildDate);