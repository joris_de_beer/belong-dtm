// Initialise the data layer if not already available
window.digitalData = window.digitalData || _satellite.getVar('data_layer')

var returnVal = false,
	action = (event.detail||{}).action;

switch (action) {
	case 'interaction':
		returnVal = true;
        digitalData.trackEvent(event);
		break;
	default:
		digitalData.trackEvent(event);
		returnVal = true;
		break;
}
_satellite.logger.info('[EBR:trackEvent]', action, "MATCHED?", returnVal, event.detail);
return returnVal;
