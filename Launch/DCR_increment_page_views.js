/*
Name: DCR_increment_page_views
Description: Adobe Launch doesn't increment the built in page views
             data for Single Page Applications, so we need to do it manually.
Extension: Core
Type: Custom Code
*/
var storagePageViewsKey = "com.adobe.reactor.core.visitorTracking.pagesViewed";
var lifeTimePageViews = parseInt(localStorage.getItem(storagePageViewsKey)),
    sessionPageViews = parseInt(sessionStorage.getItem(storagePageViewsKey));

// If pageViews is a valid number, increment by one.
if( isNaN(lifeTimePageViews) !== true ){
	localStorage.setItem(storagePageViewsKey, lifeTimePageViews+1)
}
if( isNaN(sessionPageViews) !== true ){
	sessionStorage.setItem(storagePageViewsKey, sessionPageViews+1)
}