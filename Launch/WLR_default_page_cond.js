/*
Name: WLR_default_page_cond
Description: Sometimes the trackStore or trackNavigation event runs before analytics is ready,
			 so craft an event object using the nextData object.
Extension: Core
Type: Custom Code
*/
window.digitalData = _satellite.getVar('data_layer');

var eventName,
	evtDetailStore,
	action = "pageLoad",
	_get = digitalData.util.get,
	nextData = _satellite.getVar('next_data'),
	workspace = _satellite.getVar('workspace');

// Return early when page has already been tracked
if (digitalData.pageLoadTracked == true) {
	return false;
}
switch (workspace) {
	case 'PUBLIC':
		eventName = "trackNavigation"
		evtDetailStore = _get('props.pageProps.pageData', nextData)
		break
	case 'PORTAL':
		eventName = "trackStore"
		evtDetailStore = _get('props.pageProps.snapshot', nextData)
		break
	case 'SUPPORT':
		eventName = "trackStore"
		evtDetailStore = _get('props.pageProps.data', nextData)
		break
	default:
		eventName = "trackStore"
		evtDetailStore = _get('props.pageProps.snapshot', nextData)
		break
}

var craftedEventObject = {
	type: eventName,
	action: action,
	detail: {
		action: action,
		currentPage:location.pathname,
		cache: evtDetailStore,
		get store() {
			// Do I need this second one? I think I do.
			/*_satellite.logger.warn('[PLR_default_page_cond] craftedEventObject{} > event.detail.store is deprecated. Use event.detail.cache instead.');*/
			return evtDetailStore;
		},
		workspace: workspace

	}
};

_satellite.logger.info('[PLR_default_page_rule]', eventName, digitalData.debug.plr = { action: action, pageLoadTracked: digitalData.pageLoadTracked, craftedEventObject: craftedEventObject });

digitalData.track(eventName, craftedEventObject)

return true;
