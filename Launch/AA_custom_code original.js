if (typeof _satellite == "undefined") {
	// Setup some mock Adobe Launch functionality. Only runs outside of Launch.
	_satellite = {
		'logger': {
			log: console.log,
			info: console.info,
			debug: console.debug,
			warn: console.warn,
			error: console.error
		},
		env: "mock"
	};
	_satellite.logger.log('This my Adobe Launch Mock script');
}
// Initialise the data layer
digitalData = window.digitalData || {
	ts: Date.now(),
	trackEvent: function (event) { digitalData.updateDataLayer(event); },
	trackRouter: function (event) { digitalData.updateDataLayer(event); },
	trackStore: function (event) { digitalData.updateDataLayer(event); },
	trackAPI: function (event) { digitalData.updateDataLayer(event); },
	eventSeq: {},
	pageInfo: {
		metadata: {
			pageTitle: undefined,
			description: undefined,
			keywords: undefined,
			sections: undefined
		},
		get pageTitle() {
			return digitalData.pageInfo.metadata.pageTitle;
		},
		get description() {
			return digitalData.pageInfo.metadata.description;
		},
		get keywords() {
			return digitalData.pageInfo.metadata.keywords;
		},
		get sections() {
			return digitalData.pageInfo.metadata.sections;
		},
		get siteSection() {
			return document.location.pathname.split('/').slice(1);
		}
	},
	profileInfo: {
		/* will contain information about current user */
	},
	appInfo: {
		/* will contain information about the current app/form */
	},
	deviceInfo: {
		/* will contain informaiton about the user's device (esp for mobile apps */
	},
	errorInfo: {
		code: undefined,
		description: undefined,
		sequence: undefined
	},
	util: {
		get: function (path, data) {
			var i;
			path = typeof path === 'string' ? path.split('.') : path;
			var len = path.length;
			for (i = 0; typeof data === 'object' && data !== null && i < len; ++i) {
				data = data[path[i]];
			}
			return data;
		},
		merge: function (a, b) {
			for (var key in b) {
				if (b.hasOwnProperty(key)) a[key] = b[key];
			}
			return a;
		},
		/* 
		 * Searches the trackEvents object for most recently triggered event with 
		 * the specified name, + action OR operation 
		*/
		mostRecentEvent: function (eventName, options) {
			var eventSeq = window.digitalData.eventSeq,
				action = (options || {}).action || false,
				operation = (options || {}).operation || false,
				found = false,
				arr = [];
			eventName = typeof eventName === "string" ? new RegExp(eventName + "$") : undefined
			//_satellite.logger.log('mostRecent(' + eventName, ',', eventSeq, ')');
			if (eventName instanceof RegExp === false) { return false }
			// If no trackEvents object is empty, then return early.
			if (typeof eventSeq === "undefined" || Object.keys(eventSeq).length === 0) { return false }
			// Stuff all of the keys in the trackEvent object into an array
			for (var key in eventSeq) { arr.push(key); }
			// Loop of the array in reverse to find the most recent matching eventName
			for (var i = arr.length - 1; i >= 0; i--) {
				//_satellite.logger.log('Looking at: ' + arr[i]);
				if (eventName.test(arr[i])) {
					if ((action && operation) &&
						(action == eventSeq[arr[i]].action && operation == eventSeq[arr[i]].operation)) {
						return eventSeq[arr[i]];
					} else if ((action && !operation) && action == eventSeq[arr[i]].action) {
						return eventSeq[arr[i]];
					} else if ((operation && !action) && operation == eventSeq[arr[i]].operation) {
						return eventSeq[arr[i]];
					} else if ((!operation && !action)) {
						return eventSeq[arr[i]];
					}
				}
			}
		},
		detectBrowserInfo: function () {
			function locator(e) {
				return function (t) {
					for (var a in e) {
						if (e.hasOwnProperty(a))
							if (e[a].test(t))
								return a
					}
					return "Unknown"
				}
			}
			var browsers = locator({
				"IE Edge Mobile": /Windows Phone.*Edge/,
				"IE Edge": /Edge/,
				OmniWeb: /OmniWeb/,
				"Opera Mini": /Opera Mini/,
				"Opera Mobile": /Opera Mobi/,
				Opera: /Opera/,
				Chrome: /Chrome|CriOS|CrMo/,
				Firefox: /Firefox|FxiOS/,
				"IE Mobile": /IEMobile/,
				IE: /MSIE|Trident/,
				"Mobile Safari": /Mobile(\/[0-9A-z]+)? Safari/,
				Safari: /Safari/
			}),
				operatingSystems = locator({
					Blackberry: /BlackBerry|BB10/,
					"Symbian OS": /Symbian|SymbOS/,
					Maemo: /Maemo/,
					Android: /Android/,
					Linux: / Linux /,
					Unix: /FreeBSD|OpenBSD|CrOS/,
					Windows: /[\( ]Windows /,
					iOS: /iPhone|iPad|iPod/,
					MacOS: /Macintosh;/
				}),
				deviceTypes = locator({
					Nokia: /Symbian|SymbOS|Maemo/,
					"Windows Phone": /Windows Phone/,
					Blackberry: /BlackBerry|BB10/,
					Android: /Android/,
					iPad: /iPad/,
					iPod: /iPod/,
					iPhone: /iPhone/,
					Desktop: /.*/
				}),
				i = navigator.userAgent;
			return {
				browser: browsers(i),
				os: operatingSystems(i),
				type: deviceTypes(i)
			}
		}
	}
}; // update the datalayer
digitalData.updateDataLayer = function (event) {
	// _satellite.logger.log('[updateDataLayer]', _satellite.queue);
	var _get = digitalData.util.get,
		evt = {
			sequence: Object.keys(digitalData.eventSeq).length + 1,
			detail: event.detail,
			type: _get('nativeEvent.type', event),
			action: _get('detail.event', event),
			operation: _get('detail.data.0.op', event)
		};

	// Write into the event sequence array.
	digitalData.eventSeq[evt.sequence + ': ' + evt.type] = digitalData.event = evt;

	// Print to console.
	_satellite.logger.info('[AA EXT]digtialData.updateDataLayer(' + evt.type + ' action=' + evt.action + (evt.operation ? 'op=' + evt.operation : '') + ')', evt);

	// When a page first loads
	if (evt.type === "trackStore" && evt.action == "onCreate") {
		digitalData.pageInfo.metadata = _get('detail.data.0.contentful.cache', evt)
	}

	// When the user navigates
	if (evt.type === "trackStore" && evt.action == "replace contentful currentPage") {
		digitalData.pageInfo.metadata = _get('detail.data.0', evt)
	}

	// When the page content is updated (not quite a full page navigation)
	if (evt.type === "trackStore" && evt.action == "onPatch" && evt.operation === "add") {
		digitalData.pageInfo.metadata = _get('detail.data.0', evt)
	}

	// When the page content is modified (not quite a full page navigation)
	if (evt.type === "trackStore" && evt.action == "onPatch" && evt.operation === "replace") {
		digitalData.pageInfo.metadata = _get('detail.data.0', evt)
	}

	// When an API is triggered
	if (evt.type === "trackAPI" && evt.action == "API" /* && evt.moment === "AFTER"*/) {
		var headingEl = document.querySelector('h1');
		var apiMetaData = {
			pageTitle: _get('detail.data.0', evt),
			description: headingEl ? headingEl.innerText : undefined
		}
		digitalData.util.merge(digitalData.pageInfo.metadata, apiMetaData);

		evt.endPoint = _get('detail.data.0', evt);
		evt.moment = _get('detail.data.1', evt);

		// Save any errors into the datalayer
		var errorCode = _get('detail.data.2.0.errorCode', evt),
			errorDescription = _get('detail.data.2.0.errorDescription', evt);
		if (errorCode || errorDescription) {
			digitalData.errorInfo.code = errorCode;
			digitalData.errorInfo.description = errorDescription;
			digitalData.errorInfo.sequence = evt.sequence;
		}
	}
};
digitalData.initDataLayer = function (event) {
	var nextDataEl = document.querySelector('#__NEXT_DATA__');
	if (typeof nextDataEl === "object") {
		var _get = digitalData.util.get,
			nextData = JSON.parse(nextDataEl.innerText);

		// Set some data about the react web application
		var isProduction = _get('runtimeConfig.env.isProduction', nextData);
		var isDevelopment = _get('runtimeConfig.env.isDevelopment', nextData);
		digitalData.appInfo.env = isProduction ? "prod" : "dev";
		digitalData.appInfo.buildId = _get('buildId', nextData);
		digitalData.appInfo.version = _get('runtimeConfig.version', nextData);
		digitalData.appInfo.cfEnv = _get('runtimeConfig.contentful.environment', nextData);
		digitalData.appInfo.cfSpace = _get('runtimeConfig.contentful.space', nextData);
		// Initialise the pageInfo object
		var currentPage = _get('props.pageProps.snapshot.contentful.currentPage', nextData);
		digitalData.pageInfo.metadata = _get('props.pageProps.snapshot.contentful.cache.' + currentPage, nextData);
		// Set some info about the current device
		digitalData.deviceInfo = digitalData.util.detectBrowserInfo();
	} else {
		_satellite.logger.warn('#__NEXT_DATA__ unavailable. Unable to initialise digitalData{}');
	}
}
digitalData.initDataLayer();
if (_satellite.env === "mock") {
	_satellite.logger.log('[LaunchMock] Setup mock trackEvent listeners');
	document.addEventListener('trackEvent', digitalData.trackEvent, false);
	document.addEventListener('trackRouter', digitalData.trackRouter, false);
	document.addEventListener('trackStore', digitalData.trackStore, false);
	document.addEventListener('trackAPI', digitalData.trackAPI, false);
} else if (Array.isArray(_satellite.queue)) {
	_satellite.logger.info('[AA EXT]Move', _satellite.queue.length, 'queued events to digitalData.eventSeq');
	for (var index = 0; index < _satellite.queue.length; index++) {
		digitalData.updateDataLayer(_satellite.queue[index]);
	}
}