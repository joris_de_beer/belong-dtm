/*
Name: EBR_trackAPI_direct_call_rules
Description: Actions we want to take for a specific event.
Extension: Core
Type: Custom Code
*/

var lastEvent = digitalData.event[digitalData.event.length - 1];

if (lastEvent && lastEvent.login) {
	_satellite.track('dcr:login', lastEvent)
}

_satellite.track('dcr:increment_lifetime_page_views')
