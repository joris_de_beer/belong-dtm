/*
Name: Adobe Analytics Extension - Custom Code
Description: Custom Code block set on Adobe Analytics Extension in Launch.
			 This code is configured to run before other settings are applied.
			 This code initialises the data layer, and sets up the function to update/maintain it.
Extension: Adobe Analytics Extension
*/
digitalData = window.digitalData || _satellite.getVar('data_layer',{caller:'AA_EXT custom code'});

s.usePlugins = true;
function s_doPlugins(s) {
	// If a page view has been tracked `s.t()`, then increment the page view counters
	if (s.linkType == 0 && typeof event == "object" && /message|readystatechange|popstate/.test(event.type)) {
		_satellite.track('dcr:increment_page_views')
	} else if (s.linkType == 0 && typeof event == "object" && event.type !== "click") {
		_satellite.logger.log('[AA Ext - Custom Code] s_doPlugins()',
			{ 'linkType': s.linkType, 'pageName': s.pageName, 'linkName': s.linkName, 'event.type': event.type }
		);
	}
}
s.doPlugins = s_doPlugins
