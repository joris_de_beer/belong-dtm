// Initialise the data layer if not already available
window.digitalData = window.digitalData || _satellite.getVar('data_layer')

var returnVal = false,
	eventName = 'trackAPI',
	endpoint = (event.detail||{}).endpoint;

// strip any unique keys from the endpoint.
endpoint = /\/orchestrator\/v1\/services/.test(endpoint) ? "/orchestrator/v1/services" : endpoint;

switch (endpoint) {
	case '/auth/v1/login':
	case '/product/v1/products/mobile':
	case '/orchestrator/v1/orders/activate':
	case '/orchestrator/v1/services':
		returnVal = true;
		digitalData.track(eventName,event);
		break;
	default:
		returnVal = false;
		break;
}
_satellite.logger.info('[EBR:'+eventName+']', {endpoint:endpoint, returnVal:returnVal, detail:event.detail});
return returnVal;
