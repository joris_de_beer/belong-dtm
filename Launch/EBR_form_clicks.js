var _get = digitalData.util.get
	productComponent = {
		eventName:		"analytics-event",
		componentId:	"{{ 7pTg9ncSwEcNXApa8gxbz2 }}",
		componentName:	"{{ plan select }}",
		componentRole:	"{{ product tile }}",
		userInteraction:"impression",
		textContent:	"{{  }}",
		linkTarget:		"{{  }}",
		section:		"{{ planDetailsSectionBlock }}",
		sectionTitle:	"{{ Mobile Plans }}",
		imageSrc:		"{{  }}",
		altText:		"{{  }}",
		dataObj:		"{{ product store response }}"
	};

event._this = this
event.action = _get('nativeEvent.type', event)
event.detail = {
	el: event.element,
	action: _get('nativeEvent.type', event),
	text: _get('element.text', event) || _get('element.innerText', event),
	classList: _get('element.classList', event),
	elementPath: _get('nativeEvent.path', event),
	containers: {
		// Orders from tightest to widest
		productCTA: event.element.closest('[data-product-code]'),
		tile: event.element.closest('div[class^="Tilestyles__TileWrapper"]'),
		grid: event.element.closest('div[class^="Grid-"]'),
		container: event.element.closest('div[class^="Container"]'),
		section: event.element.closest('section[class^="Sectionstyles"]'),
		pageTitle: event.element.closest('div[class^="PageTitlestyles"]'),
		header: event.element.closest('header'),
		nav: event.element.closest('nav'),
		footer: event.element.closest('div[class^="GlobalFooterstyles"]')
	}
}
productComponent.event = event;
digitalData.updateDataLayer('trackFormInput', productComponent)

return true;