/*
Name: build_data
Description: returns a complete data layer object.
Extension: Core
Type: Custom Code
*/

var color= '#41B6E6'

var getDataLayer = (function () {
	var nextData = _satellite.getVar('next_data');
		_get = function (path, data) {
		// Convenience function to safely retrieve values from deeply nested objects and arrays.
		var i; path = typeof path === 'string' ? path.split('.') : path;
		var len = path.length;
		for (i = 0; typeof data === 'object' && data !== null && i < len; ++i) {
			data = data[path[i]];
		}
		return data;
	};
	var ts = Date.now(),
		pageInstanceID = function() { return page.pageInfo.pageName +'-'+ app.env},
		trackEvent = function (event) { this.updateDataLayer(event); },
		trackRouter = function (event) { this.updateDataLayer(event); },
		trackStore = function (event) { this.updateDataLayer(event); },
		trackAPI = function (event) { this.updateDataLayer(event); },
		event = [{  //  A list of events recored since the last hard page refresh.
			eventInfo:{
				eventName: "Conversion Event ID",
				eventAction: "Conversion Event Action Type",
				eventPoints: 200,
				type: "contentModifier",
				timeStamp: Date.now()
			},
			category: {
				primaryCategory: "Conversion Event Category"
			}
		}],
		currentEvent = {}, // The current/most recent event
		app = {
			env: _get('runtimeConfig.env.isProduction', nextData) ? "prod" : "dev",
			buildId: _get('buildId', nextData),
			version: _get('runtimeConfig.version', nextData),
			cfEnv: _get('runtimeConfig.contentful.environment', nextData),
			cfSpace: _get('runtimeConfig.contentful.space', nextData)
		},
		error = {
			code: undefined,
			description: undefined,
			sequence: undefined
		},
		page = {
			pageInfo: {
				pageName: _satellite.getVar('page_name'),
				pageTitle: _satellite.getVar('document_title'),
				referringURL: _satellite.getVar('referrer'),
				sysEnv: _satellite.getVar('device_type'),
				breadCrumbs: location.pathname.split('/').slice(1, 100),
				publishDate: _satellite.getVar('build_date'),
				publisher: _satellite.getVar('site_name'),
				metadata: {
					pageTitle: undefined,
					description: undefined,
					keywords: undefined,
					sections: undefined
				}
			}
		},
		transaction = {},
		product = [],
		session = {
			sessionCount: _satellite.getVar('session_count'),
			sessionPageViews: _satellite.getVar('session_page_count'),
			landingPage: _satellite.getVar('landing_page'),
			trafficSource: _satellite.getVar('traffic_source'),
			minutesOnSite: _satellite.getVar('minutes_on_site')
		},
		user = [
			{
				newVisitor: _satellite.getVar('new_visitor'),
				lifetimePageViews: _satellite.getVar('lifetime_page_views')
			},
			{profile:[
			{
				customerId:"",
				auth0token:""
			}
			]
		}],
		device = {
			get browser() { return util.detectBrowserInfo().browser },
			get os() { return util.detectBrowserInfo().os },
			get type() { return util.detectBrowserInfo().type },
			get orientation() { return window.matchMedia("(orientation: portrait)").matches ? "portrait" : "landscape" },
			screen: window.screen,
			navigator: window.navigator
		},
		util = {
			get: function (path, data) {
				var i;
				path = typeof path === 'string' ? path.split('.') : path;
				var len = path.length;
				for (i = 0; typeof data === 'object' && data !== null && i < len; ++i) {
					data = data[path[i]];
				}
				return data;
			},
			merge: function (a, b) {
				for (var key in b) {
					if (b.hasOwnProperty(key)) a[key] = b[key];
				}
				return a;
			},
			detectBrowserInfo: function () {
				function locator(e) {
					return function (t) {
						for (var a in e) {
							if (e.hasOwnProperty(a))
								if (e[a].test(t))
									return a
						}
						return "Unknown"
					}
				}
				var browsers = locator({
					"IE Edge Mobile": /Windows Phone.*Edge/,
					"IE Edge": /Edge/,
					OmniWeb: /OmniWeb/,
					"Opera Mini": /Opera Mini/,
					"Opera Mobile": /Opera Mobi/,
					Opera: /Opera/,
					Chrome: /Chrome|CriOS|CrMo/,
					Firefox: /Firefox|FxiOS/,
					"IE Mobile": /IEMobile/,
					IE: /MSIE|Trident/,
					"Mobile Safari": /Mobile(\/[0-9A-z]+)? Safari/,
					Safari: /Safari/
				}),
					operatingSystems = locator({
						Blackberry: /BlackBerry|BB10/,
						"Symbian OS": /Symbian|SymbOS/,
						Maemo: /Maemo/,
						Android: /Android/,
						Linux: / Linux /,
						Unix: /FreeBSD|OpenBSD|CrOS/,
						Windows: /[\( ]Windows /,
						iOS: /iPhone|iPad|iPod/,
						MacOS: /Macintosh;/
					}),
					deviceTypes = locator({
						Nokia: /Symbian|SymbOS|Maemo/,
						"Windows Phone": /Windows Phone/,
						Blackberry: /BlackBerry|BB10/,
						Android: /Android/,
						iPad: /iPad/,
						iPod: /iPod/,
						iPhone: /iPhone/,
						Desktop: /.*/
					}),
					i = navigator.userAgent;
				return {
					browser: browsers(i),
					os: operatingSystems(i),
					type: deviceTypes(i)
				}
			}
		};

		var eventInfo = function(eventName, eventAction, eventPoints, type, timeStamp) {
			this.eventName = eventName;
			this.eventAction = eventAction;
			this.eventPoints = eventPoints;
			this.type = type;
			this.timeStamp = timeStamp || Date.now();
			event.push([this]);
		};
		eventInfo.prototype.eventName = function() {
			return this.eventName;
		};
		eventInfo.prototype.eventAction = function() {
			return this.eventAction;
		};
		eventInfo.prototype.eventPoints = function() {
			return this.eventAction;
		};
		eventInfo.prototype.type = function() {
			return this.type;
		};
		eventInfo.prototype.timeStamp = function() {
			return this.timeStamp;
		};
		eventInfo.prototype.debug = function() {
			return {
				'eventName':this.eventName,
				'eventAction':this.eventAction,
				'eventPoints':this.eventPoints,
				'type':this.type,
				'timeStamp':this.timeStamp
			};
		}


		return {
		ts:ts,
		get pageInstanceID(){return pageInstanceID()},
		trackEvent:trackEvent,
		trackRouter:trackRouter,
		trackRouter:trackEvent,
		trackStore:trackEvent,
		trackAPI:trackAPI,
		event:event,
		eventInfo:eventInfo,
		currentEvent:currentEvent,
		app:app,
		error:error,
		page:page,
		session:session,
		user:user,
		device:device,
		util:util
	};
});

digitalData = getDataLayer()
console.group('digitalData');
console.log(digitalData);
console.groupEnd();

var myevent = new digitalData.eventInfo('Add to cart', 'form submit', '100', 'state');
var event2 = new digitalData.eventInfo();
console.log(digitalData.event);