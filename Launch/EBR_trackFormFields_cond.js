var _get = digitalData.util.get,
	_findPaths = digitalData.util.findPaths,
	_component = digitalData.component,
	_containers = {
		// Orders from tightest to widest
		tile: event.element.closest('div[class^="Tilestyles__TileWrapper"],div[class^="GridItem"]'),
		grid: event.element.closest('div[class^="Grid-"]'),
		container: event.element.closest('div[class^="Container"]'),
		article: event.element.closest('article'),
		section: event.element.closest('section[class^="Sectionstyles"]'),
		pageTitle: event.element.closest('div[class^="PageTitlestyles"]'),
		header: event.element.closest('header'),
		nav: event.element.closest('nav'),
		footer: event.element.closest('div[class^="GlobalFooterstyles"]')
	},
	parentSegment = function(pathString, _segments){
		var segments = _segments || -1;
		return String(pathString).split('.').slice(0,segments).join('.');
	},
	getTitleForContainer = function(element){
		var isDOMElement = element instanceof Element || element instanceof HTMLDocument,
			returnVal ="";
		if(isDOMElement){
			returnVal = element.querySelector('h1,h2,h3,h4,p,strong,li').innerText
		}
		return cleanText(returnVal);
	},
	cleanText = function(str) {
		return typeof str === 'string' ? str.replace(/\s+/g, ' ').trim() : str;
	};

var pathToElementInComponentStore =  _findPaths(_component, this.value)[0]; // findPaths() always returns an array
var pathToContainerInComponentStore = parentSegment(pathToElementInComponentStore);
var pathToTileInComponentStore = parentSegment(pathToElementInComponentStore,-3);

var productComponent = {
	eventName:		"analytics-event",
	componentId:	_get(pathToContainerInComponentStore+'.id',_component),
	componentName:	_get(pathToTileInComponentStore+'.name',_component),
	componentRole:	_get(pathToTileInComponentStore+'.contentType',_component),
	containers:		_containers,
	userInteraction:"click",
	textContent:	getTitleForContainer(_containers.tile),
	linkTarget:		undefined,
	section:		_containers.section,
	sectionTitle:	getTitleForContainer(_containers.section),
	imageSrc:		undefined,
	altText:		undefined,
	dataObj:		_get(pathToContainerInComponentStore,_component),
};

/* The following is for debug purposes only, and can/should be removed once in production */
var raw = {
	action: _get('nativeEvent.type', event),
	_this : this,
	_component:_component,
	_containers:_containers,
	parentSegment:parentSegment,
	detail: {
		el: event.element,
		action: _get('nativeEvent.type', event),
		text: _get('element.text', event) || _get('element.innerText', event),
		classList: _get('element.classList', event),
		elementPath: _get('nativeEvent.path', event),
		inputValue: this.value,
		formElement: this.form,
		sectionTitle: "",
		pathToElementInComponentStore:pathToElementInComponentStore,
		pathToContainerInComponentStore:pathToContainerInComponentStore,
		pathToTileInComponentStore:pathToTileInComponentStore,

	}
}
event.productComponent = productComponent;
event.raw = raw;
_satellite.logger.info('[EBR:trackFormInput]\nproductComponent:',productComponent,'\nraw:',raw,'\nevent:',event);
digitalData.track('trackFormInput', event)
return true;