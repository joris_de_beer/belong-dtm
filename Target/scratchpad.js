transaction = {
	"orderId": ${"d1808wsh"}, 
	"total": { 
		"basePrice": ${300}, 
		"voucherCode": ${"Summer Sale"}, 
		"voucherDiscount": ${0.25}, 
		"currency": ${"AUD"}, 
		"taxRate": ${0.1}, 
		"priceWithTax": ${330}, 
		"transactionTotal": ${255} 
	}, 
	"products":[ //Object array of products included in the transaction 
		"product[n]": { //Object of nth product }
	]
}