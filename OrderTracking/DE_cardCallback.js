try {
	var cardData = BELONG.dataLayer.orderData.callback;
	var cardStatus = "Card Status:"+cardData.cardStatus,
		callbackRequestStatus = "Callback Request Status:"+cardData.callbackRequestStatus;
	var cardValue = cardStatus+"|"+callbackRequestStatus;
	BELONG.dataLayer.cards=BELONG.dataLayer.cards||{};
	BELONG.dataLayer.cards.callback={
		'cardValue':cardValue,
		'cardStatus':cardStatus,
		'callbackRequestStatus':callbackRequestStatus
	};
	_satellite.notify('Order Tracking - Callback Card Data Found',3)
	return cardValue
} catch(error){
	throw error;
	return error;
}