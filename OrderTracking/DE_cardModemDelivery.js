try{
	var cardData = BELONG.dataLayer.orderData.modemDelivery;
	var cardStatus = "Card Status:"+cardData.cardStatus,
		modemTrackingId = "Modem Tracking Id:"+cardData.modemTrackingId;
	var cardValue = cardStatus+"|"+modemTrackingId;
	BELONG.dataLayer.cards=BELONG.dataLayer.cards||{};
	BELONG.dataLayer.cards.callback={'cardValue':cardValue,'cardStatus':cardStatus,'modemTrackingId':modemTrackingId};
	_satellite.notify('Order Tracking - Modem Delivery Card Data Found',3)
	return cardValue
}catch(error){
	return error;
}