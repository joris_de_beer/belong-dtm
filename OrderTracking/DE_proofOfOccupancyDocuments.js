try{
	var orderTrackingPodStatus = BELONG.dataLayer.orderData.orderTrackingPodStatus;
	var cardStatus = "Card Status:"+ orderTrackingPodStatus.cardStatus,
		podCompleteDate = "POD Complete Date:"+ orderTrackingPodStatus.podCompleteDate,
		podStatus = "POD Status:" + orderTrackingPodStatus.podStatus;
	var cardValue = [cardStatus,podCompleteDate,podStatus].join('|');
	BELONG.dataLayer.cards=BELONG.dataLayer.cards||{};
	BELONG.dataLayer.cards.orderTrackingPodStatus={'cardValue':cardValue};
	_satellite.notify('Order Tracking - Connect Card Data Found',3)
	return cardValue
}catch(error){
	return error;
}