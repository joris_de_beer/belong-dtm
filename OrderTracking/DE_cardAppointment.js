try{
	var cardData = BELONG.dataLayer.orderData.appointment;
  var cardStatus= "Card Status:"+cardData.cardStatus,
      date			= "Appointment Date:"+cardData.appointmentDate,
      time			= "Appointment Time:"+cardData.appointmentTime;
  var cardValue = cardStatus + "|" + date + "|" + time;
  BELONG.dataLayer.cards=BELONG.dataLayer.cards||{};
  BELONG.dataLayer.cards.appointment={'cardValue':cardValue,'cardStatus':cardStatus,'date':date,'time':time};
  _satellite.notify('Order Tracking - Appointment Card Data Found',3)
	return cardValue;
}catch(error){
	return error
}