try{
	var cardData = BELONG.dataLayer.orderData.order;
	var cardStatus= "Card Status:"+cardData.cardStatus,
		futureDate= "Future Order Date:"+cardData.futureOrderDate,
		serviceDate= "Service Added Date:"+cardData.serviceAddedDate;
	var cardValue = cardStatus + "|" + futureDate + "|" + serviceDate;
	BELONG.dataLayer.cards=BELONG.dataLayer.cards||{};
	BELONG.dataLayer.cards.appointment={'cardValue':cardValue,'cardStatus':cardStatus,'futureDate':futureDate,'serviceDate':serviceDate};
	_satellite.notify('Order Tracking - Order Card Data Found',3)
	return cardValue;
}catch(error){
	return error
}