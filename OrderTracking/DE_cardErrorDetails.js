try{
	if(typeof window.JSON !== "object"){return "Browser doesn't support JSON"};
	var errorType = "Error Type:"+BELONG.dataLayer.orderData.errorType,
		errorData = JSON.stringify(BELONG.dataLayer.orderData.errors,'',0);
	var cardValue = errorType +"|"+ errorData;
	_satellite.notify('Order Tracking - Error Card Data Found',3)
	return cardValue
}catch(error){
	return error;
}