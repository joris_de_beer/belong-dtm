try{
	var cardData = BELONG.dataLayer.orderData.service;
	var cardStatus= "Card Status:"+cardData.cardStatus,
		activationDate= "Service Activation Date:"+cardData.serviceActivationDate,
		activationStatus= "Service Activation Status:"+cardData.serviceActivationStatus;
	var cardValue = cardStatus + "|" + activationDate + "|" + activationStatus;
	BELONG.dataLayer.cards=BELONG.dataLayer.cards||{};
	BELONG.dataLayer.cards.appointment={'cardValue':cardValue,'cardStatus':cardStatus,'activationDate':activationDate,'activationStatus':activationStatus};
	_satellite.notify('Order Tracking - Service Card Data Found',3)
	return cardValue;
}catch(error){
	return error
}