// Device Type
var deviceType = "",
	detectBrowserInfo= function () {
		function locator(e) {
			return function (t) {
				for (var a in e) {
					if (e.hasOwnProperty(a))
						if (e[a].test(t))
							return a
				}
				return "Unknown"
			}
		}
		var browsers = locator({
			"IE Edge Mobile": /Windows Phone.*Edge/,
			"IE Edge": /Edge/,
			OmniWeb: /OmniWeb/,
			"Opera Mini": /Opera Mini/,
			"Opera Mobile": /Opera Mobi/,
			Opera: /Opera/,
			Chrome: /Chrome|CriOS|CrMo/,
			Firefox: /Firefox|FxiOS/,
			"IE Mobile": /IEMobile/,
			IE: /MSIE|Trident/,
			"Mobile Safari": /Mobile(\/[0-9A-z]+)? Safari/,
			Safari: /Safari/
		}),
			operatingSystems = locator({
				Blackberry: /BlackBerry|BB10/,
				"Symbian OS": /Symbian|SymbOS/,
				Maemo: /Maemo/,
				Android: /Android/,
				Linux: / Linux /,
				Unix: /FreeBSD|OpenBSD|CrOS/,
				Windows: /[\( ]Windows /,
				iOS: /iPhone|iPad|iPod/,
				MacOS: /Macintosh;/
			}),
			deviceTypes = locator({
				Nokia: /Symbian|SymbOS|Maemo/,
				"Windows Phone": /Windows Phone/,
				Blackberry: /BlackBerry|BB10/,
				Android: /Android/,
				iPad: /iPad/,
				iPod: /iPod/,
				iPhone: /iPhone/,
				Desktop: /.*/
			}),
			i = navigator.userAgent;
		return {
			browser: browsers(i),
			os: operatingSystems(i),
			deviceTypes: deviceTypes(i)
		}
	};

try {
	var appSourceVal = _satellite.getVar('appSource');
	if (appSourceVal) deviceType = appSourceVal + " : ";
}
catch(err) {
		//
}
deviceType = deviceType + detectBrowserInfo.deviceType
return deviceType;