/* THIRD PARTY SCRIPT: Get Belong Customer Id and Service Status, Type, Class */
//var s_v71 = _satellite.readCookie('s_v71');
_satellite.notify('typeof s = '+typeof s+' typeof jQuery = '+typeof jQuery,1);				

if(s.eVar16 == 'undefined' || s.eVar16 === ""){
	function tryGetCustomerId() {
		function checkHTMLattr() {
			if (typeof jQuery != 'undefined') {
				custidLcl = jQuery('div#GlobalData').attr('data-utilibillid');
				if (typeof custidLcl != 'undefined' && custidLcl) {
					if (typeof s.Util != 'undefined' && typeof s.Util.cookieRead != 'undefined') {
						var recentCookieVal = s.Util.cookieRead("s_v16");
					}
					if (recentCookieVal !== custidLcl) {
						s.eVar16 = custidLcl;
						var s_linkTrackVars = s.linkTrackVars;
						s.linkTrackVars += ",eVar16";
						s.linkTrackEvents = "event8";
						var satCustType = _satellite.getVar('customerType');
						if (satCustType) {
							s.linkTrackVars += ",eVar18";
							s.eVar18 = satCustType;
						}
						s.events = "event8";
						s.tl(false,'o','bl:account:link');
						s.linkTrackVars = s_linkTrackVars;
					}
					else {
						//_satellite.notify('v16 already set this session', 1);
					}
					return true;
				}
			}
			return false;
		}
		var totalInterval = 0
		var timer = setInterval(function() {
			totalInterval = totalInterval + 100;
			var checkLocalCustData = checkHTMLattr();

			if (checkLocalCustData == true || totalInterval > 1000) {
				//_satellite.notify('Clear the timer', 1);
				clearTimeout(timer);
			}
		}, 100);
	}
	tryGetCustomerId();
}