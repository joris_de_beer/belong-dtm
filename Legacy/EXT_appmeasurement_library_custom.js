//////////////////////////////////////////////////
// Custom JS functions
//////////////////////////////////////////////////
// This function removes html tags and their inner text
s_removeHtmlTags = function(s_string) {
	var s_txtActual = s_string.replace(/<(a+)[^>]*>.*\n?<\/\1>|<(\w+)[^>]*>|<\/.+>/gi,'');
	return s_txtActual;
}
// This function removes all special characters (including html tags)
s_removeAllSpecialChars = function(s_string) {
	var s_txtActual = s_removeHtmlTags(s_string);
	s_txtActual = s_txtActual.replace(/&amp;/gi,"and");
	s_txtActual = s_txtActual.replace(/&/gi,"and");
	s_txtActual = s_txtActual.replace(/,/gi,"");
	s_txtActual = s_txtActual.replace(/  /gi,"");
	s_txtActual = s_txtActual.replace(/\n/g,"");
	s_txtActual = s_txtActual.replace(/[^\w\s\.\%]/gi, '');
	return s_txtActual;
}
//////////////////////////////////////////////////
function loadSCFunctions() {
	if (typeof (s_events) == "undefined") {s_events="";}
	var urlString = document.location.pathname;
	/* Default check for page identification information from URL */
	if(typeof(s_section) == "undefined" || s_section == ""){
		if (urlString == "/") urlString = "/homepage";
		var dirPairs = urlString.split("/");
		// Provision for overrides
		if (typeof s_subSectionOvrd != 'undefined' && s_subSectionOvrd) {
			dirPairs.splice(0, 1);
			dirPairs.unshift(s_subSectionOvrd);
			dirPairs.unshift(s_sectionOvrd);
			dirPairs.unshift("");
		}
		else if (typeof s_sectionOvrd != 'undefined' && s_sectionOvrd) {
			dirPairs.splice(0, 1);
			dirPairs.unshift(s_sectionOvrd);
			dirPairs.unshift("");
		}
		var qString = decodeURI(document.location.search);
		var qPairs = qString.split("&");
		s_pageName = s.generateURLPageName(dirPairs);
		s_pageName = "bl:"+s_pageName;
		/*  Process query string parameters */
		/* Set the valid sections to include query sting parameters OR set to all */
		//var validQryStringSections = /value1|value2/i;
		var validQryStringSections = "none";
		/* Set the valid query sting parameters OR set to all */
		var validPageNameQryStrings = /page/i;
		//var validPageNameQryStrings = "all";
		if(validQryStringSections == "all" /* || validQryStringSections.test(s_section)*/){
			var l=0;
			/* Loop through each query sting parameter */
			for (var k=0;k<qPairs.length;k++) {
				var qValue = qPairs[k];
				/* Is the parameter a valid one to include */
				if(validPageNameQryStrings == "all" || validPageNameQryStrings.test(qValue)){
					/* Does the parameter actually have a value */
					if(!/\=$/.test(qValue)){
						if (k==0){s_pageQuery=qValue;}
						else if (l==0) {s_pageQuery="?"+qValue;}
						else {s_pageQuery=s_pageQuery+"&"+qValue;}
						l++;
					}
				}
			}
		}
		// Append the page query
		if(typeof s_pageQuery != 'undefined' && typeof s_pageName != 'undefined') s_pageName=s_pageName+":"+s_pageQuery;
	} /* End URL processing for pageName */
} /* End loadSCFunctions */

if (typeof(loadSCFunctions) == "function") {
  loadSCFunctions();
}