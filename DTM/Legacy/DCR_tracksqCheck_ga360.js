/* BT-24403 Ref #5 Check address lightbox */
var fbb = _satellite.getVar("fbbProductCatalogue"),
	digitalData = window.digitalData || {},
	eventLabel = (((window.BELONG||{}).dataLayer||{}).event||{}).eventLabel,
	lastTrackedEvent = ((window.BELONG||{}).dataLayer||{}).lastTrackedEvent;

digitalData.event = digitalData.event || [];

if( eventLabel === "Click in Address Field" && lastTrackedEvent !== "analytics-tracksqCheck"){
	window.BELONG.dataLayer.lastTrackedEvent = "analytics-tracksqCheck";
	digitalData.event.push({
		eventName: "analytics-event",
		userInteraction: "impression",
		journey: "entice",
		formStep: "0",
		lastTrackedEvent: window.BELONG.dataLayer.lastTrackedEvent,
		product: fbb.getProductData(),
	});
	if(_satellite.settings.notifications){
		console.log('BT-24403 Ref #5 Check address lightbox','digitalData.event[n]:',JSON.stringify(digitalData.event[digitalData.event.length-1], null, "\t"), 'lastTrackedEvent = ',window.BELONG.dataLayer.lastTrackedEvent)
	}

}
