/*	Data Element to manage setting of eVar71 Service Type variable.
	Replaces the following legacy data elements:
	* serviceStatusTypeClass
	* broadbandServiceType
	* fbbServiceType
*/
var returnVal, returnFrom,
	serviceType = ((BELONG||{}).dataLayer||{}).serviceType,
	orderData   = ((BELONG||{}).dataLayer||{}).orderData||{},
	productData = ((BELONG||{}).dataLayer||{}).productData||{},
	sessionStorageProduct = window.sessionStorage ? window.sessionStorage.getItem('product') : undefined,
	serviceTypeCookie = _satellite.readCookie('s_v71');

if(serviceType){
	returnVal = serviceType;
	returnFrom = "serviceType";
}else if(orderData.serviceType){
	returnVal = orderData.serviceType;
	returnFrom = "orderData.serviceType";
}else if(productData.serviceType){
	returnVal = orderData.serviceType;
	returnFrom = "orderData.serviceType";
}else if(sessionStorageProduct){
	returnVal = sessionStorageProduct;
	returnFrom = "sessionStorage";
}else if(serviceTypeCookie){
	returnVal = serviceTypeCookie;
	returnFrom = "serviceTypeCookie"
}

if(returnVal){
	_satellite.setCookie('s_v71',returnVal, 45);
	_satellite.notify('serviceType '+returnVal+' found in '+returnFrom+' and written to s_v71 cookie',1);
}else if(!returnVal && _satellite.settings.notifications){
	console.log('serviceType not found',{
		'serviceType':serviceType,
		'orderData':orderData,
		'productData':productData,
		'serviceTypeCookie':serviceTypeCookie,
		'sessionStorageProduct':sessionStorageProduct
	})
}
return returnVal;