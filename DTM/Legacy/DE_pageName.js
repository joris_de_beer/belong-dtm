var pageName = window.s ? window.s.pageName : "bl:" + location.pathname.split('/').join(':'),
	returnVal,
	err;
try {
	// Try and clean up junk from pagenames
	returnVal = pageName.replace(/(http(|s)::|\.belong\.com\.au|(\:\?|\?).*|\:$)/g, "");
} catch (err) {
	s.eVar91 = "DE_pageName:" + err;
	returnVal = pageName;
}
_satellite.notify('DE_pageName: '+returnVal+' ['+err+']',1); 
return returnVal;