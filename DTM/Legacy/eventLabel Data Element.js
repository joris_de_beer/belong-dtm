(function(){ // Don't copy first line.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	try{
		var eventLabelValue="";
		if(typeof BELONG != 'undefined' && typeof BELONG.dataLayer != 'undefined' && typeof BELONG.dataLayer.event != 'undefined' && BELONG.dataLayer.event.eventLabel){
			eventLabelValue=BELONG.dataLayer.event.eventLabel;
		}else if(typeof BELONG != 'undefined' && typeof BELONG.dataLayer != 'undefined' && typeof BELONG.dataLayer.event != 'undefined' && BELONG.dataLayer.event.eventType){
			eventLabelValue=BELONG.dataLayer.event.eventType;
		}
		_satellite.notify('eventLabel Data Element returns: ' + eventLabelValue, 1);
		return eventLabelValue;
	}catch(err){
		s.eVar91 = "eventLabel DE:"+_satellite.getVar("tagError")(err);
		return "";
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}); // Don't copy last line.
