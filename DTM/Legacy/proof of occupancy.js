var dataMap = {
	"DEFAULTS": {category:"Proof-Of-Dwelling", action:"click", label:"proof of occupancy label not set", error:""},
	// User clicks banner on account overview
	"GET_POD_REQUEST":
	{
		label	:"",
		action	:""
	},
	// User selects primary document type
	"DOCUMENT_UPLOAD_SET_PRIMARY_FILE_TYPE":
	{
		label	:"Select primary file option"
	},
	// User selects secondary document type
	"DOCUMENT_UPLOAD_SET_SECONDARY_FILE_TYPE":
	{
		label	:"Select secondary file option"
	},
	// Check if name on the primary document matches the secondary document
	"DOCUMENT_UPLOAD_SET_NAME_MATCHES":
	{
		label	:"Check if name matches",
		action	:"validation"
	},
	// User selects primary document
	"DOCUMENT_UPLOAD_SET_PRIMARY_FILE":
	{
		label	:"Select primary file",
		action	:"file-select"
	},
	// User selects secondary document
	"DOCUMENT_UPLOAD_SET_SECONDARY_FILE":
	{
		label	:"Select secondary file",
		action	:"file-select"
	},
	// User selects an unsupported file type for primary document
	"DOCUMENT_UPLOAD_ERROR_UNALLOWED_MIMETYPE_PRIMARY":
	{
		label	:"Primary file mime type error",
		action	:"file-error"
	},
	// User selects an unsupported file type for secondary document
	"DOCUMENT_UPLOAD_ERROR_UNALLOWED_MIMETYPE_SECONDARY":
	{
		label	:"Secondary file mime type error",
		action	:"file-error"
	},
	// User selects a file for primary document which exceeds 5 Mb
	"DOCUMENT_UPLOAD_ERROR_FILE_SIZE_PRIMARY":
	{
		label	:"Primary file size error",
		action	:"file-error"
	},
	// User selects a file for secondary document which exceeds 5 Mb
	"DOCUMENT_UPLOAD_ERROR_FILE_SIZE_SECONDARY":
	{
		label	:"Secondary file size error",
		action	:"file-error"
	},
	// User selects more than 5 files for Lease/Rent agreement or Contract of Sale
	"DOCUMENT_UPLOAD_FILE_COUNT_ERROR":
	{
		label	:"Primary file count error",
		action	:"file-error"
	},
	// User opens more information model
	"DOCUMENT_UPLOAD_MODAL_OPEN":
	{
		label	:"More information modal open"
	},
	// User closes more information modal
	"DOCUMENT_UPLOAD_MODAL_CLOSE":
	{
		label	:"More information modal close"
	},
	// User removes selected primary files
	"DOCUMENT_UPLOAD_REMOVE_FILES":
	{
		label	:"Remove files"
	},
	// User removes selected secondary files
	"DOCUMENT_UPLOAD_REMOVE_SECONDARY_FILES":
	{
		label	:"Remove secondary files"
	},
	// Triggered when upload process is complete
	"DOCUMENT_UPLOAD_FILE_UPLOAD_COMPLETE":
	{
		label	:"Upload complete",
		action	:"file upload"
	},
	// comment
	"DOCUMENT_UPLOAD_UPDATE_PROGRESS":
	{
		label	:"Triggered to get the upload progress",
		action	:"file upload"
	},
	// Get the initial pod status for the user - To check whether POD page has to displayed for the user
	"GET_POD_REQUEST":
	{
		label	:"Pod status",
		action	:"load"
	},
	// Triggered when GET_POD_REQUEST succeeds
	"DOCUMENT_UPLOAD_GET_POD_SUCCESS":
	{
		label	:"Pod success",
		action	:"load"
	},
	// Triggered when GET_POD_REQUEST fails
	"DOCUMENT_UPLOAD_GET_POD_FAIL":
	{
		label	:"Pod failure",
		action	:"load"
	},
	// Triggered when upload fails
	"DOCUMENT_UPLOAD_ERROR":
	{
		label	:"Upload error",
		action	:"file upload"
	}
};
// Read the event data in from the dataLayer
var event		=((BELONG||{}).dataLayer||{}).event||{};
// shortcut to the eventType.
var eventType	=event.type;
// set values for event type, falling back to default if unavailable.
if(dataMap[eventType]){
	event.eventAction	= dataMap[eventType].action  ? dataMap[eventType].action  : dataMap["DEFAULTS"].action;
	event.eventLabel	= dataMap[eventType].label   ? dataMap[eventType].label   : dataMap["DEFAULTS"].label;
	event.eventCategory = dataMap[eventType].category? dataMap[eventType].category: dataMap["DEFAULTS"].category;
	event.errorCode		= dataMap[eventType].error   ? dataMap[eventType].error   : dataMap["DEFAULTS"].error;
}
// update the event key in the dataLayer
BELONG.dataLayer.event = event;

orderValue = BELONG.dataLayer.event.eventData.orderValue;
_satellite.setVar( "orderValue",	orderValue );

// set data element values	
_satellite.setVar( "eventAction",	event.eventAction );
_satellite.setVar( "eventLabel",	event.eventLabel );
_satellite.setVar( "eventCategory",	event.eventCategory );
_satellite.setVar( "errorCode",		event.errorCode );

// set s.prop values
s.prop26=[event.eventCategory,event.eventAction,event.eventLabel].join(':');

s.prop26 = Val1 + ":" + Val2 + ":";
//s.prop31=event.eventCategory;

// log information
_satellite.notify("analytics-trackProofOfOccupancy custom page code", 3);
if(_satellite.custom_debugMode) console.debug('eventType:',eventType,BELONG.dataLayer.event);
