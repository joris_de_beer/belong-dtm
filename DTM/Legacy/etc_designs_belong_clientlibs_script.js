function StringBuffer() {
    this.buffer = []
}

function Utf8EncodeEnumerator(a) {
    this._input = a, this._index = -1, this._buffer = []
}

function Base64DecodeEnumerator(a) {
    this._input = a, this._index = -1, this._buffer = []
}! function(a) {
    "use strict";
    a.util = function() {
        var b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q;
        return b = function() {
                $(".lt-ie10").length && ($("textarea[maxlength]").each(function() {
                    var a = $(this),
                        b = parseInt(a.attr("maxlength"), 10);
                    isNaN(b) === !1 && a.on("keyup.textareaMaxlength blur.textareaMaxlength", function() {
                        var a = $(this).val();
                        a.length > b && $(this).val(a.substr(0, b))
                    })
                }), String.prototype.trim || ! function() {
                    var a = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                    String.prototype.trim = function() {
                        return this.replace(a, "")
                    }
                }()), $(".lt-ie9").length && (a.IS_RESPONSIVE = !1, DD.bp.options({
                    isResponsive: !1
                }), $("li:last-child, th:last-child, td:last-child, tr:last-child").addClass("last-child"), $("tr:nth-child(2n)").addClass("odd"), function() {
                    var a = function(a) {
                        var b = $('label[for="' + a.attr("id") + '"]');
                        return a.prop("checked") ? a.add(b).addClass("is-checked") : a.add(b).removeClass("is-checked"), a
                    };
                    $("input:radio, input:checkbox").each(function() {
                        var b = $(this);
                        "radio" === b.prop("type") ? $('input[name="' + b.prop("name") + '"]').on("change.checkboxPolyfill", function() {
                            a(b)
                        }) : "checkbox" === b.prop("type") && b.change(function() {
                            a(b)
                        }), a(b)
                    })
                }()), i() && $("html").removeClass("supports-cssanimations"), Modernizr.input.placeholder || $("input, textarea").placeholder(), $.fn.noScrollFocus = function() {
                    var a = window.scrollX,
                        b = window.scrollY;
                    return this.focus(), 0 === $(".lt-ie10").length && window.scrollTo(a, b), this
                }
            }, c = function(a, b, c, d) {
                var e = c,
                    f = !1;
                if (b = "string" == typeof b ? b.substring(b.indexOf("#") + 1) : null, null === e) {
                    var g = $(document).scrollTop(),
                        h = Math.abs(a - g),
                        i = 2e3,
                        j = 1e3;
                    e = h > i ? j : a > g ? (1 - g / a) * j : (1 - a / g) * j
                }
                $("html").velocity("stop").velocity("scroll", {
                    offset: a,
                    duration: e,
                    complete: function() {
                        f || (f = !0, "string" == typeof b && (window.History && window.History.pushState ? window.History.pushState(null, null, "#" + b) : window.location.hash = b), "function" == typeof d && d())
                    }
                })
            }, d = function() {
                $("body").off("click.scrollto").on("click.scrollto", ".scrollto, .scrollup, .scrolldown", function(a) {
                    var b = $(this),
                        d = b.attr("href"),
                        e = parseInt(b.attr("data-scroll-offset"), 10) || 0,
                        f = "true" === b.attr("data-scroll-tabto"),
                        g = "false" === b.attr("data-scroll-hash") ? !1 : !0;
                    d = d.substr(d.indexOf("#") + 1);
                    var h = $("#" + d),
                        i = g ? d : null;
                    0 === h.length && (h = $('a[name="' + d + '"]')), 0 !== h.length && (a.preventDefault(), f ? c(h.offset().top + e, i, null, function() {
                        h.eq(0).noScrollFocus()
                    }) : c(h.offset().top + e, i))
                })
            }, f = function(a) {
                var b = $(document).scrollTop();
                a ? ($("body").data("scrolltop", b), $("html,body").addClass("locked"), $("html,body").css("overflow", "hidden"), $("html,body").height("100%")) : ($("html,body").removeClass("locked"), $("html,body").css("overflow", "auto"), $("html,body").height("auto"), setTimeout(function() {
                    $(window).scrollTop($("body").data("scrolltop"))
                }, 0))
            }, g = {
                toCurrency: function(a, b, c, d, e) {
                    var f = isNaN(b) ? 2 : Math.abs(b),
                        g = c || ".",
                        h = "undefined" == typeof d ? "," : d,
                        i = 0 > a ? "-" : "",
                        j = parseInt(a = Math.abs(a).toFixed(f), 10) + "",
                        k = j.length,
                        k = k > 3 ? k % 3 : 0,
                        l = e || "";
                    return i + l + (k ? j.substr(0, k) + h : "") + j.substr(k).replace(/(\d{3})(?=\d)/g, "$1" + h) + (f ? g + Math.abs(a - j).toFixed(f).slice(2) : "")
                },
                abbrNum: function(a, b) {
                    b = Math.pow(10, b);
                    for (var c = ["k", "m", "b", "t"], d = c.length - 1; d >= 0; d -= 1) {
                        var e = Math.pow(10, 3 * (d + 1));
                        if (a >= e) {
                            a = Math.round(a * b / e) / b, a += c[d];
                            break
                        }
                    }
                    return a
                },
                roundNum: function(a, b) {
                    return Math.round(a * Math.pow(10, b)) / Math.pow(10, b)
                },
                dateString: function(a, b) {
                    return void 0 === b && (b = "Do MMMM YYYY"), a ? moment(a).format(b) : ""
                }
            }, h = {
                Android: function() {
                    return navigator.userAgent.match(/Android/i)
                },
                AndroidChrome: function() {
                    return window.chrome && navigator.userAgent.match(/Android/i)
                },
                BlackBerry: function() {
                    return navigator.userAgent.match(/BlackBerry/i)
                },
                iOS: function() {
                    return navigator.userAgent.match(/iPhone|iPad|iPod/i)
                },
                Opera: function() {
                    return navigator.userAgent.match(/Opera Mini/i)
                },
                Windows: function() {
                    return navigator.userAgent.match(/IEMobile/i)
                },
                any: function() {
                    var a = h.Android() || h.BlackBerry() || h.iOS() || h.Opera() || h.Windows();
                    return null !== a
                },
                touch: function() {
                    return "ontouchstart" in window || "onmsgesturechange" in window
                }
            }, i = function() {
                var a = window.navigator.userAgent,
                    b = a.indexOf("MSIE ");
                if (b > 0) return parseInt(a.substring(b + 5, a.indexOf(".", b)), 10);
                var c = a.indexOf("Trident/");
                if (c > 0) {
                    var d = a.indexOf("rv:");
                    return parseInt(a.substring(d + 3, a.indexOf(".", d)), 10)
                }
                var e = a.indexOf("Edge/");
                return e > 0 ? parseInt(a.substring(e + 5, a.indexOf(".", e)), 10) : !1
            }, j = function() {
                $(".fn_print").on("click.print", function(a) {
                    a.preventDefault(), window.print()
                })
            },
            function() {
                $("html").addClass(h.Android() ? "d-android" : h.BlackBerry() ? "d-blackberry" : h.iOS() ? "d-ios" : h.Opera() ? "d-opera" : h.Windows() ? "d-windows" : "d-other"), h.any() && $("html").addClass("d-any"), h.touch() && $("html").addClass("d-touch")
            }(), k = function() {
                var a, b, c, d = $("body");
                return a = function() {
                    if (d.hasClass("no-scroll") === !1) {
                        var a = $(window).scrollTop();
                        d.addClass("no-scroll").css({
                            top: -a
                        })
                    }
                }, b = function() {
                    var a = parseInt(d.css("top"), 10);
                    d.css({
                        top: ""
                    }).removeClass("no-scroll"), $(window).scrollTop(-a)
                }, c = function() {
                    d.hasClass("no-scroll") && d.height() > $(window).height() && d.offset().top - $(window).height() < -d.height() && d.css({
                        top: -(d.height() - $(window).height())
                    })
                }, {
                    add: a,
                    remove: b,
                    refresh: c
                }
            }(), l = function(a) {
                var b = !isNaN(a - 0) && null !== a && "" !== a && a !== !1;
                return b
            }, m = function(a) {
                var b = unescape(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + escape(a).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
                return b.replace(/["']/g, "")
            }, n = function() {
                return $("html").hasClass("supports-no-sessionstorage")
            }, o = function() {
                this.isPrivateBrowsingMode() && (a.adobeAnalytics.global.privateBrowsing(a.sqEntry.get.sourceType()), a.analytics.methods.privateBrowsing(a.sqEntry.get.sourceType()))
            }, p = function(a, b, c) {
                c = "undefined" == typeof c ? "" : c;
                for (var d = 0, e = b.length; e > d; d++)
                    if ("object" == typeof b[d] && b[d].name === a) return b[d].value;
                return c
            }, q = function(a, b) {
                var c = {
                    success: !1,
                    value: ""
                };
                if ("undefined" == typeof a.messages);
                else if (0 == a.messages.length);
                else {
                    var d = a.messages[0];
                    "undefined" == typeof d[b] || (c.success = !0, c.value = d[b])
                }
                return c
            }, {
                helpers: b,
                scrollPageTo: c,
                scrollPage: d,
                lockScroll: f,
                placeholder: e,
                formatter: g,
                isDevice: h,
                detectIE: i,
                print: j,
                noscroll: k,
                isNumber: l,
                loadPageVar: m,
                isPrivateBrowsingMode: n,
                handlePrivateBrowsingMode: o,
                getCMSMessage: p,
                getApiResponseMessage: q
            }
    }()
}(BELONG),
function(a) {
    a.accordian = function() {
        var a = $(".accordian-container"),
            b = $(".accordian-container .expand-btn");
        b.on("click", function(b) {
            b.preventDefault();
            var c = $(this),
                d = c.closest("li");
            d.hasClass("active") ? a.find("li").removeClass("active").removeClass("inactive") : d.hasClass("inactive") ? (a.find("li").removeClass("active").addClass("inactive"), d.removeClass("inactive").addClass("active")) : (a.find("li").addClass("inactive"), d.removeClass("inactive").addClass("active")), $("html").hasClass("lt-ie9") && (d.redraw(), a.find("li").redraw())
        })
    }
}(BELONG),
function(a) {
    a.adobeAnalytics = function() {
        var b;
        return {
            init: function() {
                b = this
            },
            initDataLayer: function() {
                a.dataLayer = a.dataLayer || {}, a.dataLayer.isLoggedIn = a.GD.isLoggedIn, a.dataLayer.isCustomer = a.GD.isCustomer, a.dataLayer.isMobo = a.GD.isMobo, a.dataLayer.isAgent = a.GD.isAgent, a.dataLayer.customerID = a.GD.utilibillid
            },
            events: {
                logoLinkClick: function(a) {
                    a = a || "", b.methods.fireDtmEvent({
                        eventType: "logoLinkClick",
                        eventCategory: "Logo",
                        eventAction: "Link click",
                        eventLabel: a
                    })
                },
                headerLinkClick: function(a) {
                    a = a || "", b.methods.fireDtmEvent({
                        eventType: "headerLinkClick",
                        eventCategory: "Header-Menu",
                        eventAction: "Link click",
                        eventLabel: a
                    })
                },
                callLinkClick: function() {
                    b.methods.fireDtmEvent({
                        eventType: "callLinkClick",
                        eventCategory: "OAHP-Click-To-Call",
                        eventAction: "Link click",
                        eventLabel: "Call"
                    })
                },
                buttonBarLinkClick: function(a) {
                    a = a || "", b.methods.fireDtmEvent({
                        eventType: "buttonBarLinkClick",
                        eventCategory: "OAHP-Button-Bar",
                        eventAction: "Link click",
                        eventLabel: a
                    })
                },
                bottomContentLinkClick: function(a) {
                    a = a || "", b.methods.fireDtmEvent({
                        eventType: "bottomContentLinkClick",
                        eventCategory: "OAHP-Bottom-Content",
                        eventAction: "Link click",
                        eventLabel: a
                    })
                },
                carouselLinkClick: function(a) {
                    a = a || "", b.methods.fireDtmEvent({
                        eventType: "carouselLinkClick",
                        eventCategory: "OAHP-Carousel",
                        eventAction: "Link click",
                        eventLabel: a
                    })
                },
                productListCisLinkClick: function(a) {
                    a = a || "", b.methods.fireDtmEvent({
                        eventType: "productListCisLinkClick",
                        eventCategory: "Product-List-CIS",
                        eventAction: "Link click",
                        eventLabel: a
                    })
                },
                productListChooseLinkClick: function(a) {
                    a = a || "", b.methods.fireDtmEvent({
                        eventType: "productListChooseLinkClick",
                        eventCategory: "Product-List-Choose",
                        eventAction: "Link click",
                        eventLabel: a
                    })
                },
                featureListLinkClick: function(a) {
                    a = a || "", b.methods.fireDtmEvent({
                        eventType: "featureListLinkClick",
                        eventCategory: "Product-Feature-List",
                        eventAction: "Link click",
                        eventLabel: a
                    })
                },
                tryAnotherAddressLinkClick: function(a) {
                    a = a || "", b.methods.fireDtmEvent({
                        eventType: "tryAnotherAddressLinkClick",
                        eventCategory: "SQ-Try-Another-Addr",
                        eventAction: "Link click",
                        eventLabel: a
                    })
                },
                productAccordianLinkClick: function(a) {
                    a = a || "", b.methods.fireDtmEvent({
                        eventType: "productAccordianLinkClick",
                        eventCategory: "Product-Accordion",
                        eventAction: "Link click",
                        eventLabel: a
                    })
                },
                productFeatureBelongVoiceExpand: function(a) {
                    b.methods.fireDtmEvent({
                        eventType: "productFeatureBelongVoiceExpand",
                        eventCategory: "Product-Feature-Belong-Voice",
                        eventAction: "Expand",
                        eventLabel: a
                    })
                },
                tooltipClick: function(a) {
                    a = a || "", b.methods.fireDtmEvent({
                        eventType: "tooltipClick",
                        eventCategory: "Tooltip",
                        eventAction: "Link click",
                        eventLabel: a
                    })
                },
                footerSocialLinkClick: function(a) {
                    a = a || "", b.methods.fireDtmEvent({
                        eventType: "footerSocialLinkClick",
                        eventCategory: "Footer-Social",
                        eventAction: "Outbound link click",
                        eventLabel: a
                    })
                },
                footerMenuLinkClick: function(a) {
                    a = a || "", b.methods.fireDtmEvent({
                        eventType: "footerMenuLinkClick",
                        eventCategory: "Footer-Menu",
                        eventAction: "Link click",
                        eventLabel: a
                    })
                }
            },
            global: {
                privateBrowsing: function(a) {
                    b.methods.fireDtmEvent({
                        eventType: "globalPrivateBrowsing",
                        eventCategory: "Private-Browsing",
                        eventAction: "Click",
                        eventLabel: a.toUpperCase() + "-Private Browsing",
                        virtualPage: "/virtual/private-browsing/" + a + "-private-browsing.html"
                    })
                },
                revealPhoneNumber: function(a) {
                    console.log("EVE.adobeAnalytics.global.revealPhoneNumber(): page=" + a), b.methods.fireDtmEvent({
                        eventType: "globalRevealPhoneNumber",
                        eventCategory: "Reveal-Phone-Number",
                        eventAction: "Click",
                        eventLabel: a.toUpperCase() + "-Reveal Phone Number",
                        virtualPage: "/virtual/reveal-phone-number/" + a + ".html"
                    })
                },
                sq: {
                    openModal: function() {
                        b.methods.fireDtmEvent({
                            eventType: "globalSqOpenModal",
                            eventCategory: "SQ-Open-Modal",
                            eventAction: "Select",
                            eventLabel: "Open modal",
                            virtualPage: "/virtual/sq/sq-modal-open.html"
                        })
                    },
                    check: function(a, c, d) {
                        b.methods.fireDtmEvent({
                            eventType: "globalSqCheck",
                            eventCategory: "SQ-Check",
                            eventAction: "Click",
                            eventLabel: a.toUpperCase() + "-Check if you can Belong-" + c,
                            virtualPage: "/virtual/sq-check/" + a + "-check-if-you-can-belong-" + c + ".html",
                            eventPage: a,
                            sqAttempt: c,
                            sqType: d
                        })
                    },
                    success: function(a, c, d, e) {
                        b.methods.fireDtmEvent({
                            eventType: "globalSqSuccess",
                            eventCategory: "SQ-Success",
                            eventAction: "Click",
                            eventLabel: a + "-" + d + "-Check if you can Belong",
                            virtualPage: "/virtual/sq-success/" + a + "-" + d + "-check-if-you-can-belong.html",
                            page: a,
                            serviceClass: c,
                            product: d,
                            sqType: e
                        })
                    },
                    landlineOption: function(a, c, d) {
                        b.methods.fireDtmEvent({
                            eventType: "globalSqLandlineOption",
                            eventCategory: "Landline",
                            eventAction: "Click",
                            eventLabel: a + "-" + c,
                            virtualPage: "/virtual/landline/" + a + "-" + c.toLowerCase() + ".html",
                            page: a,
                            option: c,
                            sqType: d
                        })
                    },
                    selectAddress: function(a, c) {
                        b.methods.fireDtmEvent({
                            eventType: "globalSqSelectAddress",
                            eventCategory: "SQ-Select-Address",
                            eventAction: "Select",
                            eventLabel: "Please select an address",
                            virtualPage: "/virtual/sq/please-select-an-address.html",
                            addressID: a,
                            sqType: c
                        })
                    },
                    structuredCantFindAddress: function() {
                        b.methods.fireDtmEvent({
                            eventType: "globalSqStructuredCantFindAddress",
                            eventCategory: "SQ-Cant-Find-Address",
                            eventAction: "Click",
                            eventLabel: "Can't find it?",
                            virtualPage: "/virtual/sq/cant-find-address.html"
                        })
                    },
                    structuredClicksOnTab: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "globalSqStructuredClicksOnTab",
                            eventCategory: "SQ-Clicks-On-Structured-Address-Tab",
                            eventAction: "Click",
                            eventLabel: "Clicks on " + a,
                            virtualPage: "/virtual/sq/structured-tab-" + a.toLowerCase() + ".html",
                            tab: a
                        })
                    },
                    structuredFoundListOfRefinedAddresses: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "globalSqStructuredFoundListOfRefinedAddresses",
                            eventCategory: "SQ-Structured-Found-Refined-List",
                            eventAction: "Load",
                            eventLabel: a + " user sees refined addresses",
                            virtualPage: "/virtual/sq/structured-refined-" + a.toLowerCase() + ".html",
                            type: a
                        })
                    },
                    structuredFoundMatch: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "globalSqStructuredFoundMatch",
                            eventCategory: "SQ-Structured-Found-Matched-Address",
                            eventAction: "Click",
                            eventLabel: "User sees refined addresses",
                            virtualPage: "/virtual/sq/structured-found-address-" + a.toLowerCase() + ".html",
                            type: a
                        })
                    },
                    structuredPropertyNameEntered: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "globalSqStructuredPropertyNameEntered",
                            eventCategory: "SQ-Structured-Property-Name-Entered",
                            eventAction: "Input",
                            eventLabel: "User enters property name",
                            virtualPage: "/virtual/sq/SQ-Structured-Property-Name-Entered.html",
                            name: a
                        })
                    },
                    findOutMoreOrJoinNow: function(a, c, d, e) {
                        b.methods.fireDtmEvent({
                            eventType: "globalSqFindOutMoreOrJoinNow",
                            eventCategory: c,
                            eventAction: "Link click",
                            eventLabel: a + "-" + c,
                            virtualPage: "/virtual/" + c.toLowerCase() + "/" + a + "-" + c.toLowerCase() + ".html",
                            page: a,
                            serviceClass: d,
                            sqType: e
                        })
                    },
                    failFailureMessages: function(a, c, d, e) {
                        b.methods.fireDtmEvent({
                            eventType: "globalSqFailFailureMessages",
                            eventCategory: "SQ-Failure",
                            eventAction: "Click",
                            eventLabel: c + "-" + d + "-" + a,
                            virtualPage: "/virtual/sq-failure/" + c + "-" + d + "-" + a + ".html",
                            errorCode: a,
                            page: c,
                            product: d,
                            sqType: e
                        })
                    },
                    failExperionError: function(a, c) {
                        b.methods.fireDtmEvent({
                            eventType: "globalSqFailExperionError",
                            eventCategory: "Experion-Error",
                            eventAction: "Click",
                            eventLabel: a + "-Experion error",
                            virtualPage: "/virtual/experion-error/" + a + "-experion-error.html",
                            page: a,
                            sqType: c
                        })
                    }
                },
                eoi: {
                    eoiForm: function(a, c, d) {
                        b.methods.fireDtmEvent({
                            eventType: "globalEoiEoiForm",
                            eventCategory: "EOI",
                            eventAction: "Click",
                            eventLabel: a + "-" + c,
                            virtualPage: "/virtual/eoi/" + a + "-" + c + ".html",
                            page: a,
                            formID: c,
                            sqType: d
                        })
                    },
                    tryAgain: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "globalEoiTryAgain",
                            eventCategory: "EOI",
                            eventAction: "Click",
                            eventLabel: "Try again",
                            virtualPage: "/virtual/eoi/try-again.html",
                            sqType: a
                        })
                    },
                    keepMeInformed: function(a, c) {
                        b.methods.fireDtmEvent({
                            eventType: "globalEoiKeepMeInformed",
                            eventCategory: "EOI",
                            eventAction: "Click",
                            eventLabel: a + "-Keep me informed",
                            virtualPage: "/virtual/eoi/" + a + "-keep-me-informed.html",
                            page: a,
                            sqType: c
                        })
                    },
                    contactMe: function(a, c) {
                        b.methods.fireDtmEvent({
                            eventType: "globalEoiContactMe",
                            eventCategory: "EOI",
                            eventAction: "Click",
                            eventLabel: a + "-Contact me",
                            virtualPage: "/virtual/eoi/" + a + "contact-me.html",
                            page: a,
                            sqType: c
                        })
                    },
                    letMeKnow: function(a, c) {
                        b.methods.fireDtmEvent({
                            eventType: "globalEoiLetMeKnow",
                            eventCategory: "EOI",
                            eventAction: "Click",
                            eventLabel: a + "-Let me know",
                            virtualPage: "/virtual/eoi/" + a + "let-me-know.html",
                            page: a,
                            sqType: c
                        })
                    }
                },
                agent: {
                    backToMe: function() {
                        b.methods.fireDtmEvent({
                            eventType: "globalAgentBackToMe",
                            eventCategory: "Agent",
                            eventAction: "Click",
                            eventLabel: "Change back to me"
                        })
                    },
                    mobo: function() {
                        b.methods.fireDtmEvent({
                            eventType: "globalAgentMobo",
                            eventCategory: "Agent",
                            eventAction: "Click",
                            eventLabel: "Select and log in"
                        })
                    }
                }
            },
            pages: {
                product: {
                    faq: function(a, c) {
                        console.log(a, c), b.methods.fireDtmEvent({
                            eventType: "productFaq",
                            eventCategory: "Product FAQ",
                            eventAction: "Click",
                            eventLabel: c + " - " + a
                        })
                    }
                },
                join: {
                    productSelection: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "pagesJoinProductSelection",
                            eventCategory: "Product",
                            eventAction: "Click",
                            eventLabel: "Join-Product",
                            virtualPage: "/virtual/product/join-product",
                            plan: a
                        })
                    },
                    productPreSelection: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "pagesJoinProductPreSelection",
                            eventCategory: "Product",
                            eventAction: "PreSelected",
                            eventLabel: "Join-Product",
                            virtualPage: "/virtual/product/preselected-join-product",
                            plan: a
                        })
                    },
                    addSpeedBoost25: function() {
                        b.methods.fireDtmEvent({
                            eventType: "pagesJoinAddSpeedBoost25",
                            eventCategory: "SpeedBoost-Add",
                            eventAction: "Click",
                            eventLabel: "Join-SB-25",
                            virtualPage: "/virtual/speedboost-add/join-sb-25.html"
                        })
                    },
                    addSpeedBoost100: function() {
                        b.methods.fireDtmEvent({
                            eventType: "pagesJoinAddSpeedBoost100",
                            eventCategory: "SpeedBoost-Add",
                            eventAction: "Click",
                            eventLabel: "Join-SB-100",
                            virtualPage: "/virtual/speedboost-add/join-sb-100.html"
                        })
                    },
                    addVoice: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "pagesJoinAddVoice",
                            eventCategory: "Voice-Add",
                            eventAction: "Click",
                            eventLabel: "Join-Voice-" + a,
                            virtualPage: "/virtual/voice-add/join-voice-" + a + ".html",
                            option: a
                        })
                    },
                    checkContract: function() {
                        b.methods.fireDtmEvent({
                            eventType: "pagesJoinCheckContract",
                            eventCategory: "Contract",
                            eventAction: "Click",
                            eventLabel: "Join-Contract",
                            virtualPage: "/virtual/contract/join-contract.html"
                        })
                    },
                    promoApplied: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "pagesJoinPromoApplied",
                            eventCategory: "Promo",
                            eventAction: "Click",
                            eventLabel: "Apply",
                            virtualPage: "/virtual/Promo/apply",
                            promoCode: a
                        })
                    },
                    promoAppliedStaffDiscount: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "pagesJoinPromoAppliedStaffDiscount",
                            eventCategory: "Staff Promo",
                            eventAction: "Click",
                            eventLabel: "Continue",
                            virtualPage: "/virtual/staff-promo/continue",
                            promoCode: a
                        })
                    },
                    steps: function(a, c, d) {
                        b.methods.fireDtmEvent({
                            eventType: "pagesJoinSteps",
                            eventCategory: "Join",
                            eventAction: "Click",
                            eventLabel: a,
                            virtualPage: "/virtual/join/" + c,
                            plan: d
                        })
                    },
                    confirmYourOrder: function(c) {
                        b.methods.fireDtmEvent({
                            eventType: "pagesJoinConfirmYourOrder",
                            eventCategory: "Join",
                            eventAction: "Click",
                            eventLabel: "3 - Confirm your Order",
                            virtualPage: "/virtual/join/3-confirm-your-order.html",
                            serviceClass: c.serviceClass,
                            speedBoostStatus: c.speedBoostStatus,
                            promoCode: c.promoCode,
                            planSizeType: c.planSize + "-" + c.planType,
                            withSpeedBoost: c.withSpeedBoost,
                            withContract: c.withContract,
                            today: a.utils.getTodaysDate(),
                            orderType: c.orderType,
                            userid: c.userid,
                            planSpeed: c.planSpeed,
                            voicePlan: c.voicePlan
                        })
                    },
                    errorEmail: function(a, c) {
                        b.methods.fireDtmEvent({
                            eventType: "pagesJoinErrorEmail",
                            eventCategory: "Join: Email Error",
                            eventAction: "Click",
                            eventLabel: a + " - " + c,
                            virtualPage: "/virtual/join-confirm-email-error/" + a + "-" + c + ".html",
                            errorCode: a,
                            errorMsg: c
                        })
                    },
                    errorPersonalDetails: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "pagesJoinErrorPersonalDetails",
                            eventCategory: "Join: Personal Details Error",
                            eventAction: "Click",
                            eventLabel: a,
                            virtualPage: "/virtual/join-personal-details-error/" + a + ".html",
                            errorMsg: a
                        })
                    },
                    errorPayment: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "pagesJoinErrorPayment",
                            eventCategory: "Join: Payment Error",
                            eventAction: "Click",
                            eventLabel: a,
                            virtualPage: "/virtual/join-payment-error/" + a + ".html",
                            errorMsg: a
                        })
                    },
                    errorOrder: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "pagesJoinErrorOrder",
                            eventCategory: "Join: Order Error",
                            eventAction: "Click",
                            eventLabel: a,
                            virtualPage: "/virtual/join-order-error/" + a + ".html",
                            errorMsg: a
                        })
                    },
                    errorProductSelection: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "pagesJoinErrorProductSelection",
                            eventCategory: "Join: Product Selection Error",
                            eventAction: "Click",
                            eventLabel: a,
                            virtualPage: "/virtual/join-product-selection-error/" + a + ".html",
                            errorMsg: a
                        })
                    }
                },
                payment: {
                    payNowRedirect: function(a) {
                        a = a || "", b.methods.fireDtmEvent({
                            eventType: "paymentPayNowRedirect",
                            eventCategory: "Auto-Payment",
                            eventAction: "Link click",
                            eventLabel: a
                        })
                    },
                    payLaterClick: function() {
                        b.methods.fireDtmEvent({
                            eventType: "paymentPayLaterClick",
                            eventCategory: "Auto-Payment",
                            eventAction: "Click"
                        })
                    }
                },
                troubleshooting: {
                    cardsLog: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "troubleshootingCardsLog",
                            eventCategory: "Troubleshooting-Cards",
                            eventAction: "Card select",
                            cardId: a.cardId,
                            nextCardId: a.nextCardId,
                            question: a.question,
                            answer: a.answer
                        })
                    },
                    beginButtonClick: function() {
                        b.methods.fireDtmEvent({
                            eventType: "troubleshootingBegin",
                            eventCategory: "Troubleshooting-Cards",
                            eventAction: "Click"
                        })
                    },
                    loadPlanData: function(a, c) {
                        b.methods.fireDtmEvent({
                            eventType: "troubleshootingPlanData",
                            eventCategory: "Troubleshooting-Cards",
                            eventAction: "Load",
                            success: a,
                            errorCode: c
                        })
                    },
                    submitCase: function(a) {
                        a.contactTime = a.contactTime || "", a.contactType = a.contactType || "", b.methods.fireDtmEvent({
                            eventType: "troubleshootingSubmitCase",
                            eventCategory: "Troubleshooting-Cards",
                            eventAction: "Card submit",
                            contactTime: a.contactTime,
                            contactType: a.contactType,
                            note: a.note,
                            issue: a.issue,
                            details: a.details
                        })
                    },
                    initData: function() {
                        b.methods.fireDtmEvent({
                            eventType: "troubleshootingCardsLog",
                            eventCategory: "Troubleshooting-Cards",
                            eventAction: "Click"
                        })
                    },
                    submitOutage: function(a) {
                        b.methods.fireDtmEvent(a ? a.error ? {
                            eventType: "troubleshootingOutagesChecking",
                            eventCategory: "Troubleshooting-Outages",
                            eventAction: "Blackhawk Error"
                        } : {
                            eventType: "troubleshootingOutagesChecking",
                            eventCategory: "Troubleshooting-Outages",
                            eventAction: "Outages detected"
                        } : {
                            eventType: "troubleshootingOutagesChecking",
                            eventCategory: "Troubleshooting-Outages",
                            eventAction: "No Outage"
                        })
                    }
                },
                callbackForm: {
                    submit: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "callbackSubmit",
                            eventCategory: "Callback Submit",
                            eventAction: "Click",
                            callbackTime: a
                        })
                    }
                },
                manageService: {
                    display: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "manageServiceDisplay",
                            eventCategory: "Manage-Service-Cards",
                            eventAction: "Load",
                            oldPlan: a.oldPlan
                        })
                    },
                    review: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "manageServiceReview",
                            eventCategory: "Manage-Service-Cards",
                            eventAction: "Click",
                            updateAction: a.updateAction,
                            oldPlan: a.oldPlan
                        })
                    },
                    update: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "manageServiceUpdate",
                            eventCategory: "Manage-Service-Cards",
                            eventAction: "Click",
                            oldPlan: a.oldPlan,
                            newPlan: a.newPlan
                        })
                    }
                },
                managePayment: {
                    display: function() {
                        b.methods.fireDtmEvent({
                            eventType: "paymentDetailsDisplay",
                            eventCategory: "Payment-Details-Card",
                            eventAction: "Load"
                        })
                    },
                    retryPayment: function(a, c) {
                        b.methods.fireDtmEvent({
                            eventType: "paymentDetailsRetry",
                            eventCategory: "Payment-Details-Card",
                            eventAction: "Click",
                            success: a,
                            errorCode: c
                        })
                    },
                    updatePayment: function(a, c) {
                        b.methods.fireDtmEvent({
                            eventType: "paymentDetailsUpdate",
                            eventCategory: "Payment-Details-Card",
                            eventAction: "Click",
                            success: a,
                            errorCode: c
                        })
                    },
                    prepaid: function(a, c) {
                        b.methods.fireDtmEvent({
                            eventType: "paymentDetailsPrepaid",
                            eventCategory: "Payment-Details-Card",
                            eventAction: "Click",
                            success: a,
                            errorCode: c
                        })
                    }
                },
                personalDetails: {
                    display: function() {
                        b.methods.fireDtmEvent({
                            eventType: "personalDetailsDisplay",
                            eventCategory: "Personal-Details-Card",
                            eventAction: "Load"
                        })
                    },
                    editEmail: function() {
                        b.methods.fireDtmEvent({
                            eventType: "personalDetailsEditEmail",
                            eventCategory: "Personal-Details-Card",
                            eventAction: "Click"
                        })
                    },
                    editMobile: function() {
                        b.methods.fireDtmEvent({
                            eventType: "personalDetailsEditMobile",
                            eventCategory: "Personal-Details-Card",
                            eventAction: "Click"
                        })
                    },
                    editPassword: function() {
                        b.methods.fireDtmEvent({
                            eventType: "personalDetailsEditPassword",
                            eventCategory: "Personal-Details-Card",
                            eventAction: "Click"
                        })
                    },
                    updateEmail: function() {
                        b.methods.fireDtmEvent({
                            eventType: "personalDetailsUpdateEmail",
                            eventCategory: "Personal-Details-Card",
                            eventAction: "Click"
                        })
                    },
                    updateMobile: function() {
                        b.methods.fireDtmEvent({
                            eventType: "personalDetailsUpdateMobile",
                            eventCategory: "Personal-Details-Card",
                            eventAction: "Click"
                        })
                    },
                    updatePassword: function() {
                        b.methods.fireDtmEvent({
                            eventType: "personalDetailsUpdatePassword",
                            eventCategory: "Personal-Details-Card",
                            eventAction: "Click"
                        })
                    }
                },
                productBuilder: {
                    checkAddressClick: function(a, c) {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderCheckAddressClick",
                            eventCategory: "Product-Builder",
                            eventAction: "Click",
                            productModel: a,
                            sqDone: c.sqDone,
                            sqOrderType: c.sqOrderType,
                            sqTechType: c.sqTechType
                        })
                    },
                    closeSq: function(a, c) {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderCloseSq",
                            eventCategory: "Product-Builder",
                            eventAction: "None",
                            productModel: a,
                            sqDone: c.sqDone,
                            sqOrderType: c.sqOrderType,
                            sqTechType: c.sqTechType
                        })
                    },
                    joinClick: function(a, c) {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderJoin",
                            eventCategory: "Product-Builder",
                            eventAction: "Click",
                            productModel: a,
                            sqDone: c.sqDone,
                            sqOrderType: c.sqOrderType,
                            sqTechType: c.sqTechType
                        })
                    },
                    promoClick: function(a, c) {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderJoinWithPromo",
                            eventCategory: "Product-Builder",
                            eventAction: "Click",
                            productModel: a,
                            sqDone: c.sqDone,
                            sqOrderType: c.sqOrderType,
                            sqTechType: c.sqTechType
                        })
                    },
                    planSelection: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderPlanSelection",
                            eventCategory: "Product-Builder",
                            eventAction: "Click",
                            planSize: a
                        })
                    },
                    planModalOpen: function() {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderPlanModalOpen",
                            eventCategory: "Product-Builder",
                            eventAction: "Click"
                        })
                    },
                    planModalClose: function() {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderPlanClose",
                            eventCategory: "Product-Builder",
                            eventAction: "Click"
                        })
                    },
                    contractSelection: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderContractSelection",
                            eventCategory: "Product-Builder",
                            eventAction: "Click",
                            contractOption: a
                        })
                    },
                    contractModalOpen: function() {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderContractModalOpen",
                            eventCategory: "Product-Builder",
                            eventAction: "Click"
                        })
                    },
                    contractModalClose: function() {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderContractModalClose",
                            eventCategory: "Product-Builder",
                            eventAction: "Click"
                        })
                    },
                    speedboostSelection: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderSpeedboostSelection",
                            eventCategory: "Product-Builder",
                            eventAction: "Click",
                            speedboostOption: a
                        })
                    },
                    speedboostModalOpen: function() {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderSpeedboostModalOpen",
                            eventCategory: "Product-Builder",
                            eventAction: "Click"
                        })
                    },
                    speedboostModalSelection: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderSpeedboostModalSelection",
                            eventCategory: "Product-Builder",
                            eventAction: "Click",
                            speedboostOption: a
                        })
                    },
                    speedboostModalClose: function() {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderSpeedboostModalClose",
                            eventCategory: "Product-Builder",
                            eventAction: "Click"
                        })
                    },
                    voiceSelection: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderVoiceSelection",
                            eventCategory: "Product-Builder",
                            eventAction: "Click",
                            voicePlan: a
                        })
                    },
                    voiceModalOpen: function() {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderVoiceModalOpen",
                            eventCategory: "Product-Builder",
                            eventAction: "Click"
                        })
                    },
                    voiceModalSelection: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderVoiceModalSelection",
                            eventCategory: "Product-Builder",
                            eventAction: "Click",
                            voicePlan: a
                        })
                    },
                    voiceModalClose: function() {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderVoiceModalClose",
                            eventCategory: "Product-Builder",
                            eventAction: "Click"
                        })
                    },
                    phoneLineSelection: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderVoiceSelection",
                            eventCategory: "Product-Builder",
                            eventAction: "Click",
                            planType: a
                        })
                    },
                    phoneLineModalOpen: function() {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderPhoneLineModalOpen",
                            eventCategory: "Product-Builder",
                            eventAction: "Click"
                        })
                    },
                    phoneLineModalClose: function() {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderPhoneLineModalClose",
                            eventCategory: "Product-Builder",
                            eventAction: "Click"
                        })
                    },
                    applyPromoCodeSuccess: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderPromoCodeApply",
                            eventCategory: "Product-Builder",
                            eventAction: "Validate",
                            promocodeValidation: a
                        })
                    },
                    applyPromoCodeFail: function(a) {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderPromoCodeApply",
                            eventCategory: "Product-Builder",
                            eventAction: "Validate",
                            promocodeValidation: a
                        })
                    },
                    applyStaffPromoDetails: function() {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderApplyStaffPromoDetails",
                            eventCategory: "Product-Builder",
                            eventAction: "Validate"
                        })
                    },
                    cancelStaffPromoDetails: function() {
                        b.methods.fireDtmEvent({
                            eventType: "productBuilderCancelStaffPromoDetails",
                            eventCategory: "Product-Builder",
                            eventAction: "Validate"
                        })
                    }
                }
            },
            methods: {
                fireDtmEvent: function(a) {
                    $(document).trigger("analytics-trackEvent", [a])
                }
            }
        }
    }()
}(BELONG),
function(a) {
    a.ajax = function(b, c, d, e, f) {
        return d = d || function() {}, e = e || function() {}, f = f || !0, b = "" === b ? "/apps/telstraEveServlet" : b, $(".error-banner.rest-error").remove(), $.ajax({
            url: b,
            type: "POST",
            data: c,
            dataType: "json",
            timeout: 12e4,
            success: function(b) {
                setTimeout(function() {
                    d(b)
                }, a.DEVDELAY)
            },
            error: function(b, c, d) {
                setTimeout(function() {
                    f && $(".error-banner.rest-error").removeClass("hidden"), e(b, c, d)
                }, a.DEVDELAY)
            }
        })
    }
}(BELONG),
function(a) {
    a.ajaxSetup = function() {
        $.ajaxSetup({
            timeout: 12e4,
            dataType: "json",
            cache: !1
        })
    }
}(BELONG),
function(a) {
    a.analytics = function() {
        var b;
        return {
            init: function() {
                b = this, $("a#header-logo, a#footer-logo").click(function(c) {
                    c.preventDefault();
                    var d = this.href,
                        e = this.id,
                        f = "Link click",
                        g = "Logo";
                    a.adobeAnalytics.events.logoLinkClick(this.id), b.methods.linkClickWithPageReload(d, f, g, e)
                }), $("a#nav-adsl,a#nav-nbn,a#nav-join,a#nav-support,a#nav-login,a#nav-belong-here,a#nav-logout,a#nav-change-password").click(function(c) {
                    c.preventDefault();
                    var d = this.href,
                        e = $(this).text(),
                        f = "Link click",
                        g = "Header-Menu";
                    a.adobeAnalytics.events.headerLinkClick($(this).text()), 0 !== $("#sq-launcher").length && "nav-join" === $(this).attr("id") ? b.methods.linkClickWithNonPageReload(f, g, e) : b.methods.linkClickWithPageReload(d, f, g, e)
                }), $("a#header-call").click(function() {
                    var c = "Call",
                        d = "Link click",
                        e = "OAHP-Click-To-Call";
                    a.adobeAnalytics.events.callLinkClick(), b.methods.linkClickWithNonPageReload(d, e, c)
                }), $(".button-list span.button-text").click(function(c) {
                    c.preventDefault();
                    var d = "What's available in your area?",
                        e = "Link click",
                        f = "OAHP-Button-Bar";
                    a.adobeAnalytics.events.buttonBarLinkClick(), b.methods.linkClickWithNonPageReload(e, f, d)
                }), $("a#link-home-adsl, a#link-home-nbn").click(function(c) {
                    c.preventDefault();
                    var d = this.href,
                        e = $(this).text(),
                        f = "Link click",
                        g = "OAHP-Button-Bar";
                    a.adobeAnalytics.events.buttonBarLinkClick($(this).text()), b.methods.linkClickWithPageReload(d, f, g, e)
                }), $("a#home-left-cta-btn, a#home-right-cta-btn").click(function(c) {
                    c.preventDefault();
                    var d = this.href,
                        e = $(this).text(),
                        f = "Link click",
                        g = "OAHP-Bottom-Content";
                    a.adobeAnalytics.events.bottomContentLinkClick($(this).text()), b.methods.linkClickWithPageReload(d, f, g, e)
                }), $("#carousel .slide a").click(function(c) {
                    c.preventDefault();
                    var d = this.href,
                        e = d,
                        f = "OAHP-Carousel",
                        g = "Link click";
                    a.adobeAnalytics.events.carouselLinkClick(this.href), b.methods.linkClickWithPageReload(d, g, f, e)
                }), $("a#cis-bundle-large,a#cis-bundle-regular,a#cis-nbn-large,a#cis-nbn-regular").click(function() {
                    var c = (this.href, this.id),
                        d = "Product-List-CIS",
                        e = "Link click";
                    a.adobeAnalytics.events.productListCisLinkClick(this.id), b.methods.linkClickWithNonPageReload(e, d, c)
                }), $("a#nbn-regular,a#nbn-large,a#bundle-large,a#bundle-regular").click(function(c) {
                    c.preventDefault();
                    var d = $(this).text();
                    if ("Choose" == d) {
                        var e = this.href,
                            f = this.id,
                            g = "Product-List-Choose",
                            h = "Link click";
                        a.adobeAnalytics.events.productListChooseLinkClick(this.id), b.methods.linkClickWithPageReload(e, h, g, f)
                    }
                }), $(".feature-list a").click(function() {
                    var c = $(this).text(),
                        d = "Product-Feature-List",
                        e = "Link click";
                    a.adobeAnalytics.events.featureListLinkClick($(this).text()), b.methods.linkClickWithNonPageReload(e, d, c)
                }), $("a#adsl-churn-reset,a#nbn-reset,a#adsl-new-reset").click(function() {
                    var c = this.id,
                        d = "SQ-Try-Another-Addr",
                        e = "Link click";
                    a.adobeAnalytics.events.tryAnotherAddressLinkClick(this.id), b.methods.linkClickWithNonPageReload(e, d, c)
                }), $(".accordian-container .expand-btn").click(function() {
                    var c = $(this);
                    $thisParent = c.closest("li");
                    var d = $(this).siblings(".headline").text();
                    if ($thisParent.hasClass("active")) {
                        var e = d,
                            f = "Product-Accordion",
                            g = "Link click";
                        a.adobeAnalytics.events.productAccordianLinkClick($(this).siblings(".headline").text()), b.methods.linkClickWithNonPageReload(g, f, e)
                    }
                });
                var c;
                c = $(".product-feature-belong-voice"), 0 == c.length && (c = $(".productFeatureBelongVoice")), d = c.find(".expandcollapse a");
                var d;
                d.click(function() {
                    var c = $(this);
                    if ($thisParent = c.closest(".expandcollapse"), $thisParent.hasClass("is-collapsed")) {
                        var d = $(".sq-container").data("sq-type");
                        d = d.toUpperCase(), console.log("--- BELONG VOICE PRODUCT FEATURE: Expand(): " + d);
                        var e = "product-feature-belong-voice",
                            f = "Product Feature Belong Voice expand - " + d,
                            g = "Expand";
                        a.adobeAnalytics.events.productFeatureBelongVoiceExpand(d), b.methods.linkClickWithNonPageReload(g, e, f)
                    }
                }), $(".tooltip-btn button").click(function() {
                    if ($("#join-product-form").hasClass("join-product-form")) {
                        var c = $(this);
                        $thisParent = c.closest("span.tooltip-btn");
                        var d = $thisParent.closest("label").attr("id");

                        if ($thisParent.hasClass("is-active")) {
                            var e = d.replace("rdo-", "Join-Product-"),
                                f = "Tooltip",
                                g = "Link click";
                            a.adobeAnalytics.events.productAccordianLinkClick($thisParent.closest("label").attr("id").replace("rdo-", "Join-Product-")), b.methods.linkClickWithNonPageReload(g, f, e)
                        }
                    } else if ($("#main").hasClass("product-page")) {
                        var c = $(this);
                        $thisParent = c.closest("span.tooltip-btn");
                        var d = $thisParent.siblings("h4").text();
                        if ($thisParent.hasClass("is-active")) {
                            var e = "Product-List-" + d,
                                f = "Tooltip",
                                g = "Link click";
                            a.adobeAnalytics.events.productAccordianLinkClick("Product-List-" + $thisParent.siblings("h4").text()), b.methods.linkClickWithNonPageReload(g, f, e)
                        }
                    }
                }), $(".footer a.facebook, .footer a.twitter").click(function(c) {
                    c.preventDefault();
                    var d = this.href,
                        e = $(this).text(),
                        f = "Outbound link click",
                        g = "Footer-Social";
                    a.adobeAnalytics.events.footerSocialLinkClick($(this).text()), b.methods.linkClickWithPageReload(d, f, g, e)
                }), $(".footer .links a").click(function(c) {
                    c.preventDefault();
                    var d = this.href,
                        e = "Link click",
                        f = $(this).text(),
                        g = "Footer-Menu";
                    a.adobeAnalytics.events.footerMenuLinkClick($(this).text()), b.methods.linkClickWithPageReload(d, e, g, f)
                })
            },
            methods: {
                getTodayDate: function() {
                    var a = new Date,
                        b = a.getDate(),
                        c = a.getMonth() + 1,
                        d = a.getFullYear();
                    return 10 > b && (b = "0" + b), 10 > c && (c = "0" + c), a = b + "/" + c + "/" + d
                },
                getNumberOfDaysBetweenTwoDates: function(a, b) {
                    var c = 864e5,
                        d = a.split("/"),
                        e = new Date(d[2], parseInt(d[1]) - 1, d[0]),
                        f = Date.parse(e),
                        g = b.split("/"),
                        h = new Date(g[2], parseInt(g[1]) - 1, g[0]),
                        i = Date.parse(h),
                        j = i - f,
                        k = Math.round(j / c);
                    return k
                },
                getRangeOfDaysBetweenTwoDates: function(a) {
                    var b;
                    return b = a >= 1 && 3 >= a ? "1-3" : a >= 4 && 6 >= a ? "4-6" : a >= 7 ? "7+" : "0"
                },
                privateBrowsing: function(a) {
                    "undefined" != typeof ga && (ga("send", "event", "Private-Browsing", "Click", a.toUpperCase() + "-Private Browsing"), ga("send", "pageview", "/virtual/private-browsing/" + a + "-private-browsing.html"))
                },
                autoPaymentPrompt: {
                    PayNowRedirect: function(a, c) {
                        var d = "Link click",
                            e = "Auto-Payment";
                        b.methods.linkClickWithPageReload(a, d, e, c)
                    }
                },
                sq: {
                    sqCheck: function(a, b) {
                        "undefined" != typeof ga && (ga("send", "event", "SQ-Check", "Click", a.toUpperCase() + "-Check if you can Belong-" + b), ga("send", "pageview", "/virtual/sq-check/" + a + "-check-if-you-can-belong-" + b + ".html"))
                    },
                    sqSuccess: function(a, b, c) {
                        "undefined" != typeof ga && (ga("send", "event", "SQ-Success", "Click", a + "-" + c + "-Check if you can Belong", {
                            dimension6: b
                        }), ga("send", "pageview", "/virtual/sq-success/" + a + "-" + c + "-check-if-you-can-belong.html", {
                            dimension6: b
                        }))
                    },
                    landlineOption: function(a, b) {
                        "undefined" != typeof ga && (ga("send", "event", "Landline", "Click", a + "-" + b), ga("send", "pageview", "/virtual/landline/" + a + "-" + b.toLowerCase() + ".html"))
                    },
                    selectAddress: function(a) {
                        "undefined" != typeof ga && (ga("send", "event", "SQ-Select-Address", "Select", "Please select an address-" + a), ga("send", "pageview", "/virtual/sq/please-select-an-address.html"))
                    },
                    findOutMoreOrJoinNow: function(a, b, c, d) {
                        "undefined" != typeof ga ? ga("send", "pageview", "/virtual/" + c.toLowerCase() + "/" + b + "-" + c.toLowerCase() + ".html", ga("send", "event", c, "Link click", b + "-" + c, {
                            dimension6: d,
                            hitCallback: function() {
                                window.location.href = a
                            }
                        })) : window.location.href = a
                    }
                },
                sqFailures: {
                    validationMessages: function(a, b) {
                        "undefined" != typeof ga && (ga("send", "event", "SQ-Validation", "Click", a + " - " + b), ga("send", "pageview", "/virtual/sq-validation/" + b + "-" + b + ".html"))
                    },
                    failureMessages: function(a, b, c) {
                        "undefined" != typeof ga && (ga("send", "event", "SQ-Failure", "Click", b + "-" + c + "-" + a), ga("send", "pageview", "/virtual/sq-failure/" + b + "-" + c + "-" + a + ".html"))
                    },
                    experionError: function(a) {
                        "undefined" != typeof ga && (ga("send", "event", "Experion-Error", "Click", a + "-Experion error"), ga("send", "pageview", "/virtual/experion-error/" + a + "-experion-error.html"))
                    }
                },
                eoi: {
                    eoiForm: function(a, b) {
                        "undefined" != typeof ga && (ga("send", "event", "EOI", "Click", a + "-" + b), ga("send", "pageview", "/virtual/eoi/" + a + "-" + b + ".html"))
                    },
                    tryAgain: function() {
                        "undefined" != typeof ga && (ga("send", "event", "EOI", "Click", "Try again"), ga("send", "pageview", "/virtual/eoi/try-again.html"))
                    },
                    keepMeInformed: function(a) {
                        "undefined" != typeof ga && (ga("send", "event", "EOI", "Click", a + "-Keep me informed"), ga("send", "pageview", "/virtual/eoi/" + a + "-keep-me-informed.html"))
                    },
                    contactMe: function(a) {
                        "undefined" != typeof ga && (ga("send", "event", "EOI", "Click", a + "-Contact me"), ga("send", "pageview", "/virtual/eoi/" + a + "contact-me.html"))
                    },
                    letMeKnow: function(a) {
                        "undefined" != typeof ga && (ga("send", "event", "EOI", "Click", a + "-Let me know"), ga("send", "pageview", "/virtual/eoi/" + a + "let-me-know.html"))
                    }
                },
                faq: {
                    productFaq: function(a, b) {
                        "undefined" != typeof ga && ga("send", "event", "productFAQ", "Click", "Product-Faq", {
                            dimension31: b + " - " + a
                        })
                    }
                },
                join: {
                    productSelection: function(a) {
                        "undefined" != typeof ga && (ga("send", "event", "Product", "Click", "Join-Product", {
                            dimension16: a
                        }), ga("send", "pageview", "/virtual/product/join-product", {
                            dimension16: a
                        }))
                    },
                    productPreSelection: function(a) {
                        "undefined" != typeof ga && (ga("send", "event", "Product", "PreSelected", "Join-Product", {
                            dimension16: a
                        }), ga("send", "pageview", "/virtual/product/preselected-join-product", {
                            dimension16: a
                        }))
                    },
                    addSpeedBoost25: function() {
                        "undefined" != typeof ga && (ga("send", "event", "SpeedBoost-Add", "Click", "Join-SB-25"), ga("send", "pageview", "/virtual/speedboost-add/join-sb-25.html"))
                    },
                    addSpeedBoost100: function() {
                        "undefined" != typeof ga && (ga("send", "event", "SpeedBoost-Add", "Click", "Join-SB-100"), ga("send", "pageview", "/virtual/speedboost-add/join-sb-100.html"))
                    },
                    addVoice: function(a) {
                        "undefined" != typeof ga && (ga("send", "event", "Voice-Add", "Click", "Join-Voice-" + a), ga("send", "pageview", "/virtual/voice-add/join-voice-" + a + ".html"))
                    },
                    checkContract: function() {
                        "undefined" != typeof ga && (ga("send", "event", "Contract", "Click", "Join-Contract"), ga("send", "pageview", "/virtual/contract/join-contract.html"))
                    },
                    promoApplied: function(a) {
                        "undefined" != typeof ga && (ga("send", "event", "Promo", "Click", "Apply", {
                            dimension14: a
                        }), ga("send", "pageview", "/virtual/Promo/apply", {
                            dimension14: a
                        }))
                    },
                    promoAppliedStaffDiscount: function(a) {
                        "undefined" != typeof ga && (ga("send", "event", "Staff Promo", "Click", "Continue", {
                            dimension14: a
                        }), ga("send", "pageview", "/virtual/staff-promo/continue", {
                            dimension14: a
                        }))
                    },
                    joinSteps: function(a, b, c) {
                        "undefined" != typeof ga && (ga("send", "event", "Join", "Click", a, {
                            dimension16: c
                        }), ga("send", "pageview", "/virtual/join/" + b, {
                            dimension16: c
                        }))
                    },
                    confirmYourOrder: function(a, c, d, e, f, g, h, i, j, k, l) {
                        var m = b.methods.getTodayDate();
                        "undefined" != typeof ga && (ga("send", "event", "Join", "Click", "4 - Confirm your Order", {
                            dimension6: c,
                            dimension13: d,
                            dimension14: e,
                            dimension16: g + "-" + f,
                            dimension17: h,
                            dimension18: i,
                            dimension19: m,
                            dimension20: j,
                            dimension22: a,
                            dimension12: k,
                            dimension28: l
                        }), ga("send", "pageview", "/virtual/join/4-confirm-your-order.html", {
                            dimension6: c,
                            dimension13: d,
                            dimension14: e,
                            dimension16: g + "-" + f,
                            dimension17: h,
                            dimension18: i,
                            dimension19: m,
                            dimension20: j,
                            dimension22: a,
                            dimension12: k,
                            dimension28: l
                        }))
                    },
                    feedback: function() {
                        "undefined" != typeof ga && (ga("send", "event", "Join", "Click", "5 - Feedback "), ga("send", "pageview", "/virtual/join/5-feedback.html"))
                    }
                },
                joinErrors: {
                    errorEmail: function(a, b) {
                        "undefined" != typeof ga && (ga("send", "event", "Join: Email Error", "Click", a + " - " + b), ga("send", "pageview", "/virtual/join-confirm-email-error/" + a + "-" + b + ".html"))
                    },
                    errorPersonalDetails: function(a) {
                        "undefined" != typeof ga && (ga("send", "event", "Join: Personal Details Error", "Click", a), ga("send", "pageview", "/virtual/join-personal-details-error/" + a + ".html"))
                    },
                    errorPayment: function(a) {
                        "undefined" != typeof ga && (ga("send", "event", "Join: Payment Error", "Click", a), ga("send", "pageview", "/virtual/join-payment-error/" + a + ".html"))
                    },
                    errorOrder: function(a) {
                        "undefined" != typeof ga && (ga("send", "event", "Join: Order Error", "Click", a), ga("send", "pageview", "/virtual/join-order-error/" + a + ".html"))
                    },
                    errorProductSelection: function(a) {
                        "undefined" != typeof ga && (ga("send", "event", "Join: Product Selection Error", "Click", a), ga("send", "pageview", "/virtual/join-product-selection-error/" + a + ".html"))
                    }
                },
                linkClickWithPageReload: function(a, b, c, d) {
                    var e = d.trim();
                    "undefined" != typeof ga ? ga("send", "event", c, b, d, ga("send", "pageview", "/virtual/" + c.toLowerCase() + "/" + e.toLowerCase() + ".html", {
                        hitCallback: function() {
                            document.location = a
                        }
                    })) : document.location = a
                },
                linkClickWithNonPageReload: function(a, b, c) {
                    "undefined" != typeof ga && ga("send", "event", b, a, c)
                },
                agent: {
                    backToMe: function() {
                        "undefined" != typeof ga && ga("send", "event", "Agent", "Click", "Change back to me")
                    },
                    mobo: function() {
                        "undefined" != typeof ga && ga("send", "event", "Agent", "Click", "Select and log in", {
                            dimension7: "Agent"
                        })
                    }
                }
            }
        }
    }()
}(BELONG),
function(a) {
    "use strict";
    a.appleSmartBanner = function() {
        var a, b, c;
        return {
            init: function() {
                a = this, b = !1, c = "", "undefined" != typeof appleSmartBannerData && "undefined" != typeof appleSmartBannerData.appleiTunesAppID && "undefined" != typeof appleSmartBannerData.isActive && (c = appleSmartBannerData.appleiTunesAppID, b = "true" == appleSmartBannerData.isActive.toString().toLowerCase() ? !0 : !1, a.loadSmartBanner())
            },
            loadSmartBanner: function() {
                1 != b || 1 != a.isAccountPage() && 1 != a.isWelcomePage() || 0 != a.isAgentOrMoboLoggedIn() || (console.log("appleSmartBanner(): loadSmartBanner()"), $("head").append('<meta name="apple-itunes-app" content="app-id=' + c + '">'))
            },
            removeSmartBanner: function() {
                $("head meta[name=apple-itunes-app]").remove()
            },
            isAccountPage: function() {
                var a = window.location.pathname.toLowerCase(),
                    b = a.indexOf("/account");
                return 0 == b || 15 == b ? !0 : !1
            },
            isWelcomePage: function() {
                var a = window.location.pathname.toLowerCase(),
                    b = a.indexOf("/welcome");
                return 0 == b || 15 == b ? !0 : !1
            },
            isAgentOrMoboLoggedIn: function() {
                var a = $("#GlobalData");
                if (a.length > 0) {
                    var b = a.data("isAgent"),
                        c = a.data("isMobo");
                    return "undefined" != typeof b && b.toString().length > 0 && "true" == b.toString().toLowerCase() ? (console.log("appleSmartBanner(): isAgent"), !0) : (b = !1, "undefined" != typeof c && c.toString().length > 0 && "true" == c.toString().toLowerCase() ? (console.log("appleSmartBanner(): isMobo"), c = !0, !0) : (c = !1, !1))
                }
            }
        }
    }()
}(BELONG),
function(a) {
    a.authenticate = function() {
        var a, b, c, d;
        return {
            init: function() {
                a = this, b = $(".logout-link.icon-btn"), c = $("#GlobalData"), d = c.data("isLoggedIn"), d ? "belong here log in" === $(".icon-btn.login").text().toLowerCase() && $(".icon-btn.login").text("Belong Here") : $(".logout-link").addClass("hidden"), b.on("click", function(b) {
                    b.preventDefault(), a.methods.logout(function(a) {
                        console.log(a), window.location.href = window.location.protocol + "//" + window.location.host + "/am/UI/Logout"
                    })
                })
            },
            methods: {
                login: function(a, b, c, d, e) {
                    return d = d || function() {}, e = e || function() {}, $.ajax({
                        url: "/am/UI/Login",
                        type: "POST",
                        data: "IDToken1=" + encodeURIComponent(a) + "&IDToken2=" + encodeURIComponent(b) + "&IDButton=Log+In&goto=" + Base64.encode(c) + "&gotoOnFail=&SunQueryParamsString=cmVhbG09L0V4dGVybmFsJg%3D%3D&encoded=true&gx_charset=UTF-8",
                        dataType: "html",
                        success: d,
                        error: e
                    })
                },
                logout: function(a, b) {
                    return a = a || function() {}, b = b || function() {}, $.ajax({
                        url: window.location.protocol + "//" + window.location.host + "/apps/logout",
                        type: "GET",
                        dataType: "html",
                        success: a,
                        error: b
                    })
                }
            }
        }
    }()
}(BELONG),
function(a) {
    "use strict";
    a.autoPaymentPrompt = function() {
        var b;
        return {
            init: function() {
                b = this, $(".auto-payment-prompt").each(function() {
                    $("#auto-payment-pay-button").off("click.payButtonClick").on("click.payButtonClick", b.events.payButtonClick), $("#auto-payment-pay-later-button").off("click.payLaterButtonClick").on("click.payLaterButtonClick", b.events.payLaterButtonClick)
                })
            },
            events: {
                payButtonClick: function(b) {
                    b.preventDefault();
                    var c = $("#auto-payment-pay-button").prop("href"),
                        d = $("#auto-payment-pay-button").text();
                    a.adobeAnalytics.pages.payment.payNowRedirect($("#auto-payment-pay-button").text()), a.analytics.methods.autoPaymentPrompt.PayNowRedirect(c, d)
                },
                payLaterButtonClick: function(b) {
                    b.preventDefault(), $.post("/bin/belong/customer/redirect/remove").done(function() {
                        location.href = "/"
                    }).fail(function() {
                        $(".auto-payment-prompt__error").removeClass("hidden")
                    }), a.adobeAnalytics.pages.payment.payLaterClick()
                }
            }
        }
    }()
}(BELONG), StringBuffer.prototype.append = function(a) {
    return this.buffer.push(a), this
}, StringBuffer.prototype.toString = function() {
    return this.buffer.join("")
};
var Base64 = {
    codex: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    encode: function(a) {
        for (var b = new StringBuffer, c = new Utf8EncodeEnumerator(a); c.moveNext();) {
            var d = c.current;
            c.moveNext();
            var e = c.current;
            c.moveNext();
            var f = c.current,
                g = d >> 2,
                h = (3 & d) << 4 | e >> 4,
                i = (15 & e) << 2 | f >> 6,
                j = 63 & f;
            isNaN(e) ? i = j = 64 : isNaN(f) && (j = 64), b.append(this.codex.charAt(g) + this.codex.charAt(h) + this.codex.charAt(i) + this.codex.charAt(j))
        }
        return b.toString()
    },
    decode: function(a) {
        for (var b = new StringBuffer, c = new Base64DecodeEnumerator(a); c.moveNext();) {
            var d = c.current;
            if (128 > d) b.append(String.fromCharCode(d));
            else if (d > 191 && 224 > d) {
                c.moveNext();
                var e = c.current;
                b.append(String.fromCharCode((31 & d) << 6 | 63 & e))
            } else {
                c.moveNext();
                var e = c.current;
                c.moveNext();
                var f = c.current;
                b.append(String.fromCharCode((15 & d) << 12 | (63 & e) << 6 | 63 & f))
            }
        }
        return b.toString()
    }
};
Utf8EncodeEnumerator.prototype = {
        current: Number.NaN,
        moveNext: function() {
            if (this._buffer.length > 0) return this.current = this._buffer.shift(), !0;
            if (this._index >= this._input.length - 1) return this.current = Number.NaN, !1;
            var a = this._input.charCodeAt(++this._index);
            return 13 == a && 10 == this._input.charCodeAt(this._index + 1) && (a = 10, this._index += 2), 128 > a ? this.current = a : a > 127 && 2048 > a ? (this.current = a >> 6 | 192, this._buffer.push(63 & a | 128)) : (this.current = a >> 12 | 224, this._buffer.push(a >> 6 & 63 | 128), this._buffer.push(63 & a | 128)), !0
        }
    }, Base64DecodeEnumerator.prototype = {
        current: 64,
        moveNext: function() {
            if (this._buffer.length > 0) return this.current = this._buffer.shift(), !0;
            if (this._index >= this._input.length - 1) return this.current = 64, !1;
            var a = Base64.codex.indexOf(this._input.charAt(++this._index)),
                b = Base64.codex.indexOf(this._input.charAt(++this._index)),
                c = Base64.codex.indexOf(this._input.charAt(++this._index)),
                d = Base64.codex.indexOf(this._input.charAt(++this._index)),
                e = a << 2 | b >> 4,
                f = (15 & b) << 4 | c >> 2,
                g = (3 & c) << 6 | d;
            return this.current = e, 64 != c && this._buffer.push(f), 64 != d && this._buffer.push(g), !0
        }
    },
    function(a) {
        "use strict";
        a.buttons = function() {
            var a;
            return {
                init: function() {
                    a = this
                },
                disableButton: function(a) {
                    var b = {
                            success: !1,
                            alreadyDisabled: !1
                        },
                        c = a.attr("disabled");
                    return "undefined" != typeof c ? (b.alreadyDisabled = !0, b.success = !0, b) : (a.attr("disabled", "disabled"), a.addClass("button-disabled"), b.alreadyDisabled = !1, b.success = !0, b)
                },
                enableButton: function(a) {
                    return a.removeAttr("disabled"), a.removeClass("button-disabled"), !0
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.expandCollapse = function() {
            var b, c, d, e, f, g = $(".lt-ie9").length > 0;
            return b = {
                IS_EXPANDED_CLASS: "is-expanded",
                IS_COLLAPSED_CLASS: "is-collapsed",
                IS_DISABLED_CLASS: "is-disabled"
            }, c = {
                reset: function(a) {
                    a.css({
                        display: "",
                        opacity: ""
                    })
                },
                initClose: function(a) {
                    g ? a.hide() : a.css({
                        opacity: 0
                    }, 1).hide()
                },
                open: function(b, c, d, e) {
                    e = "boolean" == typeof e ? e : !1, b.is(":visible") || (g || e ? (b.show().css({
                        opacity: ""
                    }).data("ec-isAnimating", !1), c.trigger("opened.expandcollapse"), "function" == typeof d && d()) : b.css({
                        opacity: 0
                    }).hide().velocity("stop").velocity("slideDown", {
                        duration: 250,
                        easing: "ease-out",
                        complete: function() {
                            b.velocity({
                                opacity: 1
                            }, {
                                duration: 200,
                                easing: "ease-out",
                                complete: function() {
                                    b.data("ec-isAnimating", !1), c.trigger("opened.expandcollapse"), "function" == typeof d && d()
                                }
                            })
                        }
                    }), setTimeout(function() {
                        var b, c = $("#address-check");
                        0 !== c.length && (b = c.offset().top - 60, a.util.scrollPageTo(b))
                    }, 100))
                },
                close: function(a, b, c, d) {
                    d = "boolean" == typeof d ? d : !1, g || d ? (a.hide().data("ec-isAnimating", !1), b.trigger("closed.expandcollapse"), "function" == typeof c && c()) : a.velocity("stop").velocity("scroll", {
                        offset: -300,
                        duration: 250
                    }).velocity("slideUp", {
                        duration: 500,
                        easing: "ease-out",
                        complete: function() {
                            a.data("ec-isAnimating", !1), b.trigger("closed.expandcollapse"), "function" == typeof c && c()
                        }
                    })
                }
            }, d = function(d) {
                var e = d ? $(".expandcollapse:not(.js-expandcollapse)", d) : $(".expandcollapse:not(.js-expandcollapse)");
                e.each(function(d, e) {
                    var f = $(e).addClass("js-expandcollapse"),
                        g = f.find("a"),
                        h = g.attr("data-ec-expand"),
                        i = g.attr("data-ec-collapse"),
                        j = g.attr("data-ec-group") || !1,
                        k = g.attr("data-eq-scrollat-mq") || !1,
                        l = "#" + g.attr("href").replace(/[\w\W]*#([\w\W]+)/g, "$1"),
                        m = $(l),
                        n = !1;
                    if ($(".lt-ie8").length > 0) return void g.hide();
                    var o = {
                        enable: function() {
                            g.attr("aria-controls", l.substring(1)), m.attr({
                                role: "region",
                                tabindex: "-1",
                                "aria-expanded": f.hasClass(b.IS_EXPANDED_CLASS)
                            })
                        },
                        disable: function() {
                            g.removeAttr("aria-controls"), m.removeAttr("role").removeAttr("tabindex").removeAttr("aria-expanded")
                        },
                        update: function() {
                            n || m.attr({
                                "aria-expanded": f.hasClass(b.IS_EXPANDED_CLASS)
                            })
                        }
                    };
                    f.data("ec-group", j);
                    var p = {
                        toExpand: function() {
                            f.removeClass(b.IS_EXPANDED_CLASS).addClass(b.IS_COLLAPSED_CLASS), g.html(h), o.update()
                        },
                        toCollapse: function() {
                            f.removeClass(b.IS_COLLAPSED_CLASS).addClass(b.IS_EXPANDED_CLASS), g.html(i), o.update()
                        }
                    };
                    if (m.data("init") !== !0 && m.length > 0) {
                        var q, r, s;
                        m.data("ec-isAnimating", !1), f.hasClass(b.IS_COLLAPSED_CLASS) && c.initClose(m), q = function(d, e) {
                            e = "boolean" == typeof e ? e : !1;
                            var g, h;
                            if (g = function(a) {
                                    return n ? (m.data("ec-isAnimating", !1), void p.toCollapse()) : void(f.hasClass(b.IS_EXPANDED_CLASS) || (p.toCollapse(), c.open(m, f, a)))
                                }, h = function() {
                                    k === !1 || "string" != typeof DD.bp.get(k) ? (a.util.scrollPageTo(f.offset().top, null, 250), g()) : DD.bp.is(k) ? (a.util.scrollPageTo(f.offset().top, null, 250), g()) : g()
                                }, j) {
                                var i = !1;
                                $(".js-expandcollapse." + b.IS_EXPANDED_CLASS).each(function(a, b) {
                                    $(b).data("ec-group") === j && (i = !0, $(b).trigger("close.expandcollapse", h))
                                }), i || (e ? h() : g())
                            } else g()
                        }, r = function(a, d) {
                            return n ? (m.data("ec-isAnimating", !1), p.toExpand(), void("function" == typeof d && d())) : void(f.hasClass(b.IS_COLLAPSED_CLASS) || (p.toExpand(), c.close(m, f, d)))
                        }, s = function(a) {
                            a.preventDefault(), n || m.data("ec-isAnimating") === !1 && (m.data("ec-isAnimating", !0), f.trigger(f.hasClass(b.IS_EXPANDED_CLASS) ? "close.expandcollapse" : "open.expandcollapse"))
                        }, f.on("open.expandcollapse", q).on("close.expandcollapse", r).on("click.expandcollapse", s), f.on("reinit.expandcollapse", function(a) {
                            a.stopPropagation(), g = f.find("a");
                            var b = "#" + g.attr("href").replace(/[\w\W]*#([\w\W]+)/g, "$1");
                            m = $(b), m.data("ec-isAnimating", !1).data("init", !0), f.off("expandcollapse").on("open.expandcollapse", q).on("close.expandcollapse", r).on("click.expandcollapse", s)
                        }), f.on("attach.mqad", function(a) {
                            a.stopPropagation(), n = !1, f.hasClass(b.IS_COLLAPSED_CLASS) && c.initClose(m), f.removeClass(b.IS_DISABLED_CLASS), o.enable()
                        }), f.on("detach.mqad", function(a) {
                            a.stopPropagation(), n = !0, c.reset(m), f.addClass(b.IS_DISABLED_CLASS), o.disable()
                        }), o.enable(), m.data("init", !0)
                    }
                })
            }, e = function(a) {
                var b = a ? $(".expandcollapse", a) : $(".expandcollapse");
                b.trigger("reinit.expandcollapse")
            }, f = function(a, b, c) {
                var d = "string" == typeof c,
                    e = "boolean" == typeof b ? b : !0;
                a.find(".expandcollapse").each(function(a, b) {
                    var f = $(b);
                    if (f.removeClass("js-expandcollapse"), e ? f.removeClass("is-collapsed").addClass("is-expanded") : f.removeClass("is-expanded").addClass("is-collapsed"), d) {
                        var g = f.find("a");
                        g.attr("href", g.attr("href") + c);
                        var h = f.next(".ec-content");
                        h.attr("id", h.attr("id") + c), h.data("init", !1)
                    }
                })
            }, {
                init: d,
                update: e,
                reset: f,
                CONST: b
            }
        }()
    }(BELONG),
    function(a) {
        a.faqAccordian = function() {
            var b, c = $(".faq-accordian-header"),
                d = $(".faq-accordian-content"),
                e = $(".faq-accordian-content p"),
                f = ($(".faq-accordian-header span"), $("#header").data("page-sub-type"));
            c.addClass("is-inactive faq-accordian-icon-plus"), $(c).click(function() {
                $(this).hasClass("is-active") ? (e.hide(), $(this).next(d).velocity("slideUp", {
                    delay: 20,
                    duration: 200
                }, {
                    easing: "easeInSine"
                }), $(".faq-accordian-content p").velocity("fadeOut", {
                    delay: 200,
                    duration: 700
                }), $(this).removeClass("is-active faq-accordian-icon-minus"), $(this).addClass("is-inactive faq-accordian-icon-plus")) : (e.hide(), $(c).not(this).next(d).velocity("slideUp", {
                    delay: 20,
                    duration: 400
                }), $(c).not(this).removeClass("is-active faq-accordian-icon-minus"), $(c).not(this).addClass("is-inactive faq-accordian-icon-plus"), $(this).next(d).velocity("slideDown", {
                    delay: 20,
                    duration: 250
                }, {
                    easing: "easeInSine"
                }), $(".faq-accordian-content p").velocity("fadeIn", {
                    delay: 210,
                    duration: 700
                }), $(this).addClass("is-active faq-accordian-icon-minus"), $(this).removeClass("is-inactive faq-accordian-icon-plus"), b = $(this).find(".faq-accordian-heading").text(), a.analytics.methods.faq.productFaq(b, f), a.adobeAnalytics.pages.product.faq(b, f))
            })
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.flexslider = function() {
            $(".flexslider").each(function() {
                var b = $(this);
                b.find(".slide-inner img").each(function() {
                    $(this).data("link-flag") && ($(this).parent().wrap('<a href="' + $(this).data("href") + '">'), $(this).parent().closest("a").off("click.scrollToAnchor").on("click.scrollToAnchor", a.scrollToAnchor.events.scrollToAnchor))
                })
            }), $(".js-flexslider").each(function() {
                var a = $(this),
                    b = $(".slides > .slide").length,
                    c = !1,
                    d = !0;
                a.data("autoScroll") && (c = !0), 1 === b && (d = !1, c = !1), a.data("bannerGradient") && a.find(".banner-gradient").removeClass("hidden"), a.flexslider({
                    NAMESPACE: "flex-",
                    selector: ".slides > .slide",
                    animation: "slide",
                    easing: "swing",
                    direction: "horizontal",
                    reverse: !1,
                    animationLoop: d,
                    smoothHeight: !0,
                    startAt: 0,
                    slideshow: c,
                    slideshowSpeed: a.data("scrollSpeed"),
                    animationSpeed: 500,
                    initDelay: a.data("scrollSpeed"),
                    randomize: !1,
                    pauseOnAction: !0,
                    pauseOnHover: !0,
                    useCSS: !0,
                    touch: d,
                    video: !1,
                    controlNav: !0,
                    directionNav: d,
                    prevText: "Previous",
                    nextText: "Next",
                    keyboard: !0,
                    multipleKeyboard: !1,
                    mousewheel: !1,
                    pausePlay: !1,
                    pauseText: "Pause",
                    playText: "Play",
                    controlsContainer: "",
                    manualControls: "",
                    sync: "",
                    asNavFor: "",
                    itemWidth: 0,
                    itemMargin: 0,
                    minItems: 0,
                    maxItems: 0,
                    move: 0,
                    start: function() {
                        a.removeClass("loading")
                    },
                    before: function() {},
                    after: function() {},
                    end: function() {},
                    added: function() {},
                    removed: function() {}
                })
            })
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.forms = a.forms || {}, a.forms.setInputValue = function(a, b) {
            var c;
            c = "string" == typeof a ? $(a) : a, c.val(b), c.trigger("change")
        }, a.forms.applyShiftLabelHandler = function(a) {
            $(a).closest(".ctrl-holder").addClass("shift-label").addClass("shift-focussed")
        }, a.forms.unShiftLabelHandler = function(a) {
            var b = $(a);
            "" === a.value && b.closest(".ctrl-holder").removeClass("shift-label"), b.closest(".ctrl-holder").removeClass("shift-focussed")
        }, a.forms.applyShiftLabel = function() {
            var b = $(".ctrl-holder").not(".disable-shift-label").find('input[type="text"], input[type="password"], input[type="email"], input[type="tel"], textarea');
            b.on("focus.applyShiftLabel", function() {
                a.forms.applyShiftLabelHandler(this)
            }).on("blur.applyShiftLabel", function() {
                a.forms.unShiftLabelHandler(this)
            }).on("change.applyShiftLabel", function() {
                a.forms.applyShiftLabelHandler(this), a.forms.unShiftLabelHandler(this)
            }), a.forms.initShiftLabel(b)
        }, a.forms.initShiftLabel = function(a) {
            var b = $("select");
            a.each(function() {
                var a = $(this);
                "" !== this.value ? a.closest(".ctrl-holder").addClass("shift-label") : a.closest(".ctrl-holder").removeClass("shift-label")
            }), b.on("change", function() {
                var a = $(this),
                    b = $(this).find("option:selected").text();
                "" !== b ? a.closest(".ctrl-holder").addClass("shift-label") : a.closest(".ctrl-holder").removeClass("shift-label")
            }).change()
        }, a.forms.simpleSelect = function() {
            return $.simpleSelect ? void(0 === $(".lt-ie7").length && $("select").simpleSelect()) : void console.warn("The jQuery Plugin simpleSelect is required.")
        }, a.forms.styledRadio = function() {
            var b = $(".styled-radio");
            b.each(function() {
                var b = this;
                b.items = $(b).find("li"), b.items.on("click", function() {
                    var c = $(this).find("input");
                    b.items.removeClass("selected"), $(this).addClass("selected"), c.trigger("change");
                    var c = $(this).find("input"),
                        d = "contract",
                        e = "non-contract";
                    return c.val() != e ? (a.product.set.planSwitchOn(), !1) : c.val() != d ? (a.product.set.planSwitchOff(), !1) : void 0
                })
            })
        }, a.forms.anchorButtons = function() {
            $('a[role="button"]').on("keyup", function(a) {
                32 === a.which && this.click()
            })
        }, a.forms.applyAutoGrowTextarea = function() {
            var b = $(".f-textarea").not(".disable-autogrow").find("textarea");
            b.on("keyup.autogrowtextarea", function() {
                a.forms.calculateAutoGrowTextArea($(this))
            }), a.forms.initAutoGrowTextarea(b)
        }, a.forms.calculateAutoGrowTextArea = function(a) {
            a.each(function() {
                var a = $(this).val(),
                    b = $(this).closest(".f-textarea").find(".autogrowtextarea");
                a = a.replace(/\n/g, "<br>"), b.html(a + "<br>"), $(this).css("height", b.outerHeight() + 1)
            })
        }, a.forms.initAutoGrowTextarea = function(a) {
            a.each(function() {
                var a = $(this);
                0 === a.closest(".f-textarea").find(".autogrowtextarea").length && a.closest(".f-textarea").append('<div class="autogrowtextarea ' + a.attr("id") + '"></div>')
            })
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.headerNav = function() {
            var b, c, d, e;
            return {
                init: function() {
                    b = this, c = $("#header"), d = $(".navigation-list li"), e = $("#header").data("pageType"), b.methods.currentPage(), b.methods.showPageLinks(), $(window).resize(function() {
                        if ("none" === $("#header-call").css("display") && 0 === $(".desktop-call").length) {
                            var a = !0;
                            if (0 == a) $(".secondary-navigation").prepend('<li class="desktop-call desktop-call-visible"><a href="#" onclick="return false;">1300 235 664</a></li>');
                            else {
                                var b = "";
                                b += '<li class="desktop-call desktop-phone-reveal">', b += '  <a id="header-call-icon" href="#" class="call">', b += '    <span class="vh">Call</span>', b += "  </a>", b += '  <span class="desktop-phone hidden">', b += '    <a id="header-call" href="#" onclick="return false;">1300 235 664</a>', b += "</span>", b += "</li>", $(".secondary-navigation").prepend(b);
                                var c = $(".secondary-navigation li.desktop-call a#header-call-icon"),
                                    d = $(".secondary-navigation li.desktop-call span.desktop-phone");
                                c.off("click").on("click", function() {
                                    return c.addClass("hidden"), d.removeClass("hidden"), BELONG.adobeAnalytics.global.revealPhoneNumber(window.location.pathname), !1
                                })
                            }
                        }
                    }), $(window).trigger("resize")
                },
                methods: {
                    currentPage: function() {
                        d.each(function() {
                            var a = $(this);
                            window.location.pathname === a.find("a").attr("href") && a.addClass("current-page")
                        })
                    },
                    showPageLinks: function() {
                        d.each(function() {
                            var b = $(this),
                                c = b.find("a"),
                                d = c.data() || {},
                                f = !0;
                            switch (e) {
                                case "join":
                                    d.join || (f = !1)
                            }
                            a.GD.isAgent && !d.agent && (f = !1), a.GD.isMobo && !d.agentMobo && (f = !1), a.GD.isCustomer && !d.customer && (f = !1), a.GD.isLoggedIn && !d.auth && (f = !1), a.GD.isLoggedIn || d.unauth || (f = !1), a.GD.isAgent && d.unauth && (f = !0), f && b.removeClass("hidden")
                        }), a.GD.isLoggedIn && c.find(".authenticated-menu").removeClass("hidden").children("a").off("click.authHeaderToggle").on("click.authHeaderToggle", b.events.authHeaderToggle).find(".user-first-name").text(a.GD.loginFirstname)
                    }
                },
                events: {
                    authHeaderToggle: function(a) {
                        a.preventDefault();
                        var b = $(this).next();
                        b.toggleClass("hidden")
                    }
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.iconCarousel = function() {
            $(".icon-carousel").each(function() {
                var b, c, d, e, f, g, h = $(this);
                c = function() {
                    return b = h.find(".slide").length, a.IS_RESPONSIVE ? window.matchMedia(DD.bp.get("l")).matches ? b : window.matchMedia(DD.bp.get("xs")).matches ? 2 : 1 : b
                }, d = function() {
                    return c()
                }, e = function() {
                    return h.width() / c()
                }, f = {
                    NAMESPACE: "flex--",
                    minItems: d(),
                    maxItems: d(),
                    itemWidth: e(),
                    animationLoop: !1,
                    animation: "slide",
                    selector: ".slides > .slide",
                    start: function(b) {
                        a.storedSlider = b, h.resize()
                    }
                }, h.flexslider(f), g = function() {
                    var b = d(),
                        c = e();
                    a.storedSlider && (a.storedSlider.vars.minItems = b, a.storedSlider.vars.maxItems = b, a.storedSlider.vars.itemWidth = c, a.storedSlider.slides.length <= b ? h.find(".flex-direction-nav").addClass("hidden") : h.find(".flex-direction-nav").removeClass("hidden"))
                }, $(window).on("resize.iconCarousel", $.debounce(100, g))
            })
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.infiniteCycle = function() {
            return {
                create: function(b, c, d) {
                    var e = d || 0,
                        f = b.find(".cycle-item").length;
                    c = c || 6e3, b.find(".cycle-item").eq(e).removeClass("hidden"), b.removeClass("hidden"), a.INFINITELOOP = setInterval(function() {
                        b.find(".cycle-item").eq(e).addClass("hidden"), e === f - 1 ? e = 0 : e++, b.find(".cycle-item").eq(e).removeClass("hidden"), a.belongModal.setHeights({
                            modalContainer: $("#mdl-sq"),
                            noShadow: !0
                        })
                    }, c)
                },
                destroy: function(b) {
                    clearInterval(a.INFINITELOOP), b.addClass("hidden"), b.find(".cycle-item").addClass("hidden")
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.livePerson = function() {
            var b, c, d = "",
                e = !1;
            return {
                init: function() {
                    b = this, b.loadLivePersonMonitorCode();
                    var a = b.loadPageSections();
                    0 == a && console.error("LE20: NO SECTIONS LOADED!"), c = $("#header").data("pageType"), c = "undefined" != typeof c && null != c ? c.toLowerCase() : "", b.lpDebugOut("pageDefinitionType=" + c), b.testCurrentPage()
                },
                loadLivePersonMonitorCode: function() {
                    switch (livePersonSettingsData.isDisabled) {
                        case "true":
                            e = !0, b.lpDebugOut("livePersonSettingsData.isDisabled: TRUE");
                            break;
                        default:
                            b.lpDebugOut("livePersonSettingsData.isDisabled: FALSE")
                    }
                    switch (window.location.host) {
                        case "www.belong.com.au":
                        case "belong.com.au":
                            b.lpDebugOut("prod: " + window.location.host), d = "83898727";
                            break;
                        case "preprod.belong.com.au":
                            b.lpDebugOut("pre-prod: " + window.location.host), d = "64328663";
                            break;
                        default:
                            b.lpDebugOut("test: " + window.location.host), d = "64328663"
                    }
                    0 == b.IsAgentLoggedIn() ? b.lpDebugOut("Connecting to LP. Site code: " + d) : (b.lpDebugOut("Agent logged in. Not loading LP."), e = !0), 0 == e ? (b.lpDebugOut("Loading LivePerson."), window.lpTag = window.lpTag || {}, "undefined" == typeof window.lpTag._tagCount ? (window.lpTag = {
                        site: d,
                        section: lpTag.section || "",
                        autoStart: lpTag.autoStart === !1 ? !1 : !0,
                        ovr: lpTag.ovr || {},
                        _v: "1.5.1",
                        _tagCount: 1,
                        protocol: location.protocol,
                        events: {
                            bind: function(a, b, c) {
                                lpTag.defer(function() {
                                    lpTag.events.bind(a, b, c)
                                }, 0)
                            },
                            trigger: function(a, b, c) {
                                lpTag.defer(function() {
                                    lpTag.events.trigger(a, b, c)
                                }, 1)
                            }
                        },
                        defer: function(a, b) {
                            0 == b ? (this._defB = this._defB || [], this._defB.push(a)) : 1 == b ? (this._defT = this._defT || [], this._defT.push(a)) : (this._defL = this._defL || [], this._defL.push(a))
                        },
                        load: function(a, b, c) {
                            var d = this;
                            setTimeout(function() {
                                d._load(a, b, c)
                            }, 0)
                        },
                        _load: function(a, b, c) {
                            var d = a;
                            a || (d = this.protocol + "//" + (this.ovr && this.ovr.domain ? this.ovr.domain : "lptag.liveperson.net") + "/tag/tag.js?site=" + this.site);
                            var e = document.createElement("script");
                            e.setAttribute("charset", b ? b : "UTF-8"), c && e.setAttribute("id", c), e.setAttribute("src", d), document.getElementsByTagName("head").item(0).appendChild(e)
                        },
                        init: function() {
                            this._timing = this._timing || {}, this._timing.start = (new Date).getTime();
                            var a = this;
                            window.attachEvent ? window.attachEvent("onload", function() {
                                a._domReady("domReady")
                            }) : (window.addEventListener("DOMContentLoaded", function() {
                                a._domReady("contReady")
                            }, !1), window.addEventListener("load", function() {
                                a._domReady("domReady")
                            }, !1)), "undefined" == typeof window._lptStop && this.load()
                        },
                        start: function() {
                            this.autoStart = !0
                        },
                        _domReady: function(a) {
                            this.isDom || (this.isDom = !0, this.events.trigger("LPT", "DOM_READY", {
                                t: a
                            })), this._timing[a] = (new Date).getTime()
                        },
                        vars: lpTag.vars || [],
                        dbs: lpTag.dbs || [],
                        ctn: lpTag.ctn || [],
                        sdes: lpTag.sdes || [],
                        ev: lpTag.ev || []
                    }, lpTag.init()) : window.lpTag._tagCount += 1, lpTag.sdes = lpTag.sdes || []) : (b.lpDebugOut("LivePerson disabled."), window.lpTag = window.lpTag || {}, lpTag.sdes = lpTag.sdes || [])
                },
                IsAgentLoggedIn: function() {
                    var a = document.getElementById("GlobalData");
                    return null != a ? "true" == a.getAttribute("data-is-agent") ? (b.lpDebugOut("Agent logged in."), !0) : (b.lpDebugOut("Not an agent."), !1) : (b.lpDebugOut("Missing GlobalData"), !1)
                },
                isSessionExclusionSet: function() {
                    var a = sessionStorage.getItem("le20ExcludeSession");
                    return a ? !0 : !1
                },
                setSessionExclusionOn: function() {
                    sessionStorage.setItem("le20ExcludeSession", "true")
                },
                loadPageSections: function() {
                    var a = !1,
                        c = [],
                        d = !1;
                    if (lpSection = lpSection.trim(), "" != lpSection) {
                        c = lpSection.split(",");
                        for (var e = 0; e < c.length; e++) c[e] = c[e].trim(), "" == c[e] && c.splice(e, 1), "no-session-chat" == c[e].toLowerCase() && (d = !0)
                    }
                    return 1 == d ? b.setSessionExclusionOn() : b.isSessionExclusionSet() && (c = [], c.push("no-session-chat")), b.setSectionFromArray(c), c.length > 0 ? (b.lpDebugOut("loadPageSections(): Sections loaded [" + c.toString() + "]"), a = !0) : (b.lpDebugOut("loadPageSections(): No Section loaded"), a = !1), a
                },
                testCurrentPage: function() {
                    1 == b.isJoinPage() && b.join.pageEntry()
                },
                isJoinPage: function() {
                    return "join" == c ? !0 : !1
                },
                lpDebugOut: function(a) {
                    console.log("LE2.0: " + JSON.stringify(a))
                },
                setSection: function(a, c, d, f) {
                    if (1 != e && "undefined" != typeof lpTag) {
                        var g = [a, c];
                        "undefined" != typeof d && g.push(d), "undefined" != typeof f && g.push(f), lpTag.section = g, b.lpDebugOut("setSection(): lpTag.section = " + JSON.stringify(lpTag.section))
                    }
                },
                setSectionFromArray: function(a) {
                    1 != e && "undefined" != typeof lpTag && (lpTag.section = a, b.lpDebugOut("setSectionFromArray(): lpTag.section = " + JSON.stringify(lpTag.section)))
                },
                updateSdesArray: function(a, c) {
                    if (1 != e)
                        if ("undefined" == typeof c || null === c || 0 == c) lpTag.sdes.push(a), b.lpDebugOut("updateSdesArray(PUSH): dataObject=" + JSON.stringify(a));
                        else try {
                            lpTag.sdes.send(a), b.lpDebugOut("updateSdesArray(SEND OK): dataObject=" + JSON.stringify(a))
                        } catch (c) {
                            lpTag.sdes.push(a), b.lpDebugOut("updateSdesArray(SEND FAIL): ex=" + JSON.stringify(c) + ", dataObject=" + JSON.stringify(a))
                        }
                },
                sq: {
                    startSQ: function() {
                        console.log("SQ.startSQ()")
                    },
                    completeSQ: function() {
                        b.lpDebugOut("SQ.completeSQ()")
                    },
                    invalidPhoneError: function() {
                        b.lpDebugOut("SQ.invalidPhoneError()");
                        var a = {
                            type: "error",
                            error: {
                                message: "Phone number does not match address in SQ.",
                                code: "sq-invalid-phone-error"
                            }
                        };
                        b.updateSdesArray(a, !0)
                    },
                    addressNotFound: function() {
                        b.lpDebugOut("SQ.addressNotFound()");
                        var a = {
                            type: "error",
                            error: {
                                message: "Address not found in SQ",
                                code: "sq-address-not-found"
                            }
                        };
                        b.updateSdesArray(a, !0)
                    },
                    cannotBelongError: function() {
                        b.lpDebugOut("SQ.cannotBelongError()");
                        var a = {
                            type: "error",
                            error: {
                                message: "Service not available at address.",
                                code: "sq-cannot-belong"
                            }
                        };
                        b.updateSdesArray(a, !0)
                    },
                    sqError: function() {
                        b.lpDebugOut("SQ.sqError()");
                        var a = {
                            type: "error",
                            error: {
                                message: "Error occurred in SQ.",
                                code: "sq-error"
                            }
                        };
                        b.updateSdesArray(a, !0)
                    }
                },
                productPages: {
                    productBuilderContinue: function(a, c, d, e, f, g, h) {
                        var i = [];
                        i = b.helpers.getProductArrayFromProductModel(a, c, d, e, f, g);
                        var j = {
                            type: "cart",
                            total: h,
                            products: i
                        };
                        b.updateSdesArray(j, !0)
                    }
                },
                join: {
                    pageEntry: function(c, d) {
                        if ("undefined" == typeof c && (c = null), "undefined" == typeof d && (d = null), null == c && null == d) {
                            c = a.util.loadPageVar("planType"), d = a.util.loadPageVar("planSize");
                            var e = sessionStorage.getItem("planType"),
                                f = sessionStorage.getItem("planSize");
                            b.lpDebugOut("join.pageEntry(): planType=" + c), b.lpDebugOut("join.pageEntry(): productSize=" + d), b.lpDebugOut("join.pageEntry(): sessionPlanType=" + e), b.lpDebugOut("join.pageEntry(): sessionPlanSize=" + f), c = c || e, d = d || f, null == c && (c = ""), null == d && (d = "")
                        }
                        var g = "";
                        if ("" != c && (g = d.toLowerCase() + "-" + c.toLowerCase()), "" != g) {
                            var h = {
                                type: "prodView",
                                products: [{
                                    product: {
                                        name: g,
                                        category: ""
                                    }
                                }]
                            };
                            b.updateSdesArray(h, !0)
                        }
                    },
                    sqContinue: function(a, c) {
                        b.lpDebugOut("join.sqContinue('" + a + "','" + c + "')");
                        var d = "";
                        if ("" != a && (d = c.toLowerCase() + "-" + a.toLowerCase()), "" != d) {
                            var e = {
                                type: "prodView",
                                products: [{
                                    product: {
                                        name: d,
                                        category: ""
                                    }
                                }]
                            };
                            b.updateSdesArray(e, !0)
                        }
                    },
                    productContinue: function(a) {
                        b.lpDebugOut("join.productContinue()");
                        var c = [];
                        c = b.helpers.getProductArrayFromFormModel(a);
                        var d = {
                            type: "cart",
                            total: a.total_minimum_cost,
                            products: c
                        };
                        b.updateSdesArray(d, !0)
                    },
                    confirmOrder: function(a, c) {
                        var d;
                        d = b.helpers.getProductArrayFromFormModel(a);
                        var e = {
                            type: "purchase",
                            total: a.total_minimum_cost,
                            orderId: c,
                            cart: {
                                products: d
                            }
                        };
                        b.updateSdesArray(e, !0), e = {
                            type: "service",
                            service: {
                                status: 2
                            }
                        }, b.updateSdesArray(e, !0)
                    }
                },
                helpers: {
                    getProductArrayFromFormModel: function(a) {
                        var b = [],
                            c = {
                                product: {
                                    name: a.charges_title,
                                    category: "Broadband",
                                    price: a.monthly_price
                                },
                                quantity: 1
                            };
                        b.push(c);
                        var d = {
                            product: {
                                name: 1 == a.withContract ? "Contract" : "Month",
                                category: "Broadband"
                            },
                            quantity: 1
                        };
                        b.push(d);
                        var e = {
                            product: {
                                name: "Modem",
                                category: "Modem",
                                price: a.modem_fee
                            },
                            quantity: 1
                        };
                        if (b.push(e), 1 == a.speedboost) {
                            var f = {
                                product: {
                                    name: "Speedbost: " + a.planSpeed,
                                    category: "Speedboost"
                                },
                                quantity: 1
                            };
                            b.push(f)
                        }
                        if (1 == a.voice_national) {
                            var g = {
                                product: {
                                    name: "National Voice",
                                    category: "Voice",
                                    price: a.voice_national_cost
                                },
                                quantity: 1
                            };
                            b.push(g)
                        }
                        if (1 == a.voice_international) {
                            var h = {
                                product: {
                                    name: "International Voice",
                                    category: "Voice",
                                    price: a.voice_international_cost
                                },
                                quantity: 1
                            };
                            b.push(h)
                        }
                        return b
                    },
                    getProductArrayFromProductModel: function(a, b, c, d, e, f) {
                        var g = [],
                            h = {
                                product: {
                                    name: a,
                                    category: "Broadband",
                                    price: b
                                },
                                quantity: 1
                            };
                        g.push(h);
                        var i = {
                            product: {
                                name: c,
                                category: "Broadband"
                            },
                            quantity: 1
                        };
                        if (g.push(i), "undefined" != typeof d && "" != d) {
                            var j = {
                                product: {
                                    name: "Speedbost: " + d,
                                    category: "Speedboost"
                                },
                                quantity: 1
                            };
                            g.push(j)
                        }
                        if ("NATIONAL" == e) {
                            var k = {
                                product: {
                                    name: "National Voice",
                                    category: "Voice",
                                    price: f
                                },
                                quantity: 1
                            };
                            g.push(k)
                        } else if ("INTERNATIONAL" == e) {
                            var l = {
                                product: {
                                    name: "International Voice",
                                    category: "Voice",
                                    price: f
                                },
                                quantity: 1
                            };
                            g.push(l)
                        } else if ("BOTH" == e) {
                            var l = {
                                product: {
                                    name: "National & International Voice",
                                    category: "Voice",
                                    price: f
                                },
                                quantity: 1
                            };
                            g.push(l)
                        }
                        return g
                    }
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.loaders = function() {
            var a;
            return {
                init: function() {
                    a = this
                },
                applyButtonLoader: function(a, b) {
                    b = "undefined" == typeof b ? "replace" : "replace" == b.toLowerCase() ? "replace" : "append" == b.toLowerCase() ? "append" : "replace", a.data("buttonHtml", a.html());
                    var c;
                    c = a.width(), a.width(parseInt(c) + 35 + "px");
                    var d = "";
                    return "replace" == b ? (console.log("applyButtonLoader(): Replace: " + d), d = '<span class="button-loader-replace"></span>', a.html(d)) : "append" == b && (d = '<span class="button-loader-append"></span>', a.append(d)), !0
                },
                removeButtonLoader: function(a) {
                    var b = a.data("buttonHtml");
                    a.html(b);
                    var c;
                    return c = a.width(), console.log("currentButtonWidth: " + c), a.width(parseInt(c) - 35 + "px"), !0
                }
            }
        }()
    }(BELONG),
    function(a) {
        a.messageCycle = function() {
            var a, b, c, d, e, f, g, h, i, j;
            return i = function(a) {
                a.each(function() {
                    $(this).removeClass("visuallyhidden"), $(this).attr("data-width", $(this).width()), $(this).addClass("visuallyhidden")
                })
            }, j = function(a) {
                b.css("max-width", parseInt(a.attr("data-width")) + 101 + "px")
            }, {
                init: function() {
                    a = $("ul.cycle"), b = $(".search-btn"), d = b.find("span"), e = a.children().length, g = 1e3 * parseInt(a.attr("data")), h = 1, a.children("li").each(function() {
                        var a = $('<span class="cycle cycleHide"/>');
                        a.text($(this).text()), a.appendTo(b), a.addClass("visuallyhidden")
                    }), c = b.children("span.cycle")
                },
                cycle: function() {
                    i(c), j(c.eq(0)), d.addClass("visuallyhidden"), h = 1, c.eq(0).removeClass("cycleHide"), c.eq(0).removeClass("visuallyhidden"), f = setInterval(function() {
                        var a = c.eq(h - 1),
                            b = c.eq(h);
                        0 !== h && (a.addClass("cycleHide"), b.removeClass("visuallyhidden")), setTimeout(function() {
                            a.addClass("visuallyhidden"), b.removeClass("cycleHide"), j(b), h += 1, e - 1 >= h ? setTimeout(function() {
                                c.not(b).addClass("cycleHide")
                            }, 200) : clearInterval(f)
                        }, 1e3)
                    }, g)
                },
                cancel: function() {
                    for (var a = 0; e > a; a += 1) c.eq(a).addClass("cycleHide").addClass("visuallyhidden"), d.removeClass("visuallyhidden");
                    clearInterval(f)
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.mqad = function() {
            var b, c;
            return b = function(b) {
                var c = "boolean" == typeof b ? b : !1;
                $(".fn_mqad").each(function(b, d) {
                    var e, f, g = $(d),
                        h = g.attr("data-mqad-attach") || !1,
                        i = g.attr("data-mqad-detach") || !1,
                        j = g.attr("data-mqad-static") || null;
                    if (e = function(a) {
                            var b = "boolean" == typeof a ? a : !1;
                            g.trigger("attach.mqad", [b])
                        }, f = function(a) {
                            var b = "boolean" == typeof a ? a : !1;
                            g.trigger("detach.mqad", [b])
                        }, a.IS_RESPONSIVE === !1) {
                        if (null !== j) return void("true" === j ? e(!0) : "false" === j && f(!0));
                        c = !0
                    }(h !== !1 || i !== !1) && (h !== !1 && i !== !1 ? (c !== !0 && enquire.register(DD.bp.get(h), {
                        match: e
                    }).register(DD.bp.get(i), {
                        match: f
                    }), DD.bp.is(h) && e(!0), DD.bp.is(i) && f(!0)) : (c !== !0 && enquire.register(DD.bp.get(h ? h : i), {
                        match: h ? e : f,
                        unmatch: h ? f : e
                    }), DD.bp.is(h) ? e(!0) : f(!0)))
                })
            }, c = function() {
                b(!0)
            }, {
                init: b,
                trigger: c
            }
        }()
    }(BELONG),
    function(a) {
        a.responseMessage = function() {
            return {
                getValidationMessage: function(a, b, c) {
                    var d = $("#" + a + "-response-messages .fed_val-" + b + "-" + c),
                        e = "";
                    return e = d.length > 0 ? d.html() : ""
                },
                getApiMessage: function(a, b, c) {
                    var d = "",
                        e = $("#global-response-messages .api_msg-" + b + "-" + c);
                    e.length > 0 && (d = e.html());
                    var f = $("#" + a + "-response-messages .api_msg-" + b + "-" + c);
                    return f.length > 0 && (d = f.html()), d
                },
                getJSONMessage: function(a, b) {
                    var c = API_MESSAGES.filter(function(b) {
                            return b.id === a ? !0 : void 0
                        }),
                        d = c[0].message,
                        e = "";
                    return b = b.toString(), $.each(d, function(a, c) {
                        $.isArray(c.code) ? -1 != $.inArray(b, c.code) && (e = c.description) : c.code === b && (e = c.description)
                    }), e
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.responsiveImages = function() {
            $("img.fn_responsive_image").each(function() {
                var a = $(this);
                enquire.register(DD.bp.get("1025"), {
                    match: function() {
                        console.log("between 1025 and 1366 (laptopBackground and desktopBackground)"), a.attr("src", a.data("laptopBackground")).removeClass("mobile_img").removeClass("tablet_img").removeClass("desktop_img").addClass("laptop_img")
                    }
                }, !0), enquire.register(DD.bp.get("m", "1024"), {
                    match: function() {
                        console.log("between m and 1024 (tablet)"), a.attr("src", a.data("tabletBackground")).removeClass("mobile_img").removeClass("desktop_img").removeClass("laptop_img").addClass("tablet_img"), 0 !== a.closest(".flexslider").length && $(".flexslider .flex-viewport").css({
                            height: a.height()
                        })
                    }
                }), enquire.register(DD.bp.get(0, "s"), {
                    match: function() {
                        a.attr("src", a.data("mobileBackground")).removeClass("tablet_img").removeClass("desktop_img").removeClass("laptop_img").addClass("mobile_img")
                    }
                })
            })
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.responsiveTable = function() {
            var a = function() {
                $(".fn_responsive-table").each(function(a, b) {
                    var c, d, e, f, g, h, i, j = $(b);
                    j.removeClass("fn_responsive-table"), c = $("<div></div>"), d = $('<div class="scrolltable"></div>').append(c).insertBefore(j), j.appendTo(c), c.prepend('<span class="shadow-before"></span>').append('<span class="shadow-after"></span>'), f = function() {
                        var a = j.outerWidth() - c.outerWidth(),
                            b = Math.floor(c.scrollLeft() / a * 100);
                        10 > b ? c.find(".shadow-before").css("margin-left", -1 * parseInt(10 - b, 10) + "px") : c.find(".shadow-before").css("margin-left", 0), b > 90 ? c.find(".shadow-after").css("margin-left", -1 * parseInt(90 - b, 10) + "px") : c.find(".shadow-after").css("margin-left", 0)
                    }, g = function() {
                        j.outerWidth() > c.outerWidth() ? (d.addClass("has-scroll"), f(), c.on("scroll.responsiveTable", f)) : (d.removeClass("has-scroll"), c.off("scroll.responsiveTable", f))
                    }, h = function() {
                        var b, c, f = $("<ul />"),
                            h = $('<div class="responsive-table-menu hidden"></div>'),
                            i = $('<a href="#" class="responsive-table-menu-btn">Show/hide columns</a>'),
                            k = $('<div class="responsive-table-menu-wrapper" />').append(i).append(h.append(f)),
                            l = 0;
                        c = function(a) {
                            var b;
                            return a ? (b = a.split(" "), b = $.grep(b, function(a) {
                                return 0 === a.indexOf("column-")
                            }), b.join(" ")) : ""
                        }, j.find("thead th").each(function(b) {
                            var d = $(this),
                                e = d.attr("id"),
                                h = parseInt(d.attr("colspan"), 10) || 1,
                                i = c(d.attr("class"));
                            if (e || (e = "table-" + a + "-col-" + b, d.attr("id", e)), j.find("tbody tr, tfoot tr").each(function() {
                                    var a = $(this).find("th, td").slice(l, l + h);
                                    a.attr("headers", e), i && a.addClass(i)
                                }), !d.hasClass("column-persist")) {
                                var k = "table-" + a + "-toggle-col-" + b,
                                    m = $('<input type="checkbox" name="toggle-cols" id="' + k + '" value="' + e + '" />');
                                f.append($("<li />").append(m).append('<label for="' + k + '">' + d.text() + "</label>")), m.change(function() {
                                    var a = m.val(),
                                        b = $("#" + a + ", [headers=" + a + "]");
                                    m.is(":checked") ? b.removeClass("column-force-hide").addClass("column-force-show") : b.removeClass("column-force-show").addClass("column-force-hide"), g()
                                }), m.bind("updateInputs.responsiveTable", function() {
                                    "table-cell" === d.css("display") ? m.attr("checked", !0) : m.attr("checked", !1)
                                })
                            }
                            l += h
                        }), b = function() {
                            $("body").off("click.responsiveTable touchstart.responsiveTable"), h.addClass("hidden")
                        }, i.on("click touchstart", function(a) {
                            a.preventDefault(), h.hasClass("hidden") ? (h.removeClass("hidden"), $("body").on("click.responsiveTable touchstart.responsiveTable", function(a) {
                                0 === k.find(a.target).length && b()
                            })) : b()
                        }), e = h.find("input"), d.before(k), j.addClass("responsive-table-column-toggling-initialised")
                    }, i = function() {
                        e && e.trigger("updateInputs.responsiveTable"), g()
                    }, j.hasClass("responsive-table-column-toggling") && h(), $(window).on("resize.responsiveTable orientationchange.responsiveTable", $.throttle(100, i)), i()
                })
            };
            return {
                init: a
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.scrollToAnchor = function() {
            var a, b;
            return {
                init: function() {
                    a = this, b = $(".fn_scrollToAnchor"), b.off("click.scrollToAnchor").on("click.scrollToAnchor", a.events.scrollToAnchor)
                },
                methods: {
                    doSomething: function() {}
                },
                events: {
                    scrollToAnchor: function(a) {
                        a.preventDefault();
                        var b = $(this);
                        return console.log(0 === b.attr("href").indexOf("#")), 0 !== b.attr("href").indexOf("#") ? !0 : void $(b.attr("href")).velocity("scroll", {
                            duration: 400,
                            offset: -60
                        })
                    }
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.sectionTitle = function() {
            var a;
            return {
                init: function() {
                    a = this, a.showTaglineOnInit()
                },
                isADSLPage: function() {
                    var a = !1,
                        b = $(".main.product-page .sq-container");
                    return b.length > 0 && "adsl" === b.data("sqType") && (a = !0), a
                },
                showTaglineOnInit: function() {
                    var b = !1;
                    if (console.log("showTaglineOnInit(): isADSLPage=" + a.isADSLPage()), 1 == a.isADSLPage() ? (console.log("showTaglineOnInit(): sessionStorage.broadbandTechnologyType=" + sessionStorage.getItem("broadbandTechnologyType")), "ADSL1" === sessionStorage.getItem("broadbandTechnologyType") && (b = !0)) : b = !0, 1 == b) {
                        var c = $(".main.product-page .title-section .tagline");
                        console.log("showTaglineOnInit(): tagline.length=" + c.length), c.length > 0 && c.removeClass("hidden")
                    }
                },
                hideTaglineForADSL1: function() {
                    if (1 == a.isADSLPage()) {
                        var b = $(".main.product-page .title-section .tagline");
                        b.length > 0 && b.addClass("hidden")
                    }
                },
                showTaglineForADSL1: function() {
                    if (1 == a.isADSLPage()) {
                        var b = $(".main.product-page .title-section .tagline");
                        b.length > 0 && b.removeClass("hidden")
                    }
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.sqRules = function() {
            return {
                addressResult: {
                    multiple: function() {
                        a.sqEntry.display.message("more-addresses"), a.sqEntry.display.streetAddressSelect(), a.sqEntry.display.sqCheckButton(), a.sqEntry.hide.streetAddressEntry(), a.sqEntry.hide.landlineEntry(), a.sqEntry.hide.loadingMessages()
                    },
                    single: function() {
                        a.livePerson.sq.startSQ(), a.sqEntry.serviceQualifyRequest(a.LANDLINECHECK ? "TIAB" : "BPS")
                    },
                    none: function() {
                        a.sqEntry.hide.form(), a.eoiEntry.display.message("experian-error"), a.adobeAnalytics.global.sq.failExperionError(a.sqEntry.get.sourceType(), "standard"), a.analytics.methods.sqFailures.experionError(a.sqEntry.get.sourceType()), a.livePerson.sq.addressNotFound()
                    }
                },
                sqResult: {
                    home: {
                        nbn: {
                            success: {
                                NBN_SERVICE_CLASS_ONE: function(b, c) {
                                    console.log("home nbn success NBN_SERVICE_CLASS_ONE"), a.sqEntry.hide.loadingMessages(), a.sqEntry.display.successMessage("nbn", c), b.fireAnalytics && (a.adobeAnalytics.global.sq.success(a.sqEntry.get.sourceType(), "NBN_SERVICE_CLASS_ONE", "nbn", "standard"), a.analytics.methods.sq.sqSuccess(a.sqEntry.get.sourceType(), "NBN_SERVICE_CLASS_ONE", "nbn")), a.livePerson.sq.completeSQ()
                                },
                                NBN_SERVICE_CLASS_TWO: function(b, c) {
                                    console.log("home nbn success NBN_SERVICE_CLASS_TWO"), a.sqEntry.hide.loadingMessages(), a.sqEntry.display.successMessage("nbn", c), b.fireAnalytics && (a.adobeAnalytics.global.sq.success(a.sqEntry.get.sourceType(), "NBN_SERVICE_CLASS_TWO", "nbn", "standard"), a.analytics.methods.sq.sqSuccess(a.sqEntry.get.sourceType(), "NBN_SERVICE_CLASS_TWO", "nbn")), a.livePerson.sq.completeSQ()
                                },
                                NBN_SERVICE_CLASS_THREE: function(b, c) {
                                    console.log("home nbn success NBN_SERVICE_CLASS_THREE"), a.sqEntry.hide.loadingMessages(), a.sqEntry.display.successMessage("nbn", c), b.fireAnalytics && (a.adobeAnalytics.global.sq.success(a.sqEntry.get.sourceType(), "NBN_SERVICE_CLASS_THREE", "nbn", "standard"), a.analytics.methods.sq.sqSuccess(a.sqEntry.get.sourceType(), "NBN_SERVICE_CLASS_THREE", "nbn")), a.livePerson.sq.completeSQ()
                                },
                                NBN_SERVICE_CLASS_ELEVEN: function(b, c) {
                                    console.log("home nbn-fttn NBN_SERVICE_CLASS_ELEVEN"), a.sqEntry.hide.loadingMessages(), a.sqEntry.display.successMessage("nbn-fttn", c), b.fireAnalytics && (a.adobeAnalytics.global.sq.success(a.sqEntry.get.sourceType(), "NBN_SERVICE_CLASS_ELEVEN", "nbn-fttn", "standard"), a.analytics.methods.sq.sqSuccess(a.sqEntry.get.sourceType(), "NBN_SERVICE_CLASS_ELEVEN", "nbn-fttn")), a.livePerson.sq.completeSQ()
                                },
                                NBN_SERVICE_CLASS_TWELVE: function(b, c) {
                                    console.log("home nbn NBN_SERVICE_CLASS_TWELVE"), a.sqEntry.hide.loadingMessages(), a.sqEntry.display.successMessage("nbn-" + b.broadbandTechType, c), b.fireAnalytics && (a.adobeAnalytics.global.sq.success(a.sqEntry.get.sourceType(), "NBN_SERVICE_CLASS_TWELVE", "nbn-" + b.broadbandTechType, "standard"), a.analytics.methods.sq.sqSuccess(a.sqEntry.get.sourceType(), "NBN_SERVICE_CLASS_TWELVE", "nbn-" + b.broadbandTechType)), a.livePerson.sq.completeSQ()
                                },
                                NBN_SERVICE_CLASS_THIRTEEN: function(b, c) {
                                    console.log("home nbn NBN_SERVICE_CLASS_THIRTEEN"), a.sqEntry.hide.loadingMessages(), a.sqEntry.display.successMessage("nbn-" + b.broadbandTechType, c), b.fireAnalytics && (a.adobeAnalytics.global.sq.success(a.sqEntry.get.sourceType(), "NBN_SERVICE_CLASS_THIRTEEN", "nbn-" + b.broadbandTechType, "standard"), a.analytics.methods.sq.sqSuccess(a.sqEntry.get.sourceType(), "NBN_SERVICE_CLASS_THIRTEEN", "nbn-" + b.broadbandTechType)), a.livePerson.sq.completeSQ()
                                }
                            },
                            fail: {
                                SQ_ERROR: function(b, c) {
                                    console.log("home nbn SQ_ERROR"), a.sqEntry.display.successMessage("landline-request", c), a.sqEntry.display.landlineEntry(), a.sqEntry.display.sqCheckButton(), a.LANDLINECHECK = !0, $(".selected-address").text(a.sqEntry.getSQDisplayAddres()), a.sqEntry.hide.loadingMessages(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("SQ_ERROR", a.sqEntry.get.sourceType(), "nbn", "standard"), a.analytics.methods.sqFailures.failureMessages("SQ_ERROR", a.sqEntry.get.sourceType(), "nbn")), a.livePerson.sq.sqError(), a.join.model.sq.bpsError = !0, console.log("bpsError: true")
                                },
                                TIAB_ERROR: function(b, c) {
                                    console.log("home nbn TIAB_ERROR"), a.sqEntry.display.successMessage("landline-request", c), a.sqEntry.display.landlineEntry(), a.sqEntry.display.sqCheckButton(), a.LANDLINECHECK = !0, $(".selected-address").text(a.sqEntry.getSQDisplayAddres()), a.sqEntry.hide.loadingMessages(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("TIAB_ERROR", a.sqEntry.get.sourceType(), "nbn", "standard"), a.analytics.methods.sqFailures.failureMessages("TIAB_ERROR", a.sqEntry.get.sourceType(), "nbn")), a.livePerson.sq.sqError(), a.join.model.sq.bpsError = !0, console.log("bpsError: true")
                                },
                                TWICE_ORDER: function(b) {
                                    console.log("home nbn fail TWICE_ORDER"), a.sqEntry.hide.form(), a.eoiEntry.display.message("duplicate-address"), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("TWICE_ORDER", a.sqEntry.get.sourceType(), "nbn", "standard"), a.analytics.methods.sqFailures.failureMessages("TWICE_ORDER", a.sqEntry.get.sourceType(), "nbn")), a.livePerson.sq.cannotBelongError()
                                },
                                SERVICE_NOT_AVAILABLE: function(b, c) {
                                    console.log("home nbn fail SERVICE_NOT_AVAILABLE"), a.sqEntry.display.successMessage("landline-request", c), a.sqEntry.display.landlineEntry(), a.sqEntry.display.sqCheckButton(), a.LANDLINECHECK = !0, $(".selected-address").text(a.sqEntry.getSQDisplayAddres()), a.sqEntry.hide.loadingMessages(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("SERVICE_NOT_AVAILABLE", a.sqEntry.get.sourceType(), "nbn", "standard"), a.analytics.methods.sqFailures.failureMessages("SERVICE_NOT_AVAILABLE", a.sqEntry.get.sourceType(), "nbn")), a.livePerson.sq.completeSQ()
                                },
                                ADDR_NOT_IN_WHITELIST: function(b) {
                                    console.log("home nbn fail ADDR_NOT_IN_WHITELIST"), a.sqEntry.hide.form(), a.eoiEntry.display.message("service-error"), a.eoiEntry.display.message("sq-error-form"), a.eoiEntry.display.fields("eoi-entry-full"), $("#eoi-entry-full-address").val($("#sq-street-address").val()), a.forms.initShiftLabel($("#eoi-entry-full-address")), a.eoiEntry.bind.validation(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("ADDR_NOT_IN_WHITELIST", a.sqEntry.get.sourceType(), "nbn", "standard"), a.analytics.methods.sqFailures.failureMessages("ADDR_NOT_IN_WHITELIST", a.sqEntry.get.sourceType(), "nbn")), a.livePerson.sq.cannotBelongError()
                                }
                            }
                        },
                        adsl: {
                            success: {
                                NEW_ORDER: function(b, c) {
                                    console.log("home adsl success NEW_ORDER"), a.sqEntry.hide.loadingMessages(), a.sqEntry.display.successMessage(b.broadbandTechType + "-new", c), "adsl1" === b.broadbandTechType && a.sectionTitle.showTaglineForADSL1(), b.fireAnalytics && (a.adobeAnalytics.global.sq.success(a.sqEntry.get.sourceType(), "NEW_ORDER", "adsl", "standard"), a.analytics.methods.sq.sqSuccess(a.sqEntry.get.sourceType(), "NEW_ORDER", "adsl")), a.livePerson.sq.completeSQ()
                                },
                                PORT_ORDER: function(b, c) {
                                    console.log("home adsl success PORT_ORDER"), a.sqEntry.hide.loadingMessages(), a.sqEntry.display.successMessage(b.broadbandTechType + "-port", c), "adsl1" === b.broadbandTechType && a.sectionTitle.showTaglineForADSL1(), b.fireAnalytics && (a.adobeAnalytics.global.sq.success(a.sqEntry.get.sourceType(), "PORT_ORDER", "adsl", "standard"), a.analytics.methods.sq.sqSuccess(a.sqEntry.get.sourceType(), "PORT_ORDER", "adsl")), a.livePerson.sq.completeSQ()
                                },
                                CHURN_ORDER: function(b, c) {
                                    console.log("home adsl success CHURN_ORDER"), a.sqEntry.hide.loadingMessages(), a.sqEntry.display.successMessage(b.broadbandTechType + "-churn", c), "adsl1" === b.broadbandTechType && a.sectionTitle.showTaglineForADSL1(), b.fireAnalytics && (a.adobeAnalytics.global.sq.success(a.sqEntry.get.sourceType(), "CHURN_ORDER", "adsl", "standard"), a.analytics.methods.sq.sqSuccess(a.sqEntry.get.sourceType(), "CHURN_ORDER", "adsl")), a.livePerson.sq.completeSQ()
                                }
                            },
                            fail: {
                                SQ_ERROR: function(b) {
                                    console.log("home adsl fail SQ_ERROR"), a.sqEntry.hide.form(), a.eoiEntry.display.message("sq-error"), a.eoiEntry.display.message("sq-error-form"), a.eoiEntry.display.fields("eoi-entry-full"), $("#eoi-entry-full-address").val($("#sq-street-address").val()), a.forms.initShiftLabel($("#eoi-entry-full-address")), a.eoiEntry.bind.validation(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("SQ_ERROR", a.sqEntry.get.sourceType(), "adsl", "standard"), a.analytics.methods.sqFailures.failureMessages("SQ_ERROR", a.sqEntry.get.sourceType(), "adsl")), a.livePerson.sq.sqError()
                                },
                                TIAB_ERROR: function(b) {
                                    console.log("home adsl fail TIAB_ERROR"), a.sqEntry.hide.form(), a.eoiEntry.display.message("sq-error"), a.eoiEntry.display.message("sq-error-form"), a.eoiEntry.display.fields("eoi-entry-full"), $("#eoi-entry-full-address").val($("#sq-street-address").val()), a.forms.initShiftLabel($("#eoi-entry-full-address")), a.eoiEntry.bind.validation(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("TIAB_ERROR", a.sqEntry.get.sourceType(), "adsl", "standard"), a.analytics.methods.sqFailures.failureMessages("TIAB_ERROR", a.sqEntry.get.sourceType(), "adsl")), a.livePerson.sq.sqError()
                                },
                                TWICE_ORDER: function(b) {
                                    console.log("home adsl fail TWICE_ORDER"), a.sqEntry.hide.form(), a.eoiEntry.display.message("duplicate-address"), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("TWICE_ORDER", a.sqEntry.get.sourceType(), "adsl", "standard"), a.analytics.methods.sqFailures.failureMessages("TWICE_ORDER", a.sqEntry.get.sourceType(), "adsl")), a.livePerson.sq.cannotBelongError()
                                },
                                INVALID_ADDRESS: function(b, c) {
                                    console.log("home adsl fail INVALID_ADDRESS"), a.sqEntry.display.successMessage("landline-request", c), a.sqEntry.display.landlineEntry(), a.sqEntry.display.phoneMatchMessage(), a.sqEntry.display.sqCheckButton(), a.LANDLINECHECK = !0, $(".selected-address").text(a.sqEntry.getSQDisplayAddres()), a.sqEntry.hide.loadingMessages(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("INVALID_ADDRESS", a.sqEntry.get.sourceType(), "adsl", "standard"), a.analytics.methods.sqFailures.failureMessages("INVALID_ADDRESS", a.sqEntry.get.sourceType(), "adsl")), a.livePerson.sq.invalidPhoneError()
                                },
                                SERVICE_NOT_AVAILABLE: function(b) {
                                    console.log("home adsl fail SERVICE_NOT_AVAILABLE"), a.sqEntry.hide.form(), a.eoiEntry.display.message("service-error"), a.eoiEntry.display.message("sq-error-form"), a.eoiEntry.display.fields("eoi-entry-full"), $("#eoi-entry-full-address").val($("#sq-street-address").val()), a.forms.initShiftLabel($("#eoi-entry-full-address")), a.eoiEntry.bind.validation(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("SERVICE_NOT_AVAILABLE", a.sqEntry.get.sourceType(), "adsl", "standard"), a.analytics.methods.sqFailures.failureMessages("SERVICE_NOT_AVAILABLE", a.sqEntry.get.sourceType(), "adsl")), a.livePerson.sq.completeSQ()
                                },
                                ADDR_NOT_IN_WHITELIST: function(b) {
                                    console.log("home adsl ADDR_NOT_IN_WHITELIST"), a.sqEntry.hide.form(), a.eoiEntry.display.message("service-error"), a.eoiEntry.display.message("sq-error-form"), a.eoiEntry.display.fields("eoi-entry-full"), $("#eoi-entry-full-address").val($("#sq-street-address").val()), a.forms.initShiftLabel($("#eoi-entry-full-address")), a.eoiEntry.bind.validation(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("ADDR_NOT_IN_WHITELIST", a.sqEntry.get.sourceType(), "adsl", "standard"), a.analytics.methods.sqFailures.failureMessages("ADDR_NOT_IN_WHITELIST", a.sqEntry.get.sourceType(), "adsl")), a.livePerson.sq.cannotBelongError()
                                }
                            }
                        }
                    },
                    nbn: {
                        nbn: {
                            success: {
                                NBN_SERVICE_CLASS_ONE: function(b, c) {
                                    console.log("nbn nbn success NBN_SERVICE_CLASS_ONE"), a.sqEntry.hide.loadingMessages(), a.sqEntry.display.successMessage("nbn", c), a.sqEntry.hide.findOutMoreButton("nbn", "nbn"), a.sqEntry.display.productSelection(), b.fireAnalytics && (a.adobeAnalytics.global.sq.success("NBN", "NBN_SERVICE_CLASS_ONE", "nbn", "standard"), a.analytics.methods.sq.sqSuccess("NBN", "NBN_SERVICE_CLASS_ONE", "nbn")), a.livePerson.sq.completeSQ()
                                },
                                NBN_SERVICE_CLASS_TWO: function(b, c) {
                                    console.log("nbn nbn success NBN_SERVICE_CLASS_TWO"), a.sqEntry.hide.loadingMessages(), a.sqEntry.display.successMessage("nbn", c), a.sqEntry.hide.findOutMoreButton("nbn", "nbn"), a.sqEntry.display.productSelection(), b.fireAnalytics && (a.adobeAnalytics.global.sq.success("NBN", "NBN_SERVICE_CLASS_TWO", "nbn", "standard"), a.analytics.methods.sq.sqSuccess("NBN", "NBN_SERVICE_CLASS_TWO", "nbn")), a.livePerson.sq.completeSQ()
                                },
                                NBN_SERVICE_CLASS_THREE: function(b, c) {
                                    console.log("nbn nbn success NBN_SERVICE_CLASS_THREE"), a.sqEntry.hide.loadingMessages(), a.sqEntry.display.successMessage("nbn", c), a.sqEntry.hide.findOutMoreButton("nbn", "nbn"), a.sqEntry.display.productSelection(), b.fireAnalytics && (a.adobeAnalytics.global.sq.success("NBN", "NBN_SERVICE_CLASS_THREE", "nbn", "standard"), a.analytics.methods.sq.sqSuccess("NBN", "NBN_SERVICE_CLASS_THREE", "nbn")), a.livePerson.sq.completeSQ()
                                },
                                NBN_SERVICE_CLASS_ELEVEN: function(b, c) {
                                    console.log("nbn nbn-fttn NBN_SERVICE_CLASS_ELEVEN"), a.sqEntry.hide.loadingMessages(), a.sqEntry.display.successMessage("nbn-fttn", c), a.sqEntry.hide.findOutMoreButton("nbn", "nbn-fttn"), b.fireAnalytics && (a.adobeAnalytics.global.sq.success(a.sqEntry.get.sourceType(), "NBN_SERVICE_CLASS_ELEVEN", "nbn-fttn", "standard"), a.analytics.methods.sq.sqSuccess(a.sqEntry.get.sourceType(), "NBN_SERVICE_CLASS_ELEVEN", "nbn-fttn")), a.livePerson.sq.completeSQ()
                                },
                                NBN_SERVICE_CLASS_TWELVE: function(b, c) {
                                    console.log("nbn nbn-" + b.broadbandTechType + " NBN_SERVICE_CLASS_TWELVE"), a.sqEntry.hide.loadingMessages(), a.sqEntry.display.successMessage("nbn-" + b.broadbandTechType, c), a.sqEntry.hide.findOutMoreButton("nbn", "nbn-" + b.broadbandTechType), b.fireAnalytics && (a.adobeAnalytics.global.sq.success(a.sqEntry.get.sourceType(), "NBN_SERVICE_CLASS_TWELVE", "nbn-" + b.broadbandTechType, "standard"), a.analytics.methods.sq.sqSuccess(a.sqEntry.get.sourceType(), "NBN_SERVICE_CLASS_TWELVE", "nbn-" + b.broadbandTechType)), a.livePerson.sq.completeSQ()
                                },
                                NBN_SERVICE_CLASS_THIRTEEN: function(b, c) {
                                    console.log("nbn nbn-" + b.broadbandTechType + " NBN_SERVICE_CLASS_THIRTEEN"), a.sqEntry.hide.loadingMessages(), a.sqEntry.display.successMessage("nbn-" + b.broadbandTechType, c), a.sqEntry.hide.findOutMoreButton("nbn", "nbn-" + b.broadbandTechType), b.fireAnalytics && (a.adobeAnalytics.global.sq.success(a.sqEntry.get.sourceType(), "NBN_SERVICE_CLASS_THIRTEEN", "nbn-" + b.broadbandTechType, "standard"), a.analytics.methods.sq.sqSuccess(a.sqEntry.get.sourceType(), "NBN_SERVICE_CLASS_THIRTEEN", "nbn-" + b.broadbandTechType)), a.livePerson.sq.completeSQ()
                                }
                            },
                            fail: {
                                SQ_ERROR: function(b, c) {
                                    console.log("nbn nbn SQ_ERROR"), a.sqEntry.display.successMessage("landline-request", c), a.sqEntry.display.landlineEntry(), a.sqEntry.display.sqCheckButton(), a.LANDLINECHECK = !0, $(".selected-address").text(a.sqEntry.getSQDisplayAddres()), a.sqEntry.hide.loadingMessages(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("SQ_ERROR", "NBN", "nbn", "standard"), a.analytics.methods.sqFailures.failureMessages("SQ_ERROR", "NBN", "nbn")), a.livePerson.sq.sqError()
                                },
                                TWICE_ORDER: function(b) {
                                    console.log("nbn nbn fail TWICE_ORDER"), a.sqEntry.hide.form(), a.eoiEntry.display.message("duplicate-address"), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("TWICE_ORDER", "NBN", "nbn", "standard"), a.analytics.methods.sqFailures.failureMessages("TWICE_ORDER", "NBN", "nbn")), a.livePerson.sq.cannotBelongError()
                                },
                                SERVICE_NOT_AVAILABLE: function(b, c) {
                                    console.log("nbn nbn fail SERVICE_NOT_AVAILABLE"), a.sqEntry.display.successMessage("landline-request", c), a.sqEntry.display.landlineEntry(), a.sqEntry.display.sqCheckButton(), a.LANDLINECHECK = !0, $(".selected-address").text(a.sqEntry.getSQDisplayAddres()), a.sqEntry.hide.loadingMessages(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("SERVICE_NOT_AVAILABLE", "NBN", "nbn", "standard"), a.analytics.methods.sqFailures.failureMessages("SERVICE_NOT_AVAILABLE", "NBN", "nbn")), a.livePerson.sq.completeSQ()
                                },
                                ADDR_NOT_IN_WHITELIST: function(b) {
                                    console.log("nbn nbn fail ADDR_NOT_IN_WHITELIST"), a.sqEntry.hide.form(), a.eoiEntry.display.message("service-error"), a.eoiEntry.display.message("sq-error-form"), a.eoiEntry.display.fields("eoi-entry-full"), $("#eoi-entry-full-address").val($("#sq-street-address").val()), a.forms.initShiftLabel($("#eoi-entry-full-address")), a.eoiEntry.bind.validation(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("ADDR_NOT_IN_WHITELIST", "NBN", "nbn", "standard"), a.analytics.methods.sqFailures.failureMessages("ADDR_NOT_IN_WHITELIST", "NBN", "nbn")), a.livePerson.sq.cannotBelongError()
                                }
                            }
                        },
                        adsl: {
                            success: {
                                NEW_ORDER: function(b, c) {
                                    console.log("nbn adsl success NEW_ORDER"), a.sqEntry.hide.loadingMessages();
                                    var d = b.broadbandTechType + "-new";
                                    a.sqEntry.is.adslTypeOrder(b.broadbandTechType) && (console.log("=== ADSL Type Order (" + b.broadbandTechType + "). Show alt success message."), d += "-alternative"), a.sqEntry.display.successMessage(d, c), a.sqEntry.hide.productSelection(), a.sqEntry.hide.speedboostOffer(), "adsl1" === b.broadbandTechType && a.sectionTitle.showTaglineForADSL1(), b.fireAnalytics && (a.adobeAnalytics.global.sq.success("NBN", "NEW_ORDER", "adsl", "standard"), a.analytics.methods.sq.sqSuccess("NBN", "NEW_ORDER", "adsl")), a.livePerson.sq.completeSQ()
                                },
                                PORT_ORDER: function(b, c) {
                                    console.log("nbn adsl success PORT_ORDER"), a.sqEntry.hide.loadingMessages();
                                    var d = b.broadbandTechType + "-port";
                                    a.sqEntry.is.adslTypeOrder(b.broadbandTechType) && (console.log("=== ADSL Type Order (" + b.broadbandTechType + "). Show alt success message."), d += "-alternative"), a.sqEntry.display.successMessage(d, c), a.sqEntry.hide.productSelection(), a.sqEntry.hide.speedboostOffer(), "adsl1" === b.broadbandTechType && a.sectionTitle.showTaglineForADSL1(), b.fireAnalytics && (a.adobeAnalytics.global.sq.success("NBN", "PORT_ORDER", "adsl", "standard"), a.analytics.methods.sq.sqSuccess("NBN", "PORT_ORDER", "adsl")), a.livePerson.sq.completeSQ()
                                },
                                CHURN_ORDER: function(b, c) {
                                    console.log("nbn adsl success CHURN_ORDER"), a.sqEntry.hide.loadingMessages();
                                    var d = b.broadbandTechType + "-churn";
                                    a.sqEntry.is.adslTypeOrder(b.broadbandTechType) && (console.log("=== ADSL Type Order (" + b.broadbandTechType + "). Show alt success message."), d += "-alternative"), a.sqEntry.display.successMessage(d, c), a.sqEntry.hide.productSelection(), a.sqEntry.hide.speedboostOffer(), "adsl1" === b.broadbandTechType && a.sectionTitle.showTaglineForADSL1(), b.fireAnalytics && (a.adobeAnalytics.global.sq.success("NBN", "CHURN_ORDER", "adsl", "standard"), a.analytics.methods.sq.sqSuccess("NBN", "CHURN_ORDER", "adsl")), a.livePerson.sq.completeSQ()
                                }
                            },
                            fail: {
                                SQ_ERROR: function(b) {
                                    console.log("nbn adsl SQ_ERROR", focus), a.sqEntry.hide.form(), a.eoiEntry.display.message("sq-error"), a.eoiEntry.display.message("sq-error-form"), a.eoiEntry.display.fields("eoi-entry-full"), $("#eoi-entry-full-address").val($("#sq-street-address").val()), a.forms.initShiftLabel($("#eoi-entry-full-address")), a.eoiEntry.bind.validation(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("SQ_ERROR", "NBN", "adsl", "standard"), a.analytics.methods.sqFailures.failureMessages("SQ_ERROR", "NBN", "adsl")), a.livePerson.sq.sqError()
                                },
                                TWICE_ORDER: function(b) {
                                    console.log("nbn adsl fail TWICE_ORDER"), a.sqEntry.hide.form(), a.eoiEntry.display.message("duplicate-address"), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("TWICE_ORDER", "NBN", "adsl", "standard"), a.analytics.methods.sqFailures.failureMessages("TWICE_ORDER", "NBN", "adsl")), a.livePerson.sq.cannotBelongError()
                                },
                                INVALID_ADDRESS: function(b, c) {
                                    console.log("nbn adsl fail INVALID_ADDRESS"), a.sqEntry.display.successMessage("landline-request", c), a.sqEntry.display.landlineEntry(), a.sqEntry.display.phoneMatchMessage(), a.sqEntry.display.sqCheckButton(), a.LANDLINECHECK = !0, $(".selected-address").text(a.sqEntry.getSQDisplayAddres()), a.sqEntry.hide.loadingMessages(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("INVALID_ADDRESS", "NBN", "adsl", "standard"),
                                        a.analytics.methods.sqFailures.failureMessages("INVALID_ADDRESS", "NBN", "adsl")), a.livePerson.sq.invalidPhoneError()
                                },
                                SERVICE_NOT_AVAILABLE: function(b) {
                                    console.log("nbn adsl fail SERVICE_NOT_AVAILABLE"), a.sqEntry.hide.form(), a.eoiEntry.display.message("service-error"), a.eoiEntry.display.message("sq-error-form"), a.eoiEntry.display.fields("eoi-entry-full"), $("#eoi-entry-full-address").val($("#sq-street-address").val()), a.forms.initShiftLabel($("#eoi-entry-full-address")), a.eoiEntry.bind.validation(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("SERVICE_NOT_AVAILABLE", "NBN", "adsl", "standard"), a.analytics.methods.sqFailures.failureMessages("SERVICE_NOT_AVAILABLE", "NBN", "adsl")), a.livePerson.sq.completeSQ()
                                },
                                ADDR_NOT_IN_WHITELIST: function(b) {
                                    console.log("nbn adsl fail ADDR_NOT_IN_WHITELIST"), a.sqEntry.hide.form(), a.eoiEntry.display.message("service-error"), a.eoiEntry.display.message("sq-error-form"), a.eoiEntry.display.fields("eoi-entry-full"), $("#eoi-entry-full-address").val($("#sq-street-address").val()), a.forms.initShiftLabel($("#eoi-entry-full-address")), a.eoiEntry.bind.validation(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("ADDR_NOT_IN_WHITELIST", "NBN", "adsl", "standard"), a.analytics.methods.sqFailures.failureMessages("ADDR_NOT_IN_WHITELIST", "NBN", "adsl")), a.livePerson.sq.cannotBelongError()
                                }
                            }
                        }
                    },
                    adsl: {
                        nbn: {
                            success: {
                                NBN_SERVICE_CLASS_ONE: function(b, c) {
                                    console.log("adsl nbn success NBN_SERVICE_CLASS_ONE"), a.sqEntry.hide.loadingMessages();
                                    var d = "nbn";
                                    a.sqEntry.is.nbnTypeOrder(b.broadbandTechType) && (console.log("=== NBN Type Order (" + b.broadbandTechType + "). Show alt success message."), d += "-alternative"), a.sqEntry.display.successMessage(d, c), a.sqEntry.hide.productSelection(), a.sqEntry.hide.adslOnlyOffer(), b.fireAnalytics && (a.adobeAnalytics.global.sq.success("ADSL", "NBN_SERVICE_CLASS_ONE", "nbn", "standard"), a.analytics.methods.sq.sqSuccess("ADSL", "NBN_SERVICE_CLASS_ONE", "nbn")), a.livePerson.sq.completeSQ()
                                },
                                NBN_SERVICE_CLASS_TWO: function(b, c) {
                                    console.log("adsl nbn success NBN_SERVICE_CLASS_TWO"), a.sqEntry.hide.loadingMessages();
                                    var d = "nbn";
                                    a.sqEntry.is.nbnTypeOrder(b.broadbandTechType) && (console.log("=== NBN Type Order (" + b.broadbandTechType + "). Show alt success message."), d += "-alternative"), a.sqEntry.display.successMessage(d, c), a.sqEntry.hide.productSelection(), a.sqEntry.hide.adslOnlyOffer(), b.fireAnalytics && (a.adobeAnalytics.global.sq.success("ADSL", "NBN_SERVICE_CLASS_TWO", "nbn", "standard"), a.analytics.methods.sq.sqSuccess("ADSL", "NBN_SERVICE_CLASS_TWO", "nbn")), a.livePerson.sq.completeSQ()
                                },
                                NBN_SERVICE_CLASS_THREE: function(b, c) {
                                    console.log("adsl nbn success NBN_SERVICE_CLASS_THREE"), a.sqEntry.hide.loadingMessages();
                                    var d = "nbn";
                                    a.sqEntry.is.nbnTypeOrder(b.broadbandTechType) && (console.log("=== NBN Type Order (" + b.broadbandTechType + "). Show alt success message."), d += "-alternative"), a.sqEntry.display.successMessage(d, c), a.sqEntry.hide.productSelection(), a.sqEntry.hide.adslOnlyOffer(), b.fireAnalytics && (a.adobeAnalytics.global.sq.success("ADSL", "NBN_SERVICE_CLASS_THREE", "nbn", "standard"), a.analytics.methods.sq.sqSuccess("ADSL", "NBN_SERVICE_CLASS_THREE", "nbn")), a.livePerson.sq.completeSQ()
                                },
                                NBN_SERVICE_CLASS_ELEVEN: function(b, c) {
                                    console.log("adsl nbn-fttn NBN_SERVICE_CLASS_ELEVEN"), a.sqEntry.hide.loadingMessages();
                                    var d = "nbn-fttn";
                                    a.sqEntry.is.nbnTypeOrder(b.broadbandTechType) && (console.log("=== NBN Type Order (" + b.broadbandTechType + "). Show alt success message."), d += "-alternative"), a.sqEntry.display.successMessage(d, c), a.sqEntry.hide.productSelection(), a.sqEntry.hide.adslOnlyOffer(), b.fireAnalytics && (a.adobeAnalytics.global.sq.success("ADSL", "NBN_SERVICE_CLASS_ELEVEN", "nbn-fttn", "standard"), a.analytics.methods.sq.sqSuccess("ADSL", "NBN_SERVICE_CLASS_ELEVEN", "nbn-fttn")), a.livePerson.sq.completeSQ()
                                },
                                NBN_SERVICE_CLASS_TWELVE: function(b, c) {
                                    console.log("adsl nbn-" + b.broadbandTechType + " NBN_SERVICE_CLASS_TWELVE"), a.sqEntry.hide.loadingMessages();
                                    var d = "nbn-" + b.broadbandTechType;
                                    a.sqEntry.is.nbnTypeOrder(b.broadbandTechType) && (console.log("=== NBN Type Order (" + b.broadbandTechType + "). Show alt success message."), d += "-alternative"), a.sqEntry.display.successMessage(d, c), a.sqEntry.hide.productSelection(), a.sqEntry.hide.adslOnlyOffer(), b.fireAnalytics && (a.adobeAnalytics.global.sq.success("ADSL", "NBN_SERVICE_CLASS_TWELVE", "nbn-" + b.broadbandTechType, "standard"), a.analytics.methods.sq.sqSuccess("ADSL", "NBN_SERVICE_CLASS_TWELVE", "nbn-" + b.broadbandTechType)), a.livePerson.sq.completeSQ()
                                },
                                NBN_SERVICE_CLASS_THIRTEEN: function(b, c) {
                                    console.log("adsl nbn-" + b.broadbandTechType + " NBN_SERVICE_CLASS_THIRTEEN"), a.sqEntry.hide.loadingMessages();
                                    var d = "nbn-" + b.broadbandTechType;
                                    a.sqEntry.is.nbnTypeOrder(b.broadbandTechType) && (console.log("=== NBN Type Order (" + b.broadbandTechType + "). Show alt success message."), d += "-alternative"), a.sqEntry.display.successMessage(d, c), a.sqEntry.hide.productSelection(), a.sqEntry.hide.adslOnlyOffer(), b.fireAnalytics && (a.adobeAnalytics.global.sq.success("ADSL", "NBN_SERVICE_CLASS_THIRTEEN", "nbn-" + b.broadbandTechType, "standard"), a.analytics.methods.sq.sqSuccess("ADSL", "NBN_SERVICE_CLASS_THIRTEEN", "nbn-" + b.broadbandTechType)), a.livePerson.sq.completeSQ()
                                }
                            },
                            fail: {
                                SQ_ERROR: function(b, c) {
                                    console.log("adsl nbn SQ_ERROR"), a.sqEntry.display.successMessage("landline-request", c), a.sqEntry.display.landlineEntry(), a.sqEntry.display.sqCheckButton(), a.LANDLINECHECK = !0, $(".selected-address").text(a.sqEntry.getSQDisplayAddres()), a.sqEntry.hide.loadingMessages(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("SQ_ERROR", "ADSL", "nbn", "standard"), a.analytics.methods.sqFailures.failureMessages("SQ_ERROR", "ADSL", "nbn")), a.livePerson.sq.sqError()
                                },
                                TWICE_ORDER: function(b) {
                                    console.log("adsl nbn fail TWICE_ORDER"), a.sqEntry.hide.form(), a.eoiEntry.display.message("duplicate-address"), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("TWICE_ORDER", "ADSL", "nbn", "standard"), a.analytics.methods.sqFailures.failureMessages("TWICE_ORDER", "ADSL", "nbn")), a.livePerson.sq.cannotBelongError()
                                },
                                SERVICE_NOT_AVAILABLE: function(b, c) {
                                    console.log("adsl nbn fail SERVICE_NOT_AVAILABLE"), a.sqEntry.display.successMessage("landline-request", c), a.sqEntry.display.landlineEntry(), a.sqEntry.display.sqCheckButton(), a.LANDLINECHECK = !0, $(".selected-address").text(a.sqEntry.getSQDisplayAddres()), a.sqEntry.hide.loadingMessages(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("SERVICE_NOT_AVAILABLE", "ADSL", "nbn", "standard"), a.analytics.methods.sqFailures.failureMessages("SERVICE_NOT_AVAILABLE", "ADSL", "nbn")), a.livePerson.sq.completeSQ()
                                },
                                ADDR_NOT_IN_WHITELIST: function(b) {
                                    console.log("adsl nbn fail ADDR_NOT_IN_WHITELIST"), a.sqEntry.hide.form(), a.eoiEntry.display.message("service-error"), a.eoiEntry.display.message("sq-error-form"), a.eoiEntry.display.fields("eoi-entry-full"), $("#eoi-entry-full-address").val($("#sq-street-address").val()), a.forms.initShiftLabel($("#eoi-entry-full-address")), a.eoiEntry.bind.validation(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("ADDR_NOT_IN_WHITELIST", "ADSL", "nbn", "standard"), a.analytics.methods.sqFailures.failureMessages("ADDR_NOT_IN_WHITELIST", "ADSL", "nbn")), a.livePerson.sq.cannotBelongError()
                                }
                            }
                        },
                        adsl: {
                            success: {
                                NEW_ORDER: function(b, c) {
                                    console.log("adsl adsl success NEW_ORDER"), a.sqEntry.hide.loadingMessages(), a.sqEntry.display.successMessage(b.broadbandTechType + "-new", c), a.sqEntry.hide.findOutMoreButton("adsl", b.broadbandTechType + "-new"), a.sqEntry.display.productSelection(), a.sqEntry.hide.adslOnlyOffer(), "adsl1" === b.broadbandTechType && a.sectionTitle.showTaglineForADSL1(), b.fireAnalytics && (a.adobeAnalytics.global.sq.success("ADSL", "NEW_ORDER", "adsl", "standard"), a.analytics.methods.sq.sqSuccess("ADSL", "NEW_ORDER", "adsl")), a.livePerson.sq.completeSQ()
                                },
                                PORT_ORDER: function(b, c) {
                                    console.log("adsl adsl success PORT_ORDER"), a.sqEntry.hide.loadingMessages(), a.sqEntry.display.successMessage(b.broadbandTechType + "-port", c), a.sqEntry.hide.findOutMoreButton("adsl", b.broadbandTechType + "-port"), a.sqEntry.display.productSelection(), a.sqEntry.hide.adslOnlyOffer(), "adsl1" === b.broadbandTechType && a.sectionTitle.showTaglineForADSL1(), b.fireAnalytics && (a.adobeAnalytics.global.sq.success("ADSL", "PORT_ORDER", "adsl", "standard"), a.analytics.methods.sq.sqSuccess("ADSL", "PORT_ORDER", "adsl")), a.livePerson.sq.completeSQ()
                                },
                                CHURN_ORDER: function(b, c) {
                                    console.log("adsl adsl success CHURN_ORDER"), a.sqEntry.hide.loadingMessages(), a.sqEntry.display.successMessage(b.broadbandTechType + "-churn", c), a.sqEntry.hide.findOutMoreButton("adsl", b.broadbandTechType + "-churn"), a.sqEntry.display.productSelection(), "adsl1" === b.broadbandTechType && a.sectionTitle.showTaglineForADSL1(), b.fireAnalytics && (a.adobeAnalytics.global.sq.success("ADSL", "CHURN_ORDER", "adsl", "standard"), a.analytics.methods.sq.sqSuccess("ADSL", "CHURN_ORDER", "adsl")), a.livePerson.sq.completeSQ()
                                }
                            },
                            fail: {
                                SQ_ERROR: function(b) {
                                    console.log("adsl adsl fail SQ_ERROR"), a.sqEntry.hide.form(), a.eoiEntry.display.message("sq-error"), a.eoiEntry.display.message("sq-error-form"), a.eoiEntry.display.fields("eoi-entry-full"), $("#eoi-entry-full-address").val($("#sq-street-address").val()), a.forms.initShiftLabel($("#eoi-entry-full-address")), a.eoiEntry.bind.validation(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("SQ_ERROR", "ADSL", "adsl", "standard"), a.analytics.methods.sqFailures.failureMessages("SQ_ERROR", "ADSL", "adsl")), a.livePerson.sq.sqError()
                                },
                                TWICE_ORDER: function(b) {
                                    console.log("adsl adsl fail TWICE_ORDER"), a.sqEntry.hide.form(), a.eoiEntry.display.message("duplicate-address"), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("TWICE_ORDER", "ADSL", "adsl", "standard"), a.analytics.methods.sqFailures.failureMessages("TWICE_ORDER", "ADSL", "adsl")), a.livePerson.sq.cannotBelongError()
                                },
                                INVALID_ADDRESS: function(b, c) {
                                    console.log("nbn adsl fail INVALID_ADDRESS"), a.sqEntry.display.successMessage("landline-request", c), a.sqEntry.display.landlineEntry(), a.sqEntry.display.phoneMatchMessage(), a.sqEntry.display.sqCheckButton(), a.LANDLINECHECK = !0, $(".selected-address").text(a.sqEntry.getSQDisplayAddres()), a.sqEntry.hide.loadingMessages(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("INVALID_ADDRESS", "ADSL", "adsl", "standard"), a.analytics.methods.sqFailures.failureMessages("INVALID_ADDRESS", "ADSL", "adsl")), a.livePerson.sq.invalidPhoneError()
                                },
                                SERVICE_NOT_AVAILABLE: function(b) {
                                    console.log("adsl adsl fail SERVICE_NOT_AVAILABLE"), a.sqEntry.hide.form(), a.eoiEntry.display.message("service-error"), a.eoiEntry.display.message("sq-error-form"), a.eoiEntry.display.fields("eoi-entry-full"), $("#eoi-entry-full-address").val($("#sq-street-address").val()), a.forms.initShiftLabel($("#eoi-entry-full-address")), a.eoiEntry.bind.validation(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("SERVICE_NOT_AVAILABLE", "ADSL", "adsl", "standard"), a.analytics.methods.sqFailures.failureMessages("SERVICE_NOT_AVAILABLE", "ADSL", "adsl")), a.livePerson.sq.completeSQ()
                                },
                                ADDR_NOT_IN_WHITELIST: function(b) {
                                    console.log("adsl adsl fail ADDR_NOT_IN_WHITELIST"), a.sqEntry.hide.form(), a.eoiEntry.display.message("service-error"), a.eoiEntry.display.message("sq-error-form"), a.eoiEntry.display.fields("eoi-entry-full"), $("#eoi-entry-full-address").val($("#sq-street-address").val()), a.forms.initShiftLabel($("#eoi-entry-full-address")), a.eoiEntry.bind.validation(), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("ADDR_NOT_IN_WHITELIST", "ADSL", "adsl", "standard"), a.analytics.methods.sqFailures.failureMessages("ADDR_NOT_IN_WHITELIST", "ADSL", "adsl")), a.livePerson.sq.cannotBelongError()
                                }
                            }
                        }
                    }
                },
                eoiResult: {
                    full: {
                        success: function() {
                            console.log("eoiResult full success"), a.eoiEntry.hide.message("contact-form-success"), a.eoiEntry.display.message("full-form-success")
                        },
                        fail: function() {
                            console.log("eoiResult full fail")
                        }
                    },
                    contact: {
                        success: function() {
                            console.log("eoiResult contact success"), a.eoiEntry.hide.message("eoi-message-contact-form"), a.eoiEntry.hide.fields("eoi-entry-contact"), a.eoiEntry.display.message("eoi-message-contact-form-success")
                        },
                        fail: function() {
                            console.log("eoiResult contact fail")
                        }
                    }
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.targetMobileApp = function() {
            var a, b = navigator.userAgent,
                c = window.navigator.standalone,
                d = b.match(/iPhone|iPad|iPod/i),
                e = (b.match(/Android/i), b.match(/IEMobile/i), !!window.chrome, /safari/.test(b.toLowerCase()));
            return {
                init: function() {
                    a = this, d && (c || e || ($("#ctn-title").addClass("hidden"), $(".authenticated-page-subtitle").addClass("hidden"), $(".authenticated-page-cards").css({
                        background: "#ffffff"
                    }), a.beforeLoggedIn(), a.afterLoggedIn(), a.display.overrideStyles()))
                },
                afterLoggedIn: function() {
                    a.hide.headerSection(), a.hide.footerSection()
                },
                beforeLoggedIn: function() {
                    a.set.iOS()
                },
                set: {
                    iOS: function() {
                        $("[data-controller=forgottenpassword]").attr("id", "is-ios")
                    },
                    android: function() {
                        $("[data-controller=forgottenpassword]").attr("id", "is-android")
                    },
                    windows: function() {
                        $("[data-controller=forgottenpassword]").attr("id", "is-windows")
                    }
                },
                display: {
                    headerSection: function() {
                        $("#header").removeClass("hidden")
                    },
                    footerSection: function() {
                        $(".footer").removeClass("hidden")
                    },
                    overrideStyles: function() {
                        $(".content-container").css({
                            paddingTop: "0"
                        }), $("#ctn-cards").css({
                            background: "#ffffff"
                        }), $("#cmp-manage-payment").css({
                            background: "#ffffff"
                        }), $(".authenticated-cards").css({
                            background: "#ffffff"
                        }), $(".authenticated-page-summary").css({
                            background: "#01BFDE"
                        }), $(".txt-data-summary").css({
                            color: "#FFFFFF"
                        }), $(".summary-item-name").css({
                            color: "#FFFFFF"
                        }), $(".summary-item-cost").css({
                            color: "#FFFFFF"
                        }), $(".summary-tmc p").css({
                            color: "#FFFFFF"
                        })
                    }
                },
                hide: {
                    headerSection: function() {
                        $("#header").addClass("hidden")
                    },
                    footerSection: function() {
                        $(".footer").addClass("hidden")
                    }
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.tooltip = function() {
            var a = {
                disableHover: !0
            };
            $(".lt-ie9").length > 0 && (a.speed = 0), $("a.fn_tooltip").accessibleTooltip(a)
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.unsubscribe = function() {
            var a;
            return {
                init: function() {
                    a = this, a.processUnsubscribeToken()
                },
                processUnsubscribeToken: function() {
                    function a(a) {
                        a = a.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                        var b = new RegExp("[\\?&]" + a + "=([^&#]*)"),
                            c = b.exec(location.search);
                        return null === c ? "" : decodeURIComponent(c[1].replace(/\+/g, " "))
                    }

                    function b() {
                        var b = "";
                        if (b = a("token"), "" == b) {
                            var c = window.location.pathname.split("/");
                            c.length >= 3 && (b = c[2])
                        }
                        return b
                    }
                    if (0 != $("#header[data-page-type='unsubscribe']").length) {
                        var c = b();
                        if (console.log("unsubscrie(): token=" + c), "" == c) console.log("unsubscribe(): No token found.");
                        else {
                            var d = $.get("/bin/belong/customer/unsubscribe?unsubscribeToken=" + c);
                            d.done(function(a) {
                                console.log("unsubscribeSuccess(): result", a)
                            }), d.fail(function(a) {
                                console.log("unsubscribeFail(): xhr", a)
                            })
                        }
                    }
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.utils = function() {
            return {
                replaceAll: function(a, b, c) {
                    var d = function(a) {
                        return a.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1")
                    };
                    return a.replace(new RegExp(d(b), "g"), c)
                },
                replacePlaceholderCopy: function(b, c, d) {
                    var e = b.text();
                    b.text(a.utils.replaceAll(e, d, c))
                },
                getTodaysDate: function() {
                    var a = new Date,
                        b = a.getDate(),
                        c = a.getMonth() + 1;
                    return 10 > b && (b = "0" + b), 10 > c && (c = "0" + c), b + "/" + c + "/" + a.getFullYear()
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.validation = function() {
            var b, c = [];
            return {
                init: function() {
                    b = this, b.loadCommonMethods()
                },
                setupFormValidation: function(b, c, d) {
                    console.log("Validate(): form=", c[0]), c[0] && $.data(c[0], "validator", !1);
                    var e = {};
                    if (e.rules = {}, e.messages = {}, e.debug = !1, 1 == d.hasOwnProperty("debug") && (e.debug = d.debug), e.onkeyup = !1, 1 == d.hasOwnProperty("onkeyup") && (e.onkeyup = d.onkeyup), e.focusCleanup = !1, 1 == d.hasOwnProperty("focusCleanup") && (e.focusCleanup = d.focusCleanup), "function" == typeof d.errorPlacement && (e.errorPlacement = d.errorPlacement), e.focusInvalid = !0, 1 == d.hasOwnProperty("focusInvalid") && (e.focusInvalid = d.focusInvalid), d.hasOwnProperty("rules")) {
                        for (var f in d.rules) e.rules[f] = d.rules[f];
                        for (var g in e.rules) {
                            var h = e.rules[g];
                            for (var i in h) {
                                var f = h[i],
                                    j = a.responseMessage.getValidationMessage("global", g, i),
                                    k = a.responseMessage.getValidationMessage(b, g, i);
                                void 0 == e.messages[g] && (e.messages[g] = {}), e.messages[g][i] = j, "" != k && (e.messages[g][i] = k)
                            }
                        }
                    }
                    if (d.hasOwnProperty("messages"))
                        for (var g in d.messages) {
                            var h = d.messages[g];
                            for (var i in h) void 0 != e.messages[g] && void 0 != e.messages[g][i] ? e.messages[g][i] = h[i] : void 0 != e.messages[g] && void 0 == e.messages[g][i] ? (e.messages[g] = {}, e.messages[g][i] = h[i]) : (e.messages[g] = {}, e.messages[g][i] = h[i])
                        }
                    return c.validate(e)
                },
                validateForm: function(a) {
                    var b = a.form();
                    return b
                },
                resetForm: function(a) {
                    if ("undefined" != typeof a) {
                        a.resetForm(), a.reset();
                        var c = a.currentForm.id;
                        b.removeValidatorErrorLabels(c)
                    }
                },
                removeValidatorErrorLabels: function(a) {
                    if ("" != a) {
                        var b = $("#" + a + " label.error");
                        b.remove()
                    }
                },
                validateInput: function(a, b) {
                    var c = a.element(b);
                    return c
                },
                attachCustomMessage: function(a, b, d, e) {
                    var f = $("#" + a),
                        g = f.prop("name");
                    if (0 == a.length) throw "attachCustomMessage(): Element for ID '" + a + "' does not exist.";
                    var h = c.length + 1,
                        i = !0;
                    if ("undefined" != typeof e && (i = "false" == e ? !1 : !0), "undefined" == typeof d && (d = "error"), "error" == d.toLowerCase()) {
                        console.log("Adding error to host"), f.addClass("error");
                        var j = f.closest(".ctrl-holder");
                        j.length > 0 && j.addClass("error")
                    }
                    var k = "";
                    1 != i && (k = "hidden");
                    var l = a + "-custom-message-" + h,
                        m = '<label class="custom-message ' + d + " " + k + '" id="' + l + '" data-forElementID="' + a + '">' + b + "</label>",
                        n = $('[data-forElementID="' + a + '"], label[for="' + g + '"].error');
                    n.length > 0 ? n.last().after(m) : f.after(m);
                    var o = {
                        id: h,
                        hostElementID: a,
                        messageDomID: l,
                        typeClass: d
                    };
                    return c.push(o), o
                },
                removeCustomMessage: function(a, b) {
                    var d = a.messageDomID,
                        e = $("#" + d);
                    e.remove();
                    var f = !0;
                    if (f = "undefined" != typeof b && 0 == b ? !1 : !0, 1 == f) {
                        for (var g = "", h = "", i = 0; i < c.length; i++) {
                            var j = c[i];
                            j.id == a.id && (g = a.typeClass, h = a.hostElementID, c.splice(i, 1))
                        }
                        if ("error" == g.toLowerCase()) {
                            var k = $("#" + h);
                            console.log("Adding error to host"), k.removeClass("error");
                            var l = k.closest(".ctrl-holder");
                            l.length > 0 && l.removeClass("error")
                        }
                    }
                },
                resetCustomMessages: function() {
                    for (var a = 0; a <= c.length - 1; a++) {
                        var d = c[a];
                        b.removeCustomMessage(d, !1)
                    }
                    c = []
                },
                loadCommonMethods: function() {
                    jQuery.validator.addMethod("badPassword", function(a) {
                        var b = ["trustno1", "password1", "abcd1234"];
                        return -1 === $.inArray(a.toLowerCase(), b)
                    }, "For your security, we don't let you use common passwords (such as abcd1234)"), jQuery.validator.addMethod("disallowedCharacters", function(a) {
                        var b = /^[^<>+;\[\]\{\}\"]+$/i.test(a) || "" === a;
                        return b
                    }, "Looks like there may be a typo. This field cannot include some of those characters."), jQuery.validator.addMethod("backendEmail", function(a) {
                        return /^.+@.+\..{2,4}$/.test(a)
                    }, "Looks like there may be a typo. This field can only include a valid email address."), jQuery.validator.addMethod("australianMobileNumber", function(a) {
                        if ("" !== a) {
                            var c = b.helpers.validateAustralianMobileNumber(a);
                            return c
                        }
                        return !0
                    }, "Please specify a valid mobile number"), jQuery.validator.addMethod("australianPhoneNumber", function(a) {
                        if ("" != a) {
                            var c = b.helpers.validateAustralianLandlineNumber(a);
                            return c
                        }
                        return !0
                    }, "Looks like there's a typo in that phone number. Try again."), jQuery.validator.addMethod("australianMobileOrLandlineNumber", function(a) {
                        var c = !0;
                        return "" != a ? (c = b.helpers.validateAustralianMobileNumber(a), 0 == c ? c = b.helpers.validateAustralianLandlineNumber(a) : c) : !0
                    }, "Looks like there's a typo in that phone number. Try again."), jQuery.validator.addMethod("birthDateFormat", function(a) {
                        return /((\d{2})|(\d))\/((\d{2})|(\d))\/(\d{2})/.test(a)
                    }, "You've missed some of your date of birth information. Try again perhaps?"), jQuery.validator.addMethod("validAustralianDateString", function(b) {
                        var c = a.validation.helpers.isValidDate(b, "AU");
                        return c
                    }, "It looks like you have not provided a valid date."), jQuery.validator.addMethod("exactlength", function(a, b, c) {
                        return -1 === c ? !0 : this.optional(b) || a.length === c
                    }, "Please enter exactly {0} characters."), jQuery.validator.addMethod("passwordComplexity", function(a) {
                        return /^(?=.*\d)(?=.*[a-zA-Z]).*$/.test(a)
                    }, "So it's as secure as possible, your password needs to have a mix of letters and numbers."), jQuery.validator.addMethod("notEqualToEmail", function(a, b, c) {
                        var d = $(c.emailAddressFieldID).val();
                        return a !== d.substr(0, d.indexOf("@")) && a !== d
                    }, "For your security, we don't let you use your email address as your password."), jQuery.validator.addMethod("overEighteen", function(a, b, c) {
                        var d, e, f = $(c.dobFieldID).val(),
                            g = new Date(parseInt(f.substr(6, 4), 10), parseInt(f.substr(3, 2), 10) - 1, parseInt(f.substr(0, 2), 10)),
                            h = new Date;
                        return d = h.getFullYear() - g.getFullYear(), e = h.getMonth() - g.getMonth(), (0 > e || 0 === e && h.getDate() < g.getDate()) && (d -= 1), d >= 18
                    }, "You must be at least 18 years old to join Belong."), jQuery.validator.addMethod("validID", function(a) {
                        return !!a.match(/^\d{6}$/)
                    }, "Please reenter your Customer ID."), jQuery.validator.addMethod("priceAmount", function(a) {
                        return !!a.match(/^[\d]+(\.\d{2})?$/i)
                    }, "That doesn't seem to be a valid amount. Please use numbers and decimals only."), jQuery.validator.addMethod("equalToCaseSensitive", function(a, b, c) {
                        return $(c.compareToFieldID).val().toString().toLowerCase() === a.toLowerCase()
                    }, "That doesn't match your entered email address."), jQuery.validator.addMethod("notEqual", function(a, b, c) {
                        return this.optional(b) || a !== c
                    })
                },
                helpers: {
                    cleanAustralianPhoneNumber: function(a) {
                        if (null == a) return "";
                        var b = a.replace(/\s|-|\.|\(|\)|\,/g, "");
                        return b
                    },
                    validateAustralianMobileNumber: function(a) {
                        var c = b.helpers.cleanAustralianPhoneNumber(a);
                        return 10 != c.length ? !1 : /(^04\d{8}$)|(^610?4\d{8}$)|(^\+610?4\d{8}$)/.test(c)
                    },
                    validateAustralianLandlineNumber: function(a) {
                        var c = b.helpers.cleanAustralianPhoneNumber(a);
                        if (10 != c.length) return !1;
                        var d = /^\({0,1}((0)(2|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/.test(c);
                        return d
                    },
                    isValidDate: function(a, b) {
                        var c = function(a, b, c) {
                            var d = new Date(a, b - 1, c),
                                e = d.getFullYear().toString() + (d.getMonth() + 1).toString() + d.getDate().toString(),
                                f = a.toString() + b.toString() + c.toString(),
                                g = f == e;
                            return console.log("isValidDate.isDate(" + g + "): yearInt=" + a + ", monthInt=" + b + ", dayInt=" + c + " - givenDate=" + f + " - convertedDate=" + e), g
                        };
                        "undefined" == typeof b || null === b || "" === b ? b = "AU" : "ISO" == b.toUpperCase() || (b = "AU");
                        var d, e, f, g, h, i;
                        if ("AU" == b) {
                            if (10 != a.length) return !1;
                            var d = a.substr(0, 2),
                                f = a.substr(3, 2),
                                h = a.substr(6, 4),
                                e = parseInt(d),
                                g = parseInt(f),
                                i = parseInt(h)
                        } else if ("ISO" === b) {
                            if (10 != a.length) return !1;
                            var h = a.substr(6, 4),
                                f = a.substr(3, 2),
                                d = a.substr(0, 2),
                                e = parseInt(d),
                                g = parseInt(f),
                                i = parseInt(h)
                        }
                        if (isNaN(e) || isNaN(g) || isNaN(i)) return !1;
                        var j = c(i, g, e);
                        return console.log("isValidDate(" + a + "): isValid=" + j), j
                    }
                },
                commonRules: {},
                commonMessages: {}
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.eoiEntry = function() {
            var b;
            return {
                init: function() {
                    return 0 === $(".eoi-entry").length ? !1 : void(b = this)
                },
                createEOIRequest: function(c) {
                    var d = "",
                        e = (a.sqEntry.get.sourceType(), b.get.eoiRequestObject(c));
                    console.log("requestData=" + JSON.stringify(e)), e.sourceType = a.sqEntry.get.sourceType(), e.requestType = "createEOI", a.ajax(d, e, function(d) {
                        var e = d.success ? "success" : "fail";
                        b.hide.allMessages(), b.display.message("contact-form-success"), console.log("result:", d), console.log("eoiResult", "formType: " + c + " |", "resultStatus: " + e), a.sqRules.eoiResult[c][e]()
                    }, function(a) {
                        console.log("fail createEOIRequest", a)
                    })
                },
                display: {
                    form: function() {
                        $(".eoi-entry").removeClass("hidden")
                    },
                    message: function(c) {
                        b.display.form(), $(".eoi-message." + c).removeClass("hidden"), a.utils.replacePlaceholderCopy($(".eoi-message." + c).find("h3"), $("#eoi-entry-contact-name").val(), "$CONTACT_NAME$"), $(".try-again, .try-again-link").off("click.sqtryagain").on("click.sqtryagain", function() {
                            a.adobeAnalytics.global.eoi.tryAgain("standard"), a.analytics.methods.eoi.tryAgain()
                        }).on("click.sqtryagain", a.sqEntry.bind.resetSqEntry), $(".contact-me").off("click.eoicontactme").on("click.eoicontactme", b.bind.displayContactMe)
                    },
                    fields: function(c) {
                        b.display.form(), a.adobeAnalytics.global.eoi.eoiForm(a.sqEntry.get.sourceType(), c, "standard"), a.analytics.methods.eoi.eoiForm(a.sqEntry.get.sourceType(), c), $(".eoi-entry .eoi-fields." + c).removeClass("hidden"), $(".eoi-let-me-know-button").off("click.letmeknow").on("click.letmeknow", b.bind.clickLetMeKnow), $(".eoi-keep-me-informed-button").off("click.keepmeinformed").on("click.keepmeinformed", b.bind.clickKeepMeInformed)
                    }
                },
                hide: {
                    form: function() {
                        $(".eoi-entry").addClass("hidden")
                    },
                    fields: function(a) {
                        $(".eoi-fields." + a).addClass("hidden")
                    },
                    message: function(a) {
                        $(".eoi-message." + a).addClass("hidden")
                    },
                    allMessages: function() {
                        $(".eoi-message").addClass("hidden")
                    }
                },
                get: {
                    eoiRequestObject: function(b) {
                        switch (b) {
                            case "contact":
                                return {
                                    name: $("#eoi-entry-contact-name").val(),
                                    contactEmail: $("#eoi-entry-contact-email").val(),
                                    contactMobile: a.validation.helpers.cleanAustralianPhoneNumber($("#eoi-entry-contact-mobile").val()),
                                    contactLandline: "",
                                    contactMethod: "EMAIL",
                                    freeTextAddress: $("#eoi-entry-contact-address").val(),
                                    eoiReason: "ADDRESS_HELP_REQUIRED"
                                };
                            default:
                                return {
                                    name: $("#eoi-entry-full-name").val(),
                                    contactEmail: $("#eoi-entry-full-email").val(),
                                    contactMobile: a.validation.helpers.cleanAustralianPhoneNumber($("#eoi-entry-full-mobile").val()),
                                    contactLandline: a.validation.helpers.cleanAustralianPhoneNumber($("#eoi-entry-full-landline").val()),
                                    contactMethod: $("#eoi-entry-full-contact-method").find("option:selected").val(),
                                    freeTextAddress: $("#eoi-entry-contact-address").val(),
                                    eoiReason: "ADDR_NOT_IN_WHITELIST"
                                }
                        }
                    }
                },
                bind: {
                    clickLetMeKnow: function(c) {
                        return c.preventDefault(), a.validation.validateForm(a.eoiValidator) ? ($("html, body").velocity({
                            scrollTop: 150
                        }, 500), b.createEOIRequest("contact"), a.adobeAnalytics.global.eoi.letMeKnow(a.sqEntry.get.sourceType(), "standard"), void a.analytics.methods.eoi.letMeKnow(a.sqEntry.get.sourceType())) : !1
                    },
                    clickKeepMeInformed: function(c) {
                        return c.preventDefault(), a.validation.validateForm(a.eoiValidator) ? ($("html, body").velocity({
                            scrollTop: 150
                        }, 500), b.hide.fields("eoi-entry-full"), b.createEOIRequest("full"), a.adobeAnalytics.global.eoi.keepMeInformed(a.sqEntry.get.sourceType(), "standard"), void a.analytics.methods.eoi.keepMeInformed(a.sqEntry.get.sourceType())) : !1
                    },
                    displayContactMe: function(c) {
                        c.preventDefault(), b.hide.allMessages(), a.sqEntry.hide.form(), a.adobeAnalytics.global.eoi.contactMe(a.sqEntry.get.sourceType(), "standard"), a.analytics.methods.eoi.contactMe(a.sqEntry.get.sourceType()), $("#eoi-entry-contact-address").val($("#sq-street-address").val()), a.forms.initShiftLabel($("#eoi-entry-contact-address")), b.display.message("contact-form"), b.display.fields("eoi-entry-contact"), $("#eoi-entry-contact-name").focus(), b.bind.validation()
                    },
                    validation: function() {
                        a.eoiValidator = a.validation.setupFormValidation("eoi-entry", $("#eoi"), {
                            rules: {
                                "eoi-entry-contact-name": {
                                    required: !0,
                                    maxlength: 255,
                                    disallowedCharacters: !0
                                },
                                "eoi-entry-contact-email": {
                                    required: !0,
                                    backendEmail: !0,
                                    disallowedCharacters: !0,
                                    maxlength: 255
                                },
                                "eoi-entry-contact-mobile": {
                                    required: !0,
                                    australianMobileNumber: !0,
                                    disallowedCharacters: !0
                                },
                                "eoi-entry-full-name": {
                                    required: !0,
                                    disallowedCharacters: !0
                                },
                                "eoi-entry-full-address": {
                                    required: !0,
                                    disallowedCharacters: !0
                                },
                                "eoi-entry-full-landline": {
                                    required: function() {
                                        return "LANDLINE" === $("#eoi-entry-full-contact-method").val()
                                    },
                                    australianPhoneNumber: !0
                                },
                                "eoi-entry-full-mobile": {
                                    required: function() {
                                        return "MOBILE" === $("#eoi-entry-full-contact-method").val()
                                    },
                                    australianMobileNumber: !0,
                                    disallowedCharacters: !0
                                },
                                "eoi-entry-full-email": {
                                    required: function() {
                                        return "EMAIL" === $("#eoi-entry-full-contact-method").val()
                                    },
                                    disallowedCharacters: !0
                                },
                                "eoi-entry-full-contact-method": {
                                    required: function() {
                                        return "" === $("#eoi-entry-full-contact-method").val()
                                    }
                                }
                            }
                        }), a.eoiValidator.resetForm()
                    }
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.forms = a.forms || {}, a.forms.date = function() {
            var a, b = new Date,
                c = b.getFullYear();
            return {
                init: function() {
                    a = this, $(".date-dropdown").each(function() {
                        var b = $(this);
                        b.find("select").each(function() {
                            $(this).on("change", a.events.onUpdate)
                        })
                    })
                },
                methods: {
                    setYearsForDob: function(a) {
                        for (var b = c - 17; b > c - 100; b -= 1) a.append('<option value="' + b + '">' + b + "</option>")
                    },
                    checkLeapYear: function(a) {
                        return console.log("checkLeapYear(): year=" + a), 0 === a ? (console.log("checkLeapYear(): branch 0 - NO YEAR SELECTED"), !1) : a % 4 === 0 ? a % 100 !== 0 ? (console.log("checkLeapYear(): branch 1 - YES"), !0) : a % 400 === 0 ? (console.log("checkLeapYear(): branch 2 - YES"), !0) : (console.log("checkLeapYear(): branch 3 - NO"), !1) : (console.log("checkLeapYear(): branch 4 - NO"), !1)
                    },
                    selectDay: function(a, b) {
                        console.log("selectDay(): selectedDay=" + a);
                        var c = b;
                        1 == a.length && (a = "0" + a), c.find('option[value="' + a + '"]').attr("selected", "selected").prop("selected", !0), c.closest(".simple-select").find(".simple-label").text(a)
                    }
                },
                events: {
                    onUpdate: function() {
                        var a = $(this),
                            b = a.closest(".date-dropdown").find('select[id$="day"]'),
                            c = a.closest(".date-dropdown").find('select[id$="month"]'),
                            d = a.closest(".date-dropdown").find('select[id$="year"]'),
                            e = a.closest(".date-dropdown").find("input"),
                            f = b.find("option:selected").val(),
                            g = c.find("option:selected").val(),
                            h = d.find("option:selected").val(),
                            i = (parseInt(g, 10), "" !== h ? parseInt(d.find("option:selected").val(), 10) : "");
                        console.log("entering onUpdate: " + a.prop("id"));
                        var j = f + "/" + g + "/" + i;
                        console.log("fullDate=" + j), e.val(j)
                    }
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.collapsibles = function() {
            var a, b = $(".collapsible-container");
            return {
                init: function() {
                    a = this, b.find(".collapsible-icon").off("click.collapsibles").on("click.collapsibles", a.events.clickEvent)
                },
                methods: {
                    toggleCollapsibleContainer: function(a, b) {
                        "undefined" == typeof b || null == b || "" == b ? (a.toggleClass("collapsed"), console.log("toggleCollapsibleContainer(): state=Toggle automatic")) : "expand" == b.toLowerCase() ? (a.removeClass("collapsed"), console.log("toggleCollapsibleContainer(): state=Expanded")) : "collapse" == b.toLowerCase() ? (a.addClass("collapsed"), console.log("toggleCollapsibleContainer(): state=Collapsed")) : (a.toggleClass("collapsed"), console.log("toggleCollapsibleContainer(): state=Unknown"));
                        var c = a.find(".collapsible-icon > span");
                        2 === c.length && c.toggleClass("hidden");
                        var d = a.find(".collapsible-readonly-title > span");
                        2 === d.length && d.toggleClass("hidden")
                    }
                },
                events: {
                    clickEvent: function(b) {
                        var c = $(this).closest(".collapsible-container");
                        a.methods.toggleCollapsibleContainer(c), b.preventDefault()
                    }
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.mobo = function() {
            var b, c, d, e, f;
            return {
                init: function() {
                    if (b = this, !a.GD.isLoggedIn || !a.GD.isAgent) return !1;
                    if (c = $("#mobo-change-back-to-me"), d = $("#mobo-impersonate"), e = $("#mobo-header"), f = e.data("interactionType"), e.removeClass("hidden"), $("#main").css("padding-top", 80), a.GD.isMobo) switch (b.methods.enableMobo(), f) {
                        case "agent-only":
                            b.methods.disablePage();
                            break;
                        case "customer-only":
                        case "all":
                            b.methods.enablePage()
                    } else switch (b.methods.disableMobo(), f) {
                        case "customer-only":
                            b.methods.disablePage();
                            break;
                        case "agent-only":
                        case "all":
                            b.methods.enablePage()
                    }
                    b.methods.validateMobo(), d.off("click.impersonate").on("click.impersonate", b.events.impersonate), c.off("click.backToMe").on("click.backToMe", b.events.backToMe)
                },
                methods: {
                    disablePage: function() {
                        $("#mobo-header .mobo-disable-overlay").removeClass("hidden")
                    },
                    enablePage: function() {
                        $("#mobo-header .mobo-disable-overlay").addClass("hidden")
                    },
                    enableMobo: function() {
                        $(".mobo-inactive").addClass("hidden"), $(".mobo-active").removeClass("hidden"), $(".mobo-active .customer-name").text(a.GD.moboFirstname + " " + a.GD.moboLastname)
                    },
                    disableMobo: function() {
                        $(".mobo-active").addClass("hidden"), $(".mobo-inactive").removeClass("hidden")
                    },
                    grantPermissions: function(a, b, c, d) {
                        return c = c || function() {}, d = d || function() {}, $.ajax({
                            url: "/apps/userImpersonate/?agentUser=" + a + "&customerUser=" + b,
                            type: "GET",
                            dataType: "html",
                            success: c,
                            error: d
                        })
                    },
                    validateMobo: function() {
                        $("#mobo").length ? (a.moboValidator = a.validation.setupFormValidation("mobo", $("#mobo"), {
                            rules: {
                                "mobo-email-address": {
                                    required: !0,
                                    backendEmail: !0,
                                    disallowedCharacters: !0,
                                    maxlength: 255
                                }
                            }
                        }), a.moboValidator.resetForm()) : console.log("No mobo form found!")
                    }
                },
                events: {
                    impersonate: function(c) {
                        c.preventDefault();
                        var d = $("#mobo-email-address").val(),
                            e = a.GD.loginEmail;
                        return a.validation.validateForm(a.moboValidator) ? (a.validation.resetCustomMessages(), void b.methods.grantPermissions(e, d, function(b) {
                            "" !== b && 401 !== parseFloat(b) ? window.location.reload() : a.validation.attachCustomMessage("mobo-email-address", "The user " + d + " cannot be managed as the email is not recognised.", "error", !0)
                        }, function(b) {
                            console.log("error", b), a.validation.attachCustomMessage("mobo-email-address", "The user " + d + " cannot be managed as the email is not recognised.", "error", !0)
                        })) : !1
                    },
                    backToMe: function(b) {
                        b.preventDefault(), $.ajax({
                            url: "?mobo=-",
                            type: "GET",
                            dataType: "html",
                            success: function(b) {
                                console.log("success", b), location.reload(), a.adobeAnalytics.global.agent.backToMe(), a.analytics.methods.agent.backToMe()
                            },
                            error: function(a) {
                                console.log("error", a)
                            }
                        })
                    }
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.navMobile = function() {
            function a(a) {
                a.preventDefault(), f.hasClass("hide") ? d() : f.hasClass("open") && c()
            }

            function b() {
                setTimeout(function() {
                    f.removeClass("close"), f.addClass("hide")
                }, 250)
            }

            function c() {
                f.removeClass("open"), f.addClass("close"), b()
            }

            function d() {
                f.addClass("open"), f.removeClass("close"), f.removeClass("hide")
            }
            var e = $(".nav-toggle"),
                f = $(".mobile-menu"),
                g = function() {
                    e.off("click").on("click", a)
                };
            return {
                init: g
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.sqAddressLib = function() {
            return {
                methods: {
                    preClick: function() {
                        var b = $("#sq-street-address"),
                            c = b.val(),
                            d = $("#sq-post-code"),
                            e = $("#confirm-address");
                        if ($(".error-select-an-address").addClass("hidden"), $(".street-address-select").hasClass("hidden") || "" !== e.find("option:selected").val()) {
                            if (a.validation.validateForm(a.sqValidator)) {
                                if ($("#sq-form").length > 0 ? (a.CLEARSQONSUBMIT && (sessionStorage.removeItem("sqModel"), a.join.model.sq = {
                                        accessType: "",
                                        defaultSize: "",
                                        phoneNumber: "",
                                        serviceAddressId: "",
                                        serviceExchange: "",
                                        serviceStreetNumber: "",
                                        serviceStreet: "",
                                        serviceStreetType: "",
                                        serviceSuburb: "",
                                        serviceState: "",
                                        servicePostcode: "",
                                        serviceSubAddressDetails: "",
                                        bpsError: !1,
                                        isNbn: !1,
                                        orderType: "",
                                        isPreprovisioned: !1,
                                        promotionId: ""
                                    }, $(".street-address-select .simple-label").text($(".street-address-select select option").eq(0).text()), a.sqFormVM.landlineNumberRequired(""), a.CLEARSQONSUBMIT = !1), a.sqFormVM.state("LOADING"), d.val(c.replace(/^\s+|\s+$/g, "").slice(-4))) : (a.sqEntry.hide.allMessages(), a.sqEntry.display.loadingMessages(), d.val(c.replace(/^\s+|\s+$/g, "").slice(-4)), a.sqEntry.bind.clickLandlineEntry()), console.log("EVE.Landlinecheck=" + a.LANDLINECHECK), a.LANDLINECHECK) {
                                    if ($("#sq-form").length > 0) "YES" === a.sqFormVM.landlineNumberRequired() ? (a.adobeAnalytics.global.sq.landlineOption(a.sqRequest.get.pageSource(), "Yes", "modal"), a.analytics.methods.sq.landlineOption(a.sqRequest.get.pageSource(), "Yes")) : (a.adobeAnalytics.global.sq.landlineOption(a.sqRequest.get.pageSource(), "No", "modal"), a.analytics.methods.sq.landlineOption(a.sqRequest.get.pageSource(), "No"));
                                    else if ($("#landline-entry-yes").is(":checked")) {
                                        var f = $(".sq-container").offset().top - 200;
                                        $("html, body").animate({
                                            scrollTop: f
                                        }, 500), a.adobeAnalytics.global.sq.landlineOption(a.sqEntry.get.sourceType(), "Yes", "standard"), a.analytics.methods.sq.landlineOption(a.sqEntry.get.sourceType(), "Yes")
                                    } else {
                                        a.adobeAnalytics.global.sq.landlineOption(a.sqEntry.get.sourceType(), "No", "standard"), a.analytics.methods.sq.landlineOption(a.sqEntry.get.sourceType(), "No");
                                        var f = $(".sq-loading").offset().top - 150;
                                        $("html, body").animate({
                                            scrollTop: f
                                        }, 500)
                                    }
                                    return $("#sq-form").length > 0 ? (a.adobeAnalytics.global.sq.check(a.sqRequest.get.pageSource(), "2", "modal"), a.analytics.methods.sq.sqCheck(a.sqRequest.get.pageSource(), "2")) : (a.adobeAnalytics.global.sq.check(a.sqEntry.get.sourceType(), "2", "standard"), a.analytics.methods.sq.sqCheck(a.sqEntry.get.sourceType(), "2")), a.sqAddressLib.methods.postClick(!0), !1
                                }
                                return a.REFINEDADDRESS ? ($("#sq-form").length > 0 ? (a.adobeAnalytics.global.sq.check(a.sqRequest.get.pageSource(), "2", "modal"), a.analytics.methods.sq.sqCheck(a.sqRequest.get.pageSource(), "2")) : (a.adobeAnalytics.global.sq.check(a.sqEntry.get.sourceType(), "2", "standard"), a.analytics.methods.sq.sqCheck(a.sqEntry.get.sourceType(), "2")), a.sqAddressLib.methods.postClick(!0), !1) : ($("#sq-form").length > 0 ? (a.adobeAnalytics.global.sq.check(a.sqRequest.get.pageSource(), "1", "modal"), a.analytics.methods.sq.sqCheck(a.sqRequest.get.pageSource(), "1")) : (a.adobeAnalytics.global.sq.check(a.sqEntry.get.sourceType(), "1", "standard"), a.analytics.methods.sq.sqCheck(a.sqEntry.get.sourceType(), "1")), !0)
                            }
                            return !1
                        }
                        return $(".error-select-an-address").removeClass("hidden"), !1
                    },
                    postClick: function(b) {
                        var c;
                        b = b || !1, "undefined" != typeof globalAddressList ? 1 === globalAddressList.length || b ? 1 == a.sqExecute.forceAddressList ? (c = "multiple", a.sqExecute.forceAddressList = !1) : (1 === globalAddressList.length && ($("#sq-form").length > 0 ? a.sqRequest.set.addressDetails(globalAddressList[0]) : a.sqEntry.set.addressDetails(globalAddressList[0])), $("#sq-form").length > 0 ? a.sqRequest.set.landlineDetails() : a.sqEntry.set.landlineDetails(), c = "single") : c = "multiple" : c = "none", console.log("addressResult", "addressesReturned: " + c), $("#sq-form").length > 0 ? a.sqModal.sqRules.addressResult[c]() : a.sqRules.addressResult[c]()
                    }
                },
                events: {}
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.sqDebug = function() {
            var b;
            return {
                init: function() {
                    b = this, a.DEVSWITCH && 0 !== $(".sq-entry").length && ($("#sq-check-button").off("click.sqdebugclick").on("click.sqdebugclick", b.events.debug.clickCheckSq), b.populateGlobalAddressList()), a.DEVSWITCH && $("#sq-form").length > 0 && ($("#sq-check-button").off("click.sqdebugclick").on("click.sqdebugclick", b.events.debug.clickCheckSq), b.populateGlobalAddressList()), a.DEVSWITCH && b.populateSqConfig()
                },
                populateGlobalAddressList: function() {
                    globalAddressList = [{
                        addressId: "160134037",
                        subaddress: "",
                        streetNumber: "253",
                        streetName: "Hoddle Street",
                        suburb: "Collingwood, VIC 3066",
                        confidence: null,
                        fullAddress: '{"addressIdentification":[{"type":"ID","identifier":"160134037","issuer":"ADBOR"}],"addressStatus":"CONFIRMED","statusOfPrimaryAddress":"CONFIRMED","addressType":"PROPERTY","exchange":"CWOD","localityDetails":{"state":"VIC","country":"AUSTRALIA","locality":"COLLINGWOOD","postCode":"3066"},"propertyAddressDetails":{"streetDetails":{"streetName":"HODDLE","streetType":"STREET"},"propertyNumberFrom":"253"}}'
                    }, {
                        addressId: "419642696",
                        subaddress: "Unit 133, ",
                        streetNumber: "253",
                        streetName: "Hoddle Street",
                        suburb: "Collingwood, VIC 3066",
                        confidence: 95,
                        fullAddress: '{"addressIdentification":[{"type":"ID","identifier":"400026054","issuer":"ADBOR"}],"addressStatus":"CONFIRMED","statusOfPrimaryAddress":"CONFIRMED","addressType":"PROPERTY","exchange":"CWOD","inputMatchConfidence":95,"localityDetails":{"state":"VIC","country":"AUSTRALIA","locality":"COLLINGWOOD","postCode":"3066"},"propertyAddressDetails":{"streetDetails":{"streetName":"HODDLE","streetType":"STREET"},"propertyNumberFrom":"253"},"subAddressDetails":{"flatUnitNumberFrom":"133","flatUnitTypeEnum":"UNIT","subAddressTypeEnum":"FLATUNIT"}}'
                    }], globalAddressList.pop()
                },
                populateSqConfig: function() {
                    sqConfig = {
                        addressConfirmFieldId: "confirm-address",
                        addressErrorMsgDivId: "error",
                        addressFieldId: "sq-street-address",
                        addressFoundMsg: "Your address has been confirmed.",
                        addressMsgDivChildElement: "",
                        addressMsgDivId: "address-msg-div",
                        addressNotListedOption: "My address is not listed above",
                        addressResultDivId: "address-result",
                        addressStatus: "",
                        clientTimeoutMs: "",
                        confRatingThreshold: "90",
                        errorMsg1: "That address could not be found, try again with a format like 'Flat 1, 60 Smith Street, Sydney'",
                        errorMsg2: "If you are experiencing some difficulties with your address, please call Telstra on 12 8887.",
                        errorMsg3: "",
                        excludeAddressType: "PARCEL_LOT_WITH_PLAN,PARCEL_LOT_WITHOUT_PLAN,PARCEL_OTHER,RURAL,POSTAL",
                        fullAddressHiddenInputId: "",
                        loadingIndicatorElement: "",
                        postcodeFieldId: "sq-post-code",
                        requestor: "Eve",
                        returnFullAddress: "",
                        searchButtonId: "sq-check-button",
                        selectAddressOption: "Select a refined address",
                        streetNames: "",
                        verificationUrl: "https://www.my.telstra.com.au/addresslookup/verifyAddress?callback=?",
                        callBackHookScriptName: null,
                        delay: 200,
                        minLength: 2,
                        stripInvalidAddressChars: "",
                        disableAutoComplete: !0
                    }
                },
                events: {
                    debug: {
                        clickCheckSq: function(c) {
                            return c.preventDefault(), b.populateGlobalAddressList(), a.sqAddressLib.methods.preClick() ? void setTimeout(function() {
                                "undefined" != typeof globalAddressList && 1 !== globalAddressList.length && ($(".street-address-entry").addClass("hidden"), $(".street-address-select").removeClass("hidden")), a.sqAddressLib.methods.postClick()
                            }, a.DEVDELAY) : !1
                        }
                    }
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.sqEntry = function() {
            var b, c = a.util;
            return {
                init: function() {
                    if (0 === $(".sq-entry").length) return !1;
                    b = this, jQuery.validator.addMethod("sqHasPostCode", function(a) {
                        return /^\d{3,4}$/.test(a.replace(/^\s+|\s+$/g, "").slice(-4))
                    }, "Please use the autocomplete to select an address.");
                    var d = $(".sq-anchor"),
                        e = $(".sq-container");
                    d.on("click", function() {
                        e.velocity("scroll", {
                            duration: 250,
                            easing: "ease-out",
                            offset: -100
                        }), $("#sq-street-address").focus()
                    }), b.bind.validation(), b.display.sqMobileCheckButton(), a.LANDLINECHECK = !1, a.REFINEDADDRESS = !1, c.isPrivateBrowsingMode() || "true" !== sessionStorage.getItem("sqDone") || b.reloadSession(), $("#landline-entry-yes-label").off("click").on("click", b.events.selectHaveLandline), $("#landline-entry-no-label").off("click").on("click", b.events.selectHaveLandline), $("#sq-street-address").off("focus").on("focus", b.events.streetAddressFocus)
                },
                getSQDisplayAddres: function() {
                    var b = "";
                    return b = "undefined" != typeof a.join.model.sq.serviceAddressId && null != a.join.model.sq.serviceAddressId && "" != a.join.model.sq.serviceAddressId ? a.join.model.helpers.getAddressString() : $("#sq-street-address").val()
                },
                serviceQualifyRequest: function(a) {
                    console.log("serviceQualifyRequest: sqType=" + a);
                    var c, d, e = b.get.serviceQualifyRequestObject(a),
                        f = function(a) {
                            b.processResult(a, "success", "adsl")
                        },
                        g = function(a) {
                            b.processResult(a, "success", "nbn")
                        },
                        h = function(a) {
                            b.processResult(a.responseJSON, "fail", "adsl")
                        },
                        i = function(a) {
                            b.processResult(a.responseJSON, "fail", "nbn")
                        };
                    "BPS" === a ? (c = $.post("/bin/belong/sq/nbn", e), c.done(g), c.fail(i)) : (d = $.post("/bin/belong/sq/adsl", e), d.done(f), d.fail(h))
                },
                processResult: function(c, d, e) {
                    console.log("sqEntry.processResult(): result=" + JSON.stringify(c)), console.log("sqEntry.processResult(): resultStatus=" + d), console.log("sqEntry.processResult(): product=" + e);
                    var f = {
                            fireAnalytics: !0,
                            broadbandTechType: ""
                        },
                        g = a.sqRequest.get.pageSource(),
                        h = c.responseType;
                    if (b.set.isPreprovisioned(c.isPreprovisioned), b.set.promotionId(c.promotionId), b.set.orderType(c.responseType), b.set.broadbandTechType(c.broadbandTechnologyType), b.set.extendedNetwork(c.extendedNetwork), "fail" !== d || "undefined" != typeof h && null !== h || (h = "SQ_ERROR"), c.broadbandTechnologyType) switch (f.broadbandTechType = c.broadbandTechnologyType.toLowerCase(), f.broadbandTechType) {
                        case "fttp":
                        case "fttn":
                        case "fttb":
                            h = c.serviceClass, e = "nbn", b.set.orderType(c.serviceClass)
                    }
                    if ("success" === d && f.fireAnalytics) {
                        var i = e;
                        switch (h) {
                            case "NBN_SERVICE_CLASS_ELEVEN":
                                i = "nbn-fttn";
                                break;
                            case "NBN_SERVICE_CLASS_TWELVE":
                            case "NBN_SERVICE_CLASS_THIRTEEN":
                                i = "nbn-" + f.broadbandTechType
                        }
                        c.extendedNetwork && (i = "ADSL-ExtendedNetwork"), a.adobeAnalytics.global.sq.success(g, h, i, "modal"), a.analytics.methods.sq.sqSuccess(g, h, i)
                    }
                    a.livePerson.sq.completeSQ(), "success" !== d || a.util.isPrivateBrowsingMode() || (sessionStorage.setItem("sqDone", !0), sessionStorage.setItem("product", e), sessionStorage.setItem("broadbandTechnologyType", f.broadbandTechType), sessionStorage.setItem("resultStatus", d), sessionStorage.setItem("resultCode", h), sessionStorage.setItem("sqModel", JSON.stringify(a.join.model.sq))), console.log(e, "product"), console.log(d, "resultStatus"), console.log(h, "resultCode"), console.log(f, "sqResultData"), a.sqRules.sqResult.home[e][d][h](f)
                },
                reloadSession: function() {
                    console.log("sqEntry - retrieving existing user session"), a.join.model.sq = JSON.parse(sessionStorage.getItem("sqModel")), console.log("reloadSession(): EVE.join.model.sq=" + JSON.stringify(a.join.model.sq));
                    var c = "join" === b.get.currentPage() ? "home" : b.get.currentPage(),
                        d = {
                            fireAnalytics: !1,
                            broadbandTechType: sessionStorage.getItem("broadbandTechnologyType").toLowerCase()
                        };
                    a.sqRules.sqResult[c][sessionStorage.getItem("product")][sessionStorage.getItem("resultStatus")][sessionStorage.getItem("resultCode")](d, !1), b.hide.streetAddressEntry(), b.hide.sqCheckButton()
                },
                display: {
                    form: function() {
                        $(".sq-entry").removeClass("hidden")
                    },
                    loadingMessages: function() {
                        $(".sq-loading").removeClass("hidden"), a.infiniteCycle.create($(".sq-loading-messages")), b.hide.streetAddressEntry(), b.hide.streetAddressSelect(), b.hide.landlineEntry(), b.hide.sqCheckButton()
                    },
                    streetAddressEntry: function() {
                        $(".street-address-entry").removeClass("hidden")
                    },
                    streetAddressSelect: function() {
                        $(".street-address-select").removeClass("hidden"), $(".street-address-select select").off("change.refineaddress").on("change.refineaddress", b.bind.changeAddressSelect)
                    },
                    landlineEntry: function() {
                        $(".landline-entry").removeClass("hidden"), $('input[name="landline-entry"]').off("change.landlineentry").on("change.landlineentry", b.bind.clickLandlineEntry)
                    },
                    phoneMatchMessage: function() {
                        $(".invalid-phone-error").removeClass("hidden"), $('input[name="landline-entry-number"]').focus(function() {
                            $(".invalid-phone-error").addClass("hidden")
                        }), $('input[name="landline-entry"], button#sq-check-button').click(function() {
                            $(".invalid-phone-error").addClass("hidden")
                        })
                    },
                    sqCheckButton: function() {
                        $(".sq-check-button").removeClass("hidden")
                    },
                    sqMobileCheckButton: function() {
                        $("#sq-check-button").text("Check if you can Belong").html('<span class="m-check-button">Check if you can Belong</span>'), $('<span class="svg-loader-bblue"></span>').insertBefore(".m-check-button")
                    },
                    message: function(a) {
                        $(".sq-entry-message." + a).removeClass("hidden")
                    },
                    successMessage: function(c, d) {
                        console.log("successMessage(): messageId=" + c), "undefined" != typeof d && d && b.events.streetAddressFocus(), $(".sq-entry-success-message." + c).find("a").each(function() {
                            var c = $(this);
                            "JOIN" === b.get.sourceType() ? ($(".join-link.continue").off("click.joincontinue").on("click.joincontinue", a.join.sq.events.clickContinue), c.hasClass("join-link") || c.addClass("hidden")) : c.hasClass("join-link") && c.addClass("hidden"), c.hasClass("join-now") && c.off("click.joinNow").on("click.joinNow", b.events.joinNow), c.hasClass("find-out-more") && c.off("click.findOutMore").on("click.findOutMore", b.events.findOutMore), $("#main").hasClass("product-page") && $(".join-cta .ctab").text("Join"), c.hasClass("reset-link") && c.off("click.reset").on("click.reset", b.events.reset)
                        }), $(".sq-entry-success-message." + c + " .selected-address").html(a.join.model.helpers.getAddressString()), $(".sq-entry-success-message." + c).removeClass("hidden")
                    },
                    reset: function(c) {
                        c = c || !1, b.display.form(), b.display.streetAddressEntry(), b.display.sqCheckButton(), b.hide.streetAddressSelect(), b.hide.landlineEntry(), b.hide.loadingMessages(), b.hide.allMessages(), a.eoiEntry.hide.form(), a.eoiEntry.hide.allMessages(), $("html, body").velocity({
                            scrollTop: 150
                        }, 500), c && ($("#sq-street-address").val("").blur(), $(".street-address-select .simple-label").text("Please select an address"), a.sqValidator.resetForm(), globalAddressList = void 0, a.LANDLINECHECK = !1, a.REFINEDADDRESS = !1)
                    },
                    productSelection: function() {
                        $(".product-list").removeClass("disabled").find(".join-cta").removeClass("hidden")
                    },
                    adslOnlyOffer: function() {
                        $(".feature-list").find(".adsl-only-offer").removeClass("disabled")
                    },
                    speedboostOffer: function() {
                        $(".feature-list").find(".speedboost-offer").removeClass("disabled")
                    }
                },
                hide: {
                    form: function() {
                        $(".sq-entry").addClass("hidden"), b.hide.loadingMessages()
                    },
                    loadingMessages: function() {
                        $(".sq-loading").addClass("hidden"), a.infiniteCycle.destroy($(".sq-loading-messages"))
                    },
                    streetAddressEntry: function() {
                        $(".street-address-entry").addClass("hidden")
                    },
                    streetAddressSelect: function() {
                        $(".street-address-select").addClass("hidden")
                    },
                    landlineEntry: function() {
                        $(".landline-entry").addClass("hidden")
                    },
                    sqCheckButton: function() {
                        $(".sq-check-button, .sq-check-contact-me").addClass("hidden")
                    },
                    message: function(a) {
                        $(".sq-entry-message." + a).addClass("hidden")
                    },
                    successMessage: function(a) {
                        $(".sq-entry-message." + a).addClass("hidden")
                    },
                    allMessages: function() {
                        $(".sq-entry-message, .sq-entry-success-message").addClass("hidden")
                    },
                    productSelection: function() {
                        $(".product-list").addClass("disabled").find(".join-cta").addClass("hidden")
                    },
                    adslOnlyOffer: function() {
                        $(".feature-list").find(".adsl-only-offer").addClass("disabled")
                    },
                    speedboostOffer: function() {
                        $(".feature-list").find(".speedboost-offer").addClass("disabled")
                    },
                    findOutMoreButton: function(a, b) {
                        var c = $(".sq-container").data("sq-type");
                        if (c == a) {
                            var d = $(".sq-entry-success-message." + b).find("a.find-out-more"),
                                e = $(".sq-entry-success-message." + b).find("a.reset-link");
                            d.length > 0 && d.addClass("hidden"), e.length > 0 && e.css("display", "block")
                        }
                    }
                },
                get: {
                    serviceQualifyRequestObject: function() {
                        var b = $("#header").attr("data-page-sub-type"),
                            c = "";
                        return "nbnrapid" === b && (c = "rapid"), {
                            phoneNumber: a.join.model.sq.phoneNumber,
                            serviceAddressId: a.join.model.sq.serviceAddressId,
                            serviceExchange: a.join.model.sq.serviceExchange,
                            serviceStreetNumber: a.join.model.sq.serviceStreetNumber,
                            serviceStreet: a.join.model.sq.serviceStreet,
                            serviceStreetType: a.join.model.sq.serviceStreetType,
                            serviceSuburb: a.join.model.sq.serviceSuburb,
                            serviceState: a.join.model.sq.serviceState,
                            servicePostcode: a.join.model.sq.servicePostcode,
                            serviceSubAddressDetails: a.join.model.sq.serviceSubAddressDetails,
                            bpsError: a.join.model.sq.bpsError,
                            sqSource: c
                        }
                    },
                    sourceType: function() {
                        switch (a.sqEntry.get.currentPage()) {
                            case "home":
                                return "OAHP";
                            case "adsl":
                                return "ADSL";
                            case "nbn":
                                return "NBN";
                            case "join":
                                return "JOIN";
                            default:
                                return "undefined"
                        }
                    },
                    currentPage: function() {
                        return $(".sq-container").data("sqType")
                    },
                    productType: function(a, b) {
                        if ("BPS" === a) return "nbn";
                        var d = c.getApiResponseMessage(b, "serviceClass");
                        return 1 == d.success ? "nbn" : "adsl"
                    }
                },
                set: {
                    addressDetails: function(b) {
                        var c, d, e = JSON.parse(b.fullAddress),
                            f = e.propertyAddressDetails,
                            g = $("#sq-street-address").val();
                        a.join.model.sq.serviceAddressId = b.addressId, a.join.model.sq.serviceExchange = e.exchange, a.join.model.sq.serviceStreet = f.streetDetails.streetName, a.join.model.sq.serviceSuburb = e.localityDetails.locality, a.join.model.sq.serviceState = e.localityDetails.state, a.join.model.sq.servicePostcode = e.localityDetails.postCode, a.join.model.sq.serviceStreetNumber = "undefined" != typeof b.streetNumber ? b.streetNumber : f.propertyNumberFrom + f.propertyNumberTo + f.propertyNumberFromSuffix, a.join.model.sq.serviceStreetType = "undefined" != typeof f.streetDetails.streetType ? f.streetDetails.streetType : "", "undefined" != typeof b.subaddress ? a.join.model.sq.serviceSubAddressDetails = b.subaddress : "undefined" != typeof b.subAddressDetails ? (c = (a.join.model.sq.serviceStreetNumber + " " + a.join.model.sq.serviceStreet).toUpperCase(), d = g.toUpperCase().indexOf(c), a.join.model.sq.serviceSubAddressDetails = g.substr(0, d)) : a.join.model.sq.serviceSubAddressDetails = "", $("#sq-post-code").val(a.join.model.sq.servicePostcode)
                    },
                    landlineDetails: function() {
                        if ($("#landline-entry-yes").is(":checked")) {
                            var b = $("#landline-entry-number").val(),
                                c = a.validation.helpers.cleanAustralianPhoneNumber(b);
                            $("#landline-entry-number").val(c), b = c, a.join.model.sq.phoneNumber = b
                        } else a.join.model.sq.phoneNumber = ""
                    },
                    orderType: function(b) {
                        a.join.model.sq.orderType = b
                    },
                    broadbandTechType: function(b) {
                        a.join.model.sq.broadbandTechType = b
                    },
                    isPreprovisioned: function(b) {
                        a.join.model.sq.isPreprovisioned = b
                    },
                    promotionId: function(b) {
                        a.join.model.sq.promotionId = b
                    },
                    extendedNetwork: function(b) {
                        a.join.model.sq.extendedNetwork = b || !1
                    }
                },
                is: {
                    nbnOrder: function(a) {
                        return $.inArray(a, ["FTTB", "FTTN", "FTTP"]) > -1
                    },
                    nbnTypeOrder: function(a) {
                        return "undefined" == typeof a || null == a || "" == a ? !1 : "fttp" == a.toLowerCase() || "fttb" == a.toLowerCase() || "fttn" == a.toLowerCase() ? !0 : !1
                    },
                    adslTypeOrder: function(a) {
                        return b.is.adsl1Order(a) || b.is.adsl2Order(a) ? !0 : !1
                    },
                    adsl1Order: function(a) {
                        return "undefined" == typeof a || null == a || "" == a ? !1 : "adsl1" == a.toLowerCase() ? !0 : !1
                    },
                    adsl2Order: function(a) {
                        return "undefined" == typeof a || null == a || "" == a ? !1 : "adsl2" == a.toLowerCase() ? !0 : !1
                    },
                    portOrder: function(a) {
                        return "PORT_ORDER" === a
                    },
                    newOrder: function(a) {
                        return "NEW_ORDER" === a
                    },
                    churnOrder: function(a) {
                        return "CHURN_ORDER" === a
                    }
                },
                bind: {
                    resetSqEntry: function(c) {
                        c.preventDefault(), a.LANDLINECHECK = !1, a.REFINEDADDRESS = !1, b.display.reset(!0), $("#sq-check-contact-me").addClass("hidden"), $("#sq-street-address").focus(), $("#main").hasClass("product-page") && $(".join-cta .ctab").text("Join")
                    },
                    clickLandlineEntry: function() {
                        var a = $("#landline-entry-number").closest(".ctrl-holder");
                        $("#landline-entry-yes").is(":checked") ? (a.removeClass("hidden"), b.set.landlineDetails()) : a.addClass("hidden")
                    },
                    changeAddressSelect: function() {
                        var c = $(this).find("option:selected").val(),
                            d = $(this).find("option:selected").text();
                        switch (c) {
                            case "":
                                return !1;
                            case "address_not_found":
                                console.log("address_not_found selected"), a.adobeAnalytics.global.sq.selectAddress("address_not_found", "standard"), a.livePerson.sq.addressNotFound(), $("#sq-street-address").data("moniker", ""), b.hide.streetAddressSelect(), b.display.streetAddressEntry(), b.hide.allMessages(), b.display.message("having-trouble"), $(".sq-check-contact-me").removeClass("hidden").off("click.sqcontactme").on("click.sqcontactme", a.eoiEntry.bind.displayContactMe);
                                break;
                            default:
                                $.each(globalAddressList, function() {
                                    this.addressId === c && ($("#sq-street-address").val(d), a.sqEntry.set.addressDetails(this), a.sqEntry.set.landlineDetails(), a.REFINEDADDRESS = !0)
                                })
                        }
                    },
                    validation: function() {
                        a.sqValidator = a.validation.setupFormValidation("sq-entry", $("#sq"), {
                            rules: {
                                "sq-street-address": {
                                    required: !0,
                                    sqHasPostCode: !0,
                                    disallowedCharacters: !0,
                                    minlength: 8
                                },
                                "landline-entry-number": {
                                    required: function() {
                                        var a = $("#landline-entry-yes").is(":checked");
                                        return a
                                    },
                                    australianPhoneNumber: !0
                                },
                                "landline-entry": {
                                    required: 0 == $(".landline-entry .radio-holder").hasClass("hidden")
                                }
                            },
                            errorPlacement: function(a, b) {
                                "landline-entry" == b.attr("name") ? $(".landline-entry .radio-holder").append(a) : a.insertAfter(b)
                            }
                        }), a.sqValidator.resetForm()
                    }
                },
                events: {
                    joinNow: function(b) {
                        b.preventDefault();
                        var c = "/join";
                        a.adobeAnalytics.global.sq.findOutMoreOrJoinNow(a.sqEntry.get.sourceType(), "Join-Now", a.join.model.sq.orderType, "standard"), a.analytics.methods.sq.findOutMoreOrJoinNow(c, a.sqEntry.get.sourceType(), "Join-Now", a.join.model.sq.orderType)
                    },
                    findOutMore: function(b) {
                        if (b.preventDefault(), $(this).closest(".sq-message").hasClass("nbn") || $(this).closest(".sq-message").hasClass("nbn-fttb") || $(this).closest(".sq-message").hasClass("nbn-fttn")) {
                            var c = "/nbn";
                            a.adobeAnalytics.global.sq.findOutMoreOrJoinNow(a.sqEntry.get.sourceType(), "Find-Out-More", a.join.model.sq.orderType, "standard"), a.analytics.methods.sq.findOutMoreOrJoinNow(c, a.sqEntry.get.sourceType(), "Find-Out-More", a.join.model.sq.orderType)
                        } else {
                            var c = "/adsl";
                            a.adobeAnalytics.global.sq.findOutMoreOrJoinNow(a.sqEntry.get.sourceType(), "Find-Out-More", a.join.model.sq.orderType, "standard"), a.analytics.methods.sq.findOutMoreOrJoinNow(c, a.sqEntry.get.sourceType(), "Find-Out-More", a.join.model.sq.orderType)
                        }
                    },
                    reset: function(a) {
                        a.preventDefault(), b.display.reset(!0)
                    },
                    selectHaveLandline: function() {
                        var a = $(".landline-entry .radio-holder").offset().top - 150;
                        $("html, body").animate({
                            scrollTop: a
                        }, 500)
                    },
                    streetAddressFocus: function() {
                        console.log("street address focus");
                        var a = {
                                mobile: -150,
                                tablet: -100,
                                laptop: -50,
                                desktop: -50
                            },
                            b = a.desktop;
                        window.DD.bp.is(0, "s") ? b = a.mobile : window.DD.bp.is("m", "m") ? b = a.tablet : window.matchMedia && window.matchMedia("(min-width: 1024px) and (max-width: 1366px)").matches && (b = a.laptop, $("#sq").velocity("scroll", {
                            duration: 250,
                            offset: b
                        }))
                    }
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.sqExecute = function() {
            var b, c, d, e, f, g, h, i, j, k, l, m, n, o = {},
                p = 15e3,
                q = 0,
                r = "qasAddressPicklist",
                s = "qasAddressWindow",
                t = 200;
            return {
                init: function() {
                    return b = this, "undefined" != typeof sqConfig ? (n = sqConfig, "undefined" == typeof n ? !1 : void $(function() {
                        b.initializeVars(), b.initialiseSQOnDOMReady()
                    })) : void 0
                },
                initializeVars: function() {
                    c = $("#" + n.addressResultDivId), d = $("#" + n.addressMsgDivId), e = $("#" + n.addressErrorMsgDivId), n.loadingIndicatorElement.length > 0 && (f = $("#" + n.loadingIndicatorElement)), g = $("#" + n.addressFieldId), h = $("#" + n.postcodeFieldId), i = 0, j = !1, m = !0, l = "", k = !0, n.clientTimeoutMs.length > 0 && (p = n.clientTimeoutMs)
                },
                get_api_key: function() {
                    a.DEVSWITCH || $.ajax({
                        url: "/apps/generateKey",
                        data: {},
                        dataType: "html",
                        async: !1,
                        type: "POST",
                        error: function() {},
                        timeout: p,
                        success: function(a) {
                            l = a, m = !1
                        }
                    })
                },
                forceAddressList: !1,
                performTelstraVerification: function(d, e) {
                    var g = !1;
                    if (!j && !m) {
                        j = !0, null != f && f.show();
                        var k = {
                            address: d,
                            postcode: e,
                            excludeAddressType: n.excludeAddressType,
                            addressStatus: n.addressStatus,
                            requestor: n.requestor,
                            key: l
                        };
                        console.log("performTelstraVerification(): jsonp submit data=" + JSON.stringify(k)), $.jsonp({
                            url: n.verificationUrl,
                            data: k,
                            callback: "callback",
                            error: function(a, d) {
                                null != f && f.hide(), c.hide(), b.verificationHandler.showErrorMessage(n.errorMsg2), void 0 !== h.attr("disabled") && 1 == h.attr("disabled") && h.removeAttr("disabled"), q += 1, j = !1, "timeout" === d && (q += 1, j = !1, 6 > q ? b.search_address() : q = 0)
                            },
                            timeout: p,
                            success: function(c) {
                                var d = "";
                                if (console.log("performTelstraVerification(success): addressList=" + JSON.stringify(c)), i += 1, globalAddressList = c, null != c && c.length > 0) {
                                    if (console.log("performTelstraVerification(): Results returned=" + c.length), 1 == c.length) {
                                        console.log("performTelstraVerification(): 1 result");
                                        var e = c[0].confidence;
                                        null != e && e >= n.confRatingThreshold ? (console.log("performTelstraVerification(): confidence=" + e + ". Over threshold of " + n.confRatingThreshold), g = !0, d = c[0].addressId, a.analytics.methods.sq.selectAddress(d), $("#sq-form").length > 0 ? a.adobeAnalytics.global.sq.selectAddress(d, "modal") : a.adobeAnalytics.global.sq.selectAddress(d, "standard"), b.verificationHandler.onExactMatch(c[0])) : b.forceAddressList = !0
                                    } else if (2 == c.length) {
                                        console.log("performTelstraVerification(): 2 results, contains 100% confidence");
                                        for (var h in c) {
                                            var k = c[h],
                                                l = (k.fullAddress, b.getUnitTypeFromTelstraVerifyAddress(k).toUpperCase());
                                            if (console.log("address[" + h + "]: " + JSON.stringify(k)), console.log("getUnitTypeFromTelstraVerifyAddress[" + h + "]=" + l), 100 == k.confidence || "UNIT" == l) {
                                                d = c[0].addressId, a.analytics.methods.sq.selectAddress(d), $("#sq-form").length > 0 ? a.adobeAnalytics.global.sq.selectAddress(d, "modal") : a.adobeAnalytics.global.sq.selectAddress(d, "standard"), g = !0, b.verificationHandler.onExactMatch(k), globalAddressList = [], globalAddressList.push(k);
                                                break
                                            }
                                        }
                                    }
                                    g || b.verificationHandler.onNonExactMatch(c)
                                } else b.verificationHandler.onNoMatch();
                                null != f && f.hide(), j = !1, BELONG.sqAddressLib.methods.postClick()
                            }
                        })
                    }
                },
                getUnitTypeFromTelstraVerifyAddress: function(a) {
                    if ("undefined" == typeof a || null == a) return "";
                    if ("undefined" == typeof a.fullAddress || null == a.fullAddress) return "";
                    var b = "",
                        c = JSON.parse(a.fullAddress);
                    return "undefined" != typeof c.subAddressDetails && "undefined" != typeof c.subAddressDetails.flatUnitTypeEnum && (b = c.subAddressDetails.flatUnitTypeEnum), b
                },
                search_address: function() {
                    var a = g.val(),
                        c = h.val();
                    console.log("search_address(): user selected address= " + a), console.log("search_address(): postcode=" + c);
                    var d = g.data("moniker");
                    b.secondExperianAPI(d, function(d, e) {
                        1 == d && "undefined" != typeof e.address && "" != e.address.pid && (a = e.address.address + ", " + e.address.locality + ", " + e.address.state + ", " + e.address.postcode, c = e.address.postcode, console.log("search_address(): changing verify address to=" + a)), b.performTelstraVerification(a, c)
                    })
                },
                secondExperianAPI: function(a, b) {
                    var c = "";
                    return "undefined" != typeof a && (c = a), "" == c ? void b(!1, null) : void $.ajax({
                        url: "/apps/telstraEveServlet",
                        type: "POST",
                        dataType: "json",
                        data: {
                            requestType: s,
                            action: "DoGetAddress",
                            moniker: c
                        },
                        error: function() {
                            return "undefined" != typeof b ? void b(!1, null) : void 0
                        },
                        success: function(a) {
                            return void 0 != a ? console.log("secondExperianAPI(SUCCESS): json=", a) : console.log("secondExperianAPI(): No data returned"), "undefined" != typeof b ? void b(!0, a) : void 0
                        }
                    })
                },
                verificationHandler: {
                    showMessage: function(a) {
                        $("#" + n.addressMsgDivId + " " + n.addressMsgDivChildElement + " ").html(a), d.fadeIn()
                    },
                    showErrorMessage: function(a) {
                        $("#" + n.addressErrorMsgDivId).html(a), e.fadeIn()
                    },
                    hideMessage: function() {
                        d.fadeOut()
                    },
                    onExactMatch: function(a) {
                        e.hide();
                        var b = (a.subaddress ? a.subaddress + " " : "") + (a.streetNumber ? a.streetNumber + " " : "") + a.streetName + ", " + a.suburb,
                            d = "";
                        d += "<option selected='selected' value='" + a.addressId + "'>" + b + "</option>";
                        var f = f + "<div id='streetName_" + a.addressId + "'>" + a.streetName + "</div>",
                            g = "" + n.returnFullAddress;
                        "true" == g && $("#" + n.fullAddressHiddenInputId).val(a.fullAddress), c.hide(), k = !1, $("#" + n.addressConfirmFieldId).html(d), $("#streetNames").html(f), "function" == typeof n.callBackHookScriptName && n.callBackHookScriptName("exact_match", a)
                    },
                    onNonExactMatch: function(a) {
                        e.hide();
                        var d = "";
                        d += "<option selected='selected' value=''>" + n.selectAddressOption + "</option>";
                        var f = "";
                        $.each(a, function(a, b) {
                            var c = (b.subaddress ? b.subaddress + " " : "") + (b.streetNumber ? b.streetNumber + " " : "") + b.streetName + ", " + b.suburb;
                            d += "<option value='" + b.addressId + "'>" + c + "</option>", f = f + "<div id='streetName_" + b.addressId + "'>" + b.streetName + "</div>", o[b.addressId] = b.fullAddress
                        }), d += "<option value='address_not_found'>" + n.addressNotListedOption + "</option>", h.attr("disabled", "disabled"), k = !1, b.verificationHandler.hideMessage(), c.fadeIn(), $("#" + n.addressConfirmFieldId).html(d), $("#streetNames").html(f), "function" == typeof n.callBackHookScriptName && n.callBackHookScriptName("multiple_matches", a)
                    },
                    onNoMatch: function() {
                        c.hide(), b.verificationHandler.showErrorMessage(3 > i ? n.errorMsg1 : n.errorMsg2), void 0 !== h.attr("disabled") && 1 == h.attr("disabled") && h.removeAttr("disabled"), $("#" + n.addressConfirmFieldId).html(""), $("#streetNames").html(""), "function" == typeof n.callBackHookScriptName && n.callBackHookScriptName("no_match")
                    }
                },
                initialiseSQOnDOMReady: function() {
                    var d, e = $("#" + n.addressFieldId),
                        f = $("#" + n.postcodeFieldId),
                        g = "";
                    if (d = "" == n.disableAutoComplete || null == n.disableAutoComplete ? !1 : n.disableAutoComplete, 0 == d) {
                        if (a.DEVSWITCH) return;
                        e.autocomplete({
                            source: function(a, b) {
                                var c = $.trim(a.term);
                                c.length > 0 ? $.ajax({
                                    url: "/apps/telstraEveServlet",
                                    type: "POST",
                                    dataType: "json",
                                    data: {
                                        requestType: r,
                                        searchTerm: c
                                    },
                                    error: function() {},
                                    success: function(a) {
                                        b(void 0 != a ? $.map(a.addressList, function(a) {
                                            return {
                                                label: a.partialAddress,
                                                value: a.moniker,
                                                id: a.layout,
                                                useEntered: a.useEntered
                                            }
                                        }) : {
                                            label: "",
                                            value: ""
                                        })
                                    }
                                }) : b({
                                    label: "",
                                    value: ""
                                })
                            },
                            delay: t,
                            select: function(a, b) {
                                return $(this).data("moniker", b.item.value), $(this).val(b.item.label.trim().replace(/(<([^>]+)>)/gi, "").replace(/\s{2,}/g, " ")), !1
                            },
                            search: function() {
                                var a = ".*\\d+\\D?(,|/|-|\\s)*";
                                if (null != this.value.match(a) && this.value.match(a)[0] == this.value) return e.autocomplete("close"), !1;
                                var b = this.value.replace(new RegExp(a, ""), "");
                                return b.length < parseInt(n.minLength, 10) ? !1 : void 0
                            },
                            focus: function(a) {
                                a.preventDefault()
                            }
                        }).data("ui-autocomplete")._renderItem = function(a, b) {
                            return b.label = b.label.replace(/(<([^>]+)>)/gi, "").replace(/\s{2,}/g, " ").replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>"), $("<li></li>").data("item.autocomplete", b).append($("<a></a>").html(b.label).html(b.label)).appendTo(a)
                        }
                    }
                    var h = function(a) {
                        var d = window.event || a,
                            f = d.keyCode;
                        return 13 == f ? void e.autocomplete("close") : void(f >= 16 && 18 >= f || 20 == f || 27 == f || f >= 33 && 40 >= f || 45 == f || 91 == f || 92 == f || f >= 112 && 123 >= f || 144 == f || (c.fadeOut(), b.verificationHandler.hideMessage(), $("#" + n.addressConfirmFieldId).html(""), k = !0))
                    };
                    e.keyup(function(a) {
                        var b = $(this).val();
                        "" != g && b != g && (f.removeAttr("disabled"), g = ""), h(a)
                    }), f.keyup(function(a) {
                        h(a)
                    }), $("#" + n.addressConfirmFieldId).change(function() {
                        if ("address_not_found" == $(this).val()) e.removeAttr("disabled"), f.removeAttr("disabled"), k = !0;
                        else {
                            f.attr("disabled", "disabled"), k = !1;
                            var a = n.returnFullAddress;
                            "true" == a && $("#" + n.fullAddressHiddenInputId).val(o[$("#" + n.addressConfirmFieldId).val()])
                        }
                    });
                    var i = function(c) {
                        var d = $("#" + n.addressConfirmFieldId).val();
                        if (null != d && "address_not_found" != d && (a.analytics.methods.sq.selectAddress(d), $("#sq-form").length > 0 ? a.adobeAnalytics.global.sq.selectAddress(d, "modal") : a.adobeAnalytics.global.sq.selectAddress(d, "standard")), c.preventDefault(), BELONG.sqAddressLib.methods.preClick(), k) {
                            var e = !0;
                            if ("function" == typeof validateAddress && (validateAddress() || (e = !1)), e) {
                                var f = $("#" + n.addressFieldId).val(),
                                    g = null == n.stripInvalidAddressChars || "" == n.stripInvalidAddressChars || 1 == g ? !0 : !1;

                                if (1 == g) {
                                    for (var h; h = f.match(/(<|>|"|#|&)+/);) f = f.replace(h[1], " ");
                                    $("#" + n.addressFieldId).val(f)
                                } else {
                                    var h = f.match(/(<|>|"|#|&)+/);
                                    h && (b.verificationHandler.showErrorMessage(null == n.errorMsg3 || "" == n.errorMsg3 ? "Please remove '" + h[1] + "' character from street address." : n.errorMsg3), e = !1)
                                }
                            }
                            e && (b.get_api_key(), b.search_address())
                        }
                    };
                    $("#" + n.searchButtonId).on("click.searchbuttonclick", i)
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.aboutUs = {
            init: function() {
                var b = "about-us";
                return 0 === $("#" + b).length ? !1 : void ko.applyBindings(new a.belongTabPanel(b), document.getElementById(b))
            }
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.agentTermsModal = a.agentTermsModal || {}, $.extend(a.agentTermsModal, function() {
            var b;
            return {
                init: function() {
                    if (b = this, 0 === $("#mdl-agent-terms").length) return !1;
                    var c = new b.viewModel;
                    ko.applyBindings(c, document.getElementById("mdl-agent-terms")), a.agentTermsVM = ko.contextFor(document.getElementById("mdl-agent-terms")).$data
                },
                viewModel: function() {
                    var b = this;
                    b.productClasses = ko.observable(), b.setupTermsToDisplay = function(a) {
                        var c = "CHURN_ORDER" === a.sq.orderType && "DSL" === a.product.planType,
                            d = "CHURN_ORDER" === a.sq.orderType && "BUNDLE" === a.product.planType,
                            e = "PORT_ORDER" === a.sq.orderType;
                        b.productClasses({
                            "show-product-nbn": "NBN" === a.product.planType,
                            "show-product-adsl-bundle": "BUNDLE" === a.product.planType,
                            "show-product-adsl-dsl": "DSL" === a.product.planType,
                            "show-contract": "SIX_MONTHS" === a.product.contractTerm || "TWELVE_MONTHS" === a.product.contractTerm,
                            "show-m2m": "SIX_MONTHS" !== a.product.contractTerm && "TWELVE_MONTHS" !== a.product.contractTerm,
                            "show-speedboost-25-5": "25/5" === a.product.planSpeed,
                            "show-speedboost-100-40": "100/40" === a.product.planSpeed,
                            "show-voice-national": "NATIONAL" === a.product.voicePlan,
                            "show-voice-international": "INTERNATIONAL" === a.product.voicePlan,
                            "show-voice-both": "BOTH" === a.product.voicePlan,
                            "show-core-network": !a.sq.extendedNetwork,
                            "show-extended-exchange": a.sq.extendedNetwork,
                            "show-data-100": "Regular" === a.product.planSize,
                            "show-data-500": "Large" === a.product.planSize,
                            "show-data-1000": "ExtraLarge" === a.product.planSize,
                            "show-transfer-dsl": c,
                            "show-transfer-phone": e,
                            "show-transfer-churn-bundle": d,
                            "show-next-steps-adsl-transfer": c || e || d,
                            "show-next-steps-adsl-non-transfer": "NBN" !== a.product.planType && !c && !e && !d,
                            "show-next-steps-nbn": "NBN" === a.product.planType
                        }), console.log("product classes", b.productClasses())
                    }, b.acceptTerms = function(b, c) {
                        a.join.personal.events.onContinue(c), $.magnificPopup.proto.close.call(this)
                    }, b.cancelTerms = function() {
                        $.magnificPopup.proto.close.call(this)
                    }
                },
                open: function(b) {
                    b = b || {}, b.closeCallBack = b.closeCallBack || function() {}, b.openCallBack = b.openCallBack || function() {}, b.joinModel = b.joinModel || a.agentTermsVM.testModel4, $.magnificPopup.open({
                        items: {
                            src: "#mdl-agent-terms",
                            type: "inline"
                        },
                        alignTop: !0,
                        showCloseBtn: !1,
                        closeOnBgClick: !1,
                        enableEscapeKey: !1,
                        overflowY: "auto",
                        callbacks: {
                            open: function() {
                                a.belongModal.setupAutoResize({
                                    modalContainer: $("#mdl-agent-terms"),
                                    nameSpace: "modalAutoResizeAgentTerms"
                                }), a.agentTermsVM.setupTermsToDisplay(b.joinModel), b.openCallBack()
                            },
                            close: function() {
                                b.closeCallBack()
                            }
                        }
                    })
                }
            }
        }())
    }(BELONG),
    function(a) {
        "use strict";
        a.callbackForm = {
            init: function() {
                return 0 === $("#callback-form").length ? !1 : void ko.applyBindings(new a.callbackForm.callbackFormViewModel, document.getElementById("callback-form-container"))
            }
        }, a.callbackForm.callbackFormViewModel = function() {
            var b = this;
            b.prospectName = ko.observable(""), b.emailAddress = ko.observable(""), b.phoneNumber = ko.observable(""), b.callbackTime = ko.observable("AM"), b.showForm = ko.observable(!0), b.showSubmitSuccess = ko.observable(!1), b.showSubmitFail = ko.observable(!1), b.callbackValidator = null, b.setupFormValidation = function() {
                    var c = {
                            rules: {
                                "callback-form-propsect-name": {
                                    required: !0,
                                    disallowedCharacters: !0,
                                    maxlength: 255
                                },
                                "callback-form-prospect-email-address": {
                                    required: !0,
                                    backendEmail: !0,
                                    disallowedCharacters: !0,
                                    maxlength: 255
                                },
                                "callback-form-prospect-phone-number": {
                                    required: !0,
                                    australianMobileOrLandlineNumber: !0
                                }
                            }
                        },
                        d = "callback-form";
                    b.callbackValidator = a.validation.setupFormValidation(d, $("." + d), c)
                }, b.submit = function() {
                    var c = a.validation.validateForm(b.callbackValidator);
                    if (0 != c) {
                        var d = {
                                customerName: b.prospectName(),
                                customerEmail: b.emailAddress(),
                                customerPhoneNumber: b.phoneNumber(),
                                timePreference: b.callbackTime()
                            },
                            e = "/bin/belong/customer/callback";
                        1 == BELONG.DEVSWITCH && (e = "/bin/belong/customer/callback?success"), console.log("callbackForm.submit(): data=", d), $.post(e, d).done(function() {
                            a.adobeAnalytics.pages.callbackForm.submit(b.callbackTime()), b.showForm(!1), b.showSubmitSuccess(!0)
                        }).fail(function() {
                            b.showForm(!1), b.showSubmitFail(!0)
                        }).always(function() {
                            b.scrollToCallbackContainer()
                        })
                    }
                }, b.scrollToCallbackContainer = function() {
                    var a = $("#callback-form-container").offset().top - 200;
                    $("html, body").animate({
                        scrollTop: a
                    }, 500)
                },
                function() {
                    b.setupFormValidation()
                }()
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.compareModule = {
            init: function() {
                var b = "compare-module";
                return 0 === $("#" + b).length ? !1 : void ko.applyBindings(new a.belongTabPanel(b, !0), document.getElementById(b))
            }
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.feedbackForm = {
            init: function() {
                return 0 === $("#feedback-form-container").length ? !1 : (ko.applyBindings(new a.feedbackForm.feedbackViewModel, document.getElementById("feedback-form-container")), a.forms.applyShiftLabel(), void a.forms.applyAutoGrowTextarea())
            }
        }, a.feedbackForm.feedbackViewModel = function() {
            var b = this,
                c = $("#feedback-response-1-label").text(),
                d = $("#feedback-response-3-label").text();
            console.log(c, d), b.feedbackResponse = ko.observable(""), b.feedbackRatingYes = ko.observable(""), b.feedbackRatingNo = ko.observable(""), b.commentYes = ko.observable(""), b.commentNo = ko.observable(""), b.showForm = ko.observable(!0), b.showDetails = ko.observable(""), b.showSubmitInvalid = ko.observable(!1), b.showSubmitSuccess = ko.observable(!1), b.showSubmitFail = ko.observable(!1), b.feedbackResponse.subscribe(function() {
                    b.showSubmitInvalid(!1)
                }), b.feedbackValidator = null, b.getQueryVariable = function(a) {
                    for (var b = window.location.search.substring(1), c = b.split("&"), d = 0; d < c.length; d++) {
                        var e = c[d].split("=");
                        if (e[0] == a) return e[1]
                    }
                    return !1
                }, b.setupFormValidation = function() {
                    var c = {
                            rules: {
                                "feedback-scale1-option": {
                                    required: !0
                                },
                                "feedback-scale2-option": {
                                    required: !0
                                },
                                "feedback-comment-yes": {
                                    required: !0,
                                    disallowedCharacters: !0,
                                    maxlength: 300
                                },
                                "feedback-comment-no": {
                                    required: !0,
                                    disallowedCharacters: !0,
                                    maxlength: 300
                                }
                            },
                            messages: {
                                "feedback-comment-yes": {
                                    required: "This field is required",
                                    maxlength: "Please enter less than 300 characters",
                                    disallowedCharacters: "This field canâ€™t include some of those characters."
                                },
                                "feedback-comment-no": {
                                    required: "This field is required",
                                    maxlength: "Please enter less than 300 characters",
                                    disallowedCharacters: "This field canâ€™t include some of those characters."
                                }
                            }
                        },
                        d = "feedback-form";
                    b.feedbackValidator = a.validation.setupFormValidation(d, $("." + d), c)
                }, b.btnState = ko.observable("active"), b.submit = function() {
                    console.log("attempting SUBMIT");
                    var e = a.validation.validateForm(b.feedbackValidator);
                    if (0 == e) return void b.showSubmitInvalid(!0);
                    var f, g, h, i, j;
                    "1" === b.feedbackResponse() ? (g = c, h = b.feedbackRatingYes(), f = b.commentYes()) : "3" === b.feedbackResponse() && (g = d, h = b.feedbackRatingNo(), f = b.commentNo()), i = b.getQueryVariable("trigger"), j = b.getQueryVariable("source"), i || (i = ""), j || (j = window.location.href);
                    var k = {
                            customerFeedback: g,
                            customerFeedbackRating: h,
                            customerFeedbackComment: f,
                            feedbackTrigger: i,
                            feedbackSource: j
                        },
                        l = "/bin/belong/auth/customer/survey/feedback";
                    console.log("feedbackForm.submit(): data=", k), b.btnState("loading");
                    var m = $.post(l, k);
                    m.always(function() {
                        b.showForm(!1), b.btnState("active")
                    }), m.done(function() {
                        b.showSubmitSuccess(!0), setTimeout(function() {
                            $(".info-title.success").velocity("scroll", {
                                offset: -120
                            })
                        }, 10)
                    }), m.fail(function() {
                        b.showSubmitFail(!0)
                    })
                },
                function() {
                    b.setupFormValidation()
                }()
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.genericInformationModal = {
            init: function() {
                this.findAndBindPageLinks(), this.openModalForUrlHash()
            },
            findAndBindPageLinks: function() {
                var a = $(".generic-information-modal");
                a.each(function() {
                    var a = $(this),
                        b = a.attr("id"),
                        c = $('[href="#' + b + '"]');
                    c.off("click.genericInformationModal").on("click.genericInformationModal", function() {
                        return BELONG.genericInformationModal.openModal(b), !1
                    })
                })
            },
            openModalForUrlHash: function() {
                var a = window.location.hash;
                if (a) {
                    a = a.replace("#", "");
                    var b = $("#" + a + ".generic-information-modal ");
                    b.length > 0 && BELONG.genericInformationModal.openModal(a)
                }
            },
            openModal: function(a) {
                $.magnificPopup.open({
                    items: {
                        src: "#" + a,
                        type: "inline"
                    },
                    alignTop: !0,
                    showCloseBtn: !0,
                    closeOnBgClick: !0,
                    overflowY: "auto",
                    callbacks: {
                        open: function() {
                            BELONG.belongModal.setupAutoResize({
                                modalContainer: $("#" + a),
                                nameSpace: "modalAutoResize" + a
                            })
                        }
                    }
                }), $("#" + a + " .belong-modal__continue-link").off("click.close" + a).on("click.close" + a, function(a) {
                    a.preventDefault(), $.magnificPopup.proto.close.call(this)
                })
            }
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.join = a.join || {}, a.join.cisModal = function() {
            var b;
            return {
                init: function() {
                    b = this, this.bindCisModalOpenTrigger(), b.bindCustomerTermsModalOpenTrigger()
                },
                bindCisModalOpenTrigger: function() {
                    var a = $(".join-cis-modal"),
                        c = a.attr("id"),
                        d = $('[href="#' + c + '"]');
                    d.off("click.cisModal").on("click.cisModal", function() {
                        return b.openModal(c, b.configureCisModalContent()), !1
                    })
                },
                bindCustomerTermsModalOpenTrigger: function() {
                    var a = $(".join-customer-terms-modal"),
                        c = a.attr("id"),
                        d = $('[href="#' + c + '"]');
                    d.off("click.customerTermsModal").on("click.customerTermsModal", function() {
                        return b.openModal(c), !1
                    })
                },
                openModal: function(a, b) {
                    "function" == typeof b && b(), $.magnificPopup.open({
                        items: {
                            src: "#" + a,
                            type: "inline"
                        },
                        alignTop: !0,
                        showCloseBtn: !0,
                        closeOnBgClick: !0,
                        overflowY: "auto",
                        callbacks: {
                            open: function() {
                                BELONG.belongModal.setupAutoResize({
                                    modalContainer: $("#" + a),
                                    nameSpace: "modalAutoResize" + a
                                })
                            }
                        }
                    }), $("#" + a + " .belong-modal__continue-link").off("click.close" + a).on("click.close" + a, function(a) {
                        a.preventDefault(), $.magnificPopup.close();
                        var b = $(".join-form-container");
                        b.animate({
                            scrollTop: $(".join-cis").offset().top - b.offet().top + b.scrollTop()
                        })
                    })
                },
                configureCisModalContent: function() {
                    var b = a.join.model.product;
                    console.log("configureCisModalContent(): EVE.join.model.product=", a.join.model.product), console.log("configureCisModalContent(): EVE.join.product.methods.getCurrentFormModel()=", a.join.product.methods.getCurrentFormModel()), console.log("configureCisModalContent(): model=", b);
                    var c = a.join.model.sq.extendedNetwork;
                    $("#join-cis-modal-adsl-broadband-standard").addClass("hidden"), $("#join-cis-modal-adsl-broadband-extended").addClass("hidden"), $("#join-cis-modal-adsl-bundle-standard").addClass("hidden"), $("#join-cis-modal-adsl-bundle-extended").addClass("hidden"), $("#join-cis-modal-nbn").addClass("hidden"), $("#join-cis-modal-voice").addClass("hidden"), "DSL" == b.planType ? 0 == c ? $("#join-cis-modal-adsl-broadband-standard").removeClass("hidden") : $("#join-cis-modal-adsl-broadband-extended").removeClass("hidden") : "BUNDLE" == b.planType ? 0 == c ? $("#join-cis-modal-adsl-bundle-standard").removeClass("hidden") : $("#join-cis-modal-adsl-bundle-extended").removeClass("hidden") : "NBN" == b.planType && $("#join-cis-modal-nbn").removeClass("hidden"), ("NATIONAL" == b.voicePlan || "INTERNATIONAL" == b.voicePlan || "BOTH" == b.voicePlan) && ($("#join-cis-modal-voice").removeClass("hidden"), console.log("join.cisModal.configureModalContent(): Show Voice"))
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.join = a.join || {}, a.join.model = {
            sq: {
                accessType: "",
                defaultSize: "",
                phoneNumber: "",
                serviceAddressId: "",
                serviceExchange: "",
                serviceStreetNumber: "",
                serviceStreet: "",
                serviceStreetType: "",
                serviceSuburb: "",
                serviceState: "",
                servicePostcode: "",
                serviceSubAddressDetails: "",
                bpsError: !1,
                isNbn: !1,
                orderType: "",
                isPreprovisioned: !1,
                promotionId: "",
                broadbandTechType: "",
                extendedNetwork: !1
            },
            product: {
                planType: "",
                planSize: "",
                planSpeed: "",
                contractTerm: null,
                voicePlan: "",
                promotionId: "",
                promotionStaffId: "",
                staffPromoSavedInSession: !1
            },
            personalDetails: {
                firstName: "",
                lastName: "",
                email: "",
                password: "",
                dateOfBirth: "",
                mobilePhone: "",
                portCurrentAccountNumber: "",
                portCurrentProvider: "",
                portNameOnBill: ""
            },
            CIS: {
                agreementChecked: !1,
                portAgreementChecked: !1
            },
            payconfirm: {
                clientToken: null,
                paymentType: "CreditCard",
                paymentDetails: {},
                nonce: "",
                agreementChecked: !1,
                portAgreementChecked: !1
            },
            helpers: {
                getAddressString: function() {
                    var b = a.join.model.sq;
                    return b.serviceSubAddressDetails + " " + b.serviceStreetNumber + " " + b.serviceStreet + " " + b.serviceStreetType + " " + b.serviceSuburb + ", " + b.serviceState + ", " + b.servicePostcode
                }
            }
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.join = a.join || {}, a.join.payconfirm = function() {
            var b, c, d, e, f, g, h, i, j, k, l, m, n = "join-payconfirm",
                o = "join-cis-form",
                p = "payment-form-creditcard",
                q = "join-paypal-container";
            return {
                init: function() {
                    return b = this, c = $("#payconfirm-section-header"), d = $("#payconfirm-section-footer"), h = $("#join-payconfirm-submit"), e = $("#" + n), g = $(".cis-section"), f = $(".port-agreement-section"), i = $("#join-cis-acceptChk"), j = $("#join-cis-portChk"), k = $("#GlobalData"), i.off("click.cis").on("click.cis", b.events.checkAgreement), j.off("click.cis").on("click.cis", b.events.checkPortingAgreement), $("#join-payconfirm-zpl-continue").off("click.cis").on("click.cis", b.events.onLogin), l = a.validation.setupFormValidation(o, $("#" + o), {
                        rules: {
                            "credit-card-name": {
                                required: !0,
                                disallowedCharacters: !0,
                                maxlength: 255
                            },
                            "join-cis-portChk": {
                                required: !0
                            },
                            "join-cis-acceptChk": {
                                required: !0
                            }
                        },
                        errorPlacement: function(a, b) {
                            a.appendTo(b.parent())
                        }
                    }), 0 === $("#join-payconfirm").length ? !1 : (m = new b.paymentViewModel, ko.applyBindings(m, document.getElementById("join-payconfirm")), void m.init())
                },
                paymentViewModel: function() {
                    var c = {
                            CREDITCARD: "CREDITCARD",
                            PAYPAL: "PAYPAL"
                        },
                        e = this;
                    e.createOrderError = !1, e.clientToken = ko.observable(), e.selectedPayment = ko.observable(), e.paymentError = ko.observableArray(), e.invalidCardType = ko.observable(), e.invalidCardHolderName = ko.observable(), e.creditCardErrorList = ko.observableArray(), e.cardHolderName = ko.observable(), e.selectedPayment.subscribe(function(a) {
                        e.changePayment(a)
                    }), e.cardHolderName.subscribe(function() {
                        e.cardHolderNameValidation()
                    }), e.updateClientToken = function(a) {
                        e.clientToken(a), e.init()
                    }, e.init = function() {
                        e.clientToken() && e.selectedPayment(c.CREDITCARD)
                    }, e.catchange = function(a, b) {
                        var c = $(".credit-card-icon");
                        a ? c.each(function() {
                            $(this).hasClass("svg-" + b + "-card") ? $(this).addClass("active") : $(this).removeClass("active")
                        }) : c.removeClass("active")
                    }, e.addCreditCardError = function(a) {
                        var b = ko.utils.arrayFirst(e.creditCardErrorList(), function(b) {
                            return a.id === b.id
                        });
                        b ? e.creditCardErrorList.replace(b, a) : e.creditCardErrorList.push(a)
                    }, e.removeCreditCardErrorById = function(a) {
                        e.creditCardErrorList.remove(function(b) {
                            return b.id === a ? !0 : void 0
                        })
                    }, e.removeCreditCardErrorAll = function(a) {
                        return a.removeAll(), a
                    }, e.cardHolderNameValidation = function() {
                        var b = "";
                        if (void 0 === e.cardHolderName() || "" === e.cardHolderName() ? b = a.responseMessage.getValidationMessage("join-payment-form", "credit-card-name", "required") : /^[^<>+;\[\]\{\}\"]+$/i.test(e.cardHolderName()) || (b = a.responseMessage.getValidationMessage("join-payment-form", "credit-card-name", "disallowedCharacters")), console.log("cardHolderNameValidation", b), "" !== b) {
                            e.invalidCardHolderName(!0);
                            var c = {
                                message: b,
                                id: "credit-card-name"
                            };
                            e.addCreditCardError(c)
                        } else e.invalidCardHolderName(!1), e.removeCreditCardErrorById("credit-card-name")
                    }, e.onClickChangePayment = function() {
                        var b = !!$('input[name="payment_method_nonce"]').val();
                        if (b) {
                            var c = a.responseMessage.getValidationMessage("join-payment-form", "nonce", "alreadyhas");
                            return console.log("alreadyHasNonce:", c), $("#payment-paypal-errors").html(c), !1
                        }
                        return !0
                    }, e.changePayment = function(b) {
                        b === c.PAYPAL ? (e.braintreePayPal(), a.join.model.payconfirm.nonce = "") : e.braintreeCreditCard()
                    }, e.braintreePayPal = function() {
                        braintree.setup(e.clientToken(), "paypal", {
                            container: q,
                            displayName: "Belong",
                            onPaymentMethodReceived: function(b) {
                                $(".join-paypal-message").addClass("hidden"), $("#payment-paypal-errors").html(""), a.join.model.payconfirm.nonce = b.nonce, a.join.model.payconfirm.paymentType = b.type, a.join.model.payconfirm.paymentDetails = b.details
                            },
                            onReady: function() {
                                $("#join-braintree-loading-paypal").addClass("hidden"), $(".pay-with-paypal-container").removeClass("hidden")
                            },
                            onCancelled: function() {
                                $(".join-paypal-message").removeClass("hidden"), $('input[name="payment_method_nonce"]').val(""), $("#payment-paypal-errors").html(""), a.join.model.payconfirm.nonce = ""
                            },
                            onError: function(a) {
                                console.log(a), e.resetButtonState()
                            }
                        })
                    }, e.braintreeCreditCard = function() {
                        var b = {
                            "font-size": "16px",
                            "font-family": "source-sans-pro,HelveticaNeue-Light,Helvetica Neue Light,Helvetica Neue,Helvetica Neue Regular,Arial,Helvetica,sans-serif",
                            "font-weight": "300",
                            color: "#999"
                        };
                        braintree.setup(e.clientToken(), "custom", {
                            id: p,
                            hostedFields: {
                                number: {
                                    selector: "#credit-card-number"
                                },
                                cvv: {
                                    selector: "#credit-card-security-code"
                                },
                                expirationMonth: {
                                    selector: "#credit-card-month",
                                    placeholder: "MM"
                                },
                                expirationYear: {
                                    selector: "#credit-card-year",
                                    placeholder: "YY"
                                },
                                styles: {
                                    input: {
                                        "font-size": "16px",
                                        "font-family": "source-sans-pro,HelveticaNeue-Light,Helvetica Neue Light,Helvetica Neue,Helvetica Neue Regular,Arial,Helvetica,sans-serif",
                                        "font-weight": "300",
                                        color: "#666"
                                    },
                                    "::-webkit-input-placeholder": b,
                                    ":-moz-placeholder": b,
                                    "::-moz-placeholder": b,
                                    ":-ms-inpput-placeholder": b,
                                    ".number": {
                                        "font-weight": "300"
                                    },
                                    ".valid": {
                                        color: "#666"
                                    },
                                    ".invalid": {
                                        color: "#ff7070"
                                    }
                                },
                                onFieldEvent: function(b) {
                                    var c = $(b.target.container),
                                        d = c.closest(".ctrl-holder");
                                    if ("number" === b.target.fieldKey && null !== b.card) {
                                        if ("visa" !== b.card.type && "master-card" !== b.card.type) {
                                            c.removeClass("braintree-hosted-fields-valid"), c.addClass("braintree-hosted-fields-invalid"), e.invalidCardType(!0), e.catchange(!1);
                                            var f = {
                                                message: a.responseMessage.getValidationMessage("join-payment-form", "credit-card-number", "type"),
                                                id: "credit-card-number"
                                            };
                                            return e.addCreditCardError(f), !1
                                        }
                                        e.invalidCardType(!1), e.removeCreditCardErrorById("credit-card-number")
                                    } else "number" === b.target.fieldKey && null === b.card && (e.invalidCardType(!1), e.removeCreditCardErrorById("credit-card-number"));
                                    b.isPotentiallyValid && (null !== b.card ? e.catchange(!0, b.card.type) : e.catchange(!1)), "blur" === b.type ? d.removeClass("shift-focussed") : "focus" === b.type && d.addClass("shift-focussed"), "number" === b.target.fieldKey && ("blur" === b.type ? b.isEmpty && d.removeClass("shift-label") : "focus" === b.type && d.addClass("shift-label")), b.card && e.removeCreditCardErrorById("credit-card-braintree")
                                }
                            },
                            onPaymentMethodReceived: function(b) {
                                console.log(b), a.join.model.payconfirm.nonce = b.nonce, a.join.model.payconfirm.paymentType = b.type, a.join.model.payconfirm.paymentDetails = b.details, e.cardHolderNameValidation(), e.invalidCardType() ? e.resetButtonState() : e.invalidCardHolderName() ? e.resetButtonState() : (e.creditCardErrorList.removeAll(), e.processOrder())
                            },
                            onReady: function() {
                                $("#join-braintree-loading-creditcard").addClass("hidden"), $(".pay-by-credit-card-container").removeClass("hidden")
                            },
                            onError: function(b) {
                                e.resetButtonState();
                                var c = b.message;
                                "User did not enter a payment method" === c && (c = a.responseMessage.getValidationMessage("join-payment-form", "credit-card-number-and-expiration", "required"));
                                var d = {
                                    message: c,
                                    id: "credit-card-braintree"
                                };
                                e.cardHolderNameValidation(), e.addCreditCardError(d), e.resetButtonState()
                            }
                        })
                    }, e.formReady = function() {
                        e.clientToken() && a.forms.applyShiftLabel()
                    }, e.resetButtonState = function() {
                        h.attr("data-state", "active"), a.buttons.enableButton(h)
                    }, e.confirmOrder = function() {
                        var b = h.attr("data-state");
                        if ("loading" == b) return !1;
                        if (h.attr("data-state", "loading"), a.validation.resetCustomMessages(), d.removeClass("error"), e.createOrderError = !1, a.validation.validateForm(l)) {
                            if (a.join.model.payconfirm.agreementChecked = !0, a.join.model.payconfirm.portAgreementChecked = !0, e.selectedPayment() === c.CREDITCARD) $("#" + p + " input:submit").click();
                            else if (e.selectedPayment() === c.PAYPAL)
                                if (a.join.model.payconfirm.nonce) e.processOrder();
                                else {
                                    var f = a.responseMessage.getValidationMessage("join-payment-form", "nonce", "alreadyhas");
                                    $("#payment-paypal-errors").html(f), e.resetButtonState()
                                }
                        } else e.resetButtonState()
                    }, e.processOrderDone = function(b) {
                        console.log("join order confirm successful."), sessionStorage.removeItem("broadbandTechnologyType"), sessionStorage.removeItem("product"), sessionStorage.removeItem("resultCode"), sessionStorage.removeItem("resultStatus"), sessionStorage.removeItem("sqDone"), sessionStorage.removeItem("productModel"), sessionStorage.removeItem("sqModel");
                        var c = "NA";
                        c = b.messages[0].speedBoost ? "INACTIVE_CAN_CHANGE" : "NA", a.adobeAnalytics.pages.join.confirmYourOrder({
                            serviceClass: b.messages[0].serviceClass,
                            speedBoostStatus: c,
                            promoCode: b.messages[0].promotionId,
                            planType: b.messages[0].planType,
                            planSize: b.messages[0].planSize,
                            withSpeedBoost: b.messages[0].speedboost,
                            withContract: b.messages[0].withContract,
                            orderType: b.messages[0].orderType,
                            userid: b.messages[0].userid,
                            planSpeed: b.messages[0].planSpeed,
                            voicePlan: a.join.model.product.voicePlan
                        }), a.analytics.methods.join.confirmYourOrder(b.messages[0].userid, b.messages[0].serviceClass, c, b.messages[0].promotionId, b.messages[0].planType, b.messages[0].planSize, b.messages[0].speedboost, null !== b.messages[0].contractTerm, b.messages[0].orderType, b.messages[0].planSpeed, a.join.model.product.voicePlan);
                        var d = b.messages[0].userid;
                        a.livePerson.join.confirmOrder(a.join.product.methods.getCurrentFormModel(), d);
                        var e = Math.random() + "",
                            f = 1e13 * e;
                        $("body").append('<iframe src="https://4503403.fls.doubleclick.net/activityi;src=4503403;type=CONV0001;cat=Conve0;ord=' + f + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>')
                    }, e.processOrderFail = function(b, c, f) {
                        var g = b.responseJSON;
                        console.log("join order confirm AJAX failure."), console.log(g, c, f);
                        var h = "";
                        if (a.validation.resetCustomMessages(), "timeout" === c) h = a.responseMessage.getApiMessage("join-belong", "confirmOrder", "timeout");
                        else if (void 0 !== g.messages[0] && void 0 !== typeof g.messages[0].userMessage) {
                            var i = g.messages[0].userMessage;
                            h = a.responseMessage.getApiMessage("join-belong", "confirmOrder", i), "" === h && (h = a.responseMessage.getApiMessage("join-belong", "confirmOrder", "submit-error"))
                        } else h = a.responseMessage.getApiMessage("join-belong", "confirmOrder", "submit-error");
                        console.log(h), d.addClass("error"), a.validation.attachCustomMessage("join-payconfirm-submit", h, "error", !0), a.adobeAnalytics.pages.join.errorOrder(h), a.analytics.methods.joinErrors.errorOrder(h), e.createOrderError = !0, e.resetButtonState()
                    }, e.performLogin = function() {
                        if (e.createOrderError) return console.log("Exiting performLogin due to createOrder Error."), e.createOrderError = !1, !1;
                        console.log("----------"), console.log("logging in:"), console.log("username: " + a.join.model.personalDetails.email), console.log("password: " + a.join.model.personalDetails.password);
                        var c = b.methods.buildRedirectUrl("/account/?showsuccess=1"),
                            d = b.methods.buildRedirectUrl("/login-confirmation/?showsuccess=1"),
                            f = b.methods.buildRedirectUrl("/account/?showsuccess=1");
                        k.data("isLoggedIn") && k.data("isAgent") ? (console.log("Agent logged in", k.data("isLoggedIn"), k.data("isAgent")), a.mobo.methods.grantPermissions(a.GD.loginEmail, a.join.model.personalDetails.email).then(function() {
                            window.location.href = c
                        })) : a.authenticate.methods.login(a.join.model.personalDetails.email, a.join.model.personalDetails.password, d).then(function(a) {
                            a = a.trim();
                            var b = a.substring(a.indexOf("data-logged-in") + 16, a.indexOf("data-logged-in") + (a.substring(a.indexOf("data-logged-in"), a.length).indexOf(">") - 1));
                            console.log("url: " + d), console.log("result: " + a), console.log("logged in as: " + b), "customer" === b ? window.location.href = f : ($(".join-payconfirm .join-subfooter").addClass("hidden"), $(".join-zpl-continue-section").removeClass("hidden"))
                        }, function(a) {
                            console.log("Error logging into openAM"), console.log(a), console.log("redirecting to login page " + c), window.location.href = c
                        })
                    }, e.processOrder = function() {
                        var b = {
                            nonce: a.join.model.payconfirm.nonce,
                            agreementChecked: a.join.model.payconfirm.agreementChecked,
                            portAgreementChecked: a.join.model.payconfirm.portAgreementChecked
                        };
                        console.log("processOrder request:", b);
                        var c = $.post("/bin/belong/join/order/confirm", b);
                        c.then(e.processOrderDone, e.processOrderFail), c.then(e.performLogin)
                    }
                },
                events: {
                    checkAgreement: function() {
                        if (i.prop("checked")) {
                            var b = a.join.model.product.planSize + "-" + a.join.model.product.planType;
                            a.adobeAnalytics.pages.join.steps("3-Check Agmt", "3-check-agmt.html", b), a.analytics.methods.join.joinSteps("3-Check Agmt", "3-check-agmt.html", b), $(this).parent().children('label[for="' + this.id + '"]').addClass("checked")
                        } else $(this).parent().children('label[for="' + this.id + '"]').removeClass("checked")
                    },
                    checkPortingAgreement: function() {
                        var b = j.prop("checked") ? !0 : !1;
                        if (b) {
                            var c = a.join.model.product.planSize + "-" + a.join.model.product.planType;
                            a.adobeAnalytics.pages.join.steps("3-Check Porting Agmt", "3-check-porting-agmt.html", c), a.adobeAnalytics.pages.join.steps("3-Check Porting Agmt", "3-check-porting-agmt.html", c), a.analytics.methods.join.joinSteps("3-Check Porting Agmt ", "3-check-porting-agmt.html", c), $(this).parent().children('label[for="' + this.id + '"]').addClass("checked")
                        } else $(this).parent().children('label[for="' + this.id + '"]').removeClass("checked")
                    },
                    onLogin: function(c) {
                        c.preventDefault();
                        var d = a.buttons.disableButton($(this));
                        return d.alreadyDisabled ? !1 : void a.analytics.methods.linkClickWithPageReload(b.methods.buildRedirectUrl("/account/?showsuccess=1"), "Click", "Join-Login", "No zpl")
                    }
                },
                methods: {
                    render: function() {
                        m.updateClientToken(a.join.model.payconfirm.clientToken)
                    },
                    sqUpdateHandler: function() {
                        b.methods.render()
                    },
                    setEdit: function() {
                        console.log("Entering EVE.join.payment.methods.setEdit()"), console.log("EVE.join.product.methods.getCurrentFormModel()=", a.join.product.methods.getCurrentFormModel()), console.log("EVE.join.product.methods.getCurrentFormModel().JSON:", JSON.stringify(a.join.product.methods.getCurrentFormModel())), console.log("EVE.join.model.product=", a.join.model.product), console.log("EVE.join.model.product.json=", JSON.stringify(a.join.model.product)), e.addClass("edit").removeClass("readonly").removeClass("collapsed").velocity("stop").velocity("scroll", {
                            duration: 400,
                            easing: "ease-out",
                            offset: -70
                        })
                    },
                    setReadOnly: function() {
                        e.addClass("readonly").removeClass("edit").removeClass("collapsed")
                    },
                    setCollapsed: function() {
                        e.addClass("collapsed").removeClass("edit").removeClass("readonly")
                    },
                    buildRedirectUrl: function(a) {
                        var b = window.location.protocol + "//" + window.location.host + a;
                        return b
                    }
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.join = a.join || {}, a.join.personal = function() {
            var b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, A, B, C, D, E = "join-personal",
                F = "join-personal-form",
                G = !1;
            return {
                init: function() {
                    b = this, g = $("#join-personal"), c = $("#personal-section-header"), d = $("#personal-section-footer"), h = $("#join-personal-first-name"), i = $("#join-personal-last-name"), j = $("#join-personal-email-address"), k = $("#join-personal-password"), l = $("#join-personal-conf-password"), m = $("#join-personal-dob_day"), n = $("#join-personal-dob_month"), o = $("#join-personal-dob_year"), p = $("#join-personal-dob-input"), q = $("#join-personal-mobile"), r = $("#join-personal-current-accno"), s = $("#join-personal-current-provider"), t = $("#join-personal-nameonbill"), e = $("#join-personal-continue"), f = $("#join-personal-check-email"), u = $("#join-personal-first-name-readonly"), v = $("#join-personal-last-name-readonly"), w = $("#join-personal-email-address-readonly"), x = $("#join-personal-password-readonly"), y = $("#join-personal-dob-readonly"), z = $("#join-personal-mobile-readonly"), A = $("#join-personal-current-accno-readonly"), B = $("#join-personal-current-provider-readonly"), C = $("#join-personal-nameonbill-readonly"), a.forms.date.methods.setYearsForDob(o), $("#mdl-agent-terms").length > 0 && a.GD.isAgent ? e.off("click.joinPersonal").on("click.joinPersonal", function(b) {
                        return b.preventDefault(), a.validation.validateForm(D) ? (a.agentTermsModal.open({
                            joinModel: a.join.model
                        }), !1) : !1
                    }) : e.off("click.joinPersonal").on("click.joinPersonal", b.events.onContinue), j.off("change.joinPersonal").on("change.joinPersonal", b.events.onEmailChange), m.off("change.joinPersonal").on("change.joinPersonal", b.events.onDobDayChange), n.off("change.joinPersonal").on("change.joinPersonal", b.events.onDobMonthChange), o.off("change.joinPersonal").on("change.joinPersonal", b.events.onDobYearChange), p.off("change.joinPersonal").on("change.joinPersonal", b.events.onDobChange), $(".join-personal-form").submit(function(a) {
                        a.preventDefault()
                    });
                    var E = {
                        rules: {
                            "join-personal-first-name": {
                                required: !0,
                                disallowedCharacters: !0,
                                maxlength: 255
                            },
                            "join-personal-last-name": {
                                required: !0,
                                disallowedCharacters: !0,
                                maxlength: 255
                            },
                            "join-personal-email-address": {
                                required: !0,
                                backendEmail: !0,
                                disallowedCharacters: !0,
                                maxlength: 255
                            },
                            "join-personal-password": {
                                required: !0,
                                passwordComplexity: !0,
                                disallowedCharacters: !0,
                                badPassword: !0,
                                notEqualToEmail: {
                                    emailAddressFieldID: "#join-personal-email-address"
                                },
                                minlength: 8,
                                maxlength: 30
                            },
                            "join-personal-conf-password": {
                                equalTo: "#join-personal-password"
                            },
                            "join-personal-dob-input": {
                                required: !0,
                                birthDateFormat: !0,
                                validAustralianDateString: !0,
                                overEighteen: {
                                    dobFieldID: "#join-personal-dob-input"
                                }
                            },
                            "join-personal-mobile": {
                                required: !0,
                                australianMobileNumber: !0
                            }
                        }
                    };
                    D = a.validation.setupFormValidation(F, $("." + F), E)
                },
                methods: {
                    render: function() {
                        a.sqEntry.is.portOrder(a.join.model.sq.orderType) && g.addClass("port-order")
                    },
                    setEdit: function(b) {
                        g.removeClass("readonly").removeClass("collapsed").addClass("edit"), b ? $(b).velocity("stop").velocity("scroll", {
                            duration: 400,
                            easing: "ease-out",
                            offset: -70
                        }) : (console.log("personal.setEdit(): EVE.join.methods.fromProductBuilder()=" + a.join.methods.fromProductBuilder()), a.join.methods.fromProductBuilder() || g.velocity("stop").velocity("scroll", {
                            duration: 400,
                            easing: "ease-out",
                            offset: -70
                        }))
                    },
                    setReadOnly: function() {
                        g.removeClass("edit").removeClass("collapsed").addClass("readonly")
                    },
                    setCollapsed: function() {
                        g.removeClass("edit").removeClass("readonly").addClass("collapsed"), d.removeClass("error")
                    },
                    validateEmail: function(b, c) {
                        console.log('Performing Experian validation on email via "checkCustomerUsername" service.'), a.ajax("", {
                            requestType: "checkCustomerUsername",
                            email: b
                        }, function(b) {
                            var d;
                            if (console.log("Successful response from API checkCustomerUsername"), console.log(b), b.success) console.log("Email validation successful."), G = !0, c(!0);
                            else {
                                console.log("Email validation failed."), a.validation.resetCustomMessages(), G = !1;
                                var e = "",
                                    f = b.messages;
                                e = "undefined" != typeof f ? Object.keys(b.messages[0])[0].toLowerCase() : "", console.log("Error code is: " + e);
                                var g = !1;
                                switch (e) {
                                    case "email":
                                        d = a.responseMessage.getApiMessage("join-belong", "checkCustomerUsername", e), a.validation.attachCustomMessage("join-personal-email-address", d, "error", !0), a.adobeAnalytics.pages.join.errorEmail(e, d), a.analytics.methods.joinErrors.errorEmail(e, d), g = !1;
                                        break;
                                    case "username":
                                        d = a.responseMessage.getApiMessage("join-belong", "checkCustomerUsername", e), a.validation.attachCustomMessage("join-personal-email-address", d, "error", !0), a.adobeAnalytics.pages.join.errorEmail(e, d), a.analytics.methods.joinErrors.errorEmail(e, d), g = !1;
                                        break;
                                    case "certainty":
                                        d = a.responseMessage.getApiMessage("join-belong", "checkCustomerUsername", e), a.validation.attachCustomMessage("join-personal-email-address", d, "error", !0), a.adobeAnalytics.pages.join.errorEmail(e, d), a.analytics.methods.joinErrors.errorEmail(e, d), g = !1;
                                        break;
                                    default:
                                        d = a.responseMessage.getApiMessage("join-belong", "checkCustomerUsername", e), a.validation.attachCustomMessage("join-personal-email-address", d, "error", !0), a.adobeAnalytics.pages.join.errorEmail(e, d), a.analytics.methods.joinErrors.errorEmail(e, d), g = !0
                                }
                                c(g)
                            }
                        }, function(a) {
                            console.log("Failed response from API checkCustomerUsername"), console.log(a), G = !0, c(!0)
                        })
                    },
                    validateDob: function() {
                        var b = /\d\d\/\d\d\/\d\d\d\d/.exec(p.val());
                        b && a.validation.validateInput(D, "#join-personal-dob-input")
                    },
                    saveAndContinue: function() {
                        a.join.model.personalDetails.firstName = h.val(), a.join.model.personalDetails.lastName = i.val(), a.join.model.personalDetails.email = j.val(), a.join.model.personalDetails.password = k.val(), a.join.model.personalDetails.dateOfBirth = p.val(), a.join.model.personalDetails.mobilePhone = a.validation.helpers.cleanAustralianPhoneNumber(q.val()), a.join.model.personalDetails.portCurrentAccountNumber = r.val(), a.join.model.personalDetails.portCurrentProvider = s.val(),
                            a.join.model.personalDetails.portNameOnBill = t.val(), b.methods.populateReadOnlyFields(), b.methods.processCustomer()
                    },
                    processCustomer: function() {
                        var c = {
                            planType: a.join.model.product.planType,
                            planSize: a.join.model.product.planSize,
                            planSpeed: a.join.model.product.planSpeed,
                            contractTerm: a.join.model.product.contractTerm,
                            voicePlan: a.join.model.product.voicePlan,
                            promotionId: a.join.model.product.promotionId,
                            promotionStaffId: a.join.model.product.promotionStaffId,
                            firstName: a.join.model.personalDetails.firstName,
                            lastName: a.join.model.personalDetails.lastName,
                            email: a.join.model.personalDetails.email,
                            password: a.join.model.personalDetails.password,
                            dateOfBirth: a.join.model.personalDetails.dateOfBirth,
                            mobilePhone: a.join.model.personalDetails.mobilePhone,
                            currentProvider: a.join.model.personalDetails.portCurrentProvider,
                            currentAccountNumber: a.join.model.personalDetails.portCurrentAccountNumber,
                            nameOnCurrentBill: a.join.model.personalDetails.portNameOnBill,
                            agentId: $("#header").data("isAgent") ? $("#header").data("userEmail") : "",
                            requestType: "joinFormInitPayment"
                        };
                        $.post("/bin/belong/join/save", c).done(function(c) {
                            console.log("join save successful."), console.log(c), sessionStorage.setItem("productModel", JSON.stringify(a.join.model.product));
                            var e = a.join.model.product.planSize + "-" + a.join.model.product.planType;
                            a.adobeAnalytics.pages.join.steps("2-Personal Cont.", "2-personal-cont.html", e), a.analytics.methods.join.joinSteps("2-Personal Cont.", "2-personal-cont.html", e), a.join.model.payconfirm.clientToken = c.messages[0].clientToken, a.join.payconfirm.methods.sqUpdateHandler(), d.removeClass("error"), b.methods.resetButtonState(), b.methods.setReadOnly(), a.join.methods.activateNextModule(E)
                        }).fail(function(c, e) {
                            console.log("join save AJAX failure.");
                            var f = "";
                            if ("timeout" == e) f = a.responseMessage.getApiMessage("join-belong", "personalDetails", "timeout");
                            else {
                                var g = c.responseJSON;
                                if (void 0 !== g.messages[0] && void 0 !== typeof g.messages[0].userMessage) {
                                    var h = g.messages[0].userMessage;
                                    f = a.responseMessage.getApiMessage("join-belong", "personalDetails", h), "" === f && (f = a.responseMessage.getApiMessage("join-belong", "personalDetails", "default")), console.log("Error returned from joinFormInitPayment: " + f)
                                } else f = a.responseMessage.getApiMessage("join-belong", "personalDetails", "default")
                            }
                            d.addClass("error"), a.validation.resetCustomMessages(), a.validation.attachCustomMessage("join-personal-continue", f, "error", !0), a.adobeAnalytics.pages.join.errorPersonalDetails(f), a.analytics.methods.joinErrors.errorPersonalDetails(f), b.methods.resetButtonState()
                        })
                    },
                    prefill: function() {
                        a.forms.setInputValue(h, a.join.model.personalDetails.firstName), a.forms.setInputValue(i, a.join.model.personalDetails.lastName), a.forms.setInputValue(j, a.join.model.personalDetails.email), a.forms.setInputValue(k, a.join.model.personalDetails.password), a.forms.setInputValue(l, a.join.model.personalDetails.password), a.forms.setInputValue(p, a.join.model.personalDetails.dateOfBirth), a.forms.setInputValue(q, a.join.model.personalDetails.mobilePhone), a.forms.setInputValue(r, a.join.model.personalDetails.portCurrentAccountNumber), a.forms.setInputValue(s, a.join.model.personalDetails.portCurrentProvider), a.forms.setInputValue(t, a.join.model.personalDetails.portNameOnBill), b.methods.populateReadOnlyFields(), b.methods.render()
                    },
                    populateReadOnlyFields: function() {
                        u.text(a.join.model.personalDetails.firstName), v.text(a.join.model.personalDetails.lastName), w.text(a.join.model.personalDetails.email), x.text("* * * * * * * *"), y.text(a.join.model.personalDetails.dateOfBirth), z.text(a.join.model.personalDetails.mobilePhone), A.text(a.join.model.personalDetails.portCurrentAccountNumber), B.text(a.join.model.personalDetails.portCurrentProvider), C.text(a.join.model.personalDetails.portNameOnBill)
                    },
                    sqUpdateHandler: function() {
                        b.methods.render()
                    },
                    resetButtonState: function() {
                        e.attr("data-state", "loading"), e.attr("data-state", "active")
                    }
                },
                events: {
                    onContinue: function(c) {
                        c.preventDefault(), console.log("Entering EVE.join.personal.events.onContinue()");
                        var d = e.attr("data-state");
                        return "loading" == d ? !1 : (e.attr("data-state", "loading"), a.validation.validateForm(D) ? G ? (b.methods.saveAndContinue(), !1) : void b.methods.validateEmail(j.val(), function(a) {
                            return 1 == a ? b.methods.saveAndContinue() : b.methods.resetButtonState(), !1
                        }) : (b.methods.resetButtonState(), !1))
                    },
                    onEmailChange: function() {
                        a.validation.resetCustomMessages(), G = !1
                    },
                    onDobDayChange: function() {
                        b.methods.validateDob()
                    },
                    onDobMonthChange: function() {
                        b.methods.validateDob()
                    },
                    onDobYearChange: function() {
                        b.methods.validateDob()
                    },
                    onDobChange: function() {
                        var a = p.val().split("/");
                        m.val(a[0]), m.prev("span").text(m.find("option:selected").text()), n.val(a[1]), n.prev("span").text(n.find("option:selected").text()), o.val(a[2]), o.prev("span").text(o.find("option:selected").text())
                    }
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.join = a.join || {};
        var b = function() {
            function b(a) {
                for (var b = 0; b < h.displayedProducts().length; b++)
                    for (var c = h.displayedProducts()[b], d = 0; d < c.plans.length; d++) {
                        var e = c.plans[d],
                            f = a(c, e);
                        if (f) return f
                    }
            }

            function c() {
                var a = d(h.selectedPlanId()).planType,
                    c = {};
                return b(function(b, d) {
                    d.productSize === i.EXTRA && a === d.planType && (c = d)
                }), c
            }

            function d(a) {
                var c = {};
                return b(function(b, d) {
                    d.planId === a && (c = d)
                }), c
            }

            function e(a) {
                var c = {};
                return b(function(b, d) {
                    d.planId === a && (c = b)
                }), c
            }

            function f(a) {
                return $.extend(!0, {}, a)
            }

            function g(a) {
                var b = [];
                return $.each(a, function(a, c) {
                    b.push(f(c))
                }), b
            }
            var h = this,
                i = {
                    REGULAR: "Regular",
                    LARGE: "Large",
                    EXTRA: "ExtraLarge"
                },
                j = {
                    NBN: "NBN",
                    ADSL: "ADSL"
                },
                k = {
                    BUNDLE: "BUNDLE"
                };
            h.selectedPlanId = ko.observable(""), h.extraLargePlanSelected = ko.observable(!1), h.displayedProducts = ko.observableArray([]), h.largePlanSelected = ko.computed(function() {
                return e(h.selectedPlanId()).productSize === i.LARGE
            }), h.technologyType = ko.computed(function() {
                var a = h.displayedProducts();
                return a.length > 0 ? h.displayedProducts()[0].technologyType : ""
            }), h.extendedNetwork = ko.observable(!1), h.selectedProductAndPlan = ko.computed(function() {
                var a = h.selectedPlanId();
                h.extraLargePlanSelected() && (a = c(a).planId);
                var b = d(a),
                    g = e(a);
                return b = f(b), g = f(g), g.plans = $.isEmptyObject(b) ? [] : [b], g
            }), h.selectedPlanId.subscribe(function(b) {
                var e = d(h.selectedPlanId());
                h.extraLargePlanSelected() && e.productSize === i.REGULAR && h.extraLargePlanSelected(!1);
                var f = h.extraLargePlanSelected() ? c() : d(b);
                a.join.product.events.updateProductSelectionHandler(f.planId, f.productSize, f.planType, h.technologyType())
            }), h.extraLargePlanSelected.subscribe(function(e) {
                if (e && h.selectedPlanId()) {
                    var f = d(h.selectedPlanId());
                    f.productSize !== i.LARGE && b(function(a, b) {
                        b.planType === f.planType && b.productSize === i.LARGE && h.selectedPlanId(b.planId)
                    })
                } else e && !h.selectedPlanId() && b(function(a, b) {
                    b.productSize === i.LARGE && (a.productType === j.ADSL && b.planType === k.BUNDLE ? h.selectedPlanId(b.planId) : a.productType === j.NBN && h.selectedPlanId(b.planId))
                });
                var g = h.extraLargePlanSelected() ? c() : d(h.selectedPlanId());
                a.join.product.events.updateProductSelectionHandler(g.planId, g.productSize, g.planType, h.technologyType())
            }), h.initDisplayedProducts = function() {
                var b = a.sqEntry.is.nbnOrder(a.join.model.sq.broadbandTechType),
                    c = a.sqEntry.is.churnOrder(a.join.model.sq.orderType);
                h.extendedNetwork(a.join.model.sq.extendedNetwork);
                var d = JOIN_PRODUCT_STRUCTURE.products.filter(function(a) {
                    return a.productType === (b ? j.NBN : j.ADSL)
                });
                d = g(d), $.each(d, function(b, d) {
                    var e = [];
                    $.each(d.plans, function(b, d) {
                        d.technologyType === a.join.model.sq.broadbandTechType && (c ? "DSL" === d.planType || "BUNDLE" === d.planType : "DSL" !== d.planType) && e.push(d)
                    }), d.plans = e
                }), a.join.model.sq.extendedNetwork && $.each(d, function(a, b) {
                    $.each(b.plans, function(a, b) {
                        b.monthlyPrice = parseInt(b.monthlyPrice) + JOIN_PRODUCT_STRUCTURE.extendedNetworkSurcharge, b.monthlyContractPrice = parseInt(b.monthlyContractPrice) + JOIN_PRODUCT_STRUCTURE.extendedNetworkSurcharge
                    })
                }), h.displayedProducts(h.convertProductStructureToDisplayModel(d)), a.tooltip()
            }, h.convertProductStructureToDisplayModel = function(a) {
                return $.each(a, function(a, b) {
                    var c = b.productSize;
                    delete b.productSize, b.technologyType = b.plans.length > 0 ? b.plans[0].technologyType : "", b.technologyDisplayName = b.plans.length > 0 ? b.plans[0].technologyDisplayName : "", $.each(b.plans, function(a, b) {
                        delete b.technologyType, delete b.technologyDisplayName, b.productSize = c
                    })
                }), a
            }, h.mapIncoming = function(a) {
                var c = d(a.html_plan_id);
                c.productSize === i.EXTRA ? (b(function(a, b) {
                    b.planType === c.planType && b.productSize === i.LARGE && h.selectedPlanId(b.planId)
                }), h.extraLargePlanSelected(!0)) : h.selectedPlanId(a.html_plan_id)
            }, h.mapOutgoing = function(a) {
                return a.html_plan_id = h.selectedPlanId(), a
            }
        };
        a.join.product = function() {
            var c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z = "join-products",
                A = "join-product-form",
                B = {
                    products: {},
                    charges: {},
                    belongTrial: !1
                },
                C = ["NATIONAL", "BOTH"],
                D = ["INTERNATIONAL", "BOTH"],
                E = {},
                F = !1,
                G = [];
            return {
                init: function() {
                    c = this, d = $("#join-product-continue"), e = $('.join-product input[name="product-selection-input"]'), f = $('.join-product input[name="chk-join-speedboost"]'), g = $('.join-product input[name="chk-join-contract"]'), h = $('.join-product input[name="chk-join-belong-voice-national"]'), i = $('.join-product input[name="chk-join-belong-voice-international"]'), j = $('.voice-options input[type="checkbox"]'), k = $(".voice-options"), l = $(".join-product .promo-apply"), n = $(".join-product-banner"), s = $("#txtpromocode"), t = $(".join-product-promocode"), u = $("#PromoSectionState"), m = $(".product-option-wrapper"), o = $("#join-product-charges"), r = $(".join-product-options"), v = $(".charges-row.first-month-free"), p = $(".belong-voice-charge-national"), q = $(".belong-voice-charge-international"), x = a.validation.setupFormValidation(A, $("." + A), {
                        ignore: "",
                        rules: {
                            "product-selection-input": {
                                required: !0
                            }
                        }
                    }), c.initKnockout(), d.off("click.onContinue").on("click.onContinue", c.events.onContinue), f.off("click.speedboostHandler").on("click.speedboostHandler", c.events.speedboostHandler), g.off("click.contractHandler").on("click.contractHandler", c.events.contractHandler), j.off("click.belongVoiceHandler").on("click.belongVoiceHandler", c.events.belongVoiceHandler), l.off("click.applyPromotionHandler").on("click.applyPromotionHandler", c.events.applyPromotionHandler), s.off("keyup.changePromoHandler").on("keyup.changePromoHandler", c.events.changePromoHandler), s.on("keyup.promoApplyButton", c.display.promoApplyButton), $(".join-product-form").submit(function(a) {
                        a.preventDefault()
                    }), k.find(".tooltip").off("click.voiceTooltipHandler").on("click.voiceTooltipHandler", c.events.voiceTooltipHandler), c.methods.setupProductSelectionItemClasses(), c.hide.promoApplyButton(), c.methods.setupCmsData(), E = c.methods.getDefaultModel(), c.methods.setupPromoSectionExpandedIfRequired()
                },
                initKnockout: function() {
                    y = new b, ko.applyBindings(y, document.getElementById("join-products"))
                },
                getViewModel: function() {
                    return y
                },
                methods: {
                    getCurrentFormModel: function() {
                        return E
                    },
                    setupProductSelectionItemClasses: function() {
                        for (var a, b = $(".join-product .product-selection-item"), c = 0; c < b.length; c++) a = c % 2 === 0 ? "odd" : "even", $(b[c]).closest(".content-column").addClass(a)
                    },
                    resetPromoIdWithStoredPromoCode: function() {
                        return a.join.model.product.promotionId = (a.join.model.product.promotionId || localStorage.getItem("promocode") || "").trim(), a.join.model.product.promotionId
                    },
                    setupPromoSectionExpandedIfRequired: function() {
                        var b = u.val();
                        "expanded" === b.toLowerCase() ? (console.log("EVE.join.product.setupPromoSectionExpandedIfRequired(EXPAND): stateValue=" + b), a.collapsibles.methods.toggleCollapsibleContainer(t, "expand")) : console.log("EVE.join.product.setupPromoSectionExpandedIfRequired(DEFAULT): stateValue=" + b)
                    },
                    getDefaultModel: function() {
                        var a = {
                            planType: "",
                            speedboost: !1,
                            planSize: "",
                            planSpeed: "",
                            fttb: !1,
                            fttn: !1,
                            withContract: c.methods.getDefaultWithContractOption(),
                            promotionId: c.methods.resetPromoIdWithStoredPromoCode(),
                            voicePlan: "",
                            voice_national: !1,
                            voice_international: !1,
                            promotionStaffId: "",
                            activation_fee: B.charges.activation_fee,
                            modem_fee: B.charges.modem_fee,
                            total_minimum_cost: 0,
                            monthly_total_cost: 0,
                            speed_boost_fee: 0,
                            contract_months: 1,
                            plan_id: "",
                            html_plan_id: "",
                            monthly_price: 0,
                            speedboostAvailable: !1,
                            promotion_success: !1,
                            charges_title: "",
                            promotion: {}
                        };
                        return a
                    },
                    getDefaultWithContractOption: function() {
                        return a.join.model.sq.isPreprovisioned ? !1 : !0
                    },
                    prefill: function() {
                        console.log("Entering EVE.join.product.prefill()============================================"), console.log("Entering EVE.join.product.prefill(): formModel.speedboost (BEFORE)=" + E.speedboost), console.log("Entering EVE.join.product.prefill(): EVE.join.model.product.planSpeed=" + a.join.model.product.planSpeed), E = c.methods.getDefaultModel(), E.planType = a.join.model.product.planType, E.speedboost = a.join.model.product.planSpeed ? !0 : !1, E.planSpeed = a.join.model.product.planSpeed, E.planSize = a.join.model.product.planSize, E.voicePlan = a.join.model.product.voicePlan, E.promotionId = a.join.model.sq.isPreprovisioned ? a.join.model.sq.promotionId : a.join.model.product.promotionId, E.promotionStaffId = a.join.model.product.promotionStaffId, E.technologyType = a.join.model.sq.broadbandTechType, E.promotion = null, E.calculatedMTC = a.join.model.product.calculatedMTC, console.log("Entering EVE.join.product.prefill(): formModel.speedboost (AFTER)=" + E.speedboost);
                        var b = "true" === a.util.loadPageVar("productbuilder");
                        b && (E.withContract = null !== a.join.model.product.contractTerm), E.voice_national = -1 !== $.inArray(E.voicePlan, C), E.voice_international = -1 !== $.inArray(E.voicePlan, D), E.plan_id = E.planSize && E.planType ? E.planSize + "-" + E.planType + "-" + E.technologyType : null, console.log("EVE.join.product.prefill(): formModel", E), c.methods.updateViewBasedOnSq(!0), c.methods.updateModel(), c.methods.render(), c.getViewModel().mapIncoming(E), a.join.common.updateAccessType(E.planType)
                    },
                    getDefaultPlan: function(b) {
                        var c;
                        c = a.sqEntry.is.nbnOrder(b) ? "NBN" : "BUNDLE";
                        var d = {
                            planType: c,
                            planSize: "Regular",
                            plan_id: null,
                            technologyType: b.toUpperCase()
                        };
                        return d.plan_id = d.planSize + "-" + d.planType + "-" + d.technologyType, d
                    },
                    isSpeedBoostAvailableForCurrentPlansDisplayed: function() {
                        return a.sqEntry.is.nbnOrder(a.join.model.sq.broadbandTechType) ? !0 : !1
                    },
                    render: function() {
                        c.methods.renderSummary();
                        var b = a.join.model.sq.isPreprovisioned;
                        if (console.log("EVE.join.product.render(): formModel", E), c.methods.renderNoPlanSelected($('input[name="chk-join-speedboost"]:checked:visible').length > 0), "NBN" === E.planType ? $(".bc-dark-green").removeClass("hidden") : ($(".bc-lime").last().css("border-radius", "0 0 5px 5px"), $(".bc-dark-green").addClass("hidden")), c.methods.isSpeedBoostAvailableForCurrentPlansDisplayed() ? $(".b-speedboost").removeClass("hidden") : $(".b-speedboost").addClass("hidden"), E.speedboostAvailable && E.speedboost) switch ($(".charges-body .speed-boost-fee").removeClass("hidden"), E.planSpeed) {
                            case "25/5":
                                $(".charges-body .speed-boost-fee").removeClass("hidden").find(".charges-cost").text("$" + (b ? 0 : B.charges.speed_boost_fee_1)), $.each($('input[name="chk-join-speedboost"]'), function() {
                                    "1" === $(this).val() && ($("#speed-boost-variation-3").hasClass("hidden") || ($("#speed-boost-variation-3 .is-collapsed a").trigger("open.expandcollapse"), setTimeout(function() {
                                        $("#chk-join-speedboost_3_1").prop("checked", "checked")
                                    }, 1)), $(this).prop("checked", "checked"), $(this).closest("li").addClass("checked").closest(".ctrl-holder").addClass("join-readonly"))
                                });
                                break;
                            case "100/40":
                                $(".charges-body .speed-boost-fee").removeClass("hidden").find(".charges-cost").text("$" + (b ? 0 : B.charges.speed_boost_fee_2)), $.each($('input[name="chk-join-speedboost"]'), function() {
                                    "2" === $(this).val() && ($("#speed-boost-variation-3").hasClass("hidden") || ($("#speed-boost-variation-3 .is-collapsed a").trigger("open.expandcollapse"), setTimeout(function() {
                                        $("#chk-join-speedboost_3_2").prop("checked", "checked")
                                    }, 1)), $(this).prop("checked", "checked"), $(this).closest("li").addClass("checked").closest(".ctrl-holder").addClass("join-readonly"))
                                })
                        } else $(".charges-body .speed-boost-fee").addClass("hidden");
                        if ("fttb" === a.join.model.sq.broadbandTechType.toLowerCase() || "fttn" === a.join.model.sq.broadbandTechType.toLowerCase() ? ($(".nbn-fttb").removeClass("hidden"), $(".nbn-all").addClass("hidden")) : ($(".nbn-fttb").addClass("hidden"), $(".nbn-all").removeClass("hidden")), c.methods.renderContractAndDslElements(), E.withContract ? g.prop("checked", !0) : !1, E.voice_national ? h.prop("checked", !0) : !1, E.voice_international ? i.prop("checked", !0) : !1, E.speedboost || E.withContract || E.voice_national || E.voice_international ? $(".join-product-options").closest(".join-content").addClass("join-readonly") : $(".join-product-options").closest(".join-content").removeClass("join-readonly"), $("#" + z).hasClass("edit") ? (f.removeAttr("disabled"), g.removeAttr("disabled"), j.removeAttr("disabled")) : (f.attr("disabled", "disabled"), g.attr("disabled", "disabled"), j.attr("disabled", "disabled")), E.plan_id) {
                            $(".join-product-charges").removeClass("hidden").closest(".join-edit").addClass("join-readonly"), $(".join-product-options").removeClass("hidden"), $(".join-product-promocode").removeClass("hidden"), E.speedboost ? $('input[name="chk-join-speedboost"]:visible:checked').closest("li").addClass("checked").closest(".ctrl-holder").addClass("join-readonly") : $('input[name="chk-join-speedboost"]:visible:checked').closest("li").addClass("checked").closest(".ctrl-holder").removeClass("join-readonly"), E.withContract ? g.closest("li").addClass("checked").closest(".checkbox-holder").addClass("join-readonly") : g.closest("li").addClass("checked").closest(".checkbox-holder").removeClass("join-readonly"), E.voice_national ? h.closest("li").addClass("checked").closest(".ctrl-holder").addClass("join-readonly") : h.closest("li").addClass("checked").closest(".ctrl-holder").removeClass("join-readonly"), E.voice_international ? i.closest("li").addClass("checked").closest(".ctrl-holder").addClass("join-readonly") : i.closest("li").addClass("checked").closest(".ctrl-holder").removeClass("join-readonly"), k.find(".tooltip-btn button").addClass("join-edit"), k.find(".tooltip-btn button").off("click.toggleTooltip").on("click.toggleTooltip", function() {
                                var a = $(this).closest(".expandcollapse-content");
                                $.each(a.find(".radio-holder"), function(a, b) {
                                    $(b).removeClass("foreground")
                                }), $(this).closest(".radio-holder").addClass("foreground")
                            });
                            var d = E.charges_title;
                            a.sqEntry.is.adsl1Order(a.join.model.sq.broadbandTechType) && (d = d.replace("ADSL2+", "ADSL")), $(".charges-body .monthly-price .charges-name").html(d), $(".charges-body .monthly-price .charges-cost").html(b ? "$0" : E.withContract ? "$" + E.contract_price : "$" + E.monthly_price), $(".charges-body .col-total .total-min-cost").html(c.methods.formatCurrency(E.total_minimum_cost)), b ? (p.find(".charges-cost").text("$0"), q.find(".charges-cost").text("$0")) : (p.find(".charges-cost").text("$" + E.voice_national_cost), q.find(".charges-cost").text("$" + E.voice_international_cost)), E.voice_national ? p.removeClass("hidden") : p.addClass("hidden"), E.voice_international ? q.removeClass("hidden") : q.addClass("hidden"), B.charges.modem_rebate_fee <= 0 && $(".charges-body .modem-rebate").addClass("hidden"), "DSL" === E.planType ? $(".charges-body .col-total .phone-line-text").removeClass("hidden") : $(".charges-body .col-total .phone-line-text").addClass("hidden"), $("#join-product-charges .activation-fee .charges-cost").text("$" + c.methods.formatCurrency(E.activation_fee)), $("#join-product-charges .modem-fee .charges-cost").text("$" + c.methods.formatCurrency(E.modem_fee)), $("#txtpromocode").val(E.promotionId), E.promotionId || ($(".charges-body .special").addClass("hidden"), c.methods.updatePromoView("", ".default")), c.methods.renderPromo()
                        }
                    },
                    renderSummary: function() {
                        console.log("EVE.join.product.methods.renderSummary()"), console.log("formModel", JSON.stringify(E));
                        var b = E.charges_title,
                            d = "",
                            e = E.monthly_total_cost,
                            f = "",
                            g = "",
                            h = E.withContract ? E.contract_price : E.monthly_price;
                        1 == E.withContract ? (b += " on " + E.contract_months + " month contract", f = "Min cost over " + E.contract_months + " months: $" + E.total_minimum_cost) : f = "Min cost: $" + E.total_minimum_cost, 1 == E.speedboost && (b += " with " + E.planSpeed + " Speed Boost");
                        var i = 0;
                        if ((1 == E.voice_national || 1 == E.voice_international) && (1 == E.voice_national && 1 == E.voice_international ? (d = "Belong Voice National &amp; International", i = E.voice_national_cost + E.voice_international_cost) : 1 == E.voice_national ? (d = "Belong Voice National", i = E.voice_national_cost) : 1 == E.voice_international && (d = "Belong Voice International", i = E.voice_international_cost)), g = "", g = 1 == E.withContract ? "Min cost over " + E.contract_months + " months: $" + h + " broadband charge x " + E.contract_months + " months" : "Min cost: $" + h + " broadband charge", g += "", 1 == E.speedboost && (g += 1 == E.withContract ? " + $" + E.speed_boost_fee + " Speed Boost x " + E.contract_months + " months" : " + $" + E.speed_boost_fee + " Speed Boost x 1 month"), (1 == E.voice_national || 1 == E.voice_international) && (g += 1 == E.withContract ? " + $" + i + " Belong Voice x " + E.contract_months + " months" : " + $" + i + " Belong Voice x 1 month"), 0 != E.modem_fee && (g += " + $" + E.modem_fee + " modem"), 0 != E.activation_fee && (g += " + $" + E.activation_fee + " activation fee"), g += ". ", a.join.model.product.campaignCode && a.join.model.product.promotionId) {
                            var j = a.join.model.product.campaignCode,
                                k = c.methods.getPromoMessage(j);
                            console.log("promoCodeChipText=" + k), "" != j && ($(".join-product-summary__promo-code .promo-code-chip__msg").html(k), $(".join-product-summary__promo-code").removeClass("hidden"))
                        }
                        $(".join-product-summary__inclusions-plan").html(b), "" != d && $(".join-product-summary__inclusions-voice").html(d).removeClass("hidden"), $(".join-product-summary__monthly-amount-value").html(e), $(".join-product-summary__min-cost").html(f), $(".join-product-summary__min-cost-breakdown").html(g);
                        var l = "",
                            m = $("#join-product-access-types p").not(".hidden");
                        if (console.log("accessTypesText:", m), m.length > 0 ? console.log("Access type already selected.") : (console.log("No access type selected yet. Go!"), a.join.common.updateAccessType(E.planType)), m = $("#join-product-access-types p").not(".hidden"), m.length > 0) {
                            console.log("Second change access type selected"), l = m.html(), console.log("accessType (2)=" + m.html());
                            var n = l.replace(/\<br\>/g, "");
                            console.log("newAccessType (2)=" + n), l = n
                        }
                        if (E.withContract) {
                            var o = $("#join-products p.contract");
                            o.length > 0 && (l = l + "<p>" + o.html() + "</p>")
                        }
                        console.log("finePrint=" + l), $(".join-product-summary__fine-print").html(l)
                    },
                    getPromoMessage: function(b) {
                        return a.util.getCMSMessage(b, CMS_PROMOCODE.appliedPromoMessages)
                    },
                    renderContractAndDslElements: function() {
                        E.withContract ? ($(".b-contract").removeClass("hidden"), $(".non-contract").addClass("hidden")) : ($(".b-contract").addClass("hidden"), $(".non-contract").removeClass("hidden"))
                    },
                    formatCurrency: function(a) {
                        return a && -1 != a.toString().indexOf(".") ? parseFloat(a).toFixed(2) : a
                    },
                    renderNoPlanSelected: function(a) {
                        a = a || !1, f.closest("li").removeClass("checked"), g.closest("li").removeClass("checked"), j.closest("li").removeClass("checked"), $("#join-product-charges").addClass("hidden"), a || (f.closest("li").removeClass("checked"), f.removeAttr("checked"), f.removeAttr("disabled")), g.removeAttr("checked"), g.removeAttr("disabled"), j.removeAttr("checked").removeAttr("disabled")
                    },
                    sqUpdateHandler: function() {
                        console.log("Entering EVE.join.product.methods.sqUpdateHandler()"), c.methods.updateViewBasedOnSq(), c.methods.resetForm(), c.getViewModel().mapIncoming(E)
                    },
                    handlePreProvisionedRender: function() {
                        var b = g.closest(".ctrl-holder");
                        a.join.model.sq.isPreprovisioned ? (b.addClass("hidden"), $(".join-product-options > .join-title-subsection").addClass("hidden"), n.addClass("hidden"), g.prop("checked", !1), E.withContract = !1, console.log("join-product handlePreProvisionedRender: before disabling promoCode"), null !== a.join.model.sq.promotionId && "" !== a.join.model.sq.promotionId ? (s.prop("disabled", !0), s.addClass("manual-enable"), l.closest(".btn-col").addClass("hidden")) : (s.prop("disabled", !1), l.closest(".btn-col").removeClass("hidden"))) : ($(".join-product-options > .join-title-subsection").removeClass("hidden"), b.removeClass("hidden"), n.removeClass("hidden"), s.prop("disabled", !1), l.closest(".btn-col").removeClass("hidden"))
                    },
                    handlePreProvisionedUpdateModel: function() {
                        a.join.model.sq.isPreprovisioned && (E.withContract = !1)
                    },
                    renderChargesForPreprovisioned: function() {},
                    updateViewBasedOnSq: function() {
                        if (console.log("EVE.join.product.updateViewBasedOnSq(): EVE.join.model.sq", a.join.model.sq), a.sqEntry.is.nbnOrder(a.join.model.sq.broadbandTechType)) console.log("updateViewBasedOnSq(): is NBN");
                        else if (console.log("EVE.join.model.sq.broadbandTechType=" + a.join.model.sq.broadbandTechType), a.sqEntry.is.adsl1Order(a.join.model.sq.broadbandTechType)) {
                            console.log("updateViewBasedOnSq(): is ADSL1");
                            var b = "";
                            b += "#join-product-access-types #atype-ADSL2_BUNDLE, #join-product-access-types #atype-ADSL2_BROADBAND";
                            var d = $(b);
                            d.length > 0 && d.each(function() {
                                var a = $(this).html();
                                a = a.replace("ADSL2+", "ADSL"), $(this).html(a)
                            })
                        } else console.log("updateViewBasedOnSq(): is ADSL2");
                        $(".join-product-access-types").removeClass("hidden"), $(".access-type").addClass("hidden"), c.methods.handlePreProvisionedRender(), a.join.common.switchContractOption(E.withContract), y.initDisplayedProducts()
                    },
                    updateModel: function() {
                        var b = !1;
                        if (a.sqEntry.is.nbnOrder(a.join.model.sq.broadbandTechType) ? "NBN" !== E.planType && (b = !0) : "NBN" === E.planType && (b = !0), b === !0 && (E.planType = "", E.planSize = "", E.plan_id = ""), null === E.plan_id || "" === E.plan_id) {
                            var d = c.methods.getDefaultPlan(a.join.model.sq.broadbandTechType);
                            E.planType = d.planType, E.planSize = d.planSize, E.plan_id = d.plan_id, a.adobeAnalytics.pages.join.productPreSelection(E.plan_id), a.analytics.methods.join.productPreSelection(E.plan_id)
                        }
                        if (E.plan_id) {
                            E.planType = B.products[E.plan_id].plan_type, E.planSize = B.products[E.plan_id].plan_size, E.html_plan_id = B.products[E.plan_id].html_plan_id, E.monthly_price = B.products[E.plan_id].monthly_price, E.contract_price = B.products[E.plan_id].contract_price, E.speedboostAvailable = B.products[E.plan_id].speedboost, a.join.model.sq.extendedNetwork && (E.monthly_price = parseInt(E.monthly_price) + JOIN_PRODUCT_STRUCTURE.extendedNetworkSurcharge, E.contract_price = parseInt(E.contract_price) + JOIN_PRODUCT_STRUCTURE.extendedNetworkSurcharge), E.promotionId && !E.changingPromo ? c.methods.hasPromoBeenApplied() || c.methods.getPromotion(E.promotionId) : c.methods.resetOnceOffFees();
                            var e, f = E.withContract ? E.contract_price : E.monthly_price;
                            if (E.speedboost) switch (E.planSpeed.toString()) {
                                case "25/5":
                                    e = parseFloat(B.charges.speed_boost_fee_1);
                                    break;
                                case "100/40":
                                    e = parseFloat(B.charges.speed_boost_fee_2)
                            } else e = 0;
                            var g = 0;
                            E.voice_national_cost = 0, E.voice_international_cost = 0, B.belongTrial || (E.voice_national && (E.voice_national_cost = B.charges.voice_national, g += parseFloat(B.charges.voice_national)), E.voice_international && (E.voice_international_cost = B.charges.voice_international, g += parseFloat(B.charges.voice_international))), c.methods.updateVoicePlan(E.voice_national, E.voice_international);
                            var h = (E.withContract ? B.charges.modem_rebate_fee : 0, E.withContract ? B.charges.contract_length : 1);
                            E.contract_months = h, E.activation_fee = B.charges.activation_fee;
                            var i = E.promotion && E.promotion.success && E.promotion.promotion && E.promotion.promotion.promotion || !1;
                            if (i && i[c.methods.getPromoOrderType()] && (E.activation_fee = i[c.methods.getPromoOrderType()]), E.modem_fee = c.methods.getBaseModemFee(), E.withContract) {
                                if (E.promotion && c.methods.isPromoAppliedSuccessful()) {
                                    var j = E.promotion.promotion.modemCostContract;
                                    "undefined" != typeof j && "" != j && j >= 0 && j <= E.modem_fee && (E.modem_fee = j);
                                    var k = E.promotion.promotion.modemDiscountContract;
                                    "undefined" != typeof k && "" != k && k >= 0 && (E.modem_fee = parseFloat(E.modem_fee - E.modem_fee * k).toFixed(2))
                                }
                            } else if (E.promotion && c.methods.isPromoAppliedSuccessful()) {
                                var l = E.promotion.promotion.modemCostNonContract;
                                "undefined" != typeof l && "" != l && l >= 0 && (E.modem_fee = l);
                                var m = E.promotion.promotion.modemDiscountNonContract;
                                "undefined" != typeof m && "" != m && m >= 0 && (E.modem_fee = parseFloat(E.modem_fee - E.modem_fee * m).toFixed(2))
                            }
                            var n = 0;
                            a.join.model.sq.isPreprovisioned && (n = f + e + g), console.log("join-product updateModel preProvisionedDiscount: " + n), console.log("join-product updateModel belong_voice_fee: " + g), E.total_minimum_cost = E.calculatedMTC ? E.calculatedMTC : (f + e - n + g) * h + parseFloat(E.activation_fee) + parseFloat(E.modem_fee), E.speed_boost_fee = e, E.monthly_total_cost = f + e + g, E.charges_title = B.products[E.plan_id].title_incl_size
                        }
                    },
                    updateVoicePlan: function(b, c) {
                        b && c ? (E.voicePlan = "BOTH", a.adobeAnalytics.pages.join.addVoice(E.voicePlan), a.analytics.methods.join.addVoice(E.voicePlan)) : b ? (E.voicePlan = "NATIONAL", a.adobeAnalytics.pages.join.addVoice(E.voicePlan), a.analytics.methods.join.addVoice(E.voicePlan)) : c ? (E.voicePlan = "INTERNATIONAL", a.adobeAnalytics.pages.join.addVoice(E.voicePlan), a.analytics.methods.join.addVoice(E.voicePlan)) : E.voicePlan = ""
                    },
                    renderPromo: function() {
                        if (a.validation.resetCustomMessages(w), console.log("eve model" + a.join.model.promotionId), console.log("eve product" + a.join.product.promotionId), $(".promo-input-wrapper .error").removeClass("error"), $(".charges-row.modem-fee .special").addClass("hidden"), $("#txtpromocode").val(E.promotionId), c.methods.hasPromoBeenApplied())
                            if (E.promotion && E.promotion.success)
                                if (c.methods.isCurrentPromoForContractAndNonContract()) c.methods.updatePromoView("." + E.promotion.promotion.campaignType, ".applied"), c.methods.isCurrentPromoAModemPromo() && $(".charges-row.modem-fee .special").removeClass("hidden"), console.log("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB"), c.methods.isCurrentPromoAnActivationPromo() && (console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"), $("#join-product-charges .activation-fee .special").removeClass("hidden"));
                                else {
                                    if (c.methods.isCurrentPromoContract() && E.withContract) c.methods.updatePromoView("." + E.promotion.promotion.campaignType, ".applied"), c.methods.isCurrentPromoAModemPromo() && $(".charges-row.modem-fee .special").removeClass("hidden");
                                    else if (c.methods.isCurrentPromoContract() && !E.withContract) {
                                        c.methods.resetPromotion();
                                        var b = a.responseMessage.getApiMessage("join-belong", "validatePromotion", "ONLY_CONTRACT");
                                        w = a.validation.attachCustomMessage("txtpromocode", b, "error", !0), $("#txtpromocode").addClass("error")
                                    } else if (c.methods.isCurrentPromoNonContract() && E.withContract) {
                                        var b = a.responseMessage.getApiMessage("join-belong", "validatePromotion", "ONLY_NONCONTRACT");
                                        w = a.validation.attachCustomMessage("txtpromocode", b, "error", !0), $("#txtpromocode").addClass("error")
                                    } else c.methods.isCurrentPromoNonContract() && !E.withContract && (c.methods.updatePromoView("." + E.promotion.promotion.campaignType, ".applied"), c.methods.isCurrentPromoAModemPromo() && $(".charges-row.modem-fee .special").removeClass("hidden"));
                                    if ("HALO5" === BELONG.join.model.promotionId && !E.withContract) {
                                        var b = a.responseMessage.getApiMessage("join-belong", "validatePromotion", "ONLY_CONTRACT");
                                        w = a.validation.attachCustomMessage("txtpromocode", b, "error", !0), $("#txtpromocode").addClass("error")
                                    }
                                }
                        else {
                            var d = "" !== E.promotion.errorType && void 0 !== E.promotion.errorType ? E.promotion.errorType : "ERROR_DEFAULT",
                                b = a.responseMessage.getApiMessage("join-belong", "validatePromotion", d);
                            w = a.validation.attachCustomMessage("txtpromocode", b, "error", !0)
                        }
                    },
                    hasPromoBeenApplied: function() {
                        return E.promotion && Object.keys(E.promotion).length > 0
                    },
                    isCurrentPromoForContractAndNonContract: function() {
                        if (E.promotion) {
                            if (E.promotion.promotion.promotion || E.promotion.promotion.discount > 0) return !0;
                            if (c.methods.isCurrentPromoContract() && c.methods.isCurrentPromoNonContract() || !c.methods.isCurrentPromoContract() && !c.methods.isCurrentPromoNonContract()) return !0
                        }
                        return !1
                    },
                    isCurrentPromoContract: function() {
                        if (console.log(E), E.promotion.promotion) {
                            var a = E.promotion.promotion.modemCostContract,
                                b = E.promotion.promotion.modemDiscountContract,
                                d = "undefined" != typeof a && "" != a && a >= 0 && a <= c.methods.getBaseModemFee(),
                                e = "undefined" != typeof b && "" != b && b >= 0;
                            return d || e
                        }
                        return !1
                    },
                    isCurrentPromoNonContract: function() {
                        if (E.promotion.promotion) {
                            var a = E.promotion.promotion.modemCostNonContract,
                                b = E.promotion.promotion.modemDiscountNonContract,
                                c = "undefined" != typeof a && "" != a && a >= 0,
                                d = "undefined" != typeof b && "" != b && b >= 0;
                            return c || d
                        }
                        return !1
                    },
                    isCurrentPromoAModemPromo: function() {
                        return c.methods.isCurrentPromoContract() && E.withContract || c.methods.isCurrentPromoNonContract() && !E.withContract
                    },
                    getPromoOrderType: function() {
                        return a.sqEntry.is.nbnOrder(a.join.model.sq.broadbandTechType) ? "NEW_ORDER" : a.join.model.sq.orderType;

                    },
                    isCurrentPromoAnActivationPromo: function() {
                        var a = E.promotion && E.promotion.success && E.promotion.promotion && E.promotion.promotion.promotion || !1;
                        return a && a[c.methods.getPromoOrderType()] ? !0 : !1
                    },
                    isPromoAppliedSuccessful: function() {
                        return E.promotion && E.promotion.success
                    },
                    getBaseModemFee: function() {
                        var a = B.charges.modem_fee;
                        return B.charges.modem_contract_rebate >= 0 && E.withContract && (a = B.charges.modem_contract_rebate), a
                    },
                    setupCmsData: function() {
                        B.charges = {
                            activation_fee: o.data("activation-fee"),
                            modem_fee: o.data("modem-fee"),
                            speed_boost_fee_1: o.data("speedBoostFeeOne"),
                            speed_boost_fee_2: o.data("speedBoostFeeTwo"),
                            modem_rebate_fee: o.data("modem-rebate"),
                            contract_length: o.data("contract-length"),
                            modem_contract_rebate: o.data("modem-contract-rebate"),
                            voice_national: o.data("voice-national"),
                            voice_international: o.data("voice-international")
                        }, $.each(JOIN_PRODUCT_STRUCTURE.products, function(a, b) {
                            $.each(b.plans, function(a, c) {
                                var d = b.productSize + "-" + c.planType + "-" + c.technologyType;
                                B.products[d] = {
                                    html_plan_id: c.planId,
                                    plan_size: b.productSize,
                                    data_allowance: b.dataAllowance,
                                    speedboost: "NBN" === b.productType ? !0 : !1,
                                    plan_type: c.planType,
                                    monthly_price: parseInt(c.monthlyPrice),
                                    contract_price: parseInt(c.monthlyContractPrice),
                                    title_incl_size: c.chargesDisplayName,
                                    technologyType: c.technologyType,
                                    technologyDisplayName: c.technologyDisplayName
                                }
                            })
                        }), B.charges.modem_rebate_fee > 0 && $(this).find(".monthly-price .value .b-contract").html(parseFloat($(this).data("monthly-price")) - parseFloat(B.charges.modem_rebate_fee)), B.belongTrial = k.data("trial")
                    },
                    getPromotion: function(b) {
                        var d, e = a.DEVSWITCH ? "assets/json/promoCode.json" : "";
                        return "" === b ? (c.methods.resetPromotion(), !1) : (d = {
                            code: b,
                            requestType: "validatePromotion"
                        }, E.promotion = null, E.promotionId = b, $("#join-product-promocode").addClass("loading"), void a.ajax(e, d).then(function(d) {
                            $("#join-product-promocode").removeClass("loading"), E.promotion = d, d.success ? ("undefined" != typeof d.promotion.tacTemplate && d.promotion.tacTemplate ? a.join.model.product.staffPromoSavedInSession || (c.methods.populateStaffPromoTandC(d.promotion.tacTemplate), c.methods.displayStaffPromoModal(d, b)) : c.methods.setPromotion(d, b), "RAPID" === b.toUpperCase() ? ($("#atype-NBNSC3_pre_provisioning").removeClass("hidden"), $("#atype-NBNSC3").addClass("hidden").addClass("rapid"), $(".bc-dark-green").hide(), $(".product-selection-item.bc-lime").last().css("border-radius", "0 0 5px 5px")) : $("#atype-NBNSC3").hasClass("rapid") && ($("#atype-NBNSC3_pre_provisioning").addClass("hidden"), $("#atype-NBNSC3").removeClass("hidden").remove("rapid"))) : (c.methods.resetPromotion(), c.methods.updateModel(), c.methods.render())
                        }, function() {
                            $("#join-product-promocode").removeClass("loading"), c.methods.resetPromotion(), a.validation.resetCustomMessages(), w = a.validation.attachCustomMessage("txtpromocode", "An unexpected error occurred.", "error", !0)
                        }))
                    },
                    setPromotion: function(b, d) {
                        var e, f;
                        b.success ? (f = b.promotion || !1, e = f.discount || !1, e && (E.promotionDiscount = "number" == typeof e ? parseFloat(e) : 0), c.methods.updateModel(), c.methods.render(), a.adobeAnalytics.pages.join.promoApplied(d), a.analytics.methods.join.promoApplied(d)) : c.methods.resetPromotion()
                    },
                    resetPromotion: function() {
                        s.attr("placeholder", ""), t.find(".promo-msg").addClass("hidden"), t.find(".promo-apply span").addClass("hidden"), t.find(".promo-apply .default").removeClass("hidden"), $("#join-product-charges .special").addClass("hidden"), $("#" + z).removeClass("promo-applied"), w && a.validation.removeCustomMessage(w), c.methods.resetOnceOffFees(), c.methods.updatePromoView("", ".default")
                    },
                    clearPromoFields: function() {
                        E.promotion = null, E.promotionId = "", E.promotionStaffId = "", w && a.validation.removeCustomMessage(w), $(".promo-input-wrapper .error").removeClass("error")
                    },
                    updatePromoView: function(a, b) {
                        t.find(".promo-msg").addClass("hidden"), a && t.find(".promo-msg" + a).removeClass("hidden"), t.find(".promo-apply span").addClass("hidden"), b && t.find(".promo-apply " + b).removeClass("hidden")
                    },
                    populateStaffPromoTandC: function(a) {
                        $.ajax("/apps/belong/promotion/termsAndConditions." + a + ".html", {
                            dataType: "text"
                        }).then(function(a) {
                            $(".join-staff-promo-tc-content").html(a)
                        })
                    },
                    displayStaffPromoModal: function(a, b) {
                        F = !1, $.magnificPopup.open({
                            items: {
                                src: "#join-staff-promo-tc",
                                type: "inline"
                            },
                            showCloseBtn: !0,
                            closeOnBgClick: !1,
                            enableEscapeKey: !1,
                            overflowY: "hidden"
                        }), $("#btn-staff-promo-tc").off("click").on("click", {
                            result: a,
                            code: b
                        }, c.events.staffPromoClose), $.magnificPopup.instance.close = function() {
                            F || (c.methods.resetPromotion(), c.methods.clearPromoFields()), $.magnificPopup.proto.close.call(this)
                        }
                    },
                    validateStaffPromoPopup: function() {
                        G.forEach(function(b) {
                            a.validation.removeCustomMessage(b)
                        });
                        var b = !0;
                        if (!$("#staff-id").val()) {
                            var c = a.responseMessage.getValidationMessage(A, "product", "staff-promo-staff-id-input-required");
                            G.push(a.validation.attachCustomMessage("staff-id", c, "error", !0)), b = !1
                        }
                        if (!$("#chk-staff-promo-tc").prop("checked")) {
                            var c = a.responseMessage.getValidationMessage(A, "product", "staff-promo-tc-input-required");
                            G.push(a.validation.attachCustomMessage("label-staff-promo-tc", c, "error", !0)), b = !1
                        }
                        return b
                    },
                    resetOnceOffFees: function() {
                        E.activation_fee = B.charges.activation_fee, E.modem_fee = B.charges.modem_fee
                    },
                    setEdit: function(a) {
                        $("#" + z).addClass("edit").removeClass("readonly").removeClass("collapsed"), a ? $(a).velocity("stop").velocity("scroll", {
                            duration: 400,
                            easing: "ease-out",
                            offset: -70
                        }) : $("#" + z).velocity("stop").velocity("scroll", {
                            duration: 400,
                            easing: "ease-out",
                            offset: -70
                        }), $("#" + z + " input:not(.manual-enable)").removeAttr("disabled"), c.methods.render()
                    },
                    setReadOnly: function() {
                        $("#" + z).addClass("readonly").removeClass("edit").removeClass("collapsed"), $("#" + z + " input").attr("disabled", "disabled")
                    },
                    setCollapsed: function() {
                        $("#" + z).addClass("collapsed").removeClass("edit").removeClass("readonly")
                    },
                    resetForm: function() {
                        a.join.model.product.planType = "", a.join.model.product.planSize = "", a.join.model.product.contractTerm = "", a.join.model.product.promotionId = a.join.model.sq.promotionId, a.join.model.product.promotionStaffId = "", a.join.model.product.planSpeed = "", a.join.model.product.voicePlan = "", E = c.methods.getDefaultModel(), c.methods.updateModel(), c.methods.render(), a.join.common.updateAccessType()
                    }
                },
                display: {
                    promoApplyButton: function() {
                        $(".promo-apply").removeClass("hidden")
                    }
                },
                hide: {
                    promoApplyButton: function() {
                        $(".promo-apply").addClass("hidden")
                    }
                },
                events: {
                    onContinue: function(b) {
                        if (b.preventDefault(), console.log("onContinue clicked"), a.validation.validateForm(x)) {
                            c.methods.hasPromoBeenApplied() && c.methods.isPromoAppliedSuccessful() ? c.methods.isCurrentPromoForContractAndNonContract() || (c.methods.isCurrentPromoContract() && !E.withContract || c.methods.isCurrentPromoNonContract() && E.withContract) && c.methods.clearPromoFields() : c.methods.clearPromoFields(), $("#join-products .join-section-footer").removeClass("error"), c.methods.setReadOnly(), a.join.model.product = {
                                planType: E.planType,
                                speedboost: E.speedboost ? E.speedboost : !1,
                                planSize: E.planSize,
                                planSpeed: E.planSpeed,
                                contractTerm: a.join.common.getDefaultContractTerm(E.withContract),
                                promotionId: E.promotionId,
                                promotionStaffId: E.promotionStaffId,
                                voicePlan: E.voicePlan
                            }, a.adobeAnalytics.pages.join.steps("1-Product Cont.", "1-product-cont.html", E.plan_id), a.analytics.methods.join.joinSteps("1-Product Cont.", "1-product-cont.html", E.plan_id);
                            var d = E.planType,
                                e = E.planSize,
                                f = E.total_minimum_cost,
                                g = E.withContract;
                            a.livePerson.join.productContinue(d, e, f, g), a.join.methods.activateNextModule(z), a.join.payconfirm.methods.sqUpdateHandler()
                        } else $("#product-selection-input-error").length > 0 && ($("#join-products .join-section-footer").addClass("error"), $("#product-section-footer").append($("#product-selection-input-error")), a.adobeAnalytics.pages.join.errorProductSelection($("#product-selection-input-error").text()), a.analytics.methods.joinErrors.errorProductSelection($("#product-selection-input-error").text()));
                        return !1
                    },
                    updateModelHandler: function() {
                        console.log("in updateModel event"), c.methods.updateModel(), c.methods.render()
                    },
                    updateProductSelectionHandler: function(b, d, e, f) {
                        d && e && f && (E.plan_id = d + "-" + e + "-" + f, a.adobeAnalytics.pages.join.productSelection(E.plan_id), a.analytics.methods.join.productSelection(E.plan_id)), c.methods.updateModel(), c.methods.render(), a.join.common.updateAccessType(e), E.plan_id && $("#" + z + " .join-section-footer").removeClass("error")
                    },
                    contractHandler: function() {
                        E.withContract = g.prop("checked") ? !0 : !1, E.withContract && (a.adobeAnalytics.pages.join.checkContract(), a.analytics.methods.join.checkContract()), a.join.common.switchContractOption(E.withContract), c.methods.updateModel(), c.methods.render()
                    },
                    belongVoiceHandler: function() {
                        "NATIONAL" === $(this).val() && (E.voice_national = $(this).prop("checked")), "INTERNATIONAL" === $(this).val() && (E.voice_international = $(this).prop("checked")), c.methods.updateModel(), c.methods.render()
                    },
                    voiceTooltipHandler: function() {
                        return !1
                    },
                    speedboostHandler: function() {
                        switch ("1" === $(this).val() && "25/5" === E.planSpeed ? (E.planSpeed = "", E.speedboost = !1, $(this).prop("checked", "")) : "2" === $(this).val() && "100/40" === E.planSpeed ? (E.planSpeed = "", E.speedboost = !1, $(this).prop("checked", "")) : ($('input[name="chk-join-speedboost"].checkbox:checked:visible').length > 0 ? "1" === $('input[name="chk-join-speedboost"].checkbox:checked:visible').val() ? E.planSpeed = "25/5" : "2" === $('input[name="chk-join-speedboost"].checkbox:checked:visible').val() && (E.planSpeed = "100/40") : "1" === $('input[name="chk-join-speedboost"].radio:checked:visible').val() ? E.planSpeed = "25/5" : "2" === $('input[name="chk-join-speedboost"].radio:checked:visible').val() && (E.planSpeed = "100/40"), E.speedboost = !0), E.planSpeed) {
                            case "25/5":
                                a.adobeAnalytics.pages.join.addSpeedBoost25(), a.analytics.methods.join.addSpeedBoost25();
                                break;
                            case "100/40":
                                a.adobeAnalytics.pages.join.addSpeedBoost100(), a.analytics.methods.join.addSpeedBoost100()
                        }
                        c.methods.updateModel(), c.methods.render()
                    },
                    applyPromotionHandler: function(a) {
                        return a.preventDefault(), E.promotionId = $("#txtpromocode").val(), E.promotion = null, E.changingPromo = !1, c.methods.updateModel(), c.methods.render(), !1
                    },
                    changePromoHandler: function() {
                        if ($("#txtpromocode").val() != E.promotionId) {
                            E.changingPromo = !0;
                            var a = $("#txtpromocode").val();
                            E.promotion = null, c.methods.resetPromotion(), E.promotionId = a, c.methods.updateModel(), c.methods.render()
                        }
                    },
                    staffPromoClose: function(b) {
                        b.preventDefault(), a.join.product.methods.validateStaffPromoPopup() && (E.promotionStaffId = $("#staff-id").val(), c.methods.setPromotion(b.data.result, b.data.code), a.adobeAnalytics.pages.join.promoAppliedStaffDiscount(E.promotionId), a.analytics.methods.join.promoAppliedStaffDiscount(E.promotionId), F = !0, $.magnificPopup.close())
                    }
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.join = a.join || {}, a.join.sectionFooter = function() {
            var a;
            return {
                init: function() {
                    a = this
                },
                methods: {
                    setEdit: function() {},
                    setReadOnly: function() {}
                },
                events: {}
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.join = a.join || {}, a.join.sectionHeader = function() {
            var b;
            return {
                init: function() {
                    b = this
                },
                methods: {},
                events: {
                    onChangeClick: function() {
                        for (var b = ($(".join-submodule.edit").attr("id"), $(this).closest(".join-submodule")), c = b.attr("id"), d = a.join.methods.getNextModules(c), e = 0; e < d.length; e++) a.join.methods.collapseModule(d[e]);
                        return a.join.methods.activateModule(c), !1
                    }
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.join = a.join || {}, a.join.sq = function() {
            var b, c, d, e, f = "join-sq";
            return {
                init: function() {
                    b = this, c = $("#join-sq"), d = $("#join-sq-street-address-readonly"), e = $("#join-sq-landline-readonly"), $(".street-address-entry legend").removeClass("hidden")
                },
                methods: {
                    setEdit: function() {
                        a.sqEntry.display.reset(!0), c.addClass("edit").removeClass("readonly").removeClass("collapsed")
                    },
                    setReadOnly: function() {
                        c.removeClass("edit"), c.removeClass("collapsed"), c.addClass("readonly")
                    },
                    setCollapsed: function() {
                        c.removeClass("edit"), c.removeClass("readonly"), c.addClass("collapsed")
                    },
                    populateReadOnlyFields: function() {
                        var b = a.join.model,
                            c = "" !== b.sq.phoneNumber ? b.sq.phoneNumber : "Not applicable";
                        if (d.text(b.helpers.getAddressString()), "Not applicable" != c) {
                            var f = c.substring(0, 2);
                            f = f + " " + c.substr(2, 4), f = f + " " + c.substr(6, 4), c = f
                        }
                        e.text(c)
                    }
                },
                events: {
                    clickContinue: function(c) {
                        c.preventDefault();
                        var d = a.util.loadPageVar("planType"),
                            e = a.util.loadPageVar("planSize");
                        a.livePerson.join.sqContinue(d, e), b.methods.populateReadOnlyFields(), a.adobeAnalytics.pages.join.steps("0-SQ Cont.", "0-sq-cont.html", ""), a.analytics.methods.join.joinSteps("0-SQ Cont.", "0-sq-cont.html"), a.join.methods.handleSqUpdate(), b.methods.setReadOnly(), a.join.methods.activateNextModule(f)
                    }
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.join = a.join || {}, $.extend(a.join, function() {
            var b, c = !1,
                d = {
                    sq: "join-sq",
                    products: "join-products",
                    personal: "join-personal",
                    payconfirm: "join-payconfirm",
                    productCharges: "join-product-charges",
                    personalCharges: "join-personal-charges"
                },
                e = [d.sq, d.products, d.personal, d.payconfirm],
                f = {
                    TWELVE_MONTHS: "TWELVE_MONTHS",
                    SIX_MONTHS: "SIX_MONTHS"
                };
            return {
                init: function() {
                    b = this, $("#join-belong").each(function() {
                        b.methods.fromProductBuilder() ? (console.log("** Join coming from Product Builder"), $("#join-personal .join-section-header .step").html("1"), $("#join-payconfirm .join-section-header .step").html("2")) : (console.log("** Join initiated in self"), $("#join-sq").removeClass("hidden").addClass("edit"), $(".join-product-details").removeClass("hidden")), a.join.sq.init(), a.join.product.init(), a.join.personal.init(), a.join.payconfirm.init(), b.methods.collapseModule(d.sq), 0 == b.methods.fromProductBuilder() && b.methods.collapseModule(d.products), b.methods.collapseModule(d.personal), b.methods.collapseModule(d.payconfirm);
                        var e = sessionStorage.getItem("productModel"),
                            f = {};
                        e && (f = JSON.parse(e)), a.util.isPrivateBrowsingMode() || "true" === sessionStorage.getItem("sqDone") && $.get("/bin/belong/join/retrieve").then(function(a) {
                            b.methods.handleExistingSqSession(a, f)
                        }, function(a) {
                            b.methods.handleExistingSqSession(a.responseJSON, f)
                        }), c = "true" === a.util.loadPageVar("promo"), $(".change-link").off("click.sectionHeader").on("click.sectionHeader", a.join.sectionHeader.events.onChangeClick)
                    })
                },
                methods: {
                    fromProductBuilder: function() {
                        return "true" === a.util.loadPageVar("productbuilder")
                    },
                    activateModule: function(b) {
                        switch (console.log("activateModule(): moduleID=" + b), b) {
                            case d.sq:
                                a.join.sq.methods.setEdit();
                                break;
                            case d.products:
                                a.join.product.methods.setEdit();
                                break;
                            case d.personal:
                                a.join.personal.methods.setEdit();
                                break;
                            case d.payconfirm:
                                a.join.payconfirm.methods.setEdit();
                                break;
                            case d.productCharges:
                                a.join.product.methods.setEdit();
                                break;
                            case d.personalCharges:
                                a.join.personal.methods.setEdit()
                        }
                    },
                    activateNextModule: function(a) {
                        var c = e[e.indexOf(a) + 1];
                        b.methods.activateModule(c)
                    },
                    collapseModule: function(b) {
                        switch (b) {
                            case d.products:
                                a.join.product.methods.setCollapsed();
                                break;
                            case d.personal:
                                a.join.personal.methods.setCollapsed();
                                break;
                            case d.payconfirm:
                                a.join.payconfirm.methods.setCollapsed()
                        }
                    },
                    setModuleToReadonly: function(b) {
                        switch (b) {
                            case d.sq:
                                a.join.sq.methods.setReadOnly();
                                break;
                            case d.products:
                                a.join.product.methods.setReadOnly();
                                break;
                            case d.personal:
                                a.join.personal.methods.setReadOnly();
                                break;
                            case d.payconfirm:
                                a.join.payconfirm.methods.setReadOnly()
                        }
                    },
                    isPrevious: function(a, c) {
                        return b.methods.getModuleOrderNumber(a) < b.methods.getModuleOrderNumber(c)
                    },
                    getModuleOrderNumber: function(a) {
                        for (var b = 0; b < e.length; b++)
                            if (a === e[b]) return b;
                        return -1
                    },
                    getNextModules: function(a) {
                        return e.slice(b.methods.getModuleOrderNumber(a) + 1)
                    },
                    handleApiErrors: function(a) {
                        $(".top").append('<div class="rest-error error-banner">' + a + "</div>")
                    },
                    handleExistingSqSession: function(e, f) {
                        if (e.success) {
                            var g = f.planType || "",
                                h = f.planSize || "";
                            a.livePerson.join.pageEntry(g, h), a.join.model.sq.phoneNumber = e.homePhone, a.join.model.sq.serviceStreetNumber = e.serviceStreetNumber, a.join.model.sq.serviceStreet = e.serviceStreet, a.join.model.sq.serviceStreetType = e.serviceStreetType, a.join.model.sq.serviceExchange = e.serviceExchange, a.join.model.sq.serviceSubAddressDetails = e.serviceSubAddressDetails, a.join.model.sq.serviceSuburb = e.serviceSuburb, a.join.model.sq.serviceState = e.serviceState, a.join.model.sq.servicePostcode = e.servicePostcode, a.join.model.sq.serviceAddressId = e.serviceAddressId, a.join.model.sq.isNbn = "NBN" === e.sqType ? !0 : !1, a.join.model.sq.orderType = "undefined" != typeof e.serviceClass && null !== e.serviceClass && "" !== e.serviceClass ? e.serviceClass : e.orderType, a.join.model.sq.broadbandTechType = e.broadbandTechType, a.join.model.sq.isPreprovisioned = e.preProvisioned, a.join.model.sq.extendedNetwork = e.extendedNetwork, a.join.model.sq.promotionId = a.join.model.sq.isPreprovisioned ? e.promotionId : "", console.log("join handleExistingSqSession - response.promotionId" + e.promotionId), console.log("join handleExistingSqSession - EVE.join.model.sq.promotionId: " + a.join.model.sq.promotionId), a.join.model.product.planType = f.planType ? f.planType : e.planType, a.join.model.product.planSize = f.planSize ? f.planSize : e.planSize, a.join.model.product.planSpeed = f.planSpeed ? f.planSpeed : e.planSpeed, a.join.model.product.promotionId = f.promotionId ? f.promotionId : e.promotionId, a.join.model.product.campaignCode = f.campaignCode ? f.campaignCode : "", a.join.model.product.promotionStaffId = f.promotionStaffId ? f.promotionStaffId : e.promotionStaffId, a.join.model.product.staffPromoSavedInSession = a.join.model.product.promotionStaffId ? !0 : !1, a.join.model.product.contractTerm = f.contractTerm ? f.contractTerm : e.contractTerm, a.join.model.product.calculatedMTC = f.calculatedMTC, a.join.model.product.voicePlan = f.voicePlan ? f.voicePlan : e.voicePlan, a.join.model.personalDetails.firstName = e.firstName, a.join.model.personalDetails.lastName = e.lastName, a.join.model.personalDetails.email = e.email, a.join.model.personalDetails.password = e.password, a.join.model.personalDetails.dateOfBirth = e.dateOfBirth, a.join.model.personalDetails.mobilePhone = e.mobilePhone, a.join.model.personalDetails.portCurrentAccountNumber = e.currentAccountNumber, a.join.model.personalDetails.portCurrentProvider = e.currentProvider, a.join.model.personalDetails.portNameOnBill = e.nameOnCurrentBill, a.join.sq.methods.populateReadOnlyFields(), a.join.product.methods.prefill(), a.join.personal.methods.prefill(), b.methods.fromProductBuilder() ? (console.log("handleExistingSqSession(): From Product Builder"), $(".join-product-summary").removeClass("hidden"), $("#join-products").removeClass("collapsed").addClass("readonly"), b.methods.activateModule(d.personalCharges)) : (console.log("handleExistingSqSession(): Original Join Form"), b.methods.setModuleToReadonly(d.sq), c && !$.isEmptyObject(f) ? b.methods.activateModule(d.productCharges) : c || $.isEmptyObject(f) ? b.methods.activateModule(d.products) : (b.methods.setModuleToReadonly(d.products), b.methods.activateModule(d.personalCharges)))
                        } else if ("undefined" != typeof e.messages[0].userMessage) {
                            var i = e.messages[0].userMessage;
                            console.log("Error returned from retrieveExistingOrder: " + i);
                            var j = a.responseMessage.getApiMessage("join-belong", "sqSession", i);
                            "" !== j && b.methods.handleApiErrors(j)
                        }
                    },
                    handleSqUpdate: function() {
                        a.join.product.methods.sqUpdateHandler(), a.join.personal.methods.sqUpdateHandler(), a.join.payconfirm.methods.sqUpdateHandler()
                    }
                },
                common: {
                    getDefaultContractTerm: function(a) {
                        return "undefined" !== a ? a ? f.TWELVE_MONTHS : null : f.TWELVE_MONTHS
                    },
                    getAccessTypeFromOrderType: function(b) {
                        if ("CHURN_ORDER" === a.join.model.sq.orderType) {
                            if ("DSL" === b) return "ADSL2_BROADBAND";
                            if ("BUNDLE" === b) return "ADSL2_BUNDLE";
                            console.log("orderType is CHURN_ORDER but planType unknown"), console.log("EVE.join.model.product.planType: " + b)
                        }
                        var c = {
                            NBN_SERVICE_CLASS_ONE: "NBNSC1_2",
                            NBN_SERVICE_CLASS_TWO: "NBNSC1_2",
                            NBN_SERVICE_CLASS_THREE: "NBNSC3",
                            NBN_SERVICE_CLASS_ELEVEN: "NBNSC11",
                            NBN_SERVICE_CLASS_TWELVE: "NBNSC12",
                            NBN_SERVICE_CLASS_THIRTEEN: "NBNSC13",
                            NEW_ORDER: "ADSL2_BUNDLE",
                            PORT_ORDER: "ADSL2_BUNDLE",
                            CHURN_ORDER: "ADSL2_BROADBAND"
                        };
                        return c.hasOwnProperty(a.join.model.sq.orderType) ? c[a.join.model.sq.orderType] : ""
                    },
                    switchContractOption: function(a) {
                        a ? ($(".contract").removeClass("hidden"), $(".non-contract").addClass("hidden")) : ($(".contract").addClass("hidden"), $(".non-contract").removeClass("hidden"))
                    },
                    updateAccessType: function(c) {
                        if ($(".join-product-access-types").addClass("hidden"), $(".access-type").addClass("hidden"), a.join.common.getAccessTypeFromOrderType()) {
                            var d = b.common.getAccessTypeFromOrderType(c).toLowerCase();
                            $("." + d).each(function() {
                                if ("" !== $.trim($(this).text())) {
                                    $(this).removeClass("hidden");
                                    var a = $(this).parent();
                                    a.hasClass("join-product-access-types") && a.removeClass("hidden")
                                } else $(this).addClass("hidden")
                            }), a.join.model.sq.isPreprovisioned && "" !== a.join.model.sq.promotionId && null !== a.join.model.sq.promotionId && ($("#atype-NBNSC3_pre_provisioning").removeClass("hidden"), $("#atype-NBNSC3").addClass("hidden"))
                        }
                    }
                }
            }
        }())
    }(BELONG),
    function(a) {
        "use strict";
        a.managePayment = a.managePayment || {}, a.managePayment.invoiceCardVM = function() {
            var b = this;
            a.koComponents.tileButtonSlider.init(), b.util = a.util, b.isLoading = ko.observable(!0), b.serverError = ko.observableArray([]), b.downloadUrl = "", b.statementData = ko.observable(), b.emptyStatements = ko.observable(!0), b.monthSliderRef = null, b.init = function() {
                b.retrieveStatements()
            }, b.retry = function() {
                b.isLoading(!0), b.serverError([]), b.retrieveStatements()
            }, b.retrieveStatements = function() {
                var a = $.get("/bin/belong/auth/customer/payment/statements");
                a.done(function(a) {
                    b.statementData(a), b.initMonthSlider(a)
                }), a.fail(function(a) {
                    console.log("retrieveStatements AJAX failure."), console.log(a);
                    var c = a.responseJSON;
                    "timeout" === a.statusText ? (console.log("Server Error: timeout"), b.serverError([{
                        code: "ERROR_TIMEOUT"
                    }])) : b.serverError(void 0 !== c && void 0 !== c.errors ? c.errors : [{
                        code: "ERROR_DEFAULT"
                    }])
                }), a.always(function() {
                    b.isLoading(!1)
                })
            }, b.downloadClicked = function() {
                "" !== b.downloadUrl && window.open(b.downloadUrl, "_blank")
            }, b.monthSliderScopeCallback = function(a) {
                b.monthSliderRef = a
            }, b.sliderClicked = function(a) {
                b.downloadUrl = a.downloadUrl
            }, b.initMonthSlider = function(a) {
                var c = [];
                $.each(a.allStatements, function(a, b) {
                    var d = moment(b.dateIssued);
                    c.push({
                        primaryLabel: d.format("MMM").toUpperCase(),
                        secondaryLabel: d.format("YYYY"),
                        downloadUrl: b.downloadPDFUrl
                    })
                }), c.length && (b.emptyStatements(!1), b.monthSliderRef.init(c), b.monthSliderRef.applyFlexSlider())
            }
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.managePayment = a.managePayment || {}, a.managePayment.makePaymentVM = function() {
            var b = this;
            b.states = {
                success: ko.observable(""),
                accountStatus: ko.observable(""),
                currentBalance: ko.observable(""),
                directDebitMethod: ko.observable(""),
                dueDate: ko.observable(""),
                paymentType: ko.observable(""),
                paypalCustomerId: ko.observable(""),
                creditCardLast4: ko.observable(""),
                creditCardType: ko.observable(""),
                nextPaymentDate: ko.observable(""),
                amountPaid: ko.observable(""),
                amountToPrepay: ko.observable(""),
                amountPrepaid: ko.observable(""),
                errorCode: ko.observable(""),
                serverError: ko.observableArray([]),
                processPaymentError: ko.observableArray([])
            }, b.PAYMENT_CARD_VIEWS = a.managePayment.paymentCardViews, b.PAYMENT_TYPES = a.managePayment.paymentTypes, b.PAYMENT_CARD_TYPES = a.managePayment.paymentCardTypes, b.util = a.util, b.currentView = ko.observable(b.PAYMENT_CARD_VIEWS.MAKE_PAYMENT).syncWith("currentView"), b.customAnimation = ko.observable(!1), b.currentView.subscribe(function(a) {
                a === b.PAYMENT_CARD_VIEWS.MAKE_PAYMENT ? b.init() : b.customAnimation(!0)
            }), b.paymentProcessing = ko.observable(!1), b.isLoading = ko.observable(!0), b.paidTempState = ko.observable(!1), b.prepayView = ko.observable(!1), b.prepayState = ko.observable("DEFAULT"), b.defRetrieveErrorMsg = ko.observable(b.util.getCMSMessage("ERROR_DEFAULT", CMS_PAYMENT_CARD.paymentRetrieveServerErrors)), b.defPayProcessErrorMsg = ko.observable(b.util.getCMSMessage("ERROR_DEFAULT", CMS_PAYMENT_CARD.paymentProcessErrors)), b.states.paidMonth = ko.computed(function() {
                return b.states.dueDate() ? moment(b.states.dueDate()).format("MMMM") : b.states.nextPaymentDate() ? moment(b.states.nextPaymentDate()).subtract(1, "month").format("MMMM") : void 0
            }), b.states.prepaidMonth = ko.computed(function() {
                return b.states.nextPaymentDate() ? moment(b.states.nextPaymentDate()).format("MMMM") : void 0
            }), b.states.creditAmount = ko.computed(function() {
                return b.states.currentBalance() < 0 ? -b.states.currentBalance() : 0
            }), b.payMethodHeading = ko.computed(function() {
                var a = "";
                return a = b.states.errorCode() || b.states.processPaymentError().length ? "Payment Unsuccessful:" : "Payment method:", "BT" !== b.states.directDebitMethod() && (a = b.states.currentBalance() > 0 ? "Payment Unsuccessful" : ""), a
            }), b.states.currentPayMethod = ko.computed(function() {
                var a = b.states.paymentType(),
                    c = "",
                    d = "";
                return "BT" !== b.states.directDebitMethod() ? "" : a === b.PAYMENT_TYPES.PAYPAL ? (c = "paypal", d = b.states.paypalCustomerId(), '<span class="svg-' + c + ' payment-method-icon">PayPal</span><span>(' + d + ")</span>") : a === b.PAYMENT_TYPES.CREDITCARD ? ("Visa" === b.states.creditCardType() ? c = "visa" : "MasterCard" === b.states.creditCardType() && (c = "master-card"), d = "xxxx xxxx xxxx " + b.states.creditCardLast4(), '<span class="svg-' + c + '-card payment-method-icon">' + b.states.creditCardType() + "</span><span>(" + d + ")</span>") : void 0
            }), b.init = function() {
                b.retrieveData()
            }, b.donePayment = function() {
                b.paidTempState(!1), b.retrieveData()
            }, b.donePrepayPayment = function() {
                b.prepayView(!1), b.retrieveData()
            }, b.cancelPrepayPayment = function() {
                b.prepayView(!1)
            }, b.retrieveData = function() {
                b.isLoading(!0);
                var a = $.get("/bin/belong/auth/customer/payment/current");
                a.done(function(a) {
                    console.log("managePaymentRetrieve successful."), b.states.success(a.success), b.states.accountStatus(a.accountStatus), b.states.currentBalance(a.currentBalance), b.states.dueDate(a.dueDate), b.states.paymentType(a.paymentType), b.states.paypalCustomerId(a.paypalCustomerId), b.states.creditCardLast4(a.creditCardLast4), b.states.creditCardType(a.creditCardType), b.states.nextPaymentDate(a.nextPaymentDate), b.states.amountPaid(a.amountPaid), b.states.amountToPrepay(a.amountToPrepay), b.states.amountPrepaid(a.amountPrepaid), b.states.errorCode(a.errorCode), b.states.directDebitMethod(a.directDebitMethod), b.states.serverError([])
                }), a.fail(function(a) {
                    console.log("managePaymentRetrieve AJAX failure."), console.log(a);
                    var c = a.responseJSON;
                    "timeout" === a.statusText ? (console.log("Server Error: timeout"), b.states.serverError([{
                        code: "ERROR_TIMEOUT"
                    }])) : b.states.serverError(void 0 !== c && void 0 !== c.errors ? c.errors : [{
                        code: "ERROR_DEFAULT"
                    }])
                }), a.always(function() {
                    b.isLoading(!1)
                })
            }, b.processPaymentDone = function() {
                console.log("processPayment Done"), b.states.amountPaid(b.states.currentBalance()), b.states.currentBalance(0), b.paymentProcessing(!1), b.states.processPaymentError([]), b.paidTempState(!0), b.isLoading(!1), a.adobeAnalytics.pages.managePayment.retryPayment(!0, !1)
            }, b.processPaymentFail = function(c) {
                console.log("processPayment Fail");
                var d = c.responseJSON;
                "timeout" === c.statusText ? (console.log("Server Error: timeout"), b.states.processPaymentError([{
                    code: "ERROR_TIMEOUT"
                }])) : (console.log("error:", d), b.states.processPaymentError(void 0 !== d && void 0 !== d.errors ? d.errors : [{
                    code: "ERROR_DEFAULT"
                }]));
                var e = b.states.processPaymentError()[0].code;
                console.log("errorCode", e), a.adobeAnalytics.pages.managePayment.retryPayment(!1, e), b.paymentProcessing(!1), b.isLoading(!1)
            }, b.processPayment = function() {
                console.log("processPayment"), b.paymentProcessing(!0);
                var a = {},
                    c = $.post("/bin/belong/auth/customer/payment/current", a);
                return c.then(b.processPaymentDone, b.processPaymentFail), !1
            }, b.processPaymentRetry = function() {
                console.log("processPayment retry"), b.isLoading(!0);
                var a = {},
                    c = $.post("/bin/belong/auth/customer/payment/current", a);
                return c.then(b.processPaymentDone, b.processPaymentFail), !1
            }, b.processPrePayDone = function() {
                console.log("processPrePay Done"), b.paymentProcessing(!1), b.prepayState("PAIDTEMP"), b.isLoading(!1), a.adobeAnalytics.pages.managePayment.prepaid(!0, !1)
            }, b.processPrePayFail = function(c) {
                console.log("processPrePay Fail");
                var d = c.responseJSON;
                "timeout" === c.statusText ? (console.log("Server Error: timeout"), b.states.processPaymentError([{
                    code: "ERROR_TIMEOUT"
                }])) : (console.log("error:", d), b.states.processPaymentError(void 0 !== d && void 0 !== d.errors ? d.errors : [{
                    code: "ERROR_DEFAULT"
                }]));
                var e = b.states.processPaymentError()[0].code;
                console.log("errorCode", e), a.adobeAnalytics.pages.managePayment.prepaid(!1, e), b.paymentProcessing(!1), b.isLoading(!1)
            }, b.processPrepayRetry = function() {
                console.log("processPrepay retry"), b.isLoading(!0);
                var a = {},
                    c = $.post("/bin/belong/auth/customer/payment/next", a);
                return c.then(b.processPrePayDone, b.processPrePayFail), !1
            }, b.processPrepay = function() {
                b.isLoading(!0);
                var a = {},
                    c = $.post("/bin/belong/auth/customer/payment/next", a);
                c.then(b.processPrePayDone, b.processPrePayFail)
            }, b.selectPayMethod = function() {
                console.log("selectPayMethod"), b.currentView(b.PAYMENT_CARD_VIEWS.SELECT_PAYMETHOD)
            }, b.modifyPayMethod = function() {
                console.log("modifyPayMethod"), b.currentView(b.PAYMENT_CARD_VIEWS.MODIFY_PAYMETHOD)
            }, b.prePayNextMonth = function() {
                b.prepayView(!0), b.prepayState("DEFAULT")
            }
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.managePayment = a.managePayment || {}, $.extend(a.managePayment, function() {
            var b;
            return {
                init: function() {
                    if (b = this, 0 === $("#cmp-manage-payment").length) return !1;
                    var c = new a.managePayment.paymentCardVM,
                        d = new a.managePayment.invoiceCardVM;
                    ko.applyBindings(c, document.getElementById("ctn-payment-card")), ko.applyBindings(d, document.getElementById("ctn-invoice-card")), c.init(), d.init(), setTimeout(function() {
                        a.adobeAnalytics.pages.managePayment.display()
                    }, 400)
                },
                paymentCardViews: {
                    MAKE_PAYMENT: "make_payment",
                    MODIFY_PAYMETHOD: "modify_paymethod",
                    SELECT_PAYMETHOD: "select_paymethod"
                },
                paymentCardTypes: {
                    VISA: "visa",
                    MASTERCARD: "master-card"
                },
                paymentTypes: {
                    PAYPAL: "paypal",
                    CREDITCARD: "credit_card"
                },
                modifyPayMethodRoutes: {
                    DEFAULT: "DEFAULT",
                    UPDATE: "UPDATE",
                    SUCCESS: "SUCCESS"
                }
            }
        }())
    }(BELONG),
    function(a) {
        "use strict";
        a.managePayment = a.managePayment || {}, a.managePayment.modifyPayMethodVM = function() {
            var b = this,
                c = "edit-form-creditcard",
                d = "paypal-container";
            b.PAYMENT_CARD_VIEWS = a.managePayment.paymentCardViews, b.PAYMENT_TYPES = a.managePayment.paymentTypes, b.util = a.util, b.currentView = ko.observable().syncWith("currentView", !0), b.isLoading = ko.observable(!0), b.customAnimation = ko.observable(!0), b.clientToken = ko.observable("eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiIyZDc5OTUzYWRiYzQ4MmExMmM5MTQ5YmFkZGJkYjBmMTcxZGY4YzlmNzk3ODc0NDBlMDc5YTJkZjE2NThhOTcxfGNyZWF0ZWRfYXQ9MjAxNS0xMC0yNlQwNDo1OTozOS42OTY2MTA4MzgrMDAwMFx1MDAyNmN1c3RvbWVyX2lkPTIyNTU3MzQ4XHUwMDI2bWVyY2hhbnRfaWQ9cWc4eXlxdG5wcDZtc2s3eVx1MDAyNnB1YmxpY19rZXk9a2Z4NXhwNHJmaGNza2ozNSIsImNvbmZpZ1VybCI6Imh0dHBzOi8vYXBpLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb206NDQzL21lcmNoYW50cy9xZzh5eXF0bnBwNm1zazd5L2NsaWVudF9hcGkvdjEvY29uZmlndXJhdGlvbiIsImNoYWxsZW5nZXMiOlsiY3Z2Il0sImVudmlyb25tZW50Ijoic2FuZGJveCIsImNsaWVudEFwaVVybCI6Imh0dHBzOi8vYXBpLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb206NDQzL21lcmNoYW50cy9xZzh5eXF0bnBwNm1zazd5L2NsaWVudF9hcGkiLCJhc3NldHNVcmwiOiJodHRwczovL2Fzc2V0cy5icmFpbnRyZWVnYXRld2F5LmNvbSIsImF1dGhVcmwiOiJodHRwczovL2F1dGgudmVubW8uc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbSIsImFuYWx5dGljcyI6eyJ1cmwiOiJodHRwczovL2NsaWVudC1hbmFseXRpY3Muc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbSJ9LCJ0aHJlZURTZWN1cmVFbmFibGVkIjpmYWxzZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiT2JqZWN0IENvbnN1bHRpbmciLCJjbGllbnRJZCI6bnVsbCwicHJpdmFjeVVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS9wcCIsInVzZXJBZ3JlZW1lbnRVcmwiOiJodHRwOi8vZXhhbXBsZS5jb20vdG9zIiwiYmFzZVVybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXNzZXRzVXJsIjoiaHR0cHM6Ly9jaGVja291dC5wYXlwYWwuY29tIiwiZGlyZWN0QmFzZVVybCI6bnVsbCwiYWxsb3dIdHRwIjp0cnVlLCJlbnZpcm9ubWVudE5vTmV0d29yayI6dHJ1ZSwiZW52aXJvbm1lbnQiOiJvZmZsaW5lIiwidW52ZXR0ZWRNZXJjaGFudCI6ZmFsc2UsImJyYWludHJlZUNsaWVudElkIjoibWFzdGVyY2xpZW50MyIsImJpbGxpbmdBZ3JlZW1lbnRzRW5hYmxlZCI6ZmFsc2UsIm1lcmNoYW50QWNjb3VudElkIjoiQmVsb25nQVVEIiwiY3VycmVuY3lJc29Db2RlIjoiQVVEIn0sImNvaW5iYXNlRW5hYmxlZCI6ZmFsc2UsIm1lcmNoYW50SWQiOiJxZzh5eXF0bnBwNm1zazd5IiwidmVubW8iOiJvZmYifQ"), b.updatePayMethodError = !1, b.invalidCardType = ko.observable(), b.invalidCardHolderName = ko.observable(), b.creditCardErrorList = ko.observableArray(), b.payPalError = ko.observable(""), b.showPayPalMsg = ko.observable(!1), b.cardHolderName = ko.observable(), b.isBraintreeReady = ko.observable(!1), b.nonce = ko.observable(""), b.defClientTokenErrorMsg = ko.observable(b.util.getCMSMessage("ERROR_DEFAULT", CMS_PAYMENT_CARD.clientTokenErrors)), b.defMethodSavErrorMsg = ko.observable(b.util.getCMSMessage("ERROR_DEFAULT", CMS_PAYMENT_CARD.methodSaveErrors)),
                b.currentView.subscribe(function(a) {
                    a === b.PAYMENT_CARD_VIEWS.MODIFY_PAYMETHOD && b.init()
                }), b.states = {
                    choosenPaymethod: ko.observable(""),
                    status: ko.observable("DEFAULT"),
                    serverError: ko.observableArray([]),
                    methodSaveError: ko.observableArray([]),
                    paymentType: ko.observable(""),
                    paypalCustomerId: ko.observable(""),
                    creditCardType: ko.observable(""),
                    creditCardLast4: ko.observable("")
                }, b.states.choosenPaymethod.subscribe(function(a) {
                    b.changePayment(a)
                }), b.init = function() {
                    b.retrieveData()
                }, b.retrieveData = function() {
                    b.isLoading(!0);
                    var a = $.get("/bin/belong/auth/customer/payment/token");
                    a.done(function(a) {
                        console.log("selectPayMethod get tocken successful."), b.clientToken(a.clientToken), b.states.serverError([]), b.states.status("DEFAULT"), b.states.choosenPaymethod("")
                    }), a.fail(function(a) {
                        console.log("selectPayMethod retrieveData AJAX failure."), console.log(a);
                        var c = a.responseJSON;
                        "timeout" === a.statusText ? (console.log("Server Error: timeout"), b.states.serverError([{
                            code: "ERROR_TIMEOUT"
                        }])) : b.states.serverError(void 0 !== c && void 0 !== c.errors ? c.errors : [{
                            code: "ERROR_DEFAULT"
                        }])
                    }), a.always(function() {
                        b.isLoading(!1)
                    })
                }, b.states.currentPayMethod = ko.computed(function() {
                    var a = b.states.paymentType(),
                        c = "",
                        d = "";
                    return a === b.PAYMENT_TYPES.PAYPAL ? (c = "paypal", d = b.states.paypalCustomerId()) : a === b.PAYMENT_TYPES.CREDITCARD && ("Visa" === b.states.creditCardType() ? c = "visa" : "MasterCard" === b.states.creditCardType() && (c = "master-card"), d = "xxxx xxxx xxxx " + b.states.creditCardLast4()), '<p><span class="svg-' + c + '-full payment-card-icon"></span></p><p>' + d + "</p>"
                }), b.onClickChangePayment = function() {
                    var b = !!$('input[name="payment_method_nonce"]').val();
                    if (b) {
                        var c = a.responseMessage.getValidationMessage("join-payment-form", "nonce", "alreadyhas");
                        return console.log("alreadyHasNonce:", c), $("#payment-paypal-errors").html(c), !1
                    }
                    return !0
                }, b.changePayment = function(a) {
                    b.isLoading(!0), b.nonce(""), a === b.PAYMENT_TYPES.PAYPAL ? b.braintreePayPal() : a === b.PAYMENT_TYPES.CREDITCARD ? b.braintreeCreditCard() : b.isLoading(!1)
                }, b.catchange = function(a, b) {
                    var c = $(".credit-card-icon");
                    a ? c.each(function() {
                        $(this).hasClass("svg-" + b + "-card") ? $(this).addClass("active") : $(this).removeClass("active")
                    }) : c.removeClass("active")
                }, b.addCreditCardError = function(a) {
                    var c = ko.utils.arrayFirst(b.creditCardErrorList(), function(b) {
                        return a.id === b.id
                    });
                    c ? b.creditCardErrorList.replace(c, a) : b.creditCardErrorList.push(a)
                }, b.removeCreditCardErrorById = function(a) {
                    b.creditCardErrorList.remove(function(b) {
                        return b.id === a ? !0 : void 0
                    })
                }, b.cardHolderNameValidation = function() {
                    var a = "";
                    if (void 0 === b.cardHolderName() || "" === b.cardHolderName() ? a = b.util.getCMSMessage("credit-card-name-required", CMS_PAYMENT_CARD.cardValidation) : /^[^<>+;\[\]\{\}\"]+$/i.test(b.cardHolderName()) || (a = b.util.getCMSMessage("credit-card-name-disallowedCharacters", CMS_PAYMENT_CARD.cardValidation)), "" !== a) {
                        b.invalidCardHolderName(!0);
                        var c = {
                            message: a,
                            id: "credit-card-name"
                        };
                        b.addCreditCardError(c)
                    } else b.invalidCardHolderName(!1), b.removeCreditCardErrorById("credit-card-name")
                }, b.formReady = function() {
                    b.clientToken() && a.forms.applyShiftLabel()
                }, b.braintreePayPal = function() {
                    braintree.setup(b.clientToken(), "paypal", {
                        container: d,
                        displayName: "Belong",
                        onPaymentMethodReceived: function(a) {
                            b.nonce(a.nonce), b.payPalError(""), b.showPayPalMsg(!1)
                        },
                        onReady: function() {
                            b.isLoading(!1), b.showPayPalMsg(!0)
                        },
                        onCancelled: function() {
                            b.payPalError(""), b.nonce(""), b.showPayPalMsg(!0)
                        },
                        onError: function(a) {
                            b.nonce(""), console.log(a)
                        }
                    })
                }, b.braintreeCreditCard = function() {
                    var a = {
                        "font-size": "16px",
                        "font-family": "source-sans-pro,HelveticaNeue-Light,Helvetica Neue Light,Helvetica Neue,Helvetica Neue Regular,Arial,Helvetica,sans-serif",
                        "font-weight": "300",
                        color: "#999"
                    };
                    console.log("CREDIT_FORM_ID:", c), braintree.setup(b.clientToken(), "custom", {
                        id: c,
                        hostedFields: {
                            number: {
                                selector: "#credit-card-number"
                            },
                            cvv: {
                                selector: "#credit-card-security-code",
                                placeholder: "CVV"
                            },
                            expirationMonth: {
                                selector: "#credit-card-month",
                                placeholder: "MM"
                            },
                            expirationYear: {
                                selector: "#credit-card-year",
                                placeholder: "YY"
                            },
                            styles: {
                                input: {
                                    "font-size": "16px",
                                    "font-family": "source-sans-pro,HelveticaNeue-Light,Helvetica Neue Light,Helvetica Neue,Helvetica Neue Regular,Arial,Helvetica,sans-serif",
                                    "font-weight": "300",
                                    color: "#666"
                                },
                                "::-webkit-input-placeholder": a,
                                ":-moz-placeholder": a,
                                "::-moz-placeholder": a,
                                ":-ms-inpput-placeholder": a,
                                ".number": {
                                    "font-weight": "300"
                                },
                                ".valid": {
                                    color: "#666"
                                },
                                ".invalid": {
                                    color: "#ff7070"
                                }
                            },
                            onFieldEvent: function(a) {
                                var c = $(a.target.container),
                                    d = c.closest(".ctrl-holder");
                                if (console.log(a), "number" === a.target.fieldKey && null !== a.card) {
                                    if ("visa" !== a.card.type && "master-card" !== a.card.type) {
                                        c.removeClass("braintree-hosted-fields-valid"), c.addClass("braintree-hosted-fields-invalid"), b.invalidCardType(!0), b.catchange(!1);
                                        var e = {
                                            message: b.util.getCMSMessage("credit-card-number-type", CMS_PAYMENT_CARD.cardValidation),
                                            id: "credit-card-number"
                                        };
                                        return b.addCreditCardError(e), !1
                                    }
                                    b.invalidCardType(!1), b.removeCreditCardErrorById("credit-card-number")
                                } else "number" === a.target.fieldKey && null === a.card && (b.invalidCardType(!1), b.removeCreditCardErrorById("credit-card-number"));
                                a.isPotentiallyValid && (null !== a.card ? b.catchange(!0, a.card.type) : b.catchange(!1)), "blur" === a.type ? d.removeClass("shift-focussed") : "focus" === a.type && d.addClass("shift-focussed"), "number" === a.target.fieldKey && ("blur" === a.type ? a.isEmpty && d.removeClass("shift-label") : "focus" === a.type && d.addClass("shift-label")), a.card && b.removeCreditCardErrorById("credit-card-braintree")
                            }
                        },
                        onPaymentMethodReceived: function(a) {
                            console.log(a), b.nonce(a.nonce), b.cardHolderNameValidation(), b.invalidCardType() ? b.isLoading(!1) : b.invalidCardHolderName() ? b.isLoading(!1) : (b.creditCardErrorList.removeAll(), b.savePaymentMethod())
                        },
                        onReady: function() {
                            b.isLoading(!1)
                        },
                        onError: function(a) {
                            var c = b.util.getCMSMessage(a.message, CMS_PAYMENT_CARD.braintreeValidation) ? b.util.getCMSMessage(a.message, CMS_PAYMENT_CARD.braintreeValidation) : a.message,
                                d = {
                                    message: c,
                                    id: "credit-card-braintree"
                                };
                            b.cardHolderNameValidation(), b.addCreditCardError(d), b.isLoading(!1)
                        }
                    })
                }, b.savePayPalOnClick = function() {
                    b.isLoading(!0), "" !== b.nonce() ? b.savePaymentMethod() : (b.isLoading(!1), b.payPalError(b.util.getCMSMessage("paypal-required", CMS_PAYMENT_CARD.paypalValidation)))
                }, b.saveCreditCardOnClick = function() {
                    b.isLoading(!0), b.states.methodSaveError([]), $("#" + c + " input:submit").click()
                }, b.savePaymentMethod = function() {
                    var a = {
                        nonce: b.nonce()
                    };
                    console.log("processOrder request:", a);
                    var c = $.post("/bin/belong/auth/customer/payment/method", a);
                    c.then(b.savePaymentMethodDone, b.savePaymentMethodFail)
                }, b.savePaymentMethodDone = function(c) {
                    b.states.status("SUCCESS"), b.isLoading(!1), b.states.methodSaveError([]), console.log("save payment method successful"), b.states.paymentType(c.paymentType), b.states.paypalCustomerId(c.paypalCustomerId), b.states.creditCardLast4(c.creditCardLast4), b.states.creditCardType(c.creditCardType), a.adobeAnalytics.pages.managePayment.updatePayment(!0, !1)
                }, b.savePaymentMethodFail = function(c) {
                    b.isLoading(!1), console.log("save payment method unsuccessful");
                    var d = c.responseJSON;
                    "timeout" === c.statusText ? (console.log("Server Error: timeout"), b.states.methodSaveError([{
                        code: "ERROR_TIMEOUT"
                    }])) : b.states.methodSaveError(void 0 !== d && void 0 !== d.errors ? d.errors : [{
                        code: "ERROR_DEFAULT"
                    }]);
                    var e = b.states.methodSaveError()[0].code;
                    console.log("errorCode", e), a.adobeAnalytics.pages.managePayment.updatePayment(!0, e)
                }, b.doneUpdate = function() {
                    b.states.choosenPaymethod(""), b.states.status("DEFAULT"), b.currentView(b.PAYMENT_CARD_VIEWS.MAKE_PAYMENT)
                }, b.cancelUpdate = function() {
                    b.states.choosenPaymethod(""), b.payPalError(""), b.states.methodSaveError([]), b.creditCardErrorList.removeAll()
                }
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.managePayment = a.managePayment || {}, a.managePayment.paymentCardVM = function() {
            var b = this;
            b.init = function() {
                var b = new a.managePayment.makePaymentVM,
                    c = new a.managePayment.modifyPayMethodVM;
                ko.applyBindings(b, document.getElementById("ctn-make-payment")), ko.applyBindings(c, document.getElementById("ctn-modify-paymethod")), b.init()
            }
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.managePayment = a.managePayment || {}, a.managePayment.selectPayMethodVM = function() {
            var b = this;
            b.PAYMENT_CARD_VIEWS = a.managePayment.paymentCardViews, b.currentView = ko.observable().syncWith("currentView", !0), b.isLoading = ko.observable(!0), b.customAnimation = ko.observable(!0), b.currentView.subscribe(function(a) {
                a === b.PAYMENT_CARD_VIEWS.SELECT_PAYMETHOD && b.init()
            }), b.init = function() {
                b.retrieveData()
            }, b.retrieveData = function() {
                b.isLoading(!0);
                var a = $.get("/bin/belong/auth/customer/payment/current");
                a.done(function() {
                    console.log("selectPayMethod retrieveData successful.")
                }), a.fail(function(a) {
                    console.log("selectPayMethod retrieveData AJAX failure."), console.log(a)
                }), a.always(function() {
                    b.isLoading(!1)
                })
            }, b.doneUpdate = function() {
                b.currentView(b.PAYMENT_CARD_VIEWS.MAKE_PAYMENT)
            }
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.manageService = a.manageService || {}, a.manageService.changeContractVM = function() {
            function b() {
                var a = [];
                return $.each(MANAGE_SERVICE_CMS_DATA.contracts, function(b, c) {
                    c.option !== g.SIX_MONTHS && c.option !== g.SIX_MONTHS_BTL && c.option !== g.TWELVE_MONTHS_TENOFF && a.push(c)
                }), a
            }

            function c(a) {
                return a !== g.MONTH_BY_MONTH
            }

            function d(a) {
                return a === g.SIX_MONTHS_BTL
            }
            var e = this,
                f = a.manageService.cmsDataHelpers;
            e.STATES = a.manageService.cardStates;
            var g = {
                TWELVE_MONTHS: "TWELVE_MONTHS",
                SIX_MONTHS: "SIX_MONTHS",
                SIX_MONTHS_BTL: "SIX_MONTHS_BTL",
                MONTH_BY_MONTH: "MONTH_BY_MONTH",
                TWELVE_MONTHS_TENOFF: "TWELVE_MONTHS_TEN_DOLLAR_CREDIT"
            };
            e.mapManageServiceData = function(a) {
                return e.existingContractOption(a.currentPlan.contractOption), e.selectedContractOption(a.canRecontract && c(a.currentPlan.contractOption) ? g.TWELVE_MONTHS : a.currentPlan.contractOption), e.canRecontract(a.canRecontract), e.existingContractCost(Math.abs(a.currentPlan.contractCost)), e.canChangePlan(a.canChangePlan), e.replaceContractCost(), a
            }, e.manageServiceData = ko.observable().subscribeTo("manageServiceData", e.mapManageServiceData), e.openInformation = function(b, c) {
                a.manageService.moreInformation.open($(c.target).closest(".card-container"))
            }, e.closeInformation = function(b, c) {
                a.manageService.moreInformation.close($(c.target).closest(".card-container"))
            }, e.currentState = ko.observable(e.STATES.READONLY).syncWith("contractState"), e.contractOptions = ko.observableArray(b()), e.existingContractOption = ko.observable().publishOn("existingContractOption", !0), e.existingContractDisplayName = ko.computed(function() {
                return f.getContractDisplayName(e.existingContractOption())
            }).publishOn("existingContractDisplayName", !0), e.existingContractCost = ko.observable().publishOn("existingContractCost", !0), e.selectedContractOption = ko.observable().publishOn("selectedContractOption", !0), e.selectedContractCost = ko.computed(function() {
                return f.getContractCost(e.selectedContractOption())
            }).publishOn("selectedContractCost", !0), e.selectedContractDisplayName = ko.computed(function() {
                return f.getContractDisplayName(e.selectedContractOption())
            }).publishOn("selectedContractDisplayName", !0), e.contractOptionChanged = ko.computed(function() {
                return e.selectedContractOption() !== e.existingContractOption()
            }).publishOn("contractOptionChanged", !0), e.selectedOptionIsContract = ko.computed(function() {
                return e.selectedContractOption() ? c(e.selectedContractOption()) : !1
            }).publishOn("selectedOptionIsContract"), e.canRecontract = ko.observable(!1), e.canChangePlan = ko.observable(!1), e.isOnContract = ko.computed(function() {
                return c(e.existingContractOption())
            }), e.isOnBelowTheLineOffer = ko.computed(function() {
                return d(e.existingContractOption())
            }), e.replaceContractCost = function() {
                e.existingContractCost() >= 0 && $("#txt-contract-marketing-msg-contract").each(function() {
                    var a = $(this).text();
                    $(this).text(a.replace("{contractCost}", e.existingContractCost()))
                })
            }, e.recontractClick = function() {
                console.log("recontract clicked"), e.currentState(e.STATES.EDIT), e.selectedContractOption(e.existingContractOption() === g.SIX_MONTHS ? g.TWELVE_MONTHS : data.currentPlan.contractOption)
            }
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.manageService = a.manageService || {}, a.manageService.changeDataVM = function() {
            var b = this,
                c = a.manageService.cmsDataHelpers;
            b.STATES = a.manageService.cardStates, b.UPDATE_STATES = a.manageService.updateStates, b.mapManageServiceData = function(a) {
                return b.selectedDataOption(a.currentPlan.planSize), b.existingDataCost(a.currentPlan.planCost), b.existingDataDisplaySize(c.getDataDisplaySize(a.currentPlan.planSize)), b.canChangePlan(a.canChangePlan), a
            }, b.manageServiceData = ko.observable().subscribeTo("manageServiceData", b.mapManageServiceData), b.openInformation = function(b, c) {
                a.manageService.moreInformation.open($(c.target).closest(".card-container"))
            }, b.closeInformation = function(b, c) {
                a.manageService.moreInformation.close($(c.target).closest(".card-container"))
            }, b.updateState = ko.observable().subscribeTo("updateState", function(a) {
                return b.dataUpdateState(a.dataState), a
            }), b.dataUpdateState = ko.observable(b.UPDATE_STATES.NO_CHANGE), b.dataOptions = ko.observableArray(MANAGE_SERVICE_CMS_DATA.dataOptions), b.currentState = ko.observable(b.STATES.READONLY).syncWith("dataState"), b.canChangePlan = ko.observable(!1), b.existingDataCost = ko.observable().publishOn("existingDataCost", !0), b.existingDataDisplaySize = ko.observable().publishOn("existingDataDisplaySize", !0), b.selectedDataOption = ko.observable().publishOn("selectedDataOption", !0), b.selectedDataDisplaySize = ko.computed(function() {
                return c.getDataDisplaySize(b.selectedDataOption())
            }).publishOn("selectedDataDisplaySize", !0), b.dataSelectionChanged = ko.computed(function() {
                return b.selectedDataOption() && b.manageServiceData() ? b.selectedDataOption() !== b.manageServiceData().currentPlan.planSize : !1
            }).publishOn("dataSelectionChanged", !0), b.changeDataClick = function() {
                b.currentState(b.STATES.EDIT)
            }
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.manageService = a.manageService || {}, a.manageService.changeSpeedboostVM = function() {
            function b(a) {
                var b = [];
                return $.each(MANAGE_SERVICE_CMS_DATA.speedboostOptions, function(c, f) {
                    f.speed === d.HUNDREDOVERFORTY ? a !== e.FTTB && a !== e.FTTN && b.push(f) : b.push(f)
                }), b
            }
            var c = this,
                d = {
                    TWELVEOVERONE: "12/1",
                    TWENTYFIVEOVERFIVE: "25/5",
                    HUNDREDOVERFORTY: "100/40"
                },
                e = {
                    FTTP: "FTTP",
                    FTTB: "FTTB",
                    FTTN: "FTTN"
                },
                f = a.manageService.cmsDataHelpers;
            c.STATES = a.manageService.cardStates, c.UPDATE_STATES = a.manageService.updateStates, c.mapManageServiceData = function(a) {
                return c.selectedSpeedboostOption(a.currentPlan.planSpeed), c.existingSpeedboostCost(a.currentPlan.speedboostCost), c.planType(a.currentPlan.planType), c.broadbandTechType(a.currentPlan.broadbandTechType), c.existingSpeedboostDisplaySpeed(f.getSpeedboostDisplaySpeed(a.currentPlan.planSpeed)), c.canChangePlan(a.canChangePlan), a
            }, c.manageServiceData = ko.observable().subscribeTo("manageServiceData", c.mapManageServiceData), c.openInformation = function(b, c) {
                a.manageService.moreInformation.open($(c.target).closest(".card-container"))
            }, c.closeInformation = function(b, c) {
                a.manageService.moreInformation.close($(c.target).closest(".card-container"))
            }, c.updateState = ko.observable().subscribeTo("updateState", function(a) {
                return c.speedboostUpdateState(a.speedboostState), a
            }), c.speedboostUpdateState = ko.observable(c.UPDATE_STATES.NO_CHANGE), c.currentState = ko.observable(c.STATES.READONLY).syncWith("speedboostState"), c.canChangePlan = ko.observable(!1), c.planType = ko.observable(), c.broadbandTechType = ko.observable(), c.speedboostEnabled = ko.computed(function() {
                return "NBN" === c.planType()
            }).publishOn("speedboostEnabled"), c.speedboostOptions = ko.computed(function() {
                return b(c.broadbandTechType())
            }), c.existingSpeedboostCost = ko.observable().publishOn("existingSpeedboostCost", !0), c.existingSpeedboostDisplaySpeed = ko.observable().publishOn("existingSpeedboostDisplaySpeed", !0), c.selectedSpeedboostOption = ko.observable().publishOn("selectedSpeedboostOption", !0), c.selectedSpeedboostDisplaySpeed = ko.computed(function() {
                return f.getSpeedboostDisplaySpeed(c.selectedSpeedboostOption())
            }).publishOn("selectedSpeedboostDisplaySpeed", !0), c.speedboostSelectionChanged = ko.computed(function() {
                return c.selectedSpeedboostOption() && c.manageServiceData() ? c.selectedSpeedboostOption() !== c.manageServiceData().currentPlan.planSpeed : !1
            }).publishOn("speedboostSelectionChanged", !0), c.changeSpeedboostClick = function() {
                c.currentState(c.STATES.EDIT)
            }
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.manageService = a.manageService || {}, a.manageService.confirmModalVM = function() {
            var b = this,
                c = a.manageService.cardStates;
            a.MANAGE_SERVICE_CONFIRM_MODAL_BOUND = !0, b.dataChanged = ko.observable(!1).subscribeTo("dataSelectionChanged", !0), b.speedboostChanged = ko.observable(!1).subscribeTo("speedboostSelectionChanged", !0), b.contractChanged = ko.observable(!1).subscribeTo("contractOptionChanged", !0), b.selectedDataOption = ko.observable().subscribeTo("selectedDataOption", !0), b.selectedContractOption = ko.observable().subscribeTo("selectedContractOption", !0), b.selectedSpeedboostOption = ko.observable().subscribeTo("selectedSpeedboostOption", !0), b.selectedOptionIsContract = ko.observable().subscribeTo("selectedOptionIsContract", !0), b.contractState = ko.observable().syncWith("contractState", !0), b.dataState = ko.observable().syncWith("dataState", !0), b.speedboostState = ko.observable().syncWith("speedboostState", !0), b.manageServiceData = ko.observable().publishOn("manageServiceData", !0), b.oldPlan = ko.observable().subscribeTo("manageServiceData", !0, function(a) {
                return a.currentPlan
            }), b.updateState = ko.observable().publishOn("updateState", !0), b.buttonState = ko.observable("active"), b.updateServerError = ko.observable(!1).syncWith("updateServerError"), b.init = function() {
                console.log("modal initialised")
            }, b.confirmUpdateService = function() {
                console.log("confirm clicked"), b.buttonState("loading"), b.updateServerError(!1);
                var a = {
                    planSize: b.selectedDataOption(),
                    planSpeed: b.selectedSpeedboostOption(),
                    recontractOption: b.selectedContractOption()
                };
                console.log("Calling manageServiceUpdate with\n" + JSON.stringify(a));
                var c = $.post("/bin/belong/manage/service", a);
                c.done(function(a) {
                    console.log("manageServiceUpdate successful."), console.log(a), b.fireAnalytics(b.oldPlan(), a.manageService.currentPlan), b.updateViewModel(a), $.magnificPopup.close(), b.scrollToCards()
                }), c.fail(function(a) {
                    console.log("manageServiceUpdate AJAX failure."), console.log(a), b.updateServerError(!0)
                }), c.always(function() {
                    b.buttonState("active")
                })
            }, b.fireAnalytics = function(b, c) {
                console.log("Firing Adobe Analytics event - manageServiceUpdate"), a.adobeAnalytics.pages.manageService.update({
                    oldPlan: b,
                    newPlan: c
                })
            }, b.updateViewModel = function(a) {
                var d = b.dataChanged() && b.dataState() === c.EDIT,
                    e = b.contractChanged() && b.contractState() === c.EDIT,
                    f = b.speedboostChanged() && b.speedboostState() === c.EDIT;
                b.manageServiceData(a.manageService), b.updateState(a.updateState), b.dataState(d ? c.SUCCESS : a.manageService.canChangePlan ? c.EDIT : c.READONLY), b.speedboostState(f ? c.SUCCESS : a.manageService.canChangePlan ? c.EDIT : c.READONLY), b.contractState(e ? c.SUCCESS : a.manageService.canRecontract ? c.EDIT : c.READONLY)
            }, b.confirmCancel = function() {
                $.magnificPopup.close()
            }, b.scrollToCards = function() {
                $("#ctn-parent-view").velocity("stop").velocity("scroll", {
                    duration: 400,
                    easing: "ease-out",
                    offset: -70
                })
            }
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.manageService = a.manageService || {}, $.extend(a.manageService, function() {
            var b;
            return {
                init: function() {
                    if (b = this, 0 === $("#cmp-manage-service").length) return !1;
                    var c = new b.parentViewModel;
                    ko.applyBindings(c, document.getElementById("cmp-manage-service")), ko.applyBindings(new a.manageService.changeDataVM, document.getElementById("ctn-change-data")), ko.applyBindings(new a.manageService.changeContractVM, document.getElementById("ctn-change-contract")), ko.applyBindings(new a.manageService.changeSpeedboostVM, document.getElementById("ctn-change-speedboost")), ko.applyBindings(new a.manageService.serviceSummaryVM, document.getElementById("ctn-service-summary")), c.init()
                },
                cardStates: {
                    READONLY: "READONLY",
                    EDIT: "EDIT",
                    SUCCESS: "SUCCESS"
                },
                updateStates: {
                    DOWNGRADED: "DOWNGRADED",
                    UPGRADED: "UPGRADED",
                    NO_CHANGE: "NO_CHANGE",
                    REMOVED: "REMOVED",
                    ADDED: "ADDED"
                },
                parentViewModel: function() {
                    var b = this,
                        c = a.manageService.cardStates,
                        d = a.manageService.cmsDataHelpers;
                    b.manageServiceData = ko.observable().syncWith("manageServiceData", !1, !0), b.updateStates = ko.observable().subscribeTo("updateStates"), b.manageServiceData.subscribe(function(a) {
                        b.currentPlanType(a.currentPlan.planType), b.canChangePlan(a.canChangePlan)
                    }), b.isLoading = ko.observable(!0), b.retrieveServerError = ko.observable(!1), b.contractState = ko.observable().syncWith("contractState", !1, !0), b.dataState = ko.observable().syncWith("dataState", !1, !0), b.speedboostState = ko.observable().syncWith("speedboostState", !1, !0), b.canChangePlan = ko.observable(!1), b.currentPlanType = ko.observable(), b.currentPlanCost = ko.computed(function() {
                        var a = 0;
                        return b.manageServiceData() && (a += b.manageServiceData().currentPlan.planCost, a += b.manageServiceData().currentPlan.speedboostCost, a -= Math.abs(b.manageServiceData().currentPlan.contractCost)), a
                    }), b.currentPlanDisplayName = ko.computed(function() {
                        return d.getPlanDisplayName(b.currentPlanType())
                    }), b.init = function() {
                        b.retrieveData()
                    }, b.retrieveData = function() {
                        var a = $.get("/bin/belong/manage/service");
                        return a.done(function(a) {
                            console.log("manageServiceRetrieve successful."), console.log(a);
                            var c, d, e;
                            switch (a.manageService.currentPlan.planSize) {
                                case "Regular":
                                    c = a.manageService.currentPlan;
                                    break;
                                case "Large":
                                    d = a.manageService.currentPlan;
                                    break;
                                case "ExtraLarge":
                                    e = a.manageService.currentPlan
                            }
                            $.each(a.manageService.relatedPlans, function() {
                                switch (this.planSize) {
                                    case "Regular":
                                        c = this;
                                        break;
                                    case "Large":
                                        d = this;
                                        break;
                                    case "ExtraLarge":
                                        e = this
                                }
                            }), $.each(MANAGE_SERVICE_CMS_DATA.planCosts, function(b) {
                                if (this.type === a.manageService.currentPlan.planType) switch (this.size) {
                                    case "Regular":
                                        MANAGE_SERVICE_CMS_DATA.planCosts[b].cost = c.planCost;
                                        break;
                                    case "Large":
                                        MANAGE_SERVICE_CMS_DATA.planCosts[b].cost = d.planCost;
                                        break;
                                    case "ExtraLarge":
                                        MANAGE_SERVICE_CMS_DATA.planCosts[b].cost = e.planCost
                                }
                            }), b.manageServiceData(a.manageService), b.initCardStates(a.manageService.canChangePlan, a.manageService.canRecontract), b.fireAnalytics()
                        }), a.fail(function(a) {
                            console.log("manageServiceRetrieve AJAX failure."), console.log(a), b.retrieveServerError(!0)
                        }), a.always(function() {
                            b.isLoading(!1)
                        }), a
                    }, b.fireAnalytics = function() {
                        console.log("Firing Adobe Analytics event - manageServiceDisplay"), a.adobeAnalytics.pages.manageService.display({
                            oldPlan: b.manageServiceData().currentPlan
                        })
                    }, b.initCardStates = function(a, d) {
                        a ? (b.dataState(c.EDIT), b.speedboostState(c.EDIT)) : (b.dataState(c.READONLY), b.speedboostState(c.READONLY)), b.contractState(d ? c.EDIT : c.READONLY)
                    }
                },
                cmsDataHelpers: {
                    getPlanDisplayName: function(a) {
                        var b = $.grep(MANAGE_SERVICE_CMS_DATA.planDisplayNames, function(b) {
                            return b.planType === a
                        });
                        return b.length > 0 ? b[0].displayName : ""
                    },
                    getContractDisplayName: function(a) {
                        var b = $.grep(MANAGE_SERVICE_CMS_DATA.contracts, function(b) {
                            return b.option === a
                        });
                        return b.length > 0 ? b[0].displayName : ""
                    },
                    getContractCost: function(a) {
                        var b = $.grep(MANAGE_SERVICE_CMS_DATA.contracts, function(b) {
                            return b.option === a
                        });
                        return b.length > 0 ? b[0].cost : 0
                    },
                    getDataDisplaySize: function(a) {
                        var b = $.grep(MANAGE_SERVICE_CMS_DATA.dataOptions, function(b) {
                            return b.planSize === a
                        });
                        return b.length > 0 ? b[0].displaySize : ""
                    },
                    getPlanCosts: function(a, b) {
                        var c = $.grep(MANAGE_SERVICE_CMS_DATA.planCosts, function(c) {
                            return a === c.type && b === c.size
                        });
                        return c.length > 0 ? c[0] : null
                    },
                    getSpeedboostDisplaySpeed: function(a) {
                        var b = $.grep(MANAGE_SERVICE_CMS_DATA.speedboostOptions, function(b) {
                            return b.speed === a ? !0 : void 0
                        });
                        return b.length > 0 ? b[0].displaySpeed : ""
                    },
                    getSpeedboostCost: function(a) {
                        var b = $.grep(MANAGE_SERVICE_CMS_DATA.speedboostOptions, function(b) {
                            return b.speed === a ? !0 : void 0
                        });
                        return b.length > 0 ? b[0].cost : 0
                    }
                },
                moreInformation: {
                    open: function(a) {
                        a.addClass("expanded"), a.find(".information").hasClass("slider-activated") ? (a.find(".information").removeClass("invisible"), a.find(".information").flexslider(0), a.find(".information").flexslider("pause")) : a.find(".information").flexslider({
                            animation: "slide",
                            animationLoop: !1,
                            slideShow: !1,
                            start: function(b) {
                                a.find(".information").addClass("slider-activated").addClass("slider-count-" + b.count).removeClass("invisible"), a.find(".information").flexslider("pause")
                            }
                        })
                    },
                    close: function(a) {
                        a.removeClass("expanded"), a.find(".information").addClass("invisible")
                    }
                }
            }
        }())
    }(BELONG),
    function(a) {
        "use strict";
        a.manageService = a.manageService || {}, a.manageService.serviceSummaryVM = function() {
            function b() {
                c.currentState(c.STATES.CURRENT_PLAN);
                for (var a in c.currentCardStates) c.currentCardStates[a] === c.CARD_STATES.EDIT && c.currentState(c.STATES.MODIFIED_PLAN);
                c.setStickySummary()
            }
            var c = this,
                d = a.manageService.cmsDataHelpers,
                e = {
                    TWELVE_MONTHS: 12,
                    SIX_MONTHS: 6,
                    MONTH_BY_MONTH: 0
                };
            c.STATES = {
                CURRENT_PLAN: "CURRENT_PLAN",
                MODIFIED_PLAN: "MODIFIED_PLAN"
            }, c.CARD_STATES = a.manageService.cardStates, c.currentCardStates = {
                data: "",
                contract: "",
                speedboost: ""
            }, c.contractStateUpdated = function(a) {
                return console.log("contract state changed"), c.currentCardStates.contract = a ? c.CARD_STATES.EDIT : c.CARD_STATES.READONLY, b(), a
            }, c.contractOptionChanged = ko.observable().subscribeTo("contractOptionChanged", c.contractStateUpdated), c.dataStateUpdated = function(a) {
                return console.log("data state changed"), c.currentCardStates.data = a ? c.CARD_STATES.EDIT : c.CARD_STATES.READONLY, b(), a
            }, c.dataSelectionChanged = ko.observable().subscribeTo("dataSelectionChanged", c.dataStateUpdated), c.speedboostStateUpdated = function(a) {
                return console.log("speedboost state changed"), c.currentCardStates.speedboost = a ? c.CARD_STATES.EDIT : c.CARD_STATES.READONLY, b(), a
            }, c.speedboostSelectionChanged = ko.observable().subscribeTo("speedboostSelectionChanged", c.speedboostStateUpdated), c.currentState = ko.observable(c.STATES.CURRENT_PLAN), c.updateServerError = ko.observable().syncWith("updateServerError", !1, !0), c.manageServiceData = ko.observable().subscribeTo("manageServiceData", function(a) {
                return c.existingDataCost(a.currentPlan.planCost), c.isExtendedNetwork(a.currentPlan.extendedNetwork), a
            }), c.isExtendedNetwork = ko.observable(!1), c.existingContractDisplayName = ko.observable().subscribeTo("existingContractDisplayName"), c.existingContractOption = ko.observable().subscribeTo("existingContractOption"), c.existingContractCost = ko.observable().subscribeTo("existingContractCost"), c.displayExistingContract = ko.computed(function() {
                return "MONTH_BY_MONTH" !== c.existingContractOption()
            }), c.selectedContractDisplayName = ko.observable().subscribeTo("selectedContractDisplayName"), c.selectedContractOption = ko.observable().subscribeTo("selectedContractOption"), c.selectedContractCost = ko.observable().subscribeTo("selectedContractCost"), c.selectedOptionIsContract = ko.observable().subscribeTo("selectedOptionIsContract"), c.selectedContractLength = ko.computed(function() {
                return e[c.selectedContractOption()]
            }), c.existingDataCost = ko.observable().subscribeTo("existingDataCost"), c.existingDataDisplaySize = ko.observable().subscribeTo("existingDataDisplaySize"), c.selectedDataOption = ko.observable().subscribeTo("selectedDataOption"), c.selectedDataCost = ko.computed(function() {
                if (c.manageServiceData()) {
                    var a = c.manageServiceData().currentPlan,
                        b = d.getPlanCosts(a.planType, c.selectedDataOption());
                    return b ? b.cost : 0
                }
                return 0
            }), c.selectedDataDisplaySize = ko.observable().subscribeTo("selectedDataDisplaySize"), c.existingSpeedboostDisplaySpeed = ko.observable().subscribeTo("existingSpeedboostDisplaySpeed"), c.existingSpeedboostCost = ko.observable().subscribeTo("existingSpeedboostCost"), c.selectedSpeedboostOption = ko.observable().subscribeTo("selectedSpeedboostOption"), c.selectedSpeedboostCost = ko.computed(function() {
                if (c.manageServiceData()) {
                    {
                        c.manageServiceData().currentPlan
                    }
                    return d.getSpeedboostCost(c.selectedSpeedboostOption())
                }
                return 0
            }), c.selectedSpeedboostDisplaySpeed = ko.observable().subscribeTo("selectedSpeedboostDisplaySpeed"), c.speedboostEnabled = ko.observable().subscribeTo("speedboostEnabled"), c.displaySpeedboost = ko.computed(function() {
                return c.speedboostEnabled() && "12/1" !== c.selectedSpeedboostOption()
            }), c.monthlyCharges = function() {
                var a = 0;
                return a += c.selectedDataCost(), a += c.selectedSpeedboostCost(), c.selectedContractCost() && (a -= c.selectedContractCost()), a
            }, c.totalMinCost = function() {
                var a = c.monthlyCharges();
                return "MONTH_BY_MONTH" !== c.selectedContractOption() && (a *= e[c.selectedContractOption()]), a
            }, c.stickySummary = ko.observable(!0), c.setStickySummary = function() {
                var a = 70,
                    b = $("#ctn-service-summary").offset().top,
                    d = b + a - $(window).height() - $(window).scrollTop() < 0;
                c.stickySummary(!d)
            }, c.stickyScroll = function() {
                $("#ctn-service-summary").velocity("scroll", {
                    offset: -70
                })
            }, $(window).off("scroll.stickySummary").on("scroll.stickySummary", function() {
                c.setStickySummary()
            }), c.updateService = function() {
                if (console.log("update service clicked"), $.magnificPopup.open({
                        items: {
                            src: "#mdl-manage-service-confirm",
                            type: "inline"
                        },
                        showCloseBtn: !0,
                        closeOnBgClick: !0,
                        overflowY: "auto",
                        callbacks: {
                            close: c.resetUpdateModal
                        }
                    }), !a.MANAGE_SERVICE_CONFIRM_MODAL_BOUND) {
                    var b = new a.manageService.confirmModalVM;
                    ko.applyBindings(b, document.getElementById("mdl-manage-service-confirm")), b.init()
                }
                c.fireAnalytics()
            }, c.resetUpdateModal = function() {
                c.updateServerError(!1)
            }, c.fireAnalytics = function() {
                console.log("Firing Adobe Analytics event - manageServiceReview");
                var b = {
                        originalPlanSize: c.manageServiceData().currentPlan.planSize,
                        originalPlanSpeed: c.manageServiceData().currentPlan.planSpeed,
                        originalContractOption: c.manageServiceData().currentPlan.contractOption,
                        newPlanSize: c.selectedDataOption(),
                        newPlanSpeed: c.selectedSpeedboostOption(),
                        newContractOption: c.selectedContractOption()
                    },
                    d = c.manageServiceData().currentPlan;
                a.adobeAnalytics.pages.manageService.review({
                    updateAction: b,
                    oldPlan: d
                })
            }
        }
    }(BELONG),
    function(EVE) {
        "use strict";
        EVE.personalDetails = EVE.personalDetails || {}, $.extend(EVE.personalDetails, function() {
            var _self;
            return {
                init: function() {
                    if (_self = this, 0 === $("#cmp-personal-details").length) return !1;
                    var a = new this.viewModel;
                    ko.applyBindings(a, document.getElementById("cmp-personal-details")), a.init()
                },
                viewModel: function() {
                    function createFields() {
                        var a = $.extend(!0, [], PERSONAL_DETAILS_CMS_DATA.fields);
                        return a.forEach(function(a) {
                            a.value = ko.observable(""), a.updatedValue = ko.observable(""), a.fieldUpdated = ko.observable(!1), a.id === self.FIELDS.PASSWORD && (a.newPassword = ko.observable(""), a.confirmPassword = ko.observable(""))
                        }), a
                    }
                    var self = this,
                        API_URL = "/bin/belong/auth/customer/personal",
                        UPDATE_URLS = {
                            email: "/bin/belong/auth/customer/personal/email",
                            password: "/bin/belong/auth/customer/personal/password",
                            mobile: "/bin/belong/auth/customer/personal/mobile"
                        };
                    self.STATES = {
                        VIEW: "VIEW",
                        EDIT: "EDIT"
                    }, self.FIELDS = {
                        ADDRESS: "address",
                        MOBILE: "mobile",
                        EMAIL: "email",
                        PASSWORD: "password"
                    }, self.ERROR = {
                        invalidMobile: "personal.invalidMobile",
                        invalidEmail: "personal.invalidEmail",
                        invalidPassword: "personal.invalidPassword",
                        invalidDobFormat: "personal.invalidDobFormat",
                        dobNotMatchWithIdentity: "personal.dobNotMatchWithIdentity"
                    }, self.viewState = {
                        fields: createFields(),
                        currentState: ko.observable("VIEW"),
                        isLoading: ko.observable(!0),
                        updateLoading: ko.observable(!1),
                        serverError: ko.observable(!1),
                        updateServerError: ko.observable(!1)
                    }, self.personalDetailsModel = ko.observable(), self.init = function() {
                        self.apiRetrieve()
                    }, self.apiRetrieve = function() {
                        var a = $.get(API_URL);
                        a.done(function(a) {
                            self.personalDetailsModel(a), EVE.adobeAnalytics.pages.personalDetails.display()
                        }), a.fail(function(a) {
                            console.log(a.responseJSON), self.viewState.serverError(!0)
                        }), a.always(function() {
                            self.viewState.isLoading(!1)
                        })
                    }, self.personalDetailsModel.subscribe(function(a) {
                        self.resetForm(), self.updateFieldValues(a)
                    }), self.resetForm = function() {
                        self.viewState.fields.forEach(function(a) {
                            switch (a.id) {
                                case self.FIELDS.EMAIL:
                                case self.FIELDS.MOBILE:
                                    a.updatedValue(""), a.updatedValue.subscribe(self.fieldUpdateHandler);
                                    break;
                                case self.FIELDS.PASSWORD:
                                    a.newPassword(""), a.confirmPassword(""), a.confirmPassword.subscribe(self.fieldUpdateHandler)
                            }
                        }), self.viewState.currentEditingField = null, self.viewState.updateServerError(!1)
                    }, self.clearUpdatedState = function() {
                        self.viewState.fields.forEach(function(a) {
                            a.fieldUpdated(!1)
                        })
                    }, self.updateFieldValues = function(a) {
                        self.viewState.fields.forEach(function(b) {
                            switch (b.id) {
                                case self.FIELDS.ADDRESS:
                                    b.value(self.constructAddressString(a));
                                    break;
                                case self.FIELDS.MOBILE:
                                    b.value(a.mobile);
                                    break;

                                case self.FIELDS.EMAIL:
                                    b.value(a.email);
                                    break;
                                case self.FIELDS.PASSWORD:
                            }
                        })
                    }, self.constructAddressString = function(a) {
                        var b = a.address.trim();
                        return b = a.address2.trim() ? b + ", " + a.address2.trim() : b, b += ", " + a.suburb.trim() + " " + a.state.trim() + " " + a.postcode.trim()
                    }, self.editClicked = function(a) {
                        console.log(a.id + " edit clicked."), self.clearUpdatedState(), self.viewState.currentEditingField = a, self.viewState.currentState(self.STATES.EDIT), EVE.forms.applyShiftLabel();
                        var b = {
                            rules: {
                                "txt-email": {
                                    required: !0,
                                    backendEmail: !0,
                                    disallowedCharacters: !0,
                                    maxlength: 255
                                },
                                "txt-mobile": {
                                    required: !0,
                                    australianMobileNumber: !0
                                },
                                "txt-new-password": {
                                    required: !0,
                                    passwordComplexity: !0,
                                    disallowedCharacters: !0,
                                    badPassword: !0,
                                    minlength: 8,
                                    maxlength: 30
                                },
                                "txt-confirm-password": {
                                    equalTo: "#txt-new-password"
                                }
                            }
                        };
                        self.validator = EVE.validation.setupFormValidation("frm-edit-fields", $(".frm-edit-fields"), b), self.fireEditAnalytics(a.id, "edit")
                    }, self.editCancelled = function() {
                        console.log("Cancel edit clicked."), self.viewState.currentState(self.STATES.VIEW), self.resetForm()
                    }, self.updateClicked = function(a) {
                        if (console.log(a.id + " update clicked."), EVE.validation.validateForm(self.validator)) switch (self.viewState.updateLoading(!0), a.id) {
                            case "email":
                                a.updatedValue(a.updatedValue().replace(/ /g, "")), self.apiUpdateEmail(a);
                                break;
                            case "mobile":
                                a.updatedValue(a.updatedValue().replace(/ /g, "")), self.apiUpdateMobile(a);
                                break;
                            case "password":
                                self.apiUpdatePassword(a)
                        }
                    }, self.formSubmit = function() {
                        return !1
                    }, self.fieldUpdateHandler = function() {
                        EVE.validation.resetCustomMessages()
                    }, self.apiUpdateEmail = function(a) {
                        var b = $.post(UPDATE_URLS.email, {
                            email: a.updatedValue()
                        });
                        b.done(function() {
                            a.fieldUpdated(!0);
                            var b = self.personalDetailsModel().email;
                            self.personalDetailsModel().email = a.updatedValue(), self.personalDetailsModel.valueHasMutated(), self.viewState.currentState(self.STATES.VIEW), self.fireEditAnalytics(a.id, "update"), EVE.adobeAnalytics.pages.personalDetails.updateEmail({
                                oldValue: b,
                                newValue: self.personalDetailsModel().email
                            })
                        }), b.fail(function(a, b) {
                            self.applyApiUpdateError("txt-email", a.responseJSON, b)
                        }), b.always(function() {
                            self.viewState.updateLoading(!1)
                        })
                    }, self.apiUpdateMobile = function(a) {
                        var b = $.post(UPDATE_URLS.mobile, {
                            mobileNumber: a.updatedValue()
                        });
                        b.done(function() {
                            a.fieldUpdated(!0);
                            self.personalDetailsModel().mobile;
                            self.personalDetailsModel().mobile = a.updatedValue(), self.personalDetailsModel.valueHasMutated(), self.viewState.currentState(self.STATES.VIEW), self.fireEditAnalytics(a.id, "update")
                        }), b.fail(function(a, b) {
                            self.applyApiUpdateError("txt-mobile", a.responseJSON, b)
                        }), b.always(function() {
                            self.viewState.updateLoading(!1)
                        })
                    }, self.apiUpdatePassword = function(a) {
                        var b = $.post(UPDATE_URLS.password, {
                            password: a.confirmPassword()
                        });
                        b.done(function() {
                            a.fieldUpdated(!0), self.resetForm(), self.viewState.currentState(self.STATES.VIEW), self.fireEditAnalytics(a.id, "update")
                        }), b.fail(function(a, b) {
                            self.applyApiUpdateError("txt-confirm-password", a.responseJSON, b)
                        }), b.always(function() {
                            self.viewState.updateLoading(!1)
                        })
                    }, self.applyApiUpdateError = function(a, b, c) {
                        if ("timeout" === c) self.viewState.updateServerError(!0);
                        else {
                            var d = b.errors[0].code,
                                e = d.split(".");
                            if ("common" === e[0] || d === self.ERROR.dobNotMatchWithIdentity) self.viewState.updateServerError(!0);
                            else {
                                EVE.validation.resetCustomMessages();
                                var f = EVE.responseMessage.getApiMessage("frm-edit-fields-api", a, d.split(".")[1]);
                                EVE.validation.attachCustomMessage(a, f, "error", !0)
                            }
                        }
                    }, self.fireEditAnalytics = function(fieldId, op) {
                        var field = fieldId && fieldId[0].toUpperCase() + fieldId.slice(1),
                            func = op + field;
                        eval("EVE.adobeAnalytics.pages.personalDetails." + func + "()")
                    }
                }
            }
        }())
    }(BELONG),
    function(a) {
        "use strict";
        a.productBuilder = a.productBuilder || {}, a.productBuilder.contractCardVM = function() {
            var b = this,
                c = {
                    TWELVE_MONTHS: "TWELVE_MONTHS",
                    MONTH_BY_MONTH: "MONTH_BY_MONTH"
                };
            b.contractOptions = PRODUCT_BUILDER_CONTRACTS, b.selectedContractOption = ko.observable(c.TWELVE_MONTHS), b.selectedContractObject = ko.computed(function() {
                for (var a = 0; a < PRODUCT_BUILDER_CONTRACTS.length; a++)
                    if (PRODUCT_BUILDER_CONTRACTS[a].option === b.selectedContractOption()) return PRODUCT_BUILDER_CONTRACTS[a];
                return null
            }).publishOn("selectedContract"), b.selectedPlanSize = ko.observable({}).subscribeTo("selectedPlanSize", !0), b.productType = ko.observable().subscribeTo("productType", !0), b.selectedContractCost = ko.computed(function() {
                var a = b.selectedContractObject().cost;
                return "NBN" === b.productType() && "Regular" === b.selectedPlanSize().planSize && b.selectedContractOption() === c.MONTH_BY_MONTH && (a += 5), a
            }).publishOn("selectedContractCost"), b.openModal = function() {
                a.adobeAnalytics.pages.productBuilder.contractModalOpen(), a.productBuilder.contractModal()
            }, b.selectedContractOption.subscribe(function(b) {
                a.adobeAnalytics.pages.productBuilder.contractSelection(b)
            });
            var d = sessionStorage.getItem("tempProductBuilder");
            d && b.selectedContractOption(JSON.parse(d).contractOption)
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.productBuilder = a.productBuilder || {}, a.productBuilder.contractModalVM = function() {
            var b = this;
            b.modalCancel = function() {
                a.adobeAnalytics.pages.productBuilder.contractModalClose(), $.magnificPopup.close()
            }
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.productBuilder = a.productBuilder || {}, a.productBuilder.phoneLineCardVM = function() {
            var b = this;
            b.PHONELINEOPTIONS = {
                BUNDLE: "BUNDLE",
                DSL: "DSL"
            }, b.phoneLineOptions = PRODUCT_BUILDER_PHONELINE, b.selectedPhoneLineOption = ko.observable(b.PHONELINEOPTIONS.BUNDLE).syncWith("selectedPhoneLineOption"), b.selectedPhoneLineObject = ko.computed(function() {
                for (var a = 0; a < PRODUCT_BUILDER_PHONELINE.length; a++)
                    if (PRODUCT_BUILDER_PHONELINE[a].planType === b.selectedPhoneLineOption()) return PRODUCT_BUILDER_PHONELINE[a];
                return null
            }).syncWith("selectedPhoneLine", !0), b.priceUpdate = ko.computed(function() {
                return b.selectedPhoneLineObject().costSaving
            }), b.displayModal = function() {
                a.productBuilder.phonelineModal()
            }, b.selectedPhoneLineOption.subscribe(function(b) {
                a.adobeAnalytics.pages.productBuilder.phoneLineSelection(b)
            })
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.productBuilder = a.productBuilder || {}, a.productBuilder.phonelineModalVM = function() {
            var b = this;
            b.modalCancel = function() {
                a.adobeAnalytics.pages.productBuilder.phoneLineModalClose(), $.magnificPopup.close()
            }
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.productBuilder = a.productBuilder || {}, a.productBuilder.planCardVM = function() {
            var b = this,
                c = {
                    REGULAR: "Regular",
                    LARGE: "Large",
                    EXTRALARGE: "ExtraLarge"
                };
            b.planOptions = PRODUCT_BUILDER_PLANS, b.selectedPlanOption = ko.observable(c.REGULAR), b.selectedPlanObject = ko.computed(function() {
                for (var a = 0; a < PRODUCT_BUILDER_PLANS.length; a++)
                    if (PRODUCT_BUILDER_PLANS[a].planSize === b.selectedPlanOption()) return PRODUCT_BUILDER_PLANS[a];
                return null
            }).publishOn("selectedPlanSize"), b.selectedPlanType = ko.observable().subscribeTo("selectedPlanType", !0), b.selectedPhoneLine = ko.observable().subscribeTo("selectedPhoneLine", !0), b.selectedPlanCost = ko.observable().subscribeTo("selectedPlanCost", !0), b.modifiedPlanCost = ko.computed(function() {
                var a = b.selectedPlanCost();
                return "DSL" === b.selectedPlanType() && (a = b.selectedPlanCost() + b.selectedPhoneLine().costSaving), Modernizr.touch && ($("#lblPlanCostPopup").css({
                    display: "inline-block"
                }), setTimeout(function() {
                    $("#lblPlanCostPopup").css({
                        display: ""
                    })
                }, 10)), a
            }), b.regularPlanCostPerUnit = ko.observable().subscribeTo("regularPlanCostPerUnit", !0), b.largePlanCostPerUnit = ko.observable().subscribeTo("largePlanCostPerUnit", !0), b.extraLargePlanCostPerUnit = ko.observable().subscribeTo("extraLargePlanCostPerUnit", !0), b.displayModal = function() {
                a.adobeAnalytics.pages.productBuilder.planModalOpen(), a.productBuilder.planModal()
            }, b.selectedPlanOption.subscribe(function(b) {
                a.adobeAnalytics.pages.productBuilder.planSelection(b)
            });
            var d = sessionStorage.getItem("tempProductBuilder");
            d && b.selectedPlanOption(JSON.parse(d).planSize)
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.productBuilder = a.productBuilder || {}, a.productBuilder.planModalVM = function() {
            var b = this;
            b.modalCancel = function() {
                a.adobeAnalytics.pages.productBuilder.planModalClose(), $.magnificPopup.close()
            }
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.productBuilder = a.productBuilder || {}, $.extend(a.productBuilder, function() {
            var b, c = !1;
            return a.PLANMODALINITIALISED = !1, a.CONTRACTMODALINITIALISED = !1, a.SPEEDBOOSTMODALINITIALISED = !1, a.VOICEMODALINITIALISED = !1, a.PHONELINEMODALINITIALISED = !1, {
                init: function() {
                    function d(a) {
                        return decodeURIComponent((new RegExp("[?|&]" + a + "=([^&;]+?)(&|#|;|$)").exec(location.search) || [, ""])[1].replace(/\+/g, "%20")) || null
                    }
                    if (b = this, 0 === $("#cmp-product-builder").length) return !1;
                    $("#nav-join").remove(), $("#belong-cards").off("click").on("click", function(a) {
                        DD.bp.is("1366,4000") && !c && /belong-selection-item/i.test(a.target.parentElement.className) && (c = !0, $("#belong-cards").velocity("stop").velocity("scroll", {
                            duration: 1e3,
                            easing: "ease-out",
                            offset: -80
                        }))
                    });
                    var e = new RegExp("adsl|nbn"),
                        f = e.exec(window.location.pathname)[0].toUpperCase(),
                        g = new b.viewModel(f),
                        h = new a.productBuilder.promoCodeVM;
                    sessionStorage.removeItem("productModel"), ko.applyBindings(new a.productBuilder.contractCardVM, document.getElementById("ctn-contract-card")), ko.applyBindings(new a.productBuilder.voiceCardVM, document.getElementById("ctn-voice-card")), ko.applyBindings(new a.productBuilder.planCardVM, document.getElementById("ctn-plan-card")), ko.applyBindings(h, document.getElementById("module-promo-code")), ko.applyBindings(g, document.getElementById("cmp-product-builder")), g.promoVM = h, g.init(), h.init(), $(".fn-modal-plan").off("click").on("click", function() {
                        a.adobeAnalytics.pages.productBuilder.planModalOpen(), b.planModal()
                    }), $(".fn-modal-contract").off("click").on("click", function() {
                        a.adobeAnalytics.pages.productBuilder.contractModalOpen(), b.contractModal()
                    }), $(".fn-modal-speedboost").off("click").on("click", function() {
                        a.adobeAnalytics.pages.productBuilder.speedboostModalOpen(), b.speedboostModal()
                    }), $(".fn-modal-voice").off("click").on("click", function() {
                        a.adobeAnalytics.pages.productBuilder.voiceModalOpen(), b.voiceModal()
                    }), $(".fn-modal-phoneline").off("click").on("click", function() {
                        a.adobeAnalytics.pages.productBuilder.phoneLineModalOpen(), b.phonelineModal()
                    });
                    var i = d("modal");
                    if (i) switch (i) {
                        case "plan":
                            b.planModal();
                            break;
                        case "contract":
                            b.contractModal();
                            break;
                        case "speedboost":
                            b.speedboostModal();
                            break;
                        case "voice":
                            b.voiceModal();
                            break;
                        case "phoneline":
                            b.phonelineModal()
                    }
                    sessionStorage.removeItem("tempProductBuilder")
                },
                planModal: function() {
                    b.openModal({
                        modalId: "mdl-builder-plan",
                        initialisedCheck: a.PLANMODALINITIALISED,
                        modalVM: a.productBuilder.planModalVM,
                        autoResizeNameSpace: "modalAutoResizeBuilderPlan"
                    }), a.PLANMODALINITIALISED = !0
                },
                contractModal: function() {
                    b.openModal({
                        modalId: "mdl-builder-contract",
                        initialisedCheck: a.CONTRACTMODALINITIALISED,
                        modalVM: a.productBuilder.contractModalVM,
                        autoResizeNameSpace: "modalAutoResizeBuilderContract"
                    }), a.CONTRACTMODALINITIALISED = !0
                },
                speedboostModal: function() {
                    b.openModal({
                        modalId: "mdl-builder-speed-boost",
                        initialisedCheck: a.SPEEDBOOSTMODALINITIALISED,
                        modalVM: a.productBuilder.speedBoostModalVM,
                        autoResizeNameSpace: "modalAutoResizeBuilderSpeedBoost"
                    }), a.SPEEDBOOSTMODALINITIALISED = !0
                },
                voiceModal: function() {
                    b.openModal({
                        modalId: "mdl-builder-voice",
                        initialisedCheck: a.VOICEMODALINITIALISED,
                        modalVM: a.productBuilder.voiceModalVM,
                        autoResizeNameSpace: "modalAutoResizeBuilderVoice"
                    }), a.VOICEMODALINITIALISED = !0
                },
                phonelineModal: function() {
                    b.openModal({
                        modalId: "mdl-builder-phone-line",
                        initialisedCheck: a.PHONELINEMODALINITIALISED,
                        modalVM: a.productBuilder.phonelineModalVM,
                        autoResizeNameSpace: "modalAutoResizeBuilderPhoneLine"
                    }), a.PHONELINEMODALINITIALISED = !0
                },
                openModal: function(b) {
                    $.magnificPopup.open({
                        items: {
                            src: "#" + b.modalId,
                            type: "inline"
                        },
                        alignTop: !0,
                        showCloseBtn: !0,
                        closeOnBgClick: !0,
                        overflowY: "auto",
                        callbacks: {
                            open: function() {
                                b.initialisedCheck || ko.applyBindings(new b.modalVM, document.getElementById(b.modalId))
                            }
                        }
                    }), a.belongModal.setupAutoResize({
                        modalContainer: $("#" + b.modalId),
                        nameSpace: b.autoResizeNameSpace
                    })
                },
                viewModel: function(b) {
                    var c = this;
                    c.PROD_TYPES = {
                        NBN: "NBN",
                        ADSL: "ADSL"
                    }, c.PLAN_TYPES = {
                        NBN: "NBN",
                        DSL: "DSL",
                        BUNDLE: "BUNDLE"
                    }, c.PLAN_SIZES = {
                        REGULAR: "Regular",
                        LARGE: "Large",
                        EXTRALARGE: "ExtraLarge"
                    }, c.SQ_ORDER_TYPES = {
                        PORT: "PORT_ORDER",
                        NEW: "NEW_ORDER",
                        CHURN: "CHURN_ORDER"
                    }, c.PAGE_LINKS = {
                        JOIN: "/join?productbuilder=true",
                        NBN: "/nbn",
                        ADSL: "/adsl"
                    }, c.extendedNetworkSurcharge = PRODUCT_BUILDER_EXTENDED_NETWORK_SURCHARGE, c.bindings = {
                        phoneLineCard: !1,
                        speedboostCard: !1
                    }, c.promoVM = null, c.isLoading = ko.observable(!0);
                    PRODUCT_BUILDER_BASE_PLAN_COSTS;
                    c.productType = ko.observable(b).publishOn("productType"), c.sqModel = ko.observable({}), c.joinWithPromo = ko.observable(!1), c.sqDone = ko.observable(!1).publishOn("sqDone"), c.sqTechType = ko.observable("").publishOn("sqTechType"), c.sqOrderType = ko.observable(""), c.sqProductType = ko.computed(function() {
                        return ["ADSL1", "ADSL2"].indexOf(c.sqTechType()) >= 0 ? "ADSL" : "NBN"
                    }), c.sqExtendedNetwork = ko.observable(!1).publishOn("sqExtendedNetwork"), c.sqAddress = ko.observable(""), c.sqEditHover = function() {
                        $(".sq-result .edit-icon").removeClass("svg-edit-icon-grey"), $(".sq-result .edit-icon").addClass("svg-edit-icon-bblue")
                    }, c.sqEditOffHover = function() {
                        $(".sq-result .edit-icon").addClass("svg-edit-icon-grey"), $(".sq-result .edit-icon").removeClass("svg-edit-icon-bblue")
                    }, c.sqCheckHover = function() {
                        $(".sq-link .check-icon").removeClass("svg-house-bblue"), $(".sq-link .check-icon").addClass("svg-house-dark-blue")
                    }, c.sqCheckOffHover = function() {
                        $(".sq-link .check-icon").addClass("svg-house-bblue"), $(".sq-link .check-icon").removeClass("svg-house-dark-blue")
                    }, c.planCardVisible = ko.observable(!0).publishOn("planCardVisible"), c.contractCardVisible = ko.observable(!0).publishOn("contractCardVisible"), c.speedboostCardVisible = ko.observable(!1).publishOn("speedboostCardVisible"), c.voiceCardVisible = ko.observable(!0).publishOn("voiceCardVisible"), c.phoneLineCardVisible = ko.observable(!1).publishOn("phoneLineCardVisible"), c.selectedPlanSize = ko.observable({}).subscribeTo("selectedPlanSize"), c.selectedContract = ko.observable({}).subscribeTo("selectedContract"), c.selectedContractCost = ko.observable(0).subscribeTo("selectedContractCost"), c.selectedSpeed = ko.observable({}).subscribeTo("selectedSpeed"), c.selectedPhoneLineOption = ko.observable().syncWith("selectedPhoneLineOption", !0), c.selectedPhoneLine = ko.observable({}).syncWith("selectedPhoneLine"), c.selectedVoicePlan = ko.observable({}).subscribeTo("selectedVoicePlan"), c.promoCode = ko.observable("").subscribeTo("promoCode", !1), c.isPromoApplied = ko.observable().syncWith("isPromoApplied", !0), c.promoStaffId = ko.observable("").subscribeTo("promoStaffId", !1), c.showReapplyMessage = ko.observable("").syncWith("promoShowReapplyMessage", !0), c.campaignCode = ko.observable("").subscribeTo("campaignCode", !1), c.promoCredit = ko.observable({}).subscribeTo("promoCredit", !0), c.selectedPlanType = ko.computed(function() {
                        var a = c.productType();
                        return c.productType() === c.PROD_TYPES.ADSL && (a = c.PLAN_TYPES.BUNDLE, c.selectedPhoneLine().planType && (a = c.selectedPhoneLine().planType)), a
                    }).publishOn("selectedPlanType"), c.selectedPlanCost = ko.computed(function() {
                        if (c.selectedPlanType() && c.selectedPlanSize())
                            for (var a = 0; a < PRODUCT_BUILDER_BASE_PLAN_COSTS.length; a++) {
                                var b = PRODUCT_BUILDER_BASE_PLAN_COSTS[a];
                                if (b.planType === c.selectedPlanType() && b.planSize === c.selectedPlanSize().planSize) return c.sqExtendedNetwork() ? b.cost + c.extendedNetworkSurcharge : b.cost
                            }
                        return 0
                    }).publishOn("selectedPlanCost"), c.regularPlanCostPerUnit = ko.computed(function() {
                        if (c.selectedPlanType())
                            for (var a = 0; a < PRODUCT_BUILDER_BASE_PLAN_COSTS.length; a++) {
                                var b = PRODUCT_BUILDER_BASE_PLAN_COSTS[a];
                                if (b.planType === c.selectedPlanType() && b.planSize === c.PLAN_SIZES.REGULAR) return c.sqExtendedNetwork() && b.planType != c.PLAN_TYPES.NBN ? b.extUnitCost : b.unitCost
                            }
                        return 0
                    }).publishOn("regularPlanCostPerUnit"), c.largePlanCostPerUnit = ko.computed(function() {
                        if (c.selectedPlanType())
                            for (var a = 0; a < PRODUCT_BUILDER_BASE_PLAN_COSTS.length; a++) {
                                var b = PRODUCT_BUILDER_BASE_PLAN_COSTS[a];
                                if (b.planType === c.selectedPlanType() && b.planSize === c.PLAN_SIZES.LARGE) return c.sqExtendedNetwork() && b.planType != c.PLAN_TYPES.NBN ? b.extUnitCost : b.unitCost
                            }
                        return 0
                    }).publishOn("largePlanCostPerUnit"), c.extraLargePlanCostPerUnit = ko.computed(function() {
                        if (c.selectedPlanType())
                            for (var a = 0; a < PRODUCT_BUILDER_BASE_PLAN_COSTS.length; a++) {
                                var b = PRODUCT_BUILDER_BASE_PLAN_COSTS[a];
                                if (b.planType === c.selectedPlanType() && b.planSize === c.PLAN_SIZES.EXTRALARGE) return c.sqExtendedNetwork() && b.planType != c.PLAN_TYPES.NBN ? b.extUnitCost : b.unitCost
                            }
                        return 0
                    }).publishOn("extraLargePlanCostPerUnit"), c.calculateMonthlyCost = function() {
                        var a = c.selectedPlanCost();
                        return "TWELVE_MONTHS" === c.selectedContract().option ? a = a : "MONTH_BY_MONTH" === c.selectedContract().option && (a += c.selectedContractCost()), c.selectedPlanType() === c.PLAN_TYPES.NBN && (a += c.selectedSpeed() ? c.selectedSpeed().cost : 0), a += c.selectedVoicePlan() ? c.selectedVoicePlan().cost : 0
                    }, c.monthlyCost = ko.computed(c.calculateMonthlyCost), c.calculateTmc = function() {
                        var a = 0,
                            b = 1,
                            d = 60;
                        if ("MONTH_BY_MONTH" === c.selectedContract().option) a = c.monthlyCost() + d;
                        else if ("TWELVE_MONTHS" === c.selectedContract().option && (a = 12 * c.monthlyCost() + b, "undefined" != typeof c.promoCredit().creditType)) switch (c.promoCredit().creditType) {
                            case "FIXED":
                                a -= c.promoCredit().creditAmount * c.promoCredit().creditOccurrence;
                                break;
                            case "BASE":
                                a -= c.selectedPlanCost() * c.promoCredit().creditOccurrence
                        }
                        return console.log("TMC: " + a), a
                    }, c.tmc = ko.computed(c.calculateTmc), c.isContract = ko.computed(function() {
                        return "MONTH_BY_MONTH" !== c.selectedContract().option
                    }), c.plusLineRental = ko.computed(function() {
                        return "DSL" === c.selectedPhoneLine().planType ? !0 : !1
                    }), c.joinEnabled = ko.computed(function() {
                        return c.sqDone() ? c.productType() === c.sqProductType() : !0
                    }), c.sendProductConfigToLivePerson = function() {
                        console.log("sendProductConfigToLivePerson()"), console.log(c), console.log("self.selectedVoicePlan()=" + c.selectedVoicePlan());
                        var b = c.productType(),
                            d = c.selectedPlanSize().planSize,
                            e = c.selectedPlanCost(),
                            f = c.selectedContract().displayName,
                            g = "";
                        c.selectedSpeed() && (g = c.selectedSpeed().displaySpeed);
                        var h = "",
                            i = "";
                        c.selectedVoicePlan() && (h = c.selectedVoicePlan().plan, i = c.selectedVoicePlan().cost);
                        var j = c.tmc();
                        b = "ADSL1" == c.sqTechType() ? "ADSL1" : "ADSL2" == c.sqTechType() ? "ADSL2+" : "NBN", "BUNDLE" == c.selectedPlanType() && (b += " Bundle"), c.sqExtendedNetwork() && (b += " (Extended Network)");
                        var k = b + " - " + d;
                        a.livePerson.productPages.productBuilderContinue(k, e, f, g, h, i, j)
                    }, c.joinClick = function(b, d) {
                        console.log(d);
                        var e = c.mapProductModel();
                        sessionStorage.setItem("productModel", JSON.stringify(e));
                        var f = c.getSqAnalyticsObj();
                        a.adobeAnalytics.pages.productBuilder.joinClick(e, f), c.joinWithPromo("btn-product-builder-join-promo" === d.target.id), c.sqDone() ? c.redirectToPage(c.PAGE_LINKS.JOIN) : (c.saveTempSessionObj(), c.displaySq(c.joinClickSqCallback))
                    }, c.sqEditClick = function() {
                        c.saveTempSessionObj(), c.displaySq(c.handleSqDone);
                        var b = c.getSqAnalyticsObj(),
                            d = c.mapProductModel();
                        a.adobeAnalytics.pages.productBuilder.checkAddressClick(d, b)
                    }, c.checkADSLOptions = function() {
                        c.saveTempSessionObj(), c.redirectToPage(c.PAGE_LINKS.ADSL)
                    }, c.checkNBNOptions = function() {
                        c.saveTempSessionObj(), c.redirectToPage(c.PAGE_LINKS.NBN)
                    }, c.init = function() {
                        c.handleSqDone(), c.setCardVisibility(c.productType()), c.isLoading(!1)
                    }, c.setCardVisibility = function(b) {
                        b === c.PROD_TYPES.NBN ? (c.speedboostCardVisible(!0), c.bindings.speedboostCard || (ko.applyBindings(new a.productBuilder.speedboostCardVM, document.getElementById("ctn-speedboost-card")), c.bindings.speedboostCard = !0)) : c.speedboostCardVisible(!1), b === c.PROD_TYPES.ADSL && c.sqDone() && c.sqOrderType() === c.SQ_ORDER_TYPES.CHURN ? c.phoneLineCardVisible() !== !0 && (c.phoneLineCardVisible(!0), c.bindings.phoneLineCard || (ko.applyBindings(new a.productBuilder.phoneLineCardVM, document.getElementById("ctn-phone-line-card")), c.bindings.phoneLineCard = !0)) : c.phoneLineCardVisible(!1), c.bindings.phoneLineCard = !1, c.selectedPhoneLine({})
                    }, c.selectedSpeedBoostOption = ko.observable().syncWith("selectedSpeedBoostOptionCard", !0);
                    var d = $("#sqDoneHeader");
                    c.handleSqDone = function() {
                        c.selectedPhoneLineOption(c.PLAN_TYPES.BUNDLE);
                        var b = c.loadSqModelFromSession();
                        b && (c.sqDone(!0), d.velocity("scroll", {
                            duration: 500,
                            easing: "ease-out",
                            offset: -70,
                            complete: function() {
                                d.removeClass("highlighted")
                            }
                        }), c.mapSqModel(b), c.setCardVisibility(c.productType()), "FTTN" !== c.sqTechType() && "FTTB" !== c.sqTechType() || "100/40" !== c.selectedSpeedBoostOption() || c.selectedSpeedBoostOption("25/5")), c.setDisplayStickyPrice(), c.setDisplayStickySQ();
                        var e = c.mapProductModel(),
                            f = c.getSqAnalyticsObj();
                        a.adobeAnalytics.pages.productBuilder.closeSq(e, f), !window.location.href.indexOf("#") > 0 && $("#cmp-product-builder").velocity("scroll", {
                            duration: 400,
                            easing: "ease-out",
                            offset: -70
                        })
                    }, c.joinClickSqCallback = function() {
                        c.handleSqDone(), c.sqDone() && (c.isPromoApplied() ? c.promoVM.applyPromoCode(c.promocodeValidateCallback) : c.sqExtendedNetwork() ? $("body,html").velocity("stop").velocity("scroll", {
                            duration: 1e3,
                            easing: "ease-out",
                            offset: -70
                        }) : c.redirectToPage(c.PAGE_LINKS.JOIN))
                    }, c.promocodeValidateCallback = function() {
                        c.isPromoApplied() && c.redirectToPage(c.PAGE_LINKS.JOIN)
                    }, c.displaySq = function(b) {
                        a.sqModal.open({
                            closeCallBack: b
                        })
                    }, c.setDisplayStickyPrice = function() {
                        var a = 70,
                            b = $("#lblProductBuilderMonthlyCost").offset().top,
                            d = b + a - $(window).height() - $(window).scrollTop() < 0;
                        c.displayStickyPrice(DD.bp.is("0, 1365") && c.sqDone() ? !d : !1)
                    }, c.displayStickyPrice = ko.observable(!1), c.setDisplayStickySQ = function() {
                        var a = 0,
                            b = $("#btnProductBuilderSqCheck").offset().top,
                            d = b + a - $(window).scrollTop() >= 0;
                        c.displayStickySQ(DD.bp.is("0, 1365") && !c.sqDone() ? !d : !1)
                    }, c.displayStickySQ = ko.observable(!1), $(window).off("scroll.stickySummary").on("scroll.stickySummary", function() {
                        c.setDisplayStickyPrice(), c.setDisplayStickySQ()
                    }), $(window).off("resize").on("resize", function() {
                        c.setDisplayStickyPrice(), c.setDisplayStickySQ()
                    }), c.stickyPriceScroll = function() {
                        $("#lblProductBuilderMonthlyCost").velocity("stop").velocity("scroll", {
                            duration: 1e3,
                            easing: "ease-out",
                            offset: -200
                        })
                    }, c.loadSqModelFromSession = function() {
                        var a = sessionStorage.getItem("sqModel");
                        return a ? (c.sqModel(JSON.parse(a)), c.sqModel()) : void 0
                    }, c.mapSqModel = function(a) {
                        c.sqTechType(a.broadbandTechType), c.sqAddress(c.constructAddressString(a)), c.sqOrderType(a.orderType), c.sqDone(!0), c.sqExtendedNetwork(a.extendedNetwork)
                    }, c.mapProductModel = function() {
                        var a = {
                            planType: c.selectedPlanType(),
                            planSize: c.selectedPlanSize().planSize,
                            planSpeed: c.selectedPlanType() === c.PLAN_TYPES.NBN && "12/1" !== c.selectedSpeed().speed ? c.selectedSpeed().speed : "",
                            contractTerm: "MONTH_BY_MONTH" !== c.selectedContract().option ? c.selectedContract().option : null,
                            voicePlan: c.selectedVoicePlan() ? c.selectedVoicePlan().plan : null,
                            promotionId: c.isPromoApplied() ? c.promoCode() : "",
                            promotionStaffId: c.isPromoApplied() ? c.promoStaffId() : "",
                            campaignCode: c.isPromoApplied() ? c.campaignCode() : "",
                            calculatedMTC: c.tmc()
                        };
                        return console.log("product model", a), a
                    }, c.constructAddressString = function(a) {
                        var b = (a.serviceSubAddressDetails ? a.serviceSubAddressDetails.trim() + " " : "") + a.serviceStreetNumber.trim() + " " + a.serviceStreet.trim() + " " + a.serviceStreetType.trim() + ", " + a.serviceSuburb.trim() + " " + a.serviceState.trim() + " " + a.servicePostcode.trim();
                        return b
                    }, c.saveTempSessionObj = function() {
                        return sessionStorage.setItem("tempProductBuilder", JSON.stringify({
                            planSize: c.selectedPlanSize().planSize,
                            contractOption: c.selectedContract().option,
                            voicePlan: c.selectedVoicePlan() ? c.selectedVoicePlan().plan : null,
                            promoCode: c.promoCode()
                        }))
                    }, c.getSqAnalyticsObj = function() {
                        var a = "",
                            b = "";
                        return c.sqDone() && (a = c.sqModel().orderType, b = c.sqModel().broadbandTechType), {
                            sqDone: c.sqDone(),
                            sqOrderType: a,
                            sqTechType: b
                        }
                    }, c.redirectToPage = function(a) {
                        c.sendProductConfigToLivePerson(), window.location.href = a
                    }
                }
            }
        }())
    }(BELONG),
    function(a) {
        "use strict";
        a.productBuilder = a.productBuilder || {}, a.productBuilder.promoCodeVM = function() {
            function b(a) {
                return c.applyClicked() && "" !== c.promoCode() && (c.promoErrors([]), c.applyPromoCode()), a
            }
            var c = this;
            c.isAgent = $("#GlobalData").data("isAgent"), c.isApplying = ko.observable(!1), c.isPromoApplied = ko.observable(!1).syncWith("isPromoApplied"), c.applyClicked = ko.observable(!1), c.promoCode = ko.observable("").extend({
                trimAndUppercase: !0
            }).publishOn("promoCode"), c.promoStaffId = ko.observable("").syncWith("promoStaffId"), c.staffPromoAgreeToTC = ko.observable(!1).syncWith("staffPromoAgreeToTC"), c.appliedPromoMessage = ko.observable("").publishOn("promoMessage"), c.promoCodeInSession = ko.observable(""), c.campaignCode = ko.observable("").publishOn("campaignCode"), c.promoCredit = ko.observable({}).publishOn("promoCredit"), c.promoErrors = ko.observableArray([]), c.showReapplyMessage = ko.observable(!1).syncWith("promoShowReapplyMessage"), c.isPromoValid = ko.computed(function() {
                return !c.promoErrors().length
            }), c.productType = ko.observable().subscribeTo("productType", !0), c.sqTechType = ko.observable().subscribeTo("sqTechType", !0), c.sqDone = ko.observable().subscribeTo("sqDone", b), c.selectedPlanSize = ko.observable({}).subscribeTo("selectedPlanSize", b), c.selectedContract = ko.observable({}).subscribeTo("selectedContract", b), c.selectedSpeed = ko.observable({}).subscribeTo("selectedSpeed", b), c.selectedPhoneLine = ko.observable({}).subscribeTo("selectedPhoneLine", b), c.selectedVoicePlan = ko.observable({}).subscribeTo("selectedVoicePlan", b), c.applyButtonState = ko.computed(function() {
                return c.isApplying() ? "loading" : "" === c.promoCode() ? "disabled" : ""
            }), c.promoCode.subscribe(function() {
                c.clearPromoErrors()
            }), c.init = function() {
                var a = sessionStorage.getItem("tempProductBuilder");
                a && (c.promoCodeInSession(JSON.parse(a).promoCode), c.promoCode(c.promoCodeInSession()), c.applyClick())
            }, c.applyClick = function() {
                c.applyClicked(!0), c.clearPromoErrors(), c.applyPromoCode()
            }, c.applyPromoCode = function(a) {
                if (c.showReapplyMessage(!1), c.promoCode()) {
                    c.isApplying(!0);
                    var b = $.get("/bin/belong/join/promo/validate?code=" + c.promoCode().toUpperCase());
                    b.done(function(b) {
                        c.apiValidationPassed(b, a)
                    }), b.fail(function(b) {
                        c.apiValidationFailed(b, a)
                    })
                }
            }, c.apiValidationPassed = function(b, d) {
                c.isApplying(!1), c.isPromoApplied(!1), b.success ? (c.processValidationResults(b.rulesAndResults), c.isPromoValid() ? (!b.promotion.tacTemplate || c.promoStaffId().length > 0 && c.staffPromoAgreeToTC() || c.displayStaffPromoModal(b), c.campaignCode(b.promotion.campaignType), c.isPromoApplied(!0), c.appliedPromoMessage(c.getPromoMessage(b.promotion.campaignType)), b.promotion.credit && c.promoCredit(c.processPromoCredit(b.promotion.credit)), a.adobeAnalytics.pages.productBuilder.applyPromoCodeSuccess(b)) : a.adobeAnalytics.pages.productBuilder.applyPromoCodeFail(b)) : a.adobeAnalytics.pages.productBuilder.applyPromoCodeFail(b), d && d()
            }, c.apiValidationFailed = function(b, d) {
                c.isApplying(!1), c.isPromoApplied(!1);
                var e = b.responseJSON;
                a.adobeAnalytics.pages.productBuilder.applyPromoCodeFail(e), "timeout" === b.statusText ? (console.log("Server Error: timeout"), c.addPromoError("ERROR_TIMEOUT", c.getPromoErrorMessage("ERROR_TIMEOUT"))) : e && e.errors ? $.each(e.errors, function(a, b) {
                    c.addPromoError(b.code, c.getPromoErrorMessage(b.code))
                }) : c.addPromoError("ERROR_DEFAULT", c.getPromoErrorMessage("ERROR_DEFAULT")), d && d()
            }, c.processValidationResults = function(a) {
                $.each(a, function(a, b) {
                    switch (console.log("Processing rule: " + JSON.stringify(b)), b.resultCode) {
                        case "VALIDATION_PASS":
                            break;
                        case "VALIDATION_FAIL":
                            c.addPromoError(b.errorCode, c.getPromoErrorMessage(b.errorCode));
                            break;
                        case "FRONTEND_TO_VALIDATE":
                            c.clientValidationRules[b.rule](b.expectedValues) || c.addPromoError(b.rule, c.getPromoErrorMessage(b.rule));
                            break;
                        case "CANNOT_VALIDATE_BEFORE_SQ":
                            c.addPromoError("CANNOT_VALIDATE_BEFORE_SQ", c.getPromoErrorMessage("CANNOT_VALIDATE_BEFORE_SQ"))
                    }
                })
            }, c.processPromoCredit = function(a) {
                return a.creditType = "FIXED", "undefined" == typeof a.creditAmount ? a.creditType = "BASE" : a.creditAmount = parseInt(a.creditAmount), a.creditOccurrence = parseInt(a.creditOccurrence), a
            }, c.clearPromoCode = function() {
                c.promoCode(""), c.campaignCode(""), c.resetPromoCode()
            }, c.resetPromoCode = function() {
                c.promoStaffId(""), c.isPromoApplied(!1), c.appliedPromoMessage(""), c.promoErrors([]), c.staffPromoAgreeToTC(!1), c.promoCredit({})
            }, c.clearPromoErrors = function() {
                c.promoErrors([]), c.showReapplyMessage(!1)
            }, c.getPromoMessage = function(b) {
                return a.util.getCMSMessage(b, CMS_PROMOCODE.appliedPromoMessages)
            }, c.getPromoErrorMessage = function(b) {
                return a.util.getCMSMessage(b, CMS_PROMOCODE.promoValidation)
            }, c.addPromoError = function(a, b) {
                var d = {
                        code: a,
                        message: b
                    },
                    e = ko.utils.arrayFirst(c.promoErrors(), function(a) {
                        return d.code === a.code
                    });
                e ? c.promoErrors.replace(e, d) : c.promoErrors.push(d)
            }, c.clientValidationRules = {
                PLAN_TYPE: function(a) {
                    return a.indexOf(c.productType()) >= 0
                },
                BROADBAND_TECH_TYPE: function(a) {
                    return a.indexOf(c.sqTechType()) >= 0
                },
                VOICE_OPTION: function(a) {
                    return a.indexOf("ANY_VOICE") >= 0 ? !(!c.selectedVoicePlan() || !c.selectedVoicePlan().plan) : c.selectedVoicePlan() && c.selectedVoicePlan().plan ? a.indexOf(c.selectedVoicePlan().plan) >= 0 : void 0
                },
                SPEEDBOOST_OPTION: function(a) {
                    if (!c.selectedSpeed()) return !1;
                    var b = c.selectedSpeed().speed,
                        d = !1;
                    return $.each(a, function(a, c) {
                        switch (c) {
                            case "SPEEDBOOST_25_5":
                                d = d || "25/5" === b;
                                break;
                            case "SPEEDBOOST_100_40":
                                d = d || "100/40" === b;
                                break;
                            case "ANY_SPEEDBOOST":
                                d = d || "25/5" === b || "100/40" === b
                        }
                    }), d
                },
                CONTRACT_OPTION: function(a) {
                    var b = c.selectedContract().option,
                        d = !1;
                    return $.each(a, function(a, c) {
                        switch (c) {
                            case "CONTRACT":
                                d = d || "MONTH_BY_MONTH" !== b;
                                break;
                            case "NON_CONTRACT":
                                d = d || "MONTH_BY_MONTH" === b
                        }
                    }), d
                },
                USER_TYPE: function(a) {
                    var b = c.isAgent ? "AGENT" : "USER";
                    return a.indexOf(b) >= 0
                },
                PRE_PROVISION_OPTION: function() {
                    return !0
                },
                ADDRESS_WHITELIST: function() {
                    return !0
                },
                START_END_DATE: function() {
                    return !0
                },
                IAF: function() {
                    return !0
                }
            }, c.displayStaffPromoModal = function(b) {
                var d = new a.productBuilder.staffPromoModalVM(c.resetPromoCode);
                $.magnificPopup.open({
                    items: {
                        src: "#product-staff-promo-tc",
                        type: "inline"
                    },
                    alignTop: !0,
                    showCloseBtn: !0,
                    closeOnBgClick: !0,
                    overflowY: "auto",
                    callbacks: {
                        open: function() {
                            ko.applyBindings(d, document.getElementById("product-staff-promo-tc")), d.onOpen(b), a.forms.applyShiftLabel()
                        },
                        beforeClose: function() {
                            d.beforeClose()
                        },
                        close: function() {
                            ko.cleanNode(document.getElementById("product-staff-promo-tc"))
                        }
                    }
                })
            }
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.productBuilder = a.productBuilder || {}, a.productBuilder.speedboostCardVM = function() {
            var b = this;
            b.STATES = {
                SELECT: "SELECT",
                EDIT: "EDIT"
            };
            var c = {
                TWELVEOVERONE: "12/1",
                TWENTYFIVEOVERFIVE: "25/5",
                HUNDREDOVERFORTY: "100/40"
            };
            b.sqTechType = ko.observable().subscribeTo("sqTechType", !0), b.speedboostOptions = ko.computed(function() {
                var a = [];
                return $.each(PRODUCT_BUILDER_SPEED_BOOSTS, function(d, e) {
                    e.speed === c.HUNDREDOVERFORTY && "FTTB" !== b.sqTechType() && "FTTN" !== b.sqTechType() ? a.push(e) : e.speed !== c.HUNDREDOVERFORTY && a.push(e)
                }), a
            }), b.selectedSpeedBoostOptionCard = ko.observable(c.TWELVEOVERONE).syncWith("selectedSpeedBoostOptionCard"), b.selectedSpeedBoostOptionModal = ko.observable().syncWith("selectedSpeedBoostOptionModal", !0), b.selectedSpeedObject = ko.computed(function() {
                for (var a = 0; a < PRODUCT_BUILDER_SPEED_BOOSTS.length; a++)
                    if (PRODUCT_BUILDER_SPEED_BOOSTS[a].speed === b.selectedSpeedBoostOptionCard()) return PRODUCT_BUILDER_SPEED_BOOSTS[a];
                return null
            }).publishOn("selectedSpeed"), b.priceUpdate = ko.computed(function() {
                return b.selectedSpeedObject().cost
            }), b.currentState = ko.observable(b.STATES.SELECT).subscribeTo("speedBoostCardState"), b.selected = ko.observable(!1), b.openModal = function() {
                a.adobeAnalytics.pages.productBuilder.speedboostModalOpen(), b.selectedSpeedBoostOptionModal(b.selectedSpeedBoostOptionCard()), a.productBuilder.speedboostModal()
            }, b.selectedSpeedBoostOptionCard.subscribe(function(b) {
                a.adobeAnalytics.pages.productBuilder.speedboostSelection(b)
            })
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.productBuilder = a.productBuilder || {}, a.productBuilder.speedBoostModalVM = function() {
            var b = this;
            b.STATES = {
                SELECT: "SELECT",
                EDIT: "EDIT"
            };
            var c = {
                TWELVEOVERONE: "12/1",
                TWENTYFIVEOVERFIVE: "25/5",
                HUNDREDOVERFORTY: "100/40"
            };
            b.currentCardState = ko.observable(b.STATES.SELECT).publishOn("speedBoostCardState"), b.sqTechType = ko.observable().subscribeTo("sqTechType", !0), b.speedboostOptions = ko.computed(function() {
                var a = [];
                return $.each(PRODUCT_BUILDER_SPEED_BOOSTS, function(d, e) {
                    e.speed !== c.TWELVEOVERONE && (e.speed === c.HUNDREDOVERFORTY && "FTTB" !== b.sqTechType() && "FTTN" !== b.sqTechType() ? a.push(e) : e.speed === c.TWENTYFIVEOVERFIVE && a.push(e));

                }), a
            }), b.selectedSpeedBoostOptionCard = ko.observable().syncWith("selectedSpeedBoostOptionCard", !0), b.selectedSpeedBoostOptionModal = ko.observable("").syncWith("selectedSpeedBoostOptionModal"), b.buttonState = ko.computed(function() {
                return "" === b.selectedSpeedBoostOptionModal() || "12/1" === b.selectedSpeedBoostOptionModal() ? "disabled" : ""
            }), b.modalCancel = function() {
                a.adobeAnalytics.pages.productBuilder.speedboostModalClose(), $.magnificPopup.close()
            }, b.updateSelection = function() {
                a.adobeAnalytics.pages.productBuilder.speedboostModalClose(), "disabled" != b.buttonState() && (b.currentCardState(b.STATES.EDIT), b.selectedSpeedBoostOptionCard(b.selectedSpeedBoostOptionModal()), $.magnificPopup.close())
            }, b.selectedSpeedBoostOptionModal.subscribe(function(b) {
                a.adobeAnalytics.pages.productBuilder.speedboostModalSelection(b)
            })
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.productBuilder = a.productBuilder || {}, a.productBuilder.staffPromoModalVM = function(b) {
            var c = this;
            c.cancelCallback = b, c.tcIsLoading = ko.observable(!0), c.tcContent = ko.observable(""), c.staffId = ko.observable("").syncWith("promoStaffId", !0), c.agreeToTC = ko.observable(!1).syncWith("staffPromoAgreeToTC", !0), c.cancelling = !0, c.validator = null, c.validationRules = {
                rules: {
                    "staff-id": {
                        required: !0
                    },
                    "chk-staff-promo-tc": {
                        required: !0
                    }
                }
            }, c.continueClick = function() {
                a.validation.validateForm(c.validator) && (c.cancelling = !1, a.adobeAnalytics.pages.productBuilder.applyStaffPromoDetails(), $.magnificPopup.close())
            }, c.cancel = function() {
                a.adobeAnalytics.pages.productBuilder.cancelStaffPromoDetails(), $.magnificPopup.close()
            }, c.populateStaffPromoTandC = function(b) {
                $.ajax("/apps/belong/promotion/termsAndConditions." + b + ".html", {
                    dataType: "text"
                }).then(function(b) {
                    c.tcContent(b), c.tcIsLoading(!1), a.belongModal.setupAutoResize({
                        modalContainer: $("#product-staff-promo-tc"),
                        nameSpace: "modalAutoResizeStaffPromo"
                    })
                })
            }, c.onOpen = function(b) {
                c.populateStaffPromoTandC(b.promotion.tacTemplate), c.validator = a.validation.setupFormValidation("product-staff-promo-tc-fields", $(".product-staff-promo-tc-fields"), c.validationRules), c.cancelling = !0, a.belongModal.setupAutoResize({
                    modalContainer: $("#product-staff-promo-tc"),
                    nameSpace: "modalAutoResizeStaffPromo"
                })
            }, c.beforeClose = function() {
                c.cancelling && (c.staffId(""), c.agreeToTC(!1), a.validation.resetForm(c.validator), c.cancelCallback())
            }
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.productBuilder = a.productBuilder || {}, a.productBuilder.voiceCardVM = function() {
            function b() {
                var b = c.getVoicePlanForSelection(c.nationalCheckedModal(), c.internationalCheckedModal());
                a.adobeAnalytics.pages.productBuilder.voiceModalSelection(b)
            }
            var c = this;
            c.STATES = {
                SELECT: "SELECT",
                EDIT: "EDIT"
            }, c.currentState = ko.observable(c.STATES.SELECT).subscribeTo("voiceCardState"), c.nationalCheckedCard = ko.observable(!1).syncWith("nationalCheckedCard", !0), c.internationalCheckedCard = ko.observable(!1).syncWith("internationalCheckedCard", !0), c.nationalCheckedModal = ko.observable().syncWith("nationalCheckedModal", !0), c.internationalCheckedModal = ko.observable().syncWith("internationalCheckedModal", !0), c.getVoicePlanForSelection = function(a, b) {
                return a && b ? "BOTH" : c.nationalCheckedCard() ? "NATIONAL" : c.internationalCheckedCard() ? "INTERNATIONAL" : ""
            }, c.selectedVoicePlan = ko.computed(function() {
                for (var a = 0; a < PRODUCT_BUILDER_VOICE_PLANS.length; a++)
                    if (PRODUCT_BUILDER_VOICE_PLANS[a].plan === c.getVoicePlanForSelection(c.nationalCheckedCard(), c.internationalCheckedCard())) return PRODUCT_BUILDER_VOICE_PLANS[a];
                return null
            }).publishOn("selectedVoicePlan"), c.priceUpdate = ko.computed(function() {
                return c.selectedVoicePlan() ? c.selectedVoicePlan().cost : 0
            }), c.displayModal = function() {
                a.adobeAnalytics.pages.productBuilder.voiceModalOpen(), c.nationalCheckedModal(c.nationalCheckedCard()), c.internationalCheckedModal(c.internationalCheckedCard()), a.productBuilder.voiceModal()
            }, c.selectedVoicePlan.subscribe(function(b) {
                a.adobeAnalytics.pages.productBuilder.voiceSelection(b)
            }), c.nationalCheckedModal.subscribe(b), c.internationalCheckedModal.subscribe(b);
            var d = sessionStorage.getItem("tempProductBuilder");
            if (d) {
                var e = JSON.parse(d).voicePlan;
                if (!e) return;
                c.currentState("EDIT"), "BOTH" === e ? (c.nationalCheckedCard(!0), c.internationalCheckedCard(!0)) : "NATIONAL" === e ? c.nationalCheckedCard(!0) : "INTERNATIONAL" === e && c.internationalCheckedCard(!0)
            }
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.productBuilder = a.productBuilder || {}, a.productBuilder.voiceModalVM = function() {
            var b = this;
            b.STATES = {
                SELECT: "SELECT",
                EDIT: "EDIT"
            }, b.currentCardState = ko.observable(b.STATES.SELECT).publishOn("voiceCardState"), b.nationalCheckedCard = ko.observable().syncWith("nationalCheckedCard", !0), b.internationalCheckedCard = ko.observable().syncWith("internationalCheckedCard", !0), b.nationalCheckedModal = ko.observable(!1).syncWith("nationalCheckedModal", !0), b.internationalCheckedModal = ko.observable(!1).syncWith("internationalCheckedModal", !0), b.modalCancel = function() {
                a.adobeAnalytics.pages.productBuilder.voiceModalClose(), $.magnificPopup.close()
            }, b.buttonState = ko.computed(function() {
                return b.nationalCheckedCard() || b.internationalCheckedCard() ? "" : b.nationalCheckedModal() || b.internationalCheckedModal() ? "" : "disabled"
            }), b.updateSelection = function() {
                a.adobeAnalytics.pages.productBuilder.voiceModalClose(), "disabled" !== b.buttonState() && (b.currentCardState(b.STATES.EDIT), b.nationalCheckedCard(b.nationalCheckedModal()), b.internationalCheckedCard(b.internationalCheckedModal()), $.magnificPopup.close())
            }
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.product = function() {
            var b, c = !a.util.isPrivateBrowsingMode(),
                d = window.location.pathname,
                e = d.split("/")[1],
                f = e.split(".")[0];
            return {
                init: function() {
                    if (b = this, $(".main.product-page").each(function() {
                            $(".join-cta .ctab").off("click.productJoinNow").on("click.productJoinNow", b.events.productJoinNow)
                        }), $("#contract-switch").is(":checked") ? b.set.planSwitchOn() : $("#contract-switch").not(":checked") && b.set.planSwitchOff(), c && sessionStorage.getItem("sqDone")) {
                        var d = sessionStorage.getItem("product");
                        d !== $(".sq-container").data("sqType") && a.sqEntry.hide.productSelection()
                    }
                    "nbn" !== f && $(".bc-lime .product-allowance").removeClass("offer"), "nbnrapid" === f && ($(".contract").addClass("hidden"), $(".non-contract").removeClass("hidden"), $(".bc-dark-green .contract").addClass("hidden"), $(".bc-dark-green .non-contract").removeClass("hidden"), $(".details .contract").removeClass("hidden"), $(".details .non-contract").addClass("hidden"))
                },
                setPlanTypeAndSize: function(b) {
                    if (c) {
                        switch (a.sqEntry.get.sourceType()) {
                            case "ADSL":
                                a.join.model.product.planType = "BUNDLE";
                                break;
                            case "NBN":
                                a.join.model.product.planType = "NBN"
                        }
                        "undefined" != typeof b.data("productSize") && (a.join.model.product.planSize = b.data("productSize")), sessionStorage.setItem("productModel", JSON.stringify(a.join.model.product))
                    }
                },
                events: {
                    productJoinNow: function(d) {
                        var e, f = "";
                        if (d.preventDefault(), c ? (e = sessionStorage.getItem("sqDone") || "false", f = sessionStorage.getItem("resultCode") || "") : (f = a.join.model.sq.orderType, e = "" === f ? "false" : "true"), b.setPlanTypeAndSize($(this)), "false" === e) $(".sq-container").velocity("stop").velocity("scroll", {
                            duration: 400,
                            complete: function() {
                                $("#sq-street-address").focus()
                            }
                        });
                        else {
                            var g = "/join";
                            "Join" === $(this).text() && (a.adobeAnalytics.global.sq.findOutMoreOrJoinNow(a.sqEntry.get.sourceType(), "Product-List-Join", f, "standard"), a.analytics.methods.sq.findOutMoreOrJoinNow(g, a.sqEntry.get.sourceType(), "Product-List-Join", f))
                        }
                    }
                },
                set: {
                    planSwitchOn: function() {
                        $(".bc-lime .product-allowance").removeClass("offer"), $(".contract").addClass("hidden"), $(".non-contract").removeClass("hidden"), $(".product-options").find("li").first().removeClass("selected"), $(".product-options").find("li").last().addClass("selected"), console.log("Month by Month selected");
                        var b = "Month-by-month",
                            c = "Product-List-Options",
                            d = "Toggle";
                        a.analytics.methods.linkClickWithNonPageReload(d, c, b)
                    },
                    planSwitchOff: function() {
                        "nbn" === f && $(".bc-lime .product-allowance").addClass("offer"), $(".contract").removeClass("hidden"), $(".non-contract").addClass("hidden"), $(".product-options").find("li").first().addClass("selected"), $(".product-options").find("li").last().removeClass("selected"), console.log("Contract Option selected");
                        var b = "Contract",
                            c = "Product-List-Options",
                            d = "Toggle";
                        a.analytics.methods.linkClickWithNonPageReload(d, c, b)
                    }
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.promoCodeModal = function() {
            var a;
            return {
                open: function(b, c) {
                    a = this, c = c || {}, c.ctaPreset = c.ctaPreset || "", c.ctaCallback = c.ctaCallback || function() {}, "open-sq-modal" == c.ctaPreset && (c.ctaCallback = function() {
                        setTimeout(function() {
                            BELONG.sqModal.open()
                        }, 500)
                    }), $.magnificPopup.open({
                        items: {
                            src: "#" + b,
                            type: "inline"
                        },
                        alignTop: !0,
                        showCloseBtn: !0,
                        closeOnBgClick: !0,
                        overflowY: "auto",
                        callbacks: {
                            open: function() {
                                BELONG.belongModal.setupAutoResize({
                                    modalContainer: $("#" + b),
                                    nameSpace: "modalAutoResizePromo" + b
                                })
                            },
                            close: function() {}
                        }
                    }), $("#" + b + " .belong-modal__continue-link").off("click.close" + b).on("click.close" + b, function(a) {
                        a.preventDefault(), $.magnificPopup.close(), c.ctaCallback()
                    })
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.sqLauncher = function() {
            var b;
            return {
                init: function() {
                    return b = this, 0 === $("#sq-launcher").length ? !1 : (console.log("whut"), void $("#sq-launcher-btn, #nav-join").off("click.sqLauncherBtn").on("click.sqLauncherBtn", b.events.openSq))
                },
                events: {
                    openSq: function(b) {
                        b.preventDefault();
                        var c = null !== sessionStorage.getItem("product") ? sessionStorage.getItem("product") : "";
                        "adsl" === c ? window.location.replace("/adsl") : "nbn" === c ? window.location.replace("/nbn") : (a.sqModal.open(), a.adobeAnalytics.global.sq.openModal())
                    }
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.sqModal = a.sqModal || {}, a.sqModal.eoiFormVM = function() {
            var b = this;
            b.state = ko.observable("NONE"), b.contactFields = {
                name: ko.observable(""),
                streetAddress: ko.observable("").subscribeTo("sqModalSelectedAddress"),
                mobile: ko.observable(""),
                email: ko.observable("")
            }, b.fullFields = {
                name: ko.observable(""),
                streetAddress: ko.observable("").subscribeTo("sqModalSelectedAddress"),
                mobile: ko.observable(""),
                email: ko.observable(""),
                landline: ko.observable(""),
                preferredContant: ko.observable("")
            }, b.eoiMessage = ko.observable("NONE"), b.eoiSuccessMessage = ko.observable("NONE"), b.eoiBtnState = ko.observable("active"), b.createEOI = function() {
                if (!a.validation.validateForm(a.eoiValidator)) return !1;
                var c, d = b.getEoiRequestObject(b.state().toLowerCase());
                console.log("requestData=" + JSON.stringify(d)), b.eoiBtnState("loading"), c = $.post("/apps/telstraEveServlet", d), c.done(function(c) {
                    var d = c.success ? "success" : "fail";
                    b.eoiMessage("NONE"), b.eoiSuccessMessage("NONE"), console.log("result:", c), console.log("eoiResult", "formType: " + b.state().toLowerCase() + " |", "resultStatus: " + d), a.sqModal.sqRules.eoiResult[b.state().toLowerCase()][d]()
                }), c.fail(function(a) {
                    console.log("fail serviceQualifyRequest", a)
                }), c.always(function() {
                    b.eoiBtnState("active")
                })
            }, b.setupContactForm = function() {
                b.state("CONTACT"), b.eoiMessage("contact-form")
            }, b.resetSQ = function() {
                b.state("NONE"), a.sqFormVM.resetForm()
            }, b.state.subscribe(function(c) {
                switch (c) {
                    case "FULL":
                        b.fullFields.streetAddress($("#sq-street-address").val()), a.forms.applyShiftLabelHandler("#eoi-form-full-address");
                        break;
                    case "CONTACT":
                        b.contactFields.streetAddress($("#sq-street-address").val()), a.forms.applyShiftLabelHandler("#eoi-form-contact-address");
                        break;
                    case "NONE":
                        b.eoiMessage("NONE"), b.eoiSuccessMessage("NONE")
                }
                setTimeout(function() {
                    a.belongModal.setHeights({
                        modalContainer: $("#mdl-sq"),
                        noShadow: !0
                    })
                }, 100)
            }), b.eoiValidation = function() {
                a.eoiValidator = a.validation.setupFormValidation("eoi-form", $("#eoi-form"), {
                    rules: {
                        "eoi-form-contact-name": {
                            required: !0,
                            maxlength: 255,
                            disallowedCharacters: !0
                        },
                        "eoi-form-contact-email": {
                            required: !0,
                            backendEmail: !0,
                            disallowedCharacters: !0,
                            maxlength: 255
                        },
                        "eoi-form-contact-mobile": {
                            required: !0,
                            australianMobileNumber: !0,
                            disallowedCharacters: !0
                        },
                        "eoi-form-full-name": {
                            required: !0,
                            disallowedCharacters: !0
                        },
                        "eoi-form-full-address": {
                            required: !0,
                            disallowedCharacters: !0
                        },
                        "eoi-form-full-landline": {
                            required: function() {
                                return "LANDLINE" === $("#eoi-form-full-contact-method").val()
                            },
                            australianPhoneNumber: !0
                        },
                        "eoi-form-full-mobile": {
                            required: function() {
                                return "MOBILE" === $("#eoi-form-full-contact-method").val()
                            },
                            australianMobileNumber: !0,
                            disallowedCharacters: !0
                        },
                        "eoi-form-full-email": {
                            required: function() {
                                return "EMAIL" === $("#eoi-form-full-contact-method").val()
                            },
                            disallowedCharacters: !0
                        },
                        "eoi-form-full-contact-method": {
                            required: function() {
                                return "" === $("#eoi-form-full-contact-method").val()
                            }
                        }
                    }
                }), a.eoiValidator.resetForm()
            }, b.eoiValidation(), b.getEoiRequestObject = function(c) {
                switch (c) {
                    case "contact":
                        return {
                            name: b.contactFields.name(),
                            contactEmail: b.contactFields.email(),
                            contactMobile: a.validation.helpers.cleanAustralianPhoneNumber(b.contactFields.mobile()),
                            contactLandline: "",
                            contactMethod: "EMAIL",
                            freeTextAddress: b.contactFields.streetAddress(),
                            eoiReason: "ADDRESS_HELP_REQUIRED",
                            sourceType: a.sqRequest.get.pageSource(),
                            requestType: "createEOI"
                        };
                    default:
                        return {
                            name: b.fullFields.name(),
                            contactEmail: b.fullFields.email(),
                            contactMobile: a.validation.helpers.cleanAustralianPhoneNumber(b.fullFields.mobile()),
                            contactLandline: a.validation.helpers.cleanAustralianPhoneNumber(b.fullFields.landline()),
                            contactMethod: $("#eoi-form-full-contact-method").find("option:selected").val(),
                            freeTextAddress: b.fullFields.streetAddress(),
                            eoiReason: "ADDR_NOT_IN_WHITELIST",
                            sourceType: a.sqRequest.get.pageSource(),
                            requestType: "createEOI"
                        }
                }
            }
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.sqModal = a.sqModal || {}, a.sqModal.sqFormVM = function() {
            var b = this;
            b.state = ko.observable("ADDRESS_ENTRY"), b.redirected = ko.observable("redirected" === a.util.loadPageVar("sq_redirect")), b.entryMessage = ko.observable("NONE"), b.successMessage = ko.observable("NONE"), b.redirectMessage = ko.observable("NONE"), b.errorMessage = ko.observable("NONE"), b.selectedAddress = ko.observable("").syncWith("sqModalSelectedAddress"), b.hasRunStructuredAddress = ko.observable(!1), b.checkAddressButton = {
                show: ko.observable(!0),
                text: ko.observable("Check Address")
            }, b.setupContactForm = function() {
                b.state("EOI_ENTRY"), a.eoiFormVM.setupContactForm()
            }, b.setupStructuredAddressForm = function() {
                b.state("STRUCTURED_ADDRESS"), a.structuredAddressVM.setupStructuredAddressForm(), a.adobeAnalytics.global.sq.structuredCantFindAddress(), b.hasRunStructuredAddress(!0)
            }, b.hideAllMessages = function() {
                b.entryMessage("NONE"), b.successMessage("NONE"), b.errorMessage("NONE")
            }, b.displaySuccess = function(a) {
                b.state("ADDRESS_FOUND"), b.redirected(!1), b.successMessage(a)
            }, b.resetForm = function() {
                b.state("ADDRESS_ENTRY"), b.hideAllMessages(), a.CLEARSQONSUBMIT = !0, a.LANDLINECHECK = !1, a.REFINEDADDRESS = !1, b.hasRunStructuredAddress(!1), $("#sq-street-address").val("").blur(), a.forms.unShiftLabelHandler("sq-street-address"), $("label.error").hide()
            }, b.redirectUser = function(c) {
                c = c || {
                    delay: 500
                }, c.delay = c.delay || 500, b.redirectMessage(c.product), b.state("REDIRECTING"), setTimeout(function() {
                    a.belongModal.setHeights({
                        modalContainer: $("#mdl-sq"),
                        noShadow: !0
                    })
                }, 10), setTimeout(function() {
                    window.location.replace(c.pageUrl)
                }, c.delay)
            }, b.continueAndClose = function() {
                $.magnificPopup.proto.close.call(this)
            }, b.landlineNumberRequired = ko.observable(""), b.landlineNumberRequired.subscribe(function(c) {
                b.checkAddressButton.show("" !== c), setTimeout(function() {
                    a.belongModal.setHeights({
                        modalContainer: $("#mdl-sq"),
                        noShadow: !0
                    })
                }, 100)
            }), b.state.subscribe(function(c) {
                switch (a.infiniteCycle.destroy($(".sq-form__loading-messages")), c) {
                    case "ADDRESS_ENTRY":
                        b.checkAddressButton.show(!0), b.selectedAddress(""), b.checkAddressButton.text("Check Address");
                        break;
                    case "ADDRESS_SELECT":
                        $(".street-address-select select").off("change.refineaddress").on("change.refineaddress", a.sqRequest.changeAddressSelect), $(".street-address-select .simple-label").text($(".street-address-select select option").eq(0).text()), b.entryMessage("MORE_ADDRESSES"), b.checkAddressButton.show(!0), b.checkAddressButton.text("Check Address"), b.hasRunStructuredAddress() && a.adobeAnalytics.global.sq.structuredFoundListOfRefinedAddresses(a.structuredAddressVM.structuredAddress.type());
                        break;
                    case "ADDRESS_FOUND":
                        "" === a.join.model.sq.serviceAddressId && a.sqRequest.get.fromSession(), b.selectedAddress(a.join.model.helpers.getAddressString()), b.checkAddressButton.show(!1), b.hasRunStructuredAddress() && a.adobeAnalytics.global.sq.structuredFoundMatch(a.structuredAddressVM.structuredAddress.type());
                        break;
                    case "LANDLINE_CHECK":
                        a.LANDLINECHECK = !0, b.checkAddressButton.show("" !== b.landlineNumberRequired()), b.selectedAddress(a.join.model.helpers.getAddressString()), b.checkAddressButton.text("Continue");
                        break;
                    case "LOADING":
                        a.infiniteCycle.create($(".sq-form__loading-messages")), b.hideAllMessages(), b.checkAddressButton.show(!1);
                        break;
                    case "EOI_ENTRY":
                        b.checkAddressButton.show(!1);
                        break;
                    case "STRUCTURED_ADDRESS":
                        b.checkAddressButton.show(!1)
                }
                "EOI_ENTRY" !== c && a.eoiFormVM.state("NONE"), "STRUCTURED_ADDRESS" !== c && a.structuredAddressVM.state("NONE"), setTimeout(function() {
                    a.belongModal.setHeights({
                        modalContainer: $("#mdl-sq"),
                        noShadow: !0
                    })
                }, 100)
            }), b.serviceQualify = function(b) {
                var c, d, e = {
                        phoneNumber: a.join.model.sq.phoneNumber,
                        serviceAddressId: a.join.model.sq.serviceAddressId,
                        serviceExchange: a.join.model.sq.serviceExchange,
                        serviceStreetNumber: a.join.model.sq.serviceStreetNumber,
                        serviceStreet: a.join.model.sq.serviceStreet,
                        serviceStreetType: a.join.model.sq.serviceStreetType,
                        serviceSuburb: a.join.model.sq.serviceSuburb,
                        serviceState: a.join.model.sq.serviceState,
                        servicePostcode: a.join.model.sq.servicePostcode,
                        serviceSubAddressDetails: a.join.model.sq.serviceSubAddressDetails,
                        bpsError: a.join.model.sq.bpsError,
                        sqSource: "nbnrapid" === $("#header").attr("data-page-sub-type") ? "rapid" : ""
                    },
                    f = function(b) {
                        a.sqRequest.processResult(b, "success", "adsl")
                    },
                    g = function(b) {
                        a.sqRequest.processResult(b, "success", "nbn")
                    },
                    h = function(b) {
                        a.sqRequest.processResult(b.responseJSON, "fail", "adsl")
                    },
                    i = function(b) {
                        a.sqRequest.processResult(b.responseJSON, "fail", "nbn")
                    };
                "BPS" === b ? (c = $.post("/bin/belong/sq/nbn", e), c.done(g), c.fail(i)) : (d = $.post("/bin/belong/sq/adsl", e), d.done(f), d.fail(h))
            };
            var c = $("#sq-street-address"),
                d = $("#sq-check-address-structured");
            c.on("keyup", function() {
                setTimeout(function() {
                    "" !== c.val() && d.removeClass("hidden")
                }, 1500)
            }), c.on("blur", function() {
                "" === c.val() && d.addClass("hidden")
            })
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.sqModal = a.sqModal || {}, a.sqModal.sqRules = function() {
            return {
                addressResult: {
                    multiple: function() {
                        a.sqFormVM.state("ADDRESS_SELECT")
                    },
                    single: function() {
                        a.livePerson.sq.startSQ(), a.sqFormVM.serviceQualify(a.LANDLINECHECK ? "TIAB" : "BPS")
                    },
                    none: function() {
                        console.log("EXPERIAN_ERROR"), a.sqFormVM.state("EOI_ENTRY"), a.eoiFormVM.state("NO_FORM"), a.eoiFormVM.eoiMessage("experian-error"), a.eoiFormVM.contactFields.streetAddress($("#sq-street-address").val()), a.adobeAnalytics.global.sq.failExperionError(a.sqRequest.get.pageSource(), "modal"), a.analytics.methods.sqFailures.experionError(a.sqRequest.get.pageSource()), a.livePerson.sq.addressNotFound()
                    }
                },
                sqResult: {
                    nbn: {
                        NBN_SERVICE_CLASS_ONE: function() {
                            console.log("nbn NBN_SERVICE_CLASS_ONE"), a.sqFormVM.redirected() ? a.sqFormVM.displaySuccess("nbn") : $.magnificPopup.proto.close.call(this)
                        },
                        NBN_SERVICE_CLASS_TWO: function() {
                            console.log("nbn NBN_SERVICE_CLASS_TWO"), a.sqFormVM.redirected() ? a.sqFormVM.displaySuccess("nbn") : $.magnificPopup.proto.close.call(this)
                        },
                        NBN_SERVICE_CLASS_THREE: function() {
                            console.log("nbn NBN_SERVICE_CLASS_THREE"), a.sqFormVM.redirected() ? a.sqFormVM.displaySuccess("nbn") : $.magnificPopup.proto.close.call(this)
                        },
                        NBN_SERVICE_CLASS_ELEVEN: function() {
                            console.log("nbn-fttn NBN_SERVICE_CLASS_ELEVEN"), a.sqFormVM.displaySuccess("nbn-fttn")
                        },
                        NBN_SERVICE_CLASS_TWELVE: function(b) {
                            console.log("nbn-" + b.broadbandTechType + " NBN_SERVICE_CLASS_TWELVE"), a.sqFormVM.displaySuccess("nbn-" + b.broadbandTechType)
                        },
                        NBN_SERVICE_CLASS_THIRTEEN: function(b) {
                            console.log("nbn-" + b.broadbandTechType + " NBN_SERVICE_CLASS_THIRTEEN"), a.sqFormVM.displaySuccess("nbn-" + b.broadbandTechType)
                        },
                        SQ_ERROR: function(b) {
                            console.log("nbn SQ_ERROR"), console.log("bpsError: true"), a.join.model.sq.bpsError = !0, a.LANDLINECHECK = !0, a.sqFormVM.state("LANDLINE_CHECK"), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("SQ_ERROR", a.sqRequest.get.pageSource(), "nbn", "modal"), a.analytics.methods.sqFailures.failureMessages("SQ_ERROR", a.sqRequest.get.pageSource(), "nbn")), a.livePerson.sq.sqError()
                        },
                        TWICE_ORDER: function(b) {
                            console.log("nbn TWICE_ORDER"), a.sqFormVM.state("EOI_ENTRY"), a.eoiFormVM.state("NO_FORM"), a.eoiFormVM.eoiMessage("duplicate-address"), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("TWICE_ORDER", a.sqRequest.get.pageSource(), "nbn", "modal"), a.analytics.methods.sqFailures.failureMessages("TWICE_ORDER", a.sqRequest.get.pageSource(), "nbn")), a.livePerson.sq.cannotBelongError()
                        },
                        SERVICE_NOT_AVAILABLE: function() {
                            console.log("nbn SERVICE_NOT_AVAILABLE"), a.LANDLINECHECK = !0, a.sqFormVM.state("LANDLINE_CHECK")
                        },
                        ADDR_NOT_IN_WHITELIST: function(b) {
                            console.log("nbn ADDR_NOT_IN_WHITELIST"), a.sqFormVM.state("EOI_ENTRY"), a.eoiFormVM.state("FULL"), a.eoiFormVM.eoiMessage("service-error"), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("ADDR_NOT_IN_WHITELIST", a.sqRequest.get.pageSource(), "nbn", "modal"), a.analytics.methods.sqFailures.failureMessages("ADDR_NOT_IN_WHITELIST", a.sqRequest.get.pageSource(), "nbn")), a.livePerson.sq.cannotBelongError()
                        }
                    },
                    adsl: {
                        NEW_ORDER: function(b) {
                            console.log("adsl NEW_ORDER"), a.sqFormVM.redirected() || "adsl1" === b.broadbandTechType ? a.sqFormVM.displaySuccess(b.broadbandTechType + "-new") : $.magnificPopup.proto.close.call(this)
                        },
                        PORT_ORDER: function(b) {
                            console.log("adsl PORT_ORDER"), a.sqFormVM.displaySuccess(b.broadbandTechType + "-port")
                        },
                        CHURN_ORDER: function(b) {
                            console.log("adsl CHURN_ORDER"), a.sqFormVM.redirected() || "adsl1" === b.broadbandTechType ? a.sqFormVM.displaySuccess(b.broadbandTechType + "-churn") : $.magnificPopup.proto.close.call(this)
                        },
                        SQ_ERROR: function(b) {
                            console.log("adsl SQ_ERROR"), a.sqFormVM.state("EOI_ENTRY"), a.eoiFormVM.state("FULL"), a.eoiFormVM.eoiMessage("sq-error"), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("SQ_ERROR", a.sqRequest.get.pageSource(), "adsl", "modal"), a.analytics.methods.sqFailures.failureMessages("SQ_ERROR", a.sqRequest.get.pageSource(), "adsl")), a.livePerson.sq.sqError()
                        },
                        TIAB_ERROR: function(b) {
                            console.log("adsl SQ_ERROR"), a.sqFormVM.state("EOI_ENTRY"), a.eoiFormVM.state("FULL"), a.eoiFormVM.eoiMessage("sq-error"), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("TIAB_ERROR", a.sqRequest.get.pageSource(), "adsl", "modal"), a.analytics.methods.sqFailures.failureMessages("TIAB_ERROR", a.sqRequest.get.pageSource(), "adsl")), a.livePerson.sq.sqError()
                        },
                        TWICE_ORDER: function(b) {
                            console.log("adsl TWICE_ORDER"), a.sqFormVM.state("EOI_ENTRY"), a.eoiFormVM.state("NO_FORM"), a.eoiFormVM.eoiMessage("duplicate-address"), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("TWICE_ORDER", a.sqRequest.get.pageSource(), "adsl", "modal"), a.analytics.methods.sqFailures.failureMessages("TWICE_ORDER", a.sqRequest.get.pageSource(), "adsl")), a.livePerson.sq.cannotBelongError()
                        },
                        INVALID_ADDRESS: function(b) {
                            console.log("adsl INVALID_ADDRESS"), a.LANDLINECHECK = !0, a.sqFormVM.state("LANDLINE_CHECK"), a.sqFormVM.entryMessage("INCORRECT_PHONE"), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("INVALID_ADDRESS", a.sqRequest.get.pageSource(), "adsl", "modal"), a.analytics.methods.sqFailures.failureMessages("INVALID_ADDRESS", a.sqRequest.get.pageSource(), "adsl")), a.livePerson.sq.invalidPhoneError()
                        },
                        SERVICE_NOT_AVAILABLE: function(b) {
                            console.log("adsl SERVICE_NOT_AVAILABLE"), a.sqFormVM.state("EOI_ENTRY"), a.eoiFormVM.state("FULL"), a.eoiFormVM.eoiMessage("service-error"), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("SERVICE_NOT_AVAILABLE", a.sqRequest.get.pageSource(), "adsl", "modal"), a.analytics.methods.sqFailures.failureMessages("SERVICE_NOT_AVAILABLE", a.sqRequest.get.pageSource(), "adsl")), a.livePerson.sq.completeSQ()
                        },
                        ADDR_NOT_IN_WHITELIST: function(b) {
                            console.log("adsl ADDR_NOT_IN_WHITELIST"), a.sqFormVM.state("EOI_ENTRY"), a.eoiFormVM.state("FULL"), a.eoiFormVM.eoiMessage("service-error"), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("ADDR_NOT_IN_WHITELIST", a.sqRequest.get.pageSource(), "adsl", "modal"), a.analytics.methods.sqFailures.failureMessages("ADDR_NOT_IN_WHITELIST", a.sqRequest.get.pageSource(), "adsl")), a.livePerson.sq.cannotBelongError()
                        },
                        NO_PORT_AVAILABLE: function(b) {
                            console.log("adsl NO_PORT_AVAILABLE"), a.sqFormVM.state("EOI_ENTRY"), a.eoiFormVM.state("FULL"), a.eoiFormVM.eoiMessage("service-error"), b.fireAnalytics && (a.adobeAnalytics.global.sq.failFailureMessages("NO_PORT_AVAILABLE", a.sqRequest.get.pageSource(), "adsl", "modal"), a.analytics.methods.sqFailures.failureMessages("NO_PORT_AVAILABLE", a.sqRequest.get.pageSource(), "adsl")), a.livePerson.sq.completeSQ()
                        }
                    }
                },
                eoiResult: {
                    full: {
                        success: function() {
                            console.log("eoiResult full success"), a.eoiFormVM.state("NO_FORM"), a.eoiFormVM.eoiSuccessMessage("FULL")
                        },
                        fail: function() {
                            console.log("eoiResult full fail")
                        }
                    },
                    contact: {
                        success: function() {
                            console.log("eoiResult contact success"), a.eoiFormVM.state("NO_FORM"), a.eoiFormVM.eoiSuccessMessage("CONTACT")
                        },
                        fail: function() {
                            console.log("eoiResult contact fail")
                        }
                    }
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.sqModal = a.sqModal || {}, $.extend(a.sqModal, function() {
            var b;
            return {
                init: function() {
                    return b = this, 0 === $("#mdl-sq").length ? !1 : (ko.applyBindings(new a.sqModal.sqFormVM, document.getElementById("sq-form")), a.sqFormVM = ko.contextFor(document.getElementById("sq-form")).$data, ko.applyBindings(new a.sqModal.eoiFormVM, document.getElementById("eoi-form")), a.eoiFormVM = ko.contextFor(document.getElementById("eoi-form")).$data, ko.applyBindings(new a.sqModal.structuredAddressVM, document.getElementById("structured-address-form")), a.structuredAddressVM = ko.contextFor(document.getElementById("structured-address-form")).$data, jQuery.validator.addMethod("sqHasPostCode", function(a) {
                        return /^\d{3,4}$/.test(a.replace(/^\s+|\s+$/g, "").slice(-4))
                    }, "Please use the autocomplete to select an address."), "redirected" === a.util.loadPageVar("sq_redirect") && b.open({
                        openCallBack: function() {
                            a.sqRequest.get.fromSession() && a.sqFormVM.state("ADDRESS_FOUND")
                        }
                    }), b.validation(), a.CLEARSQONSUBMIT = !0, a.LANDLINECHECK = !1, void(a.REFINEDADDRESS = !1))
                },
                open: function(b) {
                    b = b || {}, b.closeCallBack = b.closeCallBack || function() {}, b.openCallBack = b.openCallBack || function() {}, $.magnificPopup.open({
                        items: {
                            src: "#mdl-sq",
                            type: "inline"
                        },
                        alignTop: !0,
                        showCloseBtn: !0,
                        closeOnBgClick: !0,
                        enableEscapeKey: !1,
                        overflowY: "auto",
                        callbacks: {
                            open: function() {
                                a.sqFormVM.resetForm(), a.belongModal.setupAutoResize({
                                    modalContainer: $("#mdl-sq"),
                                    nameSpace: "modalAutoResizeSq",
                                    noShadow: !0
                                }), b.openCallBack()
                            },
                            close: function() {
                                a.sqFormVM.hideAllMessages(), a.CLEARSQONSUBMIT = !0, a.LANDLINECHECK = !1, a.REFINEDADDRESS = !1, b.closeCallBack()
                            }
                        }
                    })
                },
                validation: function() {
                    a.sqValidator = a.validation.setupFormValidation("sq-entry", $("#sq-form"), {
                        rules: {
                            "sq-street-address": {
                                required: !0,
                                sqHasPostCode: !0,
                                disallowedCharacters: !0,
                                minlength: 8
                            },
                            "landline-entry-number": {
                                required: function() {
                                    var a = $("#landline-entry-yes").is(":checked");
                                    return a
                                },
                                australianPhoneNumber: !0
                            },
                            "landline-entry": {
                                required: $(".landline-entry .radio-holder").is(":visible")
                            }
                        },
                        errorPlacement: function(a, b) {
                            "landline-entry" == b.attr("name") ? $(".landline-entry .radio-holder").append(a) : a.insertAfter(b)
                        }
                    }), a.sqValidator.resetForm()
                }
            }
        }())
    }(BELONG),
    function(a) {
        "use strict";
        a.sqRequest = function() {
            return {
                processResult: function(b, c, d) {
                    var e = {
                            fireAnalytics: !0,
                            broadbandTechType: ""
                        },
                        f = a.sqRequest.get.pageSource(),
                        g = b.responseType;
                    if (a.sqRequest.set.isPreprovisioned(b.isPreprovisioned), a.sqRequest.set.promotionId(b.promotionId), a.sqRequest.set.orderType(b.responseType), a.sqRequest.set.broadbandTechType(b.broadbandTechnologyType), a.sqRequest.set.extendedNetwork(b.extendedNetwork), "fail" !== c || "undefined" != typeof g && null !== g || (g = "SQ_ERROR"), b.broadbandTechnologyType) switch (e.broadbandTechType = b.broadbandTechnologyType.toLowerCase(), e.broadbandTechType) {
                        case "fttp":
                        case "fttn":
                        case "fttb":
                            g = b.serviceClass, d = "nbn"
                    }
                    if ("success" === c && e.fireAnalytics) {
                        var h = d;
                        switch (g) {
                            case "NBN_SERVICE_CLASS_ELEVEN":
                                h = "nbn-fttn";
                                break;
                            case "NBN_SERVICE_CLASS_TWELVE":
                            case "NBN_SERVICE_CLASS_THIRTEEN":
                                h = "nbn-" + e.broadbandTechType
                        }
                        b.extendedNetwork && (h = "ADSL-ExtendedNetwork"), a.adobeAnalytics.global.sq.success(f, g, h, "modal"), a.analytics.methods.sq.sqSuccess(f, g, h)
                    }
                    a.livePerson.sq.completeSQ(), "success" !== c || a.util.isPrivateBrowsingMode() || (sessionStorage.setItem("sqDone", !0), sessionStorage.setItem("product", d), sessionStorage.setItem("broadbandTechnologyType", e.broadbandTechType), sessionStorage.setItem("resultStatus", c), sessionStorage.setItem("resultCode", g), sessionStorage.setItem("sqModel", JSON.stringify(a.join.model.sq))), "success" === c && "NBN" !== f && "nbn" === d ? a.sqFormVM.redirectUser({
                        pageUrl: "/nbn?sq_redirect=redirected",
                        product: "to-nbn"
                    }) : "success" === c && "ADSL" !== f && "adsl" === d ? a.sqFormVM.redirectUser({
                        pageUrl: "/adsl?sq_redirect=redirected",
                        product: "to-adsl"
                    }) : (console.log("product", d), console.log("resultCode", g), console.log("sqResultData", e), a.sqModal.sqRules.sqResult[d][g](e))
                },
                changeAddressSelect: function() {
                    var b = $(this).find("option:selected").val(),
                        c = $(this).find("option:selected").text();
                    switch (b) {
                        case "":
                            return !1;
                        case "address_not_found":
                            console.log("address_not_found selected"), a.adobeAnalytics.global.sq.selectAddress("address_not_found", "modal"), a.livePerson.sq.addressNotFound(), $("#sq-street-address").data("moniker", ""), a.sqFormVM.state("ADDRESS_ENTRY"), a.sqFormVM.entryMessage("HAVING_TROUBLE");
                            break;
                        default:
                            $.each(globalAddressList, function() {
                                this.addressId === b && ($("#sq-street-address").val(c), a.sqRequest.set.addressDetails(this), a.sqRequest.set.landlineDetails(), a.REFINEDADDRESS = !0)
                            })
                    }
                },
                set: {
                    addressDetails: function(b) {
                        var c, d, e = JSON.parse(b.fullAddress),
                            f = e.propertyAddressDetails,
                            g = $("#sq-street-address").val();
                        a.join.model.sq.serviceAddressId = b.addressId, a.join.model.sq.serviceExchange = e.exchange, a.join.model.sq.serviceStreet = f.streetDetails.streetName, a.join.model.sq.serviceSuburb = e.localityDetails.locality, a.join.model.sq.serviceState = e.localityDetails.state, a.join.model.sq.servicePostcode = e.localityDetails.postCode, a.join.model.sq.serviceStreetNumber = "undefined" != typeof b.streetNumber ? b.streetNumber : f.propertyNumberFrom + f.propertyNumberTo + f.propertyNumberFromSuffix, a.join.model.sq.serviceStreetType = "undefined" != typeof f.streetDetails.streetType ? f.streetDetails.streetType : "", "undefined" != typeof b.subaddress ? a.join.model.sq.serviceSubAddressDetails = b.subaddress : "undefined" != typeof b.subAddressDetails ? (c = (a.join.model.sq.serviceStreetNumber + " " + a.join.model.sq.serviceStreet).toUpperCase(), d = g.toUpperCase().indexOf(c), a.join.model.sq.serviceSubAddressDetails = g.substr(0, d)) : a.join.model.sq.serviceSubAddressDetails = "", $("#sq-post-code").val(a.join.model.sq.servicePostcode)
                    },
                    landlineDetails: function() {
                        if ($("#landline-entry-yes").is(":checked")) {
                            var b = $("#landline-entry-number").val(),
                                c = a.validation.helpers.cleanAustralianPhoneNumber(b);
                            $("#landline-entry-number").val(c), b = c, a.join.model.sq.phoneNumber = b
                        } else a.join.model.sq.phoneNumber = ""
                    },
                    orderType: function(b) {
                        a.join.model.sq.orderType = b
                    },
                    broadbandTechType: function(b) {
                        a.join.model.sq.broadbandTechType = b
                    },
                    isPreprovisioned: function(b) {
                        a.join.model.sq.isPreprovisioned = b
                    },
                    extendedNetwork: function(b) {
                        a.join.model.sq.extendedNetwork = b || !1
                    },
                    promotionId: function(b) {
                        a.join.model.sq.promotionId = b || !1
                    }
                },
                get: {
                    pageSource: function() {
                        var a = window.location.href,
                            b = window.location.pathname;
                        return "/" === b ? "OAHP" : a.indexOf("adsl") > -1 ? "ADSL" : a.indexOf("nbn") > -1 ? "NBN" : a.indexOf("join") > -1 ? "JOIN" : window.location.pathname.replace("/", "")
                    },
                    fromSession: function() {
                        if (console.log("sqEntry - retrieving existing user session"), null === JSON.parse(sessionStorage.getItem("sqModel"))) return !1;
                        a.join.model.sq = JSON.parse(sessionStorage.getItem("sqModel")), console.log("reloadSession(): EVE.join.model.sq=" + JSON.stringify(a.join.model.sq));
                        var b = {
                            fireAnalytics: !1,
                            broadbandTechType: sessionStorage.getItem("broadbandTechnologyType").toLowerCase()
                        };
                        return a.sqModal.sqRules.sqResult[sessionStorage.getItem("product")][sessionStorage.getItem("resultCode")](b), !0
                    }
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.sqModal = a.sqModal || {}, a.sqModal.structuredAddressVM = function() {
            var b = this;
            b.state = ko.observable("NONE"), b.structuredAddress = {
                type: ko.observable(""),
                unitNumber: ko.observable(""),
                streetNumber: ko.observable(""),
                streetName: ko.observable(""),
                streetType: ko.observable(""),
                suburb: ko.observable(""),
                state: ko.observable(""),
                postcode: ko.observable(""),
                propertyName: ko.observable(""),
                subAddressType: ko.observable(""),
                subAddressNumber: ko.observable("")
            }, b.structuredAddressSq = function() {
                if (!a.validation.validateForm(a.structuredAddressValidator)) return !1;
                var c = b.addressConcatenation(),
                    d = b.structuredAddress.postcode();
                b.state("NONE"), a.sqFormVM.state("LOADING"), a.CLEARSQONSUBMIT = !1, a.sqExecute.init(), a.sqExecute.initializeVars(), a.sqExecute.get_api_key(), a.sqExecute.performTelstraVerification(c, d, !0)
            }, b.setupStructuredAddressForm = function() {
                b.structuredAddress.type("UNIT_APARTMENT"), b.resetStructuredAddress(), b.state("START_STRUCTURED_SQ"), b.populateDropdownLists()
            }, b.resetStructuredAddress = function() {
                b.structuredAddress.unitNumber(""), b.structuredAddress.streetNumber(""), b.structuredAddress.streetName(""), b.structuredAddress.streetType(""), b.structuredAddress.suburb(""), b.structuredAddress.state(""), b.structuredAddress.postcode(""), b.structuredAddress.propertyName(""), b.structuredAddress.subAddressType(""), b.structuredAddress.subAddressNumber(""), $("#structured-address-form input").blur().change(), $("#structured-address-form-street-type").val("").change(), $("#structured-address-sub-address-type").val("").change(), $("#structured-address-form-state").val("").change(), $("input.error").removeClass("error"), $("label.error").hide()
            }, b.state.subscribe(function(b) {
                switch (b) {
                    case "START_STRUCTURED_SQ":
                        break;
                    case "NONE":
                }
                setTimeout(function() {
                    a.belongModal.setHeights({
                        modalContainer: $("#mdl-sq"),
                        noShadow: !0
                    })
                }, 100)
            }), b.structuredAddress.type.subscribe(function(c) {
                switch (c) {
                    case "UNIT_APARTMENT":
                        b.structuredAddress.propertyName(""), b.structuredAddress.subAddressType(""), b.structuredAddress.subAddressNumber(""), a.adobeAnalytics.global.sq.structuredClicksOnTab(c);
                        break;
                    case "HOUSE":
                        b.structuredAddress.unitNumber(""), b.structuredAddress.propertyName(""), b.structuredAddress.subAddressType(""), b.structuredAddress.subAddressNumber(""), a.adobeAnalytics.global.sq.structuredClicksOnTab(c);
                        break;
                    case "OTHER":
                        a.adobeAnalytics.global.sq.structuredClicksOnTab(c)
                }
            }), b.structuredSqValidation = function() {
                a.structuredAddressValidator = a.validation.setupFormValidation("structured-address-form", $("#structured-address-form"), {
                    rules: {
                        "property-type": {
                            required: !0
                        },
                        "structured-address-sub-address-type": {
                            required: !1
                        },
                        "structured-address-property-name": {
                            required: !1,
                            maxlength: 255,
                            disallowedCharacters: !0
                        },
                        "structured-address-form-unit-number": {
                            required: function() {
                                return "UNIT_APARTMENT" === b.structuredAddress.type()
                            }
                        },
                        "structured-address-sub-address-number": {
                            required: !1
                        },
                        "structured-address-form-street-number": {
                            required: !0
                        },
                        "structured-address-form-street-name": {
                            required: !0,
                            maxlength: 255,
                            disallowedCharacters: !0
                        },
                        "structured-address-form-street-type": {
                            required: !1,
                            maxlength: 255,
                            disallowedCharacters: !0
                        },
                        "structured-address-form-suburb": {
                            required: !0,
                            maxlength: 255,
                            disallowedCharacters: !0
                        },
                        "structured-address-form-state": {
                            required: !0,
                            maxlength: 3,
                            disallowedCharacters: !0
                        },
                        "structured-address-form-postcode": {
                            required: !0,
                            maxlength: 4,
                            exactlength: 4,
                            digits: !0,
                            disallowedCharacters: !0
                        }
                    }
                })
            }, b.structuredSqValidation(), b.addressConcatenation = function() {
                var c = "";
                return "OTHER" === b.structuredAddress.type() && (b.structuredAddress.subAddressNumber() && (c += b.structuredAddress.subAddressType() ? b.structuredAddress.subAddressType() + " " : ""), c += b.structuredAddress.subAddressNumber() ? b.structuredAddress.subAddressNumber() + ", " : "", b.structuredAddress.propertyName() && a.adobeAnalytics.global.sq.structuredPropertyNameEntered(b.structuredAddress.propertyName())), "UNIT_APARTMENT" === b.structuredAddress.type() && (c += b.structuredAddress.unitNumber() ? b.structuredAddress.unitNumber() + "/" : ""), c += b.structuredAddress.streetNumber() + " " + b.structuredAddress.streetName(), c += b.structuredAddress.streetType() ? " " + b.structuredAddress.streetType() + ", " : ", ", c += b.structuredAddress.suburb() + ", " + b.structuredAddress.state() + ", " + b.structuredAddress.postcode(), a.DEVSWITCH && console.log(c), c
            }, b.populateDropdownLists = function() {
                b.populateDropdown(ADDRESS_FIELDS_STATES, "#structured-address-form-state"), b.populateDropdown(ADDRESS_FIELDS_STREET_TYPES, "#structured-address-form-street-type"), b.populateDropdown(ADDRESS_FIELDS_SUB_ADDRESS_TYPES, "#structured-address-sub-address-type")
            }, b.populateDropdown = function(a, b, c) {
                var d = c || !0,
                    e = [];
                d && e.push('<option value=""></option>'), $.each(a, function(a, b) {
                    e.push('<option value="' + b + '">' + b + "</option>")
                }), $(b).html(e.join(""))
            }
        }
    }(BELONG),
    function(a) {
        "use strict";
        a.onlineTroubleshooting = a.onlineTroubleshooting || {}, $.extend(a.onlineTroubleshooting, function() {
            return {
                init: function() {
                    if (0 === $("#online-troubleshooting").length) return !1;
                    a.KO_STARTED = !1;
                    var b = new this.viewModel;
                    ko.applyBindings(b, document.getElementById("online-troubleshooting-modal")), b.init()
                },
                viewModel: function() {
                    var b = this,
                        c = BELONG_TS_DATA,
                        d = BELONG_TS_OPTIONS,
                        e = {
                            close_confirmation: $("#troubleshooting-close-confirmation").text(),
                            unload_confirmation: $("#troubleshooting-unload-confirmation").text(),
                            contact_callback_option: $("#troubleshooting-contact-callback-option").text(),
                            contact_call_option: $("#troubleshooting-contact-call-option").text()
                        };
                    b.cards = ko.observableArray([]), b.currentCardObj = ko.computed(function() {
                        return b.cards()[b.cards().length - 1]
                    }), b.currentCard = ko.observable(""), b.nextAnswer = ko.observable(""), b.cardsLog = ko.observableArray([]), b.troubleshootingData = ko.observable({}).publishOn("troubleshootingData", !0), b.troubleshootingData.subscribe(function() {
                        b.insertNewCard(b.determineStartCard())
                    }), b.outageData = ko.observable({}).publishOn("outageData", !0), b.serverError = ko.observable({}).publishOn("serverError", !0), b.init = function() {
                        b.apiInit(), window.onbeforeunload = function() {
                            return e.unload_confirmation
                        }, $(".modal-component-body").on("scroll.forShadow", function() {
                            $(".active-card").offset().top > $(".modal-component-body").offset().top + 10 ? $(".modal-component h2").addClass("scrolling") : $(".modal-component h2").removeClass("scrolling")
                        }), $(".open-troubleshooting-modal").off("click.openTroubleshootingModal").on("click.openTroubleshootingModal", function(c) {
                            return c.preventDefault(), a.adobeAnalytics.pages.troubleshooting.beginButtonClick(), b.openPopup(), !1
                        }), "true" === a.util.loadPageVar("openOnLoad") && b.openPopup()
                    }, b.apiInit = function() {
                        b.createLoaderCard();
                        var a = $.ajax({
                            url: "/bin/belong/customer/troubleshooting/init",
                            timeout: 6e4
                        });
                        a.done(function(a) {
                            for (var c = 0; c < a.plans.length; c++)
                                if ("ACTIVE" === a.plans[c].planStatus) {
                                    b.troubleshootingData(a.plans[c]);
                                    break
                                }
                            b.outageData(a.outage)
                        }), a.fail(function(a) {
                            b.serverError(a)
                        }), a.always(function() {
                            b.cards.remove(function(a) {
                                return "loader" === a.type
                            })
                        })
                    }, b.determineStartCard = function() {
                        var a = b.troubleshootingData();
                        return "NBN" === a.planType ? "FTTP" === a.broadbandTechType ? d.fttpStartCard : d.fttbStartCard : d.adslStartCard
                    }, b.customSteps = function(b) {
                        switch (b) {
                            case "modem-details":
                                a.ajax("/bin/belong/customer/modemDetails", {}).then(function(a) {
                                    a.success && ($(".custom-step-username").text(a.username), $(".custom-step-password").text(a.password))
                                }, function() {
                                    $(".custom-step-username").text("your username"), $(".custom-step-password").text("your password")
                                })
                        }
                    }, b.createQuestionCard = function(a) {
                        var c = a;
                        c.checkedOption = ko.observable(""), c.active = ko.observable(!0), c.editable = ko.observable(!1), c.tooltip = a.tooltip || null, c.showTooltip = ko.observable(!1), c.openTooltip = function() {
                            c.showTooltip() ? $(".active-card").velocity("scroll", {
                                container: $(".modal-component-body"),
                                duration: 200,
                                complete: function() {
                                    c.showTooltip(!1)
                                }
                            }) : (c.showTooltip(!0), $(".active-card .question").velocity("scroll", {
                                container: $(".modal-component-body"),
                                delay: 0,
                                duration: 0,
                                complete: function() {
                                    setTimeout(function() {
                                        $(".active-card").velocity("scroll", {
                                            container: $(".modal-component-body"),
                                            duration: 200
                                        })
                                    }, 200)
                                }
                            }))
                        }, b.cards.push(c)
                    }, b.createInstructionCard = function(a) {
                        var c = a;
                        c.checkedOption = ko.observable(""), c.instructionState = ko.observable("start"), c.active = ko.observable(!0), c.editable = ko.observable(!1), c.tooltip = a.tooltip || null, c.showTooltip = ko.observable(!1), c.openTooltip = function() {
                            c.showTooltip() ? $(".active-card").velocity("scroll", {
                                container: $(".modal-component-body"),
                                duration: 200,
                                complete: function() {
                                    c.showTooltip(!1)
                                }
                            }) : (c.showTooltip(!0), $(".active-card .question").velocity("scroll", {
                                container: $(".modal-component-body"),
                                delay: 0,
                                duration: 0,
                                complete: function() {
                                    setTimeout(function() {
                                        $(".active-card").velocity("scroll", {
                                            container: $(".modal-component-body"),
                                            duration: 200
                                        })
                                    }, 200)
                                }
                            }))
                        }, c.startInstructions = function() {
                            c.instructionState("steps"), b.scrollToActiveTop(), $(".steps-card-container:visible").hasClass("slider-activated") ? ($(".steps-card-container").flexslider(0), $(".steps-card-container").flexslider("pause"), setTimeout(function() {
                                $(".steps-card-container").removeClass("invisible")
                            }, 200)) : $(".steps-card-container:visible").flexslider({
                                animation: "slide",
                                animationLoop: !1,
                                slideShow: !1,
                                start: function() {
                                    $(".step-content").height($(".step").height()), $(".steps-card-container").addClass("slider-activated").removeClass("invisible"), b.customSteps("modem-details"), $(".steps-card-container").flexslider("pause")
                                },
                                before: function(a) {
                                    $(".flex-continue").off("click.flexContinue").remove(), $(".steps-card-container:visible .flex-control-nav a").removeClass("flex-done"), $(".steps-card-container:visible .flex-control-nav a").each(function(b) {
                                        var c = $(this);
                                        return c.addClass("flex-done"), b === a.animatingTo ? !1 : void 0
                                    }), a.count - 1 === a.animatingTo && ($(".flex-direction-nav").append('<a class="flex-continue" href="#">Continue</a>'), $(".flex-continue").off("click.flexContinue").on("click.flexContinue", function() {
                                        c.instructionState("question"), b.scrollToActiveTop(), $(".steps-card-container").addClass("invisible")
                                    })), b.scrollToActiveTop()
                                }
                            })
                        }, b.cards.push(c)
                    }, b.createContactCard = function(c) {
                        var d = c;
                        d.active = ko.observable(!0), d.editable = ko.observable(!1), d.contactState = ko.observable("start"), d.checkedOption = ko.observable("CONTACT_CALL"), d.comments = ko.observable(""), d.caseNumber = ko.observable(""), d.checkedCallBackOption = ko.observable("CALLBACK_AM"), d.hasSubmitCaseErrors = ko.observable(!1), d.submitCallBtnState = ko.observable(""), d.isLoading = ko.observable(!1), d.options = [{
                            optionLabel: e.contact_callback_option,
                            optionValue: "CONTACT_CALLBACK"
                        }, {
                            optionLabel: e.contact_call_option,
                            optionValue: "CONTACT_CALL"
                        }], d.callBackOptions = [{
                            optionLabel: "AM",
                            optionValue: "CALLBACK_AM"
                        }, {
                            optionLabel: "PM",
                            optionValue: "CALLBACK_PM"
                        }, {
                            optionLabel: "After 6pm",
                            optionValue: "CALLBACK_AFTER_6PM"
                        }, {
                            optionLabel: "Donâ€™t mind",
                            optionValue: "CALLBACK_DONT_CARE"
                        }], d.submitCallOptionErrors = function() {
                            d.hasSubmitCaseErrors(!0), d.submitCallBtnState(""), d.isLoading(!1)
                        }, d.submitCallOption = function(c, f) {
                            f.preventDefault(), d.isLoading(!0), d.submitCallBtnState("disabled");
                            var c = {
                                note: d.comments(),
                                issue: d.contactIssue,
                                completedStep: d.contactAction,
                                details: ""
                            };
                            return "CONTACT_CALLBACK" === d.checkedOption() ? (c.contactFlag = !0, c.contactType = e.contact_callback_option, c.contactTime = d.checkedCallBackOption()) : "CONTACT_CALL" === d.checkedOption() && (c.contactFlag = !1, c.contactType = e.contact_call_option, c.contactTime = ""), $.each(b.cardsLog(), function() {
                                c.details += "Q: " + this.cardId + " (" + this.question + ") | A: " + this.nextCardId + " (" + this.answer + ") | "
                            }), c.details = c.details.substring(0, c.details.length - 1), a.ajax("/bin/belong/customer/troubleshooting/submit", c).then(function(e) {
                                e.success ? ($(".troubleshooting-contact-card .card-loader").hide(), d.caseNumber(e.caseNumber), d.contactState("end"), b.scrollToActiveTop(), a.adobeAnalytics.pages.troubleshooting.submitCase(c)) : d.submitCallOptionErrors()
                            }, function() {
                                d.submitCallOptionErrors()
                            }), !1
                        }, b.cards.push(d)
                    }, b.createEndCard = function(a) {
                        var c = a;
                        c.active = ko.observable(!0), c.editable = ko.observable(!1), b.cards.push(c)
                    }, b.createInformationCard = function(a) {
                        var c = a;
                        c.active = ko.observable(!0), c.editable = ko.observable(!1), b.nextAnswer(a.nextStep), b.cards.push(c)
                    }, b.createLoaderCard = function() {
                        var a = {
                            active: ko.observable(!0),
                            editable: ko.observable(!1),
                            type: "loader",
                            id: "LOADER",
                            checkedOption: ko.observable()
                        };
                        b.cards.push(a)
                    }, b.insertNewCard = function(e) {
                        var f, g = null,
                            h = {};
                        switch ($.each(c, function() {
                            this.id === e && (f = this), this.id === b.currentCard() && (g = this)
                        }), null !== g && (h.cardId = g.id, h.nextCardId = f.id, h.question = g.question, $.each(g.options, function() {
                            this.optionValue === f.id && (h.answer = this.optionLabel)
                        }), b.cardsLog.push(h), console.log(h), d.enableDatabaseLog && a.ajax("/bin/belong/customer/troubleshooting/log", h), a.adobeAnalytics.pages.troubleshooting.cardsLog(h)), f.type) {
                            case "question":
                                b.createQuestionCard(f);
                                break;
                            case "instruction":
                                b.createInstructionCard(f);
                                break;
                            case "end":
                                b.createEndCard(f);
                                break;
                            case "contact":
                                b.createContactCard(f);
                                break;
                            case "information":
                                b.createInformationCard(f)
                        }
                    }, b.updateCardsLog = function() {
                        console.log(b.currentCard())
                    }, b.renderCard = function() {
                        var c = $(".troubleshooting-card:last-of-type");
                        "loader" !== b.currentCardObj().type && ($.each(b.cards(), function(c) {
                            this.editable(!1), this.active(!1), c + 1 === b.cards().length && (this.active(!0), b.currentCard(this.id), d.keepLastChecked || "end" === this.type || "contact" === this.type || "information" === this.type || this.checkedOption(""), "instruction" === this.type && this.instructionState("start")), c + 2 === b.cards().length && (this.editable(!0), "instruction" === this.type && this.instructionState("end")), "contact" === this.type && (a.forms.applyShiftLabel(), a.forms.applyAutoGrowTextarea())
                        }), c.velocity("scroll", {
                            container: $(".modal-component-body"),
                            delay: 100,
                            duration: 600,
                            complete: function() {
                                setTimeout(function() {
                                    c.find(".card-loader").hide(), c.find(".card-container").removeClass("hidden")
                                }, 200)
                            }
                        }))
                    }, b.scrollToActiveTop = function(a) {
                        $(".active-card").velocity("scroll", {
                            container: $(".modal-component-body"),
                            duration: a || 200
                        })
                    }, b.continueButton = function() {
                        $.each(b.cards(), function() {
                            this.id === b.currentCard() && "undefined" != typeof this.showTooltip && this.showTooltip() && this.openTooltip()
                        });
                        var a = b.nextAnswer();
                        b.nextAnswer(""), b.insertNewCard(a)
                    }, b.editButton = function() {
                        b.cards.remove(function(a) {
                            return a.id === b.currentCard()
                        }), b.renderCard(), b.nextAnswer("information" === b.currentCardObj().type ? b.currentCardObj().nextStep : "")
                    }, b.optionSelected = function(a) {
                        d.forceContinue ? b.nextAnswer(a.optionValue) : b.insertNewCard(a.optionValue)
                    }, b.openPopup = function() {
                        $.magnificPopup.open({
                            items: {
                                src: "#online-troubleshooting-modal",
                                type: "inline"
                            },
                            showCloseBtn: !0,
                            closeOnBgClick: !1,
                            overflowY: "hidden"
                        }), $.magnificPopup.instance.close = function() {
                            return confirm(e.close_confirmation) ? (b.closePopup(), !1) : ($("#online-troubleshooting-modal > .mfp-close").blur(), !1)
                        }
                    }, b.closePopup = function() {
                        var a = navigator.userAgent,
                            b = window.navigator.standalone,
                            c = a.match(/iPhone|iPad|iPod/i),
                            e = (a.match(/Android/i), a.match(/IEMobile/i), !!window.chrome, /safari/.test(a.toLowerCase()));
                        if (d.redirectOnClose) {
                            if (!c || b || e) return window.onbeforeunload = null, window.location.replace(d.redirectOnCloseUrl), !1;
                            $.magnificPopup.proto.close.call(this)
                        } else $.magnificPopup.proto.close.call(this)
                    }
                }
            }
        }())
    }(BELONG),
    function(a) {
        "use strict";
        a.outageCheck = a.outageCheck || {}, $.extend(a.outageCheck, function() {
            return {
                init: function() {
                    if (0 === $("#cmp-outage-check").length) return !1;
                    var a = new this.viewModel;
                    ko.applyBindings(a, document.getElementById("cmp-outage-check")), a.init()
                },
                viewModel: function() {
                    var b = this;
                    b.util = a.util, b.isCheckingOutage = ko.observable(!0), b.outagesExist = ko.observable(""), b.serverError = ko.observableArray([]), b.broadbandPlan = ko.observable(""), b.outageStartTime = ko.observable(""), b.outageEstimateResolveTime = ko.observable(""), b.outageServerError = ko.observable(!1), b.scheduledMaintenance = ko.computed(function() {
                        console.log(b.outageStartTime(), b.outageEstimateResolveTime());
                        var a = "",
                            c = "";
                        return b.outageStartTime() && (a = b.util.formatter.dateString(b.outageStartTime(), "DD-MM-YYYY HH:mm a")), b.outageEstimateResolveTime() && (c = b.util.formatter.dateString(b.outageEstimateResolveTime(), "DD-MM-YYYY HH:mm a")), a + " - " + c
                    }), ko.postbox.subscribe("outageData", function(c) {
                        c && (b.outagesExist(!0), b.outageStartTime(c.startTimestamp), b.outageEstimateResolveTime(c.etrTimestamp), b.outageServerError(c.error)), b.isCheckingOutage(!1), a.adobeAnalytics.pages.troubleshooting.submitOutage(c)
                    }), ko.postbox.subscribe("troubleshootingData", function(c) {
                        b.broadbandPlan(c.planType), a.adobeAnalytics.pages.troubleshooting.loadPlanData(!0, !1), $(".troubleshooting-cta-container").removeClass("hidden")
                    }), ko.postbox.subscribe("serverError", function(c) {
                        var d = c.responseJSON;
                        b.serverError("timeout" === c.statusText ? [{
                            code: "ERROR_TIMEOUT"
                        }] : void 0 !== d && void 0 !== d.errors ? d.errors : [{
                            code: "ERROR_DEFAULT"
                        }]);
                        var e = b.serverError()[0].code;
                        b.isCheckingOutage(!1), a.adobeAnalytics.pages.troubleshooting.initData(!1, e)
                    }), b.init = function() {}
                }
            }
        }())
    }(BELONG),
    function(a) {
        "use strict";
        a.belongModal = function() {
            return {
                setupAutoResize: function(b) {
                    b.animateResize = b.animateResize || !1, b.noShadow = b.noShadow || !1, b.nameSpace = b.nameSpace || "modalAutoResize";
                    var c = b.modalContainer;
                    c.data("winheight", $(window).height()), c.data("winwidth", $(window).width()), $(window).off("resize." + b.nameSpace).on("resize." + b.nameSpace, function() {
                        return c.data("winheight") === $(window).height() && c.data("winwidth") === $(window).width() ? !1 : (c.data("winheight", $(window).height()), c.data("winwidth", $(window).width()), a.belongModal.setHeights(b), void 0)
                    }), a.belongModal.setHeights(b)
                },
                setHeights: function(b) {
                    var c = b.modalContainer,
                        d = c.find(".belong-modal__header").height("").innerHeight(),
                        e = c.find(".belong-modal__body").innerHeight(),
                        f = c.find(".belong-modal__footer").height("").innerHeight(),
                        g = 0,
                        h = 0,
                        i = 0;
                    c.find(".belong-modal__body").height(""), g = a.belongModal.calculateModalHeight(c.height("").innerHeight()), i = g - d - f, b.animateResize ? (c.find(".belong-modal__body").height(e), c.find(".belong-modal__body").velocity({
                        height: i
                    }, {
                        duration: 200
                    })) : c.find(".belong-modal__body").height(i), c.find(".belong-modal__body").children().each(function() {
                        h += $(this).innerHeight()
                    }), h > i && !b.noShadow ? c.find(".belong-modal__body").addClass("belong-modal__body--scroll") : c.find(".belong-modal__body").removeClass("belong-modal__body--scroll")
                },
                calculateModalHeight: function(a) {
                    return $(window).height() > 900 && $(window).height() < a ? 850 : $(window).height() < a ? $(window).width() < 640 ? $(window).height() : $(window).height() - 50 : a
                }
            }
        }()
    }(BELONG),
    function(a) {
        "use strict";
        a.belongTabPanel = function(a, b) {
            function c(a) {
                var b = $("#" + g + " .tab-panel__panel.tab-panel__panel--active");
                b.addClass("hidden").removeClass("tab-panel__panel--active"), 0 != a && ($("#" + g + ' [data-panel-id="' + a + '"]').removeClass("hidden").addClass("tab-panel__panel--active"), console.log("Show panelID=" + a))
            }

            function d(a) {
                var b = $("#" + g + " .tab-panel__panel"),
                    c = $("#" + g + " .tab-panel__panels");
                1 == a ? (c.removeClass("tab-panel__panels---top-left-radius"), c.addClass("tab-panel__panels---top-right-radius")) : a == b.length ? (c.removeClass("tab-panel__panels---top-right-radius"), c.addClass("tab-panel__panels---top-left-radius")) : (c.addClass("tab-panel__panels---top-left-radius"), c.addClass("tab-panel__panels---top-right-radius"))
            }

            function e() {
                var a = i.outerWidth(),
                    b = j.outerWidth();
                if (!(0 != k && k >= a))
                    if (b > a) console.log("SWITCH STYLE: tabsWidth > panelWidth"), $("#" + g).addClass("tab-panel--small"), $("#" + g).removeClass("tab-panel--large"), k = a;
                    else {
                        $("#" + g).addClass("tab-panel--large"), $("#" + g).removeClass("tab-panel--small"), k = 0;
                        var c = $("#" + g + " .tab-panel__panel.tab-panel__panel--active");
                        0 == c.length && f.setActiveTab(1)
                    }
            }
            var f = this,
                g = a,
                h = !1;
            "undefined" != typeof b && (h = b), f.isSmallView = function() {
                return $("#" + g).hasClass("tab-panel--small")
            }, f.isLargeView = function() {
                return $("#" + g).hasClass("tab-panel--large")
            }, f.setActiveTab = function(a) {
                console.log("setActiveTab(): ", a);
                var b = $("#" + g + ' .tab-panel__tab[data-tab-id="' + a + '"]'),
                    e = a;
                return f.isLargeView() && b.hasClass("tab-panel__tab--active") ? void 0 : (f.isSmallView() && b.hasClass("tab-panel__tab--active") && (e = 0), c(e), $("#" + g + " .tab-panel__tab").removeClass("tab-panel__tab--active"), 0 != e && b.addClass("tab-panel__tab--active"), d(a), !1)
            };
            var i = $("#" + g + " .tab-panel__panels"),
                j = $("#" + g + " .tab-panel__tabs"),
                k = 0;
            ! function() {
                f.setActiveTab(1), d(1), 0 == h && (e(), $(window).resize(function() {
                    e()
                }))
            }()
        }
    }(BELONG),
    function(a) {
        var b = function() {
            ko.bindingHandlers.fadeVisible = {
                init: function(a, b) {
                    var c = b();
                    $(a).toggle(ko.unwrap(c))
                },
                update: function(a, b) {
                    var c = b();
                    ko.unwrap(c) ? $(a).velocity({
                        opacity: 1,
                        translateZ: 0,
                        translateY: 0
                    }, {
                        display: "block"
                    }) : $(a).velocity({
                        opacity: 0,
                        translateZ: 0,
                        translateY: "10px"
                    }, {
                        display: "none"
                    })
                }
            }, ko.bindingHandlers.logger = {
                update: function(a, b, c) {
                    var d = ko.utils.domData.get(a, "_ko_logger") || 0,
                        e = ko.toJS(b() || c());
                    ko.utils.domData.set(a, "_ko_logger", ++d), window.console && console.log && console.log(d, a, e)
                }
            }, ko.bindingHandlers.stopBinding = {
                init: function() {
                    return {
                        controlsDescendantBindings: !0
                    }
                }
            }, ko.virtualElements.allowedBindings.stopBinding = !0, ko.extenders.trimAndUppercase = function(a) {
                return a.subscribe(function(b) {
                    a(b.trim().toUpperCase())
                }), a
            }
        };
        a.koCustomBindings = {
            init: b
        }
    }(BELONG),
    function() {
        "use strict"
    }(BELONG),
    function(a) {
        a.koComponents = a.koComponents || {}, a.koComponents.tileButtonSlider = function() {
            var a;
            return {
                init: function() {
                    a = this, ko.components.register("tile-button-slider", {
                        viewModel: a.viewModel,
                        template: a.template
                    })
                },
                template: '<div class="tile-button-slider">                    <ul class="slides" data-bind="foreach: items">                        <li>                            <a class="tile tile--small" data-bind="click: $parent.itemClicked, css: {\'tile--active\': active}">                                <div class="tile__month" data-bind="text: primaryLabel"></div>                                <div class="tile__year" data-bind="text: secondaryLabel"></div>                            </a>                        </li>                    </ul>                </div>',
                viewModel: function(a) {
                    var b = this;
                    b.items = ko.observableArray([]), b.selectedItem = ko.observable(), b.tileClickCallback = a.tileClickCallback, b.isVisible = ko.observable(!1), b.init = function(a) {
                        $.each(a, function(a, c) {
                            c.active = ko.observable(!1), b.items.push(c)
                        })
                    }, b.itemClicked = function(a) {
                        b.selectedItem() && b.selectedItem().active(!1), a.active(!0), b.selectedItem(a), b.tileClickCallback(a)
                    }, b.applyFlexSlider = function() {
                        var c = 0,
                            d = !1,
                            e = !1;
                        if (b.items().length > 4 && (c = b.items().length - 4, d = !0, e = !0), b.items().length) {
                            var f = $("#" + a.elementId + " .tile-button-slider");
                            f.flexslider({
                                itemWidth: 60,
                                itemMargin: 10,
                                minItems: 2,
                                maxItems: 4,
                                animation: "slide",
                                animationSpeed: 400,
                                animationLoop: !1,
                                controlNav: !1,
                                move: 1,
                                slideshow: !1,
                                prevText: "",
                                nextText: "",
                                startAt: 0,
                                touch: e,
                                directionNav: d,
                                start: function() {
                                    f.flexslider(c)
                                }
                            })
                        }
                    }, a.bindingScopeCallback(b)
                }
            }
        }()
    }(BELONG);
var globalAddressList, sqConfig;
switch (window.BELONG = window.BELONG || {}, BELONG.IS_RESPONSIVE = !0, BELONG.IS_EDIT = BELONG.IS_EDIT || !1, Array.prototype.indexOf = Array.prototype.indexOf || function(a, b) {
    for (var c = b || 0, d = this.length; d > c; c++)
        if (this[c] === a) return c;
    return -1
}, window.location.host) {
    case "con.local:3000":
    case "localhost:5000":
    case "localhost:4567":
    case "localhost:4502":
    case "localhost:4503":
    case "localhost:3000":
        BELONG.DEVSWITCH = !0, BELONG.DEVDELAY = 1;
        break;
    case "dev.belong.com.au":
    case "test.belong.com.au":
    case "test1.belong.com.au":
    case "www.belong.com.au":
    case "staging.belong.com.au":
        -1 !== window.location.href.indexOf("/fed/") ? (BELONG.DEVSWITCH = !0, BELONG.DEVDELAY = 500) : (BELONG.DEVSWITCH = !1, BELONG.DEVDELAY = 0, BELONG.URLS = {});
        break;
    default:
        BELONG.DEVSWITCH = !1, BELONG.DEVDELAY = 0, BELONG.URLS = {}, console.log = function() {}
}
"undefined" == typeof console && (window.console = {}, console.log = console.error = console.info = console.debug = console.warn = console.trace = console.dir = console.dirxml = console.group = console.groupEnd = console.time = console.timeEnd = console.assert = console.profile = function() {}),
    function(a) {
        "use strict";
        a.INIT = {
            visual: function() {
                a.validation.init(), a.util.helpers(), a.tooltip(), a.sectionTitle.init(), a.product.init(), a.expandCollapse.init(), a.responsiveTable.init(), a.responsiveImages(), a.flexslider(), a.iconCarousel(), a.headerNav.init(), a.mobo.init(), a.forms.simpleSelect(), a.forms.styledRadio(), a.forms.date.init(), a.scrollToAnchor.init(), a.navMobile.init(), a.collapsibles.init(), a.accordian(), a.faqAccordian(), a.genericInformationModal.init(), a.targetMobileApp.init(), a.sqLauncher.init(), a.agentTermsModal.init()
            },
            functional: function() {
                a.analytics.init(), a.livePerson.init(), a.util.scrollPage(), a.mqad.init(), a.DEVSWITCH && a.sqDebug.init(), a.sqEntry.init(), a.sqExecute.init(), a.eoiEntry.init(), a.sqModal.init(), a.forms.applyShiftLabel(), a.util.handlePrivateBrowsingMode(), a.appleSmartBanner.init()
            },
            pages: function() {
                a.unsubscribe.init(), a.join.init(), a.join.cisModal.init(), a.autoPaymentPrompt.init(), a.outageCheck.init(), a.onlineTroubleshooting.init(), a.manageService.init(), a.managePayment.init(), a.callbackForm.init(), a.feedbackForm.init(), a.personalDetails.init(), a.productBuilder.init(), a.aboutUs.init(), a.compareModule.init()
            },
            queryStrings: function() {
                var b = a.util.loadPageVar("promocode");
                "" !== b && localStorage.setItem("promocode", b)
            }
        }, a.init = function() {
            a.GD = $("#GlobalData").data(), a.adobeAnalytics.init(), a.adobeAnalytics.initDataLayer(), a.koCustomBindings.init(), a.ajaxSetup(), a.INIT.visual(), a.INIT.functional(), a.INIT.pages(), a.INIT.queryStrings(), a.triggerReady()
        }, $(document).ready(function() {
            a.init()
        })
    }(BELONG);
