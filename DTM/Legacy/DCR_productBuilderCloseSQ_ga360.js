/* BT-24403 Ref #7 Click Continue from SQ */
var fbb = _satellite.getVar("fbbProductCatalogue"),
	digitalData = window.digitalData || {},
	eventData = ((window.BELONG || {}).dataLayer || {}).event || {};

digitalData.event = digitalData.event || [];

// eventAction will == Load when the user clicks Continue on the SQ Modal
if (eventData.eventAction === "Load") {
	digitalData.event.push({
		eventName: "analytics-event",
		userInteraction: "click",
		textContent: "Continue",
		errorCode: eventData.errorCode,
		errorReason: undefined,
		success: eventData.success,
		journey: "enter",
		formStep: "2",
		product: fbb.getProductData()
	});
	if (_satellite.settings.notifications) {
		console.log(
			"BT-24403 Ref #7 Click Continue from SQ",
			"digitalData.event[n]:",
			JSON.stringify(digitalData.event[digitalData.event.length - 1], null, "\t")
		);
	}
}
