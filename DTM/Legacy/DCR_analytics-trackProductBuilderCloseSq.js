// Custom code for analytics-trackProductBuilderCloseSq Direct Call Rule.
var productModel  = (((BELONG||{}).dataLayer||{}).event||{}).productModel||{};

var planType      = productModel.planType || "unknown",
	calculatedMTC = productModel.calculatedMTC || "0",
	contractTerm  = productModel.contractTerm || "unknown",
	planSize      = productModel.planSize || "unknown",
	planSpeed     = productModel.planSpeed || "unknown",
	quantity      = "1";
var product       = [planType,planSize,planSpeed,contractTerm].join(' ');

s.products        = [planType,product,quantity,calculatedMTC].join(';')
s.linkTrackEvents = "event4,prodView";

// Update the datalayer for use by later events.
BELONG.dataLayer.productData = {
	'planType':planType,'calculatedMTC':calculatedMTC,'contractTerm':contractTerm,
	'planSize':planSize,'planSpeed':planSpeed,'quantity':quantity
};