/* BT-24403 Product Catalogue and utility functions */
var fbb = {
	getProductCode: function(planLabel){
		return [planLabel.toUpperCase(), this.getProductType(), this.getTechType()].join('_')
	},
	getTechType: function(){
		var techType;
		try{
			techType=JSON.parse(sessionStorage.sqModel).broadbandTechType
		}catch(e){
			techType="UNKNOWN"
		}
		return techType;
	},
	getProductType: function(){
		return String(sessionStorage.product || "nbn").toUpperCase()
	},
	getSelectedPlan: function(){
		/* Determines which plan is selected on the product builder */
		var el = document.querySelector('#ctn-speedboost-card input:checked')
		var sibling = (el !== null) ? el.nextElementSibling : null
		var planText = (sibling !== null) ? sibling.querySelector('[data-bind="text: displaySpeed"]').innerText : "UNKNOWN PLAN"
		return planText
	},
	getSelectedContract: function(){
		/* Determines which contract is selected on the product builder */
		var el = document.querySelector('#ctn-speedboost-card')
		var sibling = (el !== null) ? el.nextElementSibling : null
		var contractVal = (sibling !== null) ? sibling.querySelector('input:checked, input').value : "UNKNOWN CONTRACT"
		return contractVal
	},
	getAllProducts: function(){
		/* Returns the complete product catalogue */
		return fbb.productCatalogue;
	},
	getProductData: function(planLabel,contract){
		/* Returns 1 product object from the productCatalogue for a given planLabel and contract */
		var products = this.getAllProducts(),
			planLabel = planLabel || this.getSelectedPlan(),
			contract = contract || this.getSelectedContract();
		for (var index = 0; index < products.length; index++){
			var product = products[index];
			if(planLabel===product.planLabel && contract===product.contract){
				this.storeProductData(product)
				return product;
			}
		}
	},
	storeProductData: function(productData){
		sessionStorage.setItem('productData',JSON.stringify(productData))
	},
	restoreProductData: function(){
		return JSON.parse(sessionStorage.getItem('productData'))
	},
	get productModel(){return JSON.parse(sessionStorage.getItem('productModel'))},
	productCatalogue: [
	{
		// Premium $95 Month by Month
		get productCode(){return fbb.getProductCode(this.planLabel)},
		productName: "nbn | Premium",
		description: "Premium $95/mth",
		currency: "AUD",
		dataValue: "unlimited",
		serviceType: "new",
		category: "broadband",
		contract: "MONTH_BY_MONTH",
		contractTerm: "Month by Month",
		planLabel: "Premium",
		planSize: "unlimited",
		planSpeed: "100/40",
		planType: "",
		quantity: 1,
		price: {
		priceValue: 95,
		priceUnit: "$",
		calculatedMTC: 155
		}
	},{
		// Standard Plus $65 12 Month Contract
		get productCode(){return fbb.getProductCode(this.planLabel)},
		productName: "nbn | Standard Plus",
		description: "Standard Plus $65/mth",
		currency: "AUD",
		dataValue: "unlimited",
		serviceType: "new",
		category: "broadband",
		contract: "TWELVE_MONTHS",
		contractTerm: "12 Month",
		planLabel: "Standard Plus",
		planSize: "unlimited",
		planSpeed: "50/20",
		planType: "",
		quantity: 1,
		price: {
			priceValue: 65,
			priceUnit: "$",
			calculatedMTC: 780
		}
	},{
		// Standard Plus $70 12 Month by Month
		get productCode(){return fbb.getProductCode(this.planLabel)},
		productName: "nbn | Standard Plus",
		description: "Standard Plus $70/mth",
		currency: "AUD",
		dataValue: "unlimited",
		serviceType: "new",
		category: "broadband",
		contract: "MONTH_BY_MONTH",
		contractTerm: "Month by Month",
		planLabel: "Standard Plus",
		planSize: "unlimited",
		planSpeed: "50/20",
		planType: "",
		quantity: 1,
		price: {
		priceValue: 70,
		priceUnit: "$",
		calculatedMTC: 130
		}
	},{
		get productCode(){return fbb.getProductCode(this.planLabel)},
		productName: "nbn | Starter",
		description: "Starter Plus $55/mth",
		currency: "AUD",
		dataValue: "unlimited",
		serviceType: "new",
		category: "broadband",
		contract: "TWELVE_MONTHS",
		contractTerm: "12 Month",
		planLabel: "Starter",
		planSize: "unlimited",
		planSpeed: "50/20",
		planType: "",
		quantity: 1,
		price: {
		priceValue: 55,
		priceUnit: "$",
		calculatedMTC: 660
		}
	},{
		// Starter $60 12 Month by Month
		get productCode(){return fbb.getProductCode(this.planLabel)},
		productName: "nbn | Starter",
		description: "Starter $60/mth",
		currency: "AUD",
		dataValue: "unlimited",
		serviceType: "new",
		category: "broadband",
		contract: "MONTH_BY_MONTH",
		contractTerm: "Month by Month",
		planLabel: "Starter",
		planSize: "unlimited",
		planSpeed: "50/20",
		planType: "",
		quantity: 1,
		price: {
			priceValue: 60,
			priceUnit: "$",
			calculatedMTC: 120
		}
	}]
};
//console.log('BT-BT-24900',fbb.getSelectedPlan());
return fbb;
