// If the hotjar script isn't ready yet, create a queue for pending events.
window.hj=window.hj||function(){(hj.q=hj.q||[]).push(arguments)};

if(typeof s==="object"){
	BELONG.hjTags=[];
	BELONG.hjTags.push = function() {
	  Array.prototype.push.apply(this, arguments);
	}
	var get = function(path, data){
		var i, path = (typeof path === "string") ? path.split('.') : path;
		var len = path.length;
		for (i = 0; typeof data === 'object' && data !==null && i < len; ++i) {data = data[path[i]];}
		return data;
	};
	var containsEvent = function(event, eventsList){
		var eventsArray = eventsList.split(',');
		return eventsArray.some(function(eventsArrayElement){return eventsArrayElement === event});
	},
	tagHotjarRecording = function(variable, condition, tagLabel){
		var sendTag = false;
		if(typeof s.events === "string" && /^event/.test(variable) && containsEvent(event, s.events)){
			sendTag = true;
		}else if(/^(prop|eVar|pageName|custom)/.test(variable)){
			if(condition.constructor===RegExp && condition.test(s[variable])){
				sendTag=true;
			}else if(s[variable] === condition){
				sendTag=true;
			}else if(variable==='custom'){
				sendTag=true;
			}
		}else if(variable==="target"){
			var responseTokens = get('responseTokens',BELONG)||[];
			if(responseTokens.length===0){
				return false; // Exit early if the response from target is empty.
			}else{
				for(var i = 0; i < responseTokens.length; i++){
					if(typeof responseTokens[i] === "undefined"){continue}
					var activity, experience, tntLabel;
					activity = responseTokens[i]['activity.name']||"";
					experience = responseTokens[i]['experience.name']||"";
					tntLabel = (activity+" : "+experience).substring(0,49);
					if(!BELONG.hjTags[tntLabel]){
						BELONG.hjTags.push(tntLabel);
						_satellite.notify('['+tntLabel+'] sent to Hotjar');
						hj('tagRecording', [tntLabel]);
					}
				}
			}
		}
		if(sendTag===true && !BELONG.hjTags[tagLabel]){
			BELONG.hjTags.push(tagLabel);
			_satellite.notify('['+tagLabel+'] sent to Hotjar because '+variable+'='+condition,1);
			hj('tagRecording', [tagLabel]);
		}
	};

	// When we see the following events, push the tag label to hotjar.
	tagHotjarRecording('prop26', 'liveagent-chat:click:liveagent toggle chat modal opened', 'Global Nav Chat Button Clicked');
	tagHotjarRecording('prop26', 'online-chat-invite:popup:shown', 'Proactive Chat Button Shown');
	tagHotjarRecording('prop26', /^sq-failure:click:/, 'SQ Failure');
	tagHotjarRecording('target','','BT-nnn');
	// Sometimes if you return to SQ are a period of inactivity, a sorry message will appear.
	if (/interactive|complete/i.test(document.readyState && typeof jQuery === "function")) {
		if(jQuery('#hdr-retrieve-server-error:visible').length){
			(BELONG.analytics||{}).hdrErrorTracked=(BELONG.analytics||{}).hdrErrorTracked||false;
			if(!BELONG.analytics.hdrErrorTracked){
				tagHotjarRecording('custom', '', 'HDR Retrieve Server Error');
				BELONG.analytics.hdrErrorTracked=true
				$(document).trigger("analytics-trackEvent", {
					eventType: "productbuilderavailability",
					eventCategory: "error",
					eventAction: "load",
					eventLabel: "HDR Retrieve Server Error"
				})
			}
		}
	}
}