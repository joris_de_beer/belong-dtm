var processingType = "join-flow";

if (typeof s.getPreviousValue !== 'undefined') {
	var s_storedProdString = s.getPreviousValue("",'gpv_ps');
	if (typeof(s_storedProdString) != 'undefined' && s_storedProdString && s_storedProdString != 'no value') {
		s.products = s_storedProdString;
		processingType = "account-page";
	}
}

if (processingType == "join-flow") {
	// products - can't be done through friendly UI
	var prodArray = [];
	var revProdMatch = "";
	// Service class
	try {
		var serviceClassVal = "";
		if (typeof BELONG.dataLayer.event.serviceClass != 'undefined' && BELONG.dataLayer.event.serviceClass != null) {
			serviceClassVal = BELONG.dataLayer.event.serviceClass;
		}
		if (serviceClassVal) prodArray.push("service-class:"+serviceClassVal);
	}
	catch(err) {
		//
	}
	// Plan
	try {
		var planSizeType = "";
		if (typeof BELONG.dataLayer.event.planSizeType != 'undefined' && BELONG.dataLayer.event.planSizeType != null) {
			planSizeType = BELONG.dataLayer.event.planSizeType;
		}
		if (planSizeType) {
			valToPush = planSizeType;
			prodArray.push(valToPush);
		}
		/*
		if (planSizeType) {
			var valToPush = planSizeType+"-total-min-cost";
			prodArray.push(valToPush);
			revProdMatch = valToPush;
		}
		*/
	}
	catch(err) {
		//
	}
	// Plan Speed
	try {
		var planSpeedChk = "";
		if (typeof BELONG.dataLayer.event.withSpeedBoost == 'string') {
			planSpeedChk = BELONG.dataLayer.event.withSpeedBoost;
		}
		if (planSpeedChk === "true") {
			if (typeof BELONG.dataLayer.event.planSpeed != 'undefined' && BELONG.dataLayer.event.planSpeed != null && BELONG.dataLayer.event.planSpeed) {
				var valToPush = "speed:"+BELONG.dataLayer.event.planSpeed;
				prodArray.push(valToPush);
			}
		}
	}
	catch(err) {
		//
	}
	// Contract
	try {
		var planContract = "";
		if (typeof BELONG.dataLayer.event.withContract != 'undefined' && BELONG.dataLayer.event.withContract != null) {
			planContract = BELONG.dataLayer.event.withContract;
		}
		if (planContract === "true") {
			prodArray.push("contract:yes");
		}
		else if (planContract === "false") {
			prodArray.push("contract:no");
		}
	}
	catch(err) {
		//
	}
	// Voice Plan
	try {
		var voicePlan = "";
		if (typeof BELONG.dataLayer.event.voicePlan != 'undefined' && BELONG.dataLayer.event.voicePlan != null) {
			voicePlan = BELONG.dataLayer.event.voicePlan;
		}
		if (voicePlan) {
			var valToPush = "voice-plan:"+voicePlan;
			prodArray.push(valToPush);
		}
	}
	catch(err) {
		//
	}
	// Loop through the array
	s.products = "";
	try {
		for (var i=0;i<prodArray.length;i++) {
			if (i>0) s.products = s.products + ",";
			s.products = s.products + ";" + prodArray[i] + ";1;0";
			/*
			if (revProdMatch == prodArray[i] && typeof BELONG.dataLayer.totalMinCost != 'undefined' && BELONG.dataLayer.totalMinCost > 0) {
				s.products = s.products + ";" + BELONG.dataLayer.totalMinCost.toString();
			}
			else {
				s.products = s.products + ";0";
			}
			*/
		}
		if (typeof BELONG.dataLayer.totalMinCost != 'undefined' && BELONG.dataLayer.totalMinCost > 0) {
			s.products = s.products + ",;total-min-cost;1;" + BELONG.dataLayer.totalMinCost.toString();
		}
		// Set the product string for processing on the next page
		if (typeof s.getPreviousValue !== 'undefined') {
			// Set the product string for use by the subsequent page load
			var s_varProductString = s.getPreviousValue(s.products,'gpv_ps');
			// Set a flag via the getPreviousValue function to indicate a purchase has just been made
			var s_varOrderCompleted = s.getPreviousValue("order-complete",'gpv_oc');
			// Set the join customer id for use by the subsequent page load
			if (typeof BELONG != 'undefined' && typeof BELONG.dataLayer != 'undefined' && typeof BELONG.dataLayer.event != 'undefined' && typeof BELONG.dataLayer.event.userid != 'undefined' && BELONG.dataLayer.event.userid) {
				var s_dlBc = BELONG.dataLayer.event.userid;
				var s_varBc = s.getPreviousValue(s_dlBc,'gpv_bc');
			}
		}
	}
	catch(err) {
		//
	}
}

/* BT-24403 Ref #4 Pay and Confirm Order */
var fbb = _satellite.getVar("fbbProductCatalogue"),
	digitalData = window.digitalData || {},
	_event = ((window.BELONG || {}).dataLayer || {}).event || {},
	eventLabel = _satellite.getVar('eventLabel'),
	totalMinCost = BELONG.dataLayer.totalMinCost;

digitalData.event = digitalData.event || [];
digitalData.event.push({
	eventName: "analytics-event",
	userInteraction: "click",
	textContent: "Continue",
	journey: "enter",
	checkoutStep: "personalDetails",
	product: fbb.restoreProductData(),
	transaction: {
		orderId: _satellite.getVar('customerIdJoin'),
		total: {
			basePrice: totalMinCost - (totalMinCost / 11),
			voucherCode: _satellite.getVar('promoCode'),
			voucherDiscount: _satellite.getVar('promoCode') ? 20 : 0,
			currency: 'AUD',
			taxRate: 0.1,
			priceWithTax: totalMinCost,
			transactionTotal: totalMinCost
		},
		paymentOption: _satellite.readCookie('paymentMethod')
	}
});
if (_satellite.settings.notifications) {
	console.log(
		"BT-24403 Ref #4 Pay and Confirm Order",
		"digitalData.event[n]:",
		JSON.stringify(digitalData.event[digitalData.event.length - 1], null, "\t")
	);
}
