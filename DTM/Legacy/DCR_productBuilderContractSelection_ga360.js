/* BT-24403 Ref #2 NBN Plans Selection */
var fbb = _satellite.getVar("fbbProductCatalogue"),
	digitalData = window.digitalData || {},
	snapshot = ((window.BELONG||{}).dataLayer||{}).snapshot;

var eventData = {
	eventName: "analytics-event",
	userInteraction: "click",
	textContent: fbb.getProductData().description,
	journey: "entice",
	checkoutStep: "pdp",
	product: fbb.getProductData(),
};

var eventSnapshot = JSON.stringify(eventData, null, "\t");

digitalData.event = digitalData.event || [];
arrayLength = digitalData.event.length;
// Sometimes these events come through twice, so use snapshotting to only capture 1 unique event.
if(snapshot!==eventSnapshot){
	digitalData.event.push(eventData);
	if(_satellite.settings.notifications){
		console.log('BT-24403 Ref #2 NBN Plans Selection','digitalData.event['+arrayLength+']:',eventSnapshot)
	}
}
window.BELONG.dataLayer.snapshot = eventSnapshot;
