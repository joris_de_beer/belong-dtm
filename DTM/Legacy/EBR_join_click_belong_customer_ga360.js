/* BT-24403 Ref #9 Click as Belong Customer */
var fbb = _satellite.getVar("fbbProductCatalogue"),
	digitalData = window.digitalData || {},
	_event = ((window.BELONG||{}).dataLayer||{}).event||{};

digitalData.event = digitalData.event || [];
digitalData.event.push({
	eventName: "analytics-event",
	userInteraction: "click",
	textContent: this.innerText,
	linkTarget:  (this.innerText == "I am a Belong Mobile customer") ? "/login?redirect=%2Fbroadband%2Fjoin%3Fproductbuilder%3Dtrue" : "/login",
	journey: "enter",
	product: fbb.restoreProductData(),
});
if(_satellite.settings.notifications){
	console.log('BT-24403 Ref #9 Click as Belong Customer','digitalData.event[n]:',JSON.stringify(digitalData.event[digitalData.event.length-1], null, "\t"))
}
