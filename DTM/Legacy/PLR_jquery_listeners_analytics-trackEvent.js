/* PLR: jQuery trigger listeners > nalytics-trackEvent */
if(typeof(jQuery)!=="undefined" && typeof(jQuery.fn.on)!=="undefined") {

	jQuery(document).ready(function() {
		jQuery(document).on("analytics-trackEvent", function(event, eventData) {
			var triggerName = "analytics-trackEvent",
				eventData = eventData || {};
			document.dispatchEvent(new CustomEvent('oTNT-trackEvent',{detail:eventData}));

			/*
			The purpose of this code is to:
			1. Transform the event trigger data into the belongDataLayer
			2. Call the DTM direct call rule to make the relevant tracking call
			*/
			if(_satellite.custom_debugMode) console.debug('Triggered '+triggerName, event, eventData);
			// Check for the belongDataLayer
			if (typeof _satellite.setupBelongDataLayer == 'function') _satellite.setupBelongDataLayer();
			// Clear the belongDataLayer.event
			if (typeof _satellite.clearBelongEventData == 'function') _satellite.clearBelongEventData();
			// Process the eventData
			if (typeof _satellite.processBelongEventData == 'function') _satellite.processBelongEventData(eventData, _satellite.custom_debugMode);
			/////////////////////////////////////////////////////////////
			// Check the eventType against a list of known
			if (typeof eventData.eventType !== 'undefined' && eventData.eventType) {
				var switchEventType = eventData.eventType.toLowerCase();
				////////////////////////////////////////
				// Conditional checks for SQ
				////////////////////////////////////////
				// Ignore subsequent checks from triggering sq events)
				try {
					var eventLabelVal = eventData.eventLabel.toLowerCase();
					var eventLabelCheck = /\-2$/i;
					if (eventLabelCheck.test(eventLabelVal)) switchEventType = "globalsqgeneric";
				}
				catch(err) {
				}
				// Ignore nbn service not available fails)
				try {
					var eventLabelVal = eventData.eventLabel.toLowerCase();
					var eventLabelCheck = /nbn\-service_not_available|nbn\-sq_error/i;
					if (eventLabelCheck.test(eventLabelVal)) switchEventType = "globalsqgeneric";
				}
				catch(err) {
				}
				if (/globalSqLandlineOption|globalSqFindOutMoreOrJoinNow|globalSqFailExperionError|globalSqOpenModal|productBuilderCheckAddressClick/i.test(switchEventType)) switchEventType = "globalsqgeneric";
				////////////////////////////////////////
				// Product Builder event types
				////////////////////////////////////////
				if (/productBuilderSpeedboostModalSelection|productBuilderSpeedboostSelection/i.test(switchEventType)) switchEventType = "productbuilderspeedboost";
				if (/productBuilderVoiceModalSelection|productBuilderVoiceSelection/i.test(switchEventType)) switchEventType = "productbuildervoice";
				////////////////////////////////////////
				// Ignore list
				////////////////////////////////////////
				if (/productBuilderSqDone|productBuilderSqEdit|productBuilderContractModalClose|productBuilderVoiceModalClose|productBuilderSpeedboostModalClose|productBuilderPlanClose|productBuilderPhoneLineModalClose/i.test(switchEventType)) switchEventType = "ignorelist";

				////////////////////////////////////////
				// Standard event switch logic
				////////////////////////////////////////
				if(_satellite.custom_debugMode) console.info('Switching on event:', switchEventType);
				switch (switchEventType) {
					// SQ related events
					case "globalsqcheck":
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-tracksqCheck');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-tracksqCheck');
					   break;
					case "globalsqselectaddress":
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-tracksqAddress');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-tracksqAddress');
					   break;
					case "globalsqfailfailuremessages":
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-tracksqFail');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-tracksqFail');
					   break;
					case "globalsqsuccess":
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-tracksqSuccess');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-tracksqSuccess');
					   break;
					case "globalsqgeneric":
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-tracksqGeneric');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-tracksqGeneric');
					   break;
					// Join Flow related steps
					case "pagesjoinsteps":
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackJoinStep');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackJoinStep');
					   break;
					case "pagesjoinconfirmyourorder":
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackJoinComplete');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackJoinComplete');
					   break;
					// Account upgrade related steps
					case "confirmbelongvoiceorderclick":
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackAccountAddVoice');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackAccountAddVoice');
					   break;
					// Hidden phone number
					case "globalrevealphonenumber":
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackGlobalRevealPhoneNumber');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackGlobalRevealPhoneNumber');
					   break;
					// Product call me back
					case "callbacksubmit":
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackCallbackSubmit');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackCallbackSubmit');
					   break;
					// Troubleshooting card flow
					case "troubleshootingcardslog":
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackTroubleshootingCards');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackTroubleshootingCards');
					   break;
					case "troubleshootingsubmitcase":
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackTroubleshootingContact');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackTroubleshootingContact');
					   break;
					case "troubleshootingspeeddata":
						var eventData=(eventData||{}).eventData||{}
						eventLabel="lat:"+eventData.latency+";jit:"+eventData.jitter+";down:"+eventData.download+";upload:"+eventData.upload+";server:"+eventData.testServer;
						BELONG.dataLayer.event.eventLabel = eventLabel;
						if(_satellite.custom_debugMode) console.debug('troubleshootingspeeddata', BELONG.dataLayer.event)
						_satellite.track('analytics-trackCustomLink');
					   break;

					// Manage Service triggers
					case "manageservicedisplay":
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackManageServiceDisplay');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackManageServiceDisplay');
					   break;
					case "manageservicereview":
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackManageServiceReview');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackManageServiceReview');
					   break;
					case "manageserviceupdate":
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackManageServiceUpdate');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackManageServiceUpdate');
					   break;
					// Manage Account Payment triggers
					case "paymentdetailsupdate":
						// Set the label field
						try {BELONG.dataLayer.event.eventLabel = "update";}
						catch(err) {}
						try {
							// Was it a success
							if (BELONG.dataLayer.event.success === true) {
								if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackPaymentDetailsUpdateSuccess');
								if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
								_satellite.track('analytics-trackPaymentDetailsUpdateSuccess');
							}
							else {
								if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackPaymentDetailsUpdate');
								if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
								_satellite.track('analytics-trackPaymentDetailsUpdate');
							}
						}
						catch(err) {}
					   break;
					case "paymentdetailsretry":
						// Set the label field
						try {BELONG.dataLayer.event.eventLabel = "retry";}
						catch(err) {}
						try {
							// Was it a success
							if (BELONG.dataLayer.event.success === true) {
								if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackPaymentDetailsUpdateSuccess');
								if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
								_satellite.track('analytics-trackPaymentDetailsUpdateSuccess');
							}
							else {
								if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackPaymentDetailsUpdate');
								if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
								_satellite.track('analytics-trackPaymentDetailsUpdate');
							}
						}
						catch(err) {}
					   break;
					case "paymentdetailsprepaid":
						// Set the label field
						try {BELONG.dataLayer.event.eventLabel = "prepaid";}
						catch(err) {}
						try {
							// Was it a success
							if (BELONG.dataLayer.event.success === true) {
								if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackPaymentPrePaySuccess');
								if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
								_satellite.track('analytics-trackPaymentPrePaySuccess');
							}
							else {
								if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackCustomLink');
								BELONG.dataLayer.event.eventLabel = "prepaid error";
								if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
								_satellite.track('analytics-trackCustomLink');
							}
						}
						catch(err) {}
					   break;
					// Update details steps
					case "personaldetailsupdateemail":
						try {BELONG.dataLayer.event.eventLabel = "email";}
						catch(err) {}
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackPersonalDetailsUpdate');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackPersonalDetailsUpdate');
					   break;
					case "personaldetailsupdatemobile":
						try {BELONG.dataLayer.event.eventLabel = "mobile";}
						catch(err) {}
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackPersonalDetailsUpdate');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackPersonalDetailsUpdate');
					   break;
					case "personaldetailsupdatepassword":
						try {BELONG.dataLayer.event.eventLabel = "password";}
						catch(err) {}
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackPersonalDetailsUpdate');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackPersonalDetailsUpdate');
					   break;
					// Product Builder Cases
					case "productbuildercontractselection":
						try {BELONG.dataLayer.event.eventLabel = "Contract [" + BELONG.dataLayer.event.contractOption + "]";}
						catch(err) {}
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackProductBuilderContractSelection');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackProductBuilderContractSelection');
					   break;
					case "productbuilderplanselection":
						try {BELONG.dataLayer.event.eventLabel = "Plan [" + BELONG.dataLayer.event.planSize + "]";}
						catch(err) {}
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackCustomLink');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackCustomLink');
					   break;
					case "productbuilderspeedboost":
						try {BELONG.dataLayer.event.eventLabel = "Speed Boost [" + BELONG.dataLayer.event.speedboostOption + "]";}
						catch(err) {}
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackProductBuilderSpeedboostSelection');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackProductBuilderSpeedboostSelection');
					   break;
					case "productbuildervoice":
						try {BELONG.dataLayer.event.eventLabel = "Select Voice [" + BELONG.dataLayer.event.voicePlan + "]";}
						catch(err) {}
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackCustomLink');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackCustomLink');
					   break;
					case "productbuilderavailability":
						// Set the label field
						try {BELONG.dataLayer.event.eventLabel = BELONG.dataLayer.event.errorCode;}
						catch(err) {}
						try {
								if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackProductBuilderAvailability');
								if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
								_satellite.track('analytics-trackProductBuilderAvailability');
						}
						 catch(err) {}
					   break;
					case "productbuilderjoin":
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackProductBuilderJoin');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackProductBuilderJoin');
					   break;
					case "productbuilderpromocodeapply":
						// Set the label field
						try {BELONG.dataLayer.event.eventLabel = "apply promo code";}
						catch(err) {}
						try {
							// Was it a success
							if (BELONG.dataLayer.event.promocodeValidation.success === true) {
								if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackProductBuilderPromoCodeApplySuccess');
								BELONG.dataLayer.event.eventLabel += " success";
								if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
								_satellite.track('analytics-trackProductBuilderPromoCodeApplySuccess');
							}
							else {
								if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackProductBuilderPromoCodeApplyFail');
								BELONG.dataLayer.event.eventLabel += " error";
								if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
								_satellite.track('analytics-trackProductBuilderPromoCodeApplyFail');
							}
						}
						catch(err) {}
					   break;
					case "productbuilderclosesq":
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackProductBuilderCloseSq');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackProductBuilderCloseSq');
					   break;
					case "ordertracking":
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackOrderTrackingCards');
						   if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackOrderTrackingCards');
					   break
					   // Tracking the Order cards displayed inside Account services.
					case "ordertrackingview":
						_satellite.notify('About to trigger analytics-trackOrderTrackingView');
						BELONG.dataLayer.orderData= eventData.orderData;

						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackOrderTrackingView');
					   break
					   // NES surveys
					   case "nessurveysubmitted":
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackNesSurveySubmitted');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackNesSurveySubmitted');
					   break;
					   case "nessurveyclosed":
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackNesSurveyClosed');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackNesSurveyClosed');
					   break;
					   //Track service type
					   case "analytics-trackservicetype":
						if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackServiceType');
						if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
						_satellite.track('analytics-trackServiceType');
					   break;
					   // Ignore list
					case "ignorelist":
						if(_satellite.custom_debugMode) console.debug('Ignoring trigger');
					   break;
					default:
						// Is there a virtual pageName -> Initiate the direct call rule to make the page tracking call
						if (typeof eventData.virtualPage !== 'undefined' && eventData.virtualPage) {
							if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackVirtualPage');
							if (typeof s != 'undefined' && typeof s.clearVars == 'function') s.clearVars(); // Clear out previous variables for page tracking
							_satellite.track('analytics-trackVirtualPage');
						}
						// Is there an eventCategory -> Initiate the direct call rule to make the custom link call
						else if (typeof eventData.eventCategory !== 'undefined' && eventData.eventCategory) {
							if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackCustomLink');
							_satellite.track('analytics-trackCustomLink');
						}
				}
			}
			else {
				// Log tag errors
				if(_satellite.custom_debugMode) console.debug('About to trigger analytics-trackCustomLink');
				_satellite.track('analytics-eventTagError');
			}
		});

		// Alternate style of tracking developed by Dan Nguyen
		jQuery('body').on("analytics-trackEvent", function(event) {
			var triggerName = "analytics-trackEvent";
			var event=event||{};
			var detail=event.detail||{};
			var eventType=detail.eventType||"UNKNOWN_EVENT_TYPE";
			var eventData=detail.eventData||{};
			var eventStore=detail.eventStore||{};

			if(_satellite.settings.notifications) console.debug('Triggered '+eventType, event, eventData, eventStore);
			// Check for the belongDataLayer
			if (typeof _satellite.setupBelongDataLayer == 'function') _satellite.setupBelongDataLayer();
			// Clear the belongDataLayer.event
			if (typeof _satellite.clearBelongEventData == 'function') _satellite.clearBelongEventData();
			// Process the eventData
			if (typeof _satellite.processBelongEventData == 'function'){
				_satellite.processBelongEventData(eventData, _satellite.settings.notifications);
				_satellite.addBelongDataLayerAttribute(eventStore, _satellite.settings.notifications);
			}

			switch(eventType){
				case (eventType.match(/^(DOCUMENT_UPLOAD_|GET_POD_REQUEST)/) || {}).input:
					_satellite.track('analytics-trackProofOfOccupancy');
				break;
				case (eventType.match(/^(NBN_.*?APPOINTMENT)/) || {}).input:
					_satellite.track('analytics-trackAppointmentSetting');
				break;
				//case (eventType.match(/^(analytics-trackservicetype)/) || {}).input:
				//  _satellite.notify('About to trigger analytics-trackServiceType');
				//	_satellite.track('analytics-trackServiceType');
				//break;
				default:
					//_satellite.notify(eventType, 5);
				break;
			}
		});
	});
}
