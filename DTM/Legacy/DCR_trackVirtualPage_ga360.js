/* BT-24403 Ref #5 and Ref #8 */
var eventData,
	logMessage,
	lastTrackedEvent,
	fbb = _satellite.getVar("fbbProductCatalogue"),
	digitalData = window.digitalData || {},
	_event = ((window.BELONG||{}).dataLayer||{}).event||{};

digitalData.products = fbb.getAllProducts();
digitalData.event = digitalData.event || [];

switch ( _event.eventLabel ) {

	case "Click in Address Field":
		logMessage = "BT-24403 Ref #5 Check address lightbox"
		eventData ={
			eventName: "analytics-event",
			userInteraction: "impression",
			journey: "entice",
			formStep: "0",
			product: fbb.getProductData(),
		};
	break;

	case "Join-Product":
		logMessage = "BT-24403 Ref #8 - Summary of Your Plan"
		eventData ={
			eventName: "analytics-event",
			userInteraction: "impression",
			textContent: "Continue",
			journey: "enter",
			checkoutStep: "viewCart",
			product: fbb.restoreProductData(),
		};
	break;

	default:
		break;
}

if(eventData && _event.eventLabel !== ((window.BELONG||{}).dataLayer||{}).lastTrackedEvent){
	digitalData.event.push(eventData);
	if(_satellite.settings.notifications){
		console.log(logMessage,'digitalData.event[n]:',JSON.stringify(digitalData.event[digitalData.event.length-1], null, "\t"))
	}
	((window.BELONG||{}).dataLayer||{}).lastTrackedEvent = _event.eventLabel;
}
