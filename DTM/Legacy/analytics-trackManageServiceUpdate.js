//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// trackManageServiceUpdate
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
var prodArray = [],
	oldPlan = (((BELONG||{}).dataLayer||{}).event||{}).oldPlan||{},
	newPlan = (((BELONG||{}).dataLayer||{}).event||{}).newPlan||{};

s.eVar101 = JSON.stringify({"oldPlan":oldPlan});
s.eVar102 = JSON.stringify({"newPlan":newPlan});

function getPlanType(obj){
	return (obj.currentPlan && obj.currentPlan.planType) ? obj.currentPlan.planType : false;
}
function getPlanSize(obj){
	return (obj.currentPlan && obj.currentPlan.planSize) ? obj.currentPlan.planSize : false;
}
function getPlanSpeed(obj){
	// if object has a currentPlan key, we're probably looking at an old plan object:
	if(obj.currentPlan){return (obj.currentPlan && obj.currentPlan.planSpeed) ? obj.currentPlan.planSpeed : false; }
	// new plan object stores speed in planSpeed:
	else{return (obj.currentPlan && obj.currentPlan.planSpeed) ? obj.currentPlan.planSpeed : false; }
}
function getContractOption(obj){
	// if object has a contractOption key, we're probably looking at an old plan object
	if(obj.contractOption){return obj.contractOption || false; }
	// contractOption for new plan object is deeply nested:
	else{ return (obj.recurringCharges && obj.recurringCharges.CONTRACT_CREDIT && obj.recurringCharges.CONTRACT_CREDIT.contractOption)?
		obj.recurringCharges.CONTRACT_CREDIT.contractOption : false; }
}

// Product Change details
try {
	var planType			= getPlanType( oldPlan ) || getPlanType(newPlan),
		planSizeOld			= getPlanSize( oldPlan ),
		planSizeNew			= getPlanSize( newPlan ),
		planSpeedOld		= getPlanSpeed(oldPlan ),
		planSpeedNew		= getPlanSize( newPlan ),
		planContractTermOld	= getContractOption( oldPlan ),
		planContractTermNew	= getContractOption( newPlan );

	// Prepare the values
	var valToPush = "manage-service";
	if (planType) valToPush = valToPush + " : " + planType;
	if (planSizeOld != planSizeNew) {
		valToPush = valToPush + " : change planSize : from "+(planSizeOld||"")+" : to "+(planSizeNew||"");
		prodArray.push(valToPush);
	}
	var valToPush = "manage-service";
	if (planType) valToPush = valToPush + " : " + planType;
	if (planSpeedOld != planSpeedNew) {
		valToPush = valToPush + " : change planSpeed : from "+(planSpeedOld||"")+" : to "+(planSpeedNew||"");
		prodArray.push(valToPush);
	}
	var valToPush = "manage-service";
	if (planType) valToPush = valToPush + " : " + planType;
	if (planContractTermOld != planContractTermNew) {
		valToPush = valToPush + " : change contract : from "+(planContractTermOld||"")+" : to "+(planContractTermNew||"");
		prodArray.push(valToPush);
	}
}
catch(err) {
	s.eVar91 = s_tagErrors = "Product Change details:"+_satellite.getVar("tagError")(err);
}
// Loop through the array
s_products = "";
try {
	for (var i=0;i<prodArray.length;i++) {
		if (i>0) s_products = s_products + ",";
		s_products = s_products + ";" + prodArray[i];
	}
}
catch(err) {
	s.eVar91 = s_tagErrors = "Loop through the array"+_satellite.getVar("tagError")(err);
}
