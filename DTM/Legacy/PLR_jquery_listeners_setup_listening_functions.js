/* PLR: jQuery trigger listeners > Setup listening functions */
if(typeof(jQuery)!=="undefined") {
		jQuery(document).ready(function() {
		// Print debug information if we're loading the staging library
		try {debugMode=(_satellite.configurationSettings.settings.isStaging === true)?true:false;}
		catch(err) {debugMode = false;}

		try{debugMode=localStorage.getItem('sdsat_debug')==="true"?true:false}
		catch(err) {debugMode = false;}

		_satellite.custom_debugMode=(debugMode && (typeof console !== 'undefined' || typeof console.debug !== 'undefined'))?true:false;
		//debugMode = true; // Uncomment to force debug mode
		_satellite.setupBelongDataLayer = function() {
			if (typeof(BELONG) !== 'undefined') {
				if (typeof(BELONG.dataLayer) === 'undefined') BELONG.dataLayer = {};
				if (typeof(BELONG.dataLayer.event) === 'undefined') BELONG.dataLayer.event = {};
			}
			else {
				// TODO Log tag errors
			}
		}

		_satellite.processBelongEventData = function(eventData, debugMode) {
			for (var property in eventData) {
				if (eventData.hasOwnProperty(property)) {
					_satellite.addBelongDataLayerAttribute(eventData, property, debugMode);
				}
			}
		}

		_satellite.clearBelongEventData = function() {
			if (typeof(BELONG.dataLayer.event) !== 'undefined') BELONG.dataLayer.event = {};
		}

		_satellite.addBelongDataLayerAttribute = function(eventData, attribute, debugMode) {
			if (typeof eventData == 'object') {
				if (eventData.hasOwnProperty(attribute)) {
					if(debugMode) console.debug('eventData.'+attribute+': '+eventData[attribute]);
					BELONG.dataLayer.event[attribute] = eventData[attribute];
				}
			}
		}
	});
}
