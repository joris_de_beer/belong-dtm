function loadBelongPromoMbox() {
	var mboxName="belong-promo-mbox";
	var c_r=function C(k){var ckV=(document.cookie.match('(^|; )'+k+'=([^;]*)')||0)[2];if(ckV){return decodeURIComponent(ckV)}else{return ""}};
	var customerId=c_r('s_v16')?"'profile.belongCustomerId=" + c_r('s_v16') + "'," : ""
	var belongSegments=typeof s !== "undefined"?s.list1:c_r('aam_tn_bl');
	var mboxDiv=document.createElement('div');
	mboxDiv.setAttribute('id', 'mboxPromo');
	mboxDiv.setAttribute('data-mbox', mboxName);
	var mboxDefineJS=document.createElement("script");
	mboxDefineJS.setAttribute('type', 'text/javascript');
	mboxDefineJS.textContent="mboxDefine('mboxPromo', '" + mboxName + "');" +
		"mboxUpdate('" +
		"" + mboxName + "'," + customerId +
		"'path=" + window.location.pathname + "'," +
		"'profile.belongSegments=" + belongSegments + "'," +
		"'cookie=" + document.cookie + "'" +
		");";
	document.body.appendChild(mboxDiv);
	document.body.appendChild(mboxDefineJS);
	_satellite.notify('Mbox: ' + mboxName + ' [path=' + window.location.pathname + '], [belongSegments=' + belongSegments + ']', 3);
}

if (/interactive|complete/i.test(document.readyState)) {
	loadBelongPromoMbox();
}
else {
	document.addEventListener('DOMContentLoaded', function(){
		loadBelongPromoMbox();
	});
}