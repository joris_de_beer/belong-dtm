/* BT-24403 Ref #3 Click Join button */
var fbb = _satellite.getVar("fbbProductCatalogue"),
	digitalData = window.digitalData || {};

digitalData.event = digitalData.event || [];
digitalData.event.push({
	eventName: "analytics-event",
	userInteraction: "add to cart",
	textContent: "Join Now",
	linkTarget: "/broadband/join",
	journey: "entice",
	checkoutStep: "pdp",
	product: fbb.getProductData(),
});
if(_satellite.settings.notifications){
	console.log('BT-24403 Ref #3 Click Join button','digitalData.event[n]:',JSON.stringify(digitalData.event[digitalData.event.length-1], null, "\t"))
}
