// Initialise data layer.
var eventData,
	fbb = _satellite.getVar("fbbProductCatalogue"),
	digitalData = window.digitalData || {};

digitalData.products = fbb.getAllProducts();
digitalData.event = digitalData.event || [];

switch (location.pathname) {

	case "/broadband/join":
		eventData = {
			eventName: "analytics-event",
			userInteraction: "impression",
			textContent: "Continue",
			journey: "enter",
			checkoutStep: "viewCart",
			product: fbb.getProductData(),
		}
	break;

	default:
		break;
}

if(eventData){
	digitalData.event.push(eventData);
}

if (_satellite.settings.notifications) {
	console.log("BT-24900:Broadband Signup Flow",
		"\npathname:", location.pathname,
		"\nevent[n]", JSON.stringify(digitalData.event[digitalData.event.length-1], null, "\t")
	);
}
