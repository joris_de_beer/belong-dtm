//////////////////////////////////////////////////
function processSupportPageData() {
	var pathname = document.location.pathname,
		/* Custom page detection */
		hostname = document.location.hostname,
		// Setup the taxonomy array
		s_taxArr=new Array();

	//if (hostname.indexOf('support.belong.com.au') >= 0) {
	//	s_taxArr.push("faq-public");
	//}

	var dirPairs = pathname.split("/");
	for (i=1;i<dirPairs.length;i++) {
		var dirValue = dirPairs[i];
		if (dirValue){
			dirValue = dirValue.toLowerCase();
			//dirValue = s_removeAllSpecialChars(dirValue);
			dirValue=dirValue.replace(/^\s+/,""); // Replace leading space
			dirValue=dirValue.replace(/ /g,"-"); // Replace spaces with -
			s_taxArr.push(dirValue);
		}
	}
	_satellite.notify("Support Pages:s_taxArr:"+s_taxArr.toString(),1);
	if(typeof jQuery === 'undefined'){
		s_logTagErrorsFn("jQuery missing at time of execution",1);
	}

	/* Build the page name from the taxonomy array */
	s_pageName = "";
	for (var i=0;i<s_taxArr.length;i++) {
		if (i==0){
			s_pageName=s_taxArr[i];
		}else {
			s_pageName = s_pageName+":"+s_taxArr[i];
		}
	}
	// Output the data
	if(typeof s_pageName != 'undefined') {
		s_pageName = "bl:"+s_pageName;
	}

	window.addEventListener('load', function(){
		var getPathname = function(href) {
			var location = document.createElement("a");
			location.href = href;
			if (location.host == "") {location.href = location.href;}
			return location.pathname;
		};
		window.BELONG = (window.BELONG)?window.BELONG:{};
		window.BELONG.dataLayer=(window.BELONG.dataLayer)?window.BELONG.dataLayer:{};
		// Handle Accordions
		$(document).on('belong.analytics.knowledge.openFaq',function(event,data){
			BELONG.dataLayer.openFaq={'event':event, 'data':data}; // only used to debug
			s.pageName = "bl"+getPathname(data.faqUrl).replace(/\//g,":");
			_satellite.track('analytics-openFaq');
		});
		// Handle UpVotes
		$(document).on('belong.analytics.knowledge.thumbsUp',function(event,data){
			BELONG.dataLayer.thumbsUp={'event':event, 'data':data}; // only used to debug
			s.pageName = "bl"+getPathname(data.faqUrl).replace(/\//g,":");
			_satellite.track('analytics-faq-thumbsUp');

		});
		// Handle DownVotes
		$(document).on('belong.analytics.knowledge.thumbsDown',function(event,data){
			BELONG.dataLayer.thumbsDown={'event':event, 'data':data};
			s.pageName = "bl"+getPathname(data.faqUrl).replace(/\//g,":");
			_satellite.track('analytics-faq-thumbsDown');
		});
	});

} /* End processSupportPageData */


if (typeof(processSupportPageData) == "function") {
	processSupportPageData();
}