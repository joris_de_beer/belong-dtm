/* PLR: fixed broadband marketing scripts */
window.dataLayer= window.dataLayer || [];
window.gtag		= function(){dataLayer.push(arguments)};

/* GA - Google Tag Manager Script for OMD */
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var
f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K9CVSMX');

/* DCM - Google Tag Manager Script for OMD */
_satellite.loadScript(
	'//www.googletagmanager.com/gtag/js?id=DC-8705912',
	function(){
		gtag('js', new Date());
		gtag('config', 'DC-8705912');
	}
);

/* DCM: 7488159|Belong_Brand_Audiences_All Pages|Audiences */
if(location.hostname === "www.belong.com.au"){
	gtag('event', 'conversion', {
		'allow_custom_scripts': true,
		'u5': location.href,
		'send_to': 'DC-8705912/audie0/belon0+standard'
	});
}

/* DCM: 7488156|Belong_Broadband_Landing Page_Broadband|Sales */
if(location.hostname === "www.belong.com.au" && location.pathname ==="/broadband/"){
	gtag('event', 'conversion', {
		'allow_custom_scripts': true,
		'u5': location.href,
		'send_to': 'DC-8705912/landi0/belon00+unique'
	});
}

/* DCM: 7488156|Belong_Broadband_Landing Page_Broadband|Sales */

if(location.pathname ==="/account/setup"){
	if(typeof s==="object" && typeof s.get==="function"){
		var orderData=s.get('BELONG.dataLayer.event.eventData.order',window)||{};
		var dcmCode=orderData.planType==="NBN"?"belon000":"belon001";
		if(_satellite.settings.notifications){
				console.log('DCR_conversion_pixel',orderData);
		}
		gtag('event', 'purchase', {
			'allow_custom_scripts': true,
			'value': '0',
			'transaction_id': s.get('BELONG.dataLayer.customerID',window)||"",
			'u1': orderData.planType||"",
			'u2': orderData.planSize||"",
			'u3': orderData.contractTerm||"",
			'u4': orderData.nbnPlanSpeed||"",
			'send_to': 'DC-8705912/sales0/'+dcmCode+'+transactions'
		});
	}
}

/* DCM: 7485561|Belong_Broadband_Step 1_FBB Product Page ADSL|Steps */
if(location.hostname === "www.belong.com.au" && /broadband\/adsl(\/|$)/.test(location.pathname)){
	gtag('event', 'conversion', {
		'allow_custom_scripts': true,
		'u5': location.href,
		'send_to': 'DC-8705912/steps0/belon003+unique'
	});
}

/* DCM: 7521847|Belong_Broadband_Step 1_FBB Product Page NBN|Steps */
if(location.hostname === "www.belong.com.au" && /broadband\/nbn(\/|$)/.test(location.pathname)){
	gtag('event', 'conversion', {
		'allow_custom_scripts': true,
		'u5': location.href,
		'send_to': 'DC-8705912/steps0/belon002+unique'
	});
}

/* DCM: 7520368 + 7520365|Belong_Broadband_Step 2_Join ADSL OR NBN|Steps */
if(location.hostname==="www.belong.com.au" && location.pathname==="/broadband/join" && location.search==="?productbuilder=true"){
	var send_to = _satellite.getVar('planType')==="nbn" ? "DC-8705912/steps0/belon004+standard" : "DC-8705912/steps0/belon005+standard"
	gtag('event', 'conversion', {
		'allow_custom_scripts': true,
		'u1': _satellite.getVar('planType'),
		'u2': '[Broadband Choose Data]',
		'u3': '[Broadband Contract Save]',
		'u4': '[Broadband Speed Boost]',
		'send_to': send_to
	});
}



/* Facebook Pixel Code */
try{
    var belongFacebookPixelId = "816534751879197";
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', belongFacebookPixelId );
    fbq('track', 'PageView');
}catch(e){
 // oh well...
  _satellite.notify('FB Pixel Error. Carry on.',1);
}

// Adalyser Marketing Script. Requested by elli.mcdougall@omd.com 15/6/18
(function(windowAlias, documentAlias, trackerName) {
if (!windowAlias[trackerName]) {
	windowAlias.GlobalAdalyserNamespace = windowAlias.GlobalAdalyserNamespace
			|| [];
	windowAlias.GlobalAdalyserNamespace.push(trackerName);
	windowAlias[trackerName] = function() {
		(windowAlias[trackerName].q = windowAlias[trackerName].q || []).push(arguments)
	};
	windowAlias[trackerName].q = windowAlias[trackerName].q || [];
	var nel = documentAlias.createElement("script"),
		fel = documentAlias.getElementsByTagName("script")[0];
	nel.async = 1;
	nel.src = "//c5.adalyser.com/adalyser.js?cid=belong";
	fel.parentNode.insertBefore(nel, fel)
}
}(window, document, "adalyserTracker"));

window.adalyserTracker("create", {
	campaignCookieTimeout: 15552000,
	conversionCookieTimeout: 604800,
	clientId: "belong",
	trafficSourceInternalReferrers: ["^(.*\\.)?belong.com$"]
});
window.adalyserTracker("trackSession", "lce1", {});
