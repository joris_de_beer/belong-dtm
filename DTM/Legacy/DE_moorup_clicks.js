var clickData = {
	el:			this,
	evt:		event,
	panel:		this.closest('.mat-expansion-panel'),
	tag:		this.tagName,
	text:		String(this.innerText).trim(),
	classes:	this.className,
	classList:	this.classList,
	elClicked:	event.path[0]
};
window.digitalData.clickData=clickData;
console.log('DE_moorup_clicks', clickData);

_satellite.setVar('event_category','moorup buy form');
_satellite.setVar('event_action','click');
_satellite.setVar('event_label',clickData.text);

// Clicks on Links
if(clickData.tag==="A"){
	_satellite.setVar('event_action','link click');

// Clicks on Text fields
}else if(clickData.tag==="INPUT"){
	_satellite.setVar('event_action','field touch');

// Clicks on Buttons
}else if(clickData.tag==="BUTTON"){
	_satellite.setVar('event_action','button click');

// Clicks on Text fields inside panels
}else if(clickData.elClicked.tagName==="INPUT"){
	if(clickData.txtArray.length>0){
		_satellite.setVar('event_category',clickData.txtArray[0]);
	}
	_satellite.setVar('event_action','field touch');
	_satellite.setVar('event_label',clickData.elClicked.innerText);

// Clicks on Panels
}else if(clickData.tag==="MAT-EXPANSION-PANEL"){
	_satellite.setVar('event_action','panel opened');
	clickData.txtArray = clickData.text.split('\n');
	if(clickData.txtArray.length>0){
		_satellite.setVar('event_label',clickData.txtArray[0]);
	}
}
_satellite.setVar('click_data',JSON.stringify(clickData, function (k, v) { return k ? "" + v : v; }));
return true;