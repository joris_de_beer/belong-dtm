// This data element checks whether the hit is from the Belong iOS or Android app.
// Cribbed from Belong UI _target-mobile-app.js
// NOTE, the Page Load Rule conditions expect a string, so return boolean as string :-/
var standalone = window.navigator.standalone, // On Belong iOS, navigator has a 'standalone' key.
    iOS = navigator.userAgent.match(/iPhone|iPad|iPod/i),
    android = navigator.userAgent.match(/Android/i),
    safari = /safari/.test(navigator.userAgent.toLowerCase()),
    // User agent suffix - To target BELONG android app
    ua_suffix  = navigator.userAgent.match(/BelongApp\/[0-9\.]+$/); // On Belong Android, the UA is modified.
	returnValue = "FALSE" // Adobe DTM data elements use stringy booleans (aaargh WTF??)

if (iOS){
    if (!standalone && !safari) { returnValue = "TRUE"; }
}else if(android) {
    if (ua_suffix){  returnValue =  "TRUE"; }
}
// Return a stringy boolean for whether the current page is being viewed inside a Belong App WebView.
return returnValue;
