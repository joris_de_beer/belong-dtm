window.dataLayer= window.dataLayer || [];
window.gtag		= function(){dataLayer.push(arguments)};

if(typeof s==="object" && typeof s.get==="function"){
	var orderData=s.get('BELONG.dataLayer.event.eventData.order',window)||{};
	if(_satellite.settings.notifications){
		console.log('DCR_conversion_pixel',orderData);
	}
	gtag('event', 'purchase', {
		'allow_custom_scripts': true,
		'value': '0',
		'transaction_id': s.get('BELONG.dataLayer.customerID',window)||"",
		'u1': orderData.planType||"",
		'u2': orderData.planSize||"",
		'u3': orderData.contractTerm||"",
		'u4': orderData.nbnPlanSpeed||"",
		'send_to': 'DC-8705912/sales0/belon001+transactions'
	});
	// Facebook Pixel (Accuen account)
	if(typeof window.fbq === "function"){
		fbq('track', 'Purchase', {
			content_name: (orderData.planType||"")+(orderData.planSize||""),
			content_category: orderData.planType||"",
			content_ids: [(orderData.planType||""),(orderData.planSize||""),(orderData.contractTerm||""),(orderData.nbnPlanSpeed||"")],
			content_type: 'product',
			currency: 'AUD',
			value: 1
		});
	}
}

