function loadBelongLiveAgentMbox() {
	var mboxName="belong-live-agent-mbox";
	var mboxDiv=document.createElement('div');
		mboxDiv.setAttribute('id', 'mboxDefault');
		mboxDiv.setAttribute('data-mbox', mboxName);
	if(typeof adobe === "object"){
		document.body.appendChild(mboxDiv);
		adobe.target.getOffer({
			mbox: mboxName,
			at_property: "fe64daa5-48cc-df93-e158-14c2bc1799bd",
			prarams: {
				at_property: "fe64daa5-48cc-df93-e158-14c2bc1799bd",
				property: {token:"fe64daa5-48cc-df93-e158-14c2bc1799bd"}
			},
			success: function(offer) {
				if(_satellite.settings.notifications){
					_satellite.notify('adobe.target.getOffer('+mboxName+').success()', offer);
				}
				adobe.target.applyOffer( {
					mbox: mboxName,
					offer: offer,
					selector: mboxDiv,
				})
			},
			'error': function(status, error) {
				if(_satellite.settings.notifications){
					_satellite.notify('adobe.target.getOffer('+mboxName+').error()', status, error);
				}
			}
		});

	}else{
		_satellite.notify('AT: adobe{} not ready. Live Agent Mbox not loaded.',4); 
	}
	
}

if (/interactive|complete/i.test(document.readyState)) {
	loadBelongLiveAgentMbox();
}
else {
	document.addEventListener('DOMContentLoaded', function(){
		loadBelongLiveAgentMbox();
	});
}