/* BT-24403 Ref #1 Product Impression */
// Initialise data layer.
var fbb = _satellite.getVar("fbbProductCatalogue"),
	digitalData = window.digitalData || {};

digitalData.products = fbb.getAllProducts();
digitalData.event = digitalData.event || [];
console.log('productData', ((window.BELONG||{}).dataLayer||{}).productData);
digitalData.event.push({
	eventName: "analytics-event",
	userInteraction: "impression",
	journey: "entice",
	checkoutStep: "pdp",
	product: fbb.getProductData(),
});
if(_satellite.settings.notifications){
	console.log('BT-24403 Ref #1 Product Impression','digitalData.event[n]:',JSON.stringify(digitalData.event[digitalData.event.length-1], null, "\t"))
}
