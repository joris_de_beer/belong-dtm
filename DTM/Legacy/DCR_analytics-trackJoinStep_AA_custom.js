// events
try {
	var eventLabelVal = _satellite.getVar('eventLabel');
	if (/^1\-/i.test(eventLabelVal)) s.events=s.apl(s.events,"event30",",",1);
	else if (/^2\-/i.test(eventLabelVal)) s.events=s.apl(s.events,"event31",",",1);
	else if (/^3\-pay/i.test(eventLabelVal)) s.events=s.apl(s.events,"event32",",",1);
	else if (/^4\-check|^3\-check/i.test(eventLabelVal)) s.events=s.apl(s.events,"event33",",",1);
}
catch(err) {
	//
}
// products - can't be done through friendly UI
/*
try {
	var joinProductsVal = BELONG.dataLayer.event.plan;
	if (joinProductsVal) s.products = ";"+joinProductsVal;
}
catch(err) {
	//
}
*/
/* additional data processing */
// Total Min Cost
try {
	if (typeof BELONG != 'undefined' && typeof BELONG.dataLayer != 'undefined') {
		if (typeof BELONG.dataLayer.totalMinCost == 'undefined' || BELONG.dataLayer.totalMinCost === 0) {
			// Try get the value from jQuery selector
			var totalMinCostVal = parseInt($('span#total-min-cost').text());
			BELONG.dataLayer.totalMinCost = totalMinCostVal;
		}
	}
}
catch(err) {
	//
}

/* BT-24403 Ref #10 Summary of Your Plan - Tell Us about Yourself */
var fbb = _satellite.getVar("fbbProductCatalogue"),
	digitalData = window.digitalData || {},
	_event = ((window.BELONG || {}).dataLayer || {}).event || {},
	eventLabel = _satellite.getVar('eventLabel');

// We only want to track when the user clicks Continue on Tell Us About Yourself section.
if (eventLabel === "2-personal cont.") {
	digitalData.event = digitalData.event || [];
	digitalData.event.push({
		eventName: "analytics-event",
		userInteraction: "click",
		textContent: "Continue",
		journey: "enter",
		checkoutStep: "personalDetails",
		product: fbb.restoreProductData()
	});
	if (_satellite.settings.notifications) {
		console.log(
			"BT-24403 Ref #10 Summary of Your Plan - Tell Us about Yourself",
			"digitalData.event[n]:",
			JSON.stringify(digitalData.event[digitalData.event.length - 1], null, "\t")
		);
	}
}
