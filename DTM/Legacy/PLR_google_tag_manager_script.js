/* PLR_google_tag_manager_script COND

If DTM is in staging mode,
load Maxis GTM  (GTM-KDQRBMT) else,
load Belong GTM (GTM-P7RLS5N)
*/

var gtmProperty = _satellite.settings.isStaging ? "GTM-KDQRBMT":"GTM-P7RLS5N";
gtmProperty = "GTM-P7RLS5N";
// GTM loads async, so setup a temp holder for any ga(() events that fire early.
window['GoogleAnalyticsObject'] = 'ga';
window['ga'] = window['ga'] || function() {
	(window['ga'].q = window['ga'].q || []).push(arguments)
};

// Tag Manager is owned/operated by Belong
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
_satellite.notify('[BELONG LEGACY] Google Tag Manager Script loaded for '+i,1);
})(window,document,'script','dataLayer',gtmProperty);
return true;

/* GA - Google Tag Manager Script for OMD */
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var
f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K9CVSMX');

/* GA - Google Tag Manager Script for OMD */
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var
f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','DC-8705912');