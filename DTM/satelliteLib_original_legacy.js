/**
 * @license
 * Adobe Visitor API for JavaScript version: 3.3.0
 * Copyright 2018 Adobe, Inc. All Rights Reserved
 * More info available at https://marketing.adobe.com/resources/help/en_US/mcvid/
 */
var e = function() {
    "use strict";
    function e() {
        return {
            callbacks: {},
            add: function(e, t) {
                this.callbacks[e] = this.callbacks[e] || [];
                var a = this.callbacks[e].push(t) - 1;
                return function() {
                    this.callbacks[e].splice(a, 1)
                }
            },
            execute: function(e, t) {
                if (this.callbacks[e]) {
                    t = (t = void 0 === t ? [] : t)instanceof Array ? t : [t];
                    try {
                        for (; this.callbacks[e].length; ) {
                            var a = this.callbacks[e].shift();
                            "function" == typeof a ? a.apply(null, t) : a instanceof Array && a[1].apply(a[0], t)
                        }
                        delete this.callbacks[e]
                    } catch (e) {}
                }
            },
            executeAll: function(e, t) {
                (t || e && !v.isObjectEmpty(e)) && Object.keys(this.callbacks).forEach(function(t) {
                    var a = void 0 !== e[t] ? e[t] : "";
                    this.execute(t, a)
                }, this)
            },
            hasCallbacks: function() {
                return Boolean(Object.keys(this.callbacks).length)
            }
        }
    }
    function t(e) {
        for (var t = /^\d+$/, a = 0, n = e.length; a < n; a++)
            if (!t.test(e[a]))
                return !1;
        return !0
    }
    function a(e, t) {
        for (; e.length < t.length; )
            e.push("0");
        for (; t.length < e.length; )
            t.push("0")
    }
    function n(e, t) {
        for (var a = 0; a < e.length; a++) {
            var n = parseInt(e[a], 10)
              , i = parseInt(t[a], 10);
            if (n > i)
                return 1;
            if (i > n)
                return -1
        }
        return 0
    }
    function i(e, i) {
        if (e === i)
            return 0;
        var r = e.toString().split(".")
          , o = i.toString().split(".");
        return t(r.concat(o)) ? (a(r, o),
        n(r, o)) : NaN
    }
    var r = "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : {};
    Object.assign = Object.assign || function(e) {
        for (var t, a, n = 1; n < arguments.length; ++n)
            for (t in a = arguments[n])
                Object.prototype.hasOwnProperty.call(a, t) && (e[t] = a[t]);
        return e
    }
    ;
    var o, s, c = {
        MESSAGES: {
            HANDSHAKE: "HANDSHAKE",
            GETSTATE: "GETSTATE",
            PARENTSTATE: "PARENTSTATE"
        },
        STATE_KEYS_MAP: {
            MCMID: "MCMID",
            MCAID: "MCAID",
            MCAAMB: "MCAAMB",
            MCAAMLH: "MCAAMLH",
            MCOPTOUT: "MCOPTOUT",
            CUSTOMERIDS: "CUSTOMERIDS"
        },
        ASYNC_API_MAP: {
            MCMID: "getMarketingCloudVisitorID",
            MCAID: "getAnalyticsVisitorID",
            MCAAMB: "getAudienceManagerBlob",
            MCAAMLH: "getAudienceManagerLocationHint",
            MCOPTOUT: "getOptOut"
        },
        SYNC_API_MAP: {
            CUSTOMERIDS: "getCustomerIDs"
        },
        ALL_APIS: {
            MCMID: "getMarketingCloudVisitorID",
            MCAAMB: "getAudienceManagerBlob",
            MCAAMLH: "getAudienceManagerLocationHint",
            MCOPTOUT: "getOptOut",
            MCAID: "getAnalyticsVisitorID",
            CUSTOMERIDS: "getCustomerIDs"
        },
        FIELDGROUP_TO_FIELD: {
            MC: "MCMID",
            A: "MCAID",
            AAM: "MCAAMB"
        },
        FIELDS: {
            MCMID: "MCMID",
            MCOPTOUT: "MCOPTOUT",
            MCAID: "MCAID",
            MCAAMLH: "MCAAMLH",
            MCAAMB: "MCAAMB"
        },
        AUTH_STATE: {
            UNKNOWN: 0,
            AUTHENTICATED: 1,
            LOGGED_OUT: 2
        },
        OPT_OUT: {
            GLOBAL: "global"
        }
    }, l = c.STATE_KEYS_MAP, u = function(e) {
        function t() {}
        function a(t, a) {
            var n = this;
            return function() {
                var t = e(0, l.MCMID)
                  , i = {};
                return i[l.MCMID] = t,
                n.setStateAndPublish(i),
                a(t),
                t
            }
        }
        this.getMarketingCloudVisitorID = function(e) {
            e = e || t;
            var n = this.findField(l.MCMID, e)
              , i = a.call(this, l.MCMID, e);
            return void 0 !== n ? n : i()
        }
    }, d = c.MESSAGES, g = c.ASYNC_API_MAP, p = c.SYNC_API_MAP, f = function() {
        function e() {}
        function t(e, t) {
            var a = this;
            return function() {
                return a.callbackRegistry.add(e, t),
                a.messageParent(d.GETSTATE),
                ""
            }
        }
        function a(a) {
            this[g[a]] = function(n) {
                n = n || e;
                var i = this.findField(a, n)
                  , r = t.call(this, a, n);
                return void 0 !== i ? i : r()
            }
        }
        function n(t) {
            this[p[t]] = function() {
                return this.findField(t, e) || {}
            }
        }
        Object.keys(g).forEach(a, this),
        Object.keys(p).forEach(n, this)
    }, m = c.ASYNC_API_MAP, h = function() {
        Object.keys(m).forEach(function(e) {
            this[m[e]] = function(t) {
                this.callbackRegistry.add(e, t)
            }
        }, this)
    }, v = ((o = (s = {
        exports: {}
    }).exports).isObjectEmpty = function(e) {
        return e === Object(e) && 0 === Object.keys(e).length
    }
    ,
    o.isValueEmpty = function(e) {
        return "" === e || o.isObjectEmpty(e)
    }
    ,
    o.getIeVersion = function() {
        if (document.documentMode)
            return document.documentMode;
        for (var e = 7; e > 4; e--) {
            var t = document.createElement("div");
            if (t.innerHTML = "<!--[if IE " + e + "]><span></span><![endif]-->",
            t.getElementsByTagName("span").length)
                return t = null,
                e;
            t = null
        }
        return null
    }
    ,
    o.encodeAndBuildRequest = function(e, t) {
        return e.map(encodeURIComponent).join(t)
    }
    ,
    void (o.isObject = function(e) {
        return null !== e && "object" == typeof e && !1 === Array.isArray(e)
    }
    ),
    s.exports), y = (v.isObjectEmpty,
    v.isValueEmpty,
    v.getIeVersion,
    v.encodeAndBuildRequest,
    v.isObject,
    e), b = c.MESSAGES, L = {
        0: "prefix",
        1: "orgID",
        2: "state"
    }, S = function(e, t) {
        this.parse = function(e) {
            try {
                var t = {};
                return e.data.split("|").forEach(function(e, a) {
                    void 0 !== e && (t[L[a]] = 2 !== a ? e : JSON.parse(e))
                }),
                t
            } catch (e) {}
        }
        ,
        this.isInvalid = function(a) {
            var n = this.parse(a);
            if (!n || Object.keys(n).length < 2)
                return !0;
            var i = e !== n.orgID
              , r = !t || a.origin !== t
              , o = -1 === Object.keys(b).indexOf(n.prefix);
            return i || r || o
        }
        ,
        this.send = function(a, n, i) {
            var r = n + "|" + e;
            i && i === Object(i) && (r += "|" + JSON.stringify(i));
            try {
                a.postMessage(r, t)
            } catch (e) {}
        }
    }, C = c.MESSAGES, E = function(e, t, a, n) {
        function i(e) {
            Object.assign(v, e)
        }
        function o(e) {
            Object.assign(v.state, e),
            v.callbackRegistry.executeAll(v.state)
        }
        function s(e) {
            if (!E.isInvalid(e)) {
                L = !1;
                var t = E.parse(e);
                v.setStateAndPublish(t.state)
            }
        }
        function c(e) {
            !L && b && (L = !0,
            E.send(n, e))
        }
        function l() {
            i(new u(a._generateID)),
            v.getMarketingCloudVisitorID(),
            v.callbackRegistry.executeAll(v.state, !0),
            r.removeEventListener("message", d)
        }
        function d(e) {
            if (!E.isInvalid(e)) {
                var t = E.parse(e);
                L = !1,
                r.clearTimeout(v._handshakeTimeout),
                r.removeEventListener("message", d),
                i(new f(v)),
                r.addEventListener("message", s),
                v.setStateAndPublish(t.state),
                v.callbackRegistry.hasCallbacks() && c(C.GETSTATE)
            }
        }
        function g() {
            b && postMessage ? (r.addEventListener("message", d),
            c(C.HANDSHAKE),
            v._handshakeTimeout = setTimeout(l, 250)) : l()
        }
        function p() {
            r.s_c_in || (r.s_c_il = [],
            r.s_c_in = 0),
            v._c = "Visitor",
            v._il = r.s_c_il,
            v._in = r.s_c_in,
            v._il[v._in] = v,
            r.s_c_in++
        }
        function m() {
            function e(e) {
                0 !== e.indexOf("_") && "function" == typeof a[e] && (v[e] = function() {}
                )
            }
            Object.keys(a).forEach(e),
            v.getSupplementalDataID = a.getSupplementalDataID
        }
        var v = this
          , b = t.whitelistParentDomain;
        v.state = {},
        v.version = a.version,
        v.marketingCloudOrgID = e,
        v.cookieDomain = a.cookieDomain || "",
        v._instanceType = "child";
        var L = !1
          , E = new S(e,b);
        v.callbackRegistry = y(),
        v.init = function() {
            p(),
            m(),
            i(new h(v)),
            g()
        }
        ,
        v.findField = function(e, t) {
            if (v.state[e])
                return t(v.state[e]),
                v.state[e]
        }
        ,
        v.messageParent = c,
        v.setStateAndPublish = o
    }, _ = c.MESSAGES, T = c.ALL_APIS, k = c.ASYNC_API_MAP, P = c.FIELDGROUP_TO_FIELD, D = function(e, t) {
        function a() {
            var t = {};
            return Object.keys(T).forEach(function(a) {
                var n = T[a]
                  , i = e[n]();
                v.isValueEmpty(i) || (t[a] = i)
            }),
            t
        }
        function n() {
            var t = [];
            return e._loading && Object.keys(e._loading).forEach(function(a) {
                if (e._loading[a]) {
                    var n = P[a];
                    t.push(n)
                }
            }),
            t.length ? t : null
        }
        function i(t) {
            return function a() {
                var i = n();
                if (i) {
                    var r = k[i[0]];
                    e[r](a, !0)
                } else
                    t()
            }
        }
        function r(e, n) {
            var i = a();
            t.send(e, n, i)
        }
        function o(e) {
            c(e),
            r(e, _.HANDSHAKE)
        }
        function s(e) {
            i(function() {
                r(e, _.PARENTSTATE)
            })()
        }
        function c(a) {
            function n(n) {
                i.call(e, n),
                t.send(a, _.PARENTSTATE, {
                    CUSTOMERIDS: e.getCustomerIDs()
                })
            }
            var i = e.setCustomerIDs;
            e.setCustomerIDs = n
        }
        return function(e) {
            t.isInvalid(e) || (t.parse(e).prefix === _.HANDSHAKE ? o : s)(e.source)
        }
    }, I = function(e, t) {
        function a(e) {
            return function(a) {
                n[e] = a,
                ++i === r && t(n)
            }
        }
        var n = {}
          , i = 0
          , r = Object.keys(e).length;
        Object.keys(e).forEach(function(t) {
            var n = e[t];
            if (n.fn) {
                var i = n.args || [];
                i.unshift(a(t)),
                n.fn.apply(n.context || null, i)
            }
        })
    }, O = function(e) {
        var t;
        if (!e && r.location && (e = r.location.hostname),
        t = e)
            if (/^[0-9.]+$/.test(t))
                t = "";
            else {
                var a = ",ac,ad,ae,af,ag,ai,al,am,an,ao,aq,ar,as,at,au,aw,ax,az,ba,bb,be,bf,bg,bh,bi,bj,bm,bo,br,bs,bt,bv,bw,by,bz,ca,cc,cd,cf,cg,ch,ci,cl,cm,cn,co,cr,cu,cv,cw,cx,cz,de,dj,dk,dm,do,dz,ec,ee,eg,es,et,eu,fi,fm,fo,fr,ga,gb,gd,ge,gf,gg,gh,gi,gl,gm,gn,gp,gq,gr,gs,gt,gw,gy,hk,hm,hn,hr,ht,hu,id,ie,im,in,io,iq,ir,is,it,je,jo,jp,kg,ki,km,kn,kp,kr,ky,kz,la,lb,lc,li,lk,lr,ls,lt,lu,lv,ly,ma,mc,md,me,mg,mh,mk,ml,mn,mo,mp,mq,mr,ms,mt,mu,mv,mw,mx,my,na,nc,ne,nf,ng,nl,no,nr,nu,nz,om,pa,pe,pf,ph,pk,pl,pm,pn,pr,ps,pt,pw,py,qa,re,ro,rs,ru,rw,sa,sb,sc,sd,se,sg,sh,si,sj,sk,sl,sm,sn,so,sr,st,su,sv,sx,sy,sz,tc,td,tf,tg,th,tj,tk,tl,tm,tn,to,tp,tr,tt,tv,tw,tz,ua,ug,uk,us,uy,uz,va,vc,ve,vg,vi,vn,vu,wf,ws,yt,"
                  , n = t.split(".")
                  , i = n.length - 1
                  , o = i - 1;
                if (i > 1 && n[i].length <= 2 && (2 === n[i - 1].length || a.indexOf("," + n[i] + ",") < 0) && o--,
                o > 0)
                    for (t = ""; i >= o; )
                        t = n[i] + (t ? "." : "") + t,
                        i--
            }
        return t
    }, A = {
        compare: i,
        isLessThan: function(e, t) {
            return i(e, t) < 0
        },
        areVersionsDifferent: function(e, t) {
            return 0 !== i(e, t)
        },
        isGreaterThan: function(e, t) {
            return i(e, t) > 0
        },
        isEqual: function(e, t) {
            return 0 === i(e, t)
        }
    }, N = !!r.postMessage, V = {
        postMessage: function(e, t, a) {
            var n = 1;
            t && (N ? a.postMessage(e, t.replace(/([^:]+:\/\/[^\/]+).*/, "$1")) : t && (a.location = t.replace(/#.*$/, "") + "#" + +new Date + n++ + "&" + e))
        },
        receiveMessage: function(e, t) {
            var a;
            try {
                N && (e && (a = function(a) {
                    if ("string" == typeof t && a.origin !== t || "[object Function]" === Object.prototype.toString.call(t) && !1 === t(a.origin))
                        return !1;
                    e(a)
                }
                ),
                r.addEventListener ? r[e ? "addEventListener" : "removeEventListener"]("message", a) : r[e ? "attachEvent" : "detachEvent"]("onmessage", a))
            } catch (e) {}
        }
    }, w = function(e) {
        var t, a, n = "0123456789", i = "", r = "", o = 8, s = 10, c = 10;
        if (1 == e) {
            for (n += "ABCDEF",
            t = 0; 16 > t; t++)
                a = Math.floor(Math.random() * o),
                i += n.substring(a, a + 1),
                a = Math.floor(Math.random() * o),
                r += n.substring(a, a + 1),
                o = 16;
            return i + "-" + r
        }
        for (t = 0; 19 > t; t++)
            a = Math.floor(Math.random() * s),
            i += n.substring(a, a + 1),
            0 === t && 9 == a ? s = 3 : (1 == t || 2 == t) && 10 != s && 2 > a ? s = 10 : 2 < t && (s = 10),
            a = Math.floor(Math.random() * c),
            r += n.substring(a, a + 1),
            0 === t && 9 == a ? c = 3 : (1 == t || 2 == t) && 10 != c && 2 > a ? c = 10 : 2 < t && (c = 10);
        return i + r
    }, M = function(e) {
        return {
            corsMetadata: (t = "none",
            a = !0,
            "undefined" != typeof XMLHttpRequest && XMLHttpRequest === Object(XMLHttpRequest) && ("withCredentials"in new XMLHttpRequest ? t = "XMLHttpRequest" : "undefined" != typeof XDomainRequest && XDomainRequest === Object(XDomainRequest) && (a = !1),
            Object.prototype.toString.call(r.HTMLElement).indexOf("Constructor") > 0 && (a = !1)),
            {
                corsType: t,
                corsCookiesEnabled: a
            }),
            getCORSInstance: function() {
                return "none" === this.corsMetadata.corsType ? null : new r[this.corsMetadata.corsType]
            },
            fireCORS: function(t, a) {
                function n(e) {
                    var a;
                    try {
                        if ((a = JSON.parse(e)) !== Object(a))
                            return void i.handleCORSError(t, null, "Response is not JSON")
                    } catch (e) {
                        return void i.handleCORSError(t, e, "Error parsing response as JSON")
                    }
                    try {
                        for (var n = t.callback, o = r, s = 0; s < n.length; s++)
                            o = o[n[s]];
                        o(a)
                    } catch (e) {
                        i.handleCORSError(t, e, "Error forming callback function")
                    }
                }
                var i = this;
                a && (t.loadErrorHandler = a);
                try {
                    var o = this.getCORSInstance();
                    o.open("get", t.corsUrl + "&ts=" + (new Date).getTime(), !0),
                    "XMLHttpRequest" === this.corsMetadata.corsType && (o.withCredentials = !0,
                    o.timeout = e.loadTimeout,
                    o.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"),
                    o.onreadystatechange = function() {
                        4 === this.readyState && 200 === this.status && n(this.responseText)
                    }
                    ),
                    o.onerror = function(e) {
                        i.handleCORSError(t, e, "onerror")
                    }
                    ,
                    o.ontimeout = function(e) {
                        i.handleCORSError(t, e, "ontimeout")
                    }
                    ,
                    o.send(),
                    e._log.requests.push(t.corsUrl)
                } catch (e) {
                    this.handleCORSError(t, e, "try-catch")
                }
            },
            handleCORSError: function(t, a, n) {
                e.CORSErrors.push({
                    corsData: t,
                    error: a,
                    description: n
                }),
                t.loadErrorHandler && ("ontimeout" === n ? t.loadErrorHandler(!0) : t.loadErrorHandler(!1))
            }
        };
        var t, a
    }, B = {
        POST_MESSAGE_ENABLED: !!r.postMessage,
        DAYS_BETWEEN_SYNC_ID_CALLS: 1,
        MILLIS_PER_DAY: 864e5,
        ADOBE_MC: "adobe_mc",
        ADOBE_MC_SDID: "adobe_mc_sdid",
        VALID_VISITOR_ID_REGEX: /^[0-9a-fA-F\-]+$/,
        ADOBE_MC_TTL_IN_MIN: 5,
        VERSION_REGEX: /vVersion\|((\d+\.)?(\d+\.)?(\*|\d+))(?=$|\|)/
    }, R = function(e, t) {
        var a = r.document;
        return {
            THROTTLE_START: 3e4,
            MAX_SYNCS_LENGTH: 649,
            throttleTimerSet: !1,
            id: null,
            onPagePixels: [],
            iframeHost: null,
            getIframeHost: function(e) {
                if ("string" == typeof e) {
                    var t = e.split("/");
                    return t[0] + "//" + t[2]
                }
            },
            subdomain: null,
            url: null,
            getUrl: function() {
                var t, n = "http://fast.", i = "?d_nsid=" + e.idSyncContainerID + "#" + encodeURIComponent(a.location.origin);
                return this.subdomain || (this.subdomain = "nosubdomainreturned"),
                e.loadSSL && (n = e.idSyncSSLUseAkamai ? "https://fast." : "https://"),
                t = n + this.subdomain + ".demdex.net/dest5.html" + i,
                this.iframeHost = this.getIframeHost(t),
                this.id = "destination_publishing_iframe_" + this.subdomain + "_" + e.idSyncContainerID,
                t
            },
            checkDPIframeSrc: function() {
                var t = "?d_nsid=" + e.idSyncContainerID + "#" + encodeURIComponent(a.location.href);
                "string" == typeof e.dpIframeSrc && e.dpIframeSrc.length && (this.id = "destination_publishing_iframe_" + (e._subdomain || this.subdomain || (new Date).getTime()) + "_" + e.idSyncContainerID,
                this.iframeHost = this.getIframeHost(e.dpIframeSrc),
                this.url = e.dpIframeSrc + t)
            },
            idCallNotProcesssed: null,
            doAttachIframe: !1,
            startedAttachingIframe: !1,
            iframeHasLoaded: null,
            iframeIdChanged: null,
            newIframeCreated: null,
            originalIframeHasLoadedAlready: null,
            iframeLoadedCallbacks: [],
            regionChanged: !1,
            timesRegionChanged: 0,
            sendingMessages: !1,
            messages: [],
            messagesPosted: [],
            messagesReceived: [],
            messageSendingInterval: B.POST_MESSAGE_ENABLED ? null : 100,
            jsonForComparison: [],
            jsonDuplicates: [],
            jsonWaiting: [],
            jsonProcessed: [],
            canSetThirdPartyCookies: !0,
            receivedThirdPartyCookiesNotification: !1,
            readyToAttachIframePreliminary: function() {
                return !(e.idSyncDisableSyncs || e.disableIdSyncs || e.idSyncDisable3rdPartySyncing || e.disableThirdPartyCookies || e.disableThirdPartyCalls)
            },
            readyToAttachIframe: function() {
                return this.readyToAttachIframePreliminary() && (this.doAttachIframe || e._doAttachIframe) && (this.subdomain && "nosubdomainreturned" !== this.subdomain || e._subdomain) && this.url && !this.startedAttachingIframe
            },
            attachIframe: function() {
                function e() {
                    (i = a.createElement("iframe")).sandbox = "allow-scripts allow-same-origin",
                    i.title = "Adobe ID Syncing iFrame",
                    i.id = n.id,
                    i.name = n.id + "_name",
                    i.style.cssText = "display: none; width: 0; height: 0;",
                    i.src = n.url,
                    n.newIframeCreated = !0,
                    t(),
                    a.body.appendChild(i)
                }
                function t(e) {
                    i.addEventListener("load", function() {
                        i.className = "aamIframeLoaded",
                        n.iframeHasLoaded = !0,
                        n.fireIframeLoadedCallbacks(e),
                        n.requestToProcess()
                    })
                }
                this.startedAttachingIframe = !0;
                var n = this
                  , i = a.getElementById(this.id);
                i ? "IFRAME" !== i.nodeName ? (this.id += "_2",
                this.iframeIdChanged = !0,
                e()) : (this.newIframeCreated = !1,
                "aamIframeLoaded" !== i.className ? (this.originalIframeHasLoadedAlready = !1,
                t("The destination publishing iframe already exists from a different library, but hadn't loaded yet.")) : (this.originalIframeHasLoadedAlready = !0,
                this.iframeHasLoaded = !0,
                this.iframe = i,
                this.fireIframeLoadedCallbacks("The destination publishing iframe already exists from a different library, and had loaded alresady."),
                this.requestToProcess())) : e(),
                this.iframe = i
            },
            fireIframeLoadedCallbacks: function(e) {
                this.iframeLoadedCallbacks.forEach(function(t) {
                    "function" == typeof t && t({
                        message: e || "The destination publishing iframe was attached and loaded successfully."
                    })
                }),
                this.iframeLoadedCallbacks = []
            },
            requestToProcess: function(t) {
                function a() {
                    i.jsonForComparison.push(t),
                    i.jsonWaiting.push(t),
                    i.processSyncOnPage(t)
                }
                var n, i = this;
                if (t === Object(t) && t.ibs)
                    if (n = JSON.stringify(t.ibs || []),
                    this.jsonForComparison.length) {
                        var r, o, s, c = !1;
                        for (r = 0,
                        o = this.jsonForComparison.length; r < o; r++)
                            if (s = this.jsonForComparison[r],
                            n === JSON.stringify(s.ibs || [])) {
                                c = !0;
                                break
                            }
                        c ? this.jsonDuplicates.push(t) : a()
                    } else
                        a();
                if ((this.receivedThirdPartyCookiesNotification || !B.POST_MESSAGE_ENABLED || this.iframeHasLoaded) && this.jsonWaiting.length) {
                    var l = this.jsonWaiting.shift();
                    this.process(l),
                    this.requestToProcess()
                }
                e.idSyncDisableSyncs || e.disableIdSyncs || !this.iframeHasLoaded || !this.messages.length || this.sendingMessages || (this.throttleTimerSet || (this.throttleTimerSet = !0,
                setTimeout(function() {
                    i.messageSendingInterval = B.POST_MESSAGE_ENABLED ? null : 150
                }, this.THROTTLE_START)),
                this.sendingMessages = !0,
                this.sendMessages())
            },
            getRegionAndCheckIfChanged: function(t, a) {
                var n = e._getField("MCAAMLH")
                  , i = t.d_region || t.dcs_region;
                return n ? i && (e._setFieldExpire("MCAAMLH", a),
                e._setField("MCAAMLH", i),
                parseInt(n, 10) !== i && (this.regionChanged = !0,
                this.timesRegionChanged++,
                e._setField("MCSYNCSOP", ""),
                e._setField("MCSYNCS", ""),
                n = i)) : (n = i) && (e._setFieldExpire("MCAAMLH", a),
                e._setField("MCAAMLH", n)),
                n || (n = ""),
                n
            },
            processSyncOnPage: function(e) {
                var t, a, n, i;
                if ((t = e.ibs) && t instanceof Array && (a = t.length))
                    for (n = 0; n < a; n++)
                        (i = t[n]).syncOnPage && this.checkFirstPartyCookie(i, "", "syncOnPage")
            },
            process: function(e) {
                var t, a, n, i, r, o = encodeURIComponent, s = !1;
                if ((t = e.ibs) && t instanceof Array && (a = t.length))
                    for (s = !0,
                    n = 0; n < a; n++)
                        i = t[n],
                        r = [o("ibs"), o(i.id || ""), o(i.tag || ""), v.encodeAndBuildRequest(i.url || [], ","), o(i.ttl || ""), "", "", i.fireURLSync ? "true" : "false"],
                        i.syncOnPage || (this.canSetThirdPartyCookies ? this.addMessage(r.join("|")) : i.fireURLSync && this.checkFirstPartyCookie(i, r.join("|")));
                s && this.jsonProcessed.push(e)
            },
            checkFirstPartyCookie: function(t, a, n) {
                var i = "syncOnPage" === n
                  , r = i ? "MCSYNCSOP" : "MCSYNCS";
                e._readVisitor();
                var o, s, c = e._getField(r), l = !1, u = !1, d = Math.ceil((new Date).getTime() / B.MILLIS_PER_DAY);
                c ? (o = c.split("*"),
                l = (s = this.pruneSyncData(o, t.id, d)).dataPresent,
                u = s.dataValid,
                l && u || this.fireSync(i, t, a, o, r, d)) : (o = [],
                this.fireSync(i, t, a, o, r, d))
            },
            pruneSyncData: function(e, t, a) {
                var n, i, r, o = !1, s = !1;
                for (i = 0; i < e.length; i++)
                    n = e[i],
                    r = parseInt(n.split("-")[1], 10),
                    n.match("^" + t + "-") ? (o = !0,
                    a < r ? s = !0 : (e.splice(i, 1),
                    i--)) : a >= r && (e.splice(i, 1),
                    i--);
                return {
                    dataPresent: o,
                    dataValid: s
                }
            },
            manageSyncsSize: function(e) {
                if (e.join("*").length > this.MAX_SYNCS_LENGTH)
                    for (e.sort(function(e, t) {
                        return parseInt(e.split("-")[1], 10) - parseInt(t.split("-")[1], 10)
                    }); e.join("*").length > this.MAX_SYNCS_LENGTH; )
                        e.shift()
            },
            fireSync: function(t, a, n, i, r, o) {
                var s = this;
                if (t) {
                    if ("img" === a.tag) {
                        var c, l, u, d, g = a.url, p = e.loadSSL ? "https:" : "http:";
                        for (c = 0,
                        l = g.length; c < l; c++) {
                            u = g[c],
                            d = /^\/\//.test(u);
                            var f = new Image;
                            f.addEventListener("load", function(t, a, n, i) {
                                return function() {
                                    s.onPagePixels[t] = null,
                                    e._readVisitor();
                                    var o, c, l, u, d = e._getField(r), g = [];
                                    if (d)
                                        for (c = 0,
                                        l = (o = d.split("*")).length; c < l; c++)
                                            (u = o[c]).match("^" + a.id + "-") || g.push(u);
                                    s.setSyncTrackingData(g, a, n, i)
                                }
                            }(this.onPagePixels.length, a, r, o)),
                            f.src = (d ? p : "") + u,
                            this.onPagePixels.push(f)
                        }
                    }
                } else
                    this.addMessage(n),
                    this.setSyncTrackingData(i, a, r, o)
            },
            addMessage: function(t) {
                var a = encodeURIComponent(e._enableErrorReporting ? "---destpub-debug---" : "---destpub---");
                this.messages.push((B.POST_MESSAGE_ENABLED ? "" : a) + t)
            },
            setSyncTrackingData: function(t, a, n, i) {
                t.push(a.id + "-" + (i + Math.ceil(a.ttl / 60 / 24))),
                this.manageSyncsSize(t),
                e._setField(n, t.join("*"))
            },
            sendMessages: function() {
                var e, t = this, a = "", n = encodeURIComponent;
                this.regionChanged && (a = n("---destpub-clear-dextp---"),
                this.regionChanged = !1),
                this.messages.length ? B.POST_MESSAGE_ENABLED ? (e = a + n("---destpub-combined---") + this.messages.join("%01"),
                this.postMessage(e),
                this.messages = [],
                this.sendingMessages = !1) : (e = this.messages.shift(),
                this.postMessage(a + e),
                setTimeout(function() {
                    t.sendMessages()
                }, this.messageSendingInterval)) : this.sendingMessages = !1
            },
            postMessage: function(e) {
                V.postMessage(e, this.url, this.iframe.contentWindow),
                this.messagesPosted.push(e)
            },
            receiveMessage: function(e) {
                var t, a = /^---destpub-to-parent---/;
                "string" == typeof e && a.test(e) && ("canSetThirdPartyCookies" === (t = e.replace(a, "").split("|"))[0] && (this.canSetThirdPartyCookies = "true" === t[1],
                this.receivedThirdPartyCookiesNotification = !0,
                this.requestToProcess()),
                this.messagesReceived.push(e))
            },
            processIDCallData: function(n) {
                (null == this.url || n.subdomain && "nosubdomainreturned" === this.subdomain) && ("string" == typeof e._subdomain && e._subdomain.length ? this.subdomain = e._subdomain : this.subdomain = n.subdomain || "",
                this.url = this.getUrl()),
                n.ibs instanceof Array && n.ibs.length && (this.doAttachIframe = !0),
                this.readyToAttachIframe() && (e.idSyncAttachIframeOnWindowLoad ? (t.windowLoaded || "complete" === a.readyState || "loaded" === a.readyState) && this.attachIframe() : this.attachIframeASAP()),
                "function" == typeof e.idSyncIDCallResult ? e.idSyncIDCallResult(n) : this.requestToProcess(n),
                "function" == typeof e.idSyncAfterIDCallResult && e.idSyncAfterIDCallResult(n)
            },
            canMakeSyncIDCall: function(t, a) {
                return e._forceSyncIDCall || !t || a - t > B.DAYS_BETWEEN_SYNC_ID_CALLS
            },
            attachIframeASAP: function() {
                function e() {
                    t.startedAttachingIframe || (a.body ? t.attachIframe() : setTimeout(e, 30))
                }
                var t = this;
                e()
            }
        }
    }, x = {
        audienceManagerServer: {},
        audienceManagerServerSecure: {},
        cookieDomain: {},
        cookieLifetime: {},
        cookieName: {},
        disableThirdPartyCalls: {},
        idSyncAfterIDCallResult: {},
        idSyncAttachIframeOnWindowLoad: {},
        idSyncContainerID: {},
        idSyncDisable3rdPartySyncing: {},
        disableThirdPartyCookies: {},
        idSyncDisableSyncs: {},
        disableIdSyncs: {},
        idSyncIDCallResult: {},
        idSyncSSLUseAkamai: {},
        isCoopSafe: {},
        loadSSL: {},
        loadTimeout: {},
        marketingCloudServer: {},
        marketingCloudServerSecure: {},
        overwriteCrossDomainMCIDAndAID: {},
        resetBeforeVersion: {},
        sdidParamExpiry: {},
        serverState: {},
        sessionCookieName: {},
        secureCookie: {},
        takeTimeoutMetrics: {},
        trackingServer: {},
        trackingServerSecure: {},
        whitelistIframeDomains: {},
        whitelistParentDomain: {}
    }, G = {
        getConfigNames: function() {
            return Object.keys(x)
        },
        getConfigs: function() {
            return x
        }
    }, U = function(e, t, a) {
        function n(e) {
            var t = e;
            return function(e) {
                var a = e || d.location.href;
                try {
                    var n = u._extractParamFromUri(a, t);
                    if (n)
                        return k.parsePipeDelimetedKeyValues(n)
                } catch (e) {}
            }
        }
        function i(e) {
            function t(e, t) {
                e && e.match(B.VALID_VISITOR_ID_REGEX) && t(e)
            }
            t(e[m], u.setMarketingCloudVisitorID),
            u._setFieldExpire(C, -1),
            t(e[b], u.setAnalyticsVisitorID)
        }
        function o(e) {
            e = e || {},
            u._supplementalDataIDCurrent = e.supplementalDataIDCurrent || "",
            u._supplementalDataIDCurrentConsumed = e.supplementalDataIDCurrentConsumed || {},
            u._supplementalDataIDLast = e.supplementalDataIDLast || "",
            u._supplementalDataIDLastConsumed = e.supplementalDataIDLastConsumed || {}
        }
        function s(e) {
            function t(e, t, a) {
                return (a = a ? a += "|" : a) + (e + "=") + encodeURIComponent(t)
            }
            function a(e, a) {
                var n = a[0]
                  , i = a[1];
                return null != i && i !== E && (e = t(n, i, e)),
                e
            }
            var n, i = e.reduce(a, "");
            return (n = (n = i) ? n += "|" : n) + "TS=" + k.getTimestampInSeconds()
        }
        function l(e) {
            var t = e.minutesToLive
              , a = "";
            return (u.idSyncDisableSyncs || u.disableIdSyncs) && (a = a || "Error: id syncs have been disabled"),
            "string" == typeof e.dpid && e.dpid.length || (a = a || "Error: config.dpid is empty"),
            "string" == typeof e.url && e.url.length || (a = a || "Error: config.url is empty"),
            void 0 === t ? t = 20160 : (t = parseInt(t, 10),
            (isNaN(t) || t <= 0) && (a = a || "Error: config.minutesToLive needs to be a positive number")),
            {
                error: a,
                ttl: t
            }
        }
        if (!a || a.split("").reverse().join("") !== e)
            throw new Error("Please use `Visitor.getInstance` to instantiate Visitor.");
        var u = this;
        u.version = "3.3.0";
        var d = r
          , g = d.Visitor;
        g.version = u.version,
        g.AuthState = c.AUTH_STATE,
        g.OptOut = c.OPT_OUT,
        d.s_c_in || (d.s_c_il = [],
        d.s_c_in = 0),
        u._c = "Visitor",
        u._il = d.s_c_il,
        u._in = d.s_c_in,
        u._il[u._in] = u,
        d.s_c_in++,
        u._instanceType = "regular",
        u._log = {
            requests: []
        },
        u.marketingCloudOrgID = e,
        u.cookieName = "AMCV_" + e,
        u.sessionCookieName = "AMCVS_" + e,
        u.cookieDomain = O(),
        u.cookieDomain === d.location.hostname && (u.cookieDomain = ""),
        u.loadSSL = d.location.protocol.toLowerCase().indexOf("https") >= 0,
        u.loadTimeout = 3e4,
        u.CORSErrors = [],
        u.marketingCloudServer = u.audienceManagerServer = "dpm.demdex.net",
        u.sdidParamExpiry = 30;
        var p = d.document
          , f = null
          , m = "MCMID"
          , h = "MCIDTS"
          , y = "A"
          , b = "MCAID"
          , L = "AAM"
          , C = "MCAAMB"
          , E = "NONE"
          , _ = function(e) {
            return !Object.prototype[e]
        }
          , T = M(u);
        u.FIELDS = c.FIELDS,
        u.cookieRead = function(e) {
            e = encodeURIComponent(e);
            var t = (";" + p.cookie).split(" ").join(";")
              , a = t.indexOf(";" + e + "=")
              , n = a < 0 ? a : t.indexOf(";", a + 1);
            return a < 0 ? "" : decodeURIComponent(t.substring(a + 2 + e.length, n < 0 ? t.length : n))
        }
        ,
        u.cookieWrite = function(e, t, a) {
            var n, i = u.cookieLifetime, r = "";
            if (t = "" + t,
            i = i ? ("" + i).toUpperCase() : "",
            a && "SESSION" !== i && "NONE" !== i) {
                if (n = "" !== t ? parseInt(i || 0, 10) : -60)
                    (a = new Date).setTime(a.getTime() + 1e3 * n);
                else if (1 === a) {
                    var o = (a = new Date).getYear();
                    a.setYear(o + 2 + (o < 1900 ? 1900 : 0))
                }
            } else
                a = 0;
            return e && "NONE" !== i ? (u.configs && u.configs.secureCookie && "https:" === location.protocol && (r = "Secure"),
            p.cookie = encodeURIComponent(e) + "=" + encodeURIComponent(t) + "; path=/;" + (a ? " expires=" + a.toGMTString() + ";" : "") + (u.cookieDomain ? " domain=" + u.cookieDomain + ";" : "") + r,
            u.cookieRead(e) === t) : 0
        }
        ,
        u.resetState = function(e) {
            e ? u._mergeServerState(e) : o()
        }
        ,
        u._isAllowedDone = !1,
        u._isAllowedFlag = !1,
        u.isAllowed = function() {
            return u._isAllowedDone || (u._isAllowedDone = !0,
            (u.cookieRead(u.cookieName) || u.cookieWrite(u.cookieName, "T", 1)) && (u._isAllowedFlag = !0)),
            u._isAllowedFlag
        }
        ,
        u.setMarketingCloudVisitorID = function(e) {
            u._setMarketingCloudFields(e)
        }
        ,
        u._use1stPartyMarketingCloudServer = !1,
        u.getMarketingCloudVisitorID = function(e, t) {
            if (u.isAllowed()) {
                u.marketingCloudServer && u.marketingCloudServer.indexOf(".demdex.net") < 0 && (u._use1stPartyMarketingCloudServer = !0);
                var a = u._getAudienceManagerURLData("_setMarketingCloudFields")
                  , n = a.url;
                return u._getRemoteField(m, n, e, t, a)
            }
            return ""
        }
        ,
        u.getVisitorValues = function(e, t) {
            var a = {
                MCMID: {
                    fn: u.getMarketingCloudVisitorID,
                    args: [!0],
                    context: u
                },
                MCOPTOUT: {
                    fn: u.isOptedOut,
                    args: [void 0, !0],
                    context: u
                },
                MCAID: {
                    fn: u.getAnalyticsVisitorID,
                    args: [!0],
                    context: u
                },
                MCAAMLH: {
                    fn: u.getAudienceManagerLocationHint,
                    args: [!0],
                    context: u
                },
                MCAAMB: {
                    fn: u.getAudienceManagerBlob,
                    args: [!0],
                    context: u
                }
            }
              , n = t && t.length ? k.pluck(a, t) : a;
            I(n, e)
        }
        ,
        u._currentCustomerIDs = {},
        u._customerIDsHashChanged = !1,
        u._newCustomerIDsHash = "",
        u.setCustomerIDs = function(e) {
            function t() {
                u._customerIDsHashChanged = !1
            }
            if (u.isAllowed() && e) {
                if (!v.isObject(e) || v.isObjectEmpty(e))
                    return !1;
                var a, n;
                for (a in u._readVisitor(),
                e)
                    if (_(a) && (n = e[a]))
                        if ("object" == typeof n) {
                            var i = {};
                            n.id && (i.id = n.id),
                            null != n.authState && (i.authState = n.authState),
                            u._currentCustomerIDs[a] = i
                        } else
                            u._currentCustomerIDs[a] = {
                                id: n
                            };
                var r = u.getCustomerIDs()
                  , o = u._getField("MCCIDH")
                  , s = "";
                for (a in o || (o = 0),
                r)
                    _(a) && (s += (s ? "|" : "") + a + "|" + ((n = r[a]).id ? n.id : "") + (n.authState ? n.authState : ""));
                u._newCustomerIDsHash = String(u._hash(s)),
                u._newCustomerIDsHash !== o && (u._customerIDsHashChanged = !0,
                u._mapCustomerIDs(t))
            }
        }
        ,
        u.getCustomerIDs = function() {
            u._readVisitor();
            var e, t, a = {};
            for (e in u._currentCustomerIDs)
                _(e) && (t = u._currentCustomerIDs[e],
                a[e] || (a[e] = {}),
                t.id && (a[e].id = t.id),
                null != t.authState ? a[e].authState = t.authState : a[e].authState = g.AuthState.UNKNOWN);
            return a
        }
        ,
        u.setAnalyticsVisitorID = function(e) {
            u._setAnalyticsFields(e)
        }
        ,
        u.getAnalyticsVisitorID = function(e, t, a) {
            if (!k.isTrackingServerPopulated() && !a)
                return u._callCallback(e, [""]),
                "";
            if (u.isAllowed()) {
                var n = "";
                if (a || (n = u.getMarketingCloudVisitorID(function() {
                    u.getAnalyticsVisitorID(e, !0)
                })),
                n || a) {
                    var i = a ? u.marketingCloudServer : u.trackingServer
                      , r = "";
                    u.loadSSL && (a ? u.marketingCloudServerSecure && (i = u.marketingCloudServerSecure) : u.trackingServerSecure && (i = u.trackingServerSecure));
                    var o = {};
                    if (i) {
                        var s = "http" + (u.loadSSL ? "s" : "") + "://" + i + "/id"
                          , c = "d_visid_ver=" + u.version + "&mcorgid=" + encodeURIComponent(u.marketingCloudOrgID) + (n ? "&mid=" + encodeURIComponent(n) : "") + (u.idSyncDisable3rdPartySyncing || u.disableThirdPartyCookies ? "&d_coppa=true" : "")
                          , l = ["s_c_il", u._in, "_set" + (a ? "MarketingCloud" : "Analytics") + "Fields"];
                        r = s + "?" + c + "&callback=s_c_il%5B" + u._in + "%5D._set" + (a ? "MarketingCloud" : "Analytics") + "Fields",
                        o.corsUrl = s + "?" + c,
                        o.callback = l
                    }
                    return o.url = r,
                    u._getRemoteField(a ? m : b, r, e, t, o)
                }
            }
            return ""
        }
        ,
        u.getAudienceManagerLocationHint = function(e, t) {
            if (u.isAllowed() && u.getMarketingCloudVisitorID(function() {
                u.getAudienceManagerLocationHint(e, !0)
            })) {
                var a = u._getField(b);
                if (!a && k.isTrackingServerPopulated() && (a = u.getAnalyticsVisitorID(function() {
                    u.getAudienceManagerLocationHint(e, !0)
                })),
                a || !k.isTrackingServerPopulated()) {
                    var n = u._getAudienceManagerURLData()
                      , i = n.url;
                    return u._getRemoteField("MCAAMLH", i, e, t, n)
                }
            }
            return ""
        }
        ,
        u.getLocationHint = u.getAudienceManagerLocationHint,
        u.getAudienceManagerBlob = function(e, t) {
            if (u.isAllowed() && u.getMarketingCloudVisitorID(function() {
                u.getAudienceManagerBlob(e, !0)
            })) {
                var a = u._getField(b);
                if (!a && k.isTrackingServerPopulated() && (a = u.getAnalyticsVisitorID(function() {
                    u.getAudienceManagerBlob(e, !0)
                })),
                a || !k.isTrackingServerPopulated()) {
                    var n = u._getAudienceManagerURLData()
                      , i = n.url;
                    return u._customerIDsHashChanged && u._setFieldExpire(C, -1),
                    u._getRemoteField(C, i, e, t, n)
                }
            }
            return ""
        }
        ,
        u._supplementalDataIDCurrent = "",
        u._supplementalDataIDCurrentConsumed = {},
        u._supplementalDataIDLast = "",
        u._supplementalDataIDLastConsumed = {},
        u.getSupplementalDataID = function(e, t) {
            u._supplementalDataIDCurrent || t || (u._supplementalDataIDCurrent = u._generateID(1));
            var a = u._supplementalDataIDCurrent;
            return u._supplementalDataIDLast && !u._supplementalDataIDLastConsumed[e] ? (a = u._supplementalDataIDLast,
            u._supplementalDataIDLastConsumed[e] = !0) : a && (u._supplementalDataIDCurrentConsumed[e] && (u._supplementalDataIDLast = u._supplementalDataIDCurrent,
            u._supplementalDataIDLastConsumed = u._supplementalDataIDCurrentConsumed,
            u._supplementalDataIDCurrent = a = t ? "" : u._generateID(1),
            u._supplementalDataIDCurrentConsumed = {}),
            a && (u._supplementalDataIDCurrentConsumed[e] = !0)),
            a
        }
        ,
        u.getOptOut = function(e, t) {
            if (u.isAllowed()) {
                var a = u._getAudienceManagerURLData("_setMarketingCloudFields")
                  , n = a.url;
                return u._getRemoteField("MCOPTOUT", n, e, t, a)
            }
            return ""
        }
        ,
        u.isOptedOut = function(e, t, a) {
            if (u.isAllowed()) {
                t || (t = g.OptOut.GLOBAL);
                var n = u.getOptOut(function(a) {
                    var n = a === g.OptOut.GLOBAL || a.indexOf(t) >= 0;
                    u._callCallback(e, [n])
                }, a);
                return n ? n === g.OptOut.GLOBAL || n.indexOf(t) >= 0 : null
            }
            return !1
        }
        ,
        u._fields = null,
        u._fieldsExpired = null,
        u._hash = function(e) {
            var t, a = 0;
            if (e)
                for (t = 0; t < e.length; t++)
                    a = (a << 5) - a + e.charCodeAt(t),
                    a &= a;
            return a
        }
        ,
        u._generateID = w,
        u._generateLocalMID = function() {
            var e = u._generateID(0);
            return N.isClientSideMarketingCloudVisitorID = !0,
            e
        }
        ,
        u._callbackList = null,
        u._callCallback = function(e, t) {
            try {
                "function" == typeof e ? e.apply(d, t) : e[1].apply(e[0], t)
            } catch (e) {}
        }
        ,
        u._registerCallback = function(e, t) {
            t && (null == u._callbackList && (u._callbackList = {}),
            null == u._callbackList[e] && (u._callbackList[e] = []),
            u._callbackList[e].push(t))
        }
        ,
        u._callAllCallbacks = function(e, t) {
            if (null != u._callbackList) {
                var a = u._callbackList[e];
                if (a)
                    for (; a.length > 0; )
                        u._callCallback(a.shift(), t)
            }
        }
        ,
        u._addQuerystringParam = function(e, t, a, n) {
            var i = encodeURIComponent(t) + "=" + encodeURIComponent(a)
              , r = k.parseHash(e)
              , o = k.hashlessUrl(e);
            if (-1 === o.indexOf("?"))
                return o + "?" + i + r;
            var s = o.split("?")
              , c = s[0] + "?"
              , l = s[1];
            return c + k.addQueryParamAtLocation(l, i, n) + r
        }
        ,
        u._extractParamFromUri = function(e, t) {
            var a = new RegExp("[\\?&#]" + t + "=([^&#]*)").exec(e);
            if (a && a.length)
                return decodeURIComponent(a[1])
        }
        ,
        u._parseAdobeMcFromUrl = n(B.ADOBE_MC),
        u._parseAdobeMcSdidFromUrl = n(B.ADOBE_MC_SDID),
        u._attemptToPopulateSdidFromUrl = function(t) {
            var a = u._parseAdobeMcSdidFromUrl(t)
              , n = 1e9;
            a && a.TS && (n = k.getTimestampInSeconds() - a.TS),
            a && a.SDID && a.MCORGID === e && n < u.sdidParamExpiry && (u._supplementalDataIDCurrent = a.SDID,
            u._supplementalDataIDCurrentConsumed.SDID_URL_PARAM = !0)
        }
        ,
        u._attemptToPopulateIdsFromUrl = function() {
            var t = u._parseAdobeMcFromUrl();
            if (t && t.TS) {
                var a = k.getTimestampInSeconds() - t.TS;
                if (Math.floor(a / 60) > B.ADOBE_MC_TTL_IN_MIN || t.MCORGID !== e)
                    return;
                i(t)
            }
        }
        ,
        u._mergeServerState = function(e) {
            if (e)
                try {
                    if (n = e,
                    (e = k.isObject(n) ? n : JSON.parse(n))[u.marketingCloudOrgID]) {
                        var t = e[u.marketingCloudOrgID];
                        a = t.customerIDs,
                        k.isObject(a) && u.setCustomerIDs(a),
                        o(t.sdid)
                    }
                } catch (e) {
                    throw new Error("`serverState` has an invalid format.")
                }
            var a, n
        }
        ,
        u._timeout = null,
        u._loadData = function(e, t, a, n) {
            t = u._addQuerystringParam(t, "d_fieldgroup", e, 1),
            n.url = u._addQuerystringParam(n.url, "d_fieldgroup", e, 1),
            n.corsUrl = u._addQuerystringParam(n.corsUrl, "d_fieldgroup", e, 1),
            N.fieldGroupObj[e] = !0,
            n === Object(n) && n.corsUrl && "XMLHttpRequest" === T.corsMetadata.corsType && T.fireCORS(n, a, e)
        }
        ,
        u._clearTimeout = function(e) {
            null != u._timeout && u._timeout[e] && (clearTimeout(u._timeout[e]),
            u._timeout[e] = 0)
        }
        ,
        u._settingsDigest = 0,
        u._getSettingsDigest = function() {
            if (!u._settingsDigest) {
                var e = u.version;
                u.audienceManagerServer && (e += "|" + u.audienceManagerServer),
                u.audienceManagerServerSecure && (e += "|" + u.audienceManagerServerSecure),
                u._settingsDigest = u._hash(e)
            }
            return u._settingsDigest
        }
        ,
        u._readVisitorDone = !1,
        u._readVisitor = function() {
            if (!u._readVisitorDone) {
                u._readVisitorDone = !0;
                var e, t, a, n, i, r, o = u._getSettingsDigest(), s = !1, c = u.cookieRead(u.cookieName), l = new Date;
                if (null == u._fields && (u._fields = {}),
                c && "T" !== c)
                    for ((c = c.split("|"))[0].match(/^[\-0-9]+$/) && (parseInt(c[0], 10) !== o && (s = !0),
                    c.shift()),
                    c.length % 2 == 1 && c.pop(),
                    e = 0; e < c.length; e += 2)
                        a = (t = c[e].split("-"))[0],
                        n = c[e + 1],
                        t.length > 1 ? (i = parseInt(t[1], 10),
                        r = t[1].indexOf("s") > 0) : (i = 0,
                        r = !1),
                        s && ("MCCIDH" === a && (n = ""),
                        i > 0 && (i = l.getTime() / 1e3 - 60)),
                        a && n && (u._setField(a, n, 1),
                        i > 0 && (u._fields["expire" + a] = i + (r ? "s" : ""),
                        (l.getTime() >= 1e3 * i || r && !u.cookieRead(u.sessionCookieName)) && (u._fieldsExpired || (u._fieldsExpired = {}),
                        u._fieldsExpired[a] = !0)));
                !u._getField(b) && k.isTrackingServerPopulated() && (c = u.cookieRead("s_vi")) && ((c = c.split("|")).length > 1 && c[0].indexOf("v1") >= 0 && ((e = (n = c[1]).indexOf("[")) >= 0 && (n = n.substring(0, e)),
                n && n.match(B.VALID_VISITOR_ID_REGEX) && u._setField(b, n)))
            }
        }
        ,
        u._appendVersionTo = function(e) {
            var t = "vVersion|" + u.version
              , a = e ? u._getCookieVersion(e) : null;
            return a ? A.areVersionsDifferent(a, u.version) && (e = e.replace(B.VERSION_REGEX, t)) : e += (e ? "|" : "") + t,
            e
        }
        ,
        u._writeVisitor = function() {
            var e, t, a = u._getSettingsDigest();
            for (e in u._fields)
                _(e) && u._fields[e] && "expire" !== e.substring(0, 6) && (t = u._fields[e],
                a += (a ? "|" : "") + e + (u._fields["expire" + e] ? "-" + u._fields["expire" + e] : "") + "|" + t);
            a = u._appendVersionTo(a),
            u.cookieWrite(u.cookieName, a, 1)
        }
        ,
        u._getField = function(e, t) {
            return null == u._fields || !t && u._fieldsExpired && u._fieldsExpired[e] ? null : u._fields[e]
        }
        ,
        u._setField = function(e, t, a) {
            null == u._fields && (u._fields = {}),
            u._fields[e] = t,
            a || u._writeVisitor()
        }
        ,
        u._getFieldList = function(e, t) {
            var a = u._getField(e, t);
            return a ? a.split("*") : null
        }
        ,
        u._setFieldList = function(e, t, a) {
            u._setField(e, t ? t.join("*") : "", a)
        }
        ,
        u._getFieldMap = function(e, t) {
            var a = u._getFieldList(e, t);
            if (a) {
                var n, i = {};
                for (n = 0; n < a.length; n += 2)
                    i[a[n]] = a[n + 1];
                return i
            }
            return null
        }
        ,
        u._setFieldMap = function(e, t, a) {
            var n, i = null;
            if (t)
                for (n in i = [],
                t)
                    _(n) && (i.push(n),
                    i.push(t[n]));
            u._setFieldList(e, i, a)
        }
        ,
        u._setFieldExpire = function(e, t, a) {
            var n = new Date;
            n.setTime(n.getTime() + 1e3 * t),
            null == u._fields && (u._fields = {}),
            u._fields["expire" + e] = Math.floor(n.getTime() / 1e3) + (a ? "s" : ""),
            t < 0 ? (u._fieldsExpired || (u._fieldsExpired = {}),
            u._fieldsExpired[e] = !0) : u._fieldsExpired && (u._fieldsExpired[e] = !1),
            a && (u.cookieRead(u.sessionCookieName) || u.cookieWrite(u.sessionCookieName, "1"))
        }
        ,
        u._findVisitorID = function(e) {
            return e && ("object" == typeof e && (e = e.d_mid ? e.d_mid : e.visitorID ? e.visitorID : e.id ? e.id : e.uuid ? e.uuid : "" + e),
            e && "NOTARGET" === (e = e.toUpperCase()) && (e = E),
            e && (e === E || e.match(B.VALID_VISITOR_ID_REGEX)) || (e = "")),
            e
        }
        ,
        u._setFields = function(e, t) {
            if (u._clearTimeout(e),
            null != u._loading && (u._loading[e] = !1),
            N.fieldGroupObj[e] && N.setState(e, !1),
            "MC" === e) {
                !0 !== N.isClientSideMarketingCloudVisitorID && (N.isClientSideMarketingCloudVisitorID = !1);
                var a = u._getField(m);
                if (!a || u.overwriteCrossDomainMCIDAndAID) {
                    if (!(a = "object" == typeof t && t.mid ? t.mid : u._findVisitorID(t))) {
                        if (u._use1stPartyMarketingCloudServer && !u.tried1stPartyMarketingCloudServer)
                            return u.tried1stPartyMarketingCloudServer = !0,
                            void u.getAnalyticsVisitorID(null, !1, !0);
                        a = u._generateLocalMID()
                    }
                    u._setField(m, a)
                }
                a && a !== E || (a = ""),
                "object" == typeof t && ((t.d_region || t.dcs_region || t.d_blob || t.blob) && u._setFields(L, t),
                u._use1stPartyMarketingCloudServer && t.mid && u._setFields(y, {
                    id: t.id
                })),
                u._callAllCallbacks(m, [a])
            }
            if (e === L && "object" == typeof t) {
                var n = 604800;
                null != t.id_sync_ttl && t.id_sync_ttl && (n = parseInt(t.id_sync_ttl, 10));
                var i = P.getRegionAndCheckIfChanged(t, n);
                u._callAllCallbacks("MCAAMLH", [i]);
                var r = u._getField(C);
                (t.d_blob || t.blob) && ((r = t.d_blob) || (r = t.blob),
                u._setFieldExpire(C, n),
                u._setField(C, r)),
                r || (r = ""),
                u._callAllCallbacks(C, [r]),
                !t.error_msg && u._newCustomerIDsHash && u._setField("MCCIDH", u._newCustomerIDsHash)
            }
            if (e === y) {
                var o = u._getField(b);
                o && !u.overwriteCrossDomainMCIDAndAID || ((o = u._findVisitorID(t)) ? o !== E && u._setFieldExpire(C, -1) : o = E,
                u._setField(b, o)),
                o && o !== E || (o = ""),
                u._callAllCallbacks(b, [o])
            }
            if (u.idSyncDisableSyncs || u.disableIdSyncs)
                P.idCallNotProcesssed = !0;
            else {
                P.idCallNotProcesssed = !1;
                var s = {};
                s.ibs = t.ibs,
                s.subdomain = t.subdomain,
                P.processIDCallData(s)
            }
            var c, l;
            t === Object(t) && (u.isAllowed() && (c = u._getField("MCOPTOUT")),
            c || (c = E,
            t.d_optout && t.d_optout instanceof Array && (c = t.d_optout.join(",")),
            l = parseInt(t.d_ottl, 10),
            isNaN(l) && (l = 7200),
            u._setFieldExpire("MCOPTOUT", l, !0),
            u._setField("MCOPTOUT", c)),
            u._callAllCallbacks("MCOPTOUT", [c]))
        }
        ,
        u._loading = null,
        u._getRemoteField = function(e, t, a, n, i) {
            var r, o = "", s = k.isFirstPartyAnalyticsVisitorIDCall(e), c = {
                MCAAMLH: !0,
                MCAAMB: !0
            };
            if (u.isAllowed())
                if (u._readVisitor(),
                !(!(o = u._getField(e, !0 === c[e])) || u._fieldsExpired && u._fieldsExpired[e]) || u.disableThirdPartyCalls && !s)
                    o || (e === m ? (u._registerCallback(e, a),
                    o = u._generateLocalMID(),
                    u.setMarketingCloudVisitorID(o)) : e === b ? (u._registerCallback(e, a),
                    o = "",
                    u.setAnalyticsVisitorID(o)) : (o = "",
                    n = !0));
                else if (e === m || "MCOPTOUT" === e ? r = "MC" : "MCAAMLH" === e || e === C ? r = L : e === b && (r = y),
                r)
                    return !t || null != u._loading && u._loading[r] || (null == u._loading && (u._loading = {}),
                    u._loading[r] = !0,
                    u._loadData(r, t, function(t) {
                        if (!u._getField(e)) {
                            t && N.setState(r, !0);
                            var a = "";
                            e === m ? a = u._generateLocalMID() : r === L && (a = {
                                error_msg: "timeout"
                            }),
                            u._setFields(r, a)
                        }
                    }, i)),
                    u._registerCallback(e, a),
                    o || (t || u._setFields(r, {
                        id: E
                    }),
                    "");
            return e !== m && e !== b || o !== E || (o = "",
            n = !0),
            a && n && u._callCallback(a, [o]),
            o
        }
        ,
        u._setMarketingCloudFields = function(e) {
            u._readVisitor(),
            u._setFields("MC", e)
        }
        ,
        u._mapCustomerIDs = function(e) {
            u.getAudienceManagerBlob(e, !0)
        }
        ,
        u._setAnalyticsFields = function(e) {
            u._readVisitor(),
            u._setFields(y, e)
        }
        ,
        u._setAudienceManagerFields = function(e) {
            u._readVisitor(),
            u._setFields(L, e)
        }
        ,
        u._getAudienceManagerURLData = function(e) {
            var t = u.audienceManagerServer
              , a = ""
              , n = u._getField(m)
              , i = u._getField(C, !0)
              , r = u._getField(b)
              , o = r && r !== E ? "&d_cid_ic=AVID%01" + encodeURIComponent(r) : "";
            if (u.loadSSL && u.audienceManagerServerSecure && (t = u.audienceManagerServerSecure),
            t) {
                var s, c, l = u.getCustomerIDs();
                if (l)
                    for (s in l)
                        _(s) && (c = l[s],
                        o += "&d_cid_ic=" + encodeURIComponent(s) + "%01" + encodeURIComponent(c.id ? c.id : "") + (c.authState ? "%01" + c.authState : ""));
                e || (e = "_setAudienceManagerFields");
                var d = "http" + (u.loadSSL ? "s" : "") + "://" + t + "/id"
                  , g = "d_visid_ver=" + u.version + "&d_rtbd=json&d_ver=2" + (!n && u._use1stPartyMarketingCloudServer ? "&d_verify=1" : "") + "&d_orgid=" + encodeURIComponent(u.marketingCloudOrgID) + "&d_nsid=" + (u.idSyncContainerID || 0) + (n ? "&d_mid=" + encodeURIComponent(n) : "") + (u.idSyncDisable3rdPartySyncing || u.disableThirdPartyCookies ? "&d_coppa=true" : "") + (!0 === f ? "&d_coop_safe=1" : !1 === f ? "&d_coop_unsafe=1" : "") + (i ? "&d_blob=" + encodeURIComponent(i) : "") + o
                  , p = ["s_c_il", u._in, e];
                return {
                    url: a = d + "?" + g + "&d_cb=s_c_il%5B" + u._in + "%5D." + e,
                    corsUrl: d + "?" + g,
                    callback: p
                }
            }
            return {
                url: a
            }
        }
        ,
        u.appendVisitorIDsTo = function(e) {
            try {
                var t = [[m, u._getField(m)], [b, u._getField(b)], ["MCORGID", u.marketingCloudOrgID]];
                return u._addQuerystringParam(e, B.ADOBE_MC, s(t))
            } catch (t) {
                return e
            }
        }
        ,
        u.appendSupplementalDataIDTo = function(e, t) {
            if (!(t = t || u.getSupplementalDataID(k.generateRandomString(), !0)))
                return e;
            try {
                var a = s([["SDID", t], ["MCORGID", u.marketingCloudOrgID]]);
                return u._addQuerystringParam(e, B.ADOBE_MC_SDID, a)
            } catch (t) {
                return e
            }
        }
        ;
        var k = {
            parseHash: function(e) {
                var t = e.indexOf("#");
                return t > 0 ? e.substr(t) : ""
            },
            hashlessUrl: function(e) {
                var t = e.indexOf("#");
                return t > 0 ? e.substr(0, t) : e
            },
            addQueryParamAtLocation: function(e, t, a) {
                var n = e.split("&");
                return a = null != a ? a : n.length,
                n.splice(a, 0, t),
                n.join("&")
            },
            isFirstPartyAnalyticsVisitorIDCall: function(e, t, a) {
                return e === b && (t || (t = u.trackingServer),
                a || (a = u.trackingServerSecure),
                !("string" != typeof (n = u.loadSSL ? a : t) || !n.length) && n.indexOf("2o7.net") < 0 && n.indexOf("omtrdc.net") < 0);
                var n
            },
            isObject: function(e) {
                return Boolean(e && e === Object(e))
            },
            removeCookie: function(e) {
                document.cookie = encodeURIComponent(e) + "=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;" + (u.cookieDomain ? " domain=" + u.cookieDomain + ";" : "")
            },
            isTrackingServerPopulated: function() {
                return !!u.trackingServer || !!u.trackingServerSecure
            },
            getTimestampInSeconds: function() {
                return Math.round((new Date).getTime() / 1e3)
            },
            parsePipeDelimetedKeyValues: function(e) {
                return e.split("|").reduce(function(e, t) {
                    var a = t.split("=");
                    return e[a[0]] = decodeURIComponent(a[1]),
                    e
                }, {})
            },
            generateRandomString: function(e) {
                e = e || 5;
                for (var t = "", a = "abcdefghijklmnopqrstuvwxyz0123456789"; e--; )
                    t += a[Math.floor(Math.random() * a.length)];
                return t
            },
            parseBoolean: function(e) {
                return "true" === e || "false" !== e && null
            },
            replaceMethodsWithFunction: function(e, t) {
                for (var a in e)
                    e.hasOwnProperty(a) && "function" == typeof e[a] && (e[a] = t);
                return e
            },
            pluck: function(e, t) {
                return t.reduce(function(t, a) {
                    return e[a] && (t[a] = e[a]),
                    t
                }, Object.create(null))
            }
        };
        u._helpers = k;
        var P = R(u, g);
        u._destinationPublishing = P,
        u.timeoutMetricsLog = [];
        var N = {
            isClientSideMarketingCloudVisitorID: null,
            MCIDCallTimedOut: null,
            AnalyticsIDCallTimedOut: null,
            AAMIDCallTimedOut: null,
            fieldGroupObj: {},
            setState: function(e, t) {
                switch (e) {
                case "MC":
                    !1 === t ? !0 !== this.MCIDCallTimedOut && (this.MCIDCallTimedOut = !1) : this.MCIDCallTimedOut = t;
                    break;
                case y:
                    !1 === t ? !0 !== this.AnalyticsIDCallTimedOut && (this.AnalyticsIDCallTimedOut = !1) : this.AnalyticsIDCallTimedOut = t;
                    break;
                case L:
                    !1 === t ? !0 !== this.AAMIDCallTimedOut && (this.AAMIDCallTimedOut = !1) : this.AAMIDCallTimedOut = t
                }
            }
        };
        u.isClientSideMarketingCloudVisitorID = function() {
            return N.isClientSideMarketingCloudVisitorID
        }
        ,
        u.MCIDCallTimedOut = function() {
            return N.MCIDCallTimedOut
        }
        ,
        u.AnalyticsIDCallTimedOut = function() {
            return N.AnalyticsIDCallTimedOut
        }
        ,
        u.AAMIDCallTimedOut = function() {
            return N.AAMIDCallTimedOut
        }
        ,
        u.idSyncGetOnPageSyncInfo = function() {
            return u._readVisitor(),
            u._getField("MCSYNCSOP")
        }
        ,
        u.idSyncByURL = function(e) {
            var t = l(e || {});
            if (t.error)
                return t.error;
            var a, n, i = e.url, r = encodeURIComponent, o = P;
            return i = i.replace(/^https:/, "").replace(/^http:/, ""),
            a = v.encodeAndBuildRequest(["", e.dpid, e.dpuuid || ""], ","),
            n = ["ibs", r(e.dpid), "img", r(i), t.ttl, "", a],
            o.addMessage(n.join("|")),
            o.requestToProcess(),
            "Successfully queued"
        }
        ,
        u.idSyncByDataSource = function(e) {
            return e === Object(e) && "string" == typeof e.dpuuid && e.dpuuid.length ? (e.url = "//dpm.demdex.net/ibs:dpid=" + e.dpid + "&dpuuid=" + e.dpuuid,
            u.idSyncByURL(e)) : "Error: config or config.dpuuid is empty"
        }
        ,
        u.publishDestinations = function(e, t, a) {
            if (a = "function" == typeof a ? a : function() {}
            ,
            "string" == typeof e && e.length)
                if (t instanceof Array && t.length) {
                    var n = P;
                    if (n.readyToAttachIframePreliminary()) {
                        var i = !1;
                        t.forEach(function(e) {
                            "string" == typeof e && e.length && (n.addMessage(e),
                            i = !0)
                        }),
                        i ? n.iframe ? (a({
                            message: "The destination publishing iframe is already attached and loaded."
                        }),
                        n.requestToProcess()) : !u.subdomain && u._getField(m) ? (n.subdomain = e,
                        n.doAttachIframe = !0,
                        n.url = n.getUrl(),
                        n.readyToAttachIframe() ? (n.iframeLoadedCallbacks.push(function(e) {
                            a({
                                message: "Attempted to attach and load the destination publishing iframe through this API call. Result: " + (e.message || "no result")
                            })
                        }),
                        n.attachIframe()) : a({
                            error: "Encountered a problem in attempting to attach and load the destination publishing iframe through this API call."
                        })) : n.iframeLoadedCallbacks.push(function(e) {
                            a({
                                message: "Attempted to attach and load the destination publishing iframe through normal Visitor API processing. Result: " + (e.message || "no result")
                            })
                        }) : a({
                            error: "None of the messages are populated strings."
                        })
                    } else
                        a({
                            error: "The destination publishing iframe is disabled in the Visitor library."
                        })
                } else
                    a({
                        error: "messages is not a populated array."
                    });
            else
                a({
                    error: "subdomain is not a populated string."
                })
        }
        ,
        u._getCookieVersion = function(e) {
            e = e || u.cookieRead(u.cookieName);
            var t = B.VERSION_REGEX.exec(e);
            return t && t.length > 1 ? t[1] : null
        }
        ,
        u._resetAmcvCookie = function(e) {
            var t = u._getCookieVersion();
            t && !A.isLessThan(t, e) || k.removeCookie(u.cookieName)
        }
        ,
        u.setAsCoopSafe = function() {
            f = !0
        }
        ,
        u.setAsCoopUnsafe = function() {
            f = !1
        }
        ,
        u.init = function() {
            !function() {
                if (t && "object" == typeof t) {
                    for (var e in u.configs = Object.create(null),
                    t)
                        _(e) && (u[e] = t[e],
                        u.configs[e] = t[e]);
                    u.idSyncContainerID = u.idSyncContainerID || 0,
                    f = "boolean" == typeof u.isCoopSafe ? u.isCoopSafe : k.parseBoolean(u.isCoopSafe),
                    u.resetBeforeVersion && u._resetAmcvCookie(u.resetBeforeVersion),
                    u._attemptToPopulateIdsFromUrl(),
                    u._attemptToPopulateSdidFromUrl(),
                    u._readVisitor();
                    var a = u._getField(h)
                      , n = Math.ceil((new Date).getTime() / B.MILLIS_PER_DAY);
                    u.idSyncDisableSyncs || u.disableIdSyncs || !P.canMakeSyncIDCall(a, n) || (u._setFieldExpire(C, -1),
                    u._setField(h, n)),
                    u.getMarketingCloudVisitorID(),
                    u.getAudienceManagerLocationHint(),
                    u.getAudienceManagerBlob(),
                    u._mergeServerState(u.serverState)
                } else
                    u._attemptToPopulateIdsFromUrl(),
                    u._attemptToPopulateSdidFromUrl()
            }(),
            function() {
                if (!u.idSyncDisableSyncs && !u.disableIdSyncs) {
                    P.checkDPIframeSrc();
                    var e = function() {
                        var e = P;
                        e.readyToAttachIframe() && e.attachIframe()
                    };
                    d.addEventListener("load", function() {
                        g.windowLoaded = !0,
                        e()
                    });
                    try {
                        V.receiveMessage(function(e) {
                            P.receiveMessage(e.data)
                        }, P.iframeHost)
                    } catch (e) {}
                }
            }(),
            u.whitelistIframeDomains && B.POST_MESSAGE_ENABLED && (u.whitelistIframeDomains = u.whitelistIframeDomains instanceof Array ? u.whitelistIframeDomains : [u.whitelistIframeDomains],
            u.whitelistIframeDomains.forEach(function(t) {
                var a = new S(e,t)
                  , n = D(u, a);
                V.receiveMessage(n, t)
            }))
        }
    };
    return U.getInstance = function(e, t) {
        if (!e)
            throw new Error("Visitor requires Adobe Marketing Cloud Org ID.");
        e.indexOf("@") < 0 && (e += "@AdobeOrg");
        var a = function() {
            var t = r.s_c_il;
            if (t)
                for (var a = 0; a < t.length; a++) {
                    var n = t[a];
                    if (n && "Visitor" === n._c && n.marketingCloudOrgID === e)
                        return n
                }
        }();
        if (a)
            return a;
        var n = e.split("").reverse().join("")
          , i = new U(e,null,n);
        t === Object(t) && t.cookieDomain && (i.cookieDomain = t.cookieDomain),
        r.s_c_il.splice(--r.s_c_in, 1);
        var o = v.getIeVersion();
        if ("number" == typeof o && o < 10)
            return i._helpers.replaceMethodsWithFunction(i, function() {});
        var s, c = function() {
            try {
                return r.self !== r.parent
            } catch (e) {
                return !0
            }
        }() && ((s = i).cookieWrite("TEST_AMCV_COOKIE", "T", 1),
        "T" !== s.cookieRead("TEST_AMCV_COOKIE") || (s._helpers.removeCookie("TEST_AMCV_COOKIE"),
        0)) && r.parent ? new E(e,t,i,r.parent) : new U(e,t,n);
        return i = null,
        c.init(),
        c
    }
    ,
    function() {
        function e() {
            U.windowLoaded = !0
        }
        r.addEventListener ? r.addEventListener("load", e) : r.attachEvent && r.attachEvent("onload", e),
        U.codeLoadEnd = (new Date).getTime()
    }(),
    U.config = G,
    r.Visitor = U,
    U
}();
// All code and conventions are protected by copyright
// All code and conventions are protected by copyright
!function(t, a, n) {
    function i() {
        I.getToolsByType("nielsen").length > 0 && I.domReady(I.bind(this.initialize, this))
    }
    function r(e) {
        I.domReady(I.bind(function() {
            this.twttr = e || t.twttr,
            this.initialize()
        }, this))
    }
    function o() {
        this.lastURL = I.URL(),
        this._fireIfURIChanged = I.bind(this.fireIfURIChanged, this),
        this._onPopState = I.bind(this.onPopState, this),
        this._onHashChange = I.bind(this.onHashChange, this),
        this._pushState = I.bind(this.pushState, this),
        this._replaceState = I.bind(this.replaceState, this),
        this.initialize()
    }
    function c() {
        var e = I.filter(I.rules, function(e) {
            return 0 === e.event.indexOf("dataelementchange")
        });
        this.dataElementsNames = I.map(e, function(e) {
            return e.event.match(/dataelementchange\((.*)\)/i)[1]
        }, this),
        this.initPolling()
    }
    function l() {
        I.addEventHandler(t, "orientationchange", l.orientationChange)
    }
    function u() {
        this.rules = I.filter(I.rules, function(e) {
            return "videoplayed" === e.event.substring(0, 11)
        }),
        this.eventHandler = I.bind(this.onUpdateTime, this)
    }
    function d() {
        this.defineEvents(),
        this.visibilityApiHasPriority = !0,
        a.addEventListener ? this.setVisibilityApiPriority(!1) : this.attachDetachOlderEventListeners(!0, a, "focusout");
        I.bindEvent("aftertoolinit", function() {
            I.fireEvent(I.visibility.isHidden() ? "tabblur" : "tabfocus")
        })
    }
    function g(e) {
        e = e || I.rules,
        this.rules = I.filter(e, function(e) {
            return "inview" === e.event
        }),
        this.elements = [],
        this.eventHandler = I.bind(this.track, this),
        I.addEventHandler(t, "scroll", this.eventHandler),
        I.addEventHandler(t, "load", this.eventHandler)
    }
    function p() {
        this.rules = I.filter(I.rules, function(e) {
            return "elementexists" === e.event
        })
    }
    function f(e) {
        this.delay = 250,
        this.FB = e,
        I.domReady(I.bind(function() {
            I.poll(I.bind(this.initialize, this), this.delay, 8)
        }, this))
    }
    function m() {
        var e = this.eventRegex = /^hover\(([0-9]+)\)$/
          , t = this.rules = [];
        I.each(I.rules, function(a) {
            a.event.match(e) && t.push([Number(a.event.match(e)[1]), a.selector])
        })
    }
    function h(e) {
        I.BaseTool.call(this, e),
        this.defineListeners(),
        this.beaconMethod = "plainBeacon",
        this.adapt = new h.DataAdapters,
        this.dataProvider = new h.DataProvider.Aggregate
    }
    function v(e) {
        I.BaseTool.call(this, e),
        this.styleElements = {},
        this.targetPageParamsStore = {}
    }
    function y() {
        I.BaseTool.call(this),
        this.asyncScriptCallbackQueue = [],
        this.argsForBlockingScripts = []
    }
    function b(e) {
        I.BaseTool.call(this, e),
        this.varBindings = {},
        this.events = [],
        this.products = [],
        this.customSetupFuns = []
    }
    function L(e) {
        I.BaseTool.call(this, e),
        this.name = e.name || "Basic"
    }
    function S(e) {
        I.BaseTool.call(this, e)
    }
    function C(e) {
        I.BaseTool.call(this, e)
    }
    function E(e) {
        I.BaseTool.call(this, e),
        this.name = e.name || "VisitorID",
        this.initialize()
    }
    var _, T, k, P = Object.prototype.toString, D = t._satellite && t._satellite.override, I = {
        initialized: !1,
        $data: function(e, t, a) {
            if (e) {
                var i = "__satellite__"
                  , r = I.dataCache
                  , o = e[i];
                o || (o = e[i] = I.uuid++);
                var s = r[o];
                if (s || (s = r[o] = {}),
                a === n)
                    return s[t];
                s[t] = a
            }
        },
        uuid: 1,
        dataCache: {},
        keys: function(e) {
            var t = [];
            for (var a in e)
                e.hasOwnProperty(a) && t.push(a);
            return t
        },
        values: function(e) {
            var t = [];
            for (var a in e)
                e.hasOwnProperty(a) && t.push(e[a]);
            return t
        },
        isArray: Array.isArray || function(e) {
            return "[object Array]" === P.apply(e)
        }
        ,
        isObject: function(e) {
            return null != e && !I.isArray(e) && "object" == typeof e
        },
        isString: function(e) {
            return "string" == typeof e
        },
        isNumber: function(e) {
            return "[object Number]" === P.apply(e) && !I.isNaN(e)
        },
        isNaN: function(e) {
            return e != e
        },
        isRegex: function(e) {
            return e instanceof RegExp
        },
        isLinkTag: function(e) {
            return !(!e || !e.nodeName || "a" !== e.nodeName.toLowerCase())
        },
        each: function(e, t, a) {
            for (var n = 0, i = e.length; n < i; n++)
                t.call(a, e[n], n, e)
        },
        map: function(e, t, a) {
            for (var n = [], i = 0, r = e.length; i < r; i++)
                n.push(t.call(a, e[i], i, e));
            return n
        },
        filter: function(e, t, a) {
            for (var n = [], i = 0, r = e.length; i < r; i++) {
                var o = e[i];
                t.call(a, o, i, e) && n.push(o)
            }
            return n
        },
        any: function(e, t, a) {
            for (var n = 0, i = e.length; n < i; n++) {
                var r = e[n];
                if (t.call(a, r, n, e))
                    return !0
            }
            return !1
        },
        every: function(e, t, a) {
            for (var n = !0, i = 0, r = e.length; i < r; i++) {
                var o = e[i];
                n = n && t.call(a, o, i, e)
            }
            return n
        },
        contains: function(e, t) {
            return -1 !== I.indexOf(e, t)
        },
        indexOf: function(e, t) {
            if (e.indexOf)
                return e.indexOf(t);
            for (var a = e.length; a--; )
                if (t === e[a])
                    return a;
            return -1
        },
        find: function(e, t, a) {
            if (!e)
                return null;
            for (var n = 0, i = e.length; n < i; n++) {
                var r = e[n];
                if (t.call(a, r, n, e))
                    return r
            }
            return null
        },
        textMatch: function(e, t) {
            if (null == t)
                throw new Error("Illegal Argument: Pattern is not present");
            return null != e && ("string" == typeof t ? e === t : t instanceof RegExp && t.test(e))
        },
        stringify: function(e, t) {
            if (t = t || [],
            I.isObject(e)) {
                if (I.contains(t, e))
                    return "<Cycle>";
                t.push(e)
            }
            if (I.isArray(e))
                return "[" + I.map(e, function(e) {
                    return I.stringify(e, t)
                }).join(",") + "]";
            if (I.isString(e))
                return '"' + String(e) + '"';
            if (I.isObject(e)) {
                var a = [];
                for (var n in e)
                    e.hasOwnProperty(n) && a.push(n + ": " + I.stringify(e[n], t));
                return "{" + a.join(", ") + "}"
            }
            return String(e)
        },
        trim: function(e) {
            return null == e ? null : e.trim ? e.trim() : e.replace(/^ */, "").replace(/ *$/, "")
        },
        bind: function(e, t) {
            return function() {
                return e.apply(t, arguments)
            }
        },
        throttle: function(e, t) {
            var a = null;
            return function() {
                var n = this
                  , i = arguments;
                clearTimeout(a),
                a = setTimeout(function() {
                    e.apply(n, i)
                }, t)
            }
        },
        domReady: function(t) {
            function n(e) {
                for (p = 1; e = r.shift(); )
                    e()
            }
            var i, r = [], o = !1, s = a, c = s.documentElement, l = c.doScroll, u = "DOMContentLoaded", d = "addEventListener", g = "onreadystatechange", p = /^loade|^c/.test(s.readyState);
            return s[d] && s[d](u, i = function() {
                s.removeEventListener(u, i, o),
                n()
            }
            , o),
            l && s.attachEvent(g, i = function() {
                /^c/.test(s.readyState) && (s.detachEvent(g, i),
                n())
            }
            ),
            t = l ? function(a) {
                self != top ? p ? a() : r.push(a) : function() {
                    try {
                        c.doScroll("left")
                    } catch (e) {
                        return setTimeout(function() {
                            t(a)
                        }, 50)
                    }
                    a()
                }()
            }
            : function(e) {
                p ? e() : r.push(e)
            }
        }(),
        loadScript: function(e, t) {
            var n = a.createElement("script");
            I.scriptOnLoad(e, n, t),
            n.src = e,
            a.getElementsByTagName("head")[0].appendChild(n)
        },
        scriptOnLoad: function(e, t, a) {
            function n(e) {
                e && I.logError(e),
                a && a(e)
            }
            "onload"in t ? (t.onload = function() {
                n()
            }
            ,
            t.onerror = function() {
                n(new Error("Failed to load script " + e))
            }
            ) : "readyState"in t && (t.onreadystatechange = function() {
                var e = t.readyState;
                "loaded" !== e && "complete" !== e || (t.onreadystatechange = null,
                n())
            }
            )
        },
        loadScriptOnce: function(e, t) {
            I.loadedScriptRegistry[e] || I.loadScript(e, function(a) {
                a || (I.loadedScriptRegistry[e] = !0),
                t && t(a)
            })
        },
        loadedScriptRegistry: {},
        loadScriptSync: function(e) {
            a.write ? I.domReadyFired ? I.notify('Cannot load sync the "' + e + '" script after DOM Ready.', 1) : (e.indexOf('"') > -1 && (e = encodeURI(e)),
            a.write('<script src="' + e + '"></script>')) : I.notify('Cannot load sync the "' + e + '" script because "document.write" is not available', 1)
        },
        pushAsyncScript: function(e) {
            I.tools["default"].pushAsyncScript(e)
        },
        pushBlockingScript: function(e) {
            I.tools["default"].pushBlockingScript(e)
        },
        addEventHandler: t.addEventListener ? function(e, t, a) {
            e.addEventListener(t, a, !1)
        }
        : function(e, t, a) {
            e.attachEvent("on" + t, a)
        }
        ,
        removeEventHandler: t.removeEventListener ? function(e, t, a) {
            e.removeEventListener(t, a, !1)
        }
        : function(e, t, a) {
            e.detachEvent("on" + t, a)
        }
        ,
        preventDefault: t.addEventListener ? function(e) {
            e.preventDefault()
        }
        : function(e) {
            e.returnValue = !1
        }
        ,
        stopPropagation: function(e) {
            e.cancelBubble = !0,
            e.stopPropagation && e.stopPropagation()
        },
        containsElement: function(e, t) {
            return e.contains ? e.contains(t) : !!(16 & e.compareDocumentPosition(t))
        },
        matchesCss: function(n) {
            function i(e, t) {
                var a = t.tagName;
                return !!a && e.toLowerCase() === a.toLowerCase()
            }
            var r = n.matchesSelector || n.mozMatchesSelector || n.webkitMatchesSelector || n.oMatchesSelector || n.msMatchesSelector;
            return r ? function(n, i) {
                if (i === a || i === t)
                    return !1;
                try {
                    return r.call(i, n)
                } catch (e) {
                    return !1
                }
            }
            : n.querySelectorAll ? function(t, a) {
                if (!a.parentNode)
                    return !1;
                if (t.match(/^[a-z]+$/i))
                    return i(t, a);
                try {
                    for (var n = a.parentNode.querySelectorAll(t), r = n.length; r--; )
                        if (n[r] === a)
                            return !0
                } catch (e) {}
                return !1
            }
            : function(t, a) {
                if (t.match(/^[a-z]+$/i))
                    return i(t, a);
                try {
                    return I.Sizzle.matches(t, [a]).length > 0
                } catch (e) {
                    return !1
                }
            }
        }(a.documentElement),
        cssQuery: (_ = a,
        _.querySelectorAll ? function(t, a) {
            var n;
            try {
                n = _.querySelectorAll(t)
            } catch (e) {
                n = []
            }
            a(n)
        }
        : function(t, a) {
            if (I.Sizzle) {
                var n;
                try {
                    n = I.Sizzle(t)
                } catch (e) {
                    n = []
                }
                a(n)
            } else
                I.sizzleQueue.push([t, a])
        }
        ),
        hasAttr: function(e, t) {
            return e.hasAttribute ? e.hasAttribute(t) : e[t] !== n
        },
        inherit: function(e, t) {
            var a = function() {};
            a.prototype = t.prototype,
            e.prototype = new a,
            e.prototype.constructor = e
        },
        extend: function(e, t) {
            for (var a in t)
                t.hasOwnProperty(a) && (e[a] = t[a])
        },
        toArray: function() {
            try {
                var t = Array.prototype.slice;
                return t.call(a.documentElement.childNodes, 0)[0].nodeType,
                function(e) {
                    return t.call(e, 0)
                }
            } catch (e) {
                return function(e) {
                    for (var t = [], a = 0, n = e.length; a < n; a++)
                        t.push(e[a]);
                    return t
                }
            }
        }(),
        equalsIgnoreCase: function(e, t) {
            return null == e ? null == t : null != t && String(e).toLowerCase() === String(t).toLowerCase()
        },
        poll: function(e, t, a) {
            function n() {
                I.isNumber(a) && i++ >= a || e() || setTimeout(n, t)
            }
            var i = 0;
            t = t || 1e3,
            n()
        },
        escapeForHtml: function(e) {
            return e ? String(e).replace(/\&/g, "&amp;").replace(/\</g, "&lt;").replace(/\>/g, "&gt;").replace(/\"/g, "&quot;").replace(/\'/g, "&#x27;").replace(/\//g, "&#x2F;") : e
        }
    };
    I.availableTools = {},
    I.availableEventEmitters = [],
    I.fireOnceEvents = ["condition", "elementexists"],
    I.initEventEmitters = function() {
        I.eventEmitters = I.map(I.availableEventEmitters, function(e) {
            return new e
        })
    }
    ,
    I.eventEmitterBackgroundTasks = function() {
        I.each(I.eventEmitters, function(e) {
            "backgroundTasks"in e && e.backgroundTasks()
        })
    }
    ,
    I.initTools = function(e) {
        var t = {
            "default": new y
        }
          , a = I.settings.euCookieName || "sat_track";
        for (var n in e)
            if (e.hasOwnProperty(n)) {
                var i, r, o;
                if ((i = e[n]).euCookie)
                    if ("true" !== I.readCookie(a))
                        continue;
                if (!(r = I.availableTools[i.engine])) {
                    var s = [];
                    for (var c in I.availableTools)
                        I.availableTools.hasOwnProperty(c) && s.push(c);
                    throw new Error("No tool engine named " + i.engine + ", available: " + s.join(",") + ".")
                }
                (o = new r(i)).id = n,
                t[n] = o
            }
        return t
    }
    ,
    I.preprocessArguments = function(e, t, a, n, i) {
        function r(e) {
            return n && I.isString(e) ? e.toLowerCase() : e
        }
        function o(e) {
            var c = {};
            for (var l in e)
                if (e.hasOwnProperty(l)) {
                    var u = e[l];
                    I.isObject(u) ? c[l] = o(u) : I.isArray(u) ? c[l] = s(u, n) : c[l] = r(I.replace(u, t, a, i))
                }
            return c
        }
        function s(e) {
            for (var n = [], i = 0, s = e.length; i < s; i++) {
                var c = e[i];
                I.isString(c) ? c = r(I.replace(c, t, a)) : c && c.constructor === Object && (c = o(c)),
                n.push(c)
            }
            return n
        }
        return e ? s(e, n) : e
    }
    ,
    I.execute = function(t, a, n, i) {
        function r(r) {
            var o = i[r || "default"];
            if (o)
                try {
                    o.triggerCommand(t, a, n)
                } catch (e) {
                    I.logError(e)
                }
        }
        if (!_satellite.settings.hideActivity)
            if (i = i || I.tools,
            t.engine) {
                var o = t.engine;
                for (var s in i)
                    if (i.hasOwnProperty(s)) {
                        var c = i[s];
                        c.settings && c.settings.engine === o && r(s)
                    }
            } else
                t.tool instanceof Array ? I.each(t.tool, function(e) {
                    r(e)
                }) : r(t.tool)
    }
    ,
    I.Logger = {
        outputEnabled: !1,
        messages: [],
        keepLimit: 100,
        flushed: !1,
        LEVELS: [null, null, "log", "info", "warn", "error"],
        message: function(e, t) {
            var a = this.LEVELS[t] || "log";
            this.messages.push([a, e]),
            this.messages.length > this.keepLimit && this.messages.shift(),
            this.outputEnabled && this.echo(a, e)
        },
        getHistory: function() {
            return this.messages
        },
        clearHistory: function() {
            this.messages = []
        },
        setOutputState: function(e) {
            this.outputEnabled != e && (this.outputEnabled = e,
            e ? this.flush() : this.flushed = !1)
        },
        echo: function(e, a) {
            t.console && t.console[e]("SATELLITE: " + a)
        },
        flush: function() {
            this.flushed || (I.each(this.messages, function(e) {
                !0 !== e[2] && (this.echo(e[0], e[1]),
                e[2] = !0)
            }, this),
            this.flushed = !0)
        }
    },
    I.notify = I.bind(I.Logger.message, I.Logger),
    I.cleanText = function(e) {
        return null == e ? null : I.trim(e).replace(/\s+/g, " ")
    }
    ,
    I.cleanText.legacy = function(e) {
        return null == e ? null : I.trim(e).replace(/\s{2,}/g, " ").replace(/[^\000-\177]*/g, "")
    }
    ,
    I.text = function(e) {
        return e.textContent || e.innerText
    }
    ,
    I.specialProperties = {
        text: I.text,
        cleanText: function(e) {
            return I.cleanText(I.text(e))
        }
    },
    I.getObjectProperty = function(e, t, a) {
        for (var i, r = t.split("."), o = e, s = I.specialProperties, c = 0, l = r.length; c < l; c++) {
            if (null == o)
                return n;
            var u = r[c];
            if (a && "@" === u.charAt(0))
                o = s[u.slice(1)](o);
            else if (o.getAttribute && (i = u.match(/^getAttribute\((.+)\)$/))) {
                var d = i[1];
                o = o.getAttribute(d)
            } else
                o = o[u]
        }
        return o
    }
    ,
    I.getToolsByType = function(e) {
        if (!e)
            throw new Error("Tool type is missing");
        var t = [];
        for (var a in I.tools)
            if (I.tools.hasOwnProperty(a)) {
                var n = I.tools[a];
                n.settings && n.settings.engine === e && t.push(n)
            }
        return t
    }
    ,
    I.setVar = function() {
        var e = I.data.customVars;
        if (null == e && (I.data.customVars = {},
        e = I.data.customVars),
        "string" == typeof arguments[0])
            e[arguments[0]] = arguments[1];
        else if (arguments[0]) {
            var t = arguments[0];
            for (var a in t)
                t.hasOwnProperty(a) && (e[a] = t[a])
        }
    }
    ,
    I.dataElementSafe = function(e, t) {
        if (arguments.length > 2) {
            var a = arguments[2];
            "pageview" === t ? I.dataElementSafe.pageviewCache[e] = a : "session" === t ? I.setCookie("_sdsat_" + e, a) : "visitor" === t && I.setCookie("_sdsat_" + e, a, 730)
        } else {
            if ("pageview" === t)
                return I.dataElementSafe.pageviewCache[e];
            if ("session" === t || "visitor" === t)
                return I.readCookie("_sdsat_" + e)
        }
    }
    ,
    I.dataElementSafe.pageviewCache = {},
    I.realGetDataElement = function(e) {
        var a;
        return e.selector ? I.hasSelector && I.cssQuery(e.selector, function(t) {
            if (t.length > 0) {
                var n = t[0];
                "text" === e.property ? a = n.innerText || n.textContent : e.property in n ? a = n[e.property] : I.hasAttr(n, e.property) && (a = n.getAttribute(e.property))
            }
        }) : e.queryParam ? a = e.ignoreCase ? I.getQueryParamCaseInsensitive(e.queryParam) : I.getQueryParam(e.queryParam) : e.cookie ? a = I.readCookie(e.cookie) : e.jsVariable ? a = I.getObjectProperty(t, e.jsVariable) : e.customJS ? a = e.customJS() : e.contextHub && (a = e.contextHub()),
        I.isString(a) && e.cleanText && (a = I.cleanText(a)),
        a
    }
    ,
    I.getDataElement = function(e, t, a) {
        if (null == (a = a || I.dataElements[e]))
            return I.settings.undefinedVarsReturnEmpty ? "" : null;
        var i = I.realGetDataElement(a);
        return i === n && a.storeLength ? i = I.dataElementSafe(e, a.storeLength) : i !== n && a.storeLength && I.dataElementSafe(e, a.storeLength, i),
        i || t || (i = a["default"] || ""),
        I.isString(i) && a.forceLowerCase && (i = i.toLowerCase()),
        i
    }
    ,
    I.getVar = function(e, i, r) {
        var o, s, c = I.data.customVars, l = r ? r.target || r.srcElement : null, u = {
            uri: I.URI(),
            protocol: a.location.protocol,
            hostname: a.location.hostname
        };
        if (I.dataElements && e in I.dataElements)
            return I.getDataElement(e);
        if ((s = u[e.toLowerCase()]) === n)
            if ("this." === e.substring(0, 5))
                e = e.slice(5),
                s = I.getObjectProperty(i, e, !0);
            else if ("event." === e.substring(0, 6))
                e = e.slice(6),
                s = I.getObjectProperty(r, e);
            else if ("target." === e.substring(0, 7))
                e = e.slice(7),
                s = I.getObjectProperty(l, e);
            else if ("window." === e.substring(0, 7))
                e = e.slice(7),
                s = I.getObjectProperty(t, e);
            else if ("param." === e.substring(0, 6))
                e = e.slice(6),
                s = I.getQueryParam(e);
            else if (o = e.match(/^rand([0-9]+)$/)) {
                var d = Number(o[1])
                  , g = (Math.random() * (Math.pow(10, d) - 1)).toFixed(0);
                s = Array(d - g.length + 1).join("0") + g
            } else
                s = I.getObjectProperty(c, e);
        return s
    }
    ,
    I.getVars = function(e, t, a) {
        var n = {};
        return I.each(e, function(e) {
            n[e] = I.getVar(e, t, a)
        }),
        n
    }
    ,
    I.replace = function(e, t, a, n) {
        return "string" != typeof e ? e : e.replace(/%(.*?)%/g, function(e, i) {
            var r = I.getVar(i, t, a);
            return null == r ? I.settings.undefinedVarsReturnEmpty ? "" : e : n ? I.escapeForHtml(r) : r
        })
    }
    ,
    I.escapeHtmlParams = function(e) {
        return e.escapeHtml = !0,
        e
    }
    ,
    I.searchVariables = function(e, t, a) {
        if (!e || 0 === e.length)
            return "";
        for (var n = [], i = 0, r = e.length; i < r; i++) {
            var o = e[i]
              , s = I.getVar(o, t, a);
            n.push(o + "=" + escape(s))
        }
        return "?" + n.join("&")
    }
    ,
    I.fireRule = function(e, t, a) {
        var n = e.trigger;
        if (n) {
            for (var i = 0, r = n.length; i < r; i++) {
                var o = n[i];
                I.execute(o, t, a)
            }
            I.contains(I.fireOnceEvents, e.event) && (e.expired = !0)
        }
    }
    ,
    I.isLinked = function(e) {
        for (var t = e; t; t = t.parentNode)
            if (I.isLinkTag(t))
                return !0;
        return !1
    }
    ,
    I.firePageLoadEvent = function(e) {
        for (var t = a.location, n = {
            type: e,
            target: t
        }, i = I.pageLoadRules, r = I.evtHandlers[n.type], o = i.length; o--; ) {
            var s = i[o];
            I.ruleMatches(s, n, t) && (I.notify('Rule "' + s.name + '" fired.', 1),
            I.fireRule(s, t, n))
        }
        for (var c in I.tools)
            if (I.tools.hasOwnProperty(c)) {
                var l = I.tools[c];
                l.endPLPhase && l.endPLPhase(e)
            }
        r && I.each(r, function(e) {
            e(n)
        })
    }
    ,
    I.track = function(e) {
        e = e.replace(/^\s*/, "").replace(/\s*$/, "");
        for (var t = 0; t < I.directCallRules.length; t++) {
            var a = I.directCallRules[t];
            if (a.name === e)
                return I.notify('Direct call Rule "' + e + '" fired.', 1),
                void I.fireRule(a, location, {
                    type: e
                })
        }
        I.notify('Direct call Rule "' + e + '" not found.', 1)
    }
    ,
    I.basePath = function() {
        return I.data.host ? ("https:" === a.location.protocol ? "https://" + I.data.host.https : "http://" + I.data.host.http) + "/" : this.settings.basePath
    }
    ,
    I.setLocation = function(e) {
        t.location = e
    }
    ,
    I.parseQueryParams = function(e) {
        var t = function(e) {
            var t = e;
            try {
                t = decodeURIComponent(e)
            } catch (a) {}
            return t
        };
        if ("" === e || !1 === I.isString(e))
            return {};
        0 === e.indexOf("?") && (e = e.substring(1));
        var a = {}
          , n = e.split("&");
        return I.each(n, function(e) {
            (e = e.split("="))[1] && (a[t(e[0])] = t(e[1]))
        }),
        a
    }
    ,
    I.getCaseSensitivityQueryParamsMap = function(e) {
        var t = I.parseQueryParams(e)
          , a = {};
        for (var n in t)
            t.hasOwnProperty(n) && (a[n.toLowerCase()] = t[n]);
        return {
            normal: t,
            caseInsensitive: a
        }
    }
    ,
    I.updateQueryParams = function() {
        I.QueryParams = I.getCaseSensitivityQueryParamsMap(t.location.search)
    }
    ,
    I.updateQueryParams(),
    I.getQueryParam = function(e) {
        return I.QueryParams.normal[e]
    }
    ,
    I.getQueryParamCaseInsensitive = function(e) {
        return I.QueryParams.caseInsensitive[e.toLowerCase()]
    }
    ,
    I.encodeObjectToURI = function(e) {
        if (!1 === I.isObject(e))
            return "";
        var t = [];
        for (var a in e)
            e.hasOwnProperty(a) && t.push(encodeURIComponent(a) + "=" + encodeURIComponent(e[a]));
        return t.join("&")
    }
    ,
    I.readCookie = function(e) {
        for (var t = e + "=", i = a.cookie.split(";"), r = 0; r < i.length; r++) {
            for (var o = i[r]; " " == o.charAt(0); )
                o = o.substring(1, o.length);
            if (0 === o.indexOf(t))
                return o.substring(t.length, o.length)
        }
        return n
    }
    ,
    I.setCookie = function(e, t, n) {
        var i;
        if (n) {
            var r = new Date;
            r.setTime(r.getTime() + 24 * n * 60 * 60 * 1e3),
            i = "; expires=" + r.toGMTString()
        } else
            i = "";
        a.cookie = e + "=" + t + i + "; path=/"
    }
    ,
    I.removeCookie = function(e) {
        I.setCookie(e, "", -1)
    }
    ,
    I.getElementProperty = function(e, t) {
        if ("@" === t.charAt(0)) {
            var a = I.specialProperties[t.substring(1)];
            if (a)
                return a(e)
        }
        return "innerText" === t ? I.text(e) : t in e ? e[t] : e.getAttribute ? e.getAttribute(t) : n
    }
    ,
    I.propertiesMatch = function(e, t) {
        if (e)
            for (var a in e)
                if (e.hasOwnProperty(a)) {
                    var n = e[a]
                      , i = I.getElementProperty(t, a);
                    if ("string" == typeof n && n !== i)
                        return !1;
                    if (n instanceof RegExp && !n.test(i))
                        return !1
                }
        return !0
    }
    ,
    I.isRightClick = function(e) {
        var t;
        return e.which ? t = 3 == e.which : e.button && (t = 2 == e.button),
        t
    }
    ,
    I.ruleMatches = function(t, a, n, i) {
        var r = t.condition
          , o = t.conditions
          , s = t.property
          , c = a.type
          , l = t.value
          , u = a.target || a.srcElement
          , d = n === u;
        if (t.event !== c && ("custom" !== t.event || t.customEvent !== c))
            return !1;
        if (!I.ruleInScope(t))
            return !1;
        if ("click" === t.event && I.isRightClick(a))
            return !1;
        if (t.isDefault && i > 0)
            return !1;
        if (t.expired)
            return !1;
        if ("inview" === c && a.inviewDelay !== t.inviewDelay)
            return !1;
        if (!d && (!1 === t.bubbleFireIfParent || 0 !== i && !1 === t.bubbleFireIfChildFired))
            return !1;
        if (t.selector && !I.matchesCss(t.selector, n))
            return !1;
        if (!I.propertiesMatch(s, n))
            return !1;
        if (null != l)
            if ("string" == typeof l) {
                if (l !== n.value)
                    return !1
            } else if (!l.test(n.value))
                return !1;
        if (r)
            try {
                if (!r.call(n, a, u))
                    return I.notify('Condition for rule "' + t.name + '" not met.', 1),
                    !1
            } catch (e) {
                return I.notify('Condition for rule "' + t.name + '" not met. Error: ' + e.message, 1),
                !1
            }
        if (o) {
            var g = I.find(o, function(i) {
                try {
                    return !i.call(n, a, u)
                } catch (e) {
                    return I.notify('Condition for rule "' + t.name + '" not met. Error: ' + e.message, 1),
                    !0
                }
            });
            if (g)
                return I.notify("Condition " + g.toString() + ' for rule "' + t.name + '" not met.', 1),
                !1
        }
        return !0
    }
    ,
    I.evtHandlers = {},
    I.bindEvent = function(e, t) {
        var a = I.evtHandlers;
        a[e] || (a[e] = []),
        a[e].push(t)
    }
    ,
    I.whenEvent = I.bindEvent,
    I.unbindEvent = function(e, t) {
        var a = I.evtHandlers;
        if (a[e]) {
            var n = I.indexOf(a[e], t);
            a[e].splice(n, 1)
        }
    }
    ,
    I.bindEventOnce = function(e, t) {
        var a = function() {
            I.unbindEvent(e, a),
            t.apply(null, arguments)
        };
        I.bindEvent(e, a)
    }
    ,
    I.isVMLPoisoned = function(t) {
        if (!t)
            return !1;
        try {
            t.nodeName
        } catch (e) {
            if ("Attribute only valid on v:image" === e.message)
                return !0
        }
        return !1
    }
    ,
    I.handleEvent = function(e) {
        if (!I.$data(e, "eventProcessed")) {
            var t = e.type.toLowerCase()
              , a = e.target || e.srcElement
              , n = 0
              , i = I.rules
              , r = (I.tools,
            I.evtHandlers[e.type]);
            if (I.isVMLPoisoned(a))
                I.notify("detected " + t + " on poisoned VML element, skipping.", 1);
            else {
                r && I.each(r, function(t) {
                    t(e)
                }),
                a && a.nodeName ? I.notify("detected " + t + " on " + a.nodeName, 1) : I.notify("detected " + t, 1);
                for (var o = a; o; o = o.parentNode) {
                    var s = !1;
                    if (I.each(i, function(t) {
                        I.ruleMatches(t, e, o, n) && (I.notify('Rule "' + t.name + '" fired.', 1),
                        I.fireRule(t, o, e),
                        n++,
                        t.bubbleStop && (s = !0))
                    }),
                    s)
                        break
                }
                I.$data(e, "eventProcessed", !0)
            }
        }
    }
    ,
    I.onEvent = a.querySelectorAll ? function(e) {
        I.handleEvent(e)
    }
    : (T = [],
    (k = function(e) {
        e.selector ? T.push(e) : I.handleEvent(e)
    }
    ).pendingEvents = T,
    k),
    I.fireEvent = function(e, t) {
        I.onEvent({
            type: e,
            target: t
        })
    }
    ,
    I.registerEvents = function(e, t) {
        for (var a = t.length - 1; a >= 0; a--) {
            var n = t[a];
            I.$data(e, n + ".tracked") || (I.addEventHandler(e, n, I.onEvent),
            I.$data(e, n + ".tracked", !0))
        }
    }
    ,
    I.registerEventsForTags = function(e, t) {
        for (var n = e.length - 1; n >= 0; n--)
            for (var i = e[n], r = a.getElementsByTagName(i), o = r.length - 1; o >= 0; o--)
                I.registerEvents(r[o], t)
    }
    ,
    I.setListeners = function() {
        var e = ["click", "submit"];
        I.each(I.rules, function(t) {
            "custom" === t.event && t.hasOwnProperty("customEvent") && !I.contains(e, t.customEvent) && e.push(t.customEvent)
        }),
        I.registerEvents(a, e)
    }
    ,
    I.getUniqueRuleEvents = function() {
        return I._uniqueRuleEvents || (I._uniqueRuleEvents = [],
        I.each(I.rules, function(e) {
            -1 === I.indexOf(I._uniqueRuleEvents, e.event) && I._uniqueRuleEvents.push(e.event)
        })),
        I._uniqueRuleEvents
    }
    ,
    I.setFormListeners = function() {
        if (!I._relevantFormEvents) {
            var e = ["change", "focus", "blur", "keypress"];
            I._relevantFormEvents = I.filter(I.getUniqueRuleEvents(), function(t) {
                return -1 !== I.indexOf(e, t)
            })
        }
        I._relevantFormEvents.length && I.registerEventsForTags(["input", "select", "textarea", "button"], I._relevantFormEvents)
    }
    ,
    I.setVideoListeners = function() {
        if (!I._relevantVideoEvents) {
            var e = ["play", "pause", "ended", "volumechange", "stalled", "loadeddata"];
            I._relevantVideoEvents = I.filter(I.getUniqueRuleEvents(), function(t) {
                return -1 !== I.indexOf(e, t)
            })
        }
        I._relevantVideoEvents.length && I.registerEventsForTags(["video"], I._relevantVideoEvents)
    }
    ,
    I.readStoredSetting = function(a) {
        try {
            return a = "sdsat_" + a,
            t.localStorage.getItem(a)
        } catch (e) {
            return I.notify("Cannot read stored setting from localStorage: " + e.message, 2),
            null
        }
    }
    ,
    I.loadStoredSettings = function() {
        var e = I.readStoredSetting("debug")
          , t = I.readStoredSetting("hide_activity");
        e && (I.settings.notifications = "true" === e),
        t && (I.settings.hideActivity = "true" === t)
    }
    ,
    I.isRuleActive = function(e, t) {
        function a(e, t) {
            return t = i(t, {
                hour: e[p](),
                minute: e[f]()
            }),
            Math.floor(Math.abs((e.getTime() - t.getTime()) / 864e5))
        }
        function n(e, t) {
            function a(e) {
                return 12 * e[d]() + e[g]()
            }
            return Math.abs(a(e) - a(t))
        }
        function i(e, t) {
            var a = new Date(e.getTime());
            for (var n in t)
                if (t.hasOwnProperty(n)) {
                    var i = t[n];
                    switch (n) {
                    case "hour":
                        a[m](i);
                        break;
                    case "minute":
                        a[h](i);
                        break;
                    case "date":
                        a[v](i)
                    }
                }
            return a
        }
        function r(e, t) {
            return 60 * e[p]() + e[f]() > 60 * t[p]() + t[f]()
        }
        function o(e, t) {
            return 60 * e[p]() + e[f]() < 60 * t[p]() + t[f]()
        }
        var s = e.schedule;
        if (!s)
            return !0;
        var c = s.utc
          , l = c ? "getUTCDate" : "getDate"
          , u = c ? "getUTCDay" : "getDay"
          , d = c ? "getUTCFullYear" : "getFullYear"
          , g = c ? "getUTCMonth" : "getMonth"
          , p = c ? "getUTCHours" : "getHours"
          , f = c ? "getUTCMinutes" : "getMinutes"
          , m = c ? "setUTCHours" : "setHours"
          , h = c ? "setUTCMinutes" : "setMinutes"
          , v = c ? "setUTCDate" : "setDate";
        if (t = t || new Date,
        s.repeat) {
            if (r(s.start, t))
                return !1;
            if (o(s.end, t))
                return !1;
            if (t < s.start)
                return !1;
            if (s.endRepeat && t >= s.endRepeat)
                return !1;
            if ("daily" === s.repeat) {
                if (s.repeatEvery)
                    if (a(s.start, t) % s.repeatEvery != 0)
                        return !1
            } else if ("weekly" === s.repeat) {
                if (s.days) {
                    if (!I.contains(s.days, t[u]()))
                        return !1
                } else if (s.start[u]() !== t[u]())
                    return !1;
                if (s.repeatEvery)
                    if (a(s.start, t) % (7 * s.repeatEvery) != 0)
                        return !1
            } else if ("monthly" === s.repeat) {
                if (s.repeatEvery)
                    if (n(s.start, t) % s.repeatEvery != 0)
                        return !1;
                if (s.nthWeek && s.mthDay) {
                    if (s.mthDay !== t[u]())
                        return !1;
                    var y = Math.floor((t[l]() - t[u]() + 1) / 7);
                    if (s.nthWeek !== y)
                        return !1
                } else if (s.start[l]() !== t[l]())
                    return !1
            } else if ("yearly" === s.repeat) {
                if (s.start[g]() !== t[g]())
                    return !1;
                if (s.start[l]() !== t[l]())
                    return !1;
                if (s.repeatEvery)
                    if (Math.abs(s.start[d]() - t[d]()) % s.repeatEvery != 0)
                        return !1
            }
        } else {
            if (s.start > t)
                return !1;
            if (s.end < t)
                return !1
        }
        return !0
    }
    ,
    I.isOutboundLink = function(e) {
        if (!e.getAttribute("href"))
            return !1;
        var t = e.hostname
          , a = (e.href,
        e.protocol);
        return ("http:" === a || "https:" === a) && (!I.any(I.settings.domainList, function(e) {
            return I.isSubdomainOf(t, e)
        }) && t !== location.hostname)
    }
    ,
    I.isLinkerLink = function(e) {
        return !(!e.getAttribute || !e.getAttribute("href")) && (I.hasMultipleDomains() && e.hostname != location.hostname && !e.href.match(/^javascript/i) && !I.isOutboundLink(e))
    }
    ,
    I.isSubdomainOf = function(e, t) {
        if (e === t)
            return !0;
        var a = e.length - t.length;
        return a > 0 && I.equalsIgnoreCase(e.substring(a), t)
    }
    ,
    I.getVisitorId = function() {
        var e = I.getToolsByType("visitor_id");
        return 0 === e.length ? null : e[0].getInstance()
    }
    ,
    I.URI = function() {
        var e = a.location.pathname + a.location.search;
        return I.settings.forceLowerCase && (e = e.toLowerCase()),
        e
    }
    ,
    I.URL = function() {
        var e = a.location.href;
        return I.settings.forceLowerCase && (e = e.toLowerCase()),
        e
    }
    ,
    I.filterRules = function() {
        function e(e) {
            return !!I.isRuleActive(e)
        }
        I.rules = I.filter(I.rules, e),
        I.pageLoadRules = I.filter(I.pageLoadRules, e)
    }
    ,
    I.ruleInScope = function(e, t) {
        function n(e, t) {
            function a(e) {
                return t.match(e)
            }
            var n = e.include
              , r = e.exclude;
            if (n && i(n, t))
                return !0;
            if (r) {
                if (I.isString(r) && r === t)
                    return !0;
                if (I.isArray(r) && I.any(r, a))
                    return !0;
                if (I.isRegex(r) && a(r))
                    return !0
            }
            return !1
        }
        function i(e, t) {
            function a(e) {
                return t.match(e)
            }
            return !(!I.isString(e) || e === t) || (!(!I.isArray(e) || I.any(e, a)) || !(!I.isRegex(e) || a(e)))
        }
        t = t || a.location;
        var r = e.scope;
        if (!r)
            return !0;
        var o = r.URI
          , s = r.subdomains
          , c = r.domains
          , l = r.protocols
          , u = r.hashes;
        return (!o || !n(o, t.pathname + t.search)) && ((!s || !n(s, t.hostname)) && ((!c || !i(c, t.hostname)) && ((!l || !i(l, t.protocol)) && (!u || !n(u, t.hash)))))
    }
    ,
    I.backgroundTasks = function() {
        new Date;
        I.setFormListeners(),
        I.setVideoListeners(),
        I.loadStoredSettings(),
        I.registerNewElementsForDynamicRules(),
        I.eventEmitterBackgroundTasks();
        new Date
    }
    ,
    I.registerNewElementsForDynamicRules = function() {
        function e(t, a) {
            var n = e.cache[t];
            if (n)
                return a(n);
            I.cssQuery(t, function(n) {
                e.cache[t] = n,
                a(n)
            })
        }
        e.cache = {},
        I.each(I.dynamicRules, function(t) {
            e(t.selector, function(e) {
                I.each(e, function(e) {
                    var a = "custom" === t.event ? t.customEvent : t.event;
                    I.$data(e, "dynamicRules.seen." + a) || (I.$data(e, "dynamicRules.seen." + a, !0),
                    I.propertiesMatch(t.property, e) && I.registerEvents(e, [a]))
                })
            })
        })
    }
    ,
    I.ensureCSSSelector = function() {
        a.querySelectorAll ? I.hasSelector = !0 : (I.loadingSizzle = !0,
        I.sizzleQueue = [],
        I.loadScript(I.basePath() + "selector.js", function() {
            if (I.Sizzle) {
                var e = I.onEvent.pendingEvents;
                I.each(e, function(e) {
                    I.handleEvent(e)
                }, this),
                I.onEvent = I.handleEvent,
                I.hasSelector = !0,
                delete I.loadingSizzle,
                I.each(I.sizzleQueue, function(e) {
                    I.cssQuery(e[0], e[1])
                }),
                delete I.sizzleQueue
            } else
                I.logError(new Error("Failed to load selector.js"))
        }))
    }
    ,
    I.errors = [],
    I.logError = function(e) {
        I.errors.push(e),
        I.notify(e.name + " - " + e.message, 5)
    }
    ,
    I.pageBottom = function() {
        I.initialized && (I.pageBottomFired = !0,
        I.firePageLoadEvent("pagebottom"))
    }
    ,
    I.stagingLibraryOverride = function() {
        if ("true" === I.readStoredSetting("stagingLibrary")) {
            for (var e, t, n, i = a.getElementsByTagName("script"), r = /^(.*)satelliteLib-([a-f0-9]{40})\.js$/, o = /^(.*)satelliteLib-([a-f0-9]{40})-staging\.js$/, s = 0, c = i.length; s < c && (!(n = i[s].getAttribute("src")) || (e || (e = n.match(r)),
            t || (t = n.match(o)),
            !t)); s++)
                ;
            if (e && !t) {
                var l = e[1] + "satelliteLib-" + e[2] + "-staging.js";
                if (a.write)
                    a.write('<script src="' + l + '"></script>');
                else {
                    var u = a.createElement("script");
                    u.src = l,
                    a.head.appendChild(u)
                }
                return !0
            }
        }
        return !1
    }
    ,
    I.checkAsyncInclude = function() {
        t.satellite_asyncLoad && I.notify('You may be using the async installation of Satellite. In-page HTML and the "pagebottom" event will not work. Please update your Satellite installation for these features.', 5)
    }
    ,
    I.hasMultipleDomains = function() {
        return !!I.settings.domainList && I.settings.domainList.length > 1
    }
    ,
    I.handleOverrides = function() {
        if (D)
            for (var e in D)
                D.hasOwnProperty(e) && (I.data[e] = D[e])
    }
    ,
    I.privacyManagerParams = function() {
        var e = {};
        I.extend(e, I.settings.privacyManagement);
        var t = [];
        for (var a in I.tools)
            if (I.tools.hasOwnProperty(a)) {
                var n = I.tools[a]
                  , i = n.settings;
                if (!i)
                    continue;
                "sc" === i.engine && t.push(n)
            }
        var r = I.filter(I.map(t, function(e) {
            return e.getTrackingServer()
        }), function(e) {
            return null != e
        });
        e.adobeAnalyticsTrackingServers = r;
        for (var o = ["bannerText", "headline", "introductoryText", "customCSS"], s = 0; s < o.length; s++) {
            var c = o[s]
              , l = e[c];
            if (l)
                if ("text" === l.type)
                    e[c] = l.value;
                else {
                    if ("data" !== l.type)
                        throw new Error("Invalid type: " + l.type);
                    e[c] = I.getVar(l.value)
                }
        }
        return e
    }
    ,
    I.prepareLoadPrivacyManager = function() {
        function e(e) {
            function t() {
                ++r === i.length && (a(),
                clearTimeout(o),
                e())
            }
            function a() {
                I.each(i, function(e) {
                    I.unbindEvent(e.id + ".load", t)
                })
            }
            function n() {
                a(),
                e()
            }
            var i = I.filter(I.values(I.tools), function(e) {
                return e.settings && "sc" === e.settings.engine
            });
            if (0 === i.length)
                return e();
            var r = 0;
            I.each(i, function(e) {
                I.bindEvent(e.id + ".load", t)
            });
            var o = setTimeout(n, 5e3)
        }
        I.addEventHandler(t, "load", function() {
            e(I.loadPrivacyManager)
        })
    }
    ,
    I.loadPrivacyManager = function() {
        var e = I.basePath() + "privacy_manager.js";
        I.loadScript(e, function() {
            var e = I.privacyManager;
            e.configure(I.privacyManagerParams()),
            e.openIfRequired()
        })
    }
    ,
    I.init = function(e) {
        if (!I.stagingLibraryOverride()) {
            I.configurationSettings = e;
            var a = e.tools;
            for (var i in delete e.tools,
            e)
                e.hasOwnProperty(i) && (I[i] = e[i]);
            I.data.customVars === n && (I.data.customVars = {}),
            I.data.queryParams = I.QueryParams.normal,
            I.handleOverrides(),
            I.detectBrowserInfo(),
            I.trackVisitorInfo && I.trackVisitorInfo(),
            I.loadStoredSettings(),
            I.Logger.setOutputState(I.settings.notifications),
            I.checkAsyncInclude(),
            I.ensureCSSSelector(),
            I.filterRules(),
            I.dynamicRules = I.filter(I.rules, function(e) {
                return e.eventHandlerOnElement
            }),
            I.tools = I.initTools(a),
            I.initEventEmitters(),
            I.firePageLoadEvent("aftertoolinit"),
            I.settings.privacyManagement && I.prepareLoadPrivacyManager(),
            I.hasSelector && I.domReady(I.eventEmitterBackgroundTasks),
            I.setListeners(),
            I.domReady(function() {
                I.poll(function() {
                    I.backgroundTasks()
                }, I.settings.recheckEvery || 3e3)
            }),
            I.domReady(function() {
                I.domReadyFired = !0,
                I.pageBottomFired || I.pageBottom(),
                I.firePageLoadEvent("domready")
            }),
            I.addEventHandler(t, "load", function() {
                I.firePageLoadEvent("windowload")
            }),
            I.firePageLoadEvent("pagetop"),
            I.initialized = !0
        }
    }
    ,
    I.pageLoadPhases = ["aftertoolinit", "pagetop", "pagebottom", "domready", "windowload"],
    I.loadEventBefore = function(e, t) {
        return I.indexOf(I.pageLoadPhases, e) <= I.indexOf(I.pageLoadPhases, t)
    }
    ,
    I.flushPendingCalls = function(e) {
        e.pending && (I.each(e.pending, function(t) {
            var a = t[0]
              , n = t[1]
              , i = t[2]
              , r = t[3];
            a in e ? e[a].apply(e, [n, i].concat(r)) : e.emit ? e.emit(a, n, i, r) : I.notify("Failed to trigger " + a + " for tool " + e.id, 1)
        }),
        delete e.pending)
    }
    ,
    I.setDebug = function(a) {
        try {
            t.localStorage.setItem("sdsat_debug", a)
        } catch (e) {
            I.notify("Cannot set debug mode: " + e.message, 2)
        }
    }
    ,
    I.getUserAgent = function() {
        return navigator.userAgent
    }
    ,
    I.detectBrowserInfo = function() {
        function e(e) {
            return function(t) {
                for (var a in e) {
                    if (e.hasOwnProperty(a))
                        if (e[a].test(t))
                            return a
                }
                return "Unknown"
            }
        }
        var t = e({
            "IE Edge Mobile": /Windows Phone.*Edge/,
            "IE Edge": /Edge/,
            OmniWeb: /OmniWeb/,
            "Opera Mini": /Opera Mini/,
            "Opera Mobile": /Opera Mobi/,
            Opera: /Opera/,
            Chrome: /Chrome|CriOS|CrMo/,
            Firefox: /Firefox|FxiOS/,
            "IE Mobile": /IEMobile/,
            IE: /MSIE|Trident/,
            "Mobile Safari": /Mobile(\/[0-9A-z]+)? Safari/,
            Safari: /Safari/
        })
          , a = e({
            Blackberry: /BlackBerry|BB10/,
            "Symbian OS": /Symbian|SymbOS/,
            Maemo: /Maemo/,
            Android: /Android/,
            Linux: / Linux /,
            Unix: /FreeBSD|OpenBSD|CrOS/,
            Windows: /[\( ]Windows /,
            iOS: /iPhone|iPad|iPod/,
            MacOS: /Macintosh;/
        })
          , n = e({
            Nokia: /Symbian|SymbOS|Maemo/,
            "Windows Phone": /Windows Phone/,
            Blackberry: /BlackBerry|BB10/,
            Android: /Android/,
            iPad: /iPad/,
            iPod: /iPod/,
            iPhone: /iPhone/,
            Desktop: /.*/
        })
          , i = I.getUserAgent();
        I.browserInfo = {
            browser: t(i),
            os: a(i),
            deviceType: n(i)
        }
    }
    ,
    I.isHttps = function() {
        return "https:" == a.location.protocol
    }
    ,
    I.BaseTool = function(e) {
        this.settings = e || {},
        this.forceLowerCase = I.settings.forceLowerCase,
        "forceLowerCase"in this.settings && (this.forceLowerCase = this.settings.forceLowerCase)
    }
    ,
    I.BaseTool.prototype = {
        triggerCommand: function(e, t, a) {
            var n = this.settings || {};
            if (this.initialize && this.isQueueAvailable() && this.isQueueable(e) && a && I.loadEventBefore(a.type, n.loadOn))
                this.queueCommand(e, t, a);
            else {
                var i = e.command
                  , r = this["$" + i]
                  , o = !!r && r.escapeHtml
                  , s = I.preprocessArguments(e.arguments, t, a, this.forceLowerCase, o);
                r ? r.apply(this, [t, a].concat(s)) : this.$missing$ ? this.$missing$(i, t, a, s) : I.notify("Failed to trigger " + i + " for tool " + this.id, 1)
            }
        },
        endPLPhase: function() {},
        isQueueable: function(e) {
            return "cancelToolInit" !== e.command
        },
        isQueueAvailable: function() {
            return !this.initialized && !this.initializing
        },
        flushQueue: function() {
            this.pending && (I.each(this.pending, function(e) {
                this.triggerCommand.apply(this, e)
            }, this),
            this.pending = [])
        },
        queueCommand: function(e, t, a) {
            this.pending || (this.pending = []),
            this.pending.push([e, t, a])
        },
        $cancelToolInit: function() {
            this._cancelToolInit = !0
        }
    },
    t._satellite = I,
    I.ecommerce = {
        addItem: function() {
            var e = [].slice.call(arguments);
            I.onEvent({
                type: "ecommerce.additem",
                target: e
            })
        },
        addTrans: function() {
            var e = [].slice.call(arguments);
            I.data.saleData.sale = {
                orderId: e[0],
                revenue: e[2]
            },
            I.onEvent({
                type: "ecommerce.addtrans",
                target: e
            })
        },
        trackTrans: function() {
            I.onEvent({
                type: "ecommerce.tracktrans",
                target: []
            })
        }
    },
    I.visibility = {
        isHidden: function() {
            var e = this.getHiddenProperty();
            return !!e && a[e]
        },
        isVisible: function() {
            return !this.isHidden()
        },
        getHiddenProperty: function() {
            var e = ["webkit", "moz", "ms", "o"];
            if ("hidden"in a)
                return "hidden";
            for (var t = 0; t < e.length; t++)
                if (e[t] + "Hidden"in a)
                    return e[t] + "Hidden";
            return null
        },
        getVisibilityEvent: function() {
            var e = this.getHiddenProperty();
            return e ? e.replace(/[H|h]idden/, "") + "visibilitychange" : null
        }
    },
    i.prototype = {
        obue: !1,
        initialize: function() {
            this.attachCloseListeners()
        },
        obuePrevUnload: function() {},
        obuePrevBeforeUnload: function() {},
        newObueListener: function() {
            this.obue || (this.obue = !0,
            this.triggerBeacons())
        },
        attachCloseListeners: function() {
            this.prevUnload = t.onunload,
            this.prevBeforeUnload = t.onbeforeunload,
            t.onunload = I.bind(function(e) {
                this.prevUnload && setTimeout(I.bind(function() {
                    this.prevUnload.call(t, e)
                }, this), 1),
                this.newObueListener()
            }, this),
            t.onbeforeunload = I.bind(function(e) {
                this.prevBeforeUnload && setTimeout(I.bind(function() {
                    this.prevBeforeUnload.call(t, e)
                }, this), 1),
                this.newObueListener()
            }, this)
        },
        triggerBeacons: function() {
            I.fireEvent("leave", a)
        }
    },
    I.availableEventEmitters.push(i),
    r.prototype = {
        initialize: function() {
            var e = this.twttr;
            e && "function" == typeof e.ready && e.ready(I.bind(this.bind, this))
        },
        bind: function() {
            this.twttr.events.bind("tweet", function(e) {
                e && (I.notify("tracking a tweet button", 1),
                I.onEvent({
                    type: "twitter.tweet",
                    target: a
                }))
            })
        }
    },
    I.availableEventEmitters.push(r),
    o.prototype = {
        initialize: function() {
            this.setupHistoryAPI(),
            this.setupHashChange()
        },
        fireIfURIChanged: function() {
            var e = I.URL();
            this.lastURL !== e && (this.fireEvent(),
            this.lastURL = e)
        },
        fireEvent: function() {
            I.updateQueryParams(),
            I.onEvent({
                type: "locationchange",
                target: a
            })
        },
        setupSPASupport: function() {
            this.setupHistoryAPI(),
            this.setupHashChange()
        },
        setupHistoryAPI: function() {
            var e = t.history;
            e && (e.pushState && (this.originalPushState = e.pushState,
            e.pushState = this._pushState),
            e.replaceState && (this.originalReplaceState = e.replaceState,
            e.replaceState = this._replaceState)),
            I.addEventHandler(t, "popstate", this._onPopState)
        },
        pushState: function() {
            var e = this.originalPushState.apply(history, arguments);
            return this.onPushState(),
            e
        },
        replaceState: function() {
            var e = this.originalReplaceState.apply(history, arguments);
            return this.onReplaceState(),
            e
        },
        setupHashChange: function() {
            I.addEventHandler(t, "hashchange", this._onHashChange)
        },
        onReplaceState: function() {
            setTimeout(this._fireIfURIChanged, 0)
        },
        onPushState: function() {
            setTimeout(this._fireIfURIChanged, 0)
        },
        onPopState: function() {
            setTimeout(this._fireIfURIChanged, 0)
        },
        onHashChange: function() {
            setTimeout(this._fireIfURIChanged, 0)
        },
        uninitialize: function() {
            this.cleanUpHistoryAPI(),
            this.cleanUpHashChange()
        },
        cleanUpHistoryAPI: function() {
            history.pushState === this._pushState && (history.pushState = this.originalPushState),
            history.replaceState === this._replaceState && (history.replaceState = this.originalReplaceState),
            I.removeEventHandler(t, "popstate", this._onPopState)
        },
        cleanUpHashChange: function() {
            I.removeEventHandler(t, "hashchange", this._onHashChange)
        }
    },
    I.availableEventEmitters.push(o),
    c.prototype.getStringifiedValue = t.JSON && t.JSON.stringify || I.stringify,
    c.prototype.initPolling = function() {
        0 !== this.dataElementsNames.length && (this.dataElementsStore = this.getDataElementsValues(),
        I.poll(I.bind(this.checkDataElementValues, this), 1e3))
    }
    ,
    c.prototype.getDataElementsValues = function() {
        var e = {};
        return I.each(this.dataElementsNames, function(t) {
            var a = I.getVar(t);
            e[t] = this.getStringifiedValue(a)
        }, this),
        e
    }
    ,
    c.prototype.checkDataElementValues = function() {
        I.each(this.dataElementsNames, I.bind(function(e) {
            var t = this.getStringifiedValue(I.getVar(e));
            t !== this.dataElementsStore[e] && (this.dataElementsStore[e] = t,
            I.onEvent({
                type: "dataelementchange(" + e + ")",
                target: a
            }))
        }, this))
    }
    ,
    I.availableEventEmitters.push(c),
    l.orientationChange = function(e) {
        var a = 0 === t.orientation ? "portrait" : "landscape";
        e.orientation = a,
        I.onEvent(e)
    }
    ,
    I.availableEventEmitters.push(l),
    u.prototype = {
        backgroundTasks: function() {
            var e = this.eventHandler;
            I.each(this.rules, function(t) {
                I.cssQuery(t.selector || "video", function(t) {
                    I.each(t, function(t) {
                        I.$data(t, "videoplayed.tracked") || (I.addEventHandler(t, "timeupdate", I.throttle(e, 100)),
                        I.$data(t, "videoplayed.tracked", !0))
                    })
                })
            })
        },
        evalRule: function(e, t) {
            var a = t.event
              , n = e.seekable
              , i = n.start(0)
              , r = n.end(0)
              , o = e.currentTime
              , s = t.event.match(/^videoplayed\(([0-9]+)([s%])\)$/);
            if (s) {
                var c = s[2]
                  , l = Number(s[1])
                  , u = "%" === c ? function() {
                    return l <= 100 * (o - i) / (r - i)
                }
                : function() {
                    return l <= o - i
                }
                ;
                !I.$data(e, a) && u() && (I.$data(e, a, !0),
                I.onEvent({
                    type: a,
                    target: e
                }))
            }
        },
        onUpdateTime: function(e) {
            var t = this.rules
              , a = e.target;
            if (a.seekable && 0 !== a.seekable.length)
                for (var n = 0, i = t.length; n < i; n++)
                    this.evalRule(a, t[n])
        }
    },
    I.availableEventEmitters.push(u),
    d.prototype = {
        defineEvents: function() {
            this.oldBlurClosure = function() {
                I.fireEvent("tabblur", a)
            }
            ,
            this.oldFocusClosure = I.bind(function() {
                this.visibilityApiHasPriority ? I.fireEvent("tabfocus", a) : null != I.visibility.getHiddenProperty() && I.visibility.isHidden() || I.fireEvent("tabfocus", a)
            }, this)
        },
        attachDetachModernEventListeners: function(e) {
            I[0 == e ? "removeEventHandler" : "addEventHandler"](a, I.visibility.getVisibilityEvent(), this.handleVisibilityChange)
        },
        attachDetachOlderEventListeners: function(e, a, n) {
            var i = 0 == e ? "removeEventHandler" : "addEventHandler";
            I[i](a, n, this.oldBlurClosure),
            I[i](t, "focus", this.oldFocusClosure)
        },
        handleVisibilityChange: function() {
            I.visibility.isHidden() ? I.fireEvent("tabblur", a) : I.fireEvent("tabfocus", a)
        },
        setVisibilityApiPriority: function(e) {
            this.visibilityApiHasPriority = e,
            this.attachDetachOlderEventListeners(!1, t, "blur"),
            this.attachDetachModernEventListeners(!1),
            e ? null != I.visibility.getHiddenProperty() ? this.attachDetachModernEventListeners(!0) : this.attachDetachOlderEventListeners(!0, t, "blur") : (this.attachDetachOlderEventListeners(!0, t, "blur"),
            null != I.visibility.getHiddenProperty() && this.attachDetachModernEventListeners(!0))
        },
        oldBlurClosure: null,
        oldFocusClosure: null,
        visibilityApiHasPriority: !0
    },
    I.availableEventEmitters.push(d),
    g.offset = function(n) {
        var i = null
          , r = null;
        try {
            var o = n.getBoundingClientRect()
              , s = a
              , c = s.documentElement
              , l = s.body
              , u = t
              , d = c.clientTop || l.clientTop || 0
              , g = c.clientLeft || l.clientLeft || 0
              , p = u.pageYOffset || c.scrollTop || l.scrollTop
              , f = u.pageXOffset || c.scrollLeft || l.scrollLeft;
            i = o.top + p - d,
            r = o.left + f - g
        } catch (e) {}
        return {
            top: i,
            left: r
        }
    }
    ,
    g.getViewportHeight = function() {
        var e = t.innerHeight
          , n = a.compatMode;
        return n && (e = "CSS1Compat" == n ? a.documentElement.clientHeight : a.body.clientHeight),
        e
    }
    ,
    g.getScrollTop = function() {
        return a.documentElement.scrollTop ? a.documentElement.scrollTop : a.body.scrollTop
    }
    ,
    g.isElementInDocument = function(e) {
        return a.body.contains(e)
    }
    ,
    g.isElementInView = function(e) {
        if (!g.isElementInDocument(e))
            return !1;
        var t = g.getViewportHeight()
          , a = g.getScrollTop()
          , n = g.offset(e).top
          , i = e.offsetHeight;
        return null !== n && !(a > n + i || a + t < n)
    }
    ,
    g.prototype = {
        backgroundTasks: function() {
            var e = this.elements;
            I.each(this.rules, function(t) {
                I.cssQuery(t.selector, function(a) {
                    var n = 0;
                    I.each(a, function(t) {
                        I.contains(e, t) || (e.push(t),
                        n++)
                    }),
                    n && I.notify(t.selector + " added " + n + " elements.", 1)
                })
            }),
            this.track()
        },
        checkInView: function(e, t, a) {
            var n = I.$data(e, "inview");
            if (g.isElementInView(e)) {
                n || I.$data(e, "inview", !0);
                var i = this;
                this.processRules(e, function(a, n, r) {
                    if (t || !a.inviewDelay)
                        I.$data(e, n, !0),
                        I.onEvent({
                            type: "inview",
                            target: e,
                            inviewDelay: a.inviewDelay
                        });
                    else if (a.inviewDelay) {
                        var o = I.$data(e, r);
                        o || (o = setTimeout(function() {
                            i.checkInView(e, !0, a.inviewDelay)
                        }, a.inviewDelay),
                        I.$data(e, r, o))
                    }
                }, a)
            } else {
                if (!g.isElementInDocument(e)) {
                    var r = I.indexOf(this.elements, e);
                    this.elements.splice(r, 1)
                }
                n && I.$data(e, "inview", !1),
                this.processRules(e, function(t, a, n) {
                    var i = I.$data(e, n);
                    i && clearTimeout(i)
                }, a)
            }
        },
        track: function() {
            for (var e = this.elements.length - 1; e >= 0; e--)
                this.checkInView(this.elements[e])
        },
        processRules: function(e, t, a) {
            var n = this.rules;
            a && (n = I.filter(this.rules, function(e) {
                return e.inviewDelay == a
            })),
            I.each(n, function(a, n) {
                var i = a.inviewDelay ? "viewed_" + a.inviewDelay : "viewed"
                  , r = "inview_timeout_id_" + n;
                I.$data(e, i) || I.matchesCss(a.selector, e) && t(a, i, r)
            })
        }
    },
    I.availableEventEmitters.push(g),
    p.prototype.backgroundTasks = function() {
        I.each(this.rules, function(e) {
            I.cssQuery(e.selector, function(e) {
                if (e.length > 0) {
                    var t = e[0];
                    if (I.$data(t, "elementexists.seen"))
                        return;
                    I.$data(t, "elementexists.seen", !0),
                    I.onEvent({
                        type: "elementexists",
                        target: t
                    })
                }
            })
        })
    }
    ,
    I.availableEventEmitters.push(p),
    f.prototype = {
        initialize: function() {
            if (this.FB = this.FB || t.FB,
            this.FB && this.FB.Event && this.FB.Event.subscribe)
                return this.bind(),
                !0
        },
        bind: function() {
            this.FB.Event.subscribe("edge.create", function() {
                I.notify("tracking a facebook like", 1),
                I.onEvent({
                    type: "facebook.like",
                    target: a
                })
            }),
            this.FB.Event.subscribe("edge.remove", function() {
                I.notify("tracking a facebook unlike", 1),
                I.onEvent({
                    type: "facebook.unlike",
                    target: a
                })
            }),
            this.FB.Event.subscribe("message.send", function() {
                I.notify("tracking a facebook share", 1),
                I.onEvent({
                    type: "facebook.send",
                    target: a
                })
            })
        }
    },
    I.availableEventEmitters.push(f),
    m.prototype = {
        backgroundTasks: function() {
            var e = this;
            I.each(this.rules, function(t) {
                var a = t[1]
                  , n = t[0];
                I.cssQuery(a, function(t) {
                    I.each(t, function(t) {
                        e.trackElement(t, n)
                    })
                })
            }, this)
        },
        trackElement: function(e, t) {
            var a = this
              , n = I.$data(e, "hover.delays");
            n ? I.contains(n, t) || n.push(t) : (I.addEventHandler(e, "mouseover", function(t) {
                a.onMouseOver(t, e)
            }),
            I.addEventHandler(e, "mouseout", function(t) {
                a.onMouseOut(t, e)
            }),
            I.$data(e, "hover.delays", [t]))
        },
        onMouseOver: function(e, t) {
            var a = e.target || e.srcElement
              , n = e.relatedTarget || e.fromElement;
            (t === a || I.containsElement(t, a)) && !I.containsElement(t, n) && this.onMouseEnter(t)
        },
        onMouseEnter: function(e) {
            var t = I.$data(e, "hover.delays")
              , a = I.map(t, function(t) {
                return setTimeout(function() {
                    I.onEvent({
                        type: "hover(" + t + ")",
                        target: e
                    })
                }, t)
            });
            I.$data(e, "hover.delayTimers", a)
        },
        onMouseOut: function(e, t) {
            var a = e.target || e.srcElement
              , n = e.relatedTarget || e.toElement;
            (t === a || I.containsElement(t, a)) && !I.containsElement(t, n) && this.onMouseLeave(t)
        },
        onMouseLeave: function(e) {
            var t = I.$data(e, "hover.delayTimers");
            t && I.each(t, function(e) {
                clearTimeout(e)
            })
        }
    },
    I.availableEventEmitters.push(m),
    I.inherit(h, I.BaseTool),
    I.extend(h.prototype, {
        name: "Nielsen",
        endPLPhase: function(e) {
            switch (e) {
            case "pagetop":
                this.initialize();
                break;
            case "pagebottom":
                this.enableTracking && (this.queueCommand({
                    command: "sendFirstBeacon",
                    arguments: []
                }),
                this.flushQueueWhenReady())
            }
        },
        defineListeners: function() {
            this.onTabFocus = I.bind(function() {
                this.notify("Tab visible, sending view beacon when ready", 1),
                this.tabEverVisible = !0,
                this.flushQueueWhenReady()
            }, this),
            this.onPageLeave = I.bind(function() {
                this.notify("isHuman? : " + this.isHuman(), 1),
                this.isHuman() && this.sendDurationBeacon()
            }, this),
            this.onHumanDetectionChange = I.bind(function(e) {
                this == e.target.target && (this.human = e.target.isHuman)
            }, this)
        },
        initialize: function() {
            this.initializeTracking(),
            this.initializeDataProviders(),
            this.initializeNonHumanDetection(),
            this.tabEverVisible = I.visibility.isVisible(),
            this.tabEverVisible ? this.notify("Tab visible, sending view beacon when ready", 1) : I.bindEventOnce("tabfocus", this.onTabFocus),
            this.initialized = !0
        },
        initializeTracking: function() {
            this.initialized || (this.notify("Initializing tracking", 1),
            this.addRemovePageLeaveEvent(this.enableTracking),
            this.addRemoveHumanDetectionChangeEvent(this.enableTracking),
            this.initialized = !0)
        },
        initializeDataProviders: function() {
            var e, t = this.getAnalyticsTool();
            this.dataProvider.register(new h.DataProvider.VisitorID(I.getVisitorId())),
            t ? (e = new h.DataProvider.Generic("rsid",function() {
                return t.settings.account
            }
            ),
            this.dataProvider.register(e)) : this.notify("Missing integration with Analytics: rsid will not be sent.")
        },
        initializeNonHumanDetection: function() {
            I.nonhumandetection ? (I.nonhumandetection.init(),
            this.setEnableNonHumanDetection(0 != this.settings.enableNonHumanDetection),
            this.settings.nonHumanDetectionDelay > 0 && this.setNonHumanDetectionDelay(1e3 * parseInt(this.settings.nonHumanDetectionDelay))) : this.notify("NHDM is not available.")
        },
        getAnalyticsTool: function() {
            if (this.settings.integratesWith)
                return I.tools[this.settings.integratesWith]
        },
        flushQueueWhenReady: function() {
            this.enableTracking && this.tabEverVisible && I.poll(I.bind(function() {
                if (this.isReadyToTrack())
                    return this.flushQueue(),
                    !0
            }, this), 100, 20)
        },
        isReadyToTrack: function() {
            return this.tabEverVisible && this.dataProvider.isReady()
        },
        $setVars: function(e, t, a) {
            for (var n in a) {
                var i = a[n];
                "function" == typeof i && (i = i()),
                this.settings[n] = i
            }
            this.notify("Set variables done", 2),
            this.prepareContextData()
        },
        $setEnableTracking: function(e, t, a) {
            this.notify("Will" + (a ? "" : " not") + " track time on page", 1),
            this.enableTracking != a && (this.addRemovePageLeaveEvent(a),
            this.addRemoveHumanDetectionChangeEvent(a),
            this.enableTracking = a)
        },
        $sendFirstBeacon: function() {
            this.sendViewBeacon()
        },
        setEnableNonHumanDetection: function(e) {
            e ? I.nonhumandetection.register(this) : I.nonhumandetection.unregister(this)
        },
        setNonHumanDetectionDelay: function(e) {
            I.nonhumandetection.register(this, e)
        },
        addRemovePageLeaveEvent: function(e) {
            this.notify((e ? "Attach onto" : "Detach from") + " page leave event", 1),
            I[0 == e ? "unbindEvent" : "bindEvent"]("leave", this.onPageLeave)
        },
        addRemoveHumanDetectionChangeEvent: function(e) {
            this.notify((e ? "Attach onto" : "Detach from") + " human detection change event", 1),
            I[0 == e ? "unbindEvent" : "bindEvent"]("humandetection.change", this.onHumanDetectionChange)
        },
        sendViewBeacon: function() {
            this.notify("Tracked page view.", 1),
            this.sendBeaconWith()
        },
        sendDurationBeacon: function() {
            if (I.timetracking && "function" == typeof I.timetracking.timeOnPage && null != I.timetracking.timeOnPage()) {
                this.notify("Tracked close", 1),
                this.sendBeaconWith({
                    timeOnPage: Math.round(I.timetracking.timeOnPage() / 1e3),
                    duration: "D",
                    timer: "timer"
                });
                var e;
                for (e = 0; e < this.magicConst; e++)
                    "0"
            } else
                this.notify("Could not track close due missing time on page", 5)
        },
        sendBeaconWith: function(e) {
            this.enableTracking && this[this.beaconMethod].call(this, this.prepareUrl(e))
        },
        plainBeacon: function(e) {
            var t = new Image;
            t.src = e,
            t.width = 1,
            t.height = 1,
            t.alt = ""
        },
        navigatorSendBeacon: function(e) {
            navigator.sendBeacon(e)
        },
        prepareUrl: function(e) {
            var t = this.settings;
            return I.extend(t, this.dataProvider.provide()),
            I.extend(t, e),
            this.preparePrefix(this.settings.collectionServer) + this.adapt.convertToURI(this.adapt.toNielsen(this.substituteVariables(t)))
        },
        preparePrefix: function(e) {
            return "//" + encodeURIComponent(e) + ".imrworldwide.com/cgi-bin/gn?"
        },
        substituteVariables: function(e) {
            var t = {};
            for (var a in e)
                e.hasOwnProperty(a) && (t[a] = I.replace(e[a]));
            return t
        },
        prepareContextData: function() {
            if (this.getAnalyticsTool()) {
                var e = this.settings;
                e.sdkVersion = _satellite.publishDate,
                this.getAnalyticsTool().$setVars(null, null, {
                    contextData: this.adapt.toAnalytics(this.substituteVariables(e))
                })
            } else
                this.notify("Adobe Analytics missing.")
        },
        isHuman: function() {
            return this.human
        },
        onTabFocus: function() {},
        onPageLeave: function() {},
        onHumanDetectionChange: function() {},
        notify: function(e, t) {
            I.notify(this.logPrefix + e, t)
        },
        beaconMethod: "plainBeacon",
        adapt: null,
        enableTracking: !1,
        logPrefix: "Nielsen: ",
        tabEverVisible: !1,
        human: !0,
        magicConst: 2e6
    }),
    h.DataProvider = {},
    h.DataProvider.Generic = function(e, t) {
        this.key = e,
        this.valueFn = t
    }
    ,
    I.extend(h.DataProvider.Generic.prototype, {
        isReady: function() {
            return !0
        },
        getValue: function() {
            return this.valueFn()
        },
        provide: function() {
            this.isReady() || h.prototype.notify("Not yet ready to provide value for: " + this.key, 5);
            var e = {};
            return e[this.key] = this.getValue(),
            e
        }
    }),
    h.DataProvider.VisitorID = function(e, t, a) {
        this.key = t || "uuid",
        this.visitorInstance = e,
        this.visitorInstance && (this.visitorId = e.getMarketingCloudVisitorID([this, this._visitorIdCallback])),
        this.fallbackProvider = a || new h.UUID
    }
    ,
    I.inherit(h.DataProvider.VisitorID, h.DataProvider.Generic),
    I.extend(h.DataProvider.VisitorID.prototype, {
        isReady: function() {
            return null === this.visitorInstance || !!this.visitorId
        },
        getValue: function() {
            return this.visitorId || this.fallbackProvider.get()
        },
        _visitorIdCallback: function(e) {
            this.visitorId = e
        }
    }),
    h.DataProvider.Aggregate = function() {
        this.providers = [];
        for (var e = 0; e < arguments.length; e++)
            this.register(arguments[e])
    }
    ,
    I.extend(h.DataProvider.Aggregate.prototype, {
        register: function(e) {
            this.providers.push(e)
        },
        isReady: function() {
            return I.every(this.providers, function(e) {
                return e.isReady()
            })
        },
        provide: function() {
            var e = {};
            return I.each(this.providers, function(t) {
                I.extend(e, t.provide())
            }),
            e
        }
    }),
    h.UUID = function() {}
    ,
    I.extend(h.UUID.prototype, {
        generate: function() {
            return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(e) {
                var t = 16 * Math.random() | 0;
                return ("x" == e ? t : 3 & t | 8).toString(16)
            })
        },
        get: function() {
            var e = I.readCookie(this.key("uuid"));
            return e || (e = this.generate(),
            I.setCookie(this.key("uuid"), e),
            e)
        },
        key: function(e) {
            return "_dtm_nielsen_" + e
        }
    }),
    h.DataAdapters = function() {}
    ,
    I.extend(h.DataAdapters.prototype, {
        toNielsen: function(e) {
            var t = (new Date).getTime()
              , a = {
                c6: "vc,",
                c13: "asid,",
                c15: "apn,",
                c27: "cln,",
                c32: "segA,",
                c33: "segB,",
                c34: "segC,",
                c35: "adrsid,",
                c29: "plid,",
                c30: "bldv,",
                c40: "adbid,"
            }
              , i = {
                ci: e.clientId,
                c6: e.vcid,
                c13: e.appId,
                c15: e.appName,
                prv: 1,
                forward: 0,
                ad: 0,
                cr: e.duration || "V",
                rt: "text",
                st: "dcr",
                prd: "dcr",
                r: t,
                at: e.timer || "view",
                c16: e.sdkVersion,
                c27: e.timeOnPage || 0,
                c40: e.uuid,
                c35: e.rsid,
                ti: t,
                sup: 0,
                c32: e.segmentA,
                c33: e.segmentB,
                c34: e.segmentC,
                asn: e.assetName,
                c29: e.playerID,
                c30: e.buildVersion
            };
            for (key in i)
                if (i[key] !== n && null != i[key] && i[key] !== n && null != i && "" != i) {
                    var r = encodeURIComponent(i[key]);
                    a.hasOwnProperty(key) && r && (r = a[key] + r),
                    i[key] = r
                }
            return this.filterObject(i)
        },
        toAnalytics: function(e) {
            return this.filterObject({
                "a.nielsen.clientid": e.clientId,
                "a.nielsen.vcid": e.vcid,
                "a.nielsen.appid": e.appId,
                "a.nielsen.appname": e.appName,
                "a.nielsen.accmethod": "0",
                "a.nielsen.ctype": "text",
                "a.nielsen.sega": e.segmentA,
                "a.nielsen.segb": e.segmentB,
                "a.nielsen.segc": e.segmentC,
                "a.nielsen.asset": e.assetName
            })
        },
        convertToURI: function(e) {
            if (!1 === I.isObject(e))
                return "";
            var t = [];
            for (var a in e)
                e.hasOwnProperty(a) && t.push(a + "=" + e[a]);
            return t.join("&")
        },
        filterObject: function(e) {
            for (var t in e)
                !e.hasOwnProperty(t) || null != e[t] && e[t] !== n || delete e[t];
            return e
        }
    }),
    I.availableTools.nielsen = h,
    I.inherit(v, I.BaseTool),
    I.extend(v.prototype, {
        name: "tnt",
        endPLPhase: function(e) {
            "aftertoolinit" === e && this.initialize()
        },
        initialize: function() {
            I.notify("Test & Target: Initializing", 1),
            this.initializeTargetPageParams(),
            this.load()
        },
        initializeTargetPageParams: function() {
            t.targetPageParams && this.updateTargetPageParams(this.parseTargetPageParamsResult(t.targetPageParams())),
            this.updateTargetPageParams(this.settings.pageParams),
            this.setTargetPageParamsFunction()
        },
        load: function() {
            var e = this.getMboxURL(this.settings.mboxURL);
            !1 !== this.settings.initTool ? this.settings.loadSync ? (I.loadScriptSync(e),
            this.onScriptLoaded()) : (I.loadScript(e, I.bind(this.onScriptLoaded, this)),
            this.initializing = !0) : this.initialized = !0
        },
        getMboxURL: function(e) {
            var a = e;
            return I.isObject(e) && (a = "https:" === t.location.protocol ? e.https : e.http),
            a.match(/^https?:/) ? a : I.basePath() + a
        },
        onScriptLoaded: function() {
            I.notify("Test & Target: loaded.", 1),
            this.flushQueue(),
            this.initialized = !0,
            this.initializing = !1
        },
        $addMbox: function(e, t, a) {
            var n = a.mboxGoesAround
              , i = n + "{visibility: hidden;}"
              , r = this.appendStyle(i);
            n in this.styleElements || (this.styleElements[n] = r),
            this.initialized ? this.$addMBoxStep2(null, null, a) : this.initializing && this.queueCommand({
                command: "addMBoxStep2",
                arguments: [a]
            }, e, t)
        },
        $addMBoxStep2: function(e, n, i) {
            var r = this.generateID()
              , o = this;
            I.addEventHandler(t, "load", I.bind(function() {
                I.cssQuery(i.mboxGoesAround, function(e) {
                    var n = e[0];
                    if (n) {
                        var s = a.createElement("div");
                        s.id = r,
                        n.parentNode.replaceChild(s, n),
                        s.appendChild(n),
                        t.mboxDefine(r, i.mboxName);
                        var c = [i.mboxName];
                        i.arguments && (c = c.concat(i.arguments)),
                        t.mboxUpdate.apply(null, c),
                        o.reappearWhenCallComesBack(n, r, i.timeout, i)
                    }
                })
            }, this)),
            this.lastMboxID = r
        },
        $addTargetPageParams: function(e, t, a) {
            this.updateTargetPageParams(a)
        },
        generateID: function() {
            return "_sdsat_mbox_" + String(Math.random()).substring(2) + "_"
        },
        appendStyle: function(e) {
            var t = a.getElementsByTagName("head")[0]
              , n = a.createElement("style");
            return n.type = "text/css",
            n.styleSheet ? n.styleSheet.cssText = e : n.appendChild(a.createTextNode(e)),
            t.appendChild(n),
            n
        },
        reappearWhenCallComesBack: function(e, t, a, n) {
            function i() {
                var e = r.styleElements[n.mboxGoesAround];
                e && (e.parentNode.removeChild(e),
                delete r.styleElements[n.mboxGoesAround])
            }
            var r = this;
            I.cssQuery('script[src*="omtrdc.net"]', function(e) {
                var t = e[0];
                if (t) {
                    I.scriptOnLoad(t.src, t, function() {
                        I.notify("Test & Target: request complete", 1),
                        i(),
                        clearTimeout(n)
                    });
                    var n = setTimeout(function() {
                        I.notify("Test & Target: bailing after " + a + "ms", 1),
                        i()
                    }, a)
                } else
                    I.notify("Test & Target: failed to find T&T ajax call, bailing", 1),
                    i()
            })
        },
        updateTargetPageParams: function(e) {
            var t = {};
            for (var a in e)
                e.hasOwnProperty(a) && (t[I.replace(a)] = I.replace(e[a]));
            I.extend(this.targetPageParamsStore, t)
        },
        getTargetPageParams: function() {
            return this.targetPageParamsStore
        },
        setTargetPageParamsFunction: function() {
            t.targetPageParams = I.bind(this.getTargetPageParams, this)
        },
        parseTargetPageParamsResult: function(e) {
            var t = e;
            return I.isArray(e) && (e = e.join("&")),
            I.isString(e) && (t = I.parseQueryParams(e)),
            t
        }
    }),
    I.availableTools.tnt = v,
    I.inherit(y, I.BaseTool),
    I.extend(y.prototype, {
        name: "Default",
        $loadIframe: function(e, a, n) {
            var i = n.pages
              , r = n.loadOn
              , o = I.bind(function() {
                I.each(i, function(t) {
                    this.loadIframe(e, a, t)
                }, this)
            }, this);
            r || o(),
            "domready" === r && I.domReady(o),
            "load" === r && I.addEventHandler(t, "load", o)
        },
        loadIframe: function(e, t, n) {
            var i = a.createElement("iframe");
            i.style.display = "none";
            var r = I.data.host
              , o = n.data
              , s = this.scriptURL(n.src)
              , c = I.searchVariables(o, e, t);
            r && (s = I.basePath() + s),
            s += c,
            i.src = s;
            var l = a.getElementsByTagName("body")[0];
            l ? l.appendChild(i) : I.domReady(function() {
                a.getElementsByTagName("body")[0].appendChild(i)
            })
        },
        scriptURL: function(e) {
            return (I.settings.scriptDir || "") + e
        },
        $loadScript: function(e, a, n) {
            var i = n.scripts
              , r = n.sequential
              , o = n.loadOn
              , s = I.bind(function() {
                r ? this.loadScripts(e, a, i) : I.each(i, function(t) {
                    this.loadScripts(e, a, [t])
                }, this)
            }, this);
            o ? "domready" === o ? I.domReady(s) : "load" === o && I.addEventHandler(t, "load", s) : s()
        },
        loadScripts: function(t, a, n) {
            function i() {
                o.length > 0 && r && o.shift().call(t, a, s);
                var e = n.shift();
                if (e) {
                    var l = I.data.host
                      , u = c.scriptURL(e.src);
                    l && (u = I.basePath() + u),
                    r = e,
                    I.loadScript(u, i)
                }
            }
            try {
                n = n.slice(0);
                var r, o = this.asyncScriptCallbackQueue, s = a.target || a.srcElement, c = this
            } catch (e) {
                console.error("scripts is", I.stringify(n))
            }
            i()
        },
        $loadBlockingScript: function(e, t, a) {
            var n = a.scripts;
            a.loadOn;
            I.bind(function() {
                I.each(n, function(a) {
                    this.loadBlockingScript(e, t, a)
                }, this)
            }, this)()
        },
        loadBlockingScript: function(e, t, a) {
            var n = this.scriptURL(a.src)
              , i = I.data.host
              , r = t.target || t.srcElement;
            i && (n = I.basePath() + n),
            this.argsForBlockingScripts.push([e, t, r]),
            I.loadScriptSync(n)
        },
        pushAsyncScript: function(e) {
            this.asyncScriptCallbackQueue.push(e)
        },
        pushBlockingScript: function(e) {
            var t = this.argsForBlockingScripts.shift()
              , a = t[0];
            e.apply(a, t.slice(1))
        },
        $writeHTML: I.escapeHtmlParams(function(e, t) {
            if (!I.domReadyFired && a.write)
                if ("pagebottom" === t.type || "pagetop" === t.type)
                    for (var n = 2, i = arguments.length; n < i; n++) {
                        var r = arguments[n].html;
                        r = I.replace(r, e, t),
                        a.write(r)
                    }
                else
                    I.notify("You can only use writeHTML on the `pagetop` and `pagebottom` events.", 1);
            else
                I.notify("Command writeHTML failed. You should try appending HTML using the async option.", 1)
        }),
        linkNeedsDelayActivate: function(e, a) {
            a = a || t;
            var n = e.tagName
              , i = e.getAttribute("target")
              , r = e.getAttribute("href");
            return (!n || "a" === n.toLowerCase()) && (!!r && (!i || "_blank" !== i && ("_top" === i ? a.top === a : "_parent" !== i && ("_self" === i || (!a.name || i === a.name)))))
        },
        $delayActivateLink: function(e, t) {
            if (this.linkNeedsDelayActivate(e)) {
                I.preventDefault(t);
                var a = I.settings.linkDelay || 100;
                setTimeout(function() {
                    I.setLocation(e.href)
                }, a)
            }
        },
        isQueueable: function(e) {
            return "writeHTML" !== e.command
        }
    }),
    I.availableTools["default"] = y,
    I.inherit(b, I.BaseTool),
    I.extend(b.prototype, {
        name: "SC",
        endPLPhase: function(e) {
            e === this.settings.loadOn && this.initialize(e)
        },
        initialize: function(e) {
            if (!this._cancelToolInit)
                if (this.settings.initVars = this.substituteVariables(this.settings.initVars, {
                    type: e
                }),
                !1 !== this.settings.initTool) {
                    var a = this.settings.sCodeURL || I.basePath() + "s_code.js";
                    "object" == typeof a && (a = "https:" === t.location.protocol ? a.https : a.http),
                    a.match(/^https?:/) || (a = I.basePath() + a),
                    this.settings.initVars && this.$setVars(null, null, this.settings.initVars),
                    I.loadScript(a, I.bind(this.onSCodeLoaded, this)),
                    this.initializing = !0
                } else
                    this.initializing = !0,
                    this.pollForSC()
        },
        getS: function(e, a) {
            var n = a && a.hostname || t.location.hostname
              , i = this.concatWithToolVarBindings(a && a.setVars || this.varBindings)
              , r = a && a.addEvent || this.events
              , o = this.getAccount(n)
              , s = t.s_gi;
            if (!s)
                return null;
            if (this.isValidSCInstance(e) || (e = null),
            !o && !e)
                return I.notify("Adobe Analytics: tracker not initialized because account was not found", 1),
                null;
            e = e || s(o);
            var c = "D" + I.appVersion;
            return "undefined" != typeof e.tagContainerMarker ? e.tagContainerMarker = c : "string" == typeof e.version && e.version.substring(e.version.length - 5) !== "-" + c && (e.version += "-" + c),
            e.sa && !0 !== this.settings.skipSetAccount && !1 !== this.settings.initTool && e.sa(this.settings.account),
            this.applyVarBindingsOnTracker(e, i),
            r.length > 0 && (e.events = r.join(",")),
            I.getVisitorId() && (e.visitor = I.getVisitorId()),
            e
        },
        onSCodeLoaded: function(e) {
            this.initialized = !0,
            this.initializing = !1;
            var t = ["Adobe Analytics: loaded", e ? " (manual)" : "", "."];
            I.notify(t.join(""), 1),
            I.fireEvent(this.id + ".load", this.getS()),
            e || (this.flushQueueExceptTrackLink(),
            this.sendBeacon()),
            this.flushQueue()
        },
        getAccount: function(e) {
            return t.s_account ? t.s_account : e && this.settings.accountByHost && this.settings.accountByHost[e] || this.settings.account
        },
        getTrackingServer: function() {
            var e = this
              , a = e.getS();
            if (a) {
                if (a.ssl && a.trackingServerSecure)
                    return a.trackingServerSecure;
                if (a.trackingServer)
                    return a.trackingServer
            }
            var n, i = e.getAccount(t.location.hostname);
            if (!i)
                return null;
            var r, o, s = "", c = a && a.dc;
            return (r = (n = i).indexOf(",")) >= 0 && (n = n.gb(0, r)),
            n = n.replace(/[^A-Za-z0-9]/g, ""),
            s || (s = "2o7.net"),
            c = c ? ("" + c).toLowerCase() : "d1",
            "2o7.net" == s && ("d1" == c ? c = "112" : "d2" == c && (c = "122"),
            o = ""),
            r = n + "." + c + "." + o + s
        },
        sendBeacon: function() {
            var e = this.getS(t[this.settings.renameS || "s"]);
            e ? this.settings.customInit && !1 === this.settings.customInit(e) ? I.notify("Adobe Analytics: custom init suppressed beacon", 1) : (this.settings.executeCustomPageCodeFirst && this.applyVarBindingsOnTracker(e, this.varBindings),
            this.executeCustomSetupFuns(e),
            e.t(),
            this.clearVarBindings(),
            this.clearCustomSetup(),
            I.notify("Adobe Analytics: tracked page view", 1)) : I.notify("Adobe Analytics: page code not loaded", 1)
        },
        pollForSC: function() {
            I.poll(I.bind(function() {
                if ("function" == typeof t.s_gi)
                    return this.onSCodeLoaded(!0),
                    !0
            }, this))
        },
        flushQueueExceptTrackLink: function() {
            if (this.pending) {
                for (var e = [], t = 0; t < this.pending.length; t++) {
                    var a = this.pending[t];
                    "trackLink" === a[0].command ? e.push(a) : this.triggerCommand.apply(this, a)
                }
                this.pending = e
            }
        },
        isQueueAvailable: function() {
            return !this.initialized
        },
        substituteVariables: function(e, t) {
            var a = {};
            for (var n in e)
                if (e.hasOwnProperty(n)) {
                    var i = e[n];
                    a[n] = I.replace(i, location, t)
                }
            return a
        },
        $setVars: function(e, t, a) {
            for (var n in a)
                if (a.hasOwnProperty(n)) {
                    var i = a[n];
                    "function" == typeof i && (i = i()),
                    this.varBindings[n] = i
                }
            I.notify("Adobe Analytics: set variables.", 2)
        },
        $customSetup: function(e, t, a) {
            this.customSetupFuns.push(function(n) {
                a.call(e, t, n)
            })
        },
        isValidSCInstance: function(e) {
            return !!e && "function" == typeof e.t && "function" == typeof e.tl
        },
        concatWithToolVarBindings: function(e) {
            var t = this.settings.initVars || {};
            return I.map(["trackingServer", "trackingServerSecure"], function(a) {
                t[a] && !e[a] && (e[a] = t[a])
            }),
            e
        },
        applyVarBindingsOnTracker: function(e, t) {
            for (var a in t)
                t.hasOwnProperty(a) && (e[a] = t[a])
        },
        clearVarBindings: function() {
            this.varBindings = {}
        },
        clearCustomSetup: function() {
            this.customSetupFuns = []
        },
        executeCustomSetupFuns: function(e) {
            I.each(this.customSetupFuns, function(a) {
                a.call(t, e)
            })
        },
        $trackLink: function(e, t, a) {
            var n = (a = a || {}).type
              , i = a.linkName;
            !i && e && e.nodeName && "a" === e.nodeName.toLowerCase() && (i = e.innerHTML),
            i || (i = "link clicked");
            var r = a && a.setVars
              , o = a && a.addEvent || []
              , s = this.getS(null, {
                setVars: r,
                addEvent: o
            });
            if (s) {
                var c = s.linkTrackVars
                  , l = s.linkTrackEvents
                  , u = this.definedVarNames(r);
                a && a.customSetup && a.customSetup.call(e, t, s),
                o.length > 0 && u.push("events"),
                s.products && u.push("products"),
                u = this.mergeTrackLinkVars(s.linkTrackVars, u),
                o = this.mergeTrackLinkVars(s.linkTrackEvents, o),
                s.linkTrackVars = this.getCustomLinkVarsList(u);
                var d = I.map(o, function(e) {
                    return e.split(":")[0]
                });
                s.linkTrackEvents = this.getCustomLinkVarsList(d),
                s.tl(!0, n || "o", i),
                I.notify(["Adobe Analytics: tracked link ", "using: linkTrackVars=", I.stringify(s.linkTrackVars), "; linkTrackEvents=", I.stringify(s.linkTrackEvents)].join(""), 1),
                s.linkTrackVars = c,
                s.linkTrackEvents = l
            } else
                I.notify("Adobe Analytics: page code not loaded", 1)
        },
        mergeTrackLinkVars: function(e, t) {
            return e && (t = e.split(",").concat(t)),
            t
        },
        getCustomLinkVarsList: function(e) {
            var t = I.indexOf(e, "None");
            return t > -1 && e.length > 1 && e.splice(t, 1),
            e.join(",")
        },
        definedVarNames: function(e) {
            e = e || this.varBindings;
            var t = [];
            for (var a in e)
                e.hasOwnProperty(a) && /^(eVar[0-9]+)|(prop[0-9]+)|(hier[0-9]+)|campaign|purchaseID|channel|server|state|zip|pageType$/.test(a) && t.push(a);
            return t
        },
        $trackPageView: function(e, t, a) {
            var n = a && a.setVars
              , i = a && a.addEvent || []
              , r = this.getS(null, {
                setVars: n,
                addEvent: i
            });
            r ? (r.linkTrackVars = "",
            r.linkTrackEvents = "",
            this.executeCustomSetupFuns(r),
            a && a.customSetup && a.customSetup.call(e, t, r),
            r.t(),
            this.clearVarBindings(),
            this.clearCustomSetup(),
            I.notify("Adobe Analytics: tracked page view", 1)) : I.notify("Adobe Analytics: page code not loaded", 1)
        },
        $postTransaction: function(e, a, n) {
            var i = I.data.transaction = t[n]
              , r = this.varBindings
              , o = this.settings.fieldVarMapping;
            if (I.each(i.items, function(e) {
                this.products.push(e)
            }, this),
            r.products = I.map(this.products, function(e) {
                var t = [];
                if (o && o.item)
                    for (var a in o.item)
                        if (o.item.hasOwnProperty(a)) {
                            var n = o.item[a];
                            t.push(n + "=" + e[a]),
                            "event" === n.substring(0, 5) && this.events.push(n)
                        }
                var i = ["", e.product, e.quantity, e.unitPrice * e.quantity];
                return t.length > 0 && i.push(t.join("|")),
                i.join(";")
            }, this).join(","),
            o && o.transaction) {
                var s = [];
                for (var c in o.transaction)
                    if (o.transaction.hasOwnProperty(c)) {
                        n = o.transaction[c];
                        s.push(n + "=" + i[c]),
                        "event" === n.substring(0, 5) && this.events.push(n)
                    }
                r.products.length > 0 && (r.products += ","),
                r.products += ";;;;" + s.join("|")
            }
        },
        $addEvent: function() {
            for (var e = 2, t = arguments.length; e < t; e++)
                this.events.push(arguments[e])
        },
        $addProduct: function() {
            for (var e = 2, t = arguments.length; e < t; e++)
                this.products.push(arguments[e])
        }
    }),
    I.availableTools.sc = b,
    I.inherit(L, I.BaseTool),
    I.extend(L.prototype, {
        initialize: function() {
            var e = this.settings;
            if (!1 !== this.settings.initTool) {
                var t = e.url;
                t = "string" == typeof t ? I.basePath() + t : I.isHttps() ? t.https : t.http,
                I.loadScript(t, I.bind(this.onLoad, this)),
                this.initializing = !0
            } else
                this.initialized = !0
        },
        isQueueAvailable: function() {
            return !this.initialized
        },
        onLoad: function() {
            this.initialized = !0,
            this.initializing = !1,
            this.settings.initialBeacon && this.settings.initialBeacon(),
            this.flushQueue()
        },
        endPLPhase: function(e) {
            e === this.settings.loadOn && (I.notify(this.name + ": Initializing at " + e, 1),
            this.initialize())
        },
        $fire: function(e, t, a) {
            this.initializing ? this.queueCommand({
                command: "fire",
                arguments: [a]
            }, e, t) : a.call(this.settings, e, t)
        }
    }),
    I.availableTools.am = L,
    I.availableTools.adlens = L,
    I.availableTools.aem = L,
    I.availableTools.__basic = L,
    I.inherit(S, I.BaseTool),
    I.extend(S.prototype, {
        name: "GA",
        initialize: function() {
            var e = this.settings
              , a = t._gaq
              , n = e.initCommands || []
              , i = e.customInit;
            if (a || (_gaq = []),
            this.isSuppressed())
                I.notify("GA: page code not loaded(suppressed).", 1);
            else {
                if (!a && !S.scriptLoaded) {
                    var r = I.isHttps()
                      , o = (r ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
                    e.url && (o = r ? e.url.https : e.url.http),
                    I.loadScript(o),
                    S.scriptLoaded = !0,
                    I.notify("GA: page code loaded.", 1)
                }
                e.domain;
                var s = e.trackerName
                  , c = O.allowLinker()
                  , l = I.replace(e.account, location);
                I.settings.domainList;
                _gaq.push([this.cmd("setAccount"), l]),
                c && _gaq.push([this.cmd("setAllowLinker"), c]),
                _gaq.push([this.cmd("setDomainName"), O.cookieDomain()]),
                I.each(n, function(e) {
                    var t = [this.cmd(e[0])].concat(I.preprocessArguments(e.slice(1), location, null, this.forceLowerCase));
                    _gaq.push(t)
                }, this),
                i && (this.suppressInitialPageView = !1 === i(_gaq, s)),
                e.pageName && this.$overrideInitialPageView(null, null, e.pageName)
            }
            this.initialized = !0,
            I.fireEvent(this.id + ".configure", _gaq, s)
        },
        isSuppressed: function() {
            return this._cancelToolInit || !1 === this.settings.initTool
        },
        tracker: function() {
            return this.settings.trackerName
        },
        cmd: function(e) {
            var t = this.tracker();
            return t ? t + "._" + e : "_" + e
        },
        $overrideInitialPageView: function(e, t, a) {
            this.urlOverride = a
        },
        trackInitialPageView: function() {
            if (!this.isSuppressed() && !this.suppressInitialPageView)
                if (this.urlOverride) {
                    var e = I.preprocessArguments([this.urlOverride], location, null, this.forceLowerCase);
                    this.$missing$("trackPageview", null, null, e)
                } else
                    this.$missing$("trackPageview")
        },
        endPLPhase: function(e) {
            e === this.settings.loadOn && (I.notify("GA: Initializing at " + e, 1),
            this.initialize(),
            this.flushQueue(),
            this.trackInitialPageView())
        },
        call: function(e, t, a, n) {
            if (!this._cancelToolInit) {
                this.settings;
                var i = this.tracker()
                  , r = this.cmd(e);
                n = n ? [r].concat(n) : [r];
                _gaq.push(n),
                i ? I.notify("GA: sent command " + e + " to tracker " + i + (n.length > 1 ? " with parameters [" + n.slice(1).join(", ") + "]" : "") + ".", 1) : I.notify("GA: sent command " + e + (n.length > 1 ? " with parameters [" + n.slice(1).join(", ") + "]" : "") + ".", 1)
            }
        },
        $missing$: function(e, t, a, n) {
            this.call(e, t, a, n)
        },
        $postTransaction: function(e, a, n) {
            var i = I.data.customVars.transaction = t[n];
            this.call("addTrans", e, a, [i.orderID, i.affiliation, i.total, i.tax, i.shipping, i.city, i.state, i.country]),
            I.each(i.items, function(t) {
                this.call("addItem", e, a, [t.orderID, t.sku, t.product, t.category, t.unitPrice, t.quantity])
            }, this),
            this.call("trackTrans", e, a)
        },
        delayLink: function(e, t) {
            var a = this;
            if (O.allowLinker() && e.hostname.match(this.settings.linkerDomains) && !I.isSubdomainOf(e.hostname, location.hostname)) {
                I.preventDefault(t);
                var n = I.settings.linkDelay || 100;
                setTimeout(function() {
                    a.call("link", e, t, [e.href])
                }, n)
            }
        },
        popupLink: function(e, a) {
            if (t._gat) {
                I.preventDefault(a);
                var n = this.settings.account
                  , i = t._gat._createTracker(n)._getLinkerUrl(e.href);
                t.open(i)
            }
        },
        $link: function(e, t) {
            "_blank" === e.getAttribute("target") ? this.popupLink(e, t) : this.delayLink(e, t)
        },
        $trackEvent: function(e, t) {
            var a = Array.prototype.slice.call(arguments, 2);
            if (a.length >= 4 && null != a[3]) {
                var n = parseInt(a[3], 10);
                I.isNaN(n) && (n = 1),
                a[3] = n
            }
            this.call("trackEvent", e, t, a)
        }
    }),
    I.availableTools.ga = S;
    var O = {
        allowLinker: function() {
            return I.hasMultipleDomains()
        },
        cookieDomain: function() {
            var e = I.settings.domainList
              , a = I.find(e, function(e) {
                var a = t.location.hostname;
                return I.equalsIgnoreCase(a.slice(a.length - e.length), e)
            });
            return a ? "." + a : "auto"
        }
    };
    I.inherit(C, I.BaseTool),
    I.extend(C.prototype, {
        name: "GAUniversal",
        endPLPhase: function(e) {
            e === this.settings.loadOn && (I.notify("GAU: Initializing at " + e, 1),
            this.initialize(),
            this.flushQueue(),
            this.trackInitialPageView())
        },
        getTrackerName: function() {
            return this.settings.trackerSettings.name || ""
        },
        isPageCodeLoadSuppressed: function() {
            return !1 === this.settings.initTool || !0 === this._cancelToolInit
        },
        initialize: function() {
            if (this.isPageCodeLoadSuppressed())
                return this.initialized = !0,
                void I.notify("GAU: Page code not loaded (suppressed).", 1);
            var e = "ga";
            t[e] = t[e] || this.createGAObject(),
            t.GoogleAnalyticsObject = e,
            I.notify("GAU: Page code loaded.", 1),
            I.loadScriptOnce(this.getToolUrl());
            var a = this.settings;
            (O.allowLinker() && !1 !== a.allowLinker ? this.createAccountForLinker() : this.createAccount(),
            this.executeInitCommands(),
            a.customInit) && (!1 === (0,
            a.customInit)(t[e], this.getTrackerName()) && (this.suppressInitialPageView = !0));
            this.initialized = !0
        },
        createGAObject: function() {
            var e = function() {
                e.q.push(arguments)
            };
            return e.q = [],
            e.l = 1 * new Date,
            e
        },
        createAccount: function() {
            this.create()
        },
        createAccountForLinker: function() {
            var e = {};
            O.allowLinker() && (e.allowLinker = !0),
            this.create(e),
            this.call("require", "linker"),
            this.call("linker:autoLink", this.autoLinkDomains(), !1, !0)
        },
        create: function(e) {
            var t = this.settings.trackerSettings;
            (t = I.preprocessArguments([t], location, null, this.forceLowerCase)[0]).trackingId = I.replace(this.settings.trackerSettings.trackingId, location),
            t.cookieDomain || (t.cookieDomain = O.cookieDomain()),
            I.extend(t, e || {}),
            this.call("create", t)
        },
        autoLinkDomains: function() {
            var e = location.hostname;
            return I.filter(I.settings.domainList, function(t) {
                return t !== e
            })
        },
        executeInitCommands: function() {
            var e = this.settings;
            e.initCommands && I.each(e.initCommands, function(e) {
                var t = e.splice(2, e.length - 2);
                e = e.concat(I.preprocessArguments(t, location, null, this.forceLowerCase)),
                this.call.apply(this, e)
            }, this)
        },
        trackInitialPageView: function() {
            this.suppressInitialPageView || this.isPageCodeLoadSuppressed() || this.call("send", "pageview")
        },
        call: function() {
            "function" == typeof ga ? this.isCallSuppressed() || (arguments[0] = this.cmd(arguments[0]),
            this.log(I.toArray(arguments)),
            ga.apply(t, arguments)) : I.notify("GA Universal function not found!", 4)
        },
        isCallSuppressed: function() {
            return !0 === this._cancelToolInit
        },
        $missing$: function(e, t, a, n) {
            n = n || [],
            n = [e].concat(n),
            this.call.apply(this, n)
        },
        getToolUrl: function() {
            var e = this.settings
              , t = I.isHttps();
            return e.url ? t ? e.url.https : e.url.http : (t ? "https://ssl" : "http://www") + ".google-analytics.com/analytics.js"
        },
        cmd: function(e) {
            var t = ["send", "set", "get"]
              , a = this.getTrackerName();
            return a && -1 !== I.indexOf(t, e) ? a + "." + e : e
        },
        log: function(e) {
            var t = "GA Universal: sent command " + e[0] + " to tracker " + (this.getTrackerName() || "default");
            if (e.length > 1) {
                I.stringify(e.slice(1));
                t += " with parameters " + I.stringify(e.slice(1))
            }
            t += ".",
            I.notify(t, 1)
        }
    }),
    I.availableTools.ga_universal = C,
    I.extend(E.prototype, {
        getInstance: function() {
            return this.instance
        },
        initialize: function() {
            var e, t = this.settings;
            I.notify("Visitor ID: Initializing tool", 1),
            null !== (e = this.createInstance(t.mcOrgId, t.initVars)) && (t.customerIDs && this.applyCustomerIDs(e, t.customerIDs),
            t.autoRequest && e.getMarketingCloudVisitorID(),
            this.instance = e)
        },
        createInstance: function(e, t) {
            if (!I.isString(e))
                return I.notify('Visitor ID: Cannot create instance using mcOrgId: "' + e + '"', 4),
                null;
            e = I.replace(e),
            I.notify('Visitor ID: Create instance using mcOrgId: "' + e + '"', 1),
            t = this.parseValues(t);
            var a = Visitor.getInstance(e, t);
            return I.notify("Visitor ID: Set variables: " + I.stringify(t), 1),
            a
        },
        applyCustomerIDs: function(e, t) {
            var a = this.parseIds(t);
            e.setCustomerIDs(a),
            I.notify("Visitor ID: Set Customer IDs: " + I.stringify(a), 1)
        },
        parseValues: function(e) {
            if (!1 === I.isObject(e))
                return {};
            var t = {};
            for (var a in e)
                e.hasOwnProperty(a) && (t[a] = I.replace(e[a]));
            return t
        },
        parseIds: function(e) {
            var t = {};
            if (!1 === I.isObject(e))
                return {};
            for (var a in e)
                if (e.hasOwnProperty(a)) {
                    var n = I.replace(e[a].id);
                    n !== e[a].id && n && (t[a] = {},
                    t[a].id = n,
                    t[a].authState = Visitor.AuthState[e[a].authState])
                }
            return t
        }
    }),
    I.availableTools.visitor_id = E,
    _satellite.init({
        tools: {
            "05c34c1147aedcd15c99feadca7689f86a7e9151": {
                engine: "am",
                loadOn: "pagebottom",
                name: "AudienceManager",
                initTool: !0,
                url: "3c7a22bb879803cc2beb8b04f846f928b6fea2ec/dil-contents-05c34c1147aedcd15c99feadca7689f86a7e9151.js"
            },
            cc11c78708bde0e4ba54a53992476b83: {
                engine: "sc",
                loadOn: "pagetop",
                account: "belongdev",
                euCookie: !1,
                sCodeURL: "3c7a22bb879803cc2beb8b04f846f928b6fea2ec/s-code-contents-57ede10dd4a55476898377b1fb1e9b8e70458d6a.js",
                renameS: "s",
                initVars: {
                    currencyCode: "AUD",
                    trackingServer: "info.belong.com.au",
                    trackingServerSecure: "infos.belong.com.au",
                    campaign: "%omd_tracking_code%",
                    pageURL: "%url%",
                    trackInlineStats: !0,
                    trackDownloadLinks: !0,
                    linkDownloadFileTypes: "avi,css,csv,doc,docx,eps,exe,jpg,js,m4v,mov,mp3,pdf,png,ppt,pptx,rar,svg,tab,txt,vsd,vxd,wav,wma,wmv,xls,xlsx,xml,zip",
                    trackExternalLinks: !0,
                    linkExternalFilters: "mailto:,tel:",
                    linkInternalFilters: "belong.com.au,belongtest.com.au,javascript:",
                    linkLeaveQueryString: !1,
                    dynamicVariablePrefix: "D=",
                    eVar18: "%customerType%",
                    eVar16: "%customerId%",
                    eVar7: "%deviceType%",
                    eVar43: "%metaKeywords%",
                    eVar44: "%metaDescription%",
                    eVar45: "%gclid%",
                    eVar46: "%utm_source%",
                    eVar47: "%utm_medium%",
                    eVar48: "%utm_term%",
                    eVar49: "%utm_campaign%",
                    eVar29: "%postCode%",
                    eVar30: "%state%",
                    eVar50: "%utm_content%",
                    eVar23: "%liveAgentSessionId%",
                    eVar2: "D=c2",
                    eVar57: "%marketingCloudVisitorId%",
                    eVar20: "%hotjarUserId%",
                    eVar80: "%auth0id%",
                    prop1: "BL",
                    prop2: "%subDivision%",
                    prop7: "%deviceType%"
                },
                skipSetAccount: !0,
                customInit: function(e) {
                    function t() {
                        "undefined" == typeof s_events && (s_events = "");
                        var t = a.location.pathname;
                        if ("undefined" == typeof s_section || "" == s_section) {
                            "/" == t && (t = "/homepage");
                            var n = t.split("/");
                            "undefined" != typeof s_subSectionOvrd && s_subSectionOvrd ? (n.splice(0, 1),
                            n.unshift(s_subSectionOvrd),
                            n.unshift(s_sectionOvrd),
                            n.unshift("")) : "undefined" != typeof s_sectionOvrd && s_sectionOvrd && (n.splice(0, 1),
                            n.unshift(s_sectionOvrd),
                            n.unshift(""));
                            var i = decodeURI(a.location.search).split("&");
                            s_pageName = e.generateURLPageName(n),
                            s_pageName = "bl:" + s_pageName;
                            var r = /page/i;
                            if ("all" == "none")
                                for (var o = 0, s = 0; s < i.length; s++) {
                                    var c = i[s];
                                    ("all" == r || r.test(c)) && (/\=$/.test(c) || (s_pageQuery = 0 == s ? c : 0 == o ? "?" + c : s_pageQuery + "&" + c,
                                    o++))
                                }
                            "undefined" != typeof s_pageQuery && "undefined" != typeof s_pageName && (s_pageName = s_pageName + ":" + s_pageQuery)
                        }
                    }
                    s_removeHtmlTags = function(e) {
                        return e.replace(/<(a+)[^>]*>.*\n?<\/\1>|<(\w+)[^>]*>|<\/.+>/gi, "")
                    }
                    ,
                    s_removeAllSpecialChars = function(e) {
                        var t = s_removeHtmlTags(e);
                        return t = (t = (t = (t = (t = (t = t.replace(/&amp;/gi, "and")).replace(/&/gi, "and")).replace(/,/gi, "")).replace(/  /gi, "")).replace(/\n/g, "")).replace(/[^\w\s\.\%]/gi, "")
                    }
                    ,
                    "function" == typeof t && t()
                }
            },
            "795393a3a2f132942dbe3020c216795215d31821": {
                engine: "tnt",
                mboxURL: "3c7a22bb879803cc2beb8b04f846f928b6fea2ec/mbox-contents-795393a3a2f132942dbe3020c216795215d31821.js",
                loadSync: !0,
                pageParams: {
                    site: "belong",
                    at_property: "fe64daa5-48cc-df93-e158-14c2bc1799bd",
                    fbbPlanType: "%planType%",
                    fbbServiceType: "%serviceType%"
                }
            },
            cc0a81309cba3c8200aa5a00225cfa413148449b: {
                engine: "visitor_id",
                loadOn: "pagetop",
                name: "VisitorID",
                mcOrgId: "98DC73AE52E13F1E0A490D4C@AdobeOrg",
                autoRequest: !0,
                initVars: {
                    trackingServer: "info.belong.com.au",
                    trackingServerSecure: "infos.belong.com.au",
                    marketingCloudServer: "info.belong.com.au",
                    marketingCloudServerSecure: "infos.belong.com.au",
                    loadTimeout: "2000",
                    cookieDomain: "%rootHostname%"
                },
                customerIDs: {
                    octane_id: {
                        id: "%customerId%",
                        authState: "AUTHENTICATED"
                    }
                }
            }
        },
        pageLoadRules: [{
            name: "Account Pages - Customer Id Check",
            trigger: [{
                engine: "sc",
                command: "setVars",
                arguments: [{
                    eVar71: "%serviceType%",
                    eVar80: "%auth0id%"
                }]
            }, {
                command: "loadScript",
                arguments: [{
                    sequential: !1,
                    scripts: [{
                        src: "satellite-566dfca564746d2e12004f14.js"
                    }]
                }]
            }],
            scope: {
                URI: {
                    include: [/^\/account/i]
                }
            },
            event: "domready"
        }, {
            name: "Belong Live Agent Mbox",
            trigger: [{
                command: "loadScript",
                arguments: [{
                    sequential: !1,
                    scripts: [{
                        src: "satellite-57e1f9e164746d323800822f.js"
                    }]
                }]
            }],
            conditions: [function() {
                return _satellite.textMatch(_satellite.getVar("isBelongApp"), "FALSE")
            }
            , function() {
                if (/^(.*\.belong.com.au|preprod.belongtest.com.au)$/i.test(location.hostname))
                    return !0
            }
            ],
            event: "pagetop"
        }, {
            name: "FAQ Public Pages",
            trigger: [{
                engine: "sc",
                command: "setVars",
                arguments: [{
                    eVar71: "%serviceType%",
                    prop33: "%internalSearchString%",
                    channel: "bl"
                }]
            }, {
                engine: "sc",
                command: "addEvent",
                arguments: ["event3"]
            }, {
                engine: "sc",
                command: "customSetup",
                arguments: [function(e, n) {
                    function i() {
                        var e = a.location.pathname
                          , i = (a.location.hostname,
                        new Array)
                          , r = e.split("/");
                        for (s = 1; s < r.length; s++) {
                            var o = r[s];
                            o && (o = (o = (o = o.toLowerCase()).replace(/^\s+/, "")).replace(/ /g, "-"),
                            i.push(o))
                        }
                        _satellite.notify("Support Pages:s_taxArr:" + i.toString(), 1),
                        "undefined" == typeof jQuery && s_logTagErrorsFn("jQuery missing at time of execution", 1),
                        s_pageName = "";
                        for (var s = 0; s < i.length; s++)
                            s_pageName = 0 == s ? i[s] : s_pageName + ":" + i[s];
                        "undefined" != typeof s_pageName && (s_pageName = "bl:" + s_pageName),
                        t.addEventListener("load", function() {
                            var e = function(e) {
                                var t = a.createElement("a");
                                return t.href = e,
                                "" == t.host && (t.href = t.href),
                                t.pathname
                            };
                            t.BELONG = t.BELONG ? t.BELONG : {},
                            t.BELONG.dataLayer = t.BELONG.dataLayer ? t.BELONG.dataLayer : {},
                            $(a).on("belong.analytics.knowledge.openFaq", function(t, a) {
                                BELONG.dataLayer.openFaq = {
                                    event: t,
                                    data: a
                                },
                                n.pageName = "bl" + e(a.faqUrl).replace(/\//g, ":"),
                                _satellite.track("analytics-openFaq")
                            }),
                            $(a).on("belong.analytics.knowledge.thumbsUp", function(t, a) {
                                BELONG.dataLayer.thumbsUp = {
                                    event: t,
                                    data: a
                                },
                                n.pageName = "bl" + e(a.faqUrl).replace(/\//g, ":"),
                                _satellite.track("analytics-faq-thumbsUp")
                            }),
                            $(a).on("belong.analytics.knowledge.thumbsDown", function(t, a) {
                                BELONG.dataLayer.thumbsDown = {
                                    event: t,
                                    data: a
                                },
                                n.pageName = "bl" + e(a.faqUrl).replace(/\//g, ":"),
                                _satellite.track("analytics-faq-thumbsDown")
                            })
                        })
                    }
                    "function" == typeof i && i()
                }
                ]
            }],
            scope: {
                subdomains: {
                    include: [/support/i]
                },
                domains: [/belong\.com\.au$/i]
            },
            event: "pagetop"
        }, {
            name: "Hotjar Setup",
            trigger: [{
                command: "loadBlockingScript",
                arguments: [{
                    sequential: !0,
                    scripts: [{
                        src: "satellite-5bc9123864746d3f92000a9c.js"
                    }]
                }]
            }],
            conditions: [function() {
                return _satellite.textMatch(_satellite.getVar("hotjarDomains"), "TRUE")
            }
            ],
            event: "pagebottom"
        }, {
            name: "Join Form Page",
            trigger: [{
                engine: "sc",
                command: "addEvent",
                arguments: ["event29"]
            }, {
                command: "loadBlockingScript",
                arguments: [{
                    sequential: !0,
                    scripts: [{
                        src: "satellite-5ccb9e9b64746d124b003209.js"
                    }]
                }]
            }, {
                command: "loadScript",
                arguments: [{
                    sequential: !1,
                    scripts: [{
                        src: "satellite-56e7765564746d05500039e5.js"
                    }]
                }]
            }],
            conditions: [function() {
                return _satellite.textMatch(_satellite.getVar("pathname"), /^\/.*join(\.htm(l)?|$)/i)
            }
            , function() {
                return _satellite.textMatch(_satellite.getVar("hostname"), "www.belong.com.au")
            }
            ],
            event: "pagetop"
        }, {
            name: "Livechat Click Listener",
            trigger: [{
                command: "loadScript",
                arguments: [{
                    sequential: !1,
                    scripts: [{
                        src: "satellite-56ba6c7964746d38c4000f4e.js"
                    }]
                }]
            }],
            conditions: [function() {
                return _satellite.textMatch(_satellite.getVar("moorupDomains"), "FALSE")
            }
            ],
            event: "domready"
        }, {
            name: "Load Google Scripts",
            conditions: [function() {
                var e = _satellite.settings.isStaging ? "GTM-KDQRBMT" : "GTM-P7RLS5N";
                return e = "GTM-P7RLS5N",
                t.GoogleAnalyticsObject = "ga",
                t.ga = t.ga || function() {
                    (t.ga.q = t.ga.q || []).push(arguments)
                }
                ,
                function(e, t, a, n, i) {
                    e[n] = e[n] || [],
                    e[n].push({
                        "gtm.start": (new Date).getTime(),
                        event: "gtm.js"
                    });
                    var r = t.getElementsByTagName(a)[0]
                      , o = t.createElement(a)
                      , s = "dataLayer" != n ? "&l=" + n : "";
                    o.async = !0,
                    o.src = "https://www.googletagmanager.com/gtm.js?id=" + i + s,
                    r.parentNode.insertBefore(o, r),
                    _satellite.notify("[BELONG LEGACY] Google Tag Manager Script loaded for " + i, 1)
                }(t, a, "script", "dataLayer", e),
                !0
            }
            ],
            event: "pagetop"
        }, {
            name: "Logged In Agent",
            trigger: [{
                engine: "sc",
                command: "setVars",
                arguments: [{
                    eVar17: "%agentID%",
                    eVar98: "Page Rule>Logged In Agent Testing"
                }]
            }],
            conditions: [function() {
                return !!a.querySelector("#GlobalData").getAttribute("data-is-agent")
            }
            ],
            event: "pagebottom"
        }, {
            name: "Logged In Pages",
            trigger: [{
                engine: "sc",
                command: "setVars",
                arguments: [{
                    eVar10: "%salesforce_id%",
                    eVar16: "%customerId%",
                    eVar18: "%customerType%",
                    prop73: "belong-logged-in"
                }]
            }, {
                engine: "sc",
                command: "addEvent",
                arguments: ["event8"]
            }, {
                command: "loadScript",
                arguments: [{
                    sequential: !1,
                    scripts: [{
                        src: "satellite-56e7769764746d0532003bee.js"
                    }]
                }]
            }],
            conditions: [function() {
                return _satellite.textMatch(_satellite.getVar("isLoggedIn"), "true")
            }
            ],
            event: "domready"
        }, {
            name: "Login Page",
            trigger: [{
                engine: "sc",
                command: "customSetup",
                arguments: [function() {
                    s_pageName = "bl:login"
                }
                ]
            }],
            scope: {
                URI: {
                    include: [/\/am\/ui\/login/i, "SiteLoginPage", "siteloginpage"]
                }
            },
            event: "pagebottom"
        }, {
            name: "Logout Page",
            trigger: [{
                engine: "sc",
                command: "customSetup",
                arguments: [function() {
                    s_pageName = "bl:logout"
                }
                ]
            }],
            conditions: [function() {
                if (/SiteLoginPage/i.test(location.pathname) && /logout=true/i.test(location.search))
                    return !0
            }
            ],
            event: "pagebottom"
        }, {
            name: "One Off Payment",
            trigger: [{
                engine: "sc",
                command: "setVars",
                arguments: [{
                    prop31: "one off payment",
                    pageName: "%pageName%:completed"
                }]
            }, {
                engine: "sc",
                command: "addEvent",
                arguments: ["event12", "event37"]
            }],
            scope: {
                URI: {
                    include: ["/account/pay"]
                }
            },
            conditions: [function() {
                return _satellite.textMatch(_satellite.getQueryParam("SECURETOKENID"), /.+/i)
            }
            , function() {
                return _satellite.textMatch(_satellite.getQueryParam("INVOICE"), /.+/i)
            }
            ],
            event: "pagetop"
        }, {
            name: "jQuery trigger listeners",
            trigger: [{
                command: "loadScript",
                arguments: [{
                    sequential: !1,
                    scripts: [{
                        src: "satellite-55ced3083437610014000991.js"
                    }, {
                        src: "satellite-55ced3083437610014000993.js"
                    }]
                }]
            }],
            conditions: [function() {
                return _satellite.textMatch(_satellite.getVar("moorupDomains"), "FALSE")
            }
            ],
            event: "domready"
        }],
        rules: [{
            name: "Account Add Speed Boost",
            trigger: [{
                command: "loadScript",
                arguments: [{
                    sequential: !1,
                    scripts: [{
                        src: "satellite-55e39bfe3961370014000056.js"
                    }]
                }]
            }],
            selector: "button#SpeedBoostSubmit",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "Account Join Completion Steps",
            trigger: [{
                command: "loadScript",
                arguments: [{
                    sequential: !1,
                    scripts: [{
                        src: "satellite-55dd5218633362008b000046.js"
                    }]
                }]
            }],
            scope: {
                URI: {
                    include: [/^\/account/i]
                }
            },
            selector: "div.light-box-module-container button",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "Account Troubleshooting Start",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar10: "%salesforce_id%",
                        eVar100: "account troubleshooting start",
                        eVar16: "%customerId%",
                        eVar18: "%customerType%",
                        pageName: "bl:account:troubleshooting:cards:question_adsl_landing"
                    }
                }]
            }],
            selector: "a.open-troubleshooting-modal",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "Contact Us Phone Reveal",
            trigger: [{
                command: "loadScript",
                arguments: [{
                    sequential: !1,
                    scripts: [{
                        src: "satellite-58f97afb64746d32da0090f3.js"
                    }]
                }]
            }],
            scope: {
                URI: {
                    include: ["/contact-us"]
                }
            },
            selector: "a.contact-us-btn",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "Critical Information Summary",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "product-builder:click:critical information summary",
                    addEvent: ["event4"]
                }]
            }],
            selector: "#label-join-cis-acceptChk a",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "Customer Terms",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "product-builder:click:customer terms",
                    addEvent: ["event4"]
                }]
            }],
            selector: "#label-join-cis-portChk a[href='https://www.belong.com.au/customer-terms?section=general-terms']",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "FAQ Public Phone Reveal",
            trigger: [{
                command: "loadScript",
                arguments: [{
                    sequential: !1,
                    scripts: [{
                        src: "satellite-57f44f2c64746d082c007213.js"
                    }]
                }]
            }],
            scope: {
                subdomains: {
                    include: ["support"]
                }
            },
            selector: "a.contact-us-btn",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "Join Form Submit Click ",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "Join Form - Join Button - submit click",
                    addEvent: ["event6"]
                }]
            }],
            scope: {
                URI: {
                    include: [/join/i]
                }
            },
            selector: "a#join-payconfirm-submit",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "Login Failure",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "login button:click:username or password error"
                }]
            }],
            scope: {
                URI: {
                    include: ["SiteLoginPage"]
                }
            },
            event: "dataelementchange(loginPasswordErrorDisplayed)",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "Set new password",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        prop31: "set new password",
                        pageName: "bl:account:personal-details:set-new-password"
                    },
                    addEvent: ["event12"]
                }]
            }],
            selector: "button#change-password-submit",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "Speedboost Promotion T&Cs",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "product-builder:click:speedboost promo terms",
                    addEvent: ["event4"]
                }]
            }],
            selector: ".show-for-promotional-speedboost a",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !0
        }, {
            name: "Support Form submit click",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "Contact Us Form Submit Click",
                    setVars: {
                        eVar32: "%Contact Us Enquiry Type%",
                        prop32: "%Contact Us Enquiry Type%"
                    },
                    addEvent: ["event4"]
                }]
            }],
            scope: {
                subdomains: {
                    include: ["help"]
                }
            },
            selector: "input#email_submit",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "Update Mobile Number",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        prop31: "update mobile number",
                        pageName: "bl:account:personal-details:update-mobile-number"
                    },
                    addEvent: ["event12"]
                }]
            }],
            selector: "button#change-mobile-submit",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "View Modem Details",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        prop31: "view modem details",
                        pageName: "bl:account:account-details:view-modem-details"
                    },
                    addEvent: ["event12"]
                }]
            }],
            selector: "a#belong-card-container__show-password-btn",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "(Rule for AAM integration)",
            event: "cc11c78708bde0e4ba54a53992476b83.load",
            trigger: [{
                tool: "05c34c1147aedcd15c99feadca7689f86a7e9151",
                command: "fire",
                arguments: [function(e) {
                    function t(e) {
                        for (var t in e)
                            if (e.hasOwnProperty(t) && "" !== t)
                                return !1;
                        return !0
                    }
                    var n, i, r = _satellite.getVar("auth0id"), o = "512735", s = "442858", c = _satellite.getVar("salesforce_id"), l = _satellite.getVar("utm_user_id");
                    n = l || r || c,
                    i = r ? o : s;
                    var u = DIL.create({
                        partner: "teamtelstra",
                        uuidCookie: {
                            name: "aam_uuid",
                            days: 30
                        },
                        declaredId: {
                            dpid: i,
                            dpuuid: n
                        }
                    });
                    r && u.api.aamIdSync({
                        dpid: o,
                        dpuuid: r,
                        minutesToLive: 1
                    }),
                    c && u.api.aamIdSync({
                        dpid: s,
                        dpuuid: c,
                        minutesToLive: 1
                    }),
                    a.referrer && u.api.signals({
                        c_dilReferer: a.referrer
                    }),
                    location.href && u.api.signals({
                        c_locationHref: _satellite.getVar("url")
                    });
                    var d, g = DIL.tools.decomposeURI(a.URL);
                    delete g.search,
                    delete g.href,
                    t(g.uriParams) || u.api.signals(g.uriParams, "c_"),
                    delete g.uriParams,
                    u.api.signals(g, "c_"),
                    d = void 0 !== e && e === Object(e) && "undefined" != typeof e.account && e.account ? s_gi(e.account) : s_gi(s_account),
                    DIL.modules.siteCatalyst.init(d, u, {
                        names: ["pageName", "channel", "campaign", "products", "events", "pe", "referrer", "server", "purchaseID", "zip", "state"],
                        iteratedNames: [{
                            name: "eVar",
                            maxIndex: 100
                        }, {
                            name: "prop",
                            maxIndex: 75
                        }, {
                            name: "pev",
                            maxIndex: 3
                        }, {
                            name: "hier",
                            maxIndex: 4
                        }]
                    }),
                    _satellite.settings.notifications && console.log("BL LEGACY AAM ID SYNC", {
                        adobeDil: u,
                        auth0Id: r,
                        auth0PartnerId: o,
                        salesforceId: c,
                        salesforcePartnerId: s,
                        declaredId: n,
                        declaredPartnerId: i,
                        utmUserId: l
                    })
                }
                ]
            }]
        }],
        directCallRules: [{
            name: "hotjar_recording_tags",
            trigger: [{
                command: "loadScript",
                arguments: [{
                    sequential: !0,
                    scripts: [{
                        src: "satellite-5ca68b9264746d2d0900a80b.js"
                    }]
                }]
            }]
        }, {
            name: "conversion_pixel",
            trigger: [{
                command: "loadScript",
                arguments: [{
                    sequential: !0,
                    scripts: [{
                        src: "satellite-5b35e46464746d39e10011d6.js"
                    }]
                }]
            }]
        }, {
            name: "analytics-eventTagError",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "analytics-eventTagError",
                    setVars: {
                        eVar100: "analytics-eventTagError",
                        eVar91: "Custom event triggered with no virtualPage OR eventCategory",
                        eVar99: "%dtmTriggerData%"
                    },
                    addEvent: ["event91"]
                }]
            }]
        }, {
            name: "analytics-faq-thumbsDown",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "Support Article Feedback : Not Helpful",
                    setVars: {
                        eVar8: "D=g",
                        eVar93: "%virtualPage%",
                        eVar94: "%window.s_pageName%",
                        channel: "bl:faq-public",
                        pageURL: "%virtualPage%"
                    },
                    addEvent: ["event4", "event41"]
                }]
            }]
        }, {
            name: "analytics-faq-thumbsUp",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "Support Article Feedback : Helpful",
                    setVars: {
                        eVar8: "%virtualPage%",
                        eVar93: "D=pageName",
                        eVar94: "%window.s_pageName%",
                        channel: "bl:faq-public",
                        pageURL: "%virtualPage%"
                    },
                    addEvent: ["event4", "event40"]
                }]
            }]
        }, {
            name: "analytics-openFaq",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar8: "D=g",
                        eVar93: "D=pageName"
                    },
                    addEvent: ["event3"]
                }]
            }]
        }, {
            name: "analytics-trackAccountAddSpeedBoost",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar10: "%salesforce_id%",
                        eVar100: "analytics-trackAccountAddSpeedBoost",
                        eVar16: "%customerId%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop31: "AddSpeedBoost",
                        pageName: "bl:account:account-details:add-speed-boost-complete"
                    },
                    customSetup: function(e, t) {
                        var a = []
                          , n = "";
                        try {
                            n = $('input[id^="speedboost"]:checked').attr("id")
                        } catch (o) {}
                        if (n) {
                            var i = "add:speed-boost:" + n;
                            a.push(i)
                        }
                        t.products = "";
                        try {
                            for (var r = 0; r < a.length; r++)
                                r > 0 && (t.products = t.products + ","),
                                t.products = t.products + ";" + a[r] + ";1;0"
                        } catch (o) {}
                    },
                    addEvent: ["event12", "event9", "purchase"]
                }]
            }]
        }, {
            name: "analytics-trackAccountAddVoice",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar10: "%salesforce_id%",
                        eVar100: "analytics-trackAccountAddVoice",
                        eVar16: "%customerId%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop31: "AddVoice",
                        pageName: "bl:account:account-details:add-voice-complete"
                    },
                    customSetup: function(e, t) {
                        var a = [];
                        try {
                            var n = "";
                            if ("undefined" != typeof BELONG.dataLayer.event.voicePlan && null != BELONG.dataLayer.event.voicePlan && (n = BELONG.dataLayer.event.voicePlan),
                            n) {
                                var i = "add:voice-plan:" + n;
                                a.push(i)
                            }
                        } catch (o) {}
                        t.products = "";
                        try {
                            for (var r = 0; r < a.length; r++)
                                r > 0 && (t.products = t.products + ","),
                                t.products = t.products + ";" + a[r] + ";1;0"
                        } catch (o) {}
                    },
                    addEvent: ["event12", "event9", "purchase"]
                }]
            }]
        }, {
            name: "analytics-trackAccountJoinCompletion",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    type: "o",
                    linkName: "%eventCategory%:%eventAction%:%eventLabel%",
                    setVars: {
                        eVar10: "%salesforce_id%",
                        eVar100: "analytics-trackAccountJoinCompletion",
                        eVar16: "%customerId%",
                        eVar18: "%customerType%",
                        pageName: "bl:account:join:%eventCategory%"
                    },
                    customSetup: function(e, t) {
                        try {
                            var a = _satellite.getVar("eventCategory");
                            /surveycontinue/i.test(a) && (t.events = t.apl(t.events, "event34", ",", 1))
                        } catch (n) {}
                    },
                    addEvent: ["event8"]
                }]
            }]
        }, {
            name: "analytics-trackAppointmentSetting",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-trackAppointmentSetting",
                        eVar26: "%eventCategory%:%eventAction%:%eventLabel%",
                        eVar27: "%pageName%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop27: "%pageName%",
                        prop31: "%eventCategory%"
                    },
                    customSetup: function(e, t) {
                        var a = {
                            DEFAULTS: {
                                category: "Nbn-Appointment",
                                action: "click",
                                label: "proof of occupancy label not set",
                                error: ""
                            },
                            NBN_GET_AVAILABLE_APPOINTMENTS: {
                                label: "Get available appointments",
                                action: "load"
                            },
                            NBN_GET_AVAILABLE_APPOINTMENTS_SUCCESS: {
                                label: "Get available appointments succeeds",
                                action: "load"
                            },
                            NBN_GET_AVAILABLE_APPOINTMENTS_FAIL: {
                                label: "Get available appointments failure",
                                action: "load"
                            },
                            NBN_SET_APPOINTMENT_DETAILS: {
                                label: "Set appointment details",
                                action: "load"
                            },
                            NBN_UPDATE_APPOINTMENT_TIME: {
                                label: "Triggered on selection of time",
                                action: "click"
                            },
                            NBN_SUBMIT_CHANGE_APPOINTMENT: {
                                label: "Submit change appointment",
                                action: "click"
                            },
                            NBN_CHANGE_APPOINTMENT_SUCCESS: {
                                label: "Submit change appointment success",
                                action: "load"
                            },
                            NBN_CHANGE_APPOINTMENT_FAILURE: {
                                label: "Submit change appointment fails",
                                action: "load"
                            },
                            NBN_APPOINTMENT_CHANGE_PROGRESS: {
                                label: "Submission is in progress",
                                action: "load"
                            },
                            NBN_RETRIEVE_APPOINTMENT: {
                                label: "Retrieve saved appointment",
                                action: "load"
                            },
                            NBN_RETRIEVE_APPOINTMENT_SUCCESS: {
                                label: "Retrieve Appointment success",
                                action: "load"
                            },
                            NBN_RETRIEVE_APPOINTMENT_FAIL: {
                                label: "Retrieve Appointment fails",
                                action: "load"
                            }
                        }
                          , n = (e = ((BELONG || {}).dataLayer || {}).event || {}).type;
                        a[n] && (e.eventAction = a[n].action ? a[n].action : a.DEFAULTS.action,
                        e.eventLabel = a[n].label ? a[n].label : a.DEFAULTS.label,
                        e.eventCategory = a[n].category ? a[n].category : a.DEFAULTS.category,
                        e.errorCode = a[n].error ? a[n].error : a.DEFAULTS.error),
                        BELONG.dataLayer.event = e,
                        t.prop26 = [e.eventCategory, e.eventAction, e.eventLabel].join(":"),
                        t.eVar26 = "D=c26",
                        t.prop31 = e.eventCategory,
                        _satellite.notify("analytics-trackAppointmentSetting custom page code", 3),
                        _satellite.custom_debugMode && console.debug("eventType:", n, BELONG.dataLayer.event)
                    },
                    addEvent: ["event12", "event4"]
                }]
            }]
        }, {
            name: "analytics-trackCallbackSubmit",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    type: "o",
                    linkName: "%eventCategory%:%eventAction%:%eventLabel%",
                    setVars: {
                        eVar100: "analytics-trackCallbackSubmit",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop27: "%pageName%",
                        prop32: "%pageName%:call me back",
                        pageName: "%pageName%:call me back"
                    },
                    addEvent: ["event18"]
                }]
            }]
        }, {
            name: "analytics-trackCustomLink",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "%eventCategory%:%eventAction%:%eventLabel%",
                    setVars: {
                        eVar100: "analytics-trackCustomLink",
                        eVar26: "%eventCategory%:%eventAction%:%eventLabel%",
                        eVar27: "%pageName%",
                        eVar55: "%utm_user_id%",
                        eVar80: "%auth0id%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "D=v26",
                        prop27: "D=v27"
                    },
                    addEvent: ["event4"]
                }]
            }]
        }, {
            name: "analytics-trackGlobalRevealPhoneNumber",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    type: "o",
                    linkName: "%eventCategory%:%eventAction%:%eventLabel%",
                    setVars: {
                        eVar100: "analytics-trackGlobalRevealPhoneNumber",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop27: "%pageName%",
                        prop32: "%pageName%:reveal phone number",
                        pageName: "%pageName%:reveal phone number"
                    },
                    addEvent: ["event18"]
                }]
            }]
        }, {
            name: "analytics-trackJoinComplete",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar10: "%salesforce_id%",
                        eVar100: "analytics-trackJoinComplete",
                        eVar16: "%customerIdJoin%",
                        eVar36: "%promoCode%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        pageName: "bl:join:join-complete"
                    },
                    customSetup: function(e, t) {
                        var a = "join-flow";
                        if ("undefined" != typeof t.getPreviousValue) {
                            var n = t.getPreviousValue("", "gpv_ps");
                            void 0 !== n && n && "no value" != n && (t.products = n,
                            a = "account-page")
                        }
                        if ("join-flow" == a) {
                            var i = [];
                            try {
                                var r = "";
                                "undefined" != typeof BELONG.dataLayer.event.serviceClass && null != BELONG.dataLayer.event.serviceClass && (r = BELONG.dataLayer.event.serviceClass),
                                r && i.push("service-class:" + r)
                            } catch (p) {}
                            try {
                                var o = "";
                                "undefined" != typeof BELONG.dataLayer.event.planSizeType && null != BELONG.dataLayer.event.planSizeType && (o = BELONG.dataLayer.event.planSizeType),
                                o && (c = o,
                                i.push(c))
                            } catch (p) {}
                            try {
                                var s = "";
                                if ("string" == typeof BELONG.dataLayer.event.withSpeedBoost && (s = BELONG.dataLayer.event.withSpeedBoost),
                                "true" === s && "undefined" != typeof BELONG.dataLayer.event.planSpeed && null != BELONG.dataLayer.event.planSpeed && BELONG.dataLayer.event.planSpeed) {
                                    var c = "speed:" + BELONG.dataLayer.event.planSpeed;
                                    i.push(c)
                                }
                            } catch (p) {}
                            try {
                                var l = "";
                                "undefined" != typeof BELONG.dataLayer.event.withContract && null != BELONG.dataLayer.event.withContract && (l = BELONG.dataLayer.event.withContract),
                                "true" === l ? i.push("contract:yes") : "false" === l && i.push("contract:no")
                            } catch (p) {}
                            try {
                                var u = "";
                                if ("undefined" != typeof BELONG.dataLayer.event.voicePlan && null != BELONG.dataLayer.event.voicePlan && (u = BELONG.dataLayer.event.voicePlan),
                                u) {
                                    c = "voice-plan:" + u;
                                    i.push(c)
                                }
                            } catch (p) {}
                            t.products = "";
                            try {
                                for (var d = 0; d < i.length; d++)
                                    d > 0 && (t.products = t.products + ","),
                                    t.products = t.products + ";" + i[d] + ";1;0";
                                if ("undefined" != typeof BELONG.dataLayer.totalMinCost && BELONG.dataLayer.totalMinCost > 0 && (t.products = t.products + ",;total-min-cost;1;" + BELONG.dataLayer.totalMinCost.toString()),
                                "undefined" != typeof t.getPreviousValue) {
                                    t.getPreviousValue(t.products, "gpv_ps"),
                                    t.getPreviousValue("order-complete", "gpv_oc");
                                    if ("undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && "undefined" != typeof BELONG.dataLayer.event && "undefined" != typeof BELONG.dataLayer.event.userid && BELONG.dataLayer.event.userid) {
                                        var g = BELONG.dataLayer.event.userid;
                                        t.getPreviousValue(g, "gpv_bc")
                                    }
                                }
                            } catch (p) {}
                        }
                    },
                    addEvent: ["event5", "purchase"]
                }]
            }]
        }, {
            name: "analytics-trackJoinStep",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-trackJoinStep",
                        eVar99: "%dtmTriggerData%",
                        pageName: "bl:join:%eventLabel%"
                    },
                    customSetup: function(e, t) {
                        try {
                            var a = _satellite.getVar("eventLabel");
                            /^1\-/i.test(a) ? t.events = t.apl(t.events, "event30", ",", 1) : /^2\-/i.test(a) ? t.events = t.apl(t.events, "event31", ",", 1) : /^3\-pay/i.test(a) ? t.events = t.apl(t.events, "event32", ",", 1) : /^4\-check|^3\-check/i.test(a) && (t.events = t.apl(t.events, "event33", ",", 1))
                        } catch (i) {}
                        try {
                            if ("undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && ("undefined" == typeof BELONG.dataLayer.totalMinCost || 0 === BELONG.dataLayer.totalMinCost)) {
                                var n = parseInt($("span#total-min-cost").text());
                                BELONG.dataLayer.totalMinCost = n
                            }
                        } catch (i) {}
                    }
                }]
            }]
        }, {
            name: "analytics-trackLiveChatClick",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "Live Chat Click",
                    setVars: {
                        eVar26: "live-chat:click:%liveChatImage%",
                        prop26: "live-chat:click:%liveChatImage%"
                    },
                    addEvent: ["event16", "event4"]
                }]
            }]
        }, {
            name: "analytics-trackManageServiceDisplay",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar10: "%salesforce_id%",
                        eVar100: "analytics-trackManageServiceDisplay",
                        eVar16: "%customerId%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        pageName: "bl:account:manage-service:display-current-plan"
                    },
                    customSetup: function(e, t) {
                        var a = [];
                        try {
                            var n = "";
                            "undefined" != typeof BELONG.dataLayer.event.oldPlan.planType && null != BELONG.dataLayer.event.oldPlan.planType && (n = BELONG.dataLayer.event.oldPlan.planType);
                            var i = "";
                            "undefined" != typeof BELONG.dataLayer.event.oldPlan.planSize && null != BELONG.dataLayer.event.oldPlan.planSize && (i = BELONG.dataLayer.event.oldPlan.planSize);
                            var r = "";
                            "undefined" != typeof BELONG.dataLayer.event.oldPlan.contractOption && null != BELONG.dataLayer.event.oldPlan.contractOption && (r = BELONG.dataLayer.event.oldPlan.contractOption),
                            valToPush = "manage-service : current-plan",
                            n && (valToPush = valToPush + " : " + n),
                            i && (valToPush = valToPush + " : " + i),
                            r && (valToPush = valToPush + " : " + r),
                            a.push(valToPush)
                        } catch (s) {}
                        t.products = "";
                        try {
                            for (var o = 0; o < a.length; o++)
                                o > 0 && (t.products = t.products + ","),
                                t.products = t.products + ";" + a[o]
                        } catch (s) {}
                    },
                    addEvent: ["event58"]
                }]
            }]
        }, {
            name: "analytics-trackManageServiceReview",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar10: "%salesforce_id%",
                        eVar100: "analytics-trackManageServiceReview",
                        eVar16: "%customerId%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        pageName: "bl:account:manage-service:review-changes"
                    },
                    customSetup: function() {
                        var e = [];
                        try {
                            var t = "";
                            "undefined" != typeof BELONG.dataLayer.event.oldPlan.planType && null != BELONG.dataLayer.event.oldPlan.planType && (t = BELONG.dataLayer.event.oldPlan.planType);
                            var a = ""
                              , n = "";
                            "undefined" != typeof BELONG.dataLayer.event.updateAction.originalPlanSize && null != BELONG.dataLayer.event.updateAction.originalPlanSize && (a = BELONG.dataLayer.event.updateAction.originalPlanSize),
                            "undefined" != typeof BELONG.dataLayer.event.updateAction.newPlanSize && null != BELONG.dataLayer.event.updateAction.newPlanSize && (n = BELONG.dataLayer.event.updateAction.newPlanSize);
                            var i = ""
                              , r = "";
                            "undefined" != typeof BELONG.dataLayer.event.updateAction.originalPlanSpeed && null != BELONG.dataLayer.event.updateAction.originalPlanSpeed && (i = BELONG.dataLayer.event.updateAction.originalPlanSpeed),
                            "undefined" != typeof BELONG.dataLayer.event.updateAction.newPlanSpeed && null != BELONG.dataLayer.event.updateAction.newPlanSpeed && (r = BELONG.dataLayer.event.updateAction.newPlanSpeed);
                            var o = ""
                              , s = "";
                            "undefined" != typeof BELONG.dataLayer.event.updateAction.originalContractOption && null != BELONG.dataLayer.event.updateAction.originalContractOption && (o = BELONG.dataLayer.event.updateAction.originalContractOption),
                            "undefined" != typeof BELONG.dataLayer.event.updateAction.newContractOption && null != BELONG.dataLayer.event.updateAction.newContractOption && (s = BELONG.dataLayer.event.updateAction.newContractOption),
                            "undefined" != typeof BELONG.dataLayer.event.updateAction.ipType && null != BELONG.dataLayer.event.updateAction.ipType && 1 == typeof BELONG.dataLayer.event.updateAction.ipType.hasChanged && (originalIpType = BELONG.dataLayer.event.updateAction.ipType.originalIpType.toLowerCase(),
                            newIpType = BELONG.dataLayer.event.updateAction.ipType.newIpType.toLowerCase()),
                            valToPush = "manage-service",
                            t && (valToPush = valToPush + " : " + t),
                            a != n && (valToPush = valToPush + " : change planSize : from " + a + " : to " + n,
                            e.push(valToPush)),
                            valToPush = "manage-service",
                            t && (valToPush = valToPush + " : " + t),
                            i != r && (valToPush = valToPush + " : change planSpeed : from " + i + " : to " + r,
                            e.push(valToPush)),
                            valToPush = "manage-service",
                            t && (valToPush = valToPush + " : " + t),
                            o != s && (valToPush = valToPush + " : change contract : from " + o + " : to " + s,
                            e.push(valToPush)),
                            valToPush = "manage-service",
                            t && (valToPush = valToPush + " : " + t),
                            originalIpType != newIpType && (valToPush = valToPush + " : change contract : from " + originalIpType + " : to " + newIpType,
                            e.push(valToPush))
                        } catch (l) {}
                        s_products = "";
                        try {
                            for (var c = 0; c < e.length; c++)
                                c > 0 && (s_products += ","),
                                s_products = s_products + ";" + e[c]
                        } catch (l) {}
                    },
                    addEvent: ["event59"]
                }]
            }]
        }, {
            name: "analytics-trackManageServiceUpdate",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar10: "%salesforce_id%",
                        eVar100: "analytics-trackManageServiceUpdate",
                        eVar16: "%customerId%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop31: "Manage Service",
                        pageName: "bl:account:manage-service:update-complete"
                    },
                    customSetup: function(e, t) {
                        function a(e) {
                            return e.currentPlan ? !(!e.currentPlan || !e.currentPlan.planDetail) && e.currentPlan.planDetail : !(!e || !e.planDetail) && e.planDetail
                        }
                        function n(e) {
                            var t;
                            if (e.currentPlan) {
                                for (h = -0; h < e.currentPlan.recurringChargeList.length; h++)
                                    "CONTRACT_CREDIT" === e.currentPlan.recurringChargeList[h].recurringChargeType ? chargeType = e.currentPlan.recurringChargeList[h].contractOption : chargeType = "NA";
                                return t
                            }
                            for (h = -0; h < e.recurringChargeList.length; h++)
                                "CONTRACT_CREDIT" !== e.recurringChargeList[h].recurringChargeType || e.recurringChargeList[h].contractOption;
                            return t
                        }
                        function i(e) {
                            var t;
                            if (e.currentPlan) {
                                for (h = -0; h < e.currentPlan.recurringChargeList.length; h++)
                                    "STATIC_IP" !== e.currentPlan.recurringChargeList[h].recurringChargeType || e.currentPlan.recurringChargeList[h].recurringChargeType;
                                return t
                            }
                            for (h = -0; h < e.recurringChargeList.length; h++)
                                "STATIC_IP" !== e.recurringChargeList[h].recurringChargeType || e.recurringChargeList[h].recurringChargeType;
                            return t
                        }
                        var r = []
                          , o = (((BELONG || {}).dataLayer || {}).event || {}).oldPlan || {}
                          , s = (((BELONG || {}).dataLayer || {}).event || {}).newPlan || {};
                        t.eVar101 = JSON.stringify({
                            oldPlan: o
                        }),
                        t.eVar102 = JSON.stringify({
                            newPlan: s
                        });
                        try {
                            var c = a(o, "planType") || a(s, c)
                              , l = a(o, "planSize")
                              , u = a(s, "planSize")
                              , d = a(o, "planSpeed")
                              , g = a(s, "planSpeed")
                              , p = n(o)
                              , f = n(s);
                            planIpTypeOld = i(o),
                            planIpTypeNew = i(s);
                            var m = "manage-service";
                            c && (m = m + " : " + c),
                            l != u && (m = m + " : change planSize : from " + (l || "") + " : to " + (u || ""),
                            r.push(m));
                            m = "manage-service";
                            c && (m = m + " : " + c),
                            d != g && (m = m + " : change planSpeed : from " + (d || "") + " : to " + (g || ""),
                            r.push(m));
                            m = "manage-service";
                            c && (m = m + " : " + c),
                            p != f && (m = m + " : change contract : from " + (p || "") + " : to " + (f || ""),
                            r.push(m));
                            m = "manage-service";
                            c && (m = m + " : " + c),
                            planIpTypeOld != planIpTypeNew && (m = m + " : change IP type : from " + (planIpTypeOld || "") + " : to " + (planIpTypeNew || ""),
                            r.push(m))
                        } catch (v) {
                            t.eVar91 = s_tagErrors = "Product Change details:" + _satellite.getVar("tagError")(v)
                        }
                        s_products = "";
                        try {
                            for (var h = 0; h < r.length; h++)
                                h > 0 && (s_products += ","),
                                s_products = s_products + ";" + r[h]
                        } catch (v) {
                            t.eVar91 = s_tagErrors = "Loop through the array" + _satellite.getVar("tagError")(v)
                        }
                    },
                    addEvent: ["event12", "event9", "purchase"]
                }]
            }]
        }, {
            name: "analytics-trackNesSurveyClosed",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-trackNesSurveyClosed",
                        eVar26: "%eventCategory%:%eventAction%:%eventLabel%",
                        eVar27: "%pageName%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop27: "%pageName%"
                    }
                }]
            }]
        }, {
            name: "analytics-trackNesSurveySubmitted",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-trackNesSurveySubmitted",
                        eVar26: "%eventCategory%:%eventAction%:%eventLabel%",
                        eVar27: "%pageName%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop27: "%pageName%"
                    }
                }]
            }]
        }, {
            name: "analytics-trackOrderTrackingView",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar19: "%orderTrackingSaleType%",
                        eVar56: "%cardMoreInformation%",
                        eVar59: "%cardCoolingOff%",
                        eVar60: "%cardAppointment%",
                        eVar61: "%cardCallback%",
                        eVar62: "%cardConnect%",
                        eVar63: "%cardErrorDetails%",
                        eVar64: "%cardMessages%",
                        eVar65: "%cardModemDelivery%",
                        eVar66: "%cardOrder%",
                        eVar67: "%cardService%",
                        eVar68: "%cardProofOfOccupancyDocuments%",
                        eVar71: "%serviceType%"
                    }
                }]
            }]
        }, {
            name: "analytics-trackPaymentDetailsUpdate",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-trackPaymentDetailsUpdate",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel% %errorCode%",
                        prop27: "%pageName%",
                        pageName: "bl:account:payment-details:%eventLabel% %errorCode%"
                    }
                }]
            }]
        }, {
            name: "analytics-trackPaymentDetailsUpdateSuccess",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-trackPaymentDetailsUpdateSuccess",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel% success",
                        prop27: "%pageName%",
                        prop31: "%eventCategory%:%eventLabel%",
                        pageName: "bl:account:payment-details:%eventLabel% success"
                    },
                    customSetup: function(e, t) {
                        try {
                            var a = _satellite.getVar("eventLabel");
                            /^retry/i.test(a) && "object" == typeof t && "undefined" != typeof t.apl && (t.events = t.apl(t.events, "event37", ",", 1))
                        } catch (n) {}
                    },
                    addEvent: ["event12"]
                }]
            }]
        }, {
            name: "analytics-trackPaymentPrePaySuccess",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-trackPaymentPrePaySuccess",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel% success",
                        prop27: "%pageName%",
                        prop31: "%eventCategory%:%eventLabel% success",
                        pageName: "bl:account:payment-details:%eventLabel% success"
                    },
                    addEvent: ["event12", "event39"]
                }]
            }]
        }, {
            name: "analytics-trackPersonalDetailsUpdate",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-trackPersonalDetailsUpdate",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop27: "%pageName%",
                        prop31: "%eventCategory%",
                        pageName: "bl:account:personal-details:update %eventLabel%"
                    },
                    addEvent: ["event12"]
                }]
            }]
        }, {
            name: "analytics-trackProductBuilderAvailability",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "bl:product builder availability",
                    setVars: {
                        eVar100: "analytics-trackProductBuilderAvailability",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%"
                    }
                }]
            }]
        }, {
            name: "analytics-trackProductBuilderCloseSq",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "bl:sq:sqClose",
                    setVars: {
                        eVar100: "analytics-trackProductBuilderCloseSq",
                        eVar26: "D=c26",
                        eVar40: "%serviceClass%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop34: "%eventLabel% %sqProduct%",
                        pageName: "%basePageName%:sq:sqClose"
                    },
                    customSetup: function(e, t) {
                        var a = (((BELONG || {}).dataLayer || {}).event || {}).productModel || {}
                          , n = a.planType || "unknown"
                          , i = a.calculatedMTC + ".00" || "0"
                          , r = a.contractTerm || "unknown"
                          , o = a.planSize || "unknown"
                          , s = a.planSpeed || "unknown"
                          , c = "1"
                          , l = [n, o, s, r].join(" ");
                        t.products = [n, l, c, i].join(";"),
                        t.linkTrackEvents = "event4,prodView",
                        BELONG.dataLayer.productData = {
                            planType: n,
                            calculatedMTC: i,
                            contractTerm: r,
                            planSize: o,
                            planSpeed: s,
                            quantity: c
                        }
                    },
                    addEvent: ["event4", "prodView"]
                }]
            }]
        }, {
            name: "analytics-trackProductBuilderContractSelection",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "%eventCategory%:%eventAction%:%eventLabel%",
                    setVars: {
                        eVar100: "analytics-trackProductBuilderContractSelection",
                        eVar26: "%eventCategory%:%eventAction%:%eventLabel%",
                        eVar40: "%serviceClass%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop34: "%eventLabel% %sqProduct%",
                        pageName: "%basePageName%:sq:sqClose"
                    },
                    customSetup: function(e, t) {
                        var a = ((BELONG || {}).dataLayer || {}).event || {}
                          , n = ((BELONG || {}).dataLayer || {}).productData || {}
                          , i = (a.contractOption || "").toUpperCase() || "unknown"
                          , r = n.planType || "unknown"
                          , o = n.planSpeed || "unknown"
                          , s = n.planSize || "unknown"
                          , c = "1"
                          , l = $("span[data\\-bind$=tmc]").text() + ".00"
                          , u = [r, s, o, i].join(" ");
                        t.products = [r, u, c, l].join(";"),
                        t.linkTrackEvents = "event4,prodView",
                        BELONG.dataLayer.productData = {
                            planType: r,
                            calculatedMTC: l,
                            contractTerm: i,
                            planSize: s,
                            planSpeed: o,
                            quantity: c
                        }
                    },
                    addEvent: ["event4", "prodView"]
                }]
            }]
        }, {
            name: "analytics-trackProductBuilderJoin",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-trackProductBuilderJoin",
                        eVar99: "%dtmTriggerData%",
                        pageName: "%basePageName%:productBuilder:join"
                    },
                    addEvent: ["event30"]
                }]
            }, {
                command: "loadScript",
                arguments: [{
                    sequential: !0,
                    scripts: [{
                        src: "satellite-5b23595b64746d59b40048c4.js"
                    }]
                }]
            }]
        }, {
            name: "analytics-trackProductBuilderPromoCodeApplyFail",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-trackProductBuilderPromoCodeApplyFail",
                        eVar36: "%promoCodeProductBuilder%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        pageName: "%basePageName%:apply promo code error"
                    }
                }]
            }]
        }, {
            name: "analytics-trackProductBuilderPromoCodeApplySuccess",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-trackProductBuilderPromoCodeApplySuccess",
                        eVar36: "%promoCodeProductBuilder%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        pageName: "%basePageName%:apply promo code success"
                    },
                    addEvent: ["event42"]
                }]
            }]
        }, {
            name: "analytics-trackProductBuilderSpeedboostSelection",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "%eventCategory%:%eventAction%:%eventLabel%",
                    setVars: {
                        eVar100: "analytics-trackProductBuilderContractSelection",
                        eVar26: "%eventCategory%:%eventAction%:%eventLabel%",
                        eVar40: "%serviceClass%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop34: "%eventLabel% %sqProduct%",
                        pageName: "%basePageName%:sq:sqClose"
                    },
                    customSetup: function(e, t) {
                        var a = ((BELONG || {}).dataLayer || {}).event || {}
                          , n = ((BELONG || {}).dataLayer || {}).productData || {}
                          , i = a.speedboostOption || "unknown"
                          , r = n.planType || "unknown"
                          , o = n.contractTerm || "unknown"
                          , s = n.planSize || "unknown"
                          , c = "1"
                          , l = [r, s, i, o].join(" ")
                          , u = $("span[data\\-bind$=tmc]").text() + ".00";
                        t.products = [r, l, c, u].join(";"),
                        t.linkTrackEvents = "event4,prodView",
                        BELONG.dataLayer.productData = {
                            planType: r,
                            calculatedMTC: u,
                            contractTerm: o,
                            planSize: s,
                            planSpeed: i,
                            quantity: c
                        }
                    },
                    addEvent: ["event4", "prodView"]
                }]
            }]
        }, {
            name: "analytics-trackProofOfOccupancy",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "account:upload:proof of occupancy",
                    setVars: {
                        eVar100: "analytics-trackProofOfOccupancy",
                        eVar26: "%eventCategory%:%eventAction%:%eventLabel%",
                        eVar27: "%pageName%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop27: "%pageName%",
                        prop31: "%eventCategory%",
                        channel: "bl:account:upload"
                    },
                    customSetup: function(e, t) {
                        var a = {
                            DEFAULTS: {
                                category: "Proof-Of-Dwelling",
                                action: "click",
                                label: "proof of occupancy label not set",
                                error: ""
                            },
                            GET_POD_REQUEST: {
                                label: "",
                                action: ""
                            },
                            DOCUMENT_UPLOAD_SET_PRIMARY_FILE_TYPE: {
                                label: "Select primary file option"
                            },
                            DOCUMENT_UPLOAD_SET_SECONDARY_FILE_TYPE: {
                                label: "Select secondary file option"
                            },
                            DOCUMENT_UPLOAD_SET_NAME_MATCHES: {
                                label: "Check if name matches",
                                action: "validation"
                            },
                            DOCUMENT_UPLOAD_SET_PRIMARY_FILE: {
                                label: "Select primary file",
                                action: "file-select"
                            },
                            DOCUMENT_UPLOAD_SET_SECONDARY_FILE: {
                                label: "Select secondary file",
                                action: "file-select"
                            },
                            DOCUMENT_UPLOAD_ERROR_UNALLOWED_MIMETYPE_PRIMARY: {
                                label: "Primary file mime type error",
                                action: "file-error"
                            },
                            DOCUMENT_UPLOAD_ERROR_UNALLOWED_MIMETYPE_SECONDARY: {
                                label: "Secondary file mime type error",
                                action: "file-error"
                            },
                            DOCUMENT_UPLOAD_ERROR_FILE_SIZE_PRIMARY: {
                                label: "Primary file size error",
                                action: "file-error"
                            },
                            DOCUMENT_UPLOAD_ERROR_FILE_SIZE_SECONDARY: {
                                label: "Secondary file size error",
                                action: "file-error"
                            },
                            DOCUMENT_UPLOAD_FILE_COUNT_ERROR: {
                                label: "Primary file count error",
                                action: "file-error"
                            },
                            DOCUMENT_UPLOAD_MODAL_OPEN: {
                                label: "More information modal open"
                            },
                            DOCUMENT_UPLOAD_MODAL_CLOSE: {
                                label: "More information modal close"
                            },
                            DOCUMENT_UPLOAD_REMOVE_FILES: {
                                label: "Remove files"
                            },
                            DOCUMENT_UPLOAD_REMOVE_SECONDARY_FILES: {
                                label: "Remove secondary files"
                            },
                            DOCUMENT_UPLOAD_FILE_UPLOAD_COMPLETE: {
                                label: "Upload complete",
                                action: "file upload"
                            },
                            DOCUMENT_UPLOAD_UPDATE_PROGRESS: {
                                label: "Triggered to get the upload progress",
                                action: "file upload"
                            },
                            GET_POD_REQUEST: {
                                label: "Pod status",
                                action: "load"
                            },
                            DOCUMENT_UPLOAD_GET_POD_SUCCESS: {
                                label: "Pod success",
                                action: "load"
                            },
                            DOCUMENT_UPLOAD_GET_POD_FAIL: {
                                label: "Pod failure",
                                action: "load"
                            },
                            DOCUMENT_UPLOAD_ERROR: {
                                label: "Upload error",
                                action: "file upload"
                            }
                        }
                          , n = (e = ((BELONG || {}).dataLayer || {}).event || {}).type;
                        a[n] && (e.eventAction = a[n].action ? a[n].action : a.DEFAULTS.action,
                        e.eventLabel = a[n].label ? a[n].label : a.DEFAULTS.label,
                        e.eventCategory = a[n].category ? a[n].category : a.DEFAULTS.category,
                        e.errorCode = a[n].error ? a[n].error : a.DEFAULTS.error),
                        BELONG.dataLayer.event = e,
                        t.prop26 = [e.eventCategory, e.eventAction, e.eventLabel].join(":"),
                        t.eVar26 = "D=c26",
                        t.prop31 = e.eventCategory,
                        _satellite.notify("analytics-trackProofOfOccupancy custom page code", 3),
                        _satellite.custom_debugMode && console.debug("eventType:", n, BELONG.dataLayer.event)
                    },
                    addEvent: ["event12", "event4"]
                }]
            }]
        }, {
            name: "analytics-trackServiceType",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    setVars: {
                        eVar71: "%serviceType%",
                        eVar79: "%broadbandTechType%"
                    }
                }]
            }]
        }, {
            name: "analytics-trackTroubleshootingCards",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    type: "o",
                    linkName: "%eventCategory%:%eventAction%:%eventLabel%",
                    setVars: {
                        eVar10: "%salesforce_id%",
                        eVar100: "analytics-trackTroubleshootingCards",
                        eVar16: "%customerId%",
                        eVar18: "%customerType%",
                        pageName: "bl:account:troubleshooting:%planType%:cards:%nextCardId%"
                    }
                }]
            }]
        }, {
            name: "analytics-trackTroubleshootingContact",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    type: "o",
                    linkName: "%eventCategory%:%eventAction%:%eventLabel%",
                    setVars: {
                        eVar10: "%salesforce_id%",
                        eVar100: "analytics-trackTroubleshootingContact",
                        eVar16: "%customerId%",
                        eVar18: "%customerType%",
                        prop32: "%troubleshootingContactType%",
                        pageName: "bl:account:troubleshooting:contact:submit"
                    },
                    addEvent: ["event18"]
                }]
            }]
        }, {
            name: "analytics-trackVirtualPage",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    type: "o",
                    linkName: "%eventCategory%:%eventAction%:%eventLabel%",
                    setVars: {
                        eVar100: "analytics-trackVirtualPage",
                        eVar26: "%eventCategory%:%eventAction%:%eventLabel%",
                        eVar27: "%pageName%",
                        eVar55: "%utm_user_id%",
                        eVar74: "%eoiReason%",
                        eVar80: "%auth0id%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "D=v26",
                        prop27: "D=v27",
                        pageName: "%basePageName%:%virtualPage%"
                    }
                }]
            }]
        }, {
            name: "analytics-tracksqAddress",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-tracksqAddress",
                        eVar38: "%addressId%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        pageName: "%basePageName%:sq:sqSelectAddress"
                    }
                }]
            }]
        }, {
            name: "analytics-tracksqCheck",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-tracksqCheck",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop34: "%eventLabel%",
                        pageName: "%basePageName%:sq:sqCheck"
                    },
                    addEvent: ["event25"]
                }]
            }]
        }, {
            name: "analytics-tracksqFail",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-tracksqFail",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop34: "%eventLabel%",
                        pageName: "%basePageName%:sq:sqFail"
                    },
                    addEvent: ["event27"]
                }]
            }]
        }, {
            name: "analytics-tracksqGeneric",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-tracksqGeneric",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop34: "%eventLabel%",
                        pageName: "%basePageName%:sq:%eventCategory%"
                    }
                }]
            }]
        }, {
            name: "analytics-tracksqSuccess",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-tracksqSuccess",
                        eVar40: "%serviceClass%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop34: "%eventLabel% %sqProduct%",
                        pageName: "%basePageName%:sq:sqSuccess"
                    },
                    addEvent: ["event26"]
                }]
            }]
        }],
        settings: {
            trackInternalLinks: !0,
            libraryName: "satelliteLib-c6d69479c887822e961b4db59e3767c799f4e22f",
            isStaging: !1,
            allowGATTcalls: !1,
            downloadExtensions: /\.(?:doc|docx|eps|jpg|png|svg|xls|ppt|pptx|pdf|xlsx|tab|csv|zip|txt|vsd|vxd|xml|js|css|rar|exe|wma|mov|avi|wmv|mp3|wav|m4v)($|\&|\?)/i,
            notifications: !1,
            utilVisible: !1,
            domainList: ["belong.com.au"],
            scriptDir: "3c7a22bb879803cc2beb8b04f846f928b6fea2ec/scripts/",
            undefinedVarsReturnEmpty: !0,
            euCookieName: "bltrk",
            tagTimeout: 3e3
        },
        data: {
            URI: a.location.pathname + a.location.search,
            browser: {},
            cartItems: [],
            revenue: "",
            host: {
                http: "assets.adobedtm.com",
                https: "assets.adobedtm.com"
            }
        },
        dataElements: {
            addressId: {
                jsVariable: "BELONG.dataLayer.event.addressID",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            agentID: {
                customJS: function() {
                    var e = ""
                      , t = "";
                    return "undefined" != typeof jQuery && (agentidold = $("div#GlobalData").attr("data-login-email"),
                    "undefined" != typeof agentidold && agentidold && (t = agentidold.indexOf("@"),
                    e = agentidold.substring(t + 1, 45))),
                    e
                },
                storeLength: "pageview"
            },
            appSource: {
                queryParam: "source",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0,
                ignoreCase: 1
            },
            auth0id: {
                customJS: function() {
                    var e = _satellite.readCookie("userDetails") || "{}"
                      , t = JSON.parse(decodeURIComponent(e));
                    if ("string" == typeof t.userId && 24 === t.userId.length)
                        return t.userId
                },
                storeLength: "pageview"
            },
            basePageName: {
                customJS: function() {
                    var e = "belong";
                    return "undefined" != typeof s && "undefined" != typeof s.basePageName && s.basePageName ? e = s.basePageName : "undefined" != typeof s && "function" == typeof s.generateURLPageName && (e = "bl:" + s.generateURLPageName(a.location.pathname.split("/"))),
                    e
                },
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            broadbandTechType: {
                customJS: function() {
                    try {
                        var e = BELONG.dataLayer.broadbandTechType;
                        return _satellite.notify("Logged In page broadbandTechType: " + e),
                        e
                    } catch (t) {
                        return _satellite.notify("broadbandTechType dataElement error: " + e),
                        t
                    }
                },
                storeLength: "pageview"
            },
            cardAppointment: {
                customJS: function() {
                    try {
                        var e = BELONG.dataLayer.orderData.appointment
                          , t = "Card Status:" + e.cardStatus
                          , a = "Appointment Date:" + e.appointmentDate
                          , n = "Appointment Time:" + e.appointmentTime
                          , i = t + "|" + a + "|" + n;
                        return BELONG.dataLayer.cards = BELONG.dataLayer.cards || {},
                        BELONG.dataLayer.cards.appointment = {
                            cardValue: i,
                            cardStatus: t,
                            date: a,
                            time: n
                        },
                        _satellite.notify("Order Tracking - Appointment Card Data Found", 3),
                        i
                    } catch (r) {
                        return r
                    }
                },
                storeLength: "pageview"
            },
            cardCallback: {
                customJS: function() {
                    try {
                        var e = BELONG.dataLayer.orderData.callback
                          , t = "Card Status:" + e.cardStatus
                          , a = "Callback Request Status:" + e.callbackRequestStatus
                          , n = t + "|" + a;
                        return BELONG.dataLayer.cards = BELONG.dataLayer.cards || {},
                        BELONG.dataLayer.cards.callback = {
                            cardValue: n,
                            cardStatus: t,
                            callbackRequestStatus: a
                        },
                        _satellite.notify("Order Tracking - Callback Card Data Found", 3),
                        n
                    } catch (i) {
                        throw i
                    }
                },
                storeLength: "pageview"
            },
            cardConnect: {
                customJS: function() {
                    try {
                        var e = "Card Status:" + BELONG.dataLayer.orderData.connect.cardStatus;
                        return BELONG.dataLayer.cards = BELONG.dataLayer.cards || {},
                        BELONG.dataLayer.cards.connect = {
                            cardValue: e
                        },
                        _satellite.notify("Order Tracking - Connect Card Data Found", 3),
                        e
                    } catch (t) {
                        return t
                    }
                },
                storeLength: "pageview"
            },
            cardCoolingOff: {
                customJS: function() {
                    try {
                        var e = BELONG.dataLayer.orderData.coolingOff;
                        return "Card Status:" + e.cardStatus + "|Cooling Off Date:" + e.coolingOffDate
                    } catch (t) {
                        return t
                    }
                },
                storeLength: "pageview",
                cleanText: !0
            },
            cardErrorDetails: {
                customJS: function() {
                    try {
                        if ("object" != typeof t.JSON)
                            return "Browser doesn't support JSON";
                        var e = "Error Type:" + BELONG.dataLayer.orderData.errorType + "|" + JSON.stringify(BELONG.dataLayer.orderData.errors);
                        return _satellite.notify("Order Tracking - Error Card Data Found", 3),
                        e
                    } catch (a) {
                        return a
                    }
                },
                storeLength: "pageview"
            },
            cardId: {
                jsVariable: "BELONG.dataLayer.event.cardId",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            cardMessages: {
                customJS: function() {
                    try {
                        return "object" != typeof t.JSON ? "Browser doesn't support JSON" : JSON.stringify(BELONG.dataLayer.orderData.message)
                    } catch (e) {
                        return e
                    }
                },
                storeLength: "pageview"
            },
            cardModemDelivery: {
                customJS: function() {
                    try {
                        var e = BELONG.dataLayer.orderData.modemDelivery
                          , t = "Card Status:" + e.cardStatus
                          , a = "Modem Tracking Id:" + e.modemTrackingId
                          , n = t + "|" + a;
                        return BELONG.dataLayer.cards = BELONG.dataLayer.cards || {},
                        BELONG.dataLayer.cards.callback = {
                            cardValue: n,
                            cardStatus: t,
                            modemTrackingId: a
                        },
                        _satellite.notify("Order Tracking - Modem Delivery Card Data Found", 3),
                        n
                    } catch (i) {
                        return i
                    }
                },
                storeLength: "pageview"
            },
            cardMoreInformation: {
                customJS: function() {
                    try {
                        var e = "Card Status:" + BELONG.dataLayer.orderData.connect.moreInformation;
                        return BELONG.dataLayer.cards = BELONG.dataLayer.cards || {},
                        BELONG.dataLayer.cards.moreInformation = {
                            cardValue: e
                        },
                        _satellite.notify("Order Tracking - More Information Card Data Found", 3),
                        e
                    } catch (t) {
                        return t
                    }
                },
                storeLength: "pageview"
            },
            cardOrder: {
                customJS: function() {
                    try {
                        var e = BELONG.dataLayer.orderData.order
                          , t = "Card Status:" + e.cardStatus
                          , a = "Future Order Date:" + e.futureOrderDate
                          , n = "Service Added Date:" + e.serviceAddedDate
                          , i = t + "|" + a + "|" + n;
                        return BELONG.dataLayer.cards = BELONG.dataLayer.cards || {},
                        BELONG.dataLayer.cards.appointment = {
                            cardValue: i,
                            cardStatus: t,
                            futureDate: a,
                            serviceDate: n
                        },
                        _satellite.notify("Order Tracking - Order Card Data Found", 3),
                        i
                    } catch (r) {
                        return r
                    }
                },
                storeLength: "pageview"
            },
            cardProofOfOccupancyDocuments: {
                customJS: function() {
                    try {
                        var e = BELONG.dataLayer.orderData.orderTrackingPodStatus
                          , t = ["Card Status:" + e.cardStatus, "POD Complete Date:" + e.podCompleteDate, "POD Status:" + e.podStatus].join("|");
                        return BELONG.dataLayer.cards = BELONG.dataLayer.cards || {},
                        BELONG.dataLayer.cards.orderTrackingPodStatus = {
                            cardValue: t
                        },
                        _satellite.notify("Order Tracking - Connect Card Data Found", 3),
                        t
                    } catch (a) {
                        return a
                    }
                },
                storeLength: "pageview"
            },
            cardService: {
                customJS: function() {
                    try {
                        var e = BELONG.dataLayer.orderData.service
                          , t = "Card Status:" + e.cardStatus
                          , a = "Service Activation Date:" + e.serviceActivationDate
                          , n = "Service Activation Status:" + e.serviceActivationStatus
                          , i = t + "|" + a + "|" + n;
                        return BELONG.dataLayer.cards = BELONG.dataLayer.cards || {},
                        BELONG.dataLayer.cards.appointment = {
                            cardValue: i,
                            cardStatus: t,
                            activationDate: a,
                            activationStatus: n
                        },
                        _satellite.notify("Order Tracking - Service Card Data Found", 3),
                        i
                    } catch (r) {
                        return r
                    }
                },
                storeLength: "pageview"
            },
            "Contact Us Enquiry Type": {
                customJS: function() {
                    var e = "";
                    try {
                        e = $("#ticket_custom1").val()
                    } catch (t) {}
                    return e
                },
                storeLength: "pageview"
            },
            customerId: {
                customJS: function() {
                    var e = "";
                    return "undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && "undefined" != typeof BELONG.dataLayer.customerID && BELONG.dataLayer.customerID ? e = BELONG.dataLayer.customerID : "undefined" != typeof jQuery && (custidold = $("div#GlobalData").attr("data-utilibillid"),
                    "undefined" != typeof custidold && custidold && (e = custidold)),
                    e
                },
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            customerIdJoin: {
                customJS: function() {
                    var e = "";
                    return "undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && "undefined" != typeof BELONG.dataLayer.event && "undefined" != typeof BELONG.dataLayer.event.userid && BELONG.dataLayer.event.userid ? e = BELONG.dataLayer.event.userid : "undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && "undefined" != typeof BELONG.dataLayer.customerID && BELONG.dataLayer.customerID ? e = BELONG.dataLayer.customerID : "undefined" != typeof jQuery && (custidold = $("div#GlobalData").attr("data-utilibillid"),
                    "undefined" != typeof custidold && custidold && (e = custidold)),
                    e
                },
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            customerType: {
                customJS: function() {
                    var e = "";
                    try {
                        return void 0 === _satellite.getVar("isMobo") || "true" !== _satellite.getVar("isMobo") && !0 !== _satellite.getVar("isMobo") ? void 0 === _satellite.getVar("isAgent") || "true" !== _satellite.getVar("isAgent") && !0 !== _satellite.getVar("isAgent") ? void 0 === _satellite.getVar("isCustomer") || "true" !== _satellite.getVar("isCustomer") && !0 !== _satellite.getVar("isCustomer") || (e = "Customer") : e = "Agent" : e = "Mobo",
                        e
                    } catch (t) {
                        return e
                    }
                },
                storeLength: "pageview",
                cleanText: !0
            },
            deviceType: {
                customJS: function() {
                    var e = "";
                    try {
                        var t = _satellite.getVar("appSource");
                        t && (e = t + " : ")
                    } catch (a) {}
                    return e += _satellite.browserInfo.deviceType
                },
                storeLength: "pageview"
            },
            dtmTriggerData: {
                customJS: function() {
                    try {
                        BELONG.dataLayer.event;
                        return JSON.stringify(BELONG.dataLayer.event).replace(/\t|\n|\\t|\\n/g, "")
                    } catch (e) {
                        return ""
                    }
                },
                storeLength: "pageview",
                cleanText: !0
            },
            eoiReason: {
                jsVariable: "BELONG.dataLayer.event.eoiReason",
                storeLength: "pageview"
            },
            errorCode: {
                jsVariable: "BELONG.dataLayer.event.errorCode",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            eventAction: {
                jsVariable: "BELONG.dataLayer.event.eventAction",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            eventCategory: {
                jsVariable: "BELONG.dataLayer.event.eventCategory",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            eventLabel: {
                customJS: function() {
                    try {
                        var e = "";
                        return "undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && "undefined" != typeof BELONG.dataLayer.event && BELONG.dataLayer.event.eventLabel ? e = BELONG.dataLayer.event.eventLabel : "undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && "undefined" != typeof BELONG.dataLayer.event && BELONG.dataLayer.event.eventType && (e = BELONG.dataLayer.event.eventType),
                        _satellite.notify("eventLabel Data Element returns: " + e, 1),
                        e
                    } catch (t) {
                        return s.eVar91 = "eventLabel DE:" + _satellite.getVar("tagError")(t),
                        ""
                    }
                },
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            eventPage: {
                jsVariable: "BELONG.dataLayer.event.eventPage",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            eventType: {
                jsVariable: "BELONG.dataLayer.event.eventType",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            gclid: {
                queryParam: "gclid",
                storeLength: "pageview",
                ignoreCase: 1
            },
            hostname: {
                jsVariable: "location.hostname",
                storeLength: "pageview"
            },
            hotjarDomains: {
                customJS: function() {
                    return !0 === {
                        "www.belong.com.au": !0,
                        "login.belong.com.au": !0,
                        "support.belong.com.au": !0,
                        "mobile.belongprod.com.au": !0,
                        "belong.mooruptech.com.au": !0
                    }[location.hostname] ? "TRUE" : "FALSE"
                },
                "default": "FALSE",
                storeLength: "pageview"
            },
            hotjarUserId: {
                customJS: function() {
                    var e = t.hj || {}
                      , a = e.includedInSample
                      , n = "function" == typeof ((e || {}).globals || {}).get ? ((e || {}).globals || {}).get("userId") : "";
                    if (a && n)
                        return n.substr(0, 8)
                },
                storeLength: "pageview"
            },
            internalSearchString: {
                queryParam: "search",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0,
                ignoreCase: 1
            },
            isAgent: {
                customJS: function() {
                    var e = "false";
                    return "undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && "undefined" != typeof BELONG.dataLayer.isAgent && !0 === BELONG.dataLayer.isAgent && (e = "true"),
                    e
                },
                "default": "false",
                storeLength: "pageview"
            },
            isBelongApp: {
                customJS: function() {
                    try {
                        if ("undefined" == typeof s)
                            return;
                        var e = "FALSE";
                        if ("undefined" === _satellite.readCookie("uiIdiom")) {
                            var a = t.navigator.standalone
                              , n = navigator.userAgent.match(/iPhone|iPad|iPod/i)
                              , i = navigator.userAgent.match(/Android/i)
                              , r = /safari/.test(navigator.userAgent.toLowerCase())
                              , o = navigator.userAgent.match(/BelongApp\/[0-9\.]+$/);
                            return n ? a || r || (e = "TRUE") : i && o && (e = "TRUE"),
                            e
                        }
                        "" !== _satellite.readCookie("uiIdiom") && (e = "TRUE",
                        console.log("isApp tracked from cookie"))
                    } catch (c) {
                        console.log("uiIdiom app cookie read error")
                    }
                },
                "default": "FALSE",
                storeLength: "pageview"
            },
            isCustomer: {
                customJS: function() {
                    var e = "false";
                    return "undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && "undefined" != typeof BELONG.dataLayer.isCustomer && !0 === BELONG.dataLayer.isCustomer ? e = "true" : "undefined" != typeof jQuery && (isCustomerOld = $("div#GlobalData").attr("data-is-customer"),
                    "undefined" != typeof isCustomerOld && isCustomerOld && (e = isCustomerOld)),
                    e
                },
                "default": "false",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            isLoggedIn: {
                customJS: function() {
                    var e = "false";
                    return "undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && "undefined" != typeof BELONG.dataLayer.isLoggedIn && !0 === BELONG.dataLayer.isLoggedIn ? e = "true" : "undefined" != typeof jQuery && (isLoggedInOld = $("div#GlobalData").attr("data-is-logged-in"),
                    "undefined" != typeof isLoggedInOld && isLoggedInOld && (e = isLoggedInOld)),
                    e
                },
                "default": "false",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            isMobo: {
                customJS: function() {
                    var e = "false";
                    return "undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && "undefined" != typeof BELONG.dataLayer.isMobo && !0 === BELONG.dataLayer.isMobo ? e = "true" : "undefined" != typeof jQuery && (isMoboOld = $("div#GlobalData").attr("data-is-mobo"),
                    "undefined" != typeof isMoboOld && isMoboOld && (e = isMoboOld)),
                    e
                },
                "default": "false",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            liveAgentSessionId: {
                customJS: function() {
                    var e = function(e) {
                        var t = (a.cookie.match("(^|; )" + e + "=([^;]*)") || 0)[2];
                        return t ? decodeURIComponent(t) : ""
                    }("liveagent_sid") || "";
                    return _satellite.notify("Live Agent Session Id is " + e, 3),
                    e
                },
                storeLength: "pageview"
            },
            liveChatImage: {
                jsVariable: "BELONG.dataLayer.liveChatImage",
                "default": "unknown image",
                storeLength: "pageview",
                forceLowerCase: !0
            },
            loginPasswordErrorDisplayed: {
                customJS: function() {
                    if ("complete" === a.readyState && a.getElementById("notify-user"))
                        return "flex" === t.getComputedStyle(a.getElementById("notify-user"), null).getPropertyValue("display") ? "true" : "false"
                },
                "default": "false",
                storeLength: "pageview"
            },
            marketingCloudVisitorId: {
                customJS: function() {
                    var e = _satellite.getVisitorId().getMarketingCloudVisitorID();
                    if (e)
                        return e
                },
                "default": "D=mid",
                storeLength: "pageview"
            },
            metaDescription: {
                customJS: function() {
                    if (a.querySelector("meta[name=description]")) {
                        var e = a.querySelector("meta[name=description]").getAttribute("content");
                        return "string" == typeof e && "function" == typeof e.trim ? e.substring(0, 254).trim() : ""
                    }
                    return ""
                },
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            metaKeywords: {
                customJS: function() {
                    if (a.querySelector("meta[name=keywords]")) {
                        var e = a.querySelector("meta[name=keywords]").getAttribute("content");
                        return "string" == typeof e && "function" == typeof e.trim ? e.substring(0, 254).trim() : ""
                    }
                    return ""
                },
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            moorupDomains: {
                customJS: function() {
                    return !0 === {
                        "belong.mooruptech.com.au": !0,
                        "belongsalesplatform-env.cwwyphuvmz.ap-southeast-2.elasticbeanstalk.com": !0
                    }[location.hostname] ? "TRUE" : "FALSE"
                },
                "default": "FALSE",
                storeLength: "pageview"
            },
            moorupPageName: {
                customJS: function() {
                    return ("bl:moorup" + location.pathname.replace(/\//gi, ":")).replace(/\:$/, "")
                },
                "default": "bl:moorup",
                storeLength: "pageview"
            },
            nextCardId: {
                jsVariable: "BELONG.dataLayer.event.nextCardId",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            omd_tracking_code: {
                queryParam: "tc",
                storeLength: "pageview",
                ignoreCase: 1
            },
            orderTrackingSaleType: {
                jsVariable: "BELONG.dataLayer.orderData.saleType",
                storeLength: "pageview"
            },
            pageName: {
                jsVariable: "s.pageName",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            pathname: {
                jsVariable: "window.location.pathname",
                storeLength: "pageview"
            },
            plan: {
                jsVariable: "BELONG.dataLayer.event.plan",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            planType: {
                customJS: function() {
                    var e, n = a.querySelector(".join-product-summary__planType");
                    return "object" == typeof s && (e = s.get("BELONG.dataLayer.event.planType", t)),
                    void 0 === e && null !== n && (e = n ? n.innerText : ""),
                    e
                },
                storeLength: "pageview"
            },
            postCode: {
                customJS: function() {
                    try {
                        if ("undefined" != typeof Storage && /"servicePostcode":"(.+?)"/.test(sessionStorage.sqModel))
                            return sessionStorage.sqModel.match(/"servicePostcode":"(.+?)"/)[1]
                    } catch (e) {
                        _satellite.notify("Postcode Error: " + e, 5)
                    }
                },
                storeLength: "pageview"
            },
            promoCode: {
                jsVariable: "BELONG.dataLayer.event.promoCode",
                storeLength: "session",
                forceLowerCase: !0,
                cleanText: !0
            },
            promoCodeProductBuilder: {
                jsVariable: "BELONG.dataLayer.event.promocodeValidation.promotion.description",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            resetPasswordErrorDisplayed: {
                customJS: function() {
                    if ("complete" === a.readyState && a.getElementById("Email-error")) {
                        var e = a.getElementById("Email-error").innerText;
                        return _satellite.notify("DE_resetPassswordErrorDisplayed: " + e, 1),
                        e
                    }
                },
                "default": "false",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            rootHostname: {
                customJS: function() {
                    return /\.com$/.test(location.hostname) ? location.hostname.split(".").reverse().splice(0, 2).reverse().join(".") : location.hostname.split(".").reverse().splice(0, 3).reverse().join(".")
                },
                storeLength: "pageview"
            },
            salesforce_id: {
                customJS: function() {
                    var e = _satellite.readCookie("userDetails") || "{}"
                      , t = JSON.parse(decodeURIComponent(e));
                    if ("string" == typeof t.userId && t.userId.length < 24)
                        return t.userId
                },
                storeLength: "pageview"
            },
            serviceClass: {
                jsVariable: "BELONG.dataLayer.event.serviceClass",
                storeLength: "pageview"
            },
            serviceType: {
                customJS: function() {
                    var e, a, i = ((t.BELONG || {}).dataLayer || {}).serviceType, r = ((t.BELONG || {}).dataLayer || {}).orderData || {}, o = ((t.BELONG || {}).dataLayer || {}).productData || {}, s = t.sessionStorage ? t.sessionStorage.getItem("product") : n, c = _satellite.readCookie("s_v71");
                    return i ? (e = i,
                    a = "serviceType") : r.serviceType ? (e = r.serviceType,
                    a = "orderData.serviceType") : o.serviceType ? (e = r.serviceType,
                    a = "orderData.serviceType") : s ? (e = s,
                    a = "sessionStorage") : c && (e = c,
                    a = "serviceTypeCookie"),
                    e ? (_satellite.setCookie("s_v71", e, 45),
                    _satellite.notify("serviceType " + e + " found in " + a + " and written to s_v71 cookie", 1)) : !e && _satellite.settings.notifications && console.log("serviceType not found", {
                        serviceType: i,
                        orderData: r,
                        productData: o,
                        serviceTypeCookie: c,
                        sessionStorageProduct: s
                    }),
                    e
                },
                storeLength: "pageview"
            },
            sqAttempt: {
                jsVariable: "BELONG.dataLayer.event.sqAttempt",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            sqProduct: {
                customJS: function() {
                    var e = "";
                    try {
                        sqProductTypeCheck = BELONG.dataLayer.event.product,
                        sqProductTypeCheck && "adsl" != sqProductTypeCheck.toLowerCase() && "nbn" != sqProductTypeCheck.toLowerCase() && (e = "[" + sqProductTypeCheck + "]")
                    } catch (t) {}
                    return e
                },
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            sqTechType: {
                customJS: function() {
                    try {
                        var e = "";
                        return "undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && "undefined" != typeof BELONG.dataLayer.event && BELONG.dataLayer.event.sqTechType && (e = BELONG.dataLayer.event.sqTechType),
                        e
                    } catch (t) {
                        return ""
                    }
                },
                storeLength: "pageview"
            },
            state: {
                customJS: function() {
                    try {
                        if ("undefined" != typeof Storage && /"serviceState":"(.+?)"/.test(sessionStorage.sqModel))
                            return sessionStorage.sqModel.match(/"serviceState":"(.+?)"/)[1]
                    } catch (e) {
                        _satellite.notify("State Error: " + e, 5)
                    }
                },
                storeLength: "pageview"
            },
            subDivision: {
                "default": "broadband",
                storeLength: "pageview"
            },
            tagError: {
                customJS: function() {
                    return function(e) {
                        return e.stack ? e.stack.replace(/\n|\t|\s/g, " ") : e.name && e.message ? "[" + e.name + "] " + e.message.toString() : e
                    }
                },
                storeLength: "pageview"
            },
            totalMinCost: {
                jsVariable: "BELONG.dataLayer.totalMinCost",
                storeLength: "pageview"
            },
            troubleshootingContactType: {
                customJS: function() {
                    var e = "";
                    try {
                        e = BELONG.dataLayer.event.contactType
                    } catch (a) {}
                    try {
                        var t = BELONG.dataLayer.event.issue;
                        void 0 !== t && t && (e = e + " : " + t)
                    } catch (a) {}
                    return e && (e = "troubleshooting : " + e),
                    e
                },
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            url: {
                customJS: function() {
                    return /email|username|password/i.test(location.search) ? decodeURIComponent(location.href).replace(/(email|username|password)=[^&]*(&?)/g, "$1=*******$2") : location.href
                },
                storeLength: "pageview"
            },
            utm_campaign: {
                customJS: function() {
                    var e = "utm_campaign";
                    if (_satellite.getQueryParam(e))
                        return _satellite.getQueryParam(e);
                    if (_satellite.getQueryParam("goto")) {
                        var t = decodeURIComponent(_satellite.getQueryParam("goto"));
                        if (a = _satellite.parseQueryParams(t)[e])
                            return a
                    } else {
                        if (!location.hash)
                            return !1;
                        var a, n = decodeURIComponent(location.hash);
                        if (a = _satellite.parseQueryParams(n)[e])
                            return a
                    }
                },
                storeLength: "pageview"
            },
            utm_content: {
                customJS: function() {
                    var e = "utm_content";
                    if (_satellite.getQueryParam(e))
                        return _satellite.getQueryParam(e);
                    if (_satellite.getQueryParam("goto")) {
                        var t = decodeURIComponent(_satellite.getQueryParam("goto"));
                        if (a = _satellite.parseQueryParams(t)[e])
                            return a
                    } else {
                        if (!location.hash)
                            return !1;
                        var a, n = decodeURIComponent(location.hash);
                        if (a = _satellite.parseQueryParams(n)[e])
                            return a
                    }
                },
                storeLength: "pageview"
            },
            utm_medium: {
                customJS: function() {
                    var e = "utm_medium";
                    if (_satellite.getQueryParam(e))
                        return _satellite.getQueryParam(e);
                    if (_satellite.getQueryParam("goto")) {
                        var t = decodeURIComponent(_satellite.getQueryParam("goto"));
                        if (a = _satellite.parseQueryParams(t)[e])
                            return a
                    } else {
                        if (!location.hash)
                            return !1;
                        var a, n = decodeURIComponent(location.hash);
                        if (a = _satellite.parseQueryParams(n)[e])
                            return a
                    }
                },
                storeLength: "pageview"
            },
            utm_source: {
                customJS: function() {
                    var e = "utm_source";
                    if (_satellite.getQueryParam(e))
                        return _satellite.getQueryParam(e);
                    if (_satellite.getQueryParam("goto")) {
                        var t = decodeURIComponent(_satellite.getQueryParam("goto"));
                        if (a = _satellite.parseQueryParams(t)[e])
                            return a
                    } else {
                        if (!location.hash)
                            return !1;
                        var a, n = decodeURIComponent(location.hash);
                        if (a = _satellite.parseQueryParams(n)[e])
                            return a
                    }
                },
                storeLength: "pageview"
            },
            utm_term: {
                customJS: function() {
                    var e = "utm_term";
                    if (_satellite.getQueryParam(e))
                        return _satellite.getQueryParam(e);
                    if (_satellite.getQueryParam("goto")) {
                        var t = decodeURIComponent(_satellite.getQueryParam("goto"));
                        if (a = _satellite.parseQueryParams(t)[e])
                            return a
                    } else {
                        if (!location.hash)
                            return !1;
                        var a, n = decodeURIComponent(location.hash);
                        if (a = _satellite.parseQueryParams(n)[e])
                            return a
                    }
                },
                storeLength: "pageview"
            },
            utm_user_id: {
                queryParam: "utm_userid",
                storeLength: "pageview",
                ignoreCase: 1
            },
            virtualPage: {
                customJS: function() {
                    try {
                        var e = BELONG.dataLayer.event.virtualPage;
                        return e = (e = (e = (e = e.replace(/\//g, ":")).replace(/:virtual:/g, "")).replace(/\.html/g, "")).replace(/\.htm/g, "")
                    } catch (t) {
                        return ""
                    }
                },
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            }
        },
        appVersion: "7QN",
        buildDate: "2020-04-08 22:55:03 UTC",
        publishDate: "2020-04-08 22:55:02 UTC"
    })
}(window, document);
