/**
 * @license
 * Adobe Visitor API for JavaScript version: 4.4.1
 * Copyright 2019 Adobe, Inc. All Rights Reserved
 * More info available at https://marketing.adobe.com/resources/help/en_US/mcvid/
 */
var e = function() {
    "use strict";
    function e(t) {
        return (e = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
            return typeof e
        }
        : function(e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        }
        )(t)
    }
    function t(e, t, n) {
        return t in e ? Object.defineProperty(e, t, {
            value: n,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : e[t] = n,
        e
    }
    function n() {
        return {
            callbacks: {},
            add: function(e, t) {
                this.callbacks[e] = this.callbacks[e] || [];
                var n = this.callbacks[e].push(t) - 1
                  , a = this;
                return function() {
                    a.callbacks[e].splice(n, 1)
                }
            },
            execute: function(e, t) {
                if (this.callbacks[e]) {
                    t = (t = void 0 === t ? [] : t)instanceof Array ? t : [t];
                    try {
                        for (; this.callbacks[e].length; ) {
                            var n = this.callbacks[e].shift();
                            "function" == typeof n ? n.apply(null, t) : n instanceof Array && n[1].apply(n[0], t)
                        }
                        delete this.callbacks[e]
                    } catch (e) {}
                }
            },
            executeAll: function(e, t) {
                (t || e && !N.isObjectEmpty(e)) && Object.keys(this.callbacks).forEach(function(t) {
                    var n = void 0 !== e[t] ? e[t] : "";
                    this.execute(t, n)
                }, this)
            },
            hasCallbacks: function() {
                return Boolean(Object.keys(this.callbacks).length)
            }
        }
    }
    function a(e, t, n) {
        var a = null == e ? void 0 : e[t];
        return void 0 === a ? n : a
    }
    function r(e) {
        for (var t = /^\d+$/, n = 0, a = e.length; n < a; n++)
            if (!t.test(e[n]))
                return !1;
        return !0
    }
    function i(e, t) {
        for (; e.length < t.length; )
            e.push("0");
        for (; t.length < e.length; )
            t.push("0")
    }
    function o(e, t) {
        for (var n = 0; n < e.length; n++) {
            var a = parseInt(e[n], 10)
              , r = parseInt(t[n], 10);
            if (a > r)
                return 1;
            if (r > a)
                return -1
        }
        return 0
    }
    function s(e, t) {
        if (e === t)
            return 0;
        var n = e.toString().split(".")
          , a = t.toString().split(".");
        return r(n.concat(a)) ? (i(n, a),
        o(n, a)) : NaN
    }
    function c(e) {
        return e === Object(e) && 0 === Object.keys(e).length
    }
    function l(e) {
        return "function" == typeof e || e instanceof Array && e.length
    }
    function u() {
        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : ""
          , t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : function() {
            return !0
        }
        ;
        this.log = de("log", e, t),
        this.warn = de("warn", e, t),
        this.error = de("error", e, t)
    }
    function d() {
        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}
          , t = e.isEnabled
          , n = e.cookieName
          , a = (arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {}).cookies;
        return t && n && a ? {
            remove: function() {
                a.remove(n)
            },
            get: function() {
                var e = a.get(n)
                  , t = {};
                try {
                    t = JSON.parse(e)
                } catch (e) {
                    t = {}
                }
                return t
            },
            set: function(e, t) {
                t = t || {},
                a.set(n, JSON.stringify(e), {
                    domain: t.optInCookieDomain || "",
                    cookieLifetime: t.optInStorageExpiry || 3419e4,
                    expires: !0
                })
            }
        } : {
            get: _e,
            set: _e,
            remove: _e
        }
    }
    function p(e) {
        this.name = this.constructor.name,
        this.message = e,
        "function" == typeof Error.captureStackTrace ? Error.captureStackTrace(this, this.constructor) : this.stack = new Error(e).stack
    }
    function g() {
        function e(e, t) {
            var n = fe(e);
            return n.length ? n.every(function(e) {
                return !!t[e]
            }) : me(t)
        }
        function t() {
            A(D),
            P(ae.COMPLETE),
            C(L.status, L.permissions),
            b.set(L.permissions, {
                optInCookieDomain: c,
                optInStorageExpiry: l
            }),
            S.execute(Pe)
        }
        function n(e) {
            return function(n, a) {
                if (!he(n))
                    throw new Error("[OptIn] Invalid category(-ies). Please use the `OptIn.Categories` enum.");
                return P(ae.CHANGED),
                Object.assign(D, ve(fe(n), e)),
                a || t(),
                L
            }
        }
        var a = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}
          , r = a.doesOptInApply
          , i = a.previousPermissions
          , o = a.preOptInApprovals
          , s = a.isOptInStorageEnabled
          , c = a.optInCookieDomain
          , l = a.optInStorageExpiry
          , u = a.isIabContext
          , p = (arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {}).cookies
          , g = ke(i);
        Te(g, "Invalid `previousPermissions`!"),
        Te(o, "Invalid `preOptInApprovals`!");
        var f, m, h, v, y, b = d({
            isEnabled: !!s,
            cookieName: "adobeujs-optin"
        }, {
            cookies: p
        }), L = this, C = ne(L), S = ce(), E = Le(g), _ = Le(o), k = b.get(), T = {}, O = (y = k,
        Ce(E) || y && Ce(y) ? ae.COMPLETE : ae.PENDING), I = (f = _,
        m = E,
        h = k,
        v = ve(se, !r),
        r ? Object.assign({}, v, f, m, h) : v), D = ye(I), P = function(e) {
            return O = e
        }, A = function(e) {
            return I = e
        };
        L.deny = n(!1),
        L.approve = n(!0),
        L.denyAll = L.deny.bind(L, se),
        L.approveAll = L.approve.bind(L, se),
        L.isApproved = function(t) {
            return e(t, L.permissions)
        }
        ,
        L.isPreApproved = function(t) {
            return e(t, _)
        }
        ,
        L.fetchPermissions = function(e) {
            var t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1]
              , n = t ? L.on(ae.COMPLETE, e) : _e;
            return !r || r && L.isComplete || o ? e(L.permissions) : t || S.add(Pe, function() {
                return e(L.permissions)
            }),
            n
        }
        ,
        L.complete = function() {
            L.status === ae.CHANGED && t()
        }
        ,
        L.registerPlugin = function(e) {
            if (!e || !e.name || "function" != typeof e.onRegister)
                throw new Error(Ae);
            T[e.name] || (T[e.name] = e,
            e.onRegister.call(e, L))
        }
        ,
        L.execute = De(T),
        Object.defineProperties(L, {
            permissions: {
                get: function() {
                    return I
                }
            },
            status: {
                get: function() {
                    return O
                }
            },
            Categories: {
                get: function() {
                    return re
                }
            },
            doesOptInApply: {
                get: function() {
                    return !!r
                }
            },
            isPending: {
                get: function() {
                    return L.status === ae.PENDING
                }
            },
            isComplete: {
                get: function() {
                    return L.status === ae.COMPLETE
                }
            },
            __plugins: {
                get: function() {
                    return Object.keys(T)
                }
            },
            isIabContext: {
                get: function() {
                    return u
                }
            }
        })
    }
    function f(e, t) {
        function n() {
            r = null,
            e.call(e, new p("The call took longer than you wanted!"))
        }
        function a() {
            r && (clearTimeout(r),
            e.apply(e, arguments))
        }
        if (void 0 === t)
            return e;
        var r = setTimeout(n, t);
        return a
    }
    function m() {
        if (window.__cmp)
            return window.__cmp;
        var e = window;
        if (e !== window.top) {
            for (var t; !t; ) {
                e = e.parent;
                try {
                    e.frames.__cmpLocator && (t = e)
                } catch (e) {}
                if (e === window.top)
                    break
            }
            if (t) {
                var n = {};
                return window.__cmp = function(e, a, r) {
                    var i = Math.random() + ""
                      , o = {
                        __cmpCall: {
                            command: e,
                            parameter: a,
                            callId: i
                        }
                    };
                    n[i] = r,
                    t.postMessage(o, "*")
                }
                ,
                window.addEventListener("message", function(e) {
                    var t = e.data;
                    if ("string" == typeof t)
                        try {
                            t = JSON.parse(e.data)
                        } catch (e) {}
                    if (t.__cmpReturn) {
                        var a = t.__cmpReturn;
                        n[a.callId] && (n[a.callId](a.returnValue, a.success),
                        delete n[a.callId])
                    }
                }, !1),
                window.__cmp
            }
            pe.error("__cmp not found")
        } else
            pe.error("__cmp not found")
    }
    function h() {
        var e = this;
        e.name = "iabPlugin",
        e.version = "0.0.1";
        var t = ce()
          , n = {
            allConsentData: null
        }
          , a = function(e) {
            var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
            return n[e] = t
        };
        e.fetchConsentData = function(e) {
            var t = f(e.callback, e.timeout);
            r({
                callback: t
            })
        }
        ,
        e.isApproved = function(e) {
            var t = e.callback
              , a = e.category
              , i = e.timeout;
            if (n.allConsentData)
                return t(null, s(a, n.allConsentData.vendorConsents, n.allConsentData.purposeConsents));
            var o = f(function(e) {
                var n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {}
                  , r = n.vendorConsents
                  , i = n.purposeConsents;
                t(e, s(a, r, i))
            }, i);
            r({
                category: a,
                callback: o
            })
        }
        ,
        e.onRegister = function(t) {
            var n = Object.keys(ie)
              , a = function(e) {
                var a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {}
                  , r = a.purposeConsents
                  , i = a.gdprApplies
                  , o = a.vendorConsents;
                !e && i && o && r && (n.forEach(function(e) {
                    var n = s(e, o, r);
                    t[n ? "approve" : "deny"](e, !0)
                }),
                t.complete())
            };
            e.fetchConsentData({
                callback: a
            })
        }
        ;
        var r = function(e) {
            var r = e.callback;
            if (n.allConsentData)
                return r(null, n.allConsentData);
            t.add("FETCH_CONSENT_DATA", r);
            var s = {};
            o(function() {
                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}
                  , r = e.purposeConsents
                  , o = e.gdprApplies
                  , c = e.vendorConsents;
                (arguments.length > 1 ? arguments[1] : void 0) && a("allConsentData", s = {
                    purposeConsents: r,
                    gdprApplies: o,
                    vendorConsents: c
                }),
                i(function() {
                    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                    (arguments.length > 1 ? arguments[1] : void 0) && (s.consentString = e.consentData,
                    a("allConsentData", s)),
                    t.execute("FETCH_CONSENT_DATA", [null, n.allConsentData])
                })
            })
        }
          , i = function(e) {
            var t = m();
            t && t("getConsentData", null, e)
        }
          , o = function(e) {
            var t = Ie(ie)
              , n = m();
            n && n("getVendorConsents", t, e)
        }
          , s = function(e) {
            var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {}
              , n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {};
            return !!t[ie[e]] && oe[e].every(function(e) {
                return n[e]
            })
        }
    }
    var v = "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : {};
    Object.assign = Object.assign || function(e) {
        for (var t, n, a = 1; a < arguments.length; ++a)
            for (t in n = arguments[a])
                Object.prototype.hasOwnProperty.call(n, t) && (e[t] = n[t]);
        return e
    }
    ;
    var y, b, L, C, S, E = {
        MESSAGES: {
            HANDSHAKE: "HANDSHAKE",
            GETSTATE: "GETSTATE",
            PARENTSTATE: "PARENTSTATE"
        },
        STATE_KEYS_MAP: {
            MCMID: "MCMID",
            MCAID: "MCAID",
            MCAAMB: "MCAAMB",
            MCAAMLH: "MCAAMLH",
            MCOPTOUT: "MCOPTOUT",
            CUSTOMERIDS: "CUSTOMERIDS"
        },
        ASYNC_API_MAP: {
            MCMID: "getMarketingCloudVisitorID",
            MCAID: "getAnalyticsVisitorID",
            MCAAMB: "getAudienceManagerBlob",
            MCAAMLH: "getAudienceManagerLocationHint",
            MCOPTOUT: "isOptedOut",
            ALLFIELDS: "getVisitorValues"
        },
        SYNC_API_MAP: {
            CUSTOMERIDS: "getCustomerIDs"
        },
        ALL_APIS: {
            MCMID: "getMarketingCloudVisitorID",
            MCAAMB: "getAudienceManagerBlob",
            MCAAMLH: "getAudienceManagerLocationHint",
            MCOPTOUT: "isOptedOut",
            MCAID: "getAnalyticsVisitorID",
            CUSTOMERIDS: "getCustomerIDs",
            ALLFIELDS: "getVisitorValues"
        },
        FIELDGROUP_TO_FIELD: {
            MC: "MCMID",
            A: "MCAID",
            AAM: "MCAAMB"
        },
        FIELDS: {
            MCMID: "MCMID",
            MCOPTOUT: "MCOPTOUT",
            MCAID: "MCAID",
            MCAAMLH: "MCAAMLH",
            MCAAMB: "MCAAMB"
        },
        AUTH_STATE: {
            UNKNOWN: 0,
            AUTHENTICATED: 1,
            LOGGED_OUT: 2
        },
        OPT_OUT: {
            GLOBAL: "global"
        }
    }, _ = E.STATE_KEYS_MAP, k = function(e) {
        function t() {}
        function n(t, n) {
            var a = this;
            return function() {
                var r = e(0, t)
                  , i = {};
                return i[t] = r,
                a.setStateAndPublish(i),
                n(r),
                r
            }
        }
        this.getMarketingCloudVisitorID = function(e) {
            e = e || t;
            var a = this.findField(_.MCMID, e)
              , r = n.call(this, _.MCMID, e);
            return void 0 !== a ? a : r()
        }
        ,
        this.getVisitorValues = function(e) {
            this.getMarketingCloudVisitorID(function(t) {
                e({
                    MCMID: t
                })
            })
        }
    }, T = E.MESSAGES, O = E.ASYNC_API_MAP, I = E.SYNC_API_MAP, D = function() {
        function e() {}
        function t(e, t) {
            var n = this;
            return function() {
                return n.callbackRegistry.add(e, t),
                n.messageParent(T.GETSTATE),
                ""
            }
        }
        function n(n) {
            this[O[n]] = function(a) {
                a = a || e;
                var r = this.findField(n, a)
                  , i = t.call(this, n, a);
                return void 0 !== r ? r : i()
            }
        }
        function a(t) {
            this[I[t]] = function() {
                return this.findField(t, e) || {}
            }
        }
        Object.keys(O).forEach(n, this),
        Object.keys(I).forEach(a, this)
    }, P = E.ASYNC_API_MAP, A = function() {
        Object.keys(P).forEach(function(e) {
            this[P[e]] = function(t) {
                this.callbackRegistry.add(e, t)
            }
        }, this)
    }, N = ((C = (S = {
        exports: {}
    }).exports).isObjectEmpty = function(e) {
        return e === Object(e) && 0 === Object.keys(e).length
    }
    ,
    C.isValueEmpty = function(e) {
        return "" === e || C.isObjectEmpty(e)
    }
    ,
    C.getIeVersion = function() {
        if (document.documentMode)
            return document.documentMode;
        for (var e = 7; e > 4; e--) {
            var t = document.createElement("div");
            if (t.innerHTML = "<!--[if IE " + e + "]><span></span><![endif]-->",
            t.getElementsByTagName("span").length)
                return t = null,
                e;
            t = null
        }
        return null
    }
    ,
    C.encodeAndBuildRequest = function(e, t) {
        return e.map(encodeURIComponent).join(t)
    }
    ,
    C.isObject = function(t) {
        return null !== t && "object" === e(t) && !1 === Array.isArray(t)
    }
    ,
    C.defineGlobalNamespace = function() {
        return window.adobe = C.isObject(window.adobe) ? window.adobe : {},
        window.adobe
    }
    ,
    C.pluck = function(e, t) {
        return t.reduce(function(t, n) {
            return e[n] && (t[n] = e[n]),
            t
        }, Object.create(null))
    }
    ,
    C.parseOptOut = function(e, t, n) {
        t || (t = n,
        e.d_optout && e.d_optout instanceof Array && (t = e.d_optout.join(",")));
        var a = parseInt(e.d_ottl, 10);
        return isNaN(a) && (a = 7200),
        {
            optOut: t,
            d_ottl: a
        }
    }
    ,
    void (C.normalizeBoolean = function(e) {
        var t = e;
        return "true" === e ? t = !0 : "false" === e && (t = !1),
        t
    }
    ),
    S.exports), w = (N.isObjectEmpty,
    N.isValueEmpty,
    N.getIeVersion,
    N.encodeAndBuildRequest,
    N.isObject,
    N.defineGlobalNamespace,
    N.pluck,
    N.parseOptOut,
    N.normalizeBoolean,
    n), V = E.MESSAGES, M = {
        0: "prefix",
        1: "orgID",
        2: "state"
    }, B = function(e, t) {
        this.parse = function(e) {
            try {
                var t = {};
                return e.data.split("|").forEach(function(e, n) {
                    void 0 !== e && (t[M[n]] = 2 !== n ? e : JSON.parse(e))
                }),
                t
            } catch (e) {}
        }
        ,
        this.isInvalid = function(n) {
            var a = this.parse(n);
            if (!a || Object.keys(a).length < 2)
                return !0;
            var r = e !== a.orgID
              , i = !t || n.origin !== t
              , o = -1 === Object.keys(V).indexOf(a.prefix);
            return r || i || o
        }
        ,
        this.send = function(n, a, r) {
            var i = a + "|" + e;
            r && r === Object(r) && (i += "|" + JSON.stringify(r));
            try {
                n.postMessage(i, t)
            } catch (e) {}
        }
    }, R = E.MESSAGES, x = function(e, t, n, a) {
        function r(e) {
            Object.assign(g, e)
        }
        function i(e) {
            Object.assign(g.state, e),
            Object.assign(g.state.ALLFIELDS, e),
            g.callbackRegistry.executeAll(g.state)
        }
        function o(e) {
            if (!h.isInvalid(e)) {
                m = !1;
                var t = h.parse(e);
                g.setStateAndPublish(t.state)
            }
        }
        function s(e) {
            !m && f && (m = !0,
            h.send(a, e))
        }
        function c() {
            r(new k(n._generateID)),
            g.getMarketingCloudVisitorID(),
            g.callbackRegistry.executeAll(g.state, !0),
            v.removeEventListener("message", l)
        }
        function l(e) {
            if (!h.isInvalid(e)) {
                var t = h.parse(e);
                m = !1,
                v.clearTimeout(g._handshakeTimeout),
                v.removeEventListener("message", l),
                r(new D(g)),
                v.addEventListener("message", o),
                g.setStateAndPublish(t.state),
                g.callbackRegistry.hasCallbacks() && s(R.GETSTATE)
            }
        }
        function u() {
            f && postMessage ? (v.addEventListener("message", l),
            s(R.HANDSHAKE),
            g._handshakeTimeout = setTimeout(c, 250)) : c()
        }
        function d() {
            v.s_c_in || (v.s_c_il = [],
            v.s_c_in = 0),
            g._c = "Visitor",
            g._il = v.s_c_il,
            g._in = v.s_c_in,
            g._il[g._in] = g,
            v.s_c_in++
        }
        function p() {
            function e(e) {
                0 !== e.indexOf("_") && "function" == typeof n[e] && (g[e] = function() {}
                )
            }
            Object.keys(n).forEach(e),
            g.getSupplementalDataID = n.getSupplementalDataID,
            g.isAllowed = function() {
                return !0
            }
        }
        var g = this
          , f = t.whitelistParentDomain;
        g.state = {
            ALLFIELDS: {}
        },
        g.version = n.version,
        g.marketingCloudOrgID = e,
        g.cookieDomain = n.cookieDomain || "",
        g._instanceType = "child";
        var m = !1
          , h = new B(e,f);
        g.callbackRegistry = w(),
        g.init = function() {
            d(),
            p(),
            r(new A(g)),
            u()
        }
        ,
        g.findField = function(e, t) {
            if (void 0 !== g.state[e])
                return t(g.state[e]),
                g.state[e]
        }
        ,
        g.messageParent = s,
        g.setStateAndPublish = i
    }, G = E.MESSAGES, F = E.ALL_APIS, U = E.ASYNC_API_MAP, j = E.FIELDGROUP_TO_FIELD, H = function(e, t) {
        function n() {
            var t = {};
            return Object.keys(F).forEach(function(n) {
                var a = F[n]
                  , r = e[a]();
                N.isValueEmpty(r) || (t[n] = r)
            }),
            t
        }
        function a() {
            var t = [];
            return e._loading && Object.keys(e._loading).forEach(function(n) {
                if (e._loading[n]) {
                    var a = j[n];
                    t.push(a)
                }
            }),
            t.length ? t : null
        }
        function r(t) {
            return function n() {
                var r = a();
                if (r) {
                    var i = U[r[0]];
                    e[i](n, !0)
                } else
                    t()
            }
        }
        function i(e, a) {
            var r = n();
            t.send(e, a, r)
        }
        function o(e) {
            c(e),
            i(e, G.HANDSHAKE)
        }
        function s(e) {
            r(function() {
                i(e, G.PARENTSTATE)
            })()
        }
        function c(n) {
            function a(a) {
                r.call(e, a),
                t.send(n, G.PARENTSTATE, {
                    CUSTOMERIDS: e.getCustomerIDs()
                })
            }
            var r = e.setCustomerIDs;
            e.setCustomerIDs = a
        }
        return function(e) {
            t.isInvalid(e) || (t.parse(e).prefix === G.HANDSHAKE ? o : s)(e.source)
        }
    }, q = function(e, t) {
        function n(e) {
            return function(n) {
                a[e] = n,
                ++r === i && t(a)
            }
        }
        var a = {}
          , r = 0
          , i = Object.keys(e).length;
        Object.keys(e).forEach(function(t) {
            var a = e[t];
            if (a.fn) {
                var r = a.args || [];
                r.unshift(n(t)),
                a.fn.apply(a.context || null, r)
            }
        })
    }, z = {
        get: function(e) {
            e = encodeURIComponent(e);
            var t = (";" + document.cookie).split(" ").join(";")
              , n = t.indexOf(";" + e + "=")
              , a = n < 0 ? n : t.indexOf(";", n + 1);
            return n < 0 ? "" : decodeURIComponent(t.substring(n + 2 + e.length, a < 0 ? t.length : a))
        },
        set: function(e, t, n) {
            var r = a(n, "cookieLifetime")
              , i = a(n, "expires")
              , o = a(n, "domain")
              , s = a(n, "secure") ? "Secure" : "";
            if (i && "SESSION" !== r && "NONE" !== r) {
                var c = "" !== t ? parseInt(r || 0, 10) : -60;
                if (c)
                    (i = new Date).setTime(i.getTime() + 1e3 * c);
                else if (1 === i) {
                    var l = (i = new Date).getYear();
                    i.setYear(l + 2 + (l < 1900 ? 1900 : 0))
                }
            } else
                i = 0;
            return e && "NONE" !== r ? (document.cookie = encodeURIComponent(e) + "=" + encodeURIComponent(t) + "; path=/;" + (i ? " expires=" + i.toGMTString() + ";" : "") + (o ? " domain=" + o + ";" : "") + s,
            this.get(e) === t) : 0
        },
        remove: function(e, t) {
            var n = a(t, "domain");
            n = n ? " domain=" + n + ";" : "",
            document.cookie = encodeURIComponent(e) + "=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;" + n
        }
    }, Q = function(e) {
        var t;
        !e && v.location && (e = v.location.hostname);
        var n, a = (t = e).split(".");
        for (n = a.length - 2; n >= 0; n--)
            if (t = a.slice(n).join("."),
            z.set("test", "cookie", {
                domain: t
            }))
                return z.remove("test", {
                    domain: t
                }),
                t;
        return ""
    }, $ = {
        compare: s,
        isLessThan: function(e, t) {
            return s(e, t) < 0
        },
        areVersionsDifferent: function(e, t) {
            return 0 !== s(e, t)
        },
        isGreaterThan: function(e, t) {
            return s(e, t) > 0
        },
        isEqual: function(e, t) {
            return 0 === s(e, t)
        }
    }, J = !!v.postMessage, Y = {
        postMessage: function(e, t, n) {
            var a = 1;
            t && (J ? n.postMessage(e, t.replace(/([^:]+:\/\/[^\/]+).*/, "$1")) : t && (n.location = t.replace(/#.*$/, "") + "#" + +new Date + a++ + "&" + e))
        },
        receiveMessage: function(e, t) {
            var n;
            try {
                J && (e && (n = function(n) {
                    if ("string" == typeof t && n.origin !== t || "[object Function]" === Object.prototype.toString.call(t) && !1 === t(n.origin))
                        return !1;
                    e(n)
                }
                ),
                v.addEventListener ? v[e ? "addEventListener" : "removeEventListener"]("message", n) : v[e ? "attachEvent" : "detachEvent"]("onmessage", n))
            } catch (e) {}
        }
    }, W = function(e) {
        var t, n, a = "0123456789", r = "", i = "", o = 8, s = 10, c = 10;
        if (1 == e) {
            for (a += "ABCDEF",
            t = 0; 16 > t; t++)
                n = Math.floor(Math.random() * o),
                r += a.substring(n, n + 1),
                n = Math.floor(Math.random() * o),
                i += a.substring(n, n + 1),
                o = 16;
            return r + "-" + i
        }
        for (t = 0; 19 > t; t++)
            n = Math.floor(Math.random() * s),
            r += a.substring(n, n + 1),
            0 === t && 9 == n ? s = 3 : (1 == t || 2 == t) && 10 != s && 2 > n ? s = 10 : 2 < t && (s = 10),
            n = Math.floor(Math.random() * c),
            i += a.substring(n, n + 1),
            0 === t && 9 == n ? c = 3 : (1 == t || 2 == t) && 10 != c && 2 > n ? c = 10 : 2 < t && (c = 10);
        return r + i
    }, X = function(e) {
        return {
            corsMetadata: (t = "none",
            n = !0,
            "undefined" != typeof XMLHttpRequest && XMLHttpRequest === Object(XMLHttpRequest) && ("withCredentials"in new XMLHttpRequest ? t = "XMLHttpRequest" : "undefined" != typeof XDomainRequest && XDomainRequest === Object(XDomainRequest) && (n = !1),
            Object.prototype.toString.call(v.HTMLElement).indexOf("Constructor") > 0 && (n = !1)),
            {
                corsType: t,
                corsCookiesEnabled: n
            }),
            getCORSInstance: function() {
                return "none" === this.corsMetadata.corsType ? null : new v[this.corsMetadata.corsType]
            },
            fireCORS: function(t, n) {
                function a(e) {
                    var n;
                    try {
                        if ((n = JSON.parse(e)) !== Object(n))
                            return void r.handleCORSError(t, null, "Response is not JSON")
                    } catch (e) {
                        return void r.handleCORSError(t, e, "Error parsing response as JSON")
                    }
                    try {
                        for (var a = t.callback, i = v, o = 0; o < a.length; o++)
                            i = i[a[o]];
                        i(n)
                    } catch (e) {
                        r.handleCORSError(t, e, "Error forming callback function")
                    }
                }
                var r = this;
                n && (t.loadErrorHandler = n);
                try {
                    var i = this.getCORSInstance();
                    i.open("get", t.corsUrl + "&ts=" + (new Date).getTime(), !0),
                    "XMLHttpRequest" === this.corsMetadata.corsType && (i.withCredentials = !0,
                    i.timeout = e.loadTimeout,
                    i.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"),
                    i.onreadystatechange = function() {
                        4 === this.readyState && 200 === this.status && a(this.responseText)
                    }
                    ),
                    i.onerror = function(e) {
                        r.handleCORSError(t, e, "onerror")
                    }
                    ,
                    i.ontimeout = function(e) {
                        r.handleCORSError(t, e, "ontimeout")
                    }
                    ,
                    i.send(),
                    e._log.requests.push(t.corsUrl)
                } catch (e) {
                    this.handleCORSError(t, e, "try-catch")
                }
            },
            handleCORSError: function(t, n, a) {
                e.CORSErrors.push({
                    corsData: t,
                    error: n,
                    description: a
                }),
                t.loadErrorHandler && ("ontimeout" === a ? t.loadErrorHandler(!0) : t.loadErrorHandler(!1))
            }
        };
        var t, n
    }, K = {
        POST_MESSAGE_ENABLED: !!v.postMessage,
        DAYS_BETWEEN_SYNC_ID_CALLS: 1,
        MILLIS_PER_DAY: 864e5,
        ADOBE_MC: "adobe_mc",
        ADOBE_MC_SDID: "adobe_mc_sdid",
        VALID_VISITOR_ID_REGEX: /^[0-9a-fA-F\-]+$/,
        ADOBE_MC_TTL_IN_MIN: 5,
        VERSION_REGEX: /vVersion\|((\d+\.)?(\d+\.)?(\*|\d+))(?=$|\|)/,
        FIRST_PARTY_SERVER_COOKIE: "s_ecid"
    }, Z = function(e, t) {
        var n = v.document;
        return {
            THROTTLE_START: 3e4,
            MAX_SYNCS_LENGTH: 649,
            throttleTimerSet: !1,
            id: null,
            onPagePixels: [],
            iframeHost: null,
            getIframeHost: function(e) {
                if ("string" == typeof e) {
                    var t = e.split("/");
                    return t[0] + "//" + t[2]
                }
            },
            subdomain: null,
            url: null,
            getUrl: function() {
                var t, a = "http://fast.", r = "?d_nsid=" + e.idSyncContainerID + "#" + encodeURIComponent(n.location.origin);
                return this.subdomain || (this.subdomain = "nosubdomainreturned"),
                e.loadSSL && (a = e.idSyncSSLUseAkamai ? "https://fast." : "https://"),
                t = a + this.subdomain + ".demdex.net/dest5.html" + r,
                this.iframeHost = this.getIframeHost(t),
                this.id = "destination_publishing_iframe_" + this.subdomain + "_" + e.idSyncContainerID,
                t
            },
            checkDPIframeSrc: function() {
                var t = "?d_nsid=" + e.idSyncContainerID + "#" + encodeURIComponent(n.location.href);
                "string" == typeof e.dpIframeSrc && e.dpIframeSrc.length && (this.id = "destination_publishing_iframe_" + (e._subdomain || this.subdomain || (new Date).getTime()) + "_" + e.idSyncContainerID,
                this.iframeHost = this.getIframeHost(e.dpIframeSrc),
                this.url = e.dpIframeSrc + t)
            },
            idCallNotProcesssed: null,
            doAttachIframe: !1,
            startedAttachingIframe: !1,
            iframeHasLoaded: null,
            iframeIdChanged: null,
            newIframeCreated: null,
            originalIframeHasLoadedAlready: null,
            iframeLoadedCallbacks: [],
            regionChanged: !1,
            timesRegionChanged: 0,
            sendingMessages: !1,
            messages: [],
            messagesPosted: [],
            messagesReceived: [],
            messageSendingInterval: K.POST_MESSAGE_ENABLED ? null : 100,
            onPageDestinationsFired: [],
            jsonForComparison: [],
            jsonDuplicates: [],
            jsonWaiting: [],
            jsonProcessed: [],
            canSetThirdPartyCookies: !0,
            receivedThirdPartyCookiesNotification: !1,
            readyToAttachIframePreliminary: function() {
                return !(e.idSyncDisableSyncs || e.disableIdSyncs || e.idSyncDisable3rdPartySyncing || e.disableThirdPartyCookies || e.disableThirdPartyCalls)
            },
            readyToAttachIframe: function() {
                return this.readyToAttachIframePreliminary() && (this.doAttachIframe || e._doAttachIframe) && (this.subdomain && "nosubdomainreturned" !== this.subdomain || e._subdomain) && this.url && !this.startedAttachingIframe
            },
            attachIframe: function() {
                function e() {
                    (r = n.createElement("iframe")).sandbox = "allow-scripts allow-same-origin",
                    r.title = "Adobe ID Syncing iFrame",
                    r.id = a.id,
                    r.name = a.id + "_name",
                    r.style.cssText = "display: none; width: 0; height: 0;",
                    r.src = a.url,
                    a.newIframeCreated = !0,
                    t(),
                    n.body.appendChild(r)
                }
                function t(e) {
                    r.addEventListener("load", function() {
                        r.className = "aamIframeLoaded",
                        a.iframeHasLoaded = !0,
                        a.fireIframeLoadedCallbacks(e),
                        a.requestToProcess()
                    })
                }
                this.startedAttachingIframe = !0;
                var a = this
                  , r = n.getElementById(this.id);
                r ? "IFRAME" !== r.nodeName ? (this.id += "_2",
                this.iframeIdChanged = !0,
                e()) : (this.newIframeCreated = !1,
                "aamIframeLoaded" !== r.className ? (this.originalIframeHasLoadedAlready = !1,
                t("The destination publishing iframe already exists from a different library, but hadn't loaded yet.")) : (this.originalIframeHasLoadedAlready = !0,
                this.iframeHasLoaded = !0,
                this.iframe = r,
                this.fireIframeLoadedCallbacks("The destination publishing iframe already exists from a different library, and had loaded alresady."),
                this.requestToProcess())) : e(),
                this.iframe = r
            },
            fireIframeLoadedCallbacks: function(e) {
                this.iframeLoadedCallbacks.forEach(function(t) {
                    "function" == typeof t && t({
                        message: e || "The destination publishing iframe was attached and loaded successfully."
                    })
                }),
                this.iframeLoadedCallbacks = []
            },
            requestToProcess: function(t) {
                function n() {
                    r.jsonForComparison.push(t),
                    r.jsonWaiting.push(t),
                    r.processSyncOnPage(t)
                }
                var a, r = this;
                if (t === Object(t) && t.ibs)
                    if (a = JSON.stringify(t.ibs || []),
                    this.jsonForComparison.length) {
                        var i, o, s, c = !1;
                        for (i = 0,
                        o = this.jsonForComparison.length; i < o; i++)
                            if (s = this.jsonForComparison[i],
                            a === JSON.stringify(s.ibs || [])) {
                                c = !0;
                                break
                            }
                        c ? this.jsonDuplicates.push(t) : n()
                    } else
                        n();
                if ((this.receivedThirdPartyCookiesNotification || !K.POST_MESSAGE_ENABLED || this.iframeHasLoaded) && this.jsonWaiting.length) {
                    var l = this.jsonWaiting.shift();
                    this.process(l),
                    this.requestToProcess()
                }
                e.idSyncDisableSyncs || e.disableIdSyncs || !this.iframeHasLoaded || !this.messages.length || this.sendingMessages || (this.throttleTimerSet || (this.throttleTimerSet = !0,
                setTimeout(function() {
                    r.messageSendingInterval = K.POST_MESSAGE_ENABLED ? null : 150
                }, this.THROTTLE_START)),
                this.sendingMessages = !0,
                this.sendMessages())
            },
            getRegionAndCheckIfChanged: function(t, n) {
                var a = e._getField("MCAAMLH")
                  , r = t.d_region || t.dcs_region;
                return a ? r && (e._setFieldExpire("MCAAMLH", n),
                e._setField("MCAAMLH", r),
                parseInt(a, 10) !== r && (this.regionChanged = !0,
                this.timesRegionChanged++,
                e._setField("MCSYNCSOP", ""),
                e._setField("MCSYNCS", ""),
                a = r)) : (a = r) && (e._setFieldExpire("MCAAMLH", n),
                e._setField("MCAAMLH", a)),
                a || (a = ""),
                a
            },
            processSyncOnPage: function(e) {
                var t, n, a, r;
                if ((t = e.ibs) && t instanceof Array && (n = t.length))
                    for (a = 0; a < n; a++)
                        (r = t[a]).syncOnPage && this.checkFirstPartyCookie(r, "", "syncOnPage")
            },
            process: function(e) {
                var t, n, a, r, i, o = encodeURIComponent, s = !1;
                if ((t = e.ibs) && t instanceof Array && (n = t.length))
                    for (s = !0,
                    a = 0; a < n; a++)
                        r = t[a],
                        i = [o("ibs"), o(r.id || ""), o(r.tag || ""), N.encodeAndBuildRequest(r.url || [], ","), o(r.ttl || ""), "", "", r.fireURLSync ? "true" : "false"],
                        r.syncOnPage || (this.canSetThirdPartyCookies ? this.addMessage(i.join("|")) : r.fireURLSync && this.checkFirstPartyCookie(r, i.join("|")));
                s && this.jsonProcessed.push(e)
            },
            checkFirstPartyCookie: function(t, n, a) {
                var r = "syncOnPage" === a
                  , i = r ? "MCSYNCSOP" : "MCSYNCS";
                e._readVisitor();
                var o, s, c = e._getField(i), l = !1, u = !1, d = Math.ceil((new Date).getTime() / K.MILLIS_PER_DAY);
                c ? (o = c.split("*"),
                l = (s = this.pruneSyncData(o, t.id, d)).dataPresent,
                u = s.dataValid,
                l && u || this.fireSync(r, t, n, o, i, d)) : (o = [],
                this.fireSync(r, t, n, o, i, d))
            },
            pruneSyncData: function(e, t, n) {
                var a, r, i, o = !1, s = !1;
                for (r = 0; r < e.length; r++)
                    a = e[r],
                    i = parseInt(a.split("-")[1], 10),
                    a.match("^" + t + "-") ? (o = !0,
                    n < i ? s = !0 : (e.splice(r, 1),
                    r--)) : n >= i && (e.splice(r, 1),
                    r--);
                return {
                    dataPresent: o,
                    dataValid: s
                }
            },
            manageSyncsSize: function(e) {
                if (e.join("*").length > this.MAX_SYNCS_LENGTH)
                    for (e.sort(function(e, t) {
                        return parseInt(e.split("-")[1], 10) - parseInt(t.split("-")[1], 10)
                    }); e.join("*").length > this.MAX_SYNCS_LENGTH; )
                        e.shift()
            },
            fireSync: function(t, n, a, r, i, o) {
                var s = this;
                if (t) {
                    if ("img" === n.tag) {
                        var c, l, u, d, p = n.url, g = e.loadSSL ? "https:" : "http:";
                        for (c = 0,
                        l = p.length; c < l; c++) {
                            u = p[c],
                            d = /^\/\//.test(u);
                            var f = new Image;
                            f.addEventListener("load", function(t, n, a, r) {
                                return function() {
                                    s.onPagePixels[t] = null,
                                    e._readVisitor();
                                    var o, c, l, u, d = e._getField(i), p = [];
                                    if (d)
                                        for (c = 0,
                                        l = (o = d.split("*")).length; c < l; c++)
                                            (u = o[c]).match("^" + n.id + "-") || p.push(u);
                                    s.setSyncTrackingData(p, n, a, r)
                                }
                            }(this.onPagePixels.length, n, i, o)),
                            f.src = (d ? g : "") + u,
                            this.onPagePixels.push(f)
                        }
                    }
                } else
                    this.addMessage(a),
                    this.setSyncTrackingData(r, n, i, o)
            },
            addMessage: function(t) {
                var n = encodeURIComponent(e._enableErrorReporting ? "---destpub-debug---" : "---destpub---");
                this.messages.push((K.POST_MESSAGE_ENABLED ? "" : n) + t)
            },
            setSyncTrackingData: function(t, n, a, r) {
                t.push(n.id + "-" + (r + Math.ceil(n.ttl / 60 / 24))),
                this.manageSyncsSize(t),
                e._setField(a, t.join("*"))
            },
            sendMessages: function() {
                var e, t = this, n = "", a = encodeURIComponent;
                this.regionChanged && (n = a("---destpub-clear-dextp---"),
                this.regionChanged = !1),
                this.messages.length ? K.POST_MESSAGE_ENABLED ? (e = n + a("---destpub-combined---") + this.messages.join("%01"),
                this.postMessage(e),
                this.messages = [],
                this.sendingMessages = !1) : (e = this.messages.shift(),
                this.postMessage(n + e),
                setTimeout(function() {
                    t.sendMessages()
                }, this.messageSendingInterval)) : this.sendingMessages = !1
            },
            postMessage: function(e) {
                Y.postMessage(e, this.url, this.iframe.contentWindow),
                this.messagesPosted.push(e)
            },
            receiveMessage: function(e) {
                var t, n = /^---destpub-to-parent---/;
                "string" == typeof e && n.test(e) && ("canSetThirdPartyCookies" === (t = e.replace(n, "").split("|"))[0] && (this.canSetThirdPartyCookies = "true" === t[1],
                this.receivedThirdPartyCookiesNotification = !0,
                this.requestToProcess()),
                this.messagesReceived.push(e))
            },
            processIDCallData: function(a) {
                (null == this.url || a.subdomain && "nosubdomainreturned" === this.subdomain) && ("string" == typeof e._subdomain && e._subdomain.length ? this.subdomain = e._subdomain : this.subdomain = a.subdomain || "",
                this.url = this.getUrl()),
                a.ibs instanceof Array && a.ibs.length && (this.doAttachIframe = !0),
                this.readyToAttachIframe() && (e.idSyncAttachIframeOnWindowLoad ? (t.windowLoaded || "complete" === n.readyState || "loaded" === n.readyState) && this.attachIframe() : this.attachIframeASAP()),
                "function" == typeof e.idSyncIDCallResult ? e.idSyncIDCallResult(a) : this.requestToProcess(a),
                "function" == typeof e.idSyncAfterIDCallResult && e.idSyncAfterIDCallResult(a)
            },
            canMakeSyncIDCall: function(t, n) {
                return e._forceSyncIDCall || !t || n - t > K.DAYS_BETWEEN_SYNC_ID_CALLS
            },
            attachIframeASAP: function() {
                function e() {
                    t.startedAttachingIframe || (n.body ? t.attachIframe() : setTimeout(e, 30))
                }
                var t = this;
                e()
            }
        }
    }, ee = {
        audienceManagerServer: {},
        audienceManagerServerSecure: {},
        cookieDomain: {},
        cookieLifetime: {},
        cookieName: {},
        doesOptInApply: {},
        disableThirdPartyCalls: {},
        discardTrackingServerECID: {},
        idSyncAfterIDCallResult: {},
        idSyncAttachIframeOnWindowLoad: {},
        idSyncContainerID: {},
        idSyncDisable3rdPartySyncing: {},
        disableThirdPartyCookies: {},
        idSyncDisableSyncs: {},
        disableIdSyncs: {},
        idSyncIDCallResult: {},
        idSyncSSLUseAkamai: {},
        isCoopSafe: {},
        isIabContext: {},
        isOptInStorageEnabled: {},
        loadSSL: {},
        loadTimeout: {},
        marketingCloudServer: {},
        marketingCloudServerSecure: {},
        optInCookieDomain: {},
        optInStorageExpiry: {},
        overwriteCrossDomainMCIDAndAID: {},
        preOptInApprovals: {},
        previousPermissions: {},
        resetBeforeVersion: {},
        sdidParamExpiry: {},
        serverState: {},
        sessionCookieName: {},
        secureCookie: {},
        takeTimeoutMetrics: {},
        trackingServer: {},
        trackingServerSecure: {},
        whitelistIframeDomains: {},
        whitelistParentDomain: {}
    }, te = {
        getConfigNames: function() {
            return Object.keys(ee)
        },
        getConfigs: function() {
            return ee
        },
        normalizeConfig: function(e) {
            return "function" != typeof e ? e : e()
        }
    }, ne = function(e) {
        var t = {};
        return e.on = function(e, n, a) {
            if (!n || "function" != typeof n)
                throw new Error("[ON] Callback should be a function.");
            t.hasOwnProperty(e) || (t[e] = []);
            var r = t[e].push({
                callback: n,
                context: a
            }) - 1;
            return function() {
                t[e].splice(r, 1),
                t[e].length || delete t[e]
            }
        }
        ,
        e.off = function(e, n) {
            t.hasOwnProperty(e) && (t[e] = t[e].filter(function(e) {
                if (e.callback !== n)
                    return e
            }))
        }
        ,
        e.publish = function(e) {
            if (t.hasOwnProperty(e)) {
                var n = [].slice.call(arguments, 1);
                t[e].slice(0).forEach(function(e) {
                    e.callback.apply(e.context, n)
                })
            }
        }
        ,
        e.publish
    }, ae = {
        PENDING: "pending",
        CHANGED: "changed",
        COMPLETE: "complete"
    }, re = {
        AAM: "aam",
        ADCLOUD: "adcloud",
        ANALYTICS: "aa",
        CAMPAIGN: "campaign",
        ECID: "ecid",
        LIVEFYRE: "livefyre",
        TARGET: "target",
        VIDEO_ANALYTICS: "videoaa"
    }, ie = (t(y = {}, re.AAM, 565),
    t(y, re.ECID, 565),
    y), oe = (t(b = {}, re.AAM, [1, 2, 5]),
    t(b, re.ECID, [1, 2, 5]),
    b), se = (L = re,
    Object.keys(L).map(function(e) {
        return L[e]
    })), ce = function() {
        var e = {};
        return e.callbacks = Object.create(null),
        e.add = function(t, n) {
            if (!l(n))
                throw new Error("[callbackRegistryFactory] Make sure callback is a function or an array of functions.");
            e.callbacks[t] = e.callbacks[t] || [];
            var a = e.callbacks[t].push(n) - 1;
            return function() {
                e.callbacks[t].splice(a, 1)
            }
        }
        ,
        e.execute = function(t, n) {
            if (e.callbacks[t]) {
                n = (n = void 0 === n ? [] : n)instanceof Array ? n : [n];
                try {
                    for (; e.callbacks[t].length; ) {
                        var a = e.callbacks[t].shift();
                        "function" == typeof a ? a.apply(null, n) : a instanceof Array && a[1].apply(a[0], n)
                    }
                    delete e.callbacks[t]
                } catch (e) {}
            }
        }
        ,
        e.executeAll = function(t, n) {
            (n || t && !c(t)) && Object.keys(e.callbacks).forEach(function(n) {
                var a = void 0 !== t[n] ? t[n] : "";
                e.execute(n, a)
            }, e)
        }
        ,
        e.hasCallbacks = function() {
            return Boolean(Object.keys(e.callbacks).length)
        }
        ,
        e
    }, le = function() {}, ue = function(e) {
        var t = window.console;
        return !!t && "function" == typeof t[e]
    }, de = function(e, t, n) {
        return n() ? function() {
            if (ue(e)) {
                for (var n = arguments.length, a = new Array(n), r = 0; r < n; r++)
                    a[r] = arguments[r];
                console[e].apply(console, [t].concat(a))
            }
        }
        : le
    }, pe = new u("[ADOBE OPT-IN]"), ge = function(t, n) {
        return e(t) === n
    }, fe = function(e, t) {
        return e instanceof Array ? e : ge(e, "string") ? [e] : t || []
    }, me = function(e) {
        var t = Object.keys(e);
        return !!t.length && t.every(function(t) {
            return !0 === e[t]
        })
    }, he = function(e) {
        return !(!e || be(e)) && fe(e).every(function(e) {
            return se.indexOf(e) > -1
        })
    }, ve = function(e, t) {
        return e.reduce(function(e, n) {
            return e[n] = t,
            e
        }, {})
    }, ye = function(e) {
        return JSON.parse(JSON.stringify(e))
    }, be = function(e) {
        return "[object Array]" === Object.prototype.toString.call(e) && !e.length
    }, Le = function(e) {
        if (Ee(e))
            return e;
        try {
            return JSON.parse(e)
        } catch (e) {
            return {}
        }
    }, Ce = function(e) {
        return void 0 === e || (Ee(e) ? he(Object.keys(e)) : Se(e))
    }, Se = function(e) {
        try {
            var t = JSON.parse(e);
            return !!e && ge(e, "string") && he(Object.keys(t))
        } catch (e) {
            return !1
        }
    }, Ee = function(e) {
        return null !== e && ge(e, "object") && !1 === Array.isArray(e)
    }, _e = function() {}, ke = function(e) {
        return ge(e, "function") ? e() : e
    }, Te = function(e, t) {
        Ce(e) || pe.error("".concat(t))
    }, Oe = function(e) {
        return Object.keys(e).map(function(t) {
            return e[t]
        })
    }, Ie = function(e) {
        return Oe(e).filter(function(e, t, n) {
            return n.indexOf(e) === t
        })
    }, De = function(e) {
        return function() {
            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}
              , n = t.command
              , a = t.params
              , r = void 0 === a ? {} : a
              , i = t.callback
              , o = void 0 === i ? _e : i;
            if (!n || -1 === n.indexOf("."))
                throw new Error("[OptIn.execute] Please provide a valid command.");
            try {
                var s = n.split(".")
                  , c = e[s[0]]
                  , l = s[1];
                if (!c || "function" != typeof c[l])
                    throw new Error("Make sure the plugin and API name exist.");
                var u = Object.assign(r, {
                    callback: o
                });
                c[l].call(c, u)
            } catch (e) {
                pe.error("[execute] Something went wrong: " + e.message)
            }
        }
    };
    p.prototype = Object.create(Error.prototype),
    p.prototype.constructor = p;
    var Pe = "fetchPermissions"
      , Ae = "[OptIn#registerPlugin] Plugin is invalid.";
    g.Categories = re,
    g.TimeoutError = p;
    var Ne = Object.freeze({
        OptIn: g,
        IabPlugin: h
    })
      , we = function(e, t) {
        e.publishDestinations = function(n) {
            var a = arguments[1]
              , r = arguments[2];
            try {
                r = "function" == typeof r ? r : n.callback
            } catch (e) {
                r = function() {}
            }
            var i = t;
            if (i.readyToAttachIframePreliminary()) {
                if ("string" == typeof n) {
                    if (!n.length)
                        return void r({
                            error: "subdomain is not a populated string."
                        });
                    if (!(a instanceof Array && a.length))
                        return void r({
                            error: "messages is not a populated array."
                        });
                    var o = !1;
                    if (a.forEach(function(e) {
                        "string" == typeof e && e.length && (i.addMessage(e),
                        o = !0)
                    }),
                    !o)
                        return void r({
                            error: "None of the messages are populated strings."
                        })
                } else {
                    if (!N.isObject(n))
                        return void r({
                            error: "Invalid parameters passed."
                        });
                    var s = n;
                    if ("string" != typeof (n = s.subdomain) || !n.length)
                        return void r({
                            error: "config.subdomain is not a populated string."
                        });
                    var c = s.urlDestinations;
                    if (!(c instanceof Array && c.length))
                        return void r({
                            error: "config.urlDestinations is not a populated array."
                        });
                    var l = [];
                    c.forEach(function(e) {
                        N.isObject(e) && (e.hideReferrer ? e.message && i.addMessage(e.message) : l.push(e))
                    }),
                    function u() {
                        l.length && setTimeout(function() {
                            var e = new Image
                              , t = l.shift();
                            e.src = t.url,
                            i.onPageDestinationsFired.push(t),
                            u()
                        }, 100)
                    }()
                }
                i.iframe ? (r({
                    message: "The destination publishing iframe is already attached and loaded."
                }),
                i.requestToProcess()) : !e.subdomain && e._getField("MCMID") ? (i.subdomain = n,
                i.doAttachIframe = !0,
                i.url = i.getUrl(),
                i.readyToAttachIframe() ? (i.iframeLoadedCallbacks.push(function(e) {
                    r({
                        message: "Attempted to attach and load the destination publishing iframe through this API call. Result: " + (e.message || "no result")
                    })
                }),
                i.attachIframe()) : r({
                    error: "Encountered a problem in attempting to attach and load the destination publishing iframe through this API call."
                })) : i.iframeLoadedCallbacks.push(function(e) {
                    r({
                        message: "Attempted to attach and load the destination publishing iframe through normal Visitor API processing. Result: " + (e.message || "no result")
                    })
                })
            } else
                r({
                    error: "The destination publishing iframe is disabled in the Visitor library."
                })
        }
    }
      , Ve = function He(e) {
        function t(e, t) {
            return e >>> t | e << 32 - t
        }
        for (var n, a, r = Math.pow, i = r(2, 32), o = "", s = [], c = 8 * e.length, l = He.h = He.h || [], u = He.k = He.k || [], d = u.length, p = {}, g = 2; d < 64; g++)
            if (!p[g]) {
                for (n = 0; n < 313; n += g)
                    p[n] = g;
                l[d] = r(g, .5) * i | 0,
                u[d++] = r(g, 1 / 3) * i | 0
            }
        for (e += "\x80"; e.length % 64 - 56; )
            e += "\0";
        for (n = 0; n < e.length; n++) {
            if ((a = e.charCodeAt(n)) >> 8)
                return;
            s[n >> 2] |= a << (3 - n) % 4 * 8
        }
        for (s[s.length] = c / i | 0,
        s[s.length] = c,
        a = 0; a < s.length; ) {
            var f = s.slice(a, a += 16)
              , m = l;
            for (l = l.slice(0, 8),
            n = 0; n < 64; n++) {
                var h = f[n - 15]
                  , v = f[n - 2]
                  , y = l[0]
                  , b = l[4]
                  , L = l[7] + (t(b, 6) ^ t(b, 11) ^ t(b, 25)) + (b & l[5] ^ ~b & l[6]) + u[n] + (f[n] = n < 16 ? f[n] : f[n - 16] + (t(h, 7) ^ t(h, 18) ^ h >>> 3) + f[n - 7] + (t(v, 17) ^ t(v, 19) ^ v >>> 10) | 0);
                (l = [L + ((t(y, 2) ^ t(y, 13) ^ t(y, 22)) + (y & l[1] ^ y & l[2] ^ l[1] & l[2])) | 0].concat(l))[4] = l[4] + L | 0
            }
            for (n = 0; n < 8; n++)
                l[n] = l[n] + m[n] | 0
        }
        for (n = 0; n < 8; n++)
            for (a = 3; a + 1; a--) {
                var C = l[n] >> 8 * a & 255;
                o += (C < 16 ? 0 : "") + C.toString(16)
            }
        return o
    }
      , Me = function(e, t) {
        return "SHA-256" !== t && "SHA256" !== t && "sha256" !== t && "sha-256" !== t || (e = Ve(e)),
        e
    }
      , Be = function(e) {
        return String(e).trim().toLowerCase()
    }
      , Re = Ne.OptIn;
    N.defineGlobalNamespace(),
    window.adobe.OptInCategories = Re.Categories;
    var xe = function(t, n, a) {
        function r(e) {
            var t = e;
            return function(e) {
                var n = e || L.location.href;
                try {
                    var a = f._extractParamFromUri(n, t);
                    if (a)
                        return R.parsePipeDelimetedKeyValues(a)
                } catch (e) {}
            }
        }
        function i(e) {
            function t(e, t, n) {
                e && e.match(K.VALID_VISITOR_ID_REGEX) && (n === _ && (b = !0),
                t(e))
            }
            t(e[_], f.setMarketingCloudVisitorID, _),
            f._setFieldExpire(D, -1),
            t(e[O], f.setAnalyticsVisitorID)
        }
        function o(e) {
            e = e || {},
            f._supplementalDataIDCurrent = e.supplementalDataIDCurrent || "",
            f._supplementalDataIDCurrentConsumed = e.supplementalDataIDCurrentConsumed || {},
            f._supplementalDataIDLast = e.supplementalDataIDLast || "",
            f._supplementalDataIDLastConsumed = e.supplementalDataIDLastConsumed || {}
        }
        function s(e) {
            function t(e, t, n) {
                return (n = n ? n += "|" : n) + (e + "=") + encodeURIComponent(t)
            }
            function n(e, n) {
                var a = n[0]
                  , r = n[1];
                return null != r && r !== P && (e = t(a, r, e)),
                e
            }
            var a, r = e.reduce(n, "");
            return (a = (a = r) ? a += "|" : a) + "TS=" + R.getTimestampInSeconds()
        }
        function c(e) {
            var t = e.minutesToLive
              , n = "";
            return (f.idSyncDisableSyncs || f.disableIdSyncs) && (n = n || "Error: id syncs have been disabled"),
            "string" == typeof e.dpid && e.dpid.length || (n = n || "Error: config.dpid is empty"),
            "string" == typeof e.url && e.url.length || (n = n || "Error: config.url is empty"),
            void 0 === t ? t = 20160 : (t = parseInt(t, 10),
            (isNaN(t) || t <= 0) && (n = n || "Error: config.minutesToLive needs to be a positive number")),
            {
                error: n,
                ttl: t
            }
        }
        function l() {
            return !(!f.configs.doesOptInApply || m.optIn.isComplete && u())
        }
        function u() {
            return f.configs.isIabContext ? m.optIn.isApproved(m.optIn.Categories.ECID) && y : m.optIn.isApproved(m.optIn.Categories.ECID)
        }
        function d(e, t) {
            if (y = !0,
            e)
                throw new Error("[IAB plugin] : " + e);
            t.gdprApplies && (h = t.consentString),
            f.init(),
            g()
        }
        function p() {
            m.optIn.isApproved(m.optIn.Categories.ECID) && (f.configs.isIabContext ? m.optIn.execute({
                command: "iabPlugin.fetchConsentData",
                callback: d
            }) : (f.init(),
            g()))
        }
        function g() {
            m.optIn.off("complete", p)
        }
        if (!a || a.split("").reverse().join("") !== t)
            throw new Error("Please use `Visitor.getInstance` to instantiate Visitor.");
        var f = this
          , m = window.adobe
          , h = ""
          , y = !1
          , b = !1;
        f.version = "4.4.1";
        var L = v
          , C = L.Visitor;
        C.version = f.version,
        C.AuthState = E.AUTH_STATE,
        C.OptOut = E.OPT_OUT,
        L.s_c_in || (L.s_c_il = [],
        L.s_c_in = 0),
        f._c = "Visitor",
        f._il = L.s_c_il,
        f._in = L.s_c_in,
        f._il[f._in] = f,
        L.s_c_in++,
        f._instanceType = "regular",
        f._log = {
            requests: []
        },
        f.marketingCloudOrgID = t,
        f.cookieName = "AMCV_" + t,
        f.sessionCookieName = "AMCVS_" + t,
        f.cookieDomain = Q(),
        f.loadSSL = L.location.protocol.toLowerCase().indexOf("https") >= 0,
        f.loadTimeout = 3e4,
        f.CORSErrors = [],
        f.marketingCloudServer = f.audienceManagerServer = "dpm.demdex.net",
        f.sdidParamExpiry = 30;
        var S = null
          , _ = "MCMID"
          , k = "MCIDTS"
          , T = "A"
          , O = "MCAID"
          , I = "AAM"
          , D = "MCAAMB"
          , P = "NONE"
          , A = function(e) {
            return !Object.prototype[e]
        }
          , w = X(f);
        f.FIELDS = E.FIELDS,
        f.cookieRead = function(e) {
            return z.get(e)
        }
        ,
        f.cookieWrite = function(e, t, n) {
            var a = f.cookieLifetime ? ("" + f.cookieLifetime).toUpperCase() : ""
              , r = !1;
            return f.configs && f.configs.secureCookie && "https:" === location.protocol && (r = !0),
            z.set(e, "" + t, {
                expires: n,
                domain: f.cookieDomain,
                cookieLifetime: a,
                secure: r
            })
        }
        ,
        f.resetState = function(e) {
            e ? f._mergeServerState(e) : o()
        }
        ,
        f._isAllowedDone = !1,
        f._isAllowedFlag = !1,
        f.isAllowed = function() {
            return f._isAllowedDone || (f._isAllowedDone = !0,
            (f.cookieRead(f.cookieName) || f.cookieWrite(f.cookieName, "T", 1)) && (f._isAllowedFlag = !0)),
            "T" === f.cookieRead(f.cookieName) && f._helpers.removeCookie(f.cookieName),
            f._isAllowedFlag
        }
        ,
        f.setMarketingCloudVisitorID = function(e) {
            f._setMarketingCloudFields(e)
        }
        ,
        f._use1stPartyMarketingCloudServer = !1,
        f.getMarketingCloudVisitorID = function(e, t) {
            f.marketingCloudServer && f.marketingCloudServer.indexOf(".demdex.net") < 0 && (f._use1stPartyMarketingCloudServer = !0);
            var n = f._getAudienceManagerURLData("_setMarketingCloudFields")
              , a = n.url;
            return f._getRemoteField(_, a, e, t, n)
        }
        ;
        var V = function(e, t) {
            var n = {};
            f.getMarketingCloudVisitorID(function() {
                t.forEach(function(e) {
                    n[e] = f._getField(e, !0)
                }),
                -1 !== t.indexOf("MCOPTOUT") ? f.isOptedOut(function(t) {
                    n.MCOPTOUT = t,
                    e(n)
                }, null, !0) : e(n)
            }, !0)
        };
        f.getVisitorValues = function(e, t) {
            var n = {
                MCMID: {
                    fn: f.getMarketingCloudVisitorID,
                    args: [!0],
                    context: f
                },
                MCOPTOUT: {
                    fn: f.isOptedOut,
                    args: [void 0, !0],
                    context: f
                },
                MCAID: {
                    fn: f.getAnalyticsVisitorID,
                    args: [!0],
                    context: f
                },
                MCAAMLH: {
                    fn: f.getAudienceManagerLocationHint,
                    args: [!0],
                    context: f
                },
                MCAAMB: {
                    fn: f.getAudienceManagerBlob,
                    args: [!0],
                    context: f
                }
            }
              , a = t && t.length ? N.pluck(n, t) : n;
            t && -1 === t.indexOf("MCAID") ? V(e, t) : q(a, e)
        }
        ,
        f._currentCustomerIDs = {},
        f._customerIDsHashChanged = !1,
        f._newCustomerIDsHash = "",
        f.setCustomerIDs = function(t, n) {
            function a() {
                f._customerIDsHashChanged = !1
            }
            if (!f.isOptedOut() && t) {
                if (!N.isObject(t) || N.isObjectEmpty(t))
                    return !1;
                var r, i, o;
                for (r in f._readVisitor(),
                t)
                    if (A(r) && (n = (i = t[r]).hasOwnProperty("hashType") ? i.hashType : n,
                    i))
                        if ("object" === e(i)) {
                            var s = {};
                            if (i.id) {
                                if (n) {
                                    if (!(o = Me(Be(i.id), n)))
                                        return;
                                    i.id = o,
                                    s.hashType = n
                                }
                                s.id = i.id
                            }
                            null != i.authState && (s.authState = i.authState),
                            f._currentCustomerIDs[r] = s
                        } else if (n) {
                            if (!(o = Me(Be(i), n)))
                                return;
                            f._currentCustomerIDs[r] = {
                                id: o,
                                hashType: n
                            }
                        } else
                            f._currentCustomerIDs[r] = {
                                id: i
                            };
                var c = f.getCustomerIDs()
                  , l = f._getField("MCCIDH")
                  , u = "";
                for (r in l || (l = 0),
                c)
                    A(r) && (u += (u ? "|" : "") + r + "|" + ((i = c[r]).id ? i.id : "") + (i.authState ? i.authState : ""));
                f._newCustomerIDsHash = String(f._hash(u)),
                f._newCustomerIDsHash !== l && (f._customerIDsHashChanged = !0,
                f._mapCustomerIDs(a))
            }
        }
        ,
        f.getCustomerIDs = function() {
            f._readVisitor();
            var e, t, n = {};
            for (e in f._currentCustomerIDs)
                A(e) && (t = f._currentCustomerIDs[e],
                n[e] || (n[e] = {}),
                t.id && (n[e].id = t.id),
                null != t.authState ? n[e].authState = t.authState : n[e].authState = C.AuthState.UNKNOWN,
                t.hashType && (n[e].hashType = t.hashType));
            return n
        }
        ,
        f.setAnalyticsVisitorID = function(e) {
            f._setAnalyticsFields(e)
        }
        ,
        f.getAnalyticsVisitorID = function(e, t, n) {
            if (!R.isTrackingServerPopulated() && !n)
                return f._callCallback(e, [""]),
                "";
            var a = "";
            if (n || (a = f.getMarketingCloudVisitorID(function() {
                f.getAnalyticsVisitorID(e, !0)
            })),
            a || n) {
                var r = n ? f.marketingCloudServer : f.trackingServer
                  , i = "";
                f.loadSSL && (n ? f.marketingCloudServerSecure && (r = f.marketingCloudServerSecure) : f.trackingServerSecure && (r = f.trackingServerSecure));
                var o = {};
                if (r) {
                    var s = "http" + (f.loadSSL ? "s" : "") + "://" + r + "/id"
                      , c = "d_visid_ver=" + f.version + "&mcorgid=" + encodeURIComponent(f.marketingCloudOrgID) + (a ? "&mid=" + encodeURIComponent(a) : "") + (f.idSyncDisable3rdPartySyncing || f.disableThirdPartyCookies ? "&d_coppa=true" : "")
                      , l = ["s_c_il", f._in, "_set" + (n ? "MarketingCloud" : "Analytics") + "Fields"];
                    i = s + "?" + c + "&callback=s_c_il%5B" + f._in + "%5D._set" + (n ? "MarketingCloud" : "Analytics") + "Fields",
                    o.corsUrl = s + "?" + c,
                    o.callback = l
                }
                return o.url = i,
                f._getRemoteField(n ? _ : O, i, e, t, o)
            }
            return ""
        }
        ,
        f.getAudienceManagerLocationHint = function(e, t) {
            if (f.getMarketingCloudVisitorID(function() {
                f.getAudienceManagerLocationHint(e, !0)
            })) {
                var n = f._getField(O);
                if (!n && R.isTrackingServerPopulated() && (n = f.getAnalyticsVisitorID(function() {
                    f.getAudienceManagerLocationHint(e, !0)
                })),
                n || !R.isTrackingServerPopulated()) {
                    var a = f._getAudienceManagerURLData()
                      , r = a.url;
                    return f._getRemoteField("MCAAMLH", r, e, t, a)
                }
            }
            return ""
        }
        ,
        f.getLocationHint = f.getAudienceManagerLocationHint,
        f.getAudienceManagerBlob = function(e, t) {
            if (f.getMarketingCloudVisitorID(function() {
                f.getAudienceManagerBlob(e, !0)
            })) {
                var n = f._getField(O);
                if (!n && R.isTrackingServerPopulated() && (n = f.getAnalyticsVisitorID(function() {
                    f.getAudienceManagerBlob(e, !0)
                })),
                n || !R.isTrackingServerPopulated()) {
                    var a = f._getAudienceManagerURLData()
                      , r = a.url;
                    return f._customerIDsHashChanged && f._setFieldExpire(D, -1),
                    f._getRemoteField(D, r, e, t, a)
                }
            }
            return ""
        }
        ,
        f._supplementalDataIDCurrent = "",
        f._supplementalDataIDCurrentConsumed = {},
        f._supplementalDataIDLast = "",
        f._supplementalDataIDLastConsumed = {},
        f.getSupplementalDataID = function(e, t) {
            f._supplementalDataIDCurrent || t || (f._supplementalDataIDCurrent = f._generateID(1));
            var n = f._supplementalDataIDCurrent;
            return f._supplementalDataIDLast && !f._supplementalDataIDLastConsumed[e] ? (n = f._supplementalDataIDLast,
            f._supplementalDataIDLastConsumed[e] = !0) : n && (f._supplementalDataIDCurrentConsumed[e] && (f._supplementalDataIDLast = f._supplementalDataIDCurrent,
            f._supplementalDataIDLastConsumed = f._supplementalDataIDCurrentConsumed,
            f._supplementalDataIDCurrent = n = t ? "" : f._generateID(1),
            f._supplementalDataIDCurrentConsumed = {}),
            n && (f._supplementalDataIDCurrentConsumed[e] = !0)),
            n
        }
        ;
        var M = !1;
        f._liberatedOptOut = null,
        f.getOptOut = function(e, t) {
            var n = f._getAudienceManagerURLData("_setMarketingCloudFields")
              , a = n.url;
            if (u())
                return f._getRemoteField("MCOPTOUT", a, e, t, n);
            if (f._registerCallback("liberatedOptOut", e),
            null !== f._liberatedOptOut)
                return f._callAllCallbacks("liberatedOptOut", [f._liberatedOptOut]),
                M = !1,
                f._liberatedOptOut;
            if (M)
                return null;
            M = !0;
            var r = "liberatedGetOptOut";
            return n.corsUrl = n.corsUrl.replace(/dpm\.demdex\.net\/id\?/, "dpm.demdex.net/optOutStatus?"),
            n.callback = [r],
            v[r] = function(e) {
                if (e === Object(e)) {
                    var t, n, a = N.parseOptOut(e, t, P);
                    t = a.optOut,
                    n = 1e3 * a.d_ottl,
                    f._liberatedOptOut = t,
                    setTimeout(function() {
                        f._liberatedOptOut = null
                    }, n)
                }
                f._callAllCallbacks("liberatedOptOut", [t]),
                M = !1
            }
            ,
            w.fireCORS(n),
            null
        }
        ,
        f.isOptedOut = function(e, t, n) {
            t || (t = C.OptOut.GLOBAL);
            var a = f.getOptOut(function(n) {
                var a = n === C.OptOut.GLOBAL || n.indexOf(t) >= 0;
                f._callCallback(e, [a])
            }, n);
            return a ? a === C.OptOut.GLOBAL || a.indexOf(t) >= 0 : null
        }
        ,
        f._fields = null,
        f._fieldsExpired = null,
        f._hash = function(e) {
            var t, n = 0;
            if (e)
                for (t = 0; t < e.length; t++)
                    n = (n << 5) - n + e.charCodeAt(t),
                    n &= n;
            return n
        }
        ,
        f._generateID = W,
        f._generateLocalMID = function() {
            var e = f._generateID(0);
            return G.isClientSideMarketingCloudVisitorID = !0,
            e
        }
        ,
        f._callbackList = null,
        f._callCallback = function(e, t) {
            try {
                "function" == typeof e ? e.apply(L, t) : e[1].apply(e[0], t)
            } catch (e) {}
        }
        ,
        f._registerCallback = function(e, t) {
            t && (null == f._callbackList && (f._callbackList = {}),
            null == f._callbackList[e] && (f._callbackList[e] = []),
            f._callbackList[e].push(t))
        }
        ,
        f._callAllCallbacks = function(e, t) {
            if (null != f._callbackList) {
                var n = f._callbackList[e];
                if (n)
                    for (; n.length > 0; )
                        f._callCallback(n.shift(), t)
            }
        }
        ,
        f._addQuerystringParam = function(e, t, n, a) {
            var r = encodeURIComponent(t) + "=" + encodeURIComponent(n)
              , i = R.parseHash(e)
              , o = R.hashlessUrl(e);
            if (-1 === o.indexOf("?"))
                return o + "?" + r + i;
            var s = o.split("?")
              , c = s[0] + "?"
              , l = s[1];
            return c + R.addQueryParamAtLocation(l, r, a) + i
        }
        ,
        f._extractParamFromUri = function(e, t) {
            var n = new RegExp("[\\?&#]" + t + "=([^&#]*)").exec(e);
            if (n && n.length)
                return decodeURIComponent(n[1])
        }
        ,
        f._parseAdobeMcFromUrl = r(K.ADOBE_MC),
        f._parseAdobeMcSdidFromUrl = r(K.ADOBE_MC_SDID),
        f._attemptToPopulateSdidFromUrl = function(e) {
            var n = f._parseAdobeMcSdidFromUrl(e)
              , a = 1e9;
            n && n.TS && (a = R.getTimestampInSeconds() - n.TS),
            n && n.SDID && n.MCORGID === t && a < f.sdidParamExpiry && (f._supplementalDataIDCurrent = n.SDID,
            f._supplementalDataIDCurrentConsumed.SDID_URL_PARAM = !0)
        }
        ,
        f._attemptToPopulateIdsFromUrl = function() {
            var e = f._parseAdobeMcFromUrl();
            if (e && e.TS) {
                var n = R.getTimestampInSeconds() - e.TS;
                if (Math.floor(n / 60) > K.ADOBE_MC_TTL_IN_MIN || e.MCORGID !== t)
                    return;
                i(e)
            }
        }
        ,
        f._mergeServerState = function(e) {
            if (e)
                try {
                    if (a = e,
                    (e = R.isObject(a) ? a : JSON.parse(a))[f.marketingCloudOrgID]) {
                        var t = e[f.marketingCloudOrgID];
                        n = t.customerIDs,
                        R.isObject(n) && f.setCustomerIDs(n),
                        o(t.sdid)
                    }
                } catch (e) {
                    throw new Error("`serverState` has an invalid format.")
                }
            var n, a
        }
        ,
        f._timeout = null,
        f._loadData = function(e, t, n, a) {
            t = f._addQuerystringParam(t, "d_fieldgroup", e, 1),
            a.url = f._addQuerystringParam(a.url, "d_fieldgroup", e, 1),
            a.corsUrl = f._addQuerystringParam(a.corsUrl, "d_fieldgroup", e, 1),
            G.fieldGroupObj[e] = !0,
            a === Object(a) && a.corsUrl && "XMLHttpRequest" === w.corsMetadata.corsType && w.fireCORS(a, n, e)
        }
        ,
        f._clearTimeout = function(e) {
            null != f._timeout && f._timeout[e] && (clearTimeout(f._timeout[e]),
            f._timeout[e] = 0)
        }
        ,
        f._settingsDigest = 0,
        f._getSettingsDigest = function() {
            if (!f._settingsDigest) {
                var e = f.version;
                f.audienceManagerServer && (e += "|" + f.audienceManagerServer),
                f.audienceManagerServerSecure && (e += "|" + f.audienceManagerServerSecure),
                f._settingsDigest = f._hash(e)
            }
            return f._settingsDigest
        }
        ,
        f._readVisitorDone = !1,
        f._readVisitor = function() {
            if (!f._readVisitorDone) {
                f._readVisitorDone = !0;
                var e, t, n, a, r, i, o = f._getSettingsDigest(), s = !1, c = f.cookieRead(f.cookieName), l = new Date;
                if (c || b || f.discardTrackingServerECID || (c = f.cookieRead(K.FIRST_PARTY_SERVER_COOKIE)),
                null == f._fields && (f._fields = {}),
                c && "T" !== c)
                    for ((c = c.split("|"))[0].match(/^[\-0-9]+$/) && (parseInt(c[0], 10) !== o && (s = !0),
                    c.shift()),
                    c.length % 2 == 1 && c.pop(),
                    e = 0; e < c.length; e += 2)
                        n = (t = c[e].split("-"))[0],
                        a = c[e + 1],
                        t.length > 1 ? (r = parseInt(t[1], 10),
                        i = t[1].indexOf("s") > 0) : (r = 0,
                        i = !1),
                        s && ("MCCIDH" === n && (a = ""),
                        r > 0 && (r = l.getTime() / 1e3 - 60)),
                        n && a && (f._setField(n, a, 1),
                        r > 0 && (f._fields["expire" + n] = r + (i ? "s" : ""),
                        (l.getTime() >= 1e3 * r || i && !f.cookieRead(f.sessionCookieName)) && (f._fieldsExpired || (f._fieldsExpired = {}),
                        f._fieldsExpired[n] = !0)));
                !f._getField(O) && R.isTrackingServerPopulated() && (c = f.cookieRead("s_vi")) && ((c = c.split("|")).length > 1 && c[0].indexOf("v1") >= 0 && ((e = (a = c[1]).indexOf("[")) >= 0 && (a = a.substring(0, e)),
                a && a.match(K.VALID_VISITOR_ID_REGEX) && f._setField(O, a)))
            }
        }
        ,
        f._appendVersionTo = function(e) {
            var t = "vVersion|" + f.version
              , n = e ? f._getCookieVersion(e) : null;
            return n ? $.areVersionsDifferent(n, f.version) && (e = e.replace(K.VERSION_REGEX, t)) : e += (e ? "|" : "") + t,
            e
        }
        ,
        f._writeVisitor = function() {
            var e, t, n = f._getSettingsDigest();
            for (e in f._fields)
                A(e) && f._fields[e] && "expire" !== e.substring(0, 6) && (t = f._fields[e],
                n += (n ? "|" : "") + e + (f._fields["expire" + e] ? "-" + f._fields["expire" + e] : "") + "|" + t);
            n = f._appendVersionTo(n),
            f.cookieWrite(f.cookieName, n, 1)
        }
        ,
        f._getField = function(e, t) {
            return null == f._fields || !t && f._fieldsExpired && f._fieldsExpired[e] ? null : f._fields[e]
        }
        ,
        f._setField = function(e, t, n) {
            null == f._fields && (f._fields = {}),
            f._fields[e] = t,
            n || f._writeVisitor()
        }
        ,
        f._getFieldList = function(e, t) {
            var n = f._getField(e, t);
            return n ? n.split("*") : null
        }
        ,
        f._setFieldList = function(e, t, n) {
            f._setField(e, t ? t.join("*") : "", n)
        }
        ,
        f._getFieldMap = function(e, t) {
            var n = f._getFieldList(e, t);
            if (n) {
                var a, r = {};
                for (a = 0; a < n.length; a += 2)
                    r[n[a]] = n[a + 1];
                return r
            }
            return null
        }
        ,
        f._setFieldMap = function(e, t, n) {
            var a, r = null;
            if (t)
                for (a in r = [],
                t)
                    A(a) && (r.push(a),
                    r.push(t[a]));
            f._setFieldList(e, r, n)
        }
        ,
        f._setFieldExpire = function(e, t, n) {
            var a = new Date;
            a.setTime(a.getTime() + 1e3 * t),
            null == f._fields && (f._fields = {}),
            f._fields["expire" + e] = Math.floor(a.getTime() / 1e3) + (n ? "s" : ""),
            t < 0 ? (f._fieldsExpired || (f._fieldsExpired = {}),
            f._fieldsExpired[e] = !0) : f._fieldsExpired && (f._fieldsExpired[e] = !1),
            n && (f.cookieRead(f.sessionCookieName) || f.cookieWrite(f.sessionCookieName, "1"))
        }
        ,
        f._findVisitorID = function(t) {
            return t && ("object" === e(t) && (t = t.d_mid ? t.d_mid : t.visitorID ? t.visitorID : t.id ? t.id : t.uuid ? t.uuid : "" + t),
            t && "NOTARGET" === (t = t.toUpperCase()) && (t = P),
            t && (t === P || t.match(K.VALID_VISITOR_ID_REGEX)) || (t = "")),
            t
        }
        ,
        f._setFields = function(t, n) {
            if (f._clearTimeout(t),
            null != f._loading && (f._loading[t] = !1),
            G.fieldGroupObj[t] && G.setState(t, !1),
            "MC" === t) {
                !0 !== G.isClientSideMarketingCloudVisitorID && (G.isClientSideMarketingCloudVisitorID = !1);
                var a = f._getField(_);
                if (!a || f.overwriteCrossDomainMCIDAndAID) {
                    if (!(a = "object" === e(n) && n.mid ? n.mid : f._findVisitorID(n))) {
                        if (f._use1stPartyMarketingCloudServer && !f.tried1stPartyMarketingCloudServer)
                            return f.tried1stPartyMarketingCloudServer = !0,
                            void f.getAnalyticsVisitorID(null, !1, !0);
                        a = f._generateLocalMID()
                    }
                    f._setField(_, a)
                }
                a && a !== P || (a = ""),
                "object" === e(n) && ((n.d_region || n.dcs_region || n.d_blob || n.blob) && f._setFields(I, n),
                f._use1stPartyMarketingCloudServer && n.mid && f._setFields(T, {
                    id: n.id
                })),
                f._callAllCallbacks(_, [a])
            }
            if (t === I && "object" === e(n)) {
                var r = 604800;
                null != n.id_sync_ttl && n.id_sync_ttl && (r = parseInt(n.id_sync_ttl, 10));
                var i = x.getRegionAndCheckIfChanged(n, r);
                f._callAllCallbacks("MCAAMLH", [i]);
                var o = f._getField(D);
                (n.d_blob || n.blob) && ((o = n.d_blob) || (o = n.blob),
                f._setFieldExpire(D, r),
                f._setField(D, o)),
                o || (o = ""),
                f._callAllCallbacks(D, [o]),
                !n.error_msg && f._newCustomerIDsHash && f._setField("MCCIDH", f._newCustomerIDsHash)
            }
            if (t === T) {
                var s = f._getField(O);
                s && !f.overwriteCrossDomainMCIDAndAID || ((s = f._findVisitorID(n)) ? s !== P && f._setFieldExpire(D, -1) : s = P,
                f._setField(O, s)),
                s && s !== P || (s = ""),
                f._callAllCallbacks(O, [s])
            }
            if (f.idSyncDisableSyncs || f.disableIdSyncs)
                x.idCallNotProcesssed = !0;
            else {
                x.idCallNotProcesssed = !1;
                var c = {};
                c.ibs = n.ibs,
                c.subdomain = n.subdomain,
                x.processIDCallData(c)
            }
            if (n === Object(n)) {
                var l, d;
                u() && f.isAllowed() && (l = f._getField("MCOPTOUT"));
                var p = N.parseOptOut(n, l, P);
                l = p.optOut,
                d = p.d_ottl,
                f._setFieldExpire("MCOPTOUT", d, !0),
                f._setField("MCOPTOUT", l),
                f._callAllCallbacks("MCOPTOUT", [l])
            }
        }
        ,
        f._loading = null,
        f._getRemoteField = function(e, t, n, a, r) {
            var i, o = "", s = R.isFirstPartyAnalyticsVisitorIDCall(e), c = {
                MCAAMLH: !0,
                MCAAMB: !0
            };
            if (u() && f.isAllowed())
                if (f._readVisitor(),
                !(!(o = f._getField(e, !0 === c[e])) || f._fieldsExpired && f._fieldsExpired[e]) || f.disableThirdPartyCalls && !s)
                    o || (e === _ ? (f._registerCallback(e, n),
                    o = f._generateLocalMID(),
                    f.setMarketingCloudVisitorID(o)) : e === O ? (f._registerCallback(e, n),
                    o = "",
                    f.setAnalyticsVisitorID(o)) : (o = "",
                    a = !0));
                else if (e === _ || "MCOPTOUT" === e ? i = "MC" : "MCAAMLH" === e || e === D ? i = I : e === O && (i = T),
                i)
                    return !t || null != f._loading && f._loading[i] || (null == f._loading && (f._loading = {}),
                    f._loading[i] = !0,
                    f._loadData(i, t, function(t) {
                        if (!f._getField(e)) {
                            t && G.setState(i, !0);
                            var n = "";
                            e === _ ? n = f._generateLocalMID() : i === I && (n = {
                                error_msg: "timeout"
                            }),
                            f._setFields(i, n)
                        }
                    }, r)),
                    f._registerCallback(e, n),
                    o || (t || f._setFields(i, {
                        id: P
                    }),
                    "");
            return e !== _ && e !== O || o !== P || (o = "",
            a = !0),
            n && a && f._callCallback(n, [o]),
            o
        }
        ,
        f._setMarketingCloudFields = function(e) {
            f._readVisitor(),
            f._setFields("MC", e)
        }
        ,
        f._mapCustomerIDs = function(e) {
            f.getAudienceManagerBlob(e, !0)
        }
        ,
        f._setAnalyticsFields = function(e) {
            f._readVisitor(),
            f._setFields(T, e)
        }
        ,
        f._setAudienceManagerFields = function(e) {
            f._readVisitor(),
            f._setFields(I, e)
        }
        ,
        f._getAudienceManagerURLData = function(e) {
            var t = f.audienceManagerServer
              , n = ""
              , a = f._getField(_)
              , r = f._getField(D, !0)
              , i = f._getField(O)
              , o = i && i !== P ? "&d_cid_ic=AVID%01" + encodeURIComponent(i) : "";
            if (f.loadSSL && f.audienceManagerServerSecure && (t = f.audienceManagerServerSecure),
            t) {
                var s, c, l = f.getCustomerIDs();
                if (l)
                    for (s in l)
                        A(s) && (c = l[s],
                        o += "&d_cid_ic=" + encodeURIComponent(s) + "%01" + encodeURIComponent(c.id ? c.id : "") + (c.authState ? "%01" + c.authState : ""));
                e || (e = "_setAudienceManagerFields");
                var u = "http" + (f.loadSSL ? "s" : "") + "://" + t + "/id"
                  , d = "d_visid_ver=" + f.version + (h && -1 !== u.indexOf("demdex.net") ? "&gdpr=1&gdpr_force=1&gdpr_consent=" + h : "") + "&d_rtbd=json&d_ver=2" + (!a && f._use1stPartyMarketingCloudServer ? "&d_verify=1" : "") + "&d_orgid=" + encodeURIComponent(f.marketingCloudOrgID) + "&d_nsid=" + (f.idSyncContainerID || 0) + (a ? "&d_mid=" + encodeURIComponent(a) : "") + (f.idSyncDisable3rdPartySyncing || f.disableThirdPartyCookies ? "&d_coppa=true" : "") + (!0 === S ? "&d_coop_safe=1" : !1 === S ? "&d_coop_unsafe=1" : "") + (r ? "&d_blob=" + encodeURIComponent(r) : "") + o
                  , p = ["s_c_il", f._in, e];
                return {
                    url: n = u + "?" + d + "&d_cb=s_c_il%5B" + f._in + "%5D." + e,
                    corsUrl: u + "?" + d,
                    callback: p
                }
            }
            return {
                url: n
            }
        }
        ,
        f.appendVisitorIDsTo = function(e) {
            try {
                var t = [[_, f._getField(_)], [O, f._getField(O)], ["MCORGID", f.marketingCloudOrgID]];
                return f._addQuerystringParam(e, K.ADOBE_MC, s(t))
            } catch (t) {
                return e
            }
        }
        ,
        f.appendSupplementalDataIDTo = function(e, t) {
            if (!(t = t || f.getSupplementalDataID(R.generateRandomString(), !0)))
                return e;
            try {
                var n = s([["SDID", t], ["MCORGID", f.marketingCloudOrgID]]);
                return f._addQuerystringParam(e, K.ADOBE_MC_SDID, n)
            } catch (t) {
                return e
            }
        }
        ;
        var R = {
            parseHash: function(e) {
                var t = e.indexOf("#");
                return t > 0 ? e.substr(t) : ""
            },
            hashlessUrl: function(e) {
                var t = e.indexOf("#");
                return t > 0 ? e.substr(0, t) : e
            },
            addQueryParamAtLocation: function(e, t, n) {
                var a = e.split("&");
                return n = null != n ? n : a.length,
                a.splice(n, 0, t),
                a.join("&")
            },
            isFirstPartyAnalyticsVisitorIDCall: function(e, t, n) {
                return e === O && (t || (t = f.trackingServer),
                n || (n = f.trackingServerSecure),
                !("string" != typeof (a = f.loadSSL ? n : t) || !a.length) && a.indexOf("2o7.net") < 0 && a.indexOf("omtrdc.net") < 0);
                var a
            },
            isObject: function(e) {
                return Boolean(e && e === Object(e))
            },
            removeCookie: function(e) {
                z.remove(e, {
                    domain: f.cookieDomain
                })
            },
            isTrackingServerPopulated: function() {
                return !!f.trackingServer || !!f.trackingServerSecure
            },
            getTimestampInSeconds: function() {
                return Math.round((new Date).getTime() / 1e3)
            },
            parsePipeDelimetedKeyValues: function(e) {
                return e.split("|").reduce(function(e, t) {
                    var n = t.split("=");
                    return e[n[0]] = decodeURIComponent(n[1]),
                    e
                }, {})
            },
            generateRandomString: function(e) {
                e = e || 5;
                for (var t = "", n = "abcdefghijklmnopqrstuvwxyz0123456789"; e--; )
                    t += n[Math.floor(Math.random() * n.length)];
                return t
            },
            normalizeBoolean: function(e) {
                return "true" === e || "false" !== e && e
            },
            parseBoolean: function(e) {
                return "true" === e || "false" !== e && null
            },
            replaceMethodsWithFunction: function(e, t) {
                for (var n in e)
                    e.hasOwnProperty(n) && "function" == typeof e[n] && (e[n] = t);
                return e
            }
        };
        f._helpers = R;
        var x = Z(f, C);
        f._destinationPublishing = x,
        f.timeoutMetricsLog = [];
        var G = {
            isClientSideMarketingCloudVisitorID: null,
            MCIDCallTimedOut: null,
            AnalyticsIDCallTimedOut: null,
            AAMIDCallTimedOut: null,
            fieldGroupObj: {},
            setState: function(e, t) {
                switch (e) {
                case "MC":
                    !1 === t ? !0 !== this.MCIDCallTimedOut && (this.MCIDCallTimedOut = !1) : this.MCIDCallTimedOut = t;
                    break;
                case T:
                    !1 === t ? !0 !== this.AnalyticsIDCallTimedOut && (this.AnalyticsIDCallTimedOut = !1) : this.AnalyticsIDCallTimedOut = t;
                    break;
                case I:
                    !1 === t ? !0 !== this.AAMIDCallTimedOut && (this.AAMIDCallTimedOut = !1) : this.AAMIDCallTimedOut = t
                }
            }
        };
        f.isClientSideMarketingCloudVisitorID = function() {
            return G.isClientSideMarketingCloudVisitorID
        }
        ,
        f.MCIDCallTimedOut = function() {
            return G.MCIDCallTimedOut
        }
        ,
        f.AnalyticsIDCallTimedOut = function() {
            return G.AnalyticsIDCallTimedOut
        }
        ,
        f.AAMIDCallTimedOut = function() {
            return G.AAMIDCallTimedOut
        }
        ,
        f.idSyncGetOnPageSyncInfo = function() {
            return f._readVisitor(),
            f._getField("MCSYNCSOP")
        }
        ,
        f.idSyncByURL = function(e) {
            if (!f.isOptedOut()) {
                var t = c(e || {});
                if (t.error)
                    return t.error;
                var n, a, r = e.url, i = encodeURIComponent, o = x;
                return r = r.replace(/^https:/, "").replace(/^http:/, ""),
                n = N.encodeAndBuildRequest(["", e.dpid, e.dpuuid || ""], ","),
                a = ["ibs", i(e.dpid), "img", i(r), t.ttl, "", n],
                o.addMessage(a.join("|")),
                o.requestToProcess(),
                "Successfully queued"
            }
        }
        ,
        f.idSyncByDataSource = function(e) {
            if (!f.isOptedOut())
                return e === Object(e) && "string" == typeof e.dpuuid && e.dpuuid.length ? (e.url = "//dpm.demdex.net/ibs:dpid=" + e.dpid + "&dpuuid=" + e.dpuuid,
                f.idSyncByURL(e)) : "Error: config or config.dpuuid is empty"
        }
        ,
        we(f, x),
        f._getCookieVersion = function(e) {
            e = e || f.cookieRead(f.cookieName);
            var t = K.VERSION_REGEX.exec(e);
            return t && t.length > 1 ? t[1] : null
        }
        ,
        f._resetAmcvCookie = function(e) {
            var t = f._getCookieVersion();
            t && !$.isLessThan(t, e) || R.removeCookie(f.cookieName)
        }
        ,
        f.setAsCoopSafe = function() {
            S = !0
        }
        ,
        f.setAsCoopUnsafe = function() {
            S = !1
        }
        ,
        function() {
            if (f.configs = Object.create(null),
            R.isObject(n))
                for (var e in n)
                    A(e) && (f[e] = n[e],
                    f.configs[e] = n[e])
        }(),
        [["getMarketingCloudVisitorID"], ["setCustomerIDs", void 0], ["getAnalyticsVisitorID"], ["getAudienceManagerLocationHint"], ["getLocationHint"], ["getAudienceManagerBlob"]].forEach(function(e) {
            var t = e[0]
              , n = 2 === e.length ? e[1] : ""
              , a = f[t];
            f[t] = function(e) {
                return u() && f.isAllowed() ? a.apply(f, arguments) : ("function" == typeof e && f._callCallback(e, [n]),
                n)
            }
        }),
        f.init = function() {
            if (l())
                return m.optIn.fetchPermissions(p, !0);
            !function() {
                if (R.isObject(n)) {
                    f.idSyncContainerID = f.idSyncContainerID || 0,
                    S = "boolean" == typeof f.isCoopSafe ? f.isCoopSafe : R.parseBoolean(f.isCoopSafe),
                    f.resetBeforeVersion && f._resetAmcvCookie(f.resetBeforeVersion),
                    f._attemptToPopulateIdsFromUrl(),
                    f._attemptToPopulateSdidFromUrl(),
                    f._readVisitor();
                    var e = f._getField(k)
                      , t = Math.ceil((new Date).getTime() / K.MILLIS_PER_DAY);
                    f.idSyncDisableSyncs || f.disableIdSyncs || !x.canMakeSyncIDCall(e, t) || (f._setFieldExpire(D, -1),
                    f._setField(k, t)),
                    f.getMarketingCloudVisitorID(),
                    f.getAudienceManagerLocationHint(),
                    f.getAudienceManagerBlob(),
                    f._mergeServerState(f.serverState)
                } else
                    f._attemptToPopulateIdsFromUrl(),
                    f._attemptToPopulateSdidFromUrl()
            }(),
            function() {
                if (!f.idSyncDisableSyncs && !f.disableIdSyncs) {
                    x.checkDPIframeSrc();
                    var e = function() {
                        var e = x;
                        e.readyToAttachIframe() && e.attachIframe()
                    };
                    L.addEventListener("load", function() {
                        C.windowLoaded = !0,
                        e()
                    });
                    try {
                        Y.receiveMessage(function(e) {
                            x.receiveMessage(e.data)
                        }, x.iframeHost)
                    } catch (e) {}
                }
            }(),
            f.whitelistIframeDomains && K.POST_MESSAGE_ENABLED && (f.whitelistIframeDomains = f.whitelistIframeDomains instanceof Array ? f.whitelistIframeDomains : [f.whitelistIframeDomains],
            f.whitelistIframeDomains.forEach(function(e) {
                var n = new B(t,e)
                  , a = H(f, n);
                Y.receiveMessage(a, e)
            }))
        }
    };
    xe.config = te,
    v.Visitor = xe;
    var Ge = xe
      , Fe = function(e) {
        if (N.isObject(e))
            return Object.keys(e).filter(function(t) {
                return "" !== e[t]
            }).reduce(function(t, n) {
                var a = "doesOptInApply" !== n ? e[n] : te.normalizeConfig(e[n])
                  , r = N.normalizeBoolean(a);
                return t[n] = r,
                t
            }, Object.create(null))
    }
      , Ue = Ne.OptIn
      , je = Ne.IabPlugin;
    return Ge.getInstance = function(e, t) {
        if (!e)
            throw new Error("Visitor requires Adobe Marketing Cloud Org ID.");
        e.indexOf("@") < 0 && (e += "@AdobeOrg");
        var n = function() {
            var t = v.s_c_il;
            if (t)
                for (var n = 0; n < t.length; n++) {
                    var a = t[n];
                    if (a && "Visitor" === a._c && a.marketingCloudOrgID === e)
                        return a
                }
        }();
        if (n)
            return n;
        var a, r = Fe(t);
        a = r || {},
        v.adobe.optIn = v.adobe.optIn || function() {
            var e = N.pluck(a, ["doesOptInApply", "previousPermissions", "preOptInApprovals", "isOptInStorageEnabled", "optInStorageExpiry", "isIabContext"])
              , t = a.optInCookieDomain || a.cookieDomain;
            t = (t = t || Q()) === window.location.hostname ? "" : t,
            e.optInCookieDomain = t;
            var n = new Ue(e,{
                cookies: z
            });
            if (e.isIabContext) {
                var r = new je(window.__cmp);
                n.registerPlugin(r)
            }
            return n
        }();
        var i = e.split("").reverse().join("")
          , o = new Ge(e,null,i);
        N.isObject(r) && r.cookieDomain && (o.cookieDomain = r.cookieDomain),
        v.s_c_il.splice(--v.s_c_in, 1);
        var s = N.getIeVersion();
        if ("number" == typeof s && s < 10)
            return o._helpers.replaceMethodsWithFunction(o, function() {});
        var c, l = function() {
            try {
                return v.self !== v.parent
            } catch (e) {
                return !0
            }
        }() && ((c = o).cookieWrite("TEST_AMCV_COOKIE", "T", 1),
        "T" !== c.cookieRead("TEST_AMCV_COOKIE") || (c._helpers.removeCookie("TEST_AMCV_COOKIE"),
        0)) && v.parent ? new x(e,r,o,v.parent) : new Ge(e,r,i);
        return o = null,
        l.init(),
        l
    }
    ,
    function() {
        function e() {
            Ge.windowLoaded = !0
        }
        v.addEventListener ? v.addEventListener("load", e) : v.attachEvent && v.attachEvent("onload", e),
        Ge.codeLoadEnd = (new Date).getTime()
    }(),
    Ge
}();
// All code and conventions are protected by copyright
// All code and conventions are protected by copyright
!function(t, n, a) {
    function r() {
        D.getToolsByType("nielsen").length > 0 && D.domReady(D.bind(this.initialize, this))
    }
    function i(e) {
        D.domReady(D.bind(function() {
            this.twttr = e || t.twttr,
            this.initialize()
        }, this))
    }
    function o() {
        this.lastURL = D.URL(),
        this._fireIfURIChanged = D.bind(this.fireIfURIChanged, this),
        this._onPopState = D.bind(this.onPopState, this),
        this._onHashChange = D.bind(this.onHashChange, this),
        this._pushState = D.bind(this.pushState, this),
        this._replaceState = D.bind(this.replaceState, this),
        this.initialize()
    }
    function c() {
        var e = D.filter(D.rules, function(e) {
            return 0 === e.event.indexOf("dataelementchange")
        });
        this.dataElementsNames = D.map(e, function(e) {
            return e.event.match(/dataelementchange\((.*)\)/i)[1]
        }, this),
        this.initPolling()
    }
    function l() {
        D.addEventHandler(t, "orientationchange", l.orientationChange)
    }
    function u() {
        this.rules = D.filter(D.rules, function(e) {
            return "videoplayed" === e.event.substring(0, 11)
        }),
        this.eventHandler = D.bind(this.onUpdateTime, this)
    }
    function d() {
        this.defineEvents(),
        this.visibilityApiHasPriority = !0,
        n.addEventListener ? this.setVisibilityApiPriority(!1) : this.attachDetachOlderEventListeners(!0, n, "focusout");
        D.bindEvent("aftertoolinit", function() {
            D.fireEvent(D.visibility.isHidden() ? "tabblur" : "tabfocus")
        })
    }
    function p(e) {
        e = e || D.rules,
        this.rules = D.filter(e, function(e) {
            return "inview" === e.event
        }),
        this.elements = [],
        this.eventHandler = D.bind(this.track, this),
        D.addEventHandler(t, "scroll", this.eventHandler),
        D.addEventHandler(t, "load", this.eventHandler)
    }
    function g() {
        this.rules = D.filter(D.rules, function(e) {
            return "elementexists" === e.event
        })
    }
    function f(e) {
        this.delay = 250,
        this.FB = e,
        D.domReady(D.bind(function() {
            D.poll(D.bind(this.initialize, this), this.delay, 8)
        }, this))
    }
    function m() {
        var e = this.eventRegex = /^hover\(([0-9]+)\)$/
          , t = this.rules = [];
        D.each(D.rules, function(n) {
            n.event.match(e) && t.push([Number(n.event.match(e)[1]), n.selector])
        })
    }
    function h(e) {
        D.BaseTool.call(this, e),
        this.defineListeners(),
        this.beaconMethod = "plainBeacon",
        this.adapt = new h.DataAdapters,
        this.dataProvider = new h.DataProvider.Aggregate
    }
    function v(e) {
        D.BaseTool.call(this, e),
        this.styleElements = {},
        this.targetPageParamsStore = {}
    }
    function y() {
        D.BaseTool.call(this),
        this.asyncScriptCallbackQueue = [],
        this.argsForBlockingScripts = []
    }
    function b(e) {
        D.BaseTool.call(this, e),
        this.varBindings = {},
        this.events = [],
        this.products = [],
        this.customSetupFuns = []
    }
    function L(e) {
        D.BaseTool.call(this, e),
        this.name = e.name || "Basic"
    }
    function C(e) {
        D.BaseTool.call(this, e)
    }
    function S(e) {
        D.BaseTool.call(this, e)
    }
    function E(e) {
        D.BaseTool.call(this, e),
        this.name = e.name || "VisitorID",
        this.initialize()
    }
    var _, k, T, O = Object.prototype.toString, I = t._satellite && t._satellite.override, D = {
        initialized: !1,
        $data: function(e, t, n) {
            if (e) {
                var r = "__satellite__"
                  , i = D.dataCache
                  , o = e[r];
                o || (o = e[r] = D.uuid++);
                var s = i[o];
                if (s || (s = i[o] = {}),
                n === a)
                    return s[t];
                s[t] = n
            }
        },
        uuid: 1,
        dataCache: {},
        keys: function(e) {
            var t = [];
            for (var n in e)
                e.hasOwnProperty(n) && t.push(n);
            return t
        },
        values: function(e) {
            var t = [];
            for (var n in e)
                e.hasOwnProperty(n) && t.push(e[n]);
            return t
        },
        isArray: Array.isArray || function(e) {
            return "[object Array]" === O.apply(e)
        }
        ,
        isObject: function(e) {
            return null != e && !D.isArray(e) && "object" == typeof e
        },
        isString: function(e) {
            return "string" == typeof e
        },
        isNumber: function(e) {
            return "[object Number]" === O.apply(e) && !D.isNaN(e)
        },
        isNaN: function(e) {
            return e != e
        },
        isRegex: function(e) {
            return e instanceof RegExp
        },
        isLinkTag: function(e) {
            return !(!e || !e.nodeName || "a" !== e.nodeName.toLowerCase())
        },
        each: function(e, t, n) {
            for (var a = 0, r = e.length; a < r; a++)
                t.call(n, e[a], a, e)
        },
        map: function(e, t, n) {
            for (var a = [], r = 0, i = e.length; r < i; r++)
                a.push(t.call(n, e[r], r, e));
            return a
        },
        filter: function(e, t, n) {
            for (var a = [], r = 0, i = e.length; r < i; r++) {
                var o = e[r];
                t.call(n, o, r, e) && a.push(o)
            }
            return a
        },
        any: function(e, t, n) {
            for (var a = 0, r = e.length; a < r; a++) {
                var i = e[a];
                if (t.call(n, i, a, e))
                    return !0
            }
            return !1
        },
        every: function(e, t, n) {
            for (var a = !0, r = 0, i = e.length; r < i; r++) {
                var o = e[r];
                a = a && t.call(n, o, r, e)
            }
            return a
        },
        contains: function(e, t) {
            return -1 !== D.indexOf(e, t)
        },
        indexOf: function(e, t) {
            if (e.indexOf)
                return e.indexOf(t);
            for (var n = e.length; n--; )
                if (t === e[n])
                    return n;
            return -1
        },
        find: function(e, t, n) {
            if (!e)
                return null;
            for (var a = 0, r = e.length; a < r; a++) {
                var i = e[a];
                if (t.call(n, i, a, e))
                    return i
            }
            return null
        },
        textMatch: function(e, t) {
            if (null == t)
                throw new Error("Illegal Argument: Pattern is not present");
            return null != e && ("string" == typeof t ? e === t : t instanceof RegExp && t.test(e))
        },
        stringify: function(e, t) {
            if (t = t || [],
            D.isObject(e)) {
                if (D.contains(t, e))
                    return "<Cycle>";
                t.push(e)
            }
            if (D.isArray(e))
                return "[" + D.map(e, function(e) {
                    return D.stringify(e, t)
                }).join(",") + "]";
            if (D.isString(e))
                return '"' + String(e) + '"';
            if (D.isObject(e)) {
                var n = [];
                for (var a in e)
                    e.hasOwnProperty(a) && n.push(a + ": " + D.stringify(e[a], t));
                return "{" + n.join(", ") + "}"
            }
            return String(e)
        },
        trim: function(e) {
            return null == e ? null : e.trim ? e.trim() : e.replace(/^ */, "").replace(/ *$/, "")
        },
        bind: function(e, t) {
            return function() {
                return e.apply(t, arguments)
            }
        },
        throttle: function(e, t) {
            var n = null;
            return function() {
                var a = this
                  , r = arguments;
                clearTimeout(n),
                n = setTimeout(function() {
                    e.apply(a, r)
                }, t)
            }
        },
        domReady: function(t) {
            function a(e) {
                for (g = 1; e = i.shift(); )
                    e()
            }
            var r, i = [], o = !1, s = n, c = s.documentElement, l = c.doScroll, u = "DOMContentLoaded", d = "addEventListener", p = "onreadystatechange", g = /^loade|^c/.test(s.readyState);
            return s[d] && s[d](u, r = function() {
                s.removeEventListener(u, r, o),
                a()
            }
            , o),
            l && s.attachEvent(p, r = function() {
                /^c/.test(s.readyState) && (s.detachEvent(p, r),
                a())
            }
            ),
            t = l ? function(n) {
                self != top ? g ? n() : i.push(n) : function() {
                    try {
                        c.doScroll("left")
                    } catch (e) {
                        return setTimeout(function() {
                            t(n)
                        }, 50)
                    }
                    n()
                }()
            }
            : function(e) {
                g ? e() : i.push(e)
            }
        }(),
        loadScript: function(e, t) {
            var a = n.createElement("script");
            D.scriptOnLoad(e, a, t),
            a.src = e,
            n.getElementsByTagName("head")[0].appendChild(a)
        },
        scriptOnLoad: function(e, t, n) {
            function a(e) {
                e && D.logError(e),
                n && n(e)
            }
            "onload"in t ? (t.onload = function() {
                a()
            }
            ,
            t.onerror = function() {
                a(new Error("Failed to load script " + e))
            }
            ) : "readyState"in t && (t.onreadystatechange = function() {
                var e = t.readyState;
                "loaded" !== e && "complete" !== e || (t.onreadystatechange = null,
                a())
            }
            )
        },
        loadScriptOnce: function(e, t) {
            D.loadedScriptRegistry[e] || D.loadScript(e, function(n) {
                n || (D.loadedScriptRegistry[e] = !0),
                t && t(n)
            })
        },
        loadedScriptRegistry: {},
        loadScriptSync: function(e) {
            n.write ? D.domReadyFired ? D.notify('Cannot load sync the "' + e + '" script after DOM Ready.', 1) : (e.indexOf('"') > -1 && (e = encodeURI(e)),
            n.write('<script src="' + e + '"></script>')) : D.notify('Cannot load sync the "' + e + '" script because "document.write" is not available', 1)
        },
        pushAsyncScript: function(e) {
            D.tools["default"].pushAsyncScript(e)
        },
        pushBlockingScript: function(e) {
            D.tools["default"].pushBlockingScript(e)
        },
        addEventHandler: t.addEventListener ? function(e, t, n) {
            e.addEventListener(t, n, !1)
        }
        : function(e, t, n) {
            e.attachEvent("on" + t, n)
        }
        ,
        removeEventHandler: t.removeEventListener ? function(e, t, n) {
            e.removeEventListener(t, n, !1)
        }
        : function(e, t, n) {
            e.detachEvent("on" + t, n)
        }
        ,
        preventDefault: t.addEventListener ? function(e) {
            e.preventDefault()
        }
        : function(e) {
            e.returnValue = !1
        }
        ,
        stopPropagation: function(e) {
            e.cancelBubble = !0,
            e.stopPropagation && e.stopPropagation()
        },
        containsElement: function(e, t) {
            return e.contains ? e.contains(t) : !!(16 & e.compareDocumentPosition(t))
        },
        matchesCss: function(a) {
            function r(e, t) {
                var n = t.tagName;
                return !!n && e.toLowerCase() === n.toLowerCase()
            }
            var i = a.matchesSelector || a.mozMatchesSelector || a.webkitMatchesSelector || a.oMatchesSelector || a.msMatchesSelector;
            return i ? function(a, r) {
                if (r === n || r === t)
                    return !1;
                try {
                    return i.call(r, a)
                } catch (e) {
                    return !1
                }
            }
            : a.querySelectorAll ? function(t, n) {
                if (!n.parentNode)
                    return !1;
                if (t.match(/^[a-z]+$/i))
                    return r(t, n);
                try {
                    for (var a = n.parentNode.querySelectorAll(t), i = a.length; i--; )
                        if (a[i] === n)
                            return !0
                } catch (e) {}
                return !1
            }
            : function(t, n) {
                if (t.match(/^[a-z]+$/i))
                    return r(t, n);
                try {
                    return D.Sizzle.matches(t, [n]).length > 0
                } catch (e) {
                    return !1
                }
            }
        }(n.documentElement),
        cssQuery: (_ = n,
        _.querySelectorAll ? function(t, n) {
            var a;
            try {
                a = _.querySelectorAll(t)
            } catch (e) {
                a = []
            }
            n(a)
        }
        : function(t, n) {
            if (D.Sizzle) {
                var a;
                try {
                    a = D.Sizzle(t)
                } catch (e) {
                    a = []
                }
                n(a)
            } else
                D.sizzleQueue.push([t, n])
        }
        ),
        hasAttr: function(e, t) {
            return e.hasAttribute ? e.hasAttribute(t) : e[t] !== a
        },
        inherit: function(e, t) {
            var n = function() {};
            n.prototype = t.prototype,
            e.prototype = new n,
            e.prototype.constructor = e
        },
        extend: function(e, t) {
            for (var n in t)
                t.hasOwnProperty(n) && (e[n] = t[n])
        },
        toArray: function() {
            try {
                var t = Array.prototype.slice;
                return t.call(n.documentElement.childNodes, 0)[0].nodeType,
                function(e) {
                    return t.call(e, 0)
                }
            } catch (e) {
                return function(e) {
                    for (var t = [], n = 0, a = e.length; n < a; n++)
                        t.push(e[n]);
                    return t
                }
            }
        }(),
        equalsIgnoreCase: function(e, t) {
            return null == e ? null == t : null != t && String(e).toLowerCase() === String(t).toLowerCase()
        },
        poll: function(e, t, n) {
            function a() {
                D.isNumber(n) && r++ >= n || e() || setTimeout(a, t)
            }
            var r = 0;
            t = t || 1e3,
            a()
        },
        escapeForHtml: function(e) {
            return e ? String(e).replace(/\&/g, "&amp;").replace(/\</g, "&lt;").replace(/\>/g, "&gt;").replace(/\"/g, "&quot;").replace(/\'/g, "&#x27;").replace(/\//g, "&#x2F;") : e
        }
    };
    D.availableTools = {},
    D.availableEventEmitters = [],
    D.fireOnceEvents = ["condition", "elementexists"],
    D.initEventEmitters = function() {
        D.eventEmitters = D.map(D.availableEventEmitters, function(e) {
            return new e
        })
    }
    ,
    D.eventEmitterBackgroundTasks = function() {
        D.each(D.eventEmitters, function(e) {
            "backgroundTasks"in e && e.backgroundTasks()
        })
    }
    ,
    D.initTools = function(e) {
        var t = {
            "default": new y
        }
          , n = D.settings.euCookieName || "sat_track";
        for (var a in e)
            if (e.hasOwnProperty(a)) {
                var r, i, o;
                if ((r = e[a]).euCookie)
                    if ("true" !== D.readCookie(n))
                        continue;
                if (!(i = D.availableTools[r.engine])) {
                    var s = [];
                    for (var c in D.availableTools)
                        D.availableTools.hasOwnProperty(c) && s.push(c);
                    throw new Error("No tool engine named " + r.engine + ", available: " + s.join(",") + ".")
                }
                (o = new i(r)).id = a,
                t[a] = o
            }
        return t
    }
    ,
    D.preprocessArguments = function(e, t, n, a, r) {
        function i(e) {
            return a && D.isString(e) ? e.toLowerCase() : e
        }
        function o(e) {
            var c = {};
            for (var l in e)
                if (e.hasOwnProperty(l)) {
                    var u = e[l];
                    D.isObject(u) ? c[l] = o(u) : D.isArray(u) ? c[l] = s(u, a) : c[l] = i(D.replace(u, t, n, r))
                }
            return c
        }
        function s(e) {
            for (var a = [], r = 0, s = e.length; r < s; r++) {
                var c = e[r];
                D.isString(c) ? c = i(D.replace(c, t, n)) : c && c.constructor === Object && (c = o(c)),
                a.push(c)
            }
            return a
        }
        return e ? s(e, a) : e
    }
    ,
    D.execute = function(t, n, a, r) {
        function i(i) {
            var o = r[i || "default"];
            if (o)
                try {
                    o.triggerCommand(t, n, a)
                } catch (e) {
                    D.logError(e)
                }
        }
        if (!_satellite.settings.hideActivity)
            if (r = r || D.tools,
            t.engine) {
                var o = t.engine;
                for (var s in r)
                    if (r.hasOwnProperty(s)) {
                        var c = r[s];
                        c.settings && c.settings.engine === o && i(s)
                    }
            } else
                t.tool instanceof Array ? D.each(t.tool, function(e) {
                    i(e)
                }) : i(t.tool)
    }
    ,
    D.Logger = {
        outputEnabled: !1,
        messages: [],
        keepLimit: 100,
        flushed: !1,
        LEVELS: [null, null, "log", "info", "warn", "error"],
        message: function(e, t) {
            var n = this.LEVELS[t] || "log";
            this.messages.push([n, e]),
            this.messages.length > this.keepLimit && this.messages.shift(),
            this.outputEnabled && this.echo(n, e)
        },
        getHistory: function() {
            return this.messages
        },
        clearHistory: function() {
            this.messages = []
        },
        setOutputState: function(e) {
            this.outputEnabled != e && (this.outputEnabled = e,
            e ? this.flush() : this.flushed = !1)
        },
        echo: function(e, n) {
            t.console && t.console[e]("SATELLITE: " + n)
        },
        flush: function() {
            this.flushed || (D.each(this.messages, function(e) {
                !0 !== e[2] && (this.echo(e[0], e[1]),
                e[2] = !0)
            }, this),
            this.flushed = !0)
        }
    },
    D.notify = D.bind(D.Logger.message, D.Logger),
    D.cleanText = function(e) {
        return null == e ? null : D.trim(e).replace(/\s+/g, " ")
    }
    ,
    D.cleanText.legacy = function(e) {
        return null == e ? null : D.trim(e).replace(/\s{2,}/g, " ").replace(/[^\000-\177]*/g, "")
    }
    ,
    D.text = function(e) {
        return e.textContent || e.innerText
    }
    ,
    D.specialProperties = {
        text: D.text,
        cleanText: function(e) {
            return D.cleanText(D.text(e))
        }
    },
    D.getObjectProperty = function(e, t, n) {
        for (var r, i = t.split("."), o = e, s = D.specialProperties, c = 0, l = i.length; c < l; c++) {
            if (null == o)
                return a;
            var u = i[c];
            if (n && "@" === u.charAt(0))
                o = s[u.slice(1)](o);
            else if (o.getAttribute && (r = u.match(/^getAttribute\((.+)\)$/))) {
                var d = r[1];
                o = o.getAttribute(d)
            } else
                o = o[u]
        }
        return o
    }
    ,
    D.getToolsByType = function(e) {
        if (!e)
            throw new Error("Tool type is missing");
        var t = [];
        for (var n in D.tools)
            if (D.tools.hasOwnProperty(n)) {
                var a = D.tools[n];
                a.settings && a.settings.engine === e && t.push(a)
            }
        return t
    }
    ,
    D.setVar = function() {
        var e = D.data.customVars;
        if (null == e && (D.data.customVars = {},
        e = D.data.customVars),
        "string" == typeof arguments[0])
            e[arguments[0]] = arguments[1];
        else if (arguments[0]) {
            var t = arguments[0];
            for (var n in t)
                t.hasOwnProperty(n) && (e[n] = t[n])
        }
    }
    ,
    D.dataElementSafe = function(e, t) {
        if (arguments.length > 2) {
            var n = arguments[2];
            "pageview" === t ? D.dataElementSafe.pageviewCache[e] = n : "session" === t ? D.setCookie("_sdsat_" + e, n) : "visitor" === t && D.setCookie("_sdsat_" + e, n, 730)
        } else {
            if ("pageview" === t)
                return D.dataElementSafe.pageviewCache[e];
            if ("session" === t || "visitor" === t)
                return D.readCookie("_sdsat_" + e)
        }
    }
    ,
    D.dataElementSafe.pageviewCache = {},
    D.realGetDataElement = function(e) {
        var n;
        return e.selector ? D.hasSelector && D.cssQuery(e.selector, function(t) {
            if (t.length > 0) {
                var a = t[0];
                "text" === e.property ? n = a.innerText || a.textContent : e.property in a ? n = a[e.property] : D.hasAttr(a, e.property) && (n = a.getAttribute(e.property))
            }
        }) : e.queryParam ? n = e.ignoreCase ? D.getQueryParamCaseInsensitive(e.queryParam) : D.getQueryParam(e.queryParam) : e.cookie ? n = D.readCookie(e.cookie) : e.jsVariable ? n = D.getObjectProperty(t, e.jsVariable) : e.customJS ? n = e.customJS() : e.contextHub && (n = e.contextHub()),
        D.isString(n) && e.cleanText && (n = D.cleanText(n)),
        n
    }
    ,
    D.getDataElement = function(e, t, n) {
        if (null == (n = n || D.dataElements[e]))
            return D.settings.undefinedVarsReturnEmpty ? "" : null;
        var r = D.realGetDataElement(n);
        return r === a && n.storeLength ? r = D.dataElementSafe(e, n.storeLength) : r !== a && n.storeLength && D.dataElementSafe(e, n.storeLength, r),
        r || t || (r = n["default"] || ""),
        D.isString(r) && n.forceLowerCase && (r = r.toLowerCase()),
        r
    }
    ,
    D.getVar = function(e, r, i) {
        var o, s, c = D.data.customVars, l = i ? i.target || i.srcElement : null, u = {
            uri: D.URI(),
            protocol: n.location.protocol,
            hostname: n.location.hostname
        };
        if (D.dataElements && e in D.dataElements)
            return D.getDataElement(e);
        if ((s = u[e.toLowerCase()]) === a)
            if ("this." === e.substring(0, 5))
                e = e.slice(5),
                s = D.getObjectProperty(r, e, !0);
            else if ("event." === e.substring(0, 6))
                e = e.slice(6),
                s = D.getObjectProperty(i, e);
            else if ("target." === e.substring(0, 7))
                e = e.slice(7),
                s = D.getObjectProperty(l, e);
            else if ("window." === e.substring(0, 7))
                e = e.slice(7),
                s = D.getObjectProperty(t, e);
            else if ("param." === e.substring(0, 6))
                e = e.slice(6),
                s = D.getQueryParam(e);
            else if (o = e.match(/^rand([0-9]+)$/)) {
                var d = Number(o[1])
                  , p = (Math.random() * (Math.pow(10, d) - 1)).toFixed(0);
                s = Array(d - p.length + 1).join("0") + p
            } else
                s = D.getObjectProperty(c, e);
        return s
    }
    ,
    D.getVars = function(e, t, n) {
        var a = {};
        return D.each(e, function(e) {
            a[e] = D.getVar(e, t, n)
        }),
        a
    }
    ,
    D.replace = function(e, t, n, a) {
        return "string" != typeof e ? e : e.replace(/%(.*?)%/g, function(e, r) {
            var i = D.getVar(r, t, n);
            return null == i ? D.settings.undefinedVarsReturnEmpty ? "" : e : a ? D.escapeForHtml(i) : i
        })
    }
    ,
    D.escapeHtmlParams = function(e) {
        return e.escapeHtml = !0,
        e
    }
    ,
    D.searchVariables = function(e, t, n) {
        if (!e || 0 === e.length)
            return "";
        for (var a = [], r = 0, i = e.length; r < i; r++) {
            var o = e[r]
              , s = D.getVar(o, t, n);
            a.push(o + "=" + escape(s))
        }
        return "?" + a.join("&")
    }
    ,
    D.fireRule = function(e, t, n) {
        var a = e.trigger;
        if (a) {
            for (var r = 0, i = a.length; r < i; r++) {
                var o = a[r];
                D.execute(o, t, n)
            }
            D.contains(D.fireOnceEvents, e.event) && (e.expired = !0)
        }
    }
    ,
    D.isLinked = function(e) {
        for (var t = e; t; t = t.parentNode)
            if (D.isLinkTag(t))
                return !0;
        return !1
    }
    ,
    D.firePageLoadEvent = function(e) {
        for (var t = n.location, a = {
            type: e,
            target: t
        }, r = D.pageLoadRules, i = D.evtHandlers[a.type], o = r.length; o--; ) {
            var s = r[o];
            D.ruleMatches(s, a, t) && (D.notify('Rule "' + s.name + '" fired.', 1),
            D.fireRule(s, t, a))
        }
        for (var c in D.tools)
            if (D.tools.hasOwnProperty(c)) {
                var l = D.tools[c];
                l.endPLPhase && l.endPLPhase(e)
            }
        i && D.each(i, function(e) {
            e(a)
        })
    }
    ,
    D.track = function(e) {
        e = e.replace(/^\s*/, "").replace(/\s*$/, "");
        for (var t = 0; t < D.directCallRules.length; t++) {
            var n = D.directCallRules[t];
            if (n.name === e)
                return D.notify('Direct call Rule "' + e + '" fired.', 1),
                void D.fireRule(n, location, {
                    type: e
                })
        }
        D.notify('Direct call Rule "' + e + '" not found.', 1)
    }
    ,
    D.basePath = function() {
        return D.data.host ? ("https:" === n.location.protocol ? "https://" + D.data.host.https : "http://" + D.data.host.http) + "/" : this.settings.basePath
    }
    ,
    D.setLocation = function(e) {
        t.location = e
    }
    ,
    D.parseQueryParams = function(e) {
        var t = function(e) {
            var t = e;
            try {
                t = decodeURIComponent(e)
            } catch (n) {}
            return t
        };
        if ("" === e || !1 === D.isString(e))
            return {};
        0 === e.indexOf("?") && (e = e.substring(1));
        var n = {}
          , a = e.split("&");
        return D.each(a, function(e) {
            (e = e.split("="))[1] && (n[t(e[0])] = t(e[1]))
        }),
        n
    }
    ,
    D.getCaseSensitivityQueryParamsMap = function(e) {
        var t = D.parseQueryParams(e)
          , n = {};
        for (var a in t)
            t.hasOwnProperty(a) && (n[a.toLowerCase()] = t[a]);
        return {
            normal: t,
            caseInsensitive: n
        }
    }
    ,
    D.updateQueryParams = function() {
        D.QueryParams = D.getCaseSensitivityQueryParamsMap(t.location.search)
    }
    ,
    D.updateQueryParams(),
    D.getQueryParam = function(e) {
        return D.QueryParams.normal[e]
    }
    ,
    D.getQueryParamCaseInsensitive = function(e) {
        return D.QueryParams.caseInsensitive[e.toLowerCase()]
    }
    ,
    D.encodeObjectToURI = function(e) {
        if (!1 === D.isObject(e))
            return "";
        var t = [];
        for (var n in e)
            e.hasOwnProperty(n) && t.push(encodeURIComponent(n) + "=" + encodeURIComponent(e[n]));
        return t.join("&")
    }
    ,
    D.readCookie = function(e) {
        for (var t = e + "=", r = n.cookie.split(";"), i = 0; i < r.length; i++) {
            for (var o = r[i]; " " == o.charAt(0); )
                o = o.substring(1, o.length);
            if (0 === o.indexOf(t))
                return o.substring(t.length, o.length)
        }
        return a
    }
    ,
    D.setCookie = function(e, t, a) {
        var r;
        if (a) {
            var i = new Date;
            i.setTime(i.getTime() + 24 * a * 60 * 60 * 1e3),
            r = "; expires=" + i.toGMTString()
        } else
            r = "";
        n.cookie = e + "=" + t + r + "; path=/"
    }
    ,
    D.removeCookie = function(e) {
        D.setCookie(e, "", -1)
    }
    ,
    D.getElementProperty = function(e, t) {
        if ("@" === t.charAt(0)) {
            var n = D.specialProperties[t.substring(1)];
            if (n)
                return n(e)
        }
        return "innerText" === t ? D.text(e) : t in e ? e[t] : e.getAttribute ? e.getAttribute(t) : a
    }
    ,
    D.propertiesMatch = function(e, t) {
        if (e)
            for (var n in e)
                if (e.hasOwnProperty(n)) {
                    var a = e[n]
                      , r = D.getElementProperty(t, n);
                    if ("string" == typeof a && a !== r)
                        return !1;
                    if (a instanceof RegExp && !a.test(r))
                        return !1
                }
        return !0
    }
    ,
    D.isRightClick = function(e) {
        var t;
        return e.which ? t = 3 == e.which : e.button && (t = 2 == e.button),
        t
    }
    ,
    D.ruleMatches = function(t, n, a, r) {
        var i = t.condition
          , o = t.conditions
          , s = t.property
          , c = n.type
          , l = t.value
          , u = n.target || n.srcElement
          , d = a === u;
        if (t.event !== c && ("custom" !== t.event || t.customEvent !== c))
            return !1;
        if (!D.ruleInScope(t))
            return !1;
        if ("click" === t.event && D.isRightClick(n))
            return !1;
        if (t.isDefault && r > 0)
            return !1;
        if (t.expired)
            return !1;
        if ("inview" === c && n.inviewDelay !== t.inviewDelay)
            return !1;
        if (!d && (!1 === t.bubbleFireIfParent || 0 !== r && !1 === t.bubbleFireIfChildFired))
            return !1;
        if (t.selector && !D.matchesCss(t.selector, a))
            return !1;
        if (!D.propertiesMatch(s, a))
            return !1;
        if (null != l)
            if ("string" == typeof l) {
                if (l !== a.value)
                    return !1
            } else if (!l.test(a.value))
                return !1;
        if (i)
            try {
                if (!i.call(a, n, u))
                    return D.notify('Condition for rule "' + t.name + '" not met.', 1),
                    !1
            } catch (e) {
                return D.notify('Condition for rule "' + t.name + '" not met. Error: ' + e.message, 1),
                !1
            }
        if (o) {
            var p = D.find(o, function(r) {
                try {
                    return !r.call(a, n, u)
                } catch (e) {
                    return D.notify('Condition for rule "' + t.name + '" not met. Error: ' + e.message, 1),
                    !0
                }
            });
            if (p)
                return D.notify("Condition " + p.toString() + ' for rule "' + t.name + '" not met.', 1),
                !1
        }
        return !0
    }
    ,
    D.evtHandlers = {},
    D.bindEvent = function(e, t) {
        var n = D.evtHandlers;
        n[e] || (n[e] = []),
        n[e].push(t)
    }
    ,
    D.whenEvent = D.bindEvent,
    D.unbindEvent = function(e, t) {
        var n = D.evtHandlers;
        if (n[e]) {
            var a = D.indexOf(n[e], t);
            n[e].splice(a, 1)
        }
    }
    ,
    D.bindEventOnce = function(e, t) {
        var n = function() {
            D.unbindEvent(e, n),
            t.apply(null, arguments)
        };
        D.bindEvent(e, n)
    }
    ,
    D.isVMLPoisoned = function(t) {
        if (!t)
            return !1;
        try {
            t.nodeName
        } catch (e) {
            if ("Attribute only valid on v:image" === e.message)
                return !0
        }
        return !1
    }
    ,
    D.handleEvent = function(e) {
        if (!D.$data(e, "eventProcessed")) {
            var t = e.type.toLowerCase()
              , n = e.target || e.srcElement
              , a = 0
              , r = D.rules
              , i = (D.tools,
            D.evtHandlers[e.type]);
            if (D.isVMLPoisoned(n))
                D.notify("detected " + t + " on poisoned VML element, skipping.", 1);
            else {
                i && D.each(i, function(t) {
                    t(e)
                }),
                n && n.nodeName ? D.notify("detected " + t + " on " + n.nodeName, 1) : D.notify("detected " + t, 1);
                for (var o = n; o; o = o.parentNode) {
                    var s = !1;
                    if (D.each(r, function(t) {
                        D.ruleMatches(t, e, o, a) && (D.notify('Rule "' + t.name + '" fired.', 1),
                        D.fireRule(t, o, e),
                        a++,
                        t.bubbleStop && (s = !0))
                    }),
                    s)
                        break
                }
                D.$data(e, "eventProcessed", !0)
            }
        }
    }
    ,
    D.onEvent = n.querySelectorAll ? function(e) {
        D.handleEvent(e)
    }
    : (k = [],
    (T = function(e) {
        e.selector ? k.push(e) : D.handleEvent(e)
    }
    ).pendingEvents = k,
    T),
    D.fireEvent = function(e, t) {
        D.onEvent({
            type: e,
            target: t
        })
    }
    ,
    D.registerEvents = function(e, t) {
        for (var n = t.length - 1; n >= 0; n--) {
            var a = t[n];
            D.$data(e, a + ".tracked") || (D.addEventHandler(e, a, D.onEvent),
            D.$data(e, a + ".tracked", !0))
        }
    }
    ,
    D.registerEventsForTags = function(e, t) {
        for (var a = e.length - 1; a >= 0; a--)
            for (var r = e[a], i = n.getElementsByTagName(r), o = i.length - 1; o >= 0; o--)
                D.registerEvents(i[o], t)
    }
    ,
    D.setListeners = function() {
        var e = ["click", "submit"];
        D.each(D.rules, function(t) {
            "custom" === t.event && t.hasOwnProperty("customEvent") && !D.contains(e, t.customEvent) && e.push(t.customEvent)
        }),
        D.registerEvents(n, e)
    }
    ,
    D.getUniqueRuleEvents = function() {
        return D._uniqueRuleEvents || (D._uniqueRuleEvents = [],
        D.each(D.rules, function(e) {
            -1 === D.indexOf(D._uniqueRuleEvents, e.event) && D._uniqueRuleEvents.push(e.event)
        })),
        D._uniqueRuleEvents
    }
    ,
    D.setFormListeners = function() {
        if (!D._relevantFormEvents) {
            var e = ["change", "focus", "blur", "keypress"];
            D._relevantFormEvents = D.filter(D.getUniqueRuleEvents(), function(t) {
                return -1 !== D.indexOf(e, t)
            })
        }
        D._relevantFormEvents.length && D.registerEventsForTags(["input", "select", "textarea", "button"], D._relevantFormEvents)
    }
    ,
    D.setVideoListeners = function() {
        if (!D._relevantVideoEvents) {
            var e = ["play", "pause", "ended", "volumechange", "stalled", "loadeddata"];
            D._relevantVideoEvents = D.filter(D.getUniqueRuleEvents(), function(t) {
                return -1 !== D.indexOf(e, t)
            })
        }
        D._relevantVideoEvents.length && D.registerEventsForTags(["video"], D._relevantVideoEvents)
    }
    ,
    D.readStoredSetting = function(n) {
        try {
            return n = "sdsat_" + n,
            t.localStorage.getItem(n)
        } catch (e) {
            return D.notify("Cannot read stored setting from localStorage: " + e.message, 2),
            null
        }
    }
    ,
    D.loadStoredSettings = function() {
        var e = D.readStoredSetting("debug")
          , t = D.readStoredSetting("hide_activity");
        e && (D.settings.notifications = "true" === e),
        t && (D.settings.hideActivity = "true" === t)
    }
    ,
    D.isRuleActive = function(e, t) {
        function n(e, t) {
            return t = r(t, {
                hour: e[g](),
                minute: e[f]()
            }),
            Math.floor(Math.abs((e.getTime() - t.getTime()) / 864e5))
        }
        function a(e, t) {
            function n(e) {
                return 12 * e[d]() + e[p]()
            }
            return Math.abs(n(e) - n(t))
        }
        function r(e, t) {
            var n = new Date(e.getTime());
            for (var a in t)
                if (t.hasOwnProperty(a)) {
                    var r = t[a];
                    switch (a) {
                    case "hour":
                        n[m](r);
                        break;
                    case "minute":
                        n[h](r);
                        break;
                    case "date":
                        n[v](r)
                    }
                }
            return n
        }
        function i(e, t) {
            return 60 * e[g]() + e[f]() > 60 * t[g]() + t[f]()
        }
        function o(e, t) {
            return 60 * e[g]() + e[f]() < 60 * t[g]() + t[f]()
        }
        var s = e.schedule;
        if (!s)
            return !0;
        var c = s.utc
          , l = c ? "getUTCDate" : "getDate"
          , u = c ? "getUTCDay" : "getDay"
          , d = c ? "getUTCFullYear" : "getFullYear"
          , p = c ? "getUTCMonth" : "getMonth"
          , g = c ? "getUTCHours" : "getHours"
          , f = c ? "getUTCMinutes" : "getMinutes"
          , m = c ? "setUTCHours" : "setHours"
          , h = c ? "setUTCMinutes" : "setMinutes"
          , v = c ? "setUTCDate" : "setDate";
        if (t = t || new Date,
        s.repeat) {
            if (i(s.start, t))
                return !1;
            if (o(s.end, t))
                return !1;
            if (t < s.start)
                return !1;
            if (s.endRepeat && t >= s.endRepeat)
                return !1;
            if ("daily" === s.repeat) {
                if (s.repeatEvery)
                    if (n(s.start, t) % s.repeatEvery != 0)
                        return !1
            } else if ("weekly" === s.repeat) {
                if (s.days) {
                    if (!D.contains(s.days, t[u]()))
                        return !1
                } else if (s.start[u]() !== t[u]())
                    return !1;
                if (s.repeatEvery)
                    if (n(s.start, t) % (7 * s.repeatEvery) != 0)
                        return !1
            } else if ("monthly" === s.repeat) {
                if (s.repeatEvery)
                    if (a(s.start, t) % s.repeatEvery != 0)
                        return !1;
                if (s.nthWeek && s.mthDay) {
                    if (s.mthDay !== t[u]())
                        return !1;
                    var y = Math.floor((t[l]() - t[u]() + 1) / 7);
                    if (s.nthWeek !== y)
                        return !1
                } else if (s.start[l]() !== t[l]())
                    return !1
            } else if ("yearly" === s.repeat) {
                if (s.start[p]() !== t[p]())
                    return !1;
                if (s.start[l]() !== t[l]())
                    return !1;
                if (s.repeatEvery)
                    if (Math.abs(s.start[d]() - t[d]()) % s.repeatEvery != 0)
                        return !1
            }
        } else {
            if (s.start > t)
                return !1;
            if (s.end < t)
                return !1
        }
        return !0
    }
    ,
    D.isOutboundLink = function(e) {
        if (!e.getAttribute("href"))
            return !1;
        var t = e.hostname
          , n = (e.href,
        e.protocol);
        return ("http:" === n || "https:" === n) && (!D.any(D.settings.domainList, function(e) {
            return D.isSubdomainOf(t, e)
        }) && t !== location.hostname)
    }
    ,
    D.isLinkerLink = function(e) {
        return !(!e.getAttribute || !e.getAttribute("href")) && (D.hasMultipleDomains() && e.hostname != location.hostname && !e.href.match(/^javascript/i) && !D.isOutboundLink(e))
    }
    ,
    D.isSubdomainOf = function(e, t) {
        if (e === t)
            return !0;
        var n = e.length - t.length;
        return n > 0 && D.equalsIgnoreCase(e.substring(n), t)
    }
    ,
    D.getVisitorId = function() {
        var e = D.getToolsByType("visitor_id");
        return 0 === e.length ? null : e[0].getInstance()
    }
    ,
    D.URI = function() {
        var e = n.location.pathname + n.location.search;
        return D.settings.forceLowerCase && (e = e.toLowerCase()),
        e
    }
    ,
    D.URL = function() {
        var e = n.location.href;
        return D.settings.forceLowerCase && (e = e.toLowerCase()),
        e
    }
    ,
    D.filterRules = function() {
        function e(e) {
            return !!D.isRuleActive(e)
        }
        D.rules = D.filter(D.rules, e),
        D.pageLoadRules = D.filter(D.pageLoadRules, e)
    }
    ,
    D.ruleInScope = function(e, t) {
        function a(e, t) {
            function n(e) {
                return t.match(e)
            }
            var a = e.include
              , i = e.exclude;
            if (a && r(a, t))
                return !0;
            if (i) {
                if (D.isString(i) && i === t)
                    return !0;
                if (D.isArray(i) && D.any(i, n))
                    return !0;
                if (D.isRegex(i) && n(i))
                    return !0
            }
            return !1
        }
        function r(e, t) {
            function n(e) {
                return t.match(e)
            }
            return !(!D.isString(e) || e === t) || (!(!D.isArray(e) || D.any(e, n)) || !(!D.isRegex(e) || n(e)))
        }
        t = t || n.location;
        var i = e.scope;
        if (!i)
            return !0;
        var o = i.URI
          , s = i.subdomains
          , c = i.domains
          , l = i.protocols
          , u = i.hashes;
        return (!o || !a(o, t.pathname + t.search)) && ((!s || !a(s, t.hostname)) && ((!c || !r(c, t.hostname)) && ((!l || !r(l, t.protocol)) && (!u || !a(u, t.hash)))))
    }
    ,
    D.backgroundTasks = function() {
        new Date;
        D.setFormListeners(),
        D.setVideoListeners(),
        D.loadStoredSettings(),
        D.registerNewElementsForDynamicRules(),
        D.eventEmitterBackgroundTasks();
        new Date
    }
    ,
    D.registerNewElementsForDynamicRules = function() {
        function e(t, n) {
            var a = e.cache[t];
            if (a)
                return n(a);
            D.cssQuery(t, function(a) {
                e.cache[t] = a,
                n(a)
            })
        }
        e.cache = {},
        D.each(D.dynamicRules, function(t) {
            e(t.selector, function(e) {
                D.each(e, function(e) {
                    var n = "custom" === t.event ? t.customEvent : t.event;
                    D.$data(e, "dynamicRules.seen." + n) || (D.$data(e, "dynamicRules.seen." + n, !0),
                    D.propertiesMatch(t.property, e) && D.registerEvents(e, [n]))
                })
            })
        })
    }
    ,
    D.ensureCSSSelector = function() {
        n.querySelectorAll ? D.hasSelector = !0 : (D.loadingSizzle = !0,
        D.sizzleQueue = [],
        D.loadScript(D.basePath() + "selector.js", function() {
            if (D.Sizzle) {
                var e = D.onEvent.pendingEvents;
                D.each(e, function(e) {
                    D.handleEvent(e)
                }, this),
                D.onEvent = D.handleEvent,
                D.hasSelector = !0,
                delete D.loadingSizzle,
                D.each(D.sizzleQueue, function(e) {
                    D.cssQuery(e[0], e[1])
                }),
                delete D.sizzleQueue
            } else
                D.logError(new Error("Failed to load selector.js"))
        }))
    }
    ,
    D.errors = [],
    D.logError = function(e) {
        D.errors.push(e),
        D.notify(e.name + " - " + e.message, 5)
    }
    ,
    D.pageBottom = function() {
        D.initialized && (D.pageBottomFired = !0,
        D.firePageLoadEvent("pagebottom"))
    }
    ,
    D.stagingLibraryOverride = function() {
        if ("true" === D.readStoredSetting("stagingLibrary")) {
            for (var e, t, a, r = n.getElementsByTagName("script"), i = /^(.*)satelliteLib-([a-f0-9]{40})\.js$/, o = /^(.*)satelliteLib-([a-f0-9]{40})-staging\.js$/, s = 0, c = r.length; s < c && (!(a = r[s].getAttribute("src")) || (e || (e = a.match(i)),
            t || (t = a.match(o)),
            !t)); s++)
                ;
            if (e && !t) {
                var l = e[1] + "satelliteLib-" + e[2] + "-staging.js";
                if (n.write)
                    n.write('<script src="' + l + '"></script>');
                else {
                    var u = n.createElement("script");
                    u.src = l,
                    n.head.appendChild(u)
                }
                return !0
            }
        }
        return !1
    }
    ,
    D.checkAsyncInclude = function() {
        t.satellite_asyncLoad && D.notify('You may be using the async installation of Satellite. In-page HTML and the "pagebottom" event will not work. Please update your Satellite installation for these features.', 5)
    }
    ,
    D.hasMultipleDomains = function() {
        return !!D.settings.domainList && D.settings.domainList.length > 1
    }
    ,
    D.handleOverrides = function() {
        if (I)
            for (var e in I)
                I.hasOwnProperty(e) && (D.data[e] = I[e])
    }
    ,
    D.privacyManagerParams = function() {
        var e = {};
        D.extend(e, D.settings.privacyManagement);
        var t = [];
        for (var n in D.tools)
            if (D.tools.hasOwnProperty(n)) {
                var a = D.tools[n]
                  , r = a.settings;
                if (!r)
                    continue;
                "sc" === r.engine && t.push(a)
            }
        var i = D.filter(D.map(t, function(e) {
            return e.getTrackingServer()
        }), function(e) {
            return null != e
        });
        e.adobeAnalyticsTrackingServers = i;
        for (var o = ["bannerText", "headline", "introductoryText", "customCSS"], s = 0; s < o.length; s++) {
            var c = o[s]
              , l = e[c];
            if (l)
                if ("text" === l.type)
                    e[c] = l.value;
                else {
                    if ("data" !== l.type)
                        throw new Error("Invalid type: " + l.type);
                    e[c] = D.getVar(l.value)
                }
        }
        return e
    }
    ,
    D.prepareLoadPrivacyManager = function() {
        function e(e) {
            function t() {
                ++i === r.length && (n(),
                clearTimeout(o),
                e())
            }
            function n() {
                D.each(r, function(e) {
                    D.unbindEvent(e.id + ".load", t)
                })
            }
            function a() {
                n(),
                e()
            }
            var r = D.filter(D.values(D.tools), function(e) {
                return e.settings && "sc" === e.settings.engine
            });
            if (0 === r.length)
                return e();
            var i = 0;
            D.each(r, function(e) {
                D.bindEvent(e.id + ".load", t)
            });
            var o = setTimeout(a, 5e3)
        }
        D.addEventHandler(t, "load", function() {
            e(D.loadPrivacyManager)
        })
    }
    ,
    D.loadPrivacyManager = function() {
        var e = D.basePath() + "privacy_manager.js";
        D.loadScript(e, function() {
            var e = D.privacyManager;
            e.configure(D.privacyManagerParams()),
            e.openIfRequired()
        })
    }
    ,
    D.init = function(e) {
        if (!D.stagingLibraryOverride()) {
            D.configurationSettings = e;
            var n = e.tools;
            for (var r in delete e.tools,
            e)
                e.hasOwnProperty(r) && (D[r] = e[r]);
            D.data.customVars === a && (D.data.customVars = {}),
            D.data.queryParams = D.QueryParams.normal,
            D.handleOverrides(),
            D.detectBrowserInfo(),
            D.trackVisitorInfo && D.trackVisitorInfo(),
            D.loadStoredSettings(),
            D.Logger.setOutputState(D.settings.notifications),
            D.checkAsyncInclude(),
            D.ensureCSSSelector(),
            D.filterRules(),
            D.dynamicRules = D.filter(D.rules, function(e) {
                return e.eventHandlerOnElement
            }),
            D.tools = D.initTools(n),
            D.initEventEmitters(),
            D.firePageLoadEvent("aftertoolinit"),
            D.settings.privacyManagement && D.prepareLoadPrivacyManager(),
            D.hasSelector && D.domReady(D.eventEmitterBackgroundTasks),
            D.setListeners(),
            D.domReady(function() {
                D.poll(function() {
                    D.backgroundTasks()
                }, D.settings.recheckEvery || 3e3)
            }),
            D.domReady(function() {
                D.domReadyFired = !0,
                D.pageBottomFired || D.pageBottom(),
                D.firePageLoadEvent("domready")
            }),
            D.addEventHandler(t, "load", function() {
                D.firePageLoadEvent("windowload")
            }),
            D.firePageLoadEvent("pagetop"),
            D.initialized = !0
        }
    }
    ,
    D.pageLoadPhases = ["aftertoolinit", "pagetop", "pagebottom", "domready", "windowload"],
    D.loadEventBefore = function(e, t) {
        return D.indexOf(D.pageLoadPhases, e) <= D.indexOf(D.pageLoadPhases, t)
    }
    ,
    D.flushPendingCalls = function(e) {
        e.pending && (D.each(e.pending, function(t) {
            var n = t[0]
              , a = t[1]
              , r = t[2]
              , i = t[3];
            n in e ? e[n].apply(e, [a, r].concat(i)) : e.emit ? e.emit(n, a, r, i) : D.notify("Failed to trigger " + n + " for tool " + e.id, 1)
        }),
        delete e.pending)
    }
    ,
    D.setDebug = function(n) {
        try {
            t.localStorage.setItem("sdsat_debug", n)
        } catch (e) {
            D.notify("Cannot set debug mode: " + e.message, 2)
        }
    }
    ,
    D.getUserAgent = function() {
        return navigator.userAgent
    }
    ,
    D.detectBrowserInfo = function() {
        function e(e) {
            return function(t) {
                for (var n in e) {
                    if (e.hasOwnProperty(n))
                        if (e[n].test(t))
                            return n
                }
                return "Unknown"
            }
        }
        var t = e({
            "IE Edge Mobile": /Windows Phone.*Edge/,
            "IE Edge": /Edge/,
            OmniWeb: /OmniWeb/,
            "Opera Mini": /Opera Mini/,
            "Opera Mobile": /Opera Mobi/,
            Opera: /Opera/,
            Chrome: /Chrome|CriOS|CrMo/,
            Firefox: /Firefox|FxiOS/,
            "IE Mobile": /IEMobile/,
            IE: /MSIE|Trident/,
            "Mobile Safari": /Mobile(\/[0-9A-z]+)? Safari/,
            Safari: /Safari/
        })
          , n = e({
            Blackberry: /BlackBerry|BB10/,
            "Symbian OS": /Symbian|SymbOS/,
            Maemo: /Maemo/,
            Android: /Android/,
            Linux: / Linux /,
            Unix: /FreeBSD|OpenBSD|CrOS/,
            Windows: /[\( ]Windows /,
            iOS: /iPhone|iPad|iPod/,
            MacOS: /Macintosh;/
        })
          , a = e({
            Nokia: /Symbian|SymbOS|Maemo/,
            "Windows Phone": /Windows Phone/,
            Blackberry: /BlackBerry|BB10/,
            Android: /Android/,
            iPad: /iPad/,
            iPod: /iPod/,
            iPhone: /iPhone/,
            Desktop: /.*/
        })
          , r = D.getUserAgent();
        D.browserInfo = {
            browser: t(r),
            os: n(r),
            deviceType: a(r)
        }
    }
    ,
    D.isHttps = function() {
        return "https:" == n.location.protocol
    }
    ,
    D.BaseTool = function(e) {
        this.settings = e || {},
        this.forceLowerCase = D.settings.forceLowerCase,
        "forceLowerCase"in this.settings && (this.forceLowerCase = this.settings.forceLowerCase)
    }
    ,
    D.BaseTool.prototype = {
        triggerCommand: function(e, t, n) {
            var a = this.settings || {};
            if (this.initialize && this.isQueueAvailable() && this.isQueueable(e) && n && D.loadEventBefore(n.type, a.loadOn))
                this.queueCommand(e, t, n);
            else {
                var r = e.command
                  , i = this["$" + r]
                  , o = !!i && i.escapeHtml
                  , s = D.preprocessArguments(e.arguments, t, n, this.forceLowerCase, o);
                i ? i.apply(this, [t, n].concat(s)) : this.$missing$ ? this.$missing$(r, t, n, s) : D.notify("Failed to trigger " + r + " for tool " + this.id, 1)
            }
        },
        endPLPhase: function() {},
        isQueueable: function(e) {
            return "cancelToolInit" !== e.command
        },
        isQueueAvailable: function() {
            return !this.initialized && !this.initializing
        },
        flushQueue: function() {
            this.pending && (D.each(this.pending, function(e) {
                this.triggerCommand.apply(this, e)
            }, this),
            this.pending = [])
        },
        queueCommand: function(e, t, n) {
            this.pending || (this.pending = []),
            this.pending.push([e, t, n])
        },
        $cancelToolInit: function() {
            this._cancelToolInit = !0
        }
    },
    t._satellite = D,
    D.ecommerce = {
        addItem: function() {
            var e = [].slice.call(arguments);
            D.onEvent({
                type: "ecommerce.additem",
                target: e
            })
        },
        addTrans: function() {
            var e = [].slice.call(arguments);
            D.data.saleData.sale = {
                orderId: e[0],
                revenue: e[2]
            },
            D.onEvent({
                type: "ecommerce.addtrans",
                target: e
            })
        },
        trackTrans: function() {
            D.onEvent({
                type: "ecommerce.tracktrans",
                target: []
            })
        }
    },
    D.visibility = {
        isHidden: function() {
            var e = this.getHiddenProperty();
            return !!e && n[e]
        },
        isVisible: function() {
            return !this.isHidden()
        },
        getHiddenProperty: function() {
            var e = ["webkit", "moz", "ms", "o"];
            if ("hidden"in n)
                return "hidden";
            for (var t = 0; t < e.length; t++)
                if (e[t] + "Hidden"in n)
                    return e[t] + "Hidden";
            return null
        },
        getVisibilityEvent: function() {
            var e = this.getHiddenProperty();
            return e ? e.replace(/[H|h]idden/, "") + "visibilitychange" : null
        }
    },
    r.prototype = {
        obue: !1,
        initialize: function() {
            this.attachCloseListeners()
        },
        obuePrevUnload: function() {},
        obuePrevBeforeUnload: function() {},
        newObueListener: function() {
            this.obue || (this.obue = !0,
            this.triggerBeacons())
        },
        attachCloseListeners: function() {
            this.prevUnload = t.onunload,
            this.prevBeforeUnload = t.onbeforeunload,
            t.onunload = D.bind(function(e) {
                this.prevUnload && setTimeout(D.bind(function() {
                    this.prevUnload.call(t, e)
                }, this), 1),
                this.newObueListener()
            }, this),
            t.onbeforeunload = D.bind(function(e) {
                this.prevBeforeUnload && setTimeout(D.bind(function() {
                    this.prevBeforeUnload.call(t, e)
                }, this), 1),
                this.newObueListener()
            }, this)
        },
        triggerBeacons: function() {
            D.fireEvent("leave", n)
        }
    },
    D.availableEventEmitters.push(r),
    i.prototype = {
        initialize: function() {
            var e = this.twttr;
            e && "function" == typeof e.ready && e.ready(D.bind(this.bind, this))
        },
        bind: function() {
            this.twttr.events.bind("tweet", function(e) {
                e && (D.notify("tracking a tweet button", 1),
                D.onEvent({
                    type: "twitter.tweet",
                    target: n
                }))
            })
        }
    },
    D.availableEventEmitters.push(i),
    o.prototype = {
        initialize: function() {
            this.setupHistoryAPI(),
            this.setupHashChange()
        },
        fireIfURIChanged: function() {
            var e = D.URL();
            this.lastURL !== e && (this.fireEvent(),
            this.lastURL = e)
        },
        fireEvent: function() {
            D.updateQueryParams(),
            D.onEvent({
                type: "locationchange",
                target: n
            })
        },
        setupSPASupport: function() {
            this.setupHistoryAPI(),
            this.setupHashChange()
        },
        setupHistoryAPI: function() {
            var e = t.history;
            e && (e.pushState && (this.originalPushState = e.pushState,
            e.pushState = this._pushState),
            e.replaceState && (this.originalReplaceState = e.replaceState,
            e.replaceState = this._replaceState)),
            D.addEventHandler(t, "popstate", this._onPopState)
        },
        pushState: function() {
            var e = this.originalPushState.apply(history, arguments);
            return this.onPushState(),
            e
        },
        replaceState: function() {
            var e = this.originalReplaceState.apply(history, arguments);
            return this.onReplaceState(),
            e
        },
        setupHashChange: function() {
            D.addEventHandler(t, "hashchange", this._onHashChange)
        },
        onReplaceState: function() {
            setTimeout(this._fireIfURIChanged, 0)
        },
        onPushState: function() {
            setTimeout(this._fireIfURIChanged, 0)
        },
        onPopState: function() {
            setTimeout(this._fireIfURIChanged, 0)
        },
        onHashChange: function() {
            setTimeout(this._fireIfURIChanged, 0)
        },
        uninitialize: function() {
            this.cleanUpHistoryAPI(),
            this.cleanUpHashChange()
        },
        cleanUpHistoryAPI: function() {
            history.pushState === this._pushState && (history.pushState = this.originalPushState),
            history.replaceState === this._replaceState && (history.replaceState = this.originalReplaceState),
            D.removeEventHandler(t, "popstate", this._onPopState)
        },
        cleanUpHashChange: function() {
            D.removeEventHandler(t, "hashchange", this._onHashChange)
        }
    },
    D.availableEventEmitters.push(o),
    c.prototype.getStringifiedValue = t.JSON && t.JSON.stringify || D.stringify,
    c.prototype.initPolling = function() {
        0 !== this.dataElementsNames.length && (this.dataElementsStore = this.getDataElementsValues(),
        D.poll(D.bind(this.checkDataElementValues, this), 1e3))
    }
    ,
    c.prototype.getDataElementsValues = function() {
        var e = {};
        return D.each(this.dataElementsNames, function(t) {
            var n = D.getVar(t);
            e[t] = this.getStringifiedValue(n)
        }, this),
        e
    }
    ,
    c.prototype.checkDataElementValues = function() {
        D.each(this.dataElementsNames, D.bind(function(e) {
            var t = this.getStringifiedValue(D.getVar(e));
            t !== this.dataElementsStore[e] && (this.dataElementsStore[e] = t,
            D.onEvent({
                type: "dataelementchange(" + e + ")",
                target: n
            }))
        }, this))
    }
    ,
    D.availableEventEmitters.push(c),
    l.orientationChange = function(e) {
        var n = 0 === t.orientation ? "portrait" : "landscape";
        e.orientation = n,
        D.onEvent(e)
    }
    ,
    D.availableEventEmitters.push(l),
    u.prototype = {
        backgroundTasks: function() {
            var e = this.eventHandler;
            D.each(this.rules, function(t) {
                D.cssQuery(t.selector || "video", function(t) {
                    D.each(t, function(t) {
                        D.$data(t, "videoplayed.tracked") || (D.addEventHandler(t, "timeupdate", D.throttle(e, 100)),
                        D.$data(t, "videoplayed.tracked", !0))
                    })
                })
            })
        },
        evalRule: function(e, t) {
            var n = t.event
              , a = e.seekable
              , r = a.start(0)
              , i = a.end(0)
              , o = e.currentTime
              , s = t.event.match(/^videoplayed\(([0-9]+)([s%])\)$/);
            if (s) {
                var c = s[2]
                  , l = Number(s[1])
                  , u = "%" === c ? function() {
                    return l <= 100 * (o - r) / (i - r)
                }
                : function() {
                    return l <= o - r
                }
                ;
                !D.$data(e, n) && u() && (D.$data(e, n, !0),
                D.onEvent({
                    type: n,
                    target: e
                }))
            }
        },
        onUpdateTime: function(e) {
            var t = this.rules
              , n = e.target;
            if (n.seekable && 0 !== n.seekable.length)
                for (var a = 0, r = t.length; a < r; a++)
                    this.evalRule(n, t[a])
        }
    },
    D.availableEventEmitters.push(u),
    d.prototype = {
        defineEvents: function() {
            this.oldBlurClosure = function() {
                D.fireEvent("tabblur", n)
            }
            ,
            this.oldFocusClosure = D.bind(function() {
                this.visibilityApiHasPriority ? D.fireEvent("tabfocus", n) : null != D.visibility.getHiddenProperty() && D.visibility.isHidden() || D.fireEvent("tabfocus", n)
            }, this)
        },
        attachDetachModernEventListeners: function(e) {
            D[0 == e ? "removeEventHandler" : "addEventHandler"](n, D.visibility.getVisibilityEvent(), this.handleVisibilityChange)
        },
        attachDetachOlderEventListeners: function(e, n, a) {
            var r = 0 == e ? "removeEventHandler" : "addEventHandler";
            D[r](n, a, this.oldBlurClosure),
            D[r](t, "focus", this.oldFocusClosure)
        },
        handleVisibilityChange: function() {
            D.visibility.isHidden() ? D.fireEvent("tabblur", n) : D.fireEvent("tabfocus", n)
        },
        setVisibilityApiPriority: function(e) {
            this.visibilityApiHasPriority = e,
            this.attachDetachOlderEventListeners(!1, t, "blur"),
            this.attachDetachModernEventListeners(!1),
            e ? null != D.visibility.getHiddenProperty() ? this.attachDetachModernEventListeners(!0) : this.attachDetachOlderEventListeners(!0, t, "blur") : (this.attachDetachOlderEventListeners(!0, t, "blur"),
            null != D.visibility.getHiddenProperty() && this.attachDetachModernEventListeners(!0))
        },
        oldBlurClosure: null,
        oldFocusClosure: null,
        visibilityApiHasPriority: !0
    },
    D.availableEventEmitters.push(d),
    p.offset = function(a) {
        var r = null
          , i = null;
        try {
            var o = a.getBoundingClientRect()
              , s = n
              , c = s.documentElement
              , l = s.body
              , u = t
              , d = c.clientTop || l.clientTop || 0
              , p = c.clientLeft || l.clientLeft || 0
              , g = u.pageYOffset || c.scrollTop || l.scrollTop
              , f = u.pageXOffset || c.scrollLeft || l.scrollLeft;
            r = o.top + g - d,
            i = o.left + f - p
        } catch (e) {}
        return {
            top: r,
            left: i
        }
    }
    ,
    p.getViewportHeight = function() {
        var e = t.innerHeight
          , a = n.compatMode;
        return a && (e = "CSS1Compat" == a ? n.documentElement.clientHeight : n.body.clientHeight),
        e
    }
    ,
    p.getScrollTop = function() {
        return n.documentElement.scrollTop ? n.documentElement.scrollTop : n.body.scrollTop
    }
    ,
    p.isElementInDocument = function(e) {
        return n.body.contains(e)
    }
    ,
    p.isElementInView = function(e) {
        if (!p.isElementInDocument(e))
            return !1;
        var t = p.getViewportHeight()
          , n = p.getScrollTop()
          , a = p.offset(e).top
          , r = e.offsetHeight;
        return null !== a && !(n > a + r || n + t < a)
    }
    ,
    p.prototype = {
        backgroundTasks: function() {
            var e = this.elements;
            D.each(this.rules, function(t) {
                D.cssQuery(t.selector, function(n) {
                    var a = 0;
                    D.each(n, function(t) {
                        D.contains(e, t) || (e.push(t),
                        a++)
                    }),
                    a && D.notify(t.selector + " added " + a + " elements.", 1)
                })
            }),
            this.track()
        },
        checkInView: function(e, t, n) {
            var a = D.$data(e, "inview");
            if (p.isElementInView(e)) {
                a || D.$data(e, "inview", !0);
                var r = this;
                this.processRules(e, function(n, a, i) {
                    if (t || !n.inviewDelay)
                        D.$data(e, a, !0),
                        D.onEvent({
                            type: "inview",
                            target: e,
                            inviewDelay: n.inviewDelay
                        });
                    else if (n.inviewDelay) {
                        var o = D.$data(e, i);
                        o || (o = setTimeout(function() {
                            r.checkInView(e, !0, n.inviewDelay)
                        }, n.inviewDelay),
                        D.$data(e, i, o))
                    }
                }, n)
            } else {
                if (!p.isElementInDocument(e)) {
                    var i = D.indexOf(this.elements, e);
                    this.elements.splice(i, 1)
                }
                a && D.$data(e, "inview", !1),
                this.processRules(e, function(t, n, a) {
                    var r = D.$data(e, a);
                    r && clearTimeout(r)
                }, n)
            }
        },
        track: function() {
            for (var e = this.elements.length - 1; e >= 0; e--)
                this.checkInView(this.elements[e])
        },
        processRules: function(e, t, n) {
            var a = this.rules;
            n && (a = D.filter(this.rules, function(e) {
                return e.inviewDelay == n
            })),
            D.each(a, function(n, a) {
                var r = n.inviewDelay ? "viewed_" + n.inviewDelay : "viewed"
                  , i = "inview_timeout_id_" + a;
                D.$data(e, r) || D.matchesCss(n.selector, e) && t(n, r, i)
            })
        }
    },
    D.availableEventEmitters.push(p),
    g.prototype.backgroundTasks = function() {
        D.each(this.rules, function(e) {
            D.cssQuery(e.selector, function(e) {
                if (e.length > 0) {
                    var t = e[0];
                    if (D.$data(t, "elementexists.seen"))
                        return;
                    D.$data(t, "elementexists.seen", !0),
                    D.onEvent({
                        type: "elementexists",
                        target: t
                    })
                }
            })
        })
    }
    ,
    D.availableEventEmitters.push(g),
    f.prototype = {
        initialize: function() {
            if (this.FB = this.FB || t.FB,
            this.FB && this.FB.Event && this.FB.Event.subscribe)
                return this.bind(),
                !0
        },
        bind: function() {
            this.FB.Event.subscribe("edge.create", function() {
                D.notify("tracking a facebook like", 1),
                D.onEvent({
                    type: "facebook.like",
                    target: n
                })
            }),
            this.FB.Event.subscribe("edge.remove", function() {
                D.notify("tracking a facebook unlike", 1),
                D.onEvent({
                    type: "facebook.unlike",
                    target: n
                })
            }),
            this.FB.Event.subscribe("message.send", function() {
                D.notify("tracking a facebook share", 1),
                D.onEvent({
                    type: "facebook.send",
                    target: n
                })
            })
        }
    },
    D.availableEventEmitters.push(f),
    m.prototype = {
        backgroundTasks: function() {
            var e = this;
            D.each(this.rules, function(t) {
                var n = t[1]
                  , a = t[0];
                D.cssQuery(n, function(t) {
                    D.each(t, function(t) {
                        e.trackElement(t, a)
                    })
                })
            }, this)
        },
        trackElement: function(e, t) {
            var n = this
              , a = D.$data(e, "hover.delays");
            a ? D.contains(a, t) || a.push(t) : (D.addEventHandler(e, "mouseover", function(t) {
                n.onMouseOver(t, e)
            }),
            D.addEventHandler(e, "mouseout", function(t) {
                n.onMouseOut(t, e)
            }),
            D.$data(e, "hover.delays", [t]))
        },
        onMouseOver: function(e, t) {
            var n = e.target || e.srcElement
              , a = e.relatedTarget || e.fromElement;
            (t === n || D.containsElement(t, n)) && !D.containsElement(t, a) && this.onMouseEnter(t)
        },
        onMouseEnter: function(e) {
            var t = D.$data(e, "hover.delays")
              , n = D.map(t, function(t) {
                return setTimeout(function() {
                    D.onEvent({
                        type: "hover(" + t + ")",
                        target: e
                    })
                }, t)
            });
            D.$data(e, "hover.delayTimers", n)
        },
        onMouseOut: function(e, t) {
            var n = e.target || e.srcElement
              , a = e.relatedTarget || e.toElement;
            (t === n || D.containsElement(t, n)) && !D.containsElement(t, a) && this.onMouseLeave(t)
        },
        onMouseLeave: function(e) {
            var t = D.$data(e, "hover.delayTimers");
            t && D.each(t, function(e) {
                clearTimeout(e)
            })
        }
    },
    D.availableEventEmitters.push(m),
    D.inherit(h, D.BaseTool),
    D.extend(h.prototype, {
        name: "Nielsen",
        endPLPhase: function(e) {
            switch (e) {
            case "pagetop":
                this.initialize();
                break;
            case "pagebottom":
                this.enableTracking && (this.queueCommand({
                    command: "sendFirstBeacon",
                    arguments: []
                }),
                this.flushQueueWhenReady())
            }
        },
        defineListeners: function() {
            this.onTabFocus = D.bind(function() {
                this.notify("Tab visible, sending view beacon when ready", 1),
                this.tabEverVisible = !0,
                this.flushQueueWhenReady()
            }, this),
            this.onPageLeave = D.bind(function() {
                this.notify("isHuman? : " + this.isHuman(), 1),
                this.isHuman() && this.sendDurationBeacon()
            }, this),
            this.onHumanDetectionChange = D.bind(function(e) {
                this == e.target.target && (this.human = e.target.isHuman)
            }, this)
        },
        initialize: function() {
            this.initializeTracking(),
            this.initializeDataProviders(),
            this.initializeNonHumanDetection(),
            this.tabEverVisible = D.visibility.isVisible(),
            this.tabEverVisible ? this.notify("Tab visible, sending view beacon when ready", 1) : D.bindEventOnce("tabfocus", this.onTabFocus),
            this.initialized = !0
        },
        initializeTracking: function() {
            this.initialized || (this.notify("Initializing tracking", 1),
            this.addRemovePageLeaveEvent(this.enableTracking),
            this.addRemoveHumanDetectionChangeEvent(this.enableTracking),
            this.initialized = !0)
        },
        initializeDataProviders: function() {
            var e, t = this.getAnalyticsTool();
            this.dataProvider.register(new h.DataProvider.VisitorID(D.getVisitorId())),
            t ? (e = new h.DataProvider.Generic("rsid",function() {
                return t.settings.account
            }
            ),
            this.dataProvider.register(e)) : this.notify("Missing integration with Analytics: rsid will not be sent.")
        },
        initializeNonHumanDetection: function() {
            D.nonhumandetection ? (D.nonhumandetection.init(),
            this.setEnableNonHumanDetection(0 != this.settings.enableNonHumanDetection),
            this.settings.nonHumanDetectionDelay > 0 && this.setNonHumanDetectionDelay(1e3 * parseInt(this.settings.nonHumanDetectionDelay))) : this.notify("NHDM is not available.")
        },
        getAnalyticsTool: function() {
            if (this.settings.integratesWith)
                return D.tools[this.settings.integratesWith]
        },
        flushQueueWhenReady: function() {
            this.enableTracking && this.tabEverVisible && D.poll(D.bind(function() {
                if (this.isReadyToTrack())
                    return this.flushQueue(),
                    !0
            }, this), 100, 20)
        },
        isReadyToTrack: function() {
            return this.tabEverVisible && this.dataProvider.isReady()
        },
        $setVars: function(e, t, n) {
            for (var a in n) {
                var r = n[a];
                "function" == typeof r && (r = r()),
                this.settings[a] = r
            }
            this.notify("Set variables done", 2),
            this.prepareContextData()
        },
        $setEnableTracking: function(e, t, n) {
            this.notify("Will" + (n ? "" : " not") + " track time on page", 1),
            this.enableTracking != n && (this.addRemovePageLeaveEvent(n),
            this.addRemoveHumanDetectionChangeEvent(n),
            this.enableTracking = n)
        },
        $sendFirstBeacon: function() {
            this.sendViewBeacon()
        },
        setEnableNonHumanDetection: function(e) {
            e ? D.nonhumandetection.register(this) : D.nonhumandetection.unregister(this)
        },
        setNonHumanDetectionDelay: function(e) {
            D.nonhumandetection.register(this, e)
        },
        addRemovePageLeaveEvent: function(e) {
            this.notify((e ? "Attach onto" : "Detach from") + " page leave event", 1),
            D[0 == e ? "unbindEvent" : "bindEvent"]("leave", this.onPageLeave)
        },
        addRemoveHumanDetectionChangeEvent: function(e) {
            this.notify((e ? "Attach onto" : "Detach from") + " human detection change event", 1),
            D[0 == e ? "unbindEvent" : "bindEvent"]("humandetection.change", this.onHumanDetectionChange)
        },
        sendViewBeacon: function() {
            this.notify("Tracked page view.", 1),
            this.sendBeaconWith()
        },
        sendDurationBeacon: function() {
            if (D.timetracking && "function" == typeof D.timetracking.timeOnPage && null != D.timetracking.timeOnPage()) {
                this.notify("Tracked close", 1),
                this.sendBeaconWith({
                    timeOnPage: Math.round(D.timetracking.timeOnPage() / 1e3),
                    duration: "D",
                    timer: "timer"
                });
                var e;
                for (e = 0; e < this.magicConst; e++)
                    "0"
            } else
                this.notify("Could not track close due missing time on page", 5)
        },
        sendBeaconWith: function(e) {
            this.enableTracking && this[this.beaconMethod].call(this, this.prepareUrl(e))
        },
        plainBeacon: function(e) {
            var t = new Image;
            t.src = e,
            t.width = 1,
            t.height = 1,
            t.alt = ""
        },
        navigatorSendBeacon: function(e) {
            navigator.sendBeacon(e)
        },
        prepareUrl: function(e) {
            var t = this.settings;
            return D.extend(t, this.dataProvider.provide()),
            D.extend(t, e),
            this.preparePrefix(this.settings.collectionServer) + this.adapt.convertToURI(this.adapt.toNielsen(this.substituteVariables(t)))
        },
        preparePrefix: function(e) {
            return "//" + encodeURIComponent(e) + ".imrworldwide.com/cgi-bin/gn?"
        },
        substituteVariables: function(e) {
            var t = {};
            for (var n in e)
                e.hasOwnProperty(n) && (t[n] = D.replace(e[n]));
            return t
        },
        prepareContextData: function() {
            if (this.getAnalyticsTool()) {
                var e = this.settings;
                e.sdkVersion = _satellite.publishDate,
                this.getAnalyticsTool().$setVars(null, null, {
                    contextData: this.adapt.toAnalytics(this.substituteVariables(e))
                })
            } else
                this.notify("Adobe Analytics missing.")
        },
        isHuman: function() {
            return this.human
        },
        onTabFocus: function() {},
        onPageLeave: function() {},
        onHumanDetectionChange: function() {},
        notify: function(e, t) {
            D.notify(this.logPrefix + e, t)
        },
        beaconMethod: "plainBeacon",
        adapt: null,
        enableTracking: !1,
        logPrefix: "Nielsen: ",
        tabEverVisible: !1,
        human: !0,
        magicConst: 2e6
    }),
    h.DataProvider = {},
    h.DataProvider.Generic = function(e, t) {
        this.key = e,
        this.valueFn = t
    }
    ,
    D.extend(h.DataProvider.Generic.prototype, {
        isReady: function() {
            return !0
        },
        getValue: function() {
            return this.valueFn()
        },
        provide: function() {
            this.isReady() || h.prototype.notify("Not yet ready to provide value for: " + this.key, 5);
            var e = {};
            return e[this.key] = this.getValue(),
            e
        }
    }),
    h.DataProvider.VisitorID = function(e, t, n) {
        this.key = t || "uuid",
        this.visitorInstance = e,
        this.visitorInstance && (this.visitorId = e.getMarketingCloudVisitorID([this, this._visitorIdCallback])),
        this.fallbackProvider = n || new h.UUID
    }
    ,
    D.inherit(h.DataProvider.VisitorID, h.DataProvider.Generic),
    D.extend(h.DataProvider.VisitorID.prototype, {
        isReady: function() {
            return null === this.visitorInstance || !!this.visitorId
        },
        getValue: function() {
            return this.visitorId || this.fallbackProvider.get()
        },
        _visitorIdCallback: function(e) {
            this.visitorId = e
        }
    }),
    h.DataProvider.Aggregate = function() {
        this.providers = [];
        for (var e = 0; e < arguments.length; e++)
            this.register(arguments[e])
    }
    ,
    D.extend(h.DataProvider.Aggregate.prototype, {
        register: function(e) {
            this.providers.push(e)
        },
        isReady: function() {
            return D.every(this.providers, function(e) {
                return e.isReady()
            })
        },
        provide: function() {
            var e = {};
            return D.each(this.providers, function(t) {
                D.extend(e, t.provide())
            }),
            e
        }
    }),
    h.UUID = function() {}
    ,
    D.extend(h.UUID.prototype, {
        generate: function() {
            return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(e) {
                var t = 16 * Math.random() | 0;
                return ("x" == e ? t : 3 & t | 8).toString(16)
            })
        },
        get: function() {
            var e = D.readCookie(this.key("uuid"));
            return e || (e = this.generate(),
            D.setCookie(this.key("uuid"), e),
            e)
        },
        key: function(e) {
            return "_dtm_nielsen_" + e
        }
    }),
    h.DataAdapters = function() {}
    ,
    D.extend(h.DataAdapters.prototype, {
        toNielsen: function(e) {
            var t = (new Date).getTime()
              , n = {
                c6: "vc,",
                c13: "asid,",
                c15: "apn,",
                c27: "cln,",
                c32: "segA,",
                c33: "segB,",
                c34: "segC,",
                c35: "adrsid,",
                c29: "plid,",
                c30: "bldv,",
                c40: "adbid,"
            }
              , r = {
                ci: e.clientId,
                c6: e.vcid,
                c13: e.appId,
                c15: e.appName,
                prv: 1,
                forward: 0,
                ad: 0,
                cr: e.duration || "V",
                rt: "text",
                st: "dcr",
                prd: "dcr",
                r: t,
                at: e.timer || "view",
                c16: e.sdkVersion,
                c27: e.timeOnPage || 0,
                c40: e.uuid,
                c35: e.rsid,
                ti: t,
                sup: 0,
                c32: e.segmentA,
                c33: e.segmentB,
                c34: e.segmentC,
                asn: e.assetName,
                c29: e.playerID,
                c30: e.buildVersion
            };
            for (key in r)
                if (r[key] !== a && null != r[key] && r[key] !== a && null != r && "" != r) {
                    var i = encodeURIComponent(r[key]);
                    n.hasOwnProperty(key) && i && (i = n[key] + i),
                    r[key] = i
                }
            return this.filterObject(r)
        },
        toAnalytics: function(e) {
            return this.filterObject({
                "a.nielsen.clientid": e.clientId,
                "a.nielsen.vcid": e.vcid,
                "a.nielsen.appid": e.appId,
                "a.nielsen.appname": e.appName,
                "a.nielsen.accmethod": "0",
                "a.nielsen.ctype": "text",
                "a.nielsen.sega": e.segmentA,
                "a.nielsen.segb": e.segmentB,
                "a.nielsen.segc": e.segmentC,
                "a.nielsen.asset": e.assetName
            })
        },
        convertToURI: function(e) {
            if (!1 === D.isObject(e))
                return "";
            var t = [];
            for (var n in e)
                e.hasOwnProperty(n) && t.push(n + "=" + e[n]);
            return t.join("&")
        },
        filterObject: function(e) {
            for (var t in e)
                !e.hasOwnProperty(t) || null != e[t] && e[t] !== a || delete e[t];
            return e
        }
    }),
    D.availableTools.nielsen = h,
    D.inherit(v, D.BaseTool),
    D.extend(v.prototype, {
        name: "tnt",
        endPLPhase: function(e) {
            "aftertoolinit" === e && this.initialize()
        },
        initialize: function() {
            D.notify("Test & Target: Initializing", 1),
            this.initializeTargetPageParams(),
            this.load()
        },
        initializeTargetPageParams: function() {
            t.targetPageParams && this.updateTargetPageParams(this.parseTargetPageParamsResult(t.targetPageParams())),
            this.updateTargetPageParams(this.settings.pageParams),
            this.setTargetPageParamsFunction()
        },
        load: function() {
            var e = this.getMboxURL(this.settings.mboxURL);
            !1 !== this.settings.initTool ? this.settings.loadSync ? (D.loadScriptSync(e),
            this.onScriptLoaded()) : (D.loadScript(e, D.bind(this.onScriptLoaded, this)),
            this.initializing = !0) : this.initialized = !0
        },
        getMboxURL: function(e) {
            var n = e;
            return D.isObject(e) && (n = "https:" === t.location.protocol ? e.https : e.http),
            n.match(/^https?:/) ? n : D.basePath() + n
        },
        onScriptLoaded: function() {
            D.notify("Test & Target: loaded.", 1),
            this.flushQueue(),
            this.initialized = !0,
            this.initializing = !1
        },
        $addMbox: function(e, t, n) {
            var a = n.mboxGoesAround
              , r = a + "{visibility: hidden;}"
              , i = this.appendStyle(r);
            a in this.styleElements || (this.styleElements[a] = i),
            this.initialized ? this.$addMBoxStep2(null, null, n) : this.initializing && this.queueCommand({
                command: "addMBoxStep2",
                arguments: [n]
            }, e, t)
        },
        $addMBoxStep2: function(e, a, r) {
            var i = this.generateID()
              , o = this;
            D.addEventHandler(t, "load", D.bind(function() {
                D.cssQuery(r.mboxGoesAround, function(e) {
                    var a = e[0];
                    if (a) {
                        var s = n.createElement("div");
                        s.id = i,
                        a.parentNode.replaceChild(s, a),
                        s.appendChild(a),
                        t.mboxDefine(i, r.mboxName);
                        var c = [r.mboxName];
                        r.arguments && (c = c.concat(r.arguments)),
                        t.mboxUpdate.apply(null, c),
                        o.reappearWhenCallComesBack(a, i, r.timeout, r)
                    }
                })
            }, this)),
            this.lastMboxID = i
        },
        $addTargetPageParams: function(e, t, n) {
            this.updateTargetPageParams(n)
        },
        generateID: function() {
            return "_sdsat_mbox_" + String(Math.random()).substring(2) + "_"
        },
        appendStyle: function(e) {
            var t = n.getElementsByTagName("head")[0]
              , a = n.createElement("style");
            return a.type = "text/css",
            a.styleSheet ? a.styleSheet.cssText = e : a.appendChild(n.createTextNode(e)),
            t.appendChild(a),
            a
        },
        reappearWhenCallComesBack: function(e, t, n, a) {
            function r() {
                var e = i.styleElements[a.mboxGoesAround];
                e && (e.parentNode.removeChild(e),
                delete i.styleElements[a.mboxGoesAround])
            }
            var i = this;
            D.cssQuery('script[src*="omtrdc.net"]', function(e) {
                var t = e[0];
                if (t) {
                    D.scriptOnLoad(t.src, t, function() {
                        D.notify("Test & Target: request complete", 1),
                        r(),
                        clearTimeout(a)
                    });
                    var a = setTimeout(function() {
                        D.notify("Test & Target: bailing after " + n + "ms", 1),
                        r()
                    }, n)
                } else
                    D.notify("Test & Target: failed to find T&T ajax call, bailing", 1),
                    r()
            })
        },
        updateTargetPageParams: function(e) {
            var t = {};
            for (var n in e)
                e.hasOwnProperty(n) && (t[D.replace(n)] = D.replace(e[n]));
            D.extend(this.targetPageParamsStore, t)
        },
        getTargetPageParams: function() {
            return this.targetPageParamsStore
        },
        setTargetPageParamsFunction: function() {
            t.targetPageParams = D.bind(this.getTargetPageParams, this)
        },
        parseTargetPageParamsResult: function(e) {
            var t = e;
            return D.isArray(e) && (e = e.join("&")),
            D.isString(e) && (t = D.parseQueryParams(e)),
            t
        }
    }),
    D.availableTools.tnt = v,
    D.inherit(y, D.BaseTool),
    D.extend(y.prototype, {
        name: "Default",
        $loadIframe: function(e, n, a) {
            var r = a.pages
              , i = a.loadOn
              , o = D.bind(function() {
                D.each(r, function(t) {
                    this.loadIframe(e, n, t)
                }, this)
            }, this);
            i || o(),
            "domready" === i && D.domReady(o),
            "load" === i && D.addEventHandler(t, "load", o)
        },
        loadIframe: function(e, t, a) {
            var r = n.createElement("iframe");
            r.style.display = "none";
            var i = D.data.host
              , o = a.data
              , s = this.scriptURL(a.src)
              , c = D.searchVariables(o, e, t);
            i && (s = D.basePath() + s),
            s += c,
            r.src = s;
            var l = n.getElementsByTagName("body")[0];
            l ? l.appendChild(r) : D.domReady(function() {
                n.getElementsByTagName("body")[0].appendChild(r)
            })
        },
        scriptURL: function(e) {
            return (D.settings.scriptDir || "") + e
        },
        $loadScript: function(e, n, a) {
            var r = a.scripts
              , i = a.sequential
              , o = a.loadOn
              , s = D.bind(function() {
                i ? this.loadScripts(e, n, r) : D.each(r, function(t) {
                    this.loadScripts(e, n, [t])
                }, this)
            }, this);
            o ? "domready" === o ? D.domReady(s) : "load" === o && D.addEventHandler(t, "load", s) : s()
        },
        loadScripts: function(t, n, a) {
            function r() {
                o.length > 0 && i && o.shift().call(t, n, s);
                var e = a.shift();
                if (e) {
                    var l = D.data.host
                      , u = c.scriptURL(e.src);
                    l && (u = D.basePath() + u),
                    i = e,
                    D.loadScript(u, r)
                }
            }
            try {
                a = a.slice(0);
                var i, o = this.asyncScriptCallbackQueue, s = n.target || n.srcElement, c = this
            } catch (e) {
                console.error("scripts is", D.stringify(a))
            }
            r()
        },
        $loadBlockingScript: function(e, t, n) {
            var a = n.scripts;
            n.loadOn;
            D.bind(function() {
                D.each(a, function(n) {
                    this.loadBlockingScript(e, t, n)
                }, this)
            }, this)()
        },
        loadBlockingScript: function(e, t, n) {
            var a = this.scriptURL(n.src)
              , r = D.data.host
              , i = t.target || t.srcElement;
            r && (a = D.basePath() + a),
            this.argsForBlockingScripts.push([e, t, i]),
            D.loadScriptSync(a)
        },
        pushAsyncScript: function(e) {
            this.asyncScriptCallbackQueue.push(e)
        },
        pushBlockingScript: function(e) {
            var t = this.argsForBlockingScripts.shift()
              , n = t[0];
            e.apply(n, t.slice(1))
        },
        $writeHTML: D.escapeHtmlParams(function(e, t) {
            if (!D.domReadyFired && n.write)
                if ("pagebottom" === t.type || "pagetop" === t.type)
                    for (var a = 2, r = arguments.length; a < r; a++) {
                        var i = arguments[a].html;
                        i = D.replace(i, e, t),
                        n.write(i)
                    }
                else
                    D.notify("You can only use writeHTML on the `pagetop` and `pagebottom` events.", 1);
            else
                D.notify("Command writeHTML failed. You should try appending HTML using the async option.", 1)
        }),
        linkNeedsDelayActivate: function(e, n) {
            n = n || t;
            var a = e.tagName
              , r = e.getAttribute("target")
              , i = e.getAttribute("href");
            return (!a || "a" === a.toLowerCase()) && (!!i && (!r || "_blank" !== r && ("_top" === r ? n.top === n : "_parent" !== r && ("_self" === r || (!n.name || r === n.name)))))
        },
        $delayActivateLink: function(e, t) {
            if (this.linkNeedsDelayActivate(e)) {
                D.preventDefault(t);
                var n = D.settings.linkDelay || 100;
                setTimeout(function() {
                    D.setLocation(e.href)
                }, n)
            }
        },
        isQueueable: function(e) {
            return "writeHTML" !== e.command
        }
    }),
    D.availableTools["default"] = y,
    D.inherit(b, D.BaseTool),
    D.extend(b.prototype, {
        name: "SC",
        endPLPhase: function(e) {
            e === this.settings.loadOn && this.initialize(e)
        },
        initialize: function(e) {
            if (!this._cancelToolInit)
                if (this.settings.initVars = this.substituteVariables(this.settings.initVars, {
                    type: e
                }),
                !1 !== this.settings.initTool) {
                    var n = this.settings.sCodeURL || D.basePath() + "s_code.js";
                    "object" == typeof n && (n = "https:" === t.location.protocol ? n.https : n.http),
                    n.match(/^https?:/) || (n = D.basePath() + n),
                    this.settings.initVars && this.$setVars(null, null, this.settings.initVars),
                    D.loadScript(n, D.bind(this.onSCodeLoaded, this)),
                    this.initializing = !0
                } else
                    this.initializing = !0,
                    this.pollForSC()
        },
        getS: function(e, n) {
            var a = n && n.hostname || t.location.hostname
              , r = this.concatWithToolVarBindings(n && n.setVars || this.varBindings)
              , i = n && n.addEvent || this.events
              , o = this.getAccount(a)
              , s = t.s_gi;
            if (!s)
                return null;
            if (this.isValidSCInstance(e) || (e = null),
            !o && !e)
                return D.notify("Adobe Analytics: tracker not initialized because account was not found", 1),
                null;
            e = e || s(o);
            var c = "D" + D.appVersion;
            return "undefined" != typeof e.tagContainerMarker ? e.tagContainerMarker = c : "string" == typeof e.version && e.version.substring(e.version.length - 5) !== "-" + c && (e.version += "-" + c),
            e.sa && !0 !== this.settings.skipSetAccount && !1 !== this.settings.initTool && e.sa(this.settings.account),
            this.applyVarBindingsOnTracker(e, r),
            i.length > 0 && (e.events = i.join(",")),
            D.getVisitorId() && (e.visitor = D.getVisitorId()),
            e
        },
        onSCodeLoaded: function(e) {
            this.initialized = !0,
            this.initializing = !1;
            var t = ["Adobe Analytics: loaded", e ? " (manual)" : "", "."];
            D.notify(t.join(""), 1),
            D.fireEvent(this.id + ".load", this.getS()),
            e || (this.flushQueueExceptTrackLink(),
            this.sendBeacon()),
            this.flushQueue()
        },
        getAccount: function(e) {
            return t.s_account ? t.s_account : e && this.settings.accountByHost && this.settings.accountByHost[e] || this.settings.account
        },
        getTrackingServer: function() {
            var e = this
              , n = e.getS();
            if (n) {
                if (n.ssl && n.trackingServerSecure)
                    return n.trackingServerSecure;
                if (n.trackingServer)
                    return n.trackingServer
            }
            var a, r = e.getAccount(t.location.hostname);
            if (!r)
                return null;
            var i, o, s = "", c = n && n.dc;
            return (i = (a = r).indexOf(",")) >= 0 && (a = a.gb(0, i)),
            a = a.replace(/[^A-Za-z0-9]/g, ""),
            s || (s = "2o7.net"),
            c = c ? ("" + c).toLowerCase() : "d1",
            "2o7.net" == s && ("d1" == c ? c = "112" : "d2" == c && (c = "122"),
            o = ""),
            i = a + "." + c + "." + o + s
        },
        sendBeacon: function() {
            var e = this.getS(t[this.settings.renameS || "s"]);
            e ? this.settings.customInit && !1 === this.settings.customInit(e) ? D.notify("Adobe Analytics: custom init suppressed beacon", 1) : (this.settings.executeCustomPageCodeFirst && this.applyVarBindingsOnTracker(e, this.varBindings),
            this.executeCustomSetupFuns(e),
            e.t(),
            this.clearVarBindings(),
            this.clearCustomSetup(),
            D.notify("Adobe Analytics: tracked page view", 1)) : D.notify("Adobe Analytics: page code not loaded", 1)
        },
        pollForSC: function() {
            D.poll(D.bind(function() {
                if ("function" == typeof t.s_gi)
                    return this.onSCodeLoaded(!0),
                    !0
            }, this))
        },
        flushQueueExceptTrackLink: function() {
            if (this.pending) {
                for (var e = [], t = 0; t < this.pending.length; t++) {
                    var n = this.pending[t];
                    "trackLink" === n[0].command ? e.push(n) : this.triggerCommand.apply(this, n)
                }
                this.pending = e
            }
        },
        isQueueAvailable: function() {
            return !this.initialized
        },
        substituteVariables: function(e, t) {
            var n = {};
            for (var a in e)
                if (e.hasOwnProperty(a)) {
                    var r = e[a];
                    n[a] = D.replace(r, location, t)
                }
            return n
        },
        $setVars: function(e, t, n) {
            for (var a in n)
                if (n.hasOwnProperty(a)) {
                    var r = n[a];
                    "function" == typeof r && (r = r()),
                    this.varBindings[a] = r
                }
            D.notify("Adobe Analytics: set variables.", 2)
        },
        $customSetup: function(e, t, n) {
            this.customSetupFuns.push(function(a) {
                n.call(e, t, a)
            })
        },
        isValidSCInstance: function(e) {
            return !!e && "function" == typeof e.t && "function" == typeof e.tl
        },
        concatWithToolVarBindings: function(e) {
            var t = this.settings.initVars || {};
            return D.map(["trackingServer", "trackingServerSecure"], function(n) {
                t[n] && !e[n] && (e[n] = t[n])
            }),
            e
        },
        applyVarBindingsOnTracker: function(e, t) {
            for (var n in t)
                t.hasOwnProperty(n) && (e[n] = t[n])
        },
        clearVarBindings: function() {
            this.varBindings = {}
        },
        clearCustomSetup: function() {
            this.customSetupFuns = []
        },
        executeCustomSetupFuns: function(e) {
            D.each(this.customSetupFuns, function(n) {
                n.call(t, e)
            })
        },
        $trackLink: function(e, t, n) {
            var a = (n = n || {}).type
              , r = n.linkName;
            !r && e && e.nodeName && "a" === e.nodeName.toLowerCase() && (r = e.innerHTML),
            r || (r = "link clicked");
            var i = n && n.setVars
              , o = n && n.addEvent || []
              , s = this.getS(null, {
                setVars: i,
                addEvent: o
            });
            if (s) {
                var c = s.linkTrackVars
                  , l = s.linkTrackEvents
                  , u = this.definedVarNames(i);
                n && n.customSetup && n.customSetup.call(e, t, s),
                o.length > 0 && u.push("events"),
                s.products && u.push("products"),
                u = this.mergeTrackLinkVars(s.linkTrackVars, u),
                o = this.mergeTrackLinkVars(s.linkTrackEvents, o),
                s.linkTrackVars = this.getCustomLinkVarsList(u);
                var d = D.map(o, function(e) {
                    return e.split(":")[0]
                });
                s.linkTrackEvents = this.getCustomLinkVarsList(d),
                s.tl(!0, a || "o", r),
                D.notify(["Adobe Analytics: tracked link ", "using: linkTrackVars=", D.stringify(s.linkTrackVars), "; linkTrackEvents=", D.stringify(s.linkTrackEvents)].join(""), 1),
                s.linkTrackVars = c,
                s.linkTrackEvents = l
            } else
                D.notify("Adobe Analytics: page code not loaded", 1)
        },
        mergeTrackLinkVars: function(e, t) {
            return e && (t = e.split(",").concat(t)),
            t
        },
        getCustomLinkVarsList: function(e) {
            var t = D.indexOf(e, "None");
            return t > -1 && e.length > 1 && e.splice(t, 1),
            e.join(",")
        },
        definedVarNames: function(e) {
            e = e || this.varBindings;
            var t = [];
            for (var n in e)
                e.hasOwnProperty(n) && /^(eVar[0-9]+)|(prop[0-9]+)|(hier[0-9]+)|campaign|purchaseID|channel|server|state|zip|pageType$/.test(n) && t.push(n);
            return t
        },
        $trackPageView: function(e, t, n) {
            var a = n && n.setVars
              , r = n && n.addEvent || []
              , i = this.getS(null, {
                setVars: a,
                addEvent: r
            });
            i ? (i.linkTrackVars = "",
            i.linkTrackEvents = "",
            this.executeCustomSetupFuns(i),
            n && n.customSetup && n.customSetup.call(e, t, i),
            i.t(),
            this.clearVarBindings(),
            this.clearCustomSetup(),
            D.notify("Adobe Analytics: tracked page view", 1)) : D.notify("Adobe Analytics: page code not loaded", 1)
        },
        $postTransaction: function(e, n, a) {
            var r = D.data.transaction = t[a]
              , i = this.varBindings
              , o = this.settings.fieldVarMapping;
            if (D.each(r.items, function(e) {
                this.products.push(e)
            }, this),
            i.products = D.map(this.products, function(e) {
                var t = [];
                if (o && o.item)
                    for (var n in o.item)
                        if (o.item.hasOwnProperty(n)) {
                            var a = o.item[n];
                            t.push(a + "=" + e[n]),
                            "event" === a.substring(0, 5) && this.events.push(a)
                        }
                var r = ["", e.product, e.quantity, e.unitPrice * e.quantity];
                return t.length > 0 && r.push(t.join("|")),
                r.join(";")
            }, this).join(","),
            o && o.transaction) {
                var s = [];
                for (var c in o.transaction)
                    if (o.transaction.hasOwnProperty(c)) {
                        a = o.transaction[c];
                        s.push(a + "=" + r[c]),
                        "event" === a.substring(0, 5) && this.events.push(a)
                    }
                i.products.length > 0 && (i.products += ","),
                i.products += ";;;;" + s.join("|")
            }
        },
        $addEvent: function() {
            for (var e = 2, t = arguments.length; e < t; e++)
                this.events.push(arguments[e])
        },
        $addProduct: function() {
            for (var e = 2, t = arguments.length; e < t; e++)
                this.products.push(arguments[e])
        }
    }),
    D.availableTools.sc = b,
    D.inherit(L, D.BaseTool),
    D.extend(L.prototype, {
        initialize: function() {
            var e = this.settings;
            if (!1 !== this.settings.initTool) {
                var t = e.url;
                t = "string" == typeof t ? D.basePath() + t : D.isHttps() ? t.https : t.http,
                D.loadScript(t, D.bind(this.onLoad, this)),
                this.initializing = !0
            } else
                this.initialized = !0
        },
        isQueueAvailable: function() {
            return !this.initialized
        },
        onLoad: function() {
            this.initialized = !0,
            this.initializing = !1,
            this.settings.initialBeacon && this.settings.initialBeacon(),
            this.flushQueue()
        },
        endPLPhase: function(e) {
            e === this.settings.loadOn && (D.notify(this.name + ": Initializing at " + e, 1),
            this.initialize())
        },
        $fire: function(e, t, n) {
            this.initializing ? this.queueCommand({
                command: "fire",
                arguments: [n]
            }, e, t) : n.call(this.settings, e, t)
        }
    }),
    D.availableTools.am = L,
    D.availableTools.adlens = L,
    D.availableTools.aem = L,
    D.availableTools.__basic = L,
    D.inherit(C, D.BaseTool),
    D.extend(C.prototype, {
        name: "GA",
        initialize: function() {
            var e = this.settings
              , n = t._gaq
              , a = e.initCommands || []
              , r = e.customInit;
            if (n || (_gaq = []),
            this.isSuppressed())
                D.notify("GA: page code not loaded(suppressed).", 1);
            else {
                if (!n && !C.scriptLoaded) {
                    var i = D.isHttps()
                      , o = (i ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
                    e.url && (o = i ? e.url.https : e.url.http),
                    D.loadScript(o),
                    C.scriptLoaded = !0,
                    D.notify("GA: page code loaded.", 1)
                }
                e.domain;
                var s = e.trackerName
                  , c = P.allowLinker()
                  , l = D.replace(e.account, location);
                D.settings.domainList;
                _gaq.push([this.cmd("setAccount"), l]),
                c && _gaq.push([this.cmd("setAllowLinker"), c]),
                _gaq.push([this.cmd("setDomainName"), P.cookieDomain()]),
                D.each(a, function(e) {
                    var t = [this.cmd(e[0])].concat(D.preprocessArguments(e.slice(1), location, null, this.forceLowerCase));
                    _gaq.push(t)
                }, this),
                r && (this.suppressInitialPageView = !1 === r(_gaq, s)),
                e.pageName && this.$overrideInitialPageView(null, null, e.pageName)
            }
            this.initialized = !0,
            D.fireEvent(this.id + ".configure", _gaq, s)
        },
        isSuppressed: function() {
            return this._cancelToolInit || !1 === this.settings.initTool
        },
        tracker: function() {
            return this.settings.trackerName
        },
        cmd: function(e) {
            var t = this.tracker();
            return t ? t + "._" + e : "_" + e
        },
        $overrideInitialPageView: function(e, t, n) {
            this.urlOverride = n
        },
        trackInitialPageView: function() {
            if (!this.isSuppressed() && !this.suppressInitialPageView)
                if (this.urlOverride) {
                    var e = D.preprocessArguments([this.urlOverride], location, null, this.forceLowerCase);
                    this.$missing$("trackPageview", null, null, e)
                } else
                    this.$missing$("trackPageview")
        },
        endPLPhase: function(e) {
            e === this.settings.loadOn && (D.notify("GA: Initializing at " + e, 1),
            this.initialize(),
            this.flushQueue(),
            this.trackInitialPageView())
        },
        call: function(e, t, n, a) {
            if (!this._cancelToolInit) {
                this.settings;
                var r = this.tracker()
                  , i = this.cmd(e);
                a = a ? [i].concat(a) : [i];
                _gaq.push(a),
                r ? D.notify("GA: sent command " + e + " to tracker " + r + (a.length > 1 ? " with parameters [" + a.slice(1).join(", ") + "]" : "") + ".", 1) : D.notify("GA: sent command " + e + (a.length > 1 ? " with parameters [" + a.slice(1).join(", ") + "]" : "") + ".", 1)
            }
        },
        $missing$: function(e, t, n, a) {
            this.call(e, t, n, a)
        },
        $postTransaction: function(e, n, a) {
            var r = D.data.customVars.transaction = t[a];
            this.call("addTrans", e, n, [r.orderID, r.affiliation, r.total, r.tax, r.shipping, r.city, r.state, r.country]),
            D.each(r.items, function(t) {
                this.call("addItem", e, n, [t.orderID, t.sku, t.product, t.category, t.unitPrice, t.quantity])
            }, this),
            this.call("trackTrans", e, n)
        },
        delayLink: function(e, t) {
            var n = this;
            if (P.allowLinker() && e.hostname.match(this.settings.linkerDomains) && !D.isSubdomainOf(e.hostname, location.hostname)) {
                D.preventDefault(t);
                var a = D.settings.linkDelay || 100;
                setTimeout(function() {
                    n.call("link", e, t, [e.href])
                }, a)
            }
        },
        popupLink: function(e, n) {
            if (t._gat) {
                D.preventDefault(n);
                var a = this.settings.account
                  , r = t._gat._createTracker(a)._getLinkerUrl(e.href);
                t.open(r)
            }
        },
        $link: function(e, t) {
            "_blank" === e.getAttribute("target") ? this.popupLink(e, t) : this.delayLink(e, t)
        },
        $trackEvent: function(e, t) {
            var n = Array.prototype.slice.call(arguments, 2);
            if (n.length >= 4 && null != n[3]) {
                var a = parseInt(n[3], 10);
                D.isNaN(a) && (a = 1),
                n[3] = a
            }
            this.call("trackEvent", e, t, n)
        }
    }),
    D.availableTools.ga = C;
    var P = {
        allowLinker: function() {
            return D.hasMultipleDomains()
        },
        cookieDomain: function() {
            var e = D.settings.domainList
              , n = D.find(e, function(e) {
                var n = t.location.hostname;
                return D.equalsIgnoreCase(n.slice(n.length - e.length), e)
            });
            return n ? "." + n : "auto"
        }
    };
    D.inherit(S, D.BaseTool),
    D.extend(S.prototype, {
        name: "GAUniversal",
        endPLPhase: function(e) {
            e === this.settings.loadOn && (D.notify("GAU: Initializing at " + e, 1),
            this.initialize(),
            this.flushQueue(),
            this.trackInitialPageView())
        },
        getTrackerName: function() {
            return this.settings.trackerSettings.name || ""
        },
        isPageCodeLoadSuppressed: function() {
            return !1 === this.settings.initTool || !0 === this._cancelToolInit
        },
        initialize: function() {
            if (this.isPageCodeLoadSuppressed())
                return this.initialized = !0,
                void D.notify("GAU: Page code not loaded (suppressed).", 1);
            var e = "ga";
            t[e] = t[e] || this.createGAObject(),
            t.GoogleAnalyticsObject = e,
            D.notify("GAU: Page code loaded.", 1),
            D.loadScriptOnce(this.getToolUrl());
            var n = this.settings;
            (P.allowLinker() && !1 !== n.allowLinker ? this.createAccountForLinker() : this.createAccount(),
            this.executeInitCommands(),
            n.customInit) && (!1 === (0,
            n.customInit)(t[e], this.getTrackerName()) && (this.suppressInitialPageView = !0));
            this.initialized = !0
        },
        createGAObject: function() {
            var e = function() {
                e.q.push(arguments)
            };
            return e.q = [],
            e.l = 1 * new Date,
            e
        },
        createAccount: function() {
            this.create()
        },
        createAccountForLinker: function() {
            var e = {};
            P.allowLinker() && (e.allowLinker = !0),
            this.create(e),
            this.call("require", "linker"),
            this.call("linker:autoLink", this.autoLinkDomains(), !1, !0)
        },
        create: function(e) {
            var t = this.settings.trackerSettings;
            (t = D.preprocessArguments([t], location, null, this.forceLowerCase)[0]).trackingId = D.replace(this.settings.trackerSettings.trackingId, location),
            t.cookieDomain || (t.cookieDomain = P.cookieDomain()),
            D.extend(t, e || {}),
            this.call("create", t)
        },
        autoLinkDomains: function() {
            var e = location.hostname;
            return D.filter(D.settings.domainList, function(t) {
                return t !== e
            })
        },
        executeInitCommands: function() {
            var e = this.settings;
            e.initCommands && D.each(e.initCommands, function(e) {
                var t = e.splice(2, e.length - 2);
                e = e.concat(D.preprocessArguments(t, location, null, this.forceLowerCase)),
                this.call.apply(this, e)
            }, this)
        },
        trackInitialPageView: function() {
            this.suppressInitialPageView || this.isPageCodeLoadSuppressed() || this.call("send", "pageview")
        },
        call: function() {
            "function" == typeof ga ? this.isCallSuppressed() || (arguments[0] = this.cmd(arguments[0]),
            this.log(D.toArray(arguments)),
            ga.apply(t, arguments)) : D.notify("GA Universal function not found!", 4)
        },
        isCallSuppressed: function() {
            return !0 === this._cancelToolInit
        },
        $missing$: function(e, t, n, a) {
            a = a || [],
            a = [e].concat(a),
            this.call.apply(this, a)
        },
        getToolUrl: function() {
            var e = this.settings
              , t = D.isHttps();
            return e.url ? t ? e.url.https : e.url.http : (t ? "https://ssl" : "http://www") + ".google-analytics.com/analytics.js"
        },
        cmd: function(e) {
            var t = ["send", "set", "get"]
              , n = this.getTrackerName();
            return n && -1 !== D.indexOf(t, e) ? n + "." + e : e
        },
        log: function(e) {
            var t = "GA Universal: sent command " + e[0] + " to tracker " + (this.getTrackerName() || "default");
            if (e.length > 1) {
                D.stringify(e.slice(1));
                t += " with parameters " + D.stringify(e.slice(1))
            }
            t += ".",
            D.notify(t, 1)
        }
    }),
    D.availableTools.ga_universal = S,
    D.extend(E.prototype, {
        getInstance: function() {
            return this.instance
        },
        initialize: function() {
            var e, t = this.settings;
            D.notify("Visitor ID: Initializing tool", 1),
            null !== (e = this.createInstance(t.mcOrgId, t.initVars)) && (t.customerIDs && this.applyCustomerIDs(e, t.customerIDs),
            t.autoRequest && e.getMarketingCloudVisitorID(),
            this.instance = e)
        },
        createInstance: function(e, t) {
            if (!D.isString(e))
                return D.notify('Visitor ID: Cannot create instance using mcOrgId: "' + e + '"', 4),
                null;
            e = D.replace(e),
            D.notify('Visitor ID: Create instance using mcOrgId: "' + e + '"', 1),
            t = this.parseValues(t);
            var n = Visitor.getInstance(e, t);
            return D.notify("Visitor ID: Set variables: " + D.stringify(t), 1),
            n
        },
        applyCustomerIDs: function(e, t) {
            var n = this.parseIds(t);
            e.setCustomerIDs(n),
            D.notify("Visitor ID: Set Customer IDs: " + D.stringify(n), 1)
        },
        parseValues: function(e) {
            if (!1 === D.isObject(e))
                return {};
            var t = {};
            for (var n in e)
                e.hasOwnProperty(n) && (t[n] = D.replace(e[n]));
            return t
        },
        parseIds: function(e) {
            var t = {};
            if (!1 === D.isObject(e))
                return {};
            for (var n in e)
                if (e.hasOwnProperty(n)) {
                    var a = D.replace(e[n].id);
                    a !== e[n].id && a && (t[n] = {},
                    t[n].id = a,
                    t[n].authState = Visitor.AuthState[e[n].authState])
                }
            return t
        }
    }),
    D.availableTools.visitor_id = E,
    _satellite.init({
        tools: {
            e8a934c3609c8ac1ede1d16bf48e87dc4fd97665: {
                engine: "am",
                loadOn: "pagebottom",
                name: "AudienceManager",
                initTool: !0,
                url: "09d79ff7891fd87dd491ffa94fd3a8d7eb4d80b3/dil-contents-e8a934c3609c8ac1ede1d16bf48e87dc4fd97665.js"
            },
            "782e950c976718383ed5977171fc5e5e": {
                engine: "sc",
                loadOn: "pagetop",
                euCookie: !1,
                sCodeURL: "09d79ff7891fd87dd491ffa94fd3a8d7eb4d80b3/s-code-contents-8624ce0435757acdc4b42f80e8fba39511c1f8af.js",
                renameS: "s",
                initVars: {
                    currencyCode: "AUD",
                    trackingServer: "info.belong.com.au",
                    trackingServerSecure: "infos.belong.com.au",
                    campaign: "%omd_tracking_code%",
                    pageURL: "%url%",
                    trackInlineStats: !0,
                    trackDownloadLinks: !0,
                    linkDownloadFileTypes: "avi,css,csv,doc,docx,eps,exe,jpg,js,m4v,mov,mp3,pdf,png,ppt,pptx,rar,svg,tab,txt,vsd,vxd,wav,wma,wmv,xls,xlsx,xml,zip",
                    trackExternalLinks: !0,
                    linkExternalFilters: "mailto:,tel:",
                    linkInternalFilters: "javascript:,belong.com.au,belongtest.com.au",
                    linkLeaveQueryString: !1,
                    dynamicVariablePrefix: "D=",
                    eVar2: "D=c2",
                    eVar7: "%deviceType%",
                    eVar18: "%customerType%",
                    eVar20: "%hotjarUserId%",
                    eVar23: "%liveAgentSessionId%",
                    eVar29: "%postCode%",
                    eVar30: "%state%",
                    eVar43: "%metaKeywords%",
                    eVar44: "%metaDescription%",
                    eVar45: "%gclid%",
                    eVar46: "%utm_source%",
                    eVar47: "%utm_medium%",
                    eVar48: "%utm_term%",
                    eVar49: "%utm_campaign%",
                    eVar50: "%utm_content%",
                    eVar57: "%marketingCloudVisitorId%",
                    eVar80: "%auth0id%",
                    prop1: "BL",
                    prop2: "%subDivision%",
                    prop7: "%deviceType%"
                },
                skipSetAccount: !0,
                customInit: function(e) {
                    function t() {
                        "undefined" == typeof s_events && (s_events = "");
                        var t = n.location.pathname;
                        if ("undefined" == typeof s_section || "" == s_section) {
                            "/" == t && (t = "/homepage");
                            var a = t.split("/");
                            "undefined" != typeof s_subSectionOvrd && s_subSectionOvrd ? (a.splice(0, 1),
                            a.unshift(s_subSectionOvrd),
                            a.unshift(s_sectionOvrd),
                            a.unshift("")) : "undefined" != typeof s_sectionOvrd && s_sectionOvrd && (a.splice(0, 1),
                            a.unshift(s_sectionOvrd),
                            a.unshift(""));
                            var r = decodeURI(n.location.search).split("&");
                            s_pageName = e.generateURLPageName(a),
                            s_pageName = "bl:" + s_pageName;
                            var i = /page/i;
                            if ("all" == "none")
                                for (var o = 0, s = 0; s < r.length; s++) {
                                    var c = r[s];
                                    ("all" == i || i.test(c)) && (/\=$/.test(c) || (s_pageQuery = 0 == s ? c : 0 == o ? "?" + c : s_pageQuery + "&" + c,
                                    o++))
                                }
                            "undefined" != typeof s_pageQuery && "undefined" != typeof s_pageName && (s_pageName = s_pageName + ":" + s_pageQuery)
                        }
                    }
                    s_removeHtmlTags = function(e) {
                        return e.replace(/<(a+)[^>]*>.*\n?<\/\1>|<(\w+)[^>]*>|<\/.+>/gi, "")
                    }
                    ,
                    s_removeAllSpecialChars = function(e) {
                        var t = s_removeHtmlTags(e);
                        return t = (t = (t = (t = (t = (t = t.replace(/&amp;/gi, "and")).replace(/&/gi, "and")).replace(/,/gi, "")).replace(/  /gi, "")).replace(/\n/g, "")).replace(/[^\w\s\.\%]/gi, "")
                    }
                    ,
                    "function" == typeof t && t()
                }
            },
            c2d4681c4706f0ca00c0740e11ea560304dbbc40: {
                engine: "tnt",
                mboxURL: "09d79ff7891fd87dd491ffa94fd3a8d7eb4d80b3/mbox-contents-c2d4681c4706f0ca00c0740e11ea560304dbbc40.js",
                loadSync: !0,
                pageParams: {
                    at_property: "fe64daa5-48cc-df93-e158-14c2bc1799bd",
                    fbbPlanType: "%planType%",
                    fbbServiceType: "%serviceType%",
                    site: "belong"
                }
            },
            "34a0e44beaf05631589ca2322fbf6a1634fb71af": {
                engine: "visitor_id",
                loadOn: "pagetop",
                name: "VisitorID",
                mcOrgId: "98DC73AE52E13F1E0A490D4C@AdobeOrg",
                autoRequest: !0,
                initVars: {
                    trackingServer: "info.belong.com.au",
                    trackingServerSecure: "infos.belong.com.au",
                    marketingCloudServer: "info.belong.com.au",
                    marketingCloudServerSecure: "infos.belong.com.au",
                    cookieDomain: "%rootHostname%",
                    loadTimeout: "2000"
                },
                customerIDs: {
                    octane_id: {
                        id: "%customerId%",
                        authState: "AUTHENTICATED"
                    }
                }
            }
        },
        pageLoadRules: [{
            name: "Account Pages - Customer Id Check",
            trigger: [{
                engine: "sc",
                command: "setVars",
                arguments: [{
                    eVar71: "%serviceType%",
                    eVar80: "%auth0id%"
                }]
            }, {
                command: "loadScript",
                arguments: [{
                    sequential: !1,
                    scripts: [{
                        src: "satellite-5e96926f64746d3e170005c7.js"
                    }]
                }]
            }],
            scope: {
                URI: {
                    include: [/^\/account/i]
                }
            },
            event: "domready"
        }, {
            name: "Belong Live Agent Mbox",
            trigger: [{
                command: "loadScript",
                arguments: [{
                    sequential: !1,
                    scripts: [{
                        src: "satellite-5e96926f64746d3e17000627.js"
                    }]
                }]
            }],
            conditions: [function() {
                return _satellite.textMatch(_satellite.getVar("isBelongApp"), "FALSE")
            }
            , function() {
                if (/^(.*\.belong.com.au|preprod.belongtest.com.au)$/i.test(location.hostname))
                    return !0
            }
            ],
            event: "pagetop"
        }, {
            name: "FAQ Public Pages",
            trigger: [{
                engine: "sc",
                command: "setVars",
                arguments: [{
                    eVar71: "%serviceType%",
                    prop33: "%internalSearchString%",
                    channel: "bl"
                }]
            }, {
                engine: "sc",
                command: "addEvent",
                arguments: ["event3"]
            }, {
                engine: "sc",
                command: "customSetup",
                arguments: [function(e, a) {
                    function r() {
                        var e = n.location.pathname
                          , r = (n.location.hostname,
                        new Array)
                          , i = e.split("/");
                        for (s = 1; s < i.length; s++) {
                            var o = i[s];
                            o && (o = (o = (o = o.toLowerCase()).replace(/^\s+/, "")).replace(/ /g, "-"),
                            r.push(o))
                        }
                        _satellite.notify("Support Pages:s_taxArr:" + r.toString(), 1),
                        "undefined" == typeof jQuery && s_logTagErrorsFn("jQuery missing at time of execution", 1),
                        s_pageName = "";
                        for (var s = 0; s < r.length; s++)
                            s_pageName = 0 == s ? r[s] : s_pageName + ":" + r[s];
                        "undefined" != typeof s_pageName && (s_pageName = "bl:" + s_pageName),
                        t.addEventListener("load", function() {
                            var e = function(e) {
                                var t = n.createElement("a");
                                return t.href = e,
                                "" == t.host && (t.href = t.href),
                                t.pathname
                            };
                            t.BELONG = t.BELONG ? t.BELONG : {},
                            t.BELONG.dataLayer = t.BELONG.dataLayer ? t.BELONG.dataLayer : {},
                            $(n).on("belong.analytics.knowledge.openFaq", function(t, n) {
                                BELONG.dataLayer.openFaq = {
                                    event: t,
                                    data: n
                                },
                                a.pageName = "bl" + e(n.faqUrl).replace(/\//g, ":"),
                                _satellite.track("analytics-openFaq")
                            }),
                            $(n).on("belong.analytics.knowledge.thumbsUp", function(t, n) {
                                BELONG.dataLayer.thumbsUp = {
                                    event: t,
                                    data: n
                                },
                                a.pageName = "bl" + e(n.faqUrl).replace(/\//g, ":"),
                                _satellite.track("analytics-faq-thumbsUp")
                            }),
                            $(n).on("belong.analytics.knowledge.thumbsDown", function(t, n) {
                                BELONG.dataLayer.thumbsDown = {
                                    event: t,
                                    data: n
                                },
                                a.pageName = "bl" + e(n.faqUrl).replace(/\//g, ":"),
                                _satellite.track("analytics-faq-thumbsDown")
                            })
                        })
                    }
                    "function" == typeof r && r()
                }
                ]
            }],
            scope: {
                subdomains: {
                    include: [/support/i]
                },
                domains: [/belong\.com\.au$/i]
            },
            event: "pagetop"
        }, {
            name: "Hotjar Setup",
            trigger: [{
                command: "loadBlockingScript",
                arguments: [{
                    sequential: !0,
                    scripts: [{
                        src: "satellite-5e96926f64746d3e17000654.js"
                    }]
                }]
            }],
            conditions: [function() {
                return _satellite.textMatch(_satellite.getVar("hotjarDomains"), "TRUE")
            }
            ],
            event: "pagebottom"
        }, {
            name: "Join Form Page",
            trigger: [{
                engine: "sc",
                command: "addEvent",
                arguments: ["event29"]
            }, {
                command: "loadBlockingScript",
                arguments: [{
                    sequential: !0,
                    scripts: [{
                        src: "satellite-5e96926f64746d3e17000562.js"
                    }]
                }]
            }, {
                command: "loadScript",
                arguments: [{
                    sequential: !1,
                    scripts: [{
                        src: "satellite-5e96926f64746d3e17000561.js"
                    }]
                }]
            }],
            conditions: [function() {
                return _satellite.textMatch(_satellite.getVar("pathname"), /^\/.*join(\.htm(l)?|$)/i)
            }
            , function() {
                return _satellite.textMatch(_satellite.getVar("hostname"), "www.belong.com.au")
            }
            ],
            event: "pagetop"
        }, {
            name: "Livechat Click Listener",
            trigger: [{
                command: "loadScript",
                arguments: [{
                    sequential: !1,
                    scripts: [{
                        src: "satellite-5e96926f64746d3e170005df.js"
                    }]
                }]
            }],
            conditions: [function() {
                return _satellite.textMatch(_satellite.getVar("moorupDomains - April 14, 2020 09:49:51 PM"), "FALSE")
            }
            ],
            event: "domready"
        }, {
            name: "Load Google Scripts",
            conditions: [function() {
                var e = _satellite.settings.isStaging ? "GTM-KDQRBMT" : "GTM-P7RLS5N";
                return e = "GTM-P7RLS5N",
                t.GoogleAnalyticsObject = "ga",
                t.ga = t.ga || function() {
                    (t.ga.q = t.ga.q || []).push(arguments)
                }
                ,
                function(e, t, n, a, r) {
                    e[a] = e[a] || [],
                    e[a].push({
                        "gtm.start": (new Date).getTime(),
                        event: "gtm.js"
                    });
                    var i = t.getElementsByTagName(n)[0]
                      , o = t.createElement(n)
                      , s = "dataLayer" != a ? "&l=" + a : "";
                    o.async = !0,
                    o.src = "https://www.googletagmanager.com/gtm.js?id=" + r + s,
                    i.parentNode.insertBefore(o, i),
                    _satellite.notify("[BELONG LEGACY] Google Tag Manager Script loaded for " + r, 1)
                }(t, n, "script", "dataLayer", e),
                !0
            }
            ],
            event: "pagetop"
        }, {
            name: "Logged In Agent",
            trigger: [{
                engine: "sc",
                command: "setVars",
                arguments: [{
                    eVar17: "%agentID%",
                    eVar98: "Page Rule>Logged In Agent Testing"
                }]
            }],
            conditions: [function() {
                return !!n.querySelector("#GlobalData").getAttribute("data-is-agent")
            }
            ],
            event: "pagebottom"
        }, {
            name: "Logged In Pages",
            trigger: [{
                engine: "sc",
                command: "setVars",
                arguments: [{
                    eVar10: "%salesforce_id%",
                    eVar16: "%customerId%",
                    eVar18: "%customerType%",
                    prop73: "belong-logged-in"
                }]
            }, {
                engine: "sc",
                command: "addEvent",
                arguments: ["event8"]
            }, {
                command: "loadScript",
                arguments: [{
                    sequential: !1,
                    scripts: [{
                        src: "satellite-5e96926f64746d3e17000542.js"
                    }]
                }]
            }],
            conditions: [function() {
                return _satellite.textMatch(_satellite.getVar("isLoggedIn"), "true")
            }
            ],
            event: "domready"
        }, {
            name: "Login Page",
            trigger: [{
                engine: "sc",
                command: "customSetup",
                arguments: [function() {
                    s_pageName = "bl:login"
                }
                ]
            }],
            scope: {
                URI: {
                    include: [/\/am\/ui\/login/i, "SiteLoginPage", "siteloginpage"]
                }
            },
            event: "pagebottom"
        }, {
            name: "Logout Page",
            trigger: [{
                engine: "sc",
                command: "customSetup",
                arguments: [function() {
                    s_pageName = "bl:logout"
                }
                ]
            }],
            conditions: [function() {
                if (/SiteLoginPage/i.test(location.pathname) && /logout=true/i.test(location.search))
                    return !0
            }
            ],
            event: "pagebottom"
        }, {
            name: "One Off Payment",
            trigger: [{
                engine: "sc",
                command: "setVars",
                arguments: [{
                    prop31: "one off payment",
                    pageName: "%pageName%:completed"
                }]
            }, {
                engine: "sc",
                command: "addEvent",
                arguments: ["event12", "event37"]
            }],
            scope: {
                URI: {
                    include: ["/account/pay"]
                }
            },
            conditions: [function() {
                return _satellite.textMatch(_satellite.getQueryParam("SECURETOKENID"), /.+/i)
            }
            , function() {
                return _satellite.textMatch(_satellite.getQueryParam("INVOICE"), /.+/i)
            }
            ],
            event: "pagetop"
        }, {
            name: "jQuery trigger listeners",
            trigger: [{
                command: "loadScript",
                arguments: [{
                    sequential: !1,
                    scripts: [{
                        src: "satellite-5e96926f64746d3e1700050a.js"
                    }, {
                        src: "satellite-5e96926f64746d3e1700050b.js"
                    }]
                }]
            }],
            conditions: [function() {
                return _satellite.textMatch(_satellite.getVar("moorupDomains - April 14, 2020 09:49:51 PM"), "FALSE")
            }
            ],
            event: "domready"
        }],
        rules: [{
            name: "Account Add Speed Boost",
            trigger: [{
                command: "loadScript",
                arguments: [{
                    sequential: !1,
                    scripts: [{
                        src: "satellite-5e96923164746d38d5001c4a.js"
                    }]
                }]
            }],
            selector: "button#SpeedBoostSubmit",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "Account Join Completion Steps",
            trigger: [{
                command: "loadScript",
                arguments: [{
                    sequential: !1,
                    scripts: [{
                        src: "satellite-5e96923164746d38d5001c37.js"
                    }]
                }]
            }],
            scope: {
                URI: {
                    include: [/^\/account/i]
                }
            },
            selector: "div.light-box-module-container button",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "Account Troubleshooting Start",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar10: "%salesforce_id%",
                        eVar100: "account troubleshooting start",
                        eVar16: "%customerId%",
                        eVar18: "%customerType%",
                        pageName: "bl:account:troubleshooting:cards:question_adsl_landing"
                    }
                }]
            }],
            selector: "a.open-troubleshooting-modal",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "Contact Us Phone Reveal",
            trigger: [{
                command: "loadScript",
                arguments: [{
                    sequential: !1,
                    scripts: [{
                        src: "satellite-5e96923264746d38d5001cfe.js"
                    }]
                }]
            }],
            scope: {
                URI: {
                    include: ["/contact-us"]
                }
            },
            selector: "a.contact-us-btn",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "Critical Information Summary",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "product-builder:click:critical information summary",
                    addEvent: ["event4"]
                }]
            }],
            selector: "#label-join-cis-acceptChk a",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "Customer Terms",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "product-builder:click:customer terms",
                    addEvent: ["event4"]
                }]
            }],
            selector: "#label-join-cis-portChk a[href='https://www.belong.com.au/customer-terms?section=general-terms']",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "FAQ Public Phone Reveal",
            trigger: [{
                command: "loadScript",
                arguments: [{
                    sequential: !1,
                    scripts: [{
                        src: "satellite-5e96923164746d38d5001cb4.js"
                    }]
                }]
            }],
            scope: {
                subdomains: {
                    include: ["support"]
                }
            },
            selector: "a.contact-us-btn",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "Join Form Submit Click ",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "Join Form - Join Button - submit click",
                    addEvent: ["event6"]
                }]
            }],
            scope: {
                URI: {
                    include: [/join/i]
                }
            },
            selector: "a#join-payconfirm-submit",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "Login Failure",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "login button:click:username or password error"
                }]
            }],
            scope: {
                URI: {
                    include: ["SiteLoginPage"]
                }
            },
            event: "dataelementchange(loginPasswordErrorDisplayed)",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "Set new password",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        prop31: "set new password",
                        pageName: "bl:account:personal-details:set-new-password"
                    },
                    addEvent: ["event12"]
                }]
            }],
            selector: "button#change-password-submit",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "Speedboost Promotion T&Cs",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "product-builder:click:speedboost promo terms",
                    addEvent: ["event4"]
                }]
            }],
            selector: ".show-for-promotional-speedboost a",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !0
        }, {
            name: "Support Form submit click",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "Contact Us Form Submit Click",
                    setVars: {
                        eVar32: "%Contact Us Enquiry Type%",
                        prop32: "%Contact Us Enquiry Type%"
                    },
                    addEvent: ["event4"]
                }]
            }],
            scope: {
                subdomains: {
                    include: ["help"]
                }
            },
            selector: "input#email_submit",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "Update Mobile Number",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        prop31: "update mobile number",
                        pageName: "bl:account:personal-details:update-mobile-number"
                    },
                    addEvent: ["event12"]
                }]
            }],
            selector: "button#change-mobile-submit",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "View Modem Details",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        prop31: "view modem details",
                        pageName: "bl:account:account-details:view-modem-details"
                    },
                    addEvent: ["event12"]
                }]
            }],
            selector: "a#belong-card-container__show-password-btn",
            event: "click",
            bubbleFireIfParent: !0,
            bubbleFireIfChildFired: !0,
            bubbleStop: !1
        }, {
            name: "(Rule for AAM integration)",
            event: "782e950c976718383ed5977171fc5e5e.load",
            trigger: [{
                tool: "e8a934c3609c8ac1ede1d16bf48e87dc4fd97665",
                command: "fire",
                arguments: [function(e) {
                    function t(e) {
                        for (var t in e)
                            if (e.hasOwnProperty(t) && "" !== t)
                                return !1;
                        return !0
                    }
                    var a, r, i = _satellite.getVar("auth0id"), o = "512735", s = "442858", c = _satellite.getVar("salesforce_id"), l = _satellite.getVar("utm_user_id");
                    a = l || i || c,
                    r = i ? o : s;
                    var u = DIL.create({
                        partner: "teamtelstra",
                        uuidCookie: {
                            name: "aam_uuid",
                            days: 30
                        },
                        declaredId: {
                            dpid: r,
                            dpuuid: a
                        }
                    });
                    i && u.api.aamIdSync({
                        dpid: o,
                        dpuuid: i,
                        minutesToLive: 1
                    }),
                    c && u.api.aamIdSync({
                        dpid: s,
                        dpuuid: c,
                        minutesToLive: 1
                    }),
                    n.referrer && u.api.signals({
                        c_dilReferer: n.referrer
                    }),
                    location.href && u.api.signals({
                        c_locationHref: _satellite.getVar("url")
                    });
                    var d, p = DIL.tools.decomposeURI(n.URL);
                    delete p.search,
                    delete p.href,
                    t(p.uriParams) || u.api.signals(p.uriParams, "c_"),
                    delete p.uriParams,
                    u.api.signals(p, "c_"),
                    d = void 0 !== e && e === Object(e) && "undefined" != typeof e.account && e.account ? s_gi(e.account) : s_gi(s_account),
                    DIL.modules.siteCatalyst.init(d, u, {
                        names: ["pageName", "channel", "campaign", "products", "events", "pe", "referrer", "server", "purchaseID", "zip", "state"],
                        iteratedNames: [{
                            name: "eVar",
                            maxIndex: 100
                        }, {
                            name: "prop",
                            maxIndex: 75
                        }, {
                            name: "pev",
                            maxIndex: 3
                        }, {
                            name: "hier",
                            maxIndex: 4
                        }]
                    }),
                    _satellite.settings.notifications && console.log("BL LEGACY AAM ID SYNC", {
                        adobeDil: u,
                        auth0Id: i,
                        auth0PartnerId: o,
                        salesforceId: c,
                        salesforcePartnerId: s,
                        declaredId: a,
                        declaredPartnerId: r,
                        utmUserId: l
                    })
                }
                ]
            }]
        }],
        directCallRules: [{
            name: "hotjar_recording_tags",
            trigger: [{
                command: "loadScript",
                arguments: [{
                    sequential: !0,
                    scripts: [{
                        src: "satellite-5e969b5f64746d46050017c8.js"
                    }]
                }]
            }]
        }, {
            name: "conversion_pixel",
            trigger: [{
                command: "loadScript",
                arguments: [{
                    sequential: !0,
                    scripts: [{
                        src: "satellite-5e969b5f64746d46050017b6.js"
                    }]
                }]
            }]
        }, {
            name: "analytics-eventTagError",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "analytics-eventTagError",
                    setVars: {
                        eVar100: "analytics-eventTagError",
                        eVar91: "Custom event triggered with no virtualPage OR eventCategory",
                        eVar99: "%dtmTriggerData%"
                    },
                    addEvent: ["event91"]
                }]
            }]
        }, {
            name: "analytics-faq-thumbsDown",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "Support Article Feedback : Not Helpful",
                    setVars: {
                        eVar8: "D=g",
                        eVar93: "%virtualPage%",
                        eVar94: "%window.s_pageName%",
                        channel: "bl:faq-public",
                        pageURL: "%virtualPage%"
                    },
                    addEvent: ["event4", "event41"]
                }]
            }]
        }, {
            name: "analytics-faq-thumbsUp",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "Support Article Feedback : Helpful",
                    setVars: {
                        eVar8: "%virtualPage%",
                        eVar93: "D=pageName",
                        eVar94: "%window.s_pageName%",
                        channel: "bl:faq-public",
                        pageURL: "%virtualPage%"
                    },
                    addEvent: ["event4", "event40"]
                }]
            }]
        }, {
            name: "analytics-openFaq",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar8: "D=g",
                        eVar93: "D=pageName"
                    },
                    addEvent: ["event3"]
                }]
            }]
        }, {
            name: "analytics-trackAccountAddSpeedBoost",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar10: "%salesforce_id%",
                        eVar100: "analytics-trackAccountAddSpeedBoost",
                        eVar16: "%customerId%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop31: "AddSpeedBoost",
                        pageName: "bl:account:account-details:add-speed-boost-complete"
                    },
                    customSetup: function(e, t) {
                        var n = []
                          , a = "";
                        try {
                            a = $('input[id^="speedboost"]:checked').attr("id")
                        } catch (o) {}
                        if (a) {
                            var r = "add:speed-boost:" + a;
                            n.push(r)
                        }
                        t.products = "";
                        try {
                            for (var i = 0; i < n.length; i++)
                                i > 0 && (t.products = t.products + ","),
                                t.products = t.products + ";" + n[i] + ";1;0"
                        } catch (o) {}
                    },
                    addEvent: ["event12", "event9", "purchase"]
                }]
            }]
        }, {
            name: "analytics-trackAccountAddVoice",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar10: "%salesforce_id%",
                        eVar100: "analytics-trackAccountAddVoice",
                        eVar16: "%customerId%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop31: "AddVoice",
                        pageName: "bl:account:account-details:add-voice-complete"
                    },
                    customSetup: function(e, t) {
                        var n = [];
                        try {
                            var a = "";
                            if ("undefined" != typeof BELONG.dataLayer.event.voicePlan && null != BELONG.dataLayer.event.voicePlan && (a = BELONG.dataLayer.event.voicePlan),
                            a) {
                                var r = "add:voice-plan:" + a;
                                n.push(r)
                            }
                        } catch (o) {}
                        t.products = "";
                        try {
                            for (var i = 0; i < n.length; i++)
                                i > 0 && (t.products = t.products + ","),
                                t.products = t.products + ";" + n[i] + ";1;0"
                        } catch (o) {}
                    },
                    addEvent: ["event12", "event9", "purchase"]
                }]
            }]
        }, {
            name: "analytics-trackAccountJoinCompletion",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    type: "o",
                    linkName: "%eventCategory%:%eventAction%:%eventLabel%",
                    setVars: {
                        eVar10: "%salesforce_id%",
                        eVar100: "analytics-trackAccountJoinCompletion",
                        eVar16: "%customerId%",
                        eVar18: "%customerType%",
                        pageName: "bl:account:join:%eventCategory%"
                    },
                    customSetup: function(e, t) {
                        try {
                            var n = _satellite.getVar("eventCategory");
                            /surveycontinue/i.test(n) && (t.events = t.apl(t.events, "event34", ",", 1))
                        } catch (a) {}
                    },
                    addEvent: ["event8"]
                }]
            }]
        }, {
            name: "analytics-trackAppointmentSetting",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-trackAppointmentSetting",
                        eVar26: "%eventCategory%:%eventAction%:%eventLabel%",
                        eVar27: "%pageName%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop27: "%pageName%",
                        prop31: "%eventCategory%"
                    },
                    customSetup: function(e, t) {
                        var n = {
                            DEFAULTS: {
                                category: "Nbn-Appointment",
                                action: "click",
                                label: "proof of occupancy label not set",
                                error: ""
                            },
                            NBN_GET_AVAILABLE_APPOINTMENTS: {
                                label: "Get available appointments",
                                action: "load"
                            },
                            NBN_GET_AVAILABLE_APPOINTMENTS_SUCCESS: {
                                label: "Get available appointments succeeds",
                                action: "load"
                            },
                            NBN_GET_AVAILABLE_APPOINTMENTS_FAIL: {
                                label: "Get available appointments failure",
                                action: "load"
                            },
                            NBN_SET_APPOINTMENT_DETAILS: {
                                label: "Set appointment details",
                                action: "load"
                            },
                            NBN_UPDATE_APPOINTMENT_TIME: {
                                label: "Triggered on selection of time",
                                action: "click"
                            },
                            NBN_SUBMIT_CHANGE_APPOINTMENT: {
                                label: "Submit change appointment",
                                action: "click"
                            },
                            NBN_CHANGE_APPOINTMENT_SUCCESS: {
                                label: "Submit change appointment success",
                                action: "load"
                            },
                            NBN_CHANGE_APPOINTMENT_FAILURE: {
                                label: "Submit change appointment fails",
                                action: "load"
                            },
                            NBN_APPOINTMENT_CHANGE_PROGRESS: {
                                label: "Submission is in progress",
                                action: "load"
                            },
                            NBN_RETRIEVE_APPOINTMENT: {
                                label: "Retrieve saved appointment",
                                action: "load"
                            },
                            NBN_RETRIEVE_APPOINTMENT_SUCCESS: {
                                label: "Retrieve Appointment success",
                                action: "load"
                            },
                            NBN_RETRIEVE_APPOINTMENT_FAIL: {
                                label: "Retrieve Appointment fails",
                                action: "load"
                            }
                        }
                          , a = (e = ((BELONG || {}).dataLayer || {}).event || {}).type;
                        n[a] && (e.eventAction = n[a].action ? n[a].action : n.DEFAULTS.action,
                        e.eventLabel = n[a].label ? n[a].label : n.DEFAULTS.label,
                        e.eventCategory = n[a].category ? n[a].category : n.DEFAULTS.category,
                        e.errorCode = n[a].error ? n[a].error : n.DEFAULTS.error),
                        BELONG.dataLayer.event = e,
                        t.prop26 = [e.eventCategory, e.eventAction, e.eventLabel].join(":"),
                        t.eVar26 = "D=c26",
                        t.prop31 = e.eventCategory,
                        _satellite.notify("analytics-trackAppointmentSetting custom page code", 3),
                        _satellite.custom_debugMode && console.debug("eventType:", a, BELONG.dataLayer.event)
                    },
                    addEvent: ["event12", "event4"]
                }]
            }]
        }, {
            name: "analytics-trackCallbackSubmit",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    type: "o",
                    linkName: "%eventCategory%:%eventAction%:%eventLabel%",
                    setVars: {
                        eVar100: "analytics-trackCallbackSubmit",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop27: "%pageName%",
                        prop32: "%pageName%:call me back",
                        pageName: "%pageName%:call me back"
                    },
                    addEvent: ["event18"]
                }]
            }]
        }, {
            name: "analytics-trackCustomLink",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "%eventCategory%:%eventAction%:%eventLabel%",
                    setVars: {
                        eVar100: "analytics-trackCustomLink",
                        eVar26: "%eventCategory%:%eventAction%:%eventLabel%",
                        eVar27: "%pageName%",
                        eVar55: "%utm_user_id%",
                        eVar80: "%auth0id%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "D=v26",
                        prop27: "D=v27"
                    },
                    addEvent: ["event4"]
                }]
            }]
        }, {
            name: "analytics-trackGlobalRevealPhoneNumber",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    type: "o",
                    linkName: "%eventCategory%:%eventAction%:%eventLabel%",
                    setVars: {
                        eVar100: "analytics-trackGlobalRevealPhoneNumber",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop27: "%pageName%",
                        prop32: "%pageName%:reveal phone number",
                        pageName: "%pageName%:reveal phone number"
                    },
                    addEvent: ["event18"]
                }]
            }]
        }, {
            name: "analytics-trackJoinComplete",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar10: "%salesforce_id%",
                        eVar100: "analytics-trackJoinComplete",
                        eVar16: "%customerIdJoin%",
                        eVar36: "%promoCode%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        pageName: "bl:join:join-complete"
                    },
                    customSetup: function(e, t) {
                        var n = "join-flow";
                        if ("undefined" != typeof t.getPreviousValue) {
                            var a = t.getPreviousValue("", "gpv_ps");
                            void 0 !== a && a && "no value" != a && (t.products = a,
                            n = "account-page")
                        }
                        if ("join-flow" == n) {
                            var r = [];
                            try {
                                var i = "";
                                "undefined" != typeof BELONG.dataLayer.event.serviceClass && null != BELONG.dataLayer.event.serviceClass && (i = BELONG.dataLayer.event.serviceClass),
                                i && r.push("service-class:" + i)
                            } catch (g) {}
                            try {
                                var o = "";
                                "undefined" != typeof BELONG.dataLayer.event.planSizeType && null != BELONG.dataLayer.event.planSizeType && (o = BELONG.dataLayer.event.planSizeType),
                                o && (c = o,
                                r.push(c))
                            } catch (g) {}
                            try {
                                var s = "";
                                if ("string" == typeof BELONG.dataLayer.event.withSpeedBoost && (s = BELONG.dataLayer.event.withSpeedBoost),
                                "true" === s && "undefined" != typeof BELONG.dataLayer.event.planSpeed && null != BELONG.dataLayer.event.planSpeed && BELONG.dataLayer.event.planSpeed) {
                                    var c = "speed:" + BELONG.dataLayer.event.planSpeed;
                                    r.push(c)
                                }
                            } catch (g) {}
                            try {
                                var l = "";
                                "undefined" != typeof BELONG.dataLayer.event.withContract && null != BELONG.dataLayer.event.withContract && (l = BELONG.dataLayer.event.withContract),
                                "true" === l ? r.push("contract:yes") : "false" === l && r.push("contract:no")
                            } catch (g) {}
                            try {
                                var u = "";
                                if ("undefined" != typeof BELONG.dataLayer.event.voicePlan && null != BELONG.dataLayer.event.voicePlan && (u = BELONG.dataLayer.event.voicePlan),
                                u) {
                                    c = "voice-plan:" + u;
                                    r.push(c)
                                }
                            } catch (g) {}
                            t.products = "";
                            try {
                                for (var d = 0; d < r.length; d++)
                                    d > 0 && (t.products = t.products + ","),
                                    t.products = t.products + ";" + r[d] + ";1;0";
                                if ("undefined" != typeof BELONG.dataLayer.totalMinCost && BELONG.dataLayer.totalMinCost > 0 && (t.products = t.products + ",;total-min-cost;1;" + BELONG.dataLayer.totalMinCost.toString()),
                                "undefined" != typeof t.getPreviousValue) {
                                    t.getPreviousValue(t.products, "gpv_ps"),
                                    t.getPreviousValue("order-complete", "gpv_oc");
                                    if ("undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && "undefined" != typeof BELONG.dataLayer.event && "undefined" != typeof BELONG.dataLayer.event.userid && BELONG.dataLayer.event.userid) {
                                        var p = BELONG.dataLayer.event.userid;
                                        t.getPreviousValue(p, "gpv_bc")
                                    }
                                }
                            } catch (g) {}
                        }
                    },
                    addEvent: ["event5", "purchase"]
                }]
            }]
        }, {
            name: "analytics-trackJoinStep",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-trackJoinStep",
                        eVar99: "%dtmTriggerData%",
                        pageName: "bl:join:%eventLabel%"
                    },
                    customSetup: function(e, t) {
                        try {
                            var n = _satellite.getVar("eventLabel");
                            /^1\-/i.test(n) ? t.events = t.apl(t.events, "event30", ",", 1) : /^2\-/i.test(n) ? t.events = t.apl(t.events, "event31", ",", 1) : /^3\-pay/i.test(n) ? t.events = t.apl(t.events, "event32", ",", 1) : /^4\-check|^3\-check/i.test(n) && (t.events = t.apl(t.events, "event33", ",", 1))
                        } catch (r) {}
                        try {
                            if ("undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && ("undefined" == typeof BELONG.dataLayer.totalMinCost || 0 === BELONG.dataLayer.totalMinCost)) {
                                var a = parseInt($("span#total-min-cost").text());
                                BELONG.dataLayer.totalMinCost = a
                            }
                        } catch (r) {}
                    }
                }]
            }]
        }, {
            name: "analytics-trackLiveChatClick",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "Live Chat Click",
                    setVars: {
                        eVar26: "live-chat:click:%liveChatImage%",
                        prop26: "live-chat:click:%liveChatImage%"
                    },
                    addEvent: ["event16", "event4"]
                }]
            }]
        }, {
            name: "analytics-trackManageServiceDisplay",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar10: "%salesforce_id%",
                        eVar100: "analytics-trackManageServiceDisplay",
                        eVar16: "%customerId%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        pageName: "bl:account:manage-service:display-current-plan"
                    },
                    customSetup: function(e, t) {
                        var n = [];
                        try {
                            var a = "";
                            "undefined" != typeof BELONG.dataLayer.event.oldPlan.planType && null != BELONG.dataLayer.event.oldPlan.planType && (a = BELONG.dataLayer.event.oldPlan.planType);
                            var r = "";
                            "undefined" != typeof BELONG.dataLayer.event.oldPlan.planSize && null != BELONG.dataLayer.event.oldPlan.planSize && (r = BELONG.dataLayer.event.oldPlan.planSize);
                            var i = "";
                            "undefined" != typeof BELONG.dataLayer.event.oldPlan.contractOption && null != BELONG.dataLayer.event.oldPlan.contractOption && (i = BELONG.dataLayer.event.oldPlan.contractOption),
                            valToPush = "manage-service : current-plan",
                            a && (valToPush = valToPush + " : " + a),
                            r && (valToPush = valToPush + " : " + r),
                            i && (valToPush = valToPush + " : " + i),
                            n.push(valToPush)
                        } catch (s) {}
                        t.products = "";
                        try {
                            for (var o = 0; o < n.length; o++)
                                o > 0 && (t.products = t.products + ","),
                                t.products = t.products + ";" + n[o]
                        } catch (s) {}
                    },
                    addEvent: ["event58"]
                }]
            }]
        }, {
            name: "analytics-trackManageServiceReview",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar10: "%salesforce_id%",
                        eVar100: "analytics-trackManageServiceReview",
                        eVar16: "%customerId%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        pageName: "bl:account:manage-service:review-changes"
                    },
                    customSetup: function() {
                        var e = [];
                        try {
                            var t = "";
                            "undefined" != typeof BELONG.dataLayer.event.oldPlan.planType && null != BELONG.dataLayer.event.oldPlan.planType && (t = BELONG.dataLayer.event.oldPlan.planType);
                            var n = ""
                              , a = "";
                            "undefined" != typeof BELONG.dataLayer.event.updateAction.originalPlanSize && null != BELONG.dataLayer.event.updateAction.originalPlanSize && (n = BELONG.dataLayer.event.updateAction.originalPlanSize),
                            "undefined" != typeof BELONG.dataLayer.event.updateAction.newPlanSize && null != BELONG.dataLayer.event.updateAction.newPlanSize && (a = BELONG.dataLayer.event.updateAction.newPlanSize);
                            var r = ""
                              , i = "";
                            "undefined" != typeof BELONG.dataLayer.event.updateAction.originalPlanSpeed && null != BELONG.dataLayer.event.updateAction.originalPlanSpeed && (r = BELONG.dataLayer.event.updateAction.originalPlanSpeed),
                            "undefined" != typeof BELONG.dataLayer.event.updateAction.newPlanSpeed && null != BELONG.dataLayer.event.updateAction.newPlanSpeed && (i = BELONG.dataLayer.event.updateAction.newPlanSpeed);
                            var o = ""
                              , s = "";
                            "undefined" != typeof BELONG.dataLayer.event.updateAction.originalContractOption && null != BELONG.dataLayer.event.updateAction.originalContractOption && (o = BELONG.dataLayer.event.updateAction.originalContractOption),
                            "undefined" != typeof BELONG.dataLayer.event.updateAction.newContractOption && null != BELONG.dataLayer.event.updateAction.newContractOption && (s = BELONG.dataLayer.event.updateAction.newContractOption),
                            "undefined" != typeof BELONG.dataLayer.event.updateAction.ipType && null != BELONG.dataLayer.event.updateAction.ipType && 1 == typeof BELONG.dataLayer.event.updateAction.ipType.hasChanged && (originalIpType = BELONG.dataLayer.event.updateAction.ipType.originalIpType.toLowerCase(),
                            newIpType = BELONG.dataLayer.event.updateAction.ipType.newIpType.toLowerCase()),
                            valToPush = "manage-service",
                            t && (valToPush = valToPush + " : " + t),
                            n != a && (valToPush = valToPush + " : change planSize : from " + n + " : to " + a,
                            e.push(valToPush)),
                            valToPush = "manage-service",
                            t && (valToPush = valToPush + " : " + t),
                            r != i && (valToPush = valToPush + " : change planSpeed : from " + r + " : to " + i,
                            e.push(valToPush)),
                            valToPush = "manage-service",
                            t && (valToPush = valToPush + " : " + t),
                            o != s && (valToPush = valToPush + " : change contract : from " + o + " : to " + s,
                            e.push(valToPush)),
                            valToPush = "manage-service",
                            t && (valToPush = valToPush + " : " + t),
                            originalIpType != newIpType && (valToPush = valToPush + " : change contract : from " + originalIpType + " : to " + newIpType,
                            e.push(valToPush))
                        } catch (l) {}
                        s_products = "";
                        try {
                            for (var c = 0; c < e.length; c++)
                                c > 0 && (s_products += ","),
                                s_products = s_products + ";" + e[c]
                        } catch (l) {}
                    },
                    addEvent: ["event59"]
                }]
            }]
        }, {
            name: "analytics-trackManageServiceUpdate",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar10: "%salesforce_id%",
                        eVar100: "analytics-trackManageServiceUpdate",
                        eVar16: "%customerId%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop31: "Manage Service",
                        pageName: "bl:account:manage-service:update-complete"
                    },
                    customSetup: function(e, t) {
                        function n(e) {
                            return e.currentPlan ? !(!e.currentPlan || !e.currentPlan.planDetail) && e.currentPlan.planDetail : !(!e || !e.planDetail) && e.planDetail
                        }
                        function a(e) {
                            var t;
                            if (e.currentPlan) {
                                for (h = -0; h < e.currentPlan.recurringChargeList.length; h++)
                                    "CONTRACT_CREDIT" === e.currentPlan.recurringChargeList[h].recurringChargeType ? chargeType = e.currentPlan.recurringChargeList[h].contractOption : chargeType = "NA";
                                return t
                            }
                            for (h = -0; h < e.recurringChargeList.length; h++)
                                "CONTRACT_CREDIT" !== e.recurringChargeList[h].recurringChargeType || e.recurringChargeList[h].contractOption;
                            return t
                        }
                        function r(e) {
                            var t;
                            if (e.currentPlan) {
                                for (h = -0; h < e.currentPlan.recurringChargeList.length; h++)
                                    "STATIC_IP" !== e.currentPlan.recurringChargeList[h].recurringChargeType || e.currentPlan.recurringChargeList[h].recurringChargeType;
                                return t
                            }
                            for (h = -0; h < e.recurringChargeList.length; h++)
                                "STATIC_IP" !== e.recurringChargeList[h].recurringChargeType || e.recurringChargeList[h].recurringChargeType;
                            return t
                        }
                        var i = []
                          , o = (((BELONG || {}).dataLayer || {}).event || {}).oldPlan || {}
                          , s = (((BELONG || {}).dataLayer || {}).event || {}).newPlan || {};
                        t.eVar101 = JSON.stringify({
                            oldPlan: o
                        }),
                        t.eVar102 = JSON.stringify({
                            newPlan: s
                        });
                        try {
                            var c = n(o, "planType") || n(s, c)
                              , l = n(o, "planSize")
                              , u = n(s, "planSize")
                              , d = n(o, "planSpeed")
                              , p = n(s, "planSpeed")
                              , g = a(o)
                              , f = a(s);
                            planIpTypeOld = r(o),
                            planIpTypeNew = r(s);
                            var m = "manage-service";
                            c && (m = m + " : " + c),
                            l != u && (m = m + " : change planSize : from " + (l || "") + " : to " + (u || ""),
                            i.push(m));
                            m = "manage-service";
                            c && (m = m + " : " + c),
                            d != p && (m = m + " : change planSpeed : from " + (d || "") + " : to " + (p || ""),
                            i.push(m));
                            m = "manage-service";
                            c && (m = m + " : " + c),
                            g != f && (m = m + " : change contract : from " + (g || "") + " : to " + (f || ""),
                            i.push(m));
                            m = "manage-service";
                            c && (m = m + " : " + c),
                            planIpTypeOld != planIpTypeNew && (m = m + " : change IP type : from " + (planIpTypeOld || "") + " : to " + (planIpTypeNew || ""),
                            i.push(m))
                        } catch (v) {
                            t.eVar91 = s_tagErrors = "Product Change details:" + _satellite.getVar("tagError")(v)
                        }
                        s_products = "";
                        try {
                            for (var h = 0; h < i.length; h++)
                                h > 0 && (s_products += ","),
                                s_products = s_products + ";" + i[h]
                        } catch (v) {
                            t.eVar91 = s_tagErrors = "Loop through the array" + _satellite.getVar("tagError")(v)
                        }
                    },
                    addEvent: ["event12", "event9", "purchase"]
                }]
            }]
        }, {
            name: "analytics-trackNesSurveyClosed",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-trackNesSurveyClosed",
                        eVar26: "%eventCategory%:%eventAction%:%eventLabel%",
                        eVar27: "%pageName%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop27: "%pageName%"
                    }
                }]
            }]
        }, {
            name: "analytics-trackNesSurveySubmitted",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-trackNesSurveySubmitted",
                        eVar26: "%eventCategory%:%eventAction%:%eventLabel%",
                        eVar27: "%pageName%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop27: "%pageName%"
                    }
                }]
            }]
        }, {
            name: "analytics-trackOrderTrackingView",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar19: "%orderTrackingSaleType%",
                        eVar56: "%cardMoreInformation%",
                        eVar59: "%cardCoolingOff%",
                        eVar60: "%cardAppointment%",
                        eVar61: "%cardCallback%",
                        eVar62: "%cardConnect%",
                        eVar63: "%cardErrorDetails%",
                        eVar64: "%cardMessages%",
                        eVar65: "%cardModemDelivery%",
                        eVar66: "%cardOrder%",
                        eVar67: "%cardService%",
                        eVar68: "%cardProofOfOccupancyDocuments%",
                        eVar71: "%serviceType%"
                    }
                }]
            }]
        }, {
            name: "analytics-trackPaymentDetailsUpdate",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-trackPaymentDetailsUpdate",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel% %errorCode%",
                        prop27: "%pageName%",
                        pageName: "bl:account:payment-details:%eventLabel% %errorCode%"
                    }
                }]
            }]
        }, {
            name: "analytics-trackPaymentDetailsUpdateSuccess",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-trackPaymentDetailsUpdateSuccess",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel% success",
                        prop27: "%pageName%",
                        prop31: "%eventCategory%:%eventLabel%",
                        pageName: "bl:account:payment-details:%eventLabel% success"
                    },
                    customSetup: function(e, t) {
                        try {
                            var n = _satellite.getVar("eventLabel");
                            /^retry/i.test(n) && "object" == typeof t && "undefined" != typeof t.apl && (t.events = t.apl(t.events, "event37", ",", 1))
                        } catch (a) {}
                    },
                    addEvent: ["event12"]
                }]
            }]
        }, {
            name: "analytics-trackPaymentPrePaySuccess",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-trackPaymentPrePaySuccess",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel% success",
                        prop27: "%pageName%",
                        prop31: "%eventCategory%:%eventLabel% success",
                        pageName: "bl:account:payment-details:%eventLabel% success"
                    },
                    addEvent: ["event12", "event39"]
                }]
            }]
        }, {
            name: "analytics-trackPersonalDetailsUpdate",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-trackPersonalDetailsUpdate",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop27: "%pageName%",
                        prop31: "%eventCategory%",
                        pageName: "bl:account:personal-details:update %eventLabel%"
                    },
                    addEvent: ["event12"]
                }]
            }]
        }, {
            name: "analytics-trackProductBuilderAvailability",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "bl:product builder availability",
                    setVars: {
                        eVar100: "analytics-trackProductBuilderAvailability",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%"
                    }
                }]
            }]
        }, {
            name: "analytics-trackProductBuilderCloseSq",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "bl:sq:sqClose",
                    setVars: {
                        eVar100: "analytics-trackProductBuilderCloseSq",
                        eVar26: "D=c26",
                        eVar40: "%serviceClass%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop34: "%eventLabel% %sqProduct%",
                        pageName: "%basePageName%:sq:sqClose"
                    },
                    customSetup: function(e, t) {
                        var n = (((BELONG || {}).dataLayer || {}).event || {}).productModel || {}
                          , a = n.planType || "unknown"
                          , r = n.calculatedMTC + ".00" || "0"
                          , i = n.contractTerm || "unknown"
                          , o = n.planSize || "unknown"
                          , s = n.planSpeed || "unknown"
                          , c = "1"
                          , l = [a, o, s, i].join(" ");
                        t.products = [a, l, c, r].join(";"),
                        t.linkTrackEvents = "event4,prodView",
                        BELONG.dataLayer.productData = {
                            planType: a,
                            calculatedMTC: r,
                            contractTerm: i,
                            planSize: o,
                            planSpeed: s,
                            quantity: c
                        }
                    },
                    addEvent: ["event4", "prodView"]
                }]
            }]
        }, {
            name: "analytics-trackProductBuilderContractSelection",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "%eventCategory%:%eventAction%:%eventLabel%",
                    setVars: {
                        eVar100: "analytics-trackProductBuilderContractSelection",
                        eVar26: "%eventCategory%:%eventAction%:%eventLabel%",
                        eVar40: "%serviceClass%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop34: "%eventLabel% %sqProduct%",
                        pageName: "%basePageName%:sq:sqClose"
                    },
                    customSetup: function(e, t) {
                        var n = ((BELONG || {}).dataLayer || {}).event || {}
                          , a = ((BELONG || {}).dataLayer || {}).productData || {}
                          , r = (n.contractOption || "").toUpperCase() || "unknown"
                          , i = a.planType || "unknown"
                          , o = a.planSpeed || "unknown"
                          , s = a.planSize || "unknown"
                          , c = "1"
                          , l = $("span[data\\-bind$=tmc]").text() + ".00"
                          , u = [i, s, o, r].join(" ");
                        t.products = [i, u, c, l].join(";"),
                        t.linkTrackEvents = "event4,prodView",
                        BELONG.dataLayer.productData = {
                            planType: i,
                            calculatedMTC: l,
                            contractTerm: r,
                            planSize: s,
                            planSpeed: o,
                            quantity: c
                        }
                    },
                    addEvent: ["event4", "prodView"]
                }]
            }]
        }, {
            name: "analytics-trackProductBuilderJoin",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-trackProductBuilderJoin",
                        eVar99: "%dtmTriggerData%",
                        pageName: "%basePageName%:productBuilder:join"
                    },
                    addEvent: ["event30"]
                }]
            }, {
                command: "loadScript",
                arguments: [{
                    sequential: !0,
                    scripts: [{
                        src: "satellite-5e969b9d64746d22ca0018fe.js"
                    }]
                }]
            }]
        }, {
            name: "analytics-trackProductBuilderPromoCodeApplyFail",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-trackProductBuilderPromoCodeApplyFail",
                        eVar36: "%promoCodeProductBuilder%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        pageName: "%basePageName%:apply promo code error"
                    }
                }]
            }]
        }, {
            name: "analytics-trackProductBuilderPromoCodeApplySuccess",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-trackProductBuilderPromoCodeApplySuccess",
                        eVar36: "%promoCodeProductBuilder%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        pageName: "%basePageName%:apply promo code success"
                    },
                    addEvent: ["event42"]
                }]
            }]
        }, {
            name: "analytics-trackProductBuilderSpeedboostSelection",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "%eventCategory%:%eventAction%:%eventLabel%",
                    setVars: {
                        eVar100: "analytics-trackProductBuilderContractSelection",
                        eVar26: "%eventCategory%:%eventAction%:%eventLabel%",
                        eVar40: "%serviceClass%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop34: "%eventLabel% %sqProduct%",
                        pageName: "%basePageName%:sq:sqClose"
                    },
                    customSetup: function(e, t) {
                        var n = ((BELONG || {}).dataLayer || {}).event || {}
                          , a = ((BELONG || {}).dataLayer || {}).productData || {}
                          , r = n.speedboostOption || "unknown"
                          , i = a.planType || "unknown"
                          , o = a.contractTerm || "unknown"
                          , s = a.planSize || "unknown"
                          , c = "1"
                          , l = [i, s, r, o].join(" ")
                          , u = $("span[data\\-bind$=tmc]").text() + ".00";
                        t.products = [i, l, c, u].join(";"),
                        t.linkTrackEvents = "event4,prodView",
                        BELONG.dataLayer.productData = {
                            planType: i,
                            calculatedMTC: u,
                            contractTerm: o,
                            planSize: s,
                            planSpeed: r,
                            quantity: c
                        }
                    },
                    addEvent: ["event4", "prodView"]
                }]
            }]
        }, {
            name: "analytics-trackProofOfOccupancy",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    type: "o",
                    linkName: "account:upload:proof of occupancy",
                    setVars: {
                        eVar100: "analytics-trackProofOfOccupancy",
                        eVar26: "%eventCategory%:%eventAction%:%eventLabel%",
                        eVar27: "%pageName%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop27: "%pageName%",
                        prop31: "%eventCategory%",
                        channel: "bl:account:upload"
                    },
                    customSetup: function(e, t) {
                        var n = {
                            DEFAULTS: {
                                category: "Proof-Of-Dwelling",
                                action: "click",
                                label: "proof of occupancy label not set",
                                error: ""
                            },
                            GET_POD_REQUEST: {
                                label: "",
                                action: ""
                            },
                            DOCUMENT_UPLOAD_SET_PRIMARY_FILE_TYPE: {
                                label: "Select primary file option"
                            },
                            DOCUMENT_UPLOAD_SET_SECONDARY_FILE_TYPE: {
                                label: "Select secondary file option"
                            },
                            DOCUMENT_UPLOAD_SET_NAME_MATCHES: {
                                label: "Check if name matches",
                                action: "validation"
                            },
                            DOCUMENT_UPLOAD_SET_PRIMARY_FILE: {
                                label: "Select primary file",
                                action: "file-select"
                            },
                            DOCUMENT_UPLOAD_SET_SECONDARY_FILE: {
                                label: "Select secondary file",
                                action: "file-select"
                            },
                            DOCUMENT_UPLOAD_ERROR_UNALLOWED_MIMETYPE_PRIMARY: {
                                label: "Primary file mime type error",
                                action: "file-error"
                            },
                            DOCUMENT_UPLOAD_ERROR_UNALLOWED_MIMETYPE_SECONDARY: {
                                label: "Secondary file mime type error",
                                action: "file-error"
                            },
                            DOCUMENT_UPLOAD_ERROR_FILE_SIZE_PRIMARY: {
                                label: "Primary file size error",
                                action: "file-error"
                            },
                            DOCUMENT_UPLOAD_ERROR_FILE_SIZE_SECONDARY: {
                                label: "Secondary file size error",
                                action: "file-error"
                            },
                            DOCUMENT_UPLOAD_FILE_COUNT_ERROR: {
                                label: "Primary file count error",
                                action: "file-error"
                            },
                            DOCUMENT_UPLOAD_MODAL_OPEN: {
                                label: "More information modal open"
                            },
                            DOCUMENT_UPLOAD_MODAL_CLOSE: {
                                label: "More information modal close"
                            },
                            DOCUMENT_UPLOAD_REMOVE_FILES: {
                                label: "Remove files"
                            },
                            DOCUMENT_UPLOAD_REMOVE_SECONDARY_FILES: {
                                label: "Remove secondary files"
                            },
                            DOCUMENT_UPLOAD_FILE_UPLOAD_COMPLETE: {
                                label: "Upload complete",
                                action: "file upload"
                            },
                            DOCUMENT_UPLOAD_UPDATE_PROGRESS: {
                                label: "Triggered to get the upload progress",
                                action: "file upload"
                            },
                            GET_POD_REQUEST: {
                                label: "Pod status",
                                action: "load"
                            },
                            DOCUMENT_UPLOAD_GET_POD_SUCCESS: {
                                label: "Pod success",
                                action: "load"
                            },
                            DOCUMENT_UPLOAD_GET_POD_FAIL: {
                                label: "Pod failure",
                                action: "load"
                            },
                            DOCUMENT_UPLOAD_ERROR: {
                                label: "Upload error",
                                action: "file upload"
                            }
                        }
                          , a = (e = ((BELONG || {}).dataLayer || {}).event || {}).type;
                        n[a] && (e.eventAction = n[a].action ? n[a].action : n.DEFAULTS.action,
                        e.eventLabel = n[a].label ? n[a].label : n.DEFAULTS.label,
                        e.eventCategory = n[a].category ? n[a].category : n.DEFAULTS.category,
                        e.errorCode = n[a].error ? n[a].error : n.DEFAULTS.error),
                        BELONG.dataLayer.event = e,
                        t.prop26 = [e.eventCategory, e.eventAction, e.eventLabel].join(":"),
                        t.eVar26 = "D=c26",
                        t.prop31 = e.eventCategory,
                        _satellite.notify("analytics-trackProofOfOccupancy custom page code", 3),
                        _satellite.custom_debugMode && console.debug("eventType:", a, BELONG.dataLayer.event)
                    },
                    addEvent: ["event12", "event4"]
                }]
            }]
        }, {
            name: "analytics-trackServiceType",
            trigger: [{
                engine: "sc",
                command: "trackLink",
                arguments: [{
                    setVars: {
                        eVar71: "%serviceType%",
                        eVar79: "%broadbandTechType%"
                    }
                }]
            }]
        }, {
            name: "analytics-trackTroubleshootingCards",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    type: "o",
                    linkName: "%eventCategory%:%eventAction%:%eventLabel%",
                    setVars: {
                        eVar10: "%salesforce_id%",
                        eVar100: "analytics-trackTroubleshootingCards",
                        eVar16: "%customerId%",
                        eVar18: "%customerType%",
                        pageName: "bl:account:troubleshooting:%planType%:cards:%nextCardId%"
                    }
                }]
            }]
        }, {
            name: "analytics-trackTroubleshootingContact",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    type: "o",
                    linkName: "%eventCategory%:%eventAction%:%eventLabel%",
                    setVars: {
                        eVar10: "%salesforce_id%",
                        eVar100: "analytics-trackTroubleshootingContact",
                        eVar16: "%customerId%",
                        eVar18: "%customerType%",
                        prop32: "%troubleshootingContactType%",
                        pageName: "bl:account:troubleshooting:contact:submit"
                    },
                    addEvent: ["event18"]
                }]
            }]
        }, {
            name: "analytics-trackVirtualPage",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    type: "o",
                    linkName: "%eventCategory%:%eventAction%:%eventLabel%",
                    setVars: {
                        eVar100: "analytics-trackVirtualPage",
                        eVar26: "%eventCategory%:%eventAction%:%eventLabel%",
                        eVar27: "%pageName%",
                        eVar55: "%utm_user_id%",
                        eVar74: "%eoiReason%",
                        eVar80: "%auth0id%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "D=v26",
                        prop27: "D=v27",
                        pageName: "%basePageName%:%virtualPage%"
                    }
                }]
            }]
        }, {
            name: "analytics-tracksqAddress",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-tracksqAddress",
                        eVar38: "%addressId%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        pageName: "%basePageName%:sq:sqSelectAddress"
                    }
                }]
            }]
        }, {
            name: "analytics-tracksqCheck",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-tracksqCheck",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop34: "%eventLabel%",
                        pageName: "%basePageName%:sq:sqCheck"
                    },
                    addEvent: ["event25"]
                }]
            }]
        }, {
            name: "analytics-tracksqFail",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-tracksqFail",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop34: "%eventLabel%",
                        pageName: "%basePageName%:sq:sqFail"
                    },
                    addEvent: ["event27"]
                }]
            }]
        }, {
            name: "analytics-tracksqGeneric",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-tracksqGeneric",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop34: "%eventLabel%",
                        pageName: "%basePageName%:sq:%eventCategory%"
                    }
                }]
            }, {
                command: "loadScript",
                arguments: [{
                    sequential: !1,
                    scripts: [{
                        src: "satellite-5e969b9d64746d22ca00187b.js"
                    }]
                }]
            }]
        }, {
            name: "analytics-tracksqSuccess",
            trigger: [{
                engine: "sc",
                command: "trackPageView",
                arguments: [{
                    setVars: {
                        eVar100: "analytics-tracksqSuccess",
                        eVar40: "%serviceClass%",
                        eVar99: "%dtmTriggerData%",
                        prop26: "%eventCategory%:%eventAction%:%eventLabel%",
                        prop34: "%eventLabel% %sqProduct%",
                        pageName: "%basePageName%:sq:sqSuccess"
                    },
                    addEvent: ["event26"]
                }]
            }]
        }],
        settings: {
            trackInternalLinks: !0,
            libraryName: "satelliteLib-2e0b9f8cfcb3743674e7fdbc57e2ffcfeec196e8",
            isStaging: !1,
            allowGATTcalls: !1,
            downloadExtensions: /\.(?:doc|docx|eps|jpg|png|svg|xls|ppt|pptx|pdf|xlsx|tab|csv|zip|txt|vsd|vxd|xml|js|css|rar|exe|wma|mov|avi|wmv|mp3|wav|m4v)($|\&|\?)/i,
            notifications: !1,
            utilVisible: !1,
            domainList: ["belong.com.au", "telstra.com.au"],
            scriptDir: "09d79ff7891fd87dd491ffa94fd3a8d7eb4d80b3/scripts/",
            euCookieName: "sat_track",
            tagTimeout: 3e3
        },
        data: {
            URI: n.location.pathname + n.location.search,
            browser: {},
            cartItems: [],
            revenue: "",
            host: {
                http: "assets.adobedtm.com",
                https: "assets.adobedtm.com"
            }
        },
        dataElements: {
            addressId: {
                jsVariable: "BELONG.dataLayer.event.addressID",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            agentID: {
                customJS: function() {
                    var e = ""
                      , t = "";
                    return "undefined" != typeof jQuery && (agentidold = $("div#GlobalData").attr("data-login-email"),
                    "undefined" != typeof agentidold && agentidold && (t = agentidold.indexOf("@"),
                    e = agentidold.substring(t + 1, 45))),
                    e
                },
                storeLength: "pageview"
            },
            appSource: {
                queryParam: "source",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0,
                ignoreCase: 1
            },
            auth0id: {
                customJS: function() {
                    var e = _satellite.readCookie("userDetails") || "{}"
                      , t = JSON.parse(decodeURIComponent(e));
                    if ("string" == typeof t.userId && 24 === t.userId.length)
                        return t.userId
                },
                storeLength: "pageview"
            },
            basePageName: {
                customJS: function() {
                    var e = "belong";
                    return "undefined" != typeof s && "undefined" != typeof s.basePageName && s.basePageName ? e = s.basePageName : "undefined" != typeof s && "function" == typeof s.generateURLPageName && (e = "bl:" + s.generateURLPageName(n.location.pathname.split("/"))),
                    e
                },
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            broadbandTechType: {
                customJS: function() {
                    try {
                        var e = BELONG.dataLayer.broadbandTechType;
                        return _satellite.notify("Logged In page broadbandTechType: " + e),
                        e
                    } catch (t) {
                        return _satellite.notify("broadbandTechType dataElement error: " + e),
                        t
                    }
                },
                storeLength: "pageview"
            },
            cardAppointment: {
                customJS: function() {
                    try {
                        var e = BELONG.dataLayer.orderData.appointment
                          , t = "Card Status:" + e.cardStatus
                          , n = "Appointment Date:" + e.appointmentDate
                          , a = "Appointment Time:" + e.appointmentTime
                          , r = t + "|" + n + "|" + a;
                        return BELONG.dataLayer.cards = BELONG.dataLayer.cards || {},
                        BELONG.dataLayer.cards.appointment = {
                            cardValue: r,
                            cardStatus: t,
                            date: n,
                            time: a
                        },
                        _satellite.notify("Order Tracking - Appointment Card Data Found", 3),
                        r
                    } catch (i) {
                        return i
                    }
                },
                storeLength: "pageview"
            },
            cardCallback: {
                customJS: function() {
                    try {
                        var e = BELONG.dataLayer.orderData.callback
                          , t = "Card Status:" + e.cardStatus
                          , n = "Callback Request Status:" + e.callbackRequestStatus
                          , a = t + "|" + n;
                        return BELONG.dataLayer.cards = BELONG.dataLayer.cards || {},
                        BELONG.dataLayer.cards.callback = {
                            cardValue: a,
                            cardStatus: t,
                            callbackRequestStatus: n
                        },
                        _satellite.notify("Order Tracking - Callback Card Data Found", 3),
                        a
                    } catch (r) {
                        throw r
                    }
                },
                storeLength: "pageview"
            },
            cardConnect: {
                customJS: function() {
                    try {
                        var e = "Card Status:" + BELONG.dataLayer.orderData.connect.cardStatus;
                        return BELONG.dataLayer.cards = BELONG.dataLayer.cards || {},
                        BELONG.dataLayer.cards.connect = {
                            cardValue: e
                        },
                        _satellite.notify("Order Tracking - Connect Card Data Found", 3),
                        e
                    } catch (t) {
                        return t
                    }
                },
                storeLength: "pageview"
            },
            cardCoolingOff: {
                customJS: function() {
                    try {
                        var e = BELONG.dataLayer.orderData.coolingOff;
                        return "Card Status:" + e.cardStatus + "|Cooling Off Date:" + e.coolingOffDate
                    } catch (t) {
                        return t
                    }
                },
                storeLength: "pageview",
                cleanText: !0
            },
            cardErrorDetails: {
                customJS: function() {
                    try {
                        if ("object" != typeof t.JSON)
                            return "Browser doesn't support JSON";
                        var e = "Error Type:" + BELONG.dataLayer.orderData.errorType + "|" + JSON.stringify(BELONG.dataLayer.orderData.errors);
                        return _satellite.notify("Order Tracking - Error Card Data Found", 3),
                        e
                    } catch (n) {
                        return n
                    }
                },
                storeLength: "pageview"
            },
            cardId: {
                jsVariable: "BELONG.dataLayer.event.cardId",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            cardMessages: {
                customJS: function() {
                    try {
                        return "object" != typeof t.JSON ? "Browser doesn't support JSON" : JSON.stringify(BELONG.dataLayer.orderData.message)
                    } catch (e) {
                        return e
                    }
                },
                storeLength: "pageview"
            },
            cardModemDelivery: {
                customJS: function() {
                    try {
                        var e = BELONG.dataLayer.orderData.modemDelivery
                          , t = "Card Status:" + e.cardStatus
                          , n = "Modem Tracking Id:" + e.modemTrackingId
                          , a = t + "|" + n;
                        return BELONG.dataLayer.cards = BELONG.dataLayer.cards || {},
                        BELONG.dataLayer.cards.callback = {
                            cardValue: a,
                            cardStatus: t,
                            modemTrackingId: n
                        },
                        _satellite.notify("Order Tracking - Modem Delivery Card Data Found", 3),
                        a
                    } catch (r) {
                        return r
                    }
                },
                storeLength: "pageview"
            },
            cardMoreInformation: {
                customJS: function() {
                    try {
                        var e = "Card Status:" + BELONG.dataLayer.orderData.connect.moreInformation;
                        return BELONG.dataLayer.cards = BELONG.dataLayer.cards || {},
                        BELONG.dataLayer.cards.moreInformation = {
                            cardValue: e
                        },
                        _satellite.notify("Order Tracking - More Information Card Data Found", 3),
                        e
                    } catch (t) {
                        return t
                    }
                },
                storeLength: "pageview"
            },
            cardOrder: {
                customJS: function() {
                    try {
                        var e = BELONG.dataLayer.orderData.order
                          , t = "Card Status:" + e.cardStatus
                          , n = "Future Order Date:" + e.futureOrderDate
                          , a = "Service Added Date:" + e.serviceAddedDate
                          , r = t + "|" + n + "|" + a;
                        return BELONG.dataLayer.cards = BELONG.dataLayer.cards || {},
                        BELONG.dataLayer.cards.appointment = {
                            cardValue: r,
                            cardStatus: t,
                            futureDate: n,
                            serviceDate: a
                        },
                        _satellite.notify("Order Tracking - Order Card Data Found", 3),
                        r
                    } catch (i) {
                        return i
                    }
                },
                storeLength: "pageview"
            },
            cardProofOfOccupancyDocuments: {
                customJS: function() {
                    try {
                        var e = BELONG.dataLayer.orderData.orderTrackingPodStatus
                          , t = ["Card Status:" + e.cardStatus, "POD Complete Date:" + e.podCompleteDate, "POD Status:" + e.podStatus].join("|");
                        return BELONG.dataLayer.cards = BELONG.dataLayer.cards || {},
                        BELONG.dataLayer.cards.orderTrackingPodStatus = {
                            cardValue: t
                        },
                        _satellite.notify("Order Tracking - Connect Card Data Found", 3),
                        t
                    } catch (n) {
                        return n
                    }
                },
                storeLength: "pageview"
            },
            cardService: {
                customJS: function() {
                    try {
                        var e = BELONG.dataLayer.orderData.service
                          , t = "Card Status:" + e.cardStatus
                          , n = "Service Activation Date:" + e.serviceActivationDate
                          , a = "Service Activation Status:" + e.serviceActivationStatus
                          , r = t + "|" + n + "|" + a;
                        return BELONG.dataLayer.cards = BELONG.dataLayer.cards || {},
                        BELONG.dataLayer.cards.appointment = {
                            cardValue: r,
                            cardStatus: t,
                            activationDate: n,
                            activationStatus: a
                        },
                        _satellite.notify("Order Tracking - Service Card Data Found", 3),
                        r
                    } catch (i) {
                        return i
                    }
                },
                storeLength: "pageview"
            },
            "Contact Us Enquiry Type": {
                customJS: function() {
                    var e = "";
                    try {
                        e = $("#ticket_custom1").val()
                    } catch (t) {}
                    return e
                },
                storeLength: "pageview"
            },
            customerId: {
                customJS: function() {
                    var e = "";
                    return "undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && "undefined" != typeof BELONG.dataLayer.customerID && BELONG.dataLayer.customerID ? e = BELONG.dataLayer.customerID : "undefined" != typeof jQuery && (custidold = $("div#GlobalData").attr("data-utilibillid"),
                    "undefined" != typeof custidold && custidold && (e = custidold)),
                    e
                },
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            customerIdJoin: {
                customJS: function() {
                    var e = "";
                    return "undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && "undefined" != typeof BELONG.dataLayer.event && "undefined" != typeof BELONG.dataLayer.event.userid && BELONG.dataLayer.event.userid ? e = BELONG.dataLayer.event.userid : "undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && "undefined" != typeof BELONG.dataLayer.customerID && BELONG.dataLayer.customerID ? e = BELONG.dataLayer.customerID : "undefined" != typeof jQuery && (custidold = $("div#GlobalData").attr("data-utilibillid"),
                    "undefined" != typeof custidold && custidold && (e = custidold)),
                    e
                },
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            customerType: {
                customJS: function() {
                    var e = "";
                    try {
                        return void 0 === _satellite.getVar("isMobo") || "true" !== _satellite.getVar("isMobo") && !0 !== _satellite.getVar("isMobo") ? void 0 === _satellite.getVar("isAgent") || "true" !== _satellite.getVar("isAgent") && !0 !== _satellite.getVar("isAgent") ? void 0 === _satellite.getVar("isCustomer") || "true" !== _satellite.getVar("isCustomer") && !0 !== _satellite.getVar("isCustomer") || (e = "Customer") : e = "Agent" : e = "Mobo",
                        e
                    } catch (t) {
                        return e
                    }
                },
                storeLength: "pageview",
                cleanText: !0
            },
            deviceType: {
                customJS: function() {
                    var e = "";
                    try {
                        var t = _satellite.getVar("appSource");
                        t && (e = t + " : ")
                    } catch (n) {}
                    return e += _satellite.browserInfo.deviceType
                },
                storeLength: "pageview"
            },
            dtmTriggerData: {
                customJS: function() {
                    try {
                        BELONG.dataLayer.event;
                        return JSON.stringify(BELONG.dataLayer.event).replace(/\t|\n|\\t|\\n/g, "")
                    } catch (e) {
                        return ""
                    }
                },
                storeLength: "pageview",
                cleanText: !0
            },
            eoiReason: {
                jsVariable: "BELONG.dataLayer.event.eoiReason",
                storeLength: "pageview"
            },
            errorCode: {
                jsVariable: "BELONG.dataLayer.event.errorCode",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            eventAction: {
                jsVariable: "BELONG.dataLayer.event.eventAction",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            eventCategory: {
                jsVariable: "BELONG.dataLayer.event.eventCategory",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            eventLabel: {
                customJS: function() {
                    try {
                        var e = "";
                        return "undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && "undefined" != typeof BELONG.dataLayer.event && BELONG.dataLayer.event.eventLabel ? e = BELONG.dataLayer.event.eventLabel : "undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && "undefined" != typeof BELONG.dataLayer.event && BELONG.dataLayer.event.eventType && (e = BELONG.dataLayer.event.eventType),
                        _satellite.notify("eventLabel Data Element returns: " + e, 1),
                        e
                    } catch (t) {
                        return s.eVar91 = "eventLabel DE:" + _satellite.getVar("tagError")(t),
                        ""
                    }
                },
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            eventPage: {
                jsVariable: "BELONG.dataLayer.event.eventPage",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            eventType: {
                jsVariable: "BELONG.dataLayer.event.eventType",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            gclid: {
                queryParam: "gclid",
                storeLength: "pageview",
                ignoreCase: 1
            },
            hostname: {
                jsVariable: "location.hostname",
                storeLength: "pageview"
            },
            hotjarDomains: {
                customJS: function() {
                    return !0 === {
                        "www.belong.com.au": !0,
                        "login.belong.com.au": !0,
                        "support.belong.com.au": !0,
                        "mobile.belongprod.com.au": !0,
                        "belong.mooruptech.com.au": !0
                    }[location.hostname] ? "TRUE" : "FALSE"
                },
                "default": "FALSE",
                storeLength: "pageview"
            },
            hotjarUserId: {
                customJS: function() {
                    var e = t.hj || {}
                      , n = e.includedInSample
                      , a = "function" == typeof ((e || {}).globals || {}).get ? ((e || {}).globals || {}).get("userId") : "";
                    if (n && a)
                        return a.substr(0, 8)
                },
                storeLength: "pageview"
            },
            internalSearchString: {
                queryParam: "search",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0,
                ignoreCase: 1
            },
            isAgent: {
                customJS: function() {
                    var e = "false";
                    return "undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && "undefined" != typeof BELONG.dataLayer.isAgent && !0 === BELONG.dataLayer.isAgent && (e = "true"),
                    e
                },
                "default": "false",
                storeLength: "pageview"
            },
            isBelongApp: {
                customJS: function() {
                    try {
                        if ("undefined" == typeof s)
                            return;
                        var e = "FALSE";
                        if ("undefined" === _satellite.readCookie("uiIdiom")) {
                            var n = t.navigator.standalone
                              , a = navigator.userAgent.match(/iPhone|iPad|iPod/i)
                              , r = navigator.userAgent.match(/Android/i)
                              , i = /safari/.test(navigator.userAgent.toLowerCase())
                              , o = navigator.userAgent.match(/BelongApp\/[0-9\.]+$/);
                            return a ? n || i || (e = "TRUE") : r && o && (e = "TRUE"),
                            e
                        }
                        "" !== _satellite.readCookie("uiIdiom") && (e = "TRUE",
                        console.log("isApp tracked from cookie"))
                    } catch (c) {
                        console.log("uiIdiom app cookie read error")
                    }
                },
                "default": "FALSE",
                storeLength: "pageview"
            },
            isCustomer: {
                customJS: function() {
                    var e = "false";
                    return "undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && "undefined" != typeof BELONG.dataLayer.isCustomer && !0 === BELONG.dataLayer.isCustomer ? e = "true" : "undefined" != typeof jQuery && (isCustomerOld = $("div#GlobalData").attr("data-is-customer"),
                    "undefined" != typeof isCustomerOld && isCustomerOld && (e = isCustomerOld)),
                    e
                },
                "default": "false",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            isLoggedIn: {
                customJS: function() {
                    var e = "false";
                    return "undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && "undefined" != typeof BELONG.dataLayer.isLoggedIn && !0 === BELONG.dataLayer.isLoggedIn ? e = "true" : "undefined" != typeof jQuery && (isLoggedInOld = $("div#GlobalData").attr("data-is-logged-in"),
                    "undefined" != typeof isLoggedInOld && isLoggedInOld && (e = isLoggedInOld)),
                    e
                },
                "default": "false",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            isMobo: {
                customJS: function() {
                    var e = "false";
                    return "undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && "undefined" != typeof BELONG.dataLayer.isMobo && !0 === BELONG.dataLayer.isMobo ? e = "true" : "undefined" != typeof jQuery && (isMoboOld = $("div#GlobalData").attr("data-is-mobo"),
                    "undefined" != typeof isMoboOld && isMoboOld && (e = isMoboOld)),
                    e
                },
                "default": "false",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            liveAgentSessionId: {
                customJS: function() {
                    var e = function(e) {
                        var t = (n.cookie.match("(^|; )" + e + "=([^;]*)") || 0)[2];
                        return t ? decodeURIComponent(t) : ""
                    }("liveagent_sid") || "";
                    return _satellite.notify("Live Agent Session Id is " + e, 3),
                    e
                },
                storeLength: "pageview"
            },
            liveChatImage: {
                jsVariable: "BELONG.dataLayer.liveChatImage",
                "default": "unknown image",
                storeLength: "pageview",
                forceLowerCase: !0
            },
            loginPasswordErrorDisplayed: {
                customJS: function() {
                    if ("complete" === n.readyState && n.getElementById("notify-user"))
                        return "flex" === t.getComputedStyle(n.getElementById("notify-user"), null).getPropertyValue("display") ? "true" : "false"
                },
                "default": "false",
                storeLength: "pageview"
            },
            marketingCloudVisitorId: {
                customJS: function() {
                    var e = _satellite.getVisitorId().getMarketingCloudVisitorID();
                    if (e)
                        return e
                },
                "default": "D=mid",
                storeLength: "pageview"
            },
            metaDescription: {
                customJS: function() {
                    if (n.querySelector("meta[name=description]")) {
                        var e = n.querySelector("meta[name=description]").getAttribute("content");
                        return "string" == typeof e && "function" == typeof e.trim ? e.substring(0, 254).trim() : ""
                    }
                    return ""
                },
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            metaKeywords: {
                customJS: function() {
                    if (n.querySelector("meta[name=keywords]")) {
                        var e = n.querySelector("meta[name=keywords]").getAttribute("content");
                        return "string" == typeof e && "function" == typeof e.trim ? e.substring(0, 254).trim() : ""
                    }
                    return ""
                },
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            moorupDomains: {
                customJS: function() {
                    return !0 === {
                        "belong.mooruptech.com.au": !0,
                        "belongsalesplatform-env.cwwyphuvmz.ap-southeast-2.elasticbeanstalk.com": !0
                    }[location.hostname] ? "TRUE" : "FALSE"
                },
                "default": "FALSE",
                storeLength: "pageview"
            },
            moorupPageName: {
                customJS: function() {
                    return ("bl:moorup" + location.pathname.replace(/\//gi, ":")).replace(/\:$/, "")
                },
                "default": "bl:moorup",
                storeLength: "pageview"
            },
            nextCardId: {
                jsVariable: "BELONG.dataLayer.event.nextCardId",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            omd_tracking_code: {
                queryParam: "tc",
                storeLength: "pageview",
                ignoreCase: 1
            },
            orderTrackingSaleType: {
                jsVariable: "BELONG.dataLayer.orderData.saleType",
                storeLength: "pageview"
            },
            pageName: {
                jsVariable: "s.pageName",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            pathname: {
                jsVariable: "window.location.pathname",
                storeLength: "pageview"
            },
            plan: {
                jsVariable: "BELONG.dataLayer.event.plan",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            planType: {
                customJS: function() {
                    var e, a = n.querySelector(".join-product-summary__planType");
                    return "object" == typeof s && (e = s.get("BELONG.dataLayer.event.planType", t)),
                    void 0 === e && null !== a && (e = a ? a.innerText : ""),
                    e
                },
                storeLength: "pageview"
            },
            postCode: {
                customJS: function() {
                    try {
                        if ("undefined" != typeof Storage && /"servicePostcode":"(.+?)"/.test(sessionStorage.sqModel))
                            return sessionStorage.sqModel.match(/"servicePostcode":"(.+?)"/)[1]
                    } catch (e) {
                        _satellite.notify("Postcode Error: " + e, 5)
                    }
                },
                storeLength: "pageview"
            },
            promoCode: {
                jsVariable: "BELONG.dataLayer.event.promoCode",
                storeLength: "session",
                forceLowerCase: !0,
                cleanText: !0
            },
            promoCodeProductBuilder: {
                jsVariable: "BELONG.dataLayer.event.promocodeValidation.promotion.description",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            resetPasswordErrorDisplayed: {
                customJS: function() {
                    if ("complete" === n.readyState && n.getElementById("Email-error")) {
                        var e = n.getElementById("Email-error").innerText;
                        return _satellite.notify("DE_resetPassswordErrorDisplayed: " + e, 1),
                        e
                    }
                },
                "default": "false",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            rootHostname: {
                customJS: function() {
                    return /\.com$/.test(location.hostname) ? location.hostname.split(".").reverse().splice(0, 2).reverse().join(".") : location.hostname.split(".").reverse().splice(0, 3).reverse().join(".")
                },
                storeLength: "pageview"
            },
            salesforce_id: {
                customJS: function() {
                    var e = _satellite.readCookie("userDetails") || "{}"
                      , t = JSON.parse(decodeURIComponent(e));
                    if ("string" == typeof t.userId && t.userId.length < 24)
                        return t.userId
                },
                storeLength: "pageview"
            },
            serviceClass: {
                jsVariable: "BELONG.dataLayer.event.serviceClass",
                storeLength: "pageview"
            },
            serviceType: {
                customJS: function() {
                    var e, n, r = ((t.BELONG || {}).dataLayer || {}).serviceType, i = ((t.BELONG || {}).dataLayer || {}).orderData || {}, o = ((t.BELONG || {}).dataLayer || {}).productData || {}, s = t.sessionStorage ? t.sessionStorage.getItem("product") : a, c = _satellite.readCookie("s_v71");
                    return r ? (e = r,
                    n = "serviceType") : i.serviceType ? (e = i.serviceType,
                    n = "orderData.serviceType") : o.serviceType ? (e = i.serviceType,
                    n = "orderData.serviceType") : s ? (e = s,
                    n = "sessionStorage") : c && (e = c,
                    n = "serviceTypeCookie"),
                    e ? (_satellite.setCookie("s_v71", e, 45),
                    _satellite.notify("serviceType " + e + " found in " + n + " and written to s_v71 cookie", 1)) : !e && _satellite.settings.notifications && console.log("serviceType not found", {
                        serviceType: r,
                        orderData: i,
                        productData: o,
                        serviceTypeCookie: c,
                        sessionStorageProduct: s
                    }),
                    e
                },
                storeLength: "pageview"
            },
            sqAttempt: {
                jsVariable: "BELONG.dataLayer.event.sqAttempt",
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            sqProduct: {
                customJS: function() {
                    var e = "";
                    try {
                        sqProductTypeCheck = BELONG.dataLayer.event.product,
                        sqProductTypeCheck && "adsl" != sqProductTypeCheck.toLowerCase() && "nbn" != sqProductTypeCheck.toLowerCase() && (e = "[" + sqProductTypeCheck + "]")
                    } catch (t) {}
                    return e
                },
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            sqTechType: {
                customJS: function() {
                    try {
                        var e = "";
                        return "undefined" != typeof BELONG && "undefined" != typeof BELONG.dataLayer && "undefined" != typeof BELONG.dataLayer.event && BELONG.dataLayer.event.sqTechType && (e = BELONG.dataLayer.event.sqTechType),
                        e
                    } catch (t) {
                        return ""
                    }
                },
                storeLength: "pageview"
            },
            state: {
                customJS: function() {
                    try {
                        if ("undefined" != typeof Storage && /"serviceState":"(.+?)"/.test(sessionStorage.sqModel))
                            return sessionStorage.sqModel.match(/"serviceState":"(.+?)"/)[1]
                    } catch (e) {
                        _satellite.notify("State Error: " + e, 5)
                    }
                },
                storeLength: "pageview"
            },
            subDivision: {
                "default": "broadband",
                storeLength: "pageview"
            },
            tagError: {
                customJS: function() {
                    return function(e) {
                        return e.stack ? e.stack.replace(/\n|\t|\s/g, " ") : e.name && e.message ? "[" + e.name + "] " + e.message.toString() : e
                    }
                },
                storeLength: "pageview"
            },
            totalMinCost: {
                jsVariable: "BELONG.dataLayer.totalMinCost",
                storeLength: "pageview"
            },
            troubleshootingContactType: {
                customJS: function() {
                    var e = "";
                    try {
                        e = BELONG.dataLayer.event.contactType
                    } catch (n) {}
                    try {
                        var t = BELONG.dataLayer.event.issue;
                        void 0 !== t && t && (e = e + " : " + t)
                    } catch (n) {}
                    return e && (e = "troubleshooting : " + e),
                    e
                },
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            },
            url: {
                customJS: function() {
                    return /email|username|password/i.test(location.search) ? decodeURIComponent(location.href).replace(/(email|username|password)=[^&]*(&?)/g, "$1=*******$2") : location.href
                },
                storeLength: "pageview"
            },
            utm_campaign: {
                customJS: function() {
                    var e = "utm_campaign";
                    if (_satellite.getQueryParam(e))
                        return _satellite.getQueryParam(e);
                    if (_satellite.getQueryParam("goto")) {
                        var t = decodeURIComponent(_satellite.getQueryParam("goto"));
                        if (n = _satellite.parseQueryParams(t)[e])
                            return n
                    } else {
                        if (!location.hash)
                            return !1;
                        var n, a = decodeURIComponent(location.hash);
                        if (n = _satellite.parseQueryParams(a)[e])
                            return n
                    }
                },
                storeLength: "pageview"
            },
            utm_content: {
                customJS: function() {
                    var e = "utm_content";
                    if (_satellite.getQueryParam(e))
                        return _satellite.getQueryParam(e);
                    if (_satellite.getQueryParam("goto")) {
                        var t = decodeURIComponent(_satellite.getQueryParam("goto"));
                        if (n = _satellite.parseQueryParams(t)[e])
                            return n
                    } else {
                        if (!location.hash)
                            return !1;
                        var n, a = decodeURIComponent(location.hash);
                        if (n = _satellite.parseQueryParams(a)[e])
                            return n
                    }
                },
                storeLength: "pageview"
            },
            utm_medium: {
                customJS: function() {
                    var e = "utm_medium";
                    if (_satellite.getQueryParam(e))
                        return _satellite.getQueryParam(e);
                    if (_satellite.getQueryParam("goto")) {
                        var t = decodeURIComponent(_satellite.getQueryParam("goto"));
                        if (n = _satellite.parseQueryParams(t)[e])
                            return n
                    } else {
                        if (!location.hash)
                            return !1;
                        var n, a = decodeURIComponent(location.hash);
                        if (n = _satellite.parseQueryParams(a)[e])
                            return n
                    }
                },
                storeLength: "pageview"
            },
            utm_source: {
                customJS: function() {
                    var e = "utm_source";
                    if (_satellite.getQueryParam(e))
                        return _satellite.getQueryParam(e);
                    if (_satellite.getQueryParam("goto")) {
                        var t = decodeURIComponent(_satellite.getQueryParam("goto"));
                        if (n = _satellite.parseQueryParams(t)[e])
                            return n
                    } else {
                        if (!location.hash)
                            return !1;
                        var n, a = decodeURIComponent(location.hash);
                        if (n = _satellite.parseQueryParams(a)[e])
                            return n
                    }
                },
                storeLength: "pageview"
            },
            utm_term: {
                customJS: function() {
                    var e = "utm_term";
                    if (_satellite.getQueryParam(e))
                        return _satellite.getQueryParam(e);
                    if (_satellite.getQueryParam("goto")) {
                        var t = decodeURIComponent(_satellite.getQueryParam("goto"));
                        if (n = _satellite.parseQueryParams(t)[e])
                            return n
                    } else {
                        if (!location.hash)
                            return !1;
                        var n, a = decodeURIComponent(location.hash);
                        if (n = _satellite.parseQueryParams(a)[e])
                            return n
                    }
                },
                storeLength: "pageview"
            },
            utm_user_id: {
                queryParam: "utm_userid",
                storeLength: "pageview",
                ignoreCase: 1
            },
            virtualPage: {
                customJS: function() {
                    try {
                        var e = BELONG.dataLayer.event.virtualPage;
                        return e = (e = (e = (e = e.replace(/\//g, ":")).replace(/:virtual:/g, "")).replace(/\.html/g, "")).replace(/\.htm/g, "")
                    } catch (t) {
                        return ""
                    }
                },
                storeLength: "pageview",
                forceLowerCase: !0,
                cleanText: !0
            }
        },
        appVersion: "7QN",
        buildDate: "2020-04-15 06:36:35 UTC",
        publishDate: "2020-04-15 06:36:34 UTC"
    })
}(window, document);
