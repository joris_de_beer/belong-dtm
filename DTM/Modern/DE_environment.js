var production = /^www.belong.com.au$/i,
	staging = /^(preprod|staging).belong.com.au$/i,
	testing = /^test/i,
	development = /^localhost$/i;

if(production.test(location.hostname)){ return "production" }
else if(staging.test(location.hostname)){ return "staging" }
else if(testing.test(location.hostname)){ return "testing" }
else if(development.test(location.hostname)){ return "development" }
else{ return location.hostname.split('.').shift() }
