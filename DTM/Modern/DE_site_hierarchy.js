/* Returns a pipe (|) delimited value of the folder path */
returnVal = location.pathname.replace(/(^\/|\/$)/g,"").replace(/\//g,"|");

_satellite.notify("Hier = "+returnVal+ " ["+_satellite.getVar('last_triggered_rule')+"]",3);

return returnVal;
