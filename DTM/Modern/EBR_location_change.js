if(typeof s==="object" && typeof s.extend==="function"){
	s.extend(window.digitalData, {
		page: {
			pageInfo: {
				get activeNavItem(){
					var activeNavEl = document.querySelector('nav.primary-navigation .primary-navigation__item--active .primary-navigation__item__link__content');
					return activeNavEl?activeNavEl.innerText : "";
				},
				pageName: _satellite.getVar('page_name'),
				pageTitle: document.title,
				referringURL: _satellite.getVar('referrer'),
				sysEnv : _satellite.getVar('environment'),
				breadCrumbs : location.pathname.split('/').slice(1,100),
				publisher: _satellite.getVar('site_name')
			}
		}
	});
}
if(typeof adobe==="object"){
	function targetPageParams() {
		return {
		  "at_property": "fe64daa5-48cc-df93-e158-14c2bc1799bd"
		};
	}
	function targetPageParamsAll() {
		return {
		  "at_property": "fe64daa5-48cc-df93-e158-14c2bc1799bd"
		};
	}
	adobe.target.getOffer({
		mbox: 'belong-react-location-change',
		at_property: "fe64daa5-48cc-df93-e158-14c2bc1799bd",
		prarams: {
			at_property: "fe64daa5-48cc-df93-e158-14c2bc1799bd",
			property: {token:"fe64daa5-48cc-df93-e158-14c2bc1799bd"}
		},
		success: function(offer) {
			if(_satellite.settings.notifications){
				console.log('adobe.target.getOffer(belong-react-location-change).success()', offer);
			}
			adobe.target.applyOffer( {
				mbox: "belong-react-location-change",
				offer: offer
			})
		},
		'error': function(status, error) {
			if(_satellite.settings.notifications){
				console.log('adobe.target.getOffer(belong-react-location-change).error()', status, error);
			}
		}
	});
};
/* Only run on the Belong Mobiles product page */
if(/^\/mobile(|\/|\/plans)$/.test(window.location.pathname)){
	document.body.dispatchEvent(new Event('mobiles/products/UPDATE_PRODUCTS'))
}
return true;