var clickData = {
	event:		event,
	target:		this,
	label:		s.get('detail.eventData.data.label', event),
	state:		s.get('detail.eventData.data.state', event)
};

/* Event is passing the current state of the accordion,
   not the state it will become after click.
   So manually swap the values around.
*/
if(clickData.state==="open"){clickData.state="closed"}
else if(clickData.state==="closed"){clickData.state="opened"}

_satellite.setVar('event_category',"accordion");
_satellite.setVar('event_action',  "click");
_satellite.setVar('event_label', clickData.label +" "+ clickData.state);
digitalData.clickData = clickData;
return true;
