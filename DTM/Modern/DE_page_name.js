/* Returns a page name based on the folder path*/
var division = _satellite.getVar('division').toLowerCase(),
	folderPath = location.pathname.replace(/(^\/|\/$)/g,"").replace(/\//g,":");

return division + ":"+ folderPath;

/* Version 1.
var division = _satellite.getVar('division'),
	siteSection = _satellite.getVar('site_section'),
	fileName = location.pathname.split('/').pop(),
	formName = _satellite.getVar('form_name')
	returnVal = "";

if(siteSection && formName) {
 	returnVal =  [division, siteSection, formName].join(':');
}else if(siteSection !== fileName && fileName !== "") {
	returnVal = [division, siteSection, fileName].join(':');
}else{
	returnVal = [division, siteSection].join(':');
}
return returnVal;
*/
