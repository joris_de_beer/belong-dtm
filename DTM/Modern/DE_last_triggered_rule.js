/* DE - Last Triggered Rule */
var logData = {
	messages : _satellite.Logger.messages,
	messageCount : _satellite.Logger.messages.length,
	lastTriggeredRule : "unknown"
};
try{
	for(i=logData.messageCount-1; i >= 0; i--){
		var messageText = logData.messages[i][1]||"skip";
		if(/^(Direct call Rule |Rule )".*" fired\.$/.test(messageText)){
			logData.lastTriggeredRule=messageText;
			break;
    	}
	}
}catch(err){
	logData.jsError = err.message;
	_satellite.notify(_satellite.stringify(logData.jsError),4);
}
// This looks a bit cryptic, but its to deal with time issues as the pages load.
window.digitalData=window.digitalData||{};
window.digitalData.logData=logData
return logData.lastTriggeredRule;
