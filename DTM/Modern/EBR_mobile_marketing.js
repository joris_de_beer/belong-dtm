/* Navigation within the Belong Mobiles pages can happen via normal page loads,
   or via PushState/Hashstate changes.
   This rule handles situations where navigation is via pushstate/hashchange.
*/

window.dataLayer= window.dataLayer || [];
window.gtag		= function(){dataLayer.push(arguments)};
var domainValidForMarketingTags = _satellite.getVar('is_marketing_script_safe');

/* DCM: 7488159|Belong_Brand_Audiences_All Pages|Audiences */
if(domainValidForMarketingTags){
	gtag('event', 'conversion', {
		'allow_custom_scripts': true,
		'u5': _satellite.getVar('url'),
		'send_to': 'DC-8705912/audie0/belon0+standard'
	});
}

/* DCM: 7509452|Belong_Mobile_Landing Page_Mobile|Landing Page */
if(domainValidForMarketingTags && /^\/mobile(\/|$)/.test(location.pathname)){
	gtag('event', 'conversion', {
		'allow_custom_scripts': true,
		'send_to': 'DC-8705912/landi0/belon0+unique'
	});
}

/* DCM: 7522714|Belong_Mobile_Step 1 _Connect_Your Account|Steps */
if(domainValidForMarketingTags && /^\/mobile\/connect(\/|$)/.test(location.pathname)){
	gtag('event', 'conversion', {
		'allow_custom_scripts': true,
		'u5': _satellite.getVar('url'),
		'send_to': 'DC-8705912/steps0/belon001+unique'
	});
}

/* DCM: ??|Belong_Handsets_Landing Page_Homepage|Smartphones */
if(domainValidForMarketingTags && /^\/mobile\/smartphones(\/|$)/.test(location.pathname)){
	gtag('event', 'conversion', {
		'allow_custom_scripts': true,
		'send_to': 'DC-8705912/landi0/belon001+unique'
	});
}

// Third Party Marketing Conversion Points
if(domainValidForMarketingTags && /^\/mobile\/(get-sim\/success|connect\/activating)/.test(location.pathname)){
	_satellite.track('conversion_pixel')
}

