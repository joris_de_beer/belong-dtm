var clickData = {
	event:		event,
	target:		this,
	action:		s.get('detail.eventData.data.action', event),
	promoCode:	s.get('detail.eventData.data.promoCode', event)||"",
	message:	s.get('detail.eventData.data.message', event)||"",
};

_satellite.setVar('event_category',"promo widget");
_satellite.setVar('event_action',  "click");
_satellite.setVar('event_label', clickData.action );
if(clickData.message){
	_satellite.setVar('event_message', "promo error:"+clickData.message); // For errors
}else{
	_satellite.setVar('event_message', ""); //No error
}
_satellite.setVar('promo_code', clickData.promoCode);

if(_satellite.settings.notifications){
	console.log('promo/WIDGET',clickData);
}
if(clickData.action) {
	return true;
}
