// Returns the a combination of mobile and broadband id for the user from one of two locations.
if(typeof window.s === "object" && typeof window.s.get === "function"){
	var viaWithAuthObj = s.get('trackEvent.detail.eventStore.withAuth.signedInUser.combinedId',window.digitalData);
	// Prefer viaWithAuthObj, but sometimes it's empty so fallback to concatenating Id from viaAsynAccount;
	if(viaWithAuthObj !== null && viaWithAuthObj !== "null|null"){
		return viaWithAuthObj
	}else{ // Look for IDs in asyncAccount.
		var broadbandId = s.get('trackEvent.store.account.asyncAccount.data.broadbandId',window.digitalData),
			mobileId = s.get('trackEvent.store.account.asyncAccount.data.mobileId',window.digitalData);
		return 	broadbandId + "|" + mobileId;
	}
}
