var digitalData =window.digitalData||{},
	logData = {
		messages :_satellite.Logger.messages,
		messageCount :_satellite.Logger.messages.length
	};
try{
	logData.lastMessage =logData.messages[logData.messageCount-1];
	logData.lastMessageTxt =logData.lastMessage[1] || _satellite.stringify(logData.lastMessage);
}catch(err){
	logData.jsError = err.message;
	logData.lastMessageTxt =_satellite.stringify(logData.jsError);
	_satellite.notify(logData.lastMessageTxt,4);
}
digitalData.logData=logData;
return logData.lastMessageTxt;
