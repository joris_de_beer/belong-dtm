var pathArray = location.pathname.split('/'),
    returnVal = "";
    welcomePageTechType = _satellite.getQueryParamCaseInsensitive('setup')
if(pathArray.length == 2){ // Top level homepage
	returnVal = "";
}else if(pathArray.length >= 3){
  returnVal = _satellite.getVar('site_section') + ":" + pathArray[2];
}else if(pathArray.length >= 4){
  returnVal = _satellite.getVar('site_section') + ":" + pathArray[2] + ":" + pathArray[3];
}

// Special case for /welcome page
if(location.pathname==="/welcome" && welcomePageTechType){
	returnVal = _satellite.getVar('site_section') + ":" + welcomePageTechType;
}
return returnVal;