/* Only run on the Belong Mobiles product page */
if(! /^\/mobile(|\/|\/plans)$/.test(window.location.pathname)){
	return false;
}
/* This code tries to track the default plan selected on the mobiles page */
var eventData = {
	el : this,
	event : event,
	radioLabel : $('.plan-builder .input-option__item--selected label .p--large').text() || "large 30gb",
	radioName : "planSelect",
	formLabel : _satellite.getVar('form_id')||_satellite.getVar('pathname').split('/').pop()
};

_satellite.setVar('form_label',eventData.formLabel);
// Capture some additional data on the plan select page.
_satellite.setVar('input_label', (eventData.radioName+" "+ eventData.radioLabel).toLowerCase());

digitalData.eventData = eventData;
return true;