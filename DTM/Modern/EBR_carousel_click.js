var clickData = {
	event:		event,
	target:		this,
	label:		s.get('detail.eventData.data.label', event),
	direction:	s.get('detail.eventData.data.direction', event),
	openAccordion: $('.accordion-section--open h4').text()
};
_satellite.setVar('event_category',"carousel");
_satellite.setVar('event_action',  "click");
_satellite.setVar('event_label', clickData.direction+" button from "+clickData.label+" in "+clickData.openAccordion);
digitalData.clickData = clickData;
return true;
