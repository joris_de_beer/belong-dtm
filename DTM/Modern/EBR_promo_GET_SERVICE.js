var clickData = {
	event:		event,
	target:		this,
	serviceType:s.get('detail.eventData.data.serviceType', event).toLowerCase(),
	promoCode:	s.get('detail.eventData.data.promoCode', event),
};
_satellite.setVar('event_category',"promo");
_satellite.setVar('event_action',  "click");
_satellite.setVar('event_label', "get "+clickData.serviceType);
_satellite.setVar('promo_code', clickData.promoCode);

if(_satellite.settings.notifications){
	console.log('promo/GET_SERVICE',clickData);
}
return true;
