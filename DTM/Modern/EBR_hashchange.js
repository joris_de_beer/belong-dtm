if(typeof adobe==="object"){
	function targetPageParams() {
		return {
		  "at_property": "fe64daa5-48cc-df93-e158-14c2bc1799bd"
		};
	}
	function targetPageParamsAll() {
		return {
		  "at_property": "fe64daa5-48cc-df93-e158-14c2bc1799bd"
		};
	}
	_satellite.notify(
		'adobe.target.triggerView('+_satellite.getVar('page_name')+')'
	,1); 
	adobe.target.triggerView(
		_satellite.getVar('page_name'),
		{page: true}
	)
	adobe.target.getOffer({
		mbox: 'TelstraGlobalMBox',
		at_property: "fe64daa5-48cc-df93-e158-14c2bc1799bd",
		prarams: {
			at_property: "fe64daa5-48cc-df93-e158-14c2bc1799bd",
			property: {token:"fe64daa5-48cc-df93-e158-14c2bc1799bd"}
		},
		success: function(offer) {
			if(_satellite.settings.notifications){
				console.log('adobe.target.getOffer(TelstraGlobalMBox).success()', offer);
			}
			adobe.target.applyOffer( {
				mbox: "TelstraGlobalMBox",
				offer: offer
			})
		},
		'error': function(status, error) {
			if(_satellite.settings.notifications){
				console.log('adobe.target.getOffer(TelstraGlobalMBox).error()', status, error);
			}
		}
	});
};
return _satellite.getVar('is_marketing_script_safe');