// If the hotjar script isn't ready yet, create a queue for pending events.
window.hj=window.hj||function(){(hj.q=hj.q||[]).push(arguments)};

if(typeof s==="object"){
	digitalData.hjTags=[];
	digitalData.hjTags.push = function() {
	  Array.prototype.push.apply(this, arguments);
	}
	var get = function(path, data){
		var i, path = (typeof path === "string") ? path.split('.') : path;
		var len = path.length;
		for (i = 0; typeof data === 'object' && data !==null && i < len; ++i) {data = data[path[i]];}
		return data;
	};
	var containsEvent = function(event, eventsList){
		var eventsArray = eventsList.split(',');
		return eventsArray.some(function(eventsArrayElement){return eventsArrayElement === event});
	},
	tagHotjarRecording = function(variable, condition, tagLabel){
		var sendTag = false;
		if(typeof s.events === "string" && /^event/.test(variable) && containsEvent(event, s.events)){
			sendTag = true;
		}else if(/^(prop|eVar|pageName)/.test(variable)){
			if(s[variable] === condition){
				sendTag=true;
			}
		}else if(variable==="target"){
			var responseTokens = digitalData.responseTokens?[get('responseTokens',digitalData)]:[];
			if(responseTokens.length===0){
				return false; // Exit early if the response from target is empty.
			}else{
				for(var i = 0; i < responseTokens.length; i++){
					var actName, experience, shortName=[], tntLabel;
					activity = responseTokens[i]['activity.name']||"";
					experience = responseTokens[i]['experience.name']||"";
					tntLabel = (activity+" : "+experience).substring(0,49);
					if(!digitalData.hjTags[tntLabel]){
						digitalData.hjTags.push(tntLabel);
						_satellite.notify('['+tntLabel+'] sent to Hotjar');
						hj('tagRecording', [tntLabel]);
					}
				}
				
			}
		}
		if(sendTag===true && !BELONG.hjTags[tagLabel]){
			digitalData.hjTags.push(tagLabel);
			_satellite.notify('['+tagLabel+'] sent to Hotjar because '+variable+'='+condition,1);
			hj('tagRecording', [tagLabel]);
		}
	};

	// When we see the following events, push the tag label to hotjar.
	tagHotjarRecording('prop26', 'liveagent-chat:click:liveagent toggle chat modal opened', 'Global Nav Chat Button Clicked');
	tagHotjarRecording('target','','BT-nnn');
}
