try{
	var form = {
		event:		event,
		target:		this,
		formId:		_satellite.getVar('form_id'),
    	trackEvent: digitalData.trackEvent,
    	eventCategory: "support request",
    	eventAction: "respond", /* Default action is respond. Check for reopen later */
    	updateCaseDetailsEvent : s.getMostRecentEvent('UPDATE_CASE_DETAILS')
	};

	if(s.get('trackEvent.data.data.status', digitalData) === "RESOLVED"){
		form.eventAction = "reopen";
		/*	When a RESOLVED case is viewed, and UPDATE_CASE_DETAILS react event is
			triggered even before the user has done anything.
			So to stop this EBR from running, return false now
		*/
		digitalData.form = form;
		return false;
	}
	_satellite.setVar('event_category',form.eventCategory);
	_satellite.setVar('event_action',  form.eventAction);
	_satellite.setVar('event_label',   s.get('trackEvent.data.data.title', digitalData));
	digitalData.form = form;
	return true;
}catch(error){
	console.warn('----',error);
}
