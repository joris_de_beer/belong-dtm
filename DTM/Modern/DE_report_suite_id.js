var developmentReportSuiteId = "belongdev",
		productionReportSuiteId  = "belongprd",
    globalProdReportSuiteId  = "telstraglobalprd";

if( // if we're not on the prod domain, or dtm is in staging mode, then set report suite to dev
  /^www.belong.com.au$/.test(window.location.hostname) === false
  ||
  _satellite.settings.isStaging === true
  ){
	return developmentReportSuiteId;
}else{
  return productionReportSuiteId + "," + globalProdReportSuiteId;
}
