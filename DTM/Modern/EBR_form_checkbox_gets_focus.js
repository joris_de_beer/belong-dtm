var clickData = {
	el : this,
	event : event,
	checkboxLabel : $(this).parents('.input').find('label').text() || "",
	checkboxName : this.getAttribute('name'),
	formLabel : _satellite.getVar('form_id')||_satellite.getVar('pathname').split('/').pop()
};

_satellite.setVar('form_label',clickData.formLabel);
// Prefer the input label field.
if(clickData.checkboxLabel){
	_satellite.setVar('input_label',clickData.checkboxLabel);
}else{ // Fallback to the name attribute on the text field.
	_satellite.setVar('input_label',clickData.checkboxName);
}
digitalData.clickData = clickData;
return true;
