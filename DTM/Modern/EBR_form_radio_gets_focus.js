var clickData = {
	el : this,
	event : event,
	radioLabel : $(this).next('label').find('.plan-builder__plan-title .p--large').text() || "",
	radioName : this.getAttribute('name'),
	formLabel : _satellite.getVar('form_id')||_satellite.getVar('pathname').split('/').pop()
};

_satellite.setVar('form_label',clickData.formLabel);
// Prefer the input label field.
if(clickData.radioLabel){
	_satellite.setVar('input_label',clickData.radioLabel);
}else{ // Fallback to the name attribute on the text field.
	_satellite.setVar('input_label',clickData.radioName);
}

// Capture some additional data on the plan select page.
if(clickData.formLabel === "mobile" && clickData.radioName == "planSelect") {
	_satellite.setVar('input_label', (clickData.radioName+" "+ clickData.radioLabel).toLowerCase());
}

digitalData.clickData = clickData;
return true;
