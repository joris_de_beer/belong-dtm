if(typeof s==="object" && typeof s.extend==="function"){
	s.extend(window.digitalData, {
		page: {
			pageInfo: {
				get activeNavItem(){
					var activeNavEl = document.querySelector('nav.primary-navigation .primary-navigation__item--active .primary-navigation__item__link__content');
					return activeNavEl?activeNavEl.innerText : "";
				},
				pageName: _satellite.getVar('page_name'),
				pageTitle: document.title,
				referringURL: _satellite.getVar('referrer'),
				sysEnv : _satellite.getVar('environment'),
				breadCrumbs : location.pathname.split('/').slice(1,100),
				publisher: _satellite.getVar('site_name')
			}
		}
	});
}
return true;
