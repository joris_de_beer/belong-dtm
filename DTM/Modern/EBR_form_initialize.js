try{
	var form = {
		event:		event,
		target:		this,
		formId:		_satellite.getVar('form_id'),
		formName:	_satellite.getVar('form_name'),
		formPrefix:	_satellite.getVar('form_prefix'),
		formSuffix:	_satellite.getVar('form_suffix'),
		formStepName:_satellite.getVar('form_step_name'),
		trackEvent: digitalData.trackEvent,
	};
	digitalData.form=form;
	return true;
}catch(error){
	console.warn('EBR_form_initialize error',error);
}
