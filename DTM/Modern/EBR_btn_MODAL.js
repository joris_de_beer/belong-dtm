var clickData = {
	event:		event,
	target:		this,
	action:		s.get('detail.eventData.data.action', event),
	modalName:	s.get('detail.eventData.data.modalName', event)||"",
};

_satellite.setVar('event_category',"modal");
_satellite.setVar('event_action',  clickData.action);
_satellite.setVar('event_label', clickData.modalName );

if(_satellite.settings.notifications){
	console.log('promo/MODAL',clickData);
}
if(clickData.action) {
	return true;
}
