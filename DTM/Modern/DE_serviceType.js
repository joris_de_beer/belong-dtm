/*	Data Element to manage setting of eVar71 Service Type variable.
	Replaces the following legacy data elements:
	* serviceStatusTypeClass
	* broadbandServiceType
	* fbbServiceType
*/
// If Adobe's s{} object isn't ready, just return early.
if(typeof s === "undefined"){return}

// Attempt to retrieve the service type information from either the My Account data layer, or the custom data layer embedded in the Order Tracking cards

var serviceType = s.get('BELONG.dataLayer.serviceType', window) || s.get('BELONG.dataLayer.orderData.serviceType',window),
	cookieExpiryDate = new Date();
	cookieExpiryDate.setTime(cookieExpiryDate.getTime()+(45*24*60*60*1000));

// If ServiceType was found, write to a cookie and return.
if(serviceType){
	s.Util.cookieWrite('s_v71', serviceType, cookieExpiryDate )
	_satellite.notify('serviceType found in datalayer',1);
	return serviceType;

// Fallback to looking for serviceType in cookie:
}else{
	_satellite.notify('serviceType found in s_v71 cookie',1);
	serviceType =  s.Util.cookieRead('s_v71');
	// Delete cookies previously set using DTM because they will be set to the wrong domain (www.belong.com.au, not .belong.com.au)
	_satellite.removeCookie('s_v71');
	// Force write the cookie back to .belong.com.au using the s.Util.cookieWrite function.
	s.Util.cookieWrite('s_v71', serviceType, cookieExpiryDate );
	return serviceType;
}
