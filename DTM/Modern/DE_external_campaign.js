/* Date element to populate eVar11 */
var returnVal,
	legacy_campaign_code = _satellite.getQueryParamCaseInsensitive('xid'),
	gclid = _satellite.getQueryParamCaseInsensitive('gclid'),
    utmSource = _satellite.getVar('utm_source'),
    utmMedium = _satellite.getVar('utm_medium'),
    utmCampaign = _satellite.getVar('utm_campaign');

// If there is ?xid= query param, AND all utm params are empty, return the legacy legacy campaign code
if(legacy_campaign_param && !(utmSource||utmMedium||utmCampaign)) {
	return legacy_campaign_param;

// If there is ?gclid= query param (Adwords Click ID), AND all utm params are empty, return the "google:cpc:sem"
}else if(gclid && !(utmSource||utmMedium||utmCampaign)){
	return "google:cpc:sem";

// if there are any UTM params, return as eVar11.
}else if(utmSource||utmMedium||utmCampaign){
	return [utmSource, utmMedium, utmCampaign].join(':').replace(/::/g, ':');
}
