reevaluatedPageData= {
	pageName: _satellite.getVar('page_name'),
	pageTitle: document.title,
	referringURL: _satellite.getVar('referrer'),
	sysEnv : _satellite.getVar('environment'),
	breadCrumbs : location.pathname.split('/').slice(1,100),
	publishDate : String(new Date()),
	publisher: _satellite.getVar('site_name')
}
s.extend(window.digitalData.page.pageInfo, reevaluatedPageData)
_satellite.notify('Updated the digitalData layer',3);
return true;


// Initial object
digitalData.page = {
	pageName: "maxis:business:contact-us",
	siteSection: "business",
	subSection: "contact-us"
}

// A new object, that only has some of the values set. Some we want to overwrite, and some we want to take from the original

formData = {
	pageName: "maxis:business:contact-us:complete",
	formResult: "form-submitted"
}

s.extend(digitalData.page, formData)

// Resulting object:
digitalData.page = {
	pageName: "maxis:business:contact-us:complete",
	siteSection: "business",
	subSection: "contact-us",
	formResult: "form-submitted"
} 