// Determine which mobile addons the visitor has selected in the Get A Sim form
var selectedTopups = _satellite.getQueryParamCaseInsensitive('a'),
	scrapedText = ($('.plan-summary__addon').text()||"").toLowerCase();

if(scrapedText){
	_satellite.notify('DE_mobile_plan_topup using scrapedText:'+scrapedText,1);
	return scrapedText + " mobile topup ("+selectedTopups+")";
}else{
	_satellite.notify('DE_mobile_plan_topup undetermined',1);
	return "Undetermined mobile topup ("+selectedTopups+")";
}
