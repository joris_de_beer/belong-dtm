// Determine which mobile plan the visitor has selected in the Get A Sim form
var selectedPlan = _satellite.getQueryParamCaseInsensitive('p'),
	eventWithData = s.getMostRecentEvent('mobiles/products/UPDATE_PRODUCTS');
	scrapedText = ($('.plan-summary__plan').text()||"").replace(/(small|regular|large)/i,"$1 ")
var plans = s.get('data.data.plans',eventWithData)||"Unknown Plan"

// First try and get the plan name from the dataLayer.
if(selectedPlan!==undefined||plans!==undefined) {
	for (i in plans) {
		if (plans[i].id === selectedPlan) {
			_satellite.notify('DE_mobile_plan_name using dataLayer:'+plans[i].name,1);
			return plans[i].name
		}
	}
}else if(scrapedText){ // sometimes it's not available (such as after a page reload), so scrape from page
	_satellite.notify('DE_mobile_plan_name using scrapedText:'+scrapedText,1);
	return scrapedText + " mobile plan";
}else{
	_satellite.notify('DE_mobile_plan_name undetermined',1);
	return "Undetermined mobile plan";
}
