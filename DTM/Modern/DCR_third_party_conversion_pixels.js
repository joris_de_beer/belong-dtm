if(location.pathname==="/mobile/get-sim/success"){
	// Get Sim Complete conversion event. Requested by elli.mcdougall@omd.com 15/6/18
	if(typeof adalyserTracker==="function"){
		adalyserTracker("trackEvent", "lce3", {a4:"SIM"},true);
	}
	// DCM: 7521385|Belong_Mobile_Sales_Get a SIM_Thank you|Sales
	if(typeof gtag==="function"){
		gtag('event', 'purchase', {
			'allow_custom_scripts': true,
			'value': '0',
			'transaction_id': _satellite.getVar('rand8'),
			'u1': 'getsim',
			'send_to': 'DC-8705912/sales0/belon00+transactions'
		});
	}
	// Facebook Pixel (Accuen account)
	if(typeof window.fbq === "function"){
		fbq('track', 'Purchase', {
			currency: 'AUD',
			value: 1
		});
	}
}
if(location.pathname==="/mobile/connect/activating"){
	// DCM: 7521385|Belong_Mobile_Sales_Connect_Thank you|Sales
	if(typeof gtag==="function"){
		gtag('event', 'purchase', {
			'allow_custom_scripts': true,
			'value': '0',
			'transaction_id': _satellite.getVar('rand8'),
			'send_to': 'DC-8705912/sales0/belon0+transactions'
		});
	}
}

if(/mobile\/smartphones(\/|$)/.test(location.pathname)){
	// DCM: ??|Belong_Handsets_Landing Page_Get A Smartphone Button|11/21/2018
	if(typeof gtag==="function"){
		gtag('event', 'conversion', {
			'allow_custom_scripts': true,
			'send_to': 'DC-8705912/steps0/belon007+unique'
		});
	}
}