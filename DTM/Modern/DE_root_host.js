// Return the top level domain, either belong.com.au, or belongtest.com.au.
// Assumes an aussie .com.au suffix
if(/\.com$/.test(location.hostname)){
  	return location.hostname.split('.').reverse().splice(0,2).reverse().join('.');
}else{
	return location.hostname.split('.').reverse().splice(0,3).reverse().join('.');
}