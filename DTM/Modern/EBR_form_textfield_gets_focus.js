var el = this,
	inputLabel = $(this).parents('.input').find('label').text()||"",
	inputName = this.getAttribute('name');

// Prefer the input label field.
if(inputLabel){
	_satellite.setVar('input_label',inputLabel);
}else{ // Fallback to the name attribute on the text field.
	_satellite.setVar('input_label',inputName);
}
digitalData.el = el;
return true;
