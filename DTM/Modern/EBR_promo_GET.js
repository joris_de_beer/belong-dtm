var clickData = {
	event:		event,
	target:		this,
	serviceType:s.get('detail.eventData.data.serviceType', event).toLowerCase(),
	promoCode:	s.get('detail.eventData.data.promoCode', event),
};

if(clickData.serviceType==="mobile"){clickData.ctaLabel = "get a sim"}
if(clickData.serviceType==="broadband"){clickData.ctaLabel = "check address"}

_satellite.setVar('service_type',clickData.serviceType);
_satellite.setVar('event_category',"promo");
_satellite.setVar('event_action',  "click");
_satellite.setVar('event_label', clickData.serviceType+" "+clickData.ctaLabel );
_satellite.setVar('promo_code', clickData.promoCode);

if(_satellite.settings.notifications){
	console.log('promo/GET',clickData);
}
return true;
