var formName =	(((((window||{}).digitalData||{}).trackEvent||{}).data||{}).meta||{}).form // Look in trackEvent from REACT.
				||(((window||{}).digitalData||{}).form||{}).formName // Fallback to form objects this DTM DE creates.
				||""; // Finally, if we can't find it, return an empty string.
if(formName){
	formName = formName.replace(/.*\/|_/g," ").toLowerCase().trim();
}
return formName;
