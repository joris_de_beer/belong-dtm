// AAM Belong MODERN Custom Code
var declaredId, declaredPartnerId,
	auth0Id = _satellite.getVar('auth0_id'),
	auth0PartnerId = "512735",
	salesforcePartnerId = "442858",
	salesforceId = _satellite.getVar('salesforce_id'),
	utmUserId = _satellite.getVar('utm_user_id');

// Priority Order: Query param, Auth0, Salesforce cookie
declaredId = utmUserId || auth0Id || salesforceId;
declaredPartnerId = auth0Id ? auth0PartnerId : salesforcePartnerId;

var adobeDil = DIL.create({
	partner: "teamtelstra",
	uuidCookie: {
		name: 'aam_uuid',
		days: 30
	},
	declaredId: {
		dpid: declaredPartnerId,
		dpuuid: declaredId
	},
});
if (auth0Id) {
	adobeDil.api.aamIdSync({
		dpid: auth0PartnerId,
		dpuuid: auth0Id,
		minutesToLive: 1
	});
}
if (salesforceId) {
	adobeDil.api.aamIdSync({
		dpid: salesforcePartnerId,
		dpuuid: salesforceId,
		minutesToLive: 1
	});
}
if (document.referrer) {
	adobeDil.api.signals({
		c_dilReferer: document.referrer
	});
}
if (location.href) {
	adobeDil.api.signals({
		c_locationHref: _satellite.getVar('url')
	});
}

//URI data collection
function objIsEmpty(obj) {
	for (var prop in obj) {
		if (obj.hasOwnProperty(prop) && prop !== "")
			return false;
	}
	return true;
}
var uriData = DIL.tools.decomposeURI(document.URL);
delete uriData.search;
delete uriData.href;
if (!objIsEmpty(uriData.uriParams)) {
	adobeDil.api.signals(uriData.uriParams, 'c_');
};
delete uriData.uriParams;
adobeDil.api.signals(uriData, 'c_')

var _scDilObj;
if (typeof s != 'undefined' && s === Object(s) && typeof s.account != 'undefined' && s.account) {
	_scDilObj = s_gi(s.account);
} else {
	_scDilObj = s_gi(s_account);
}

DIL.modules.siteCatalyst.init(_scDilObj, adobeDil, {
	names: ['pageName', 'channel', 'campaign', 'products', 'events', 'pe', 'referrer', 'server', 'purchaseID', 'zip', 'state'],
	iteratedNames: [{
		name: 'eVar',
		maxIndex: 100
	}, {
		name: 'prop',
		maxIndex: 75
	}, {
		name: 'pev',
		maxIndex: 3
	}, {
		name: 'hier',
		maxIndex: 4
	}]
});

if (_satellite.settings.notifications) {
	console.log('BL MODERN AAM ID SYNC', {
		'adobeDil': adobeDil,
		'auth0Id': auth0Id, 'auth0PartnerId': auth0PartnerId,
		'salesforceId': salesforceId, 'salesforcePartnerId': salesforcePartnerId,
		'declaredId':declaredId, 'declaredPartnerId':declaredPartnerId,
		'utmUserId': utmUserId,
	})
}
