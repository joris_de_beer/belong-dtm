/* PLR: belong mobile marketing scripts */
window.dataLayer= window.dataLayer || [];
window.gtag		= function(){dataLayer.push(arguments)};

var domainValidForMarketingTags = _satellite.getVar('is_marketing_script_safe'),
	mobile_plan_name = _satellite.getVar('mobile_plan_name');

/* GA - Google Tag Manager Script for OMD */
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var
f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K9CVSMX');

/* DCM - Google Tag Manager Script for OMD */
_satellite.loadScript(
	'//www.googletagmanager.com/gtag/js?id=DC-8705912',
	function(){
		gtag('js', new Date());
		gtag('config', 'DC-8705912');
	}
);

/* DCM: 7488159|Belong_Brand_Audiences_All Pages|Audiences */
if(domainValidForMarketingTags){
	gtag('event', 'conversion', {
		'allow_custom_scripts': true,
		'u5': _satellite.getVar('url'),
		'send_to': 'DC-8705912/audie0/belon0+standard'
	});
}

/* DCM: 7509461|Belong_Brand_Landing Page_Homepage|Landing Page */
/* Belong homepage uses the Belong MODERN/Mobile DTM Property */
if(domainValidForMarketingTags && location.pathname ==="/"){
	gtag('event', 'conversion', {
		'allow_custom_scripts': true,
		'u5': _satellite.getVar('url'),
		'send_to': 'DC-8705912/landi0/belon000+unique'
	});
}

/* DCM: 7509452|Belong_Mobile_Landing Page_Mobile|Landing Page */
if(domainValidForMarketingTags && /^\/mobile(\/|$)/.test(location.pathname)){
	gtag('event', 'conversion', {
		'allow_custom_scripts': true,
		'send_to': 'DC-8705912/landi0/belon0+unique'
	});
}

/* DCM: 7522714|Belong_Mobile_Step 1 _Connect_Your Account|Steps */
if(domainValidForMarketingTags && /^\/mobile\/connect(\/|$)/.test(location.pathname)){
	gtag('event', 'conversion', {
		'allow_custom_scripts': true,
		'u5': _satellite.getVar('url'),
		'send_to': 'DC-8705912/steps0/belon001+unique'
	});
}

/* DCM: 7520356|Belong_Mobile_Step 1 _Plans_Mobile SIM Only Plans|Steps */
if(domainValidForMarketingTags && /^\/mobile\/plans(\/|$)/.test(location.pathname)){
	gtag('event', 'conversion', {
		'allow_custom_scripts': true,
		'u5': _satellite.getVar('url'),
		'send_to': 'DC-8705912/steps0/belon0+unique'
	});
}

/* DCM: 7488150|Belong_Mobile_Step 2_Get a SIM_Landing Page|Steps */
if(domainValidForMarketingTags && /^\/mobile\/get-sim(\/|$)/.test(location.pathname)) {
	gtag('event', 'conversion', {
		'allow_custom_scripts': true,
		'u1': mobile_plan_name,
		'send_to': 'DC-8705912/steps0/belon00+unique'
	});
}

/* DCM: 7488150|Belong_Mobile_Step 2_Get a SIM_Landing Page|Steps */
if(domainValidForMarketingTags && /^\/mobile\/get-sim(\/|$)/.test(location.pathname)) {
	gtag('event', 'conversion', {
		'allow_custom_scripts': true,
		'u1': mobile_plan_name,
		'send_to': 'DC-8705912/steps0/belon00+unique'
	});
}

/* DCM: ??|Belong_Handsets_Landing Page_Homepage|Smartphones */
if(domainValidForMarketingTags && /^\/mobile\/smartphones(\/|$)/.test(location.pathname)){
	gtag('event', 'conversion', {
		'allow_custom_scripts': true,
		'send_to': 'DC-8705912/landi0/belon001+unique'
	});
}

/* Facebook Pixel Code */
if(domainValidForMarketingTags){
    var belongFacebookPixelId = "816534751879197";
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', belongFacebookPixelId );
    fbq('track', 'PageView');
}

// Adalyser Marketing Script. Requested by elli.mcdougall@omd.com 15/6/18
if(domainValidForMarketingTags) {
	(function (windowAlias, documentAlias, trackerName) {
		if (!windowAlias[trackerName]) {
			windowAlias.GlobalAdalyserNamespace = windowAlias.GlobalAdalyserNamespace
				|| [];
			windowAlias.GlobalAdalyserNamespace.push(trackerName);
			windowAlias[trackerName] = function () {
				(windowAlias[trackerName].q = windowAlias[trackerName].q || []).push(arguments)
			};
			windowAlias[trackerName].q = windowAlias[trackerName].q || [];
			var nel = documentAlias.createElement("script"),
				fel = documentAlias.getElementsByTagName("script")[0];
			nel.async = 1;
			nel.src = "//c5.adalyser.com/adalyser.js?cid=belong";
			fel.parentNode.insertBefore(nel, fel)
		}
	}(window, document, "adalyserTracker"));

	window.adalyserTracker("create", {
		campaignCookieTimeout: 15552000,
		conversionCookieTimeout: 604800,
		clientId: "belong",
		trafficSourceInternalReferrers: ["^(.*\\.)?belong.com$"]
	});
	window.adalyserTracker("trackSession", "lce1", {});
}
