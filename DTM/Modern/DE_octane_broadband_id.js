// Returns the mobile id for the user from one of two locations.
if(typeof window.s === "object" && typeof window.s.get === "function") {
	var viaWithAuthObj = s.get('trackEvent.detail.eventStore.withAuth.signedInUser.mobileId', window.digitalData),
		viaAsynAccount = s.get('trackEvent.store.account.asyncAccount.data.mobileId', window.digitalData);
// Prefer viaWithAuthObj, but sometimes it's empty so fallback to viaAsynAccount;
	return viaWithAuthObj ? viaWithAuthObj : viaAsynAccount;
}
