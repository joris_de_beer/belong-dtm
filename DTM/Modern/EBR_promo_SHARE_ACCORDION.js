var clickData = {
  event:		event,
  target:		this,
  state: s.get('detail.eventData.data.state', event).replace(/^open$/,'opened')
};
_satellite.setVar('event_category',"promo");
_satellite.setVar('event_action',  "click");
_satellite.setVar('event_label', "accordion "+clickData.state);

if(_satellite.settings.notifications){
	console.log('promo/SHARE_ACCORDION',clickData);
}
return true;
