/*	NOTE: sometimes analytics-trackEvent is triggered before s_code has loaded,
	so some utility functions are duplicated here.
*/
try{
	window.digitalData=window.digitalData||{};
	window.digitalData.trackEvents=window.digitalData.trackEvents||{};
	var s = window.s || {
		get : function(path, data) {
			var i, path = (typeof path==="string")?path.split('.'):path;
			var len = path.length;
			for (i = 0; typeof data === 'object' && data !==null && i < len; ++i) {data = data[path[i]];}
			return data;
		}
	}
	var trackEvent = {
		sequence:	Object.keys(digitalData.trackEvents).length+1,
		customEvent:event,
		target:     this,
		detail:     event.detail,
		type:       event.detail.eventType,
		action:     s.get('detail.eventData.payload.action',event),
		store:      event.detail.eventStore,
		data:       event.detail.eventData,
		route:      event.detail.eventRoute
	}
	var trackEventLabel = trackEvent.sequence+": "+trackEvent.type;
	window.digitalData.trackEvent=trackEvent;
	window.digitalData.trackEvents[trackEventLabel] = trackEvent // Keep a list of all trackEvents
	_satellite.notify("digitalData.trackEvents['"+trackEventLabel+"']", 3);
	document.querySelector('body').dispatchEvent(new CustomEvent(trackEvent.type, trackEvent));
	return true;
}catch(error){
	console.warn('analyticsTrackEvent error',error,event);
	return false
}
// comment
