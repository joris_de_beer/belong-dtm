/* Data Element: site_type
 Try to determine if user is viewing the page on a mobile device browser,
 a webview in an app, or on a desktop
*/
var standalone =window.navigator.standalone, // On iOS, webview's navigator has a 'standalone' key.
    iOS =		navigator.userAgent.match(/iPhone|iPad|iPod/i),
    android =	navigator.userAgent.match(/Android/i),
    safari =	/safari/.test(navigator.userAgent.toLowerCase()),
    // User agent suffix - To target BELONG android app
    ua_suffix =	navigator.userAgent.match(/BelongApp\/[0-9\.]+$/); // On Android, the UA is modified.
    uiIdiom	=	window.UI_IDIOM, // Belong Mobile html will set UI_IDIOM to browser or "x_ANDROID"
	returnVal =	"Unkown" // Adobe DTM data elements use stringy booleans (aaargh WTF??)

if      (iOS && standalone){	returnValue = "iOS App" }
else if (android && ua_suffix){	returnValue =  "Android App" }
else if (uiIdiom && /android/i.test(uiIdiom)){	returnValue =  "Android App" }
else { returnVal = _satellite.browserInfo.deviceType }

return returnVal;
