if (/^\/account\/mobile$/.test(location.pathname)) {
	var form = {
		event: event,
		target: this,
		formId: _satellite.getVar('form_id'),
		user: ((event.detail || {}).eventData || {}).user,
		userType: function () {
			var returnVal = "Unknown";
			try {
				if (this.user.isAgent()) {
					returnVal = "Agent";
				} else if (this.user.isSupportAgent()) {
					returnVal = "Support Agent";
				} else if (this.user.isOutboundAgent()) {
					returnVal = "Outbound Agent";
				} else if (this.user.isInboundAgent()) {
					returnVal = "Inbound Agent";
				} else {
					returnVal = "Customer";
				}
			} catch (error) {
				if (_satellite.settings.notifications) {
					console.log(error)
				}
				this.tagError = error;

			}
			return returnVal;
		},
		trackEvent: digitalData.trackEvent,
		eventCategory: "authentication",
		eventAction: "login",
		eventLabel: "login" + s.get('trackEvent.detail.eventRoute', digitalData).replace(/\//g, '-'),
		withAuthSignin: s.getMostRecentEvent('WITHAUTH_SIGNIN')
	};

	_satellite.setVar('event_category', form.eventCategory);
	_satellite.setVar('event_action', form.eventAction);
	_satellite.setVar('event_label', form.eventLabel);
	_satellite.setVar('customer_type', form.userType());
	_satellite.setVar('tag_error', form.error || "");
	digitalData.form = form;
	return true;
}