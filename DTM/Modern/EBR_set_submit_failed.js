try{
	var form = {
		event:event,
		//trackEvent: JSON.parse(JSON.stringify(digitalData.trackEvent)),
		target:		this,
		formId:		_satellite.getVar('form_id'),
		formName:	_satellite.getVar('form_name'),
		formPrefix:	_satellite.getVar('form_prefix'),
		formSuffix:	_satellite.getVar('form_suffix'),
		formStepName:_satellite.getVar('form_step_name'),
		trackEvent: digitalData.trackEvent,
		elementsWithErrors : $('input').parents('.input--error, .input-option--error').find('.input__error'),
		inputsWithErrors: {},
		errorDescriptions: []
	};
	digitalData.form=form;
	_satellite.notify('Looking for inputs with errors at: digitalData.form.trackEvent.store.form.'+form.formId+'.syncErrors',1);
	form.inputsWithErrors = s.get('trackEvent.store.form.'+form.formId+'.syncErrors',form)
	form.inputsWithErrorsAsString = Object.keys(digitalData.form.inputsWithErrors).join('|')
	_satellite.setVar('input_errors',form.inputsWithErrorsAsString)

	// Note, there is a bug, and the errors displayed on the page aren't always available at the time this Event Based Rule runs in DTM.
	// So it's recommended the error_descriptions data element is not used until this bug can be resolved.
	if(form.elementsWithErrors.length){
		for(var i=0; i<form.elementsWithErrors.length;i++){
			form.errorDescriptions.push($(form.elementsWithErrors[i]).text())
		}
		_satellite.setVar('error_descriptions',form.errorDescriptions.join('|'));
	}
	console.info('EBR_set_submit_failed:', form);

	return true;
}catch(error){
	console.warn('EBR_set_submit_failed error',error);
	return false;
}
