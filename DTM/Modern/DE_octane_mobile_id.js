// Returns the broadband id for the user from one of two locations.
if(typeof window.s === "object" && typeof window.s.get === "function") {
	var viaWithAuthObj = s.get('trackEvent.detail.eventStore.withAuth.signedInUser.broadbandId', window.digitalData),
		viaAsynAccount = s.get('trackEvent.store.account.asyncAccount.data.broadbandId', window.digitalData);
// Prefer viaWithAuthObj, but sometimes it's empty so fallback to viaAsynAccount;
	return viaWithAuthObj ? viaWithAuthObj : viaAsynAccount;
}
