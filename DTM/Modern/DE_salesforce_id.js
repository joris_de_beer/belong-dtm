//Get the SFID from the userDatails cookie and return it
try {
	function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		
		}
		return "";
	}
	var SFID = getCookie('userDetails');
  if (SFID != "") {
 	SFID = JSON.parse(decodeURIComponent(SFID));
	return SFID.userId;
  }
  else return "";
}
catch(error) {
	console.log('userDetails cookie read error')
}