console.info("@@redux-form/SET_SUBMIT_SUCCEEDED",event, this, digitalData.trackEvent)
try{
	var form = {
		event:		event,
		target:		this,
		formId:		_satellite.getVar('form_id'),
    	trackEvent: digitalData.trackEvent
	};
	// When submitting a support request, attempt to find the name of the article.
	if(form.formId==="support/REQUEST_FORM"){
		// Look for article name
		form.updateArticleEvent = s.getMostRecentEvent('UPDATE_ARTICLE');
		var category = 'support request',
			action= 'create',
			label = s.get('updateArticleEvent.data.data.caseSubject',form);
		// Save to digitalData for debugging.
		form.category=category;form.action=action;form.label=label;
		// Set a temporary data element
		_satellite.setVar('custom_event_variable', category+":"+action+":"+label);
	}
	digitalData.form=form;
	return true;
}catch(error){
	console.warn('----',error);
}
